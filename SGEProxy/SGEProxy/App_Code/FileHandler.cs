﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.Configuration;
using DBTools;

/// <summary>
/// Summary description for FileHandler
/// </summary>
public class FileHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        var Type = context.Request.QueryString["TYP"];
        var FileUID = context.Request.QueryString["FUID"];
        var MarcarOpened = context.Request.QueryString["MRC"];

        ST_File MyFile = ReadFileDB(Type, FileUID, MarcarOpened);


        if (!MyFile.STA)
        {
            context.Response.Write("<BR><BR><BR><center><B>Archivo no encontrado</B></center>");
        }
        else if (MyFile.DAT.Length == 0)
        {
            context.Response.Write("<BR><BR><BR><center><B>Archivo dañado</B></center>");
        }
        else
        {
            context.Response.Clear();
            context.Response.ContentType = MyFile.EXT;

            context.Response.Charset = "UTF-8";
            context.Response.Expires = 0;
            context.Response.AppendHeader("Content-transfer-encoding", "binary");
            context.Response.AppendHeader("Content-Disposition", "inline;filename=" + MyFile.NAM);
            //context.Response.AppendHeader("Content-Disposition", "attachment; filename=TesGestion" + Extension);
            context.Response.BinaryWrite(MyFile.DAT);
            context.Response.Flush();
            context.Response.End();
        }
     
    }

    private class ST_File
    {
        public Guid UID { get; set; }  //UID
        public string NAM { get; set; }  //FileName
        public string EXT { get; set; }  //FileExtension
        public byte[] DAT { get; set; }  //FileData
        public bool STA { get; set; }  //Estado OK true, Failed false

        public ST_File() {
            this.STA = false;
        }
    }
    
    private ST_File ReadFileDB(string Type, string UID, string MarcarOpened)
    {
        var sLog = "UID=" + UID;
        csData cData = null;
        ST_File mResult = new ST_File();
        try
        {
            string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
            cData = new csData();
            bool ConectedOk = cData.Connect(strConn);

            if (!ConectedOk)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "!ConectedOk", strConn, sLog);
                return mResult;
            }
            string sSQL="";

            switch (Type)
            {

                case "ANX":
                    sSQL = @"Select ANX_UID UID, ANX_NAME NAME, ANX_CONTENT_TYPE CONTENT_TYPE, 0 OPENED, ANX_FILE FILEDATA
                        From RADIO.ANEXOS 
                        Where ANX_UID='" + UID + "'";
                    break;

                case "ANXT":
                    sSQL = @"Select ANX_UID UID, ANX_NAME NAME, ANX_CONTENT_TYPE CONTENT_TYPE, 0 OPENED, ANX_FILE FILEDATA
                        From RADIO.ANEXOS_TEMP 
                        Where ANX_UID='" + UID + "'";
                    break;

                case "COM":
                    sSQL = @"Select COM_UID UID, COM_NAME NAME, 'application/pdf' CONTENT_TYPE, 0 OPENED, COM_FILE FILEDATA
                        From RADIO.COMUNICADOS 
                        Where COM_UID='" + UID + "'";
                    break;

                case "DOC":
                    sSQL = @"Select DOC_UID UID, DOC_NAME NAME, 'application/pdf' CONTENT_TYPE, DOC_OPENED OPENED, DOC_FILE FILEDATA
                        From RADIO.DOCUMENTS 
                        Where DOC_UID='" + UID + "'";
                    break;

                case "TEC":
                    sSQL = @"Select TECH_UID UID, TECH_NAME NAME, TECH_CONTENT_TYPE CONTENT_TYPE, 0 OPENED, TECH_FILE FILEDATA
                        From RADIO.TECH_DOCUMENTS 
                        Where TECH_UID='" + UID + "'";
                    break;

                case "PTRES":
                    sSQL = @"Select RES_PLT_UID UID, RES_PLT_NAME + '.pdf' NAME, 'application/pdf' CONTENT_TYPE, 0 OPENED, RES_PLT_PDF_FILE FILEDATA
                        From RADIO.RESOLUCION_PLANTILLAS
                        Where RES_PLT_UID='" + UID + "'";
                    break;

                case "RES":
                    sSQL = @"Select SOL_UID UID, isnull(RES_NAME,'Resolucion') + '.pdf' NAME, 'application/pdf' CONTENT_TYPE, 0 OPENED, RES_PDF_FILE FILEDATA
                        From RADIO.RESOLUCIONES
                        Where SOL_UID='" + UID + "'";
                    break;

                case "VIA":
                    sSQL = @"Select SOL_UID UID, isnull(RES_NAME,'Resolucion') + '.pdf' NAME, 'application/pdf' CONTENT_TYPE, 0 OPENED, RES_PDF_FILE FILEDATA
                        From RADIO.RESOLUCIONES_VIABILIDAD
                        Where SOL_UID='" + UID + "'";
                    break;
                case "TANX":
                    sSQL = @"Select ANX_UID UID, ANX_NAME NAME, ANX_CONTENT_TYPE CONTENT_TYPE, 0 OPENED, ANX_FILE FILEDATA
                        From RADIO.TECH_ANEXOS 
                        Where ANX_UID='" + UID + "'";
                    break;
                case "OBS":
                    sSQL = @"Select ID_IMAGEN UID, NOMBRE_IMAGEN NAME, 'image/jpeg' CONTENT_TYPE, 0 OPENED, IMAGEN FILEDATA
                        From RADIO.OBSERVACIONES_IMAGENES 
                        Where ID_IMAGEN='" + UID + "'";
                    break;

            }

            DataTable Tabla = cData.ObtenerDataTable(sSQL);
            var dt = Tabla.AsEnumerable().AsQueryable();

            if (dt.Count() != 1)
                return mResult;


            mResult = (from bp in dt
                       select new ST_File
                       {
                           UID = bp.Field<Guid>("UID"),
                           NAM = bp.Field<string>("NAME"),
                           EXT = bp.Field<string>("CONTENT_TYPE"),
                           DAT = bp.Field<byte[]>("FILEDATA"),
                           STA = true
                       }).ToList().FirstOrDefault();

            if (MarcarOpened == "1")
            {
                Tabla.Rows[0]["OPENED"] = 1;
                cData.ModifyDataTable(sSQL, Tabla);
            }

            return mResult;
        }
        catch (Exception e)
        {
            WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, sLog);
            return mResult;
        }
        finally
        {
            if (cData != null)
            {
                cData.CloseConnection();
            }
        }

    }

    private void WriteLogError(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
    {
        try
        {
            string sTesGestionProyectosLogs = WebConfigurationManager.AppSettings["SGELogs"];

            string fname = Path.Combine(sTesGestionProyectosLogs, "SGEProxy.txt");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            sb.AppendLine(FuncName);

            sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            if (!string.IsNullOrEmpty(sDesc0))
                sb.AppendLine("Desc0=" + sDesc0);
            if (!string.IsNullOrEmpty(sDesc1))
                sb.AppendLine("Desc1=" + sDesc1);
            if (!string.IsNullOrEmpty(sDesc2))
                sb.AppendLine("Desc2=" + sDesc2);
            if (!string.IsNullOrEmpty(sDesc3))
                sb.AppendLine("Desc3=" + sDesc3);
            sb.AppendLine("-------------------------------");

            File.AppendAllText(fname, sb.ToString());
        }
        catch
        {

        }

    }


    public bool IsReusable
    {
        get { return false; }
    }
}