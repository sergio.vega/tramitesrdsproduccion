﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

public class ExportHandler : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        //Para hacer debug
        /*
        Stream stream = context.Request.InputStream;
        BinaryReader br = new BinaryReader(stream);
        byte[] b = br.ReadBytes((int)stream.Length);
        File.WriteAllBytes(@"C:\inetpub\wwwroot\SGEProxy\Prueba1.txt", b);
        */
        if (context.Request.Form["contentType"] != null &&
           context.Request.Form["fileName"] != null &&
           context.Request.Form["data"] != null)
        {
            context.Response.Clear();
            context.Response.ContentType = context.Request.Form["contentType"].ToString();
            context.Response.Charset = "UTF-8";
            context.Response.Expires = 0;
            context.Response.AppendHeader("Content-transfer-encoding", "binary");
            context.Response.AppendHeader("Content-Disposition",
                                          "attachment; filename=" + context.Request.Form["fileName"].ToString());
            context.Response.BinaryWrite(Convert.FromBase64String(context.Request.Form["data"].ToString()));
            context.Response.Flush();
            context.Response.End();
        }
    }
    public bool IsReusable
    {
        get { return false; }
    }
}