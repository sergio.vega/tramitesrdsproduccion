﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.IO;
using System.Globalization;
using System.Configuration;
using System.Web.Configuration;

namespace DBTools
{
    public class csData
    {
        private SqlConnection CnSQL;
        
        public csData()
        {
        }

        public bool Connect(string strConnection)
        {
            try
            {
                CnSQL = new SqlConnection();
                CnSQL.ConnectionString = strConnection;
                CnSQL.Open();
                return true;
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Connect fail:", strConnection, "e:" + e.Message);
                return false;
            }
        }

        public string TraerValor(string sCommand, string sDefValue)
        {
            string sResult = sDefValue;

            SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
            try
            {
                var obj = CmSQL.ExecuteScalar();
                if (obj == null) sResult = sDefValue;
                else sResult = Convert.ToString(obj);
            }
            catch
            {
                sResult = sDefValue;
            }
            CmSQL.Dispose();
            CmSQL = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            if (String.IsNullOrEmpty(sResult))
                sResult = sDefValue;

            return sResult;
        }

        public int TraerValor(string sCommand, int iDefValue)
        {
            int iResult = iDefValue;

            SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);

            try
            {
                var obj = CmSQL.ExecuteScalar();
                if (obj == null) iResult = iDefValue;
                else iResult = Convert.ToInt32(obj);
            }
            catch
            {
                iResult = iDefValue;
            }
            //  try { iResult = Convert.ToInt32(CmSQL.ExecuteScalar()); }
            // catch { iResult = iDefValue; }
            CmSQL.Dispose();
            CmSQL = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            return iResult;
        }

        public DataSet ObtenerDataSet(string sCommand)
        {

            DataSet dtResultado = new DataSet();
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter();
                    SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);

                    daResultado.SelectCommand = CmSQL;
                    daResultado.Fill(dtResultado);
                    daResultado.Dispose();
                    daResultado = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
            }
            catch
            {
                //return null;
            }
            return dtResultado;
        }

        public bool ModifyDataSet(string sCommand, DataSet ds)
        {
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter(sCommand, CnSQL);
                    SqlCommandBuilder myBuilder = new SqlCommandBuilder(daResultado);
                    
                    daResultado.UpdateCommand = myBuilder.GetUpdateCommand();
                    daResultado.InsertCommand = myBuilder.GetInsertCommand();

                    SqlTransaction trans = CnSQL.BeginTransaction();

                    daResultado.UpdateCommand.Transaction = trans;
                    
                    
                    try
                    {
                        daResultado.Update(ds);
                        trans.Commit();
                        return true;
                    }
                    catch
                    {
                        trans.Rollback();
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRecs <= 0", sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                        return false;
                    }
                }
                else
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "CnSQL = null", sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                    return false;
                }
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                return false;
            }
        }

        public bool InsertDataSet(string sCommand, DataSet ds)
        {
            try
            {
                if (CnSQL != null)
                {
                    string[] lstCommand = sCommand.Split(';');

                    List<SqlDataAdapter> lstAdapters = new List<SqlDataAdapter>();

                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        SqlDataAdapter daResultado = new SqlDataAdapter();
                        SqlCommand CmSQL = new SqlCommand(lstCommand[i], CnSQL);
                        daResultado.SelectCommand = CmSQL;
                        SqlCommandBuilder myBuilder = new SqlCommandBuilder(daResultado);

                        daResultado.UpdateCommand = myBuilder.GetUpdateCommand();
                        daResultado.InsertCommand = myBuilder.GetInsertCommand();
                        daResultado.DeleteCommand = myBuilder.GetDeleteCommand();

                        lstAdapters.Add(daResultado);
                    }

                    SqlTransaction trans = CnSQL.BeginTransaction();
                    foreach (var Item in lstAdapters)
                    {
                        Item.UpdateCommand.Transaction = trans;
                        Item.InsertCommand.Transaction = trans;
                        Item.DeleteCommand.Transaction = trans;
                    }


                    try
                    {
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            lstAdapters[i].Update(ds.Tables[i]);
                        }
                        trans.Commit();
                        return true;

                    }
                    catch
                    {
                        trans.Rollback();
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRecs <= 0", sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                        return false;
                    }

                }
                else
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "CnSQL = null", sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                    return false;
                }
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sCommand, "Rows:" + ds.Tables[0].Rows.Count);
                return false;
            }
        }

        public DataTable ObtenerDataTable(string sCommand)
        {
            DataTable dtResultado = new DataTable();
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter();
                    SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                    daResultado.SelectCommand = CmSQL;
                    daResultado.Fill(dtResultado);
                    daResultado.Dispose();
                    daResultado = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
            }
            catch
            {
                //return null;
            }
            return dtResultado;
        }

        public bool InsertDataTable(string sCommand, DataTable dt)
        {
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter();
                    SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                    daResultado.SelectCommand = CmSQL;

                    SqlCommandBuilder myBuilder = new SqlCommandBuilder(daResultado);
                    //myBuilder.GetUpdateCommand();
                    //daResultado.UpdateCommand = myBuilder.GetUpdateCommand();
                    daResultado.InsertCommand = myBuilder.GetInsertCommand();
                    daResultado.Update(dt);

                    daResultado.Dispose();
                    daResultado = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    return true;
                }
                else
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "NA", "CnSQL = null", sCommand, "Rows:" + dt.Rows.Count);
                    return false;
                }
            }
            catch (SqlException x1)
            {
               
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
        }

        public bool ModifyDataTable(string sCommand, DataTable dt)
        {
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter();
                    SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                    daResultado.SelectCommand = CmSQL;

                    SqlCommandBuilder myBuilder = new SqlCommandBuilder(daResultado);
                    //myBuilder.GetUpdateCommand();
                    daResultado.UpdateCommand = myBuilder.GetUpdateCommand();
                    //daResultado.InsertCommand = myBuilder.GetInsertCommand();
                    int nRecs = daResultado.Update(dt);
                    
                    daResultado.Dispose();
                    daResultado = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    if (nRecs <= 0)
                    {
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRecs <= 0", sCommand, "Rows:" + dt.Rows.Count);
                        return false;
                    }
                    return true;
                }
                else
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "CnSQL = null", sCommand, "Rows:" + dt.Rows.Count);
                    return false;
                }
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
        }

        public bool DeleteDataTable(string sCommand, DataTable dt)
        {
            try
            {
                if (CnSQL != null)
                {
                    SqlDataAdapter daResultado = new SqlDataAdapter();
                    SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                    daResultado.SelectCommand = CmSQL;

                    SqlCommandBuilder myBuilder = new SqlCommandBuilder(daResultado);
                    //myBuilder.GetUpdateCommand();
                    //daResultado.UpdateCommand = myBuilder.GetUpdateCommand();
                    daResultado.DeleteCommand = myBuilder.GetDeleteCommand();
                    daResultado.Update(dt);

                    daResultado.Dispose();
                    daResultado = null;
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    return true;
                }
                else
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "NA", "CnSQL = null", sCommand, "Rows:" + dt.Rows.Count);
                    return false;
                }
            }
            catch (SqlException x1)
            {

                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sCommand, "Rows:" + dt.Rows.Count);
                return false;
            }
        }

        public bool ClickExecuteNonQuery(string sSQL)
        {
            try
            {
                string sCommand = sSQL;

                SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                int nRet = CmSQL.ExecuteNonQuery();

                CmSQL.Dispose();
                CmSQL = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                if (nRet <= 0)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRet <= 0", sSQL);
                    return false;
                }
                return true;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sSQL);
                return false;
            }
        }

        public bool ClickExecuteNonQueryV2(string sSQL)
        {
            try
            {
                string sCommand = sSQL;

                SqlCommand CmSQL = new SqlCommand(sCommand, CnSQL);
                int nRet = CmSQL.ExecuteNonQuery();

                CmSQL.Dispose();
                CmSQL = null;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                if (nRet < 0)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRet < 0", sSQL);
                    return false;
                }
                return true;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sSQL);
                return false;
            }
        }

        private void WriteLogError(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
        {
   
            try
            {

                string sTesGestionProyectosLogs = WebConfigurationManager.AppSettings["SGELogs"];

                string fname = Path.Combine(sTesGestionProyectosLogs, "SGEProxy.txt");

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                sb.AppendLine(FuncName);

                sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (!string.IsNullOrEmpty(sDesc0))
                    sb.AppendLine("Desc0=" + sDesc0);
                if (!string.IsNullOrEmpty(sDesc1))
                    sb.AppendLine("Desc1=" + sDesc1);
                if (!string.IsNullOrEmpty(sDesc2))
                    sb.AppendLine("Desc2=" + sDesc2);
                if (!string.IsNullOrEmpty(sDesc3))
                    sb.AppendLine("Desc3=" + sDesc3);
                sb.AppendLine("-------------------------------");

                File.AppendAllText(fname, sb.ToString());
            }
            catch
            {

            }
        }

        public bool InsertPortalEvent(int IDUserWeb, string FuncName, string sLog)
        {
            SqlCommand CmSQL = null;
            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append("Insert INTO TGP_PortalEvents (IDUserWeb, FuncName, sLog ) ");
                sb.Append("VALUES ");
                sb.Append("(");
                sb.Append("@IDUserWeb, ");
                sb.Append("@FuncName, ");
                sb.Append("@sLog");
                sb.Append(")");

                string sSQL = sb.ToString();

                CmSQL = new SqlCommand(sSQL, CnSQL);

                CmSQL.Parameters.AddWithValue("@IDUserWeb", IDUserWeb);
                CmSQL.Parameters.AddWithValue("@FuncName", FuncName);
                CmSQL.Parameters.AddWithValue("@sLog", sLog);

                int nRows = CmSQL.ExecuteNonQuery();

                if (nRows <= 0)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRows <= 0", "No se pudo insertar el registro.", sLog);
                    return false;
                }

                return true;
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sLog);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sLog);
                return false;
            }
            finally
            {
                if (CmSQL != null)
                {
                    CmSQL.Dispose();
                    CmSQL = null;
                }
            }
        }

        public bool InsertPortalVisit(int IDCompany, int IDUserWeb, string Modulo, string Submodulo, string Metodo, string Parametros)
        {
            string sLog = "Metodo=" + Metodo + "; Parametros=" + Parametros;

            SqlCommand CmSQL = null;
            try
            {


                StringBuilder sb = new StringBuilder();
                sb.Append("Insert INTO Portal_Visits (IDCompany, IDUserWeb, Modulo, Submodulo, Metodo, Parametros) ");
                sb.Append("VALUES ");
                sb.Append("(");
                sb.Append("@IDCompany, ");
                sb.Append("@IDUserWeb, ");
                sb.Append("@Modulo, ");
                sb.Append("@Submodulo, ");
                sb.Append("@Metodo, ");
                sb.Append("@Parametros");
                sb.Append(")");

                string sSQL = sb.ToString();

                CmSQL = new SqlCommand(sSQL, CnSQL);

                CmSQL.Parameters.AddWithValue("@IDCompany", IDCompany);
                CmSQL.Parameters.AddWithValue("@IDUserWeb", IDUserWeb);
                CmSQL.Parameters.AddWithValue("@Modulo", Modulo);
                CmSQL.Parameters.AddWithValue("@Submodulo", Submodulo);
                CmSQL.Parameters.AddWithValue("@Metodo", Metodo);
                CmSQL.Parameters.AddWithValue("@Parametros", Parametros);

                int nRows = CmSQL.ExecuteNonQuery();

                if (nRows <= 0)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRows <= 0", "No se pudo insertar el registro.", sLog);
                    return false;
                }

                return true;
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sLog);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sLog);
                return false;
            }
            finally
            {
                if (CmSQL != null)
                {
                    CmSQL.Dispose();
                    CmSQL = null;
                }
            }
        }

        public bool InsertAlert(int IDAlert, int IDUser, int IDFiltro, int IDFiltro1, Guid? IDFiltroUID,  int Valor1)
        {
            string sLog = "IDAlert=" + IDAlert + "; IDUser=" + IDUser + "; IDFiltro=" + IDFiltro + "; IDFiltro1=" + IDFiltro1 + "; IDFiltroUID=" + IDFiltroUID + "; Valor1=" + Valor1;

            SqlCommand CmSQL = null;
            try
            {


                StringBuilder sb = new StringBuilder();
                sb.Append("Insert INTO TGP_Alertas (IDAlert, IDUser, IDFiltro, IDFiltro1, IDFiltroUID, Valor1) ");
                sb.Append("VALUES ");
                sb.Append("(");
                sb.Append("@IDAlert, ");
                sb.Append("@IDUser, ");
                sb.Append("@IDFiltro, ");
                sb.Append("@IDFiltro1, ");
                sb.Append("@IDFiltroUID, ");
                sb.Append("@Valor1");
                sb.Append(")");

                string sSQL = sb.ToString();

                CmSQL = new SqlCommand(sSQL, CnSQL);

                CmSQL.Parameters.AddWithValue("@IDAlert", IDAlert);
                CmSQL.Parameters.AddWithValue("@IDUser", IDUser);
                CmSQL.Parameters.AddWithValue("@IDFiltro", IDFiltro);
                CmSQL.Parameters.AddWithValue("@IDFiltro1", IDFiltro1);
                if (IDFiltroUID.HasValue)
                    CmSQL.Parameters.AddWithValue("@IDFiltroUID", IDFiltroUID);
                else
                    CmSQL.Parameters.AddWithValue("@IDFiltroUID", (Object)DBNull.Value);
                CmSQL.Parameters.AddWithValue("@Valor1", Valor1);

                int nRows = CmSQL.ExecuteNonQuery();

                if (nRows <= 0)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "nRows <= 0", "No se pudo insertar el registro.", sLog);
                    return false;
                }

                return true;
            }
            catch (SqlException x1)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "SqlException x1", x1.Message, sLog);
                return false;
            }
            catch (Exception x)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception x", x.Message, sLog);
                return false;
            }
            finally
            {
                if (CmSQL != null)
                {
                    CmSQL.Dispose();
                    CmSQL = null;
                }
            }
        }

        public void CloseConnection()
        {
            if (CnSQL != null)
            {
                CnSQL.Close();

                CnSQL.Dispose();
            }
        }

    }
}