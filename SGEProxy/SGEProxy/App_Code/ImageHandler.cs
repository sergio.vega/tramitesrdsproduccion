﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using DBTools;

/// <summary>
/// Summary description for ImageHandler
/// </summary>
public class ImageHandler : IHttpHandler
{
    private class ST_ImageFile
    {
        public string CONTENT_TYPE { get; set; }
        public byte[] DAT { get; set; }  //FileData
        public bool STA { get; set; }  //Estado OK true, Failed false

        public ST_ImageFile()
        {
            this.STA = false;
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        var IMG_ID = context.Request.QueryString["IMG_ID"];
        ST_ImageFile MyImageFile = ReadFileDB(IMG_ID);

        if (!MyImageFile.STA)
        {
            context.Response.Write("");
        }
        else if (MyImageFile.DAT.Length == 0)
        {
            context.Response.Write("");
        }
        else
        {
            context.Response.Clear();
            context.Response.ContentType = MyImageFile.CONTENT_TYPE;

            context.Response.Charset = "UTF-8";
            context.Response.Expires = 0;
            context.Response.AppendHeader("Content-transfer-encoding", "binary");
            context.Response.AppendHeader("Content-Disposition", "inline;filename=" + IMG_ID);
            //context.Response.AppendHeader("Content-Disposition", "attachment; filename=TesGestion" + Extension);
            context.Response.BinaryWrite(MyImageFile.DAT);
            context.Response.Flush();
            context.Response.End();
        }

    }



    private ST_ImageFile ReadFileDB(string IMG_ID)
    {
        var sLog = "IMG_ID=" + IMG_ID;
        csData cData = null;
        ST_ImageFile mResult = new ST_ImageFile();
        try
        {
            string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
            cData = new csData();
            bool ConectedOk = cData.Connect(strConn);

            if (!ConectedOk)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "!ConectedOk", strConn, sLog);
                return mResult;
            }
            string sSQL = "";


            sSQL = @"Select IMG_CONTENT_TYPE, IMG_FILE
                    From RADIO.IMAGES
                    Where IMG_ID='" + IMG_ID +"'";
  

            DataTable Tabla = cData.ObtenerDataTable(sSQL);
            var dt = Tabla.AsEnumerable().AsQueryable();

            if (dt.Count() != 1)
                return mResult;


            mResult = (from bp in dt
                       select new ST_ImageFile
                       {
                           CONTENT_TYPE = bp.Field<string>("IMG_CONTENT_TYPE"),
                           DAT = bp.Field<byte[]>("IMG_FILE"),
                           STA = true
                       }).ToList().FirstOrDefault();

            return mResult;
        }
        catch (Exception e)
        {
            WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, sLog);
            return mResult;
        }
        finally
        {
            if (cData != null)
            {
                cData.CloseConnection();
            }
        }

    }

    private void WriteLogError(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
    {
        try
        {
            string sTesGestionProyectosLogs = WebConfigurationManager.AppSettings["SGELogs"];

            string fname = Path.Combine(sTesGestionProyectosLogs, "SGEProxy.txt");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            sb.AppendLine(FuncName);

            sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            if (!string.IsNullOrEmpty(sDesc0))
                sb.AppendLine("Desc0=" + sDesc0);
            if (!string.IsNullOrEmpty(sDesc1))
                sb.AppendLine("Desc1=" + sDesc1);
            if (!string.IsNullOrEmpty(sDesc2))
                sb.AppendLine("Desc2=" + sDesc2);
            if (!string.IsNullOrEmpty(sDesc3))
                sb.AppendLine("Desc3=" + sDesc3);
            sb.AppendLine("-------------------------------");

            File.AppendAllText(fname, sb.ToString());
        }
        catch
        {

        }

    }


    public bool IsReusable
    {
        get { return false; }
    }

}