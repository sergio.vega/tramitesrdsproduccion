﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SGEServiceApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            int n = 1;
            var Today = DateTime.Now.ToString("yyyyMMdd");

            Utilities.IniFile MyIni;
            string AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            AppPath = AppPath.Replace(@"file:\", "") + @"\";
            MyIni = new Utilities.IniFile(AppPath + "Master.ini");

            var NoWindow = new NoWindow();
            NoWindow.DoJobDesistir();
            NoWindow.Terminar();

            MyIni.WriteValue("Data", "Last", n);

            int m = MyIni.GetInt32("Data", "Total", 0);
            MyIni.WriteValue("Data", "Total", m + n);

            m = MyIni.GetInt32("Data", Today, 0);
            MyIni.WriteValue("Data", Today, m + n);
        }
    }
}
