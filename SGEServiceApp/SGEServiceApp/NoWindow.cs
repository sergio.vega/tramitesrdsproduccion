﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace SGEServiceApp
{
    class NoWindow
    {
        private SGEServiceApp.Properties.Settings settings;
        private string AppPath;

        public NoWindow()
        {
            AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            AppPath = AppPath.Replace(@"file:\", "") + @"\";
            settings = new SGEServiceApp.Properties.Settings();

        }

        public void Terminar()
        {

        }


        public void DoJobDesistir()
        {
            try
            {
                string sURL = settings.SGEWCF2;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(sURL);
                httpWebRequest.Timeout = 120 * 1000;
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Token:" + settings.TOKEN);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write("");
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    string result = streamReader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception ex", ex.Message);
            }

        }

        private void WriteLogError(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
        {
            try
            {

                string fname = Path.Combine(settings.SGELogs, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt");

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                sb.AppendLine(FuncName);

                sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (!string.IsNullOrEmpty(sDesc0))
                    sb.AppendLine("Desc0=" + sDesc0);
                if (!string.IsNullOrEmpty(sDesc1))
                    sb.AppendLine("Desc1=" + sDesc1);
                if (!string.IsNullOrEmpty(sDesc2))
                    sb.AppendLine("Desc2=" + sDesc2);
                if (!string.IsNullOrEmpty(sDesc3))
                    sb.AppendLine("Desc3=" + sDesc3);
                sb.AppendLine("-------------------------------");

                File.AppendAllText(fname, sb.ToString());
            }
            catch
            {

            }

        }
    }
}
