﻿namespace SGENavigate
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tLogin = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.tEmail = new System.Windows.Forms.TextBox();
            this.cGroup = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tLogin
            // 
            this.tLogin.Location = new System.Drawing.Point(12, 15);
            this.tLogin.Name = "tLogin";
            this.tLogin.Size = new System.Drawing.Size(376, 20);
            this.tLogin.TabIndex = 0;
            this.tLogin.Text = "mintic@lcardona";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(406, 14);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(106, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Chrome";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tEmail
            // 
            this.tEmail.Location = new System.Drawing.Point(12, 41);
            this.tEmail.Name = "tEmail";
            this.tEmail.Size = new System.Drawing.Size(376, 20);
            this.tEmail.TabIndex = 1;
            this.tEmail.Text = "lcardona@mintic.gob.co";
            // 
            // cGroup
            // 
            this.cGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cGroup.FormattingEnabled = true;
            this.cGroup.Items.AddRange(new object[] {
            "Concesionario",
            "MinTIC",
            "ANE"});
            this.cGroup.Location = new System.Drawing.Point(12, 68);
            this.cGroup.Name = "cGroup";
            this.cGroup.Size = new System.Drawing.Size(376, 21);
            this.cGroup.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 102);
            this.Controls.Add(this.cGroup);
            this.Controls.Add(this.tEmail);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SGENavigate";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tLogin;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox tEmail;
        private System.Windows.Forms.ComboBox cGroup;
    }
}

