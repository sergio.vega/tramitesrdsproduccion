﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace SGENavigate
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cGroup.SelectedIndex = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ST_MyEncrypt mEncrypt = new ST_MyEncrypt(tLogin.Text, tEmail.Text, cGroup.SelectedIndex);
            string CadEnc = MyEncrypt(mEncrypt);
            ST_MyEncrypt mDecrypt = MyDecrypt(CadEnc);
            
            Process.Start("chrome.exe", "http://localhost/sgeweb/index.html?ID=" + CadEnc);
            //Process.Start("chrome.exe", "http://172.23.24.76:8015/sgeweb/index.html?ID=" + CadEnc);
        }


        public static string MyEncrypt(ST_MyEncrypt mEncrypt)
        {
            string sText = "";
            int checkSum = 0;
            var Serializer = new JavaScriptSerializer();
            mEncrypt.Date = DateTime.Now.ToString("yyyyMMddHHmmss");

            sText = Serializer.Serialize(mEncrypt);

            if (sText.Length < 248)
            {
                sText = sText.PadRight(248);
            }
            
            for (int i = 0; i < 248; i++)
            {
                checkSum += sText[i];
            }
            sText = "Ð" + checkSum.ToString("D6") + "Ð" + sText;

            byte[] clearBytes = Encoding.UTF8.GetBytes(sText);



            int n = clearBytes.Length;

            byte[] KeyBytes = Encoding.UTF8.GetBytes("CRGID10288018CRGCEL3114409465ABPID75082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034PAOCEL3124896735CRGID10288018CRGCEL3114409465YCID1007275830YCCEL310792247375082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034");

            for (int i = 0; i < n - 2; i += 2)
            {
                clearBytes[i] ^= clearBytes[n - 1];
                clearBytes[i + 1] ^= clearBytes[n - 2];
            }

            for (int i = 0; i < n - 4; i += 2)
            {
                clearBytes[i] ^= clearBytes[n - 3];
                clearBytes[i + 1] ^= clearBytes[n - 4];
            }

            for (int i = 0; i < n; i++)
            {
                clearBytes[i] ^= KeyBytes[i];
            }

            sText = Convert.ToBase64String(clearBytes);

            
            return sText;
        }

        public static ST_MyEncrypt MyDecrypt(string sText)
        {
            var Serializer = new JavaScriptSerializer();
            int checkSum = 0;
            ST_MyEncrypt mResult = new ST_MyEncrypt();

            byte[] clearBytes = Convert.FromBase64String(sText);
            int n = clearBytes.Length;
            if (n != 258)
                return mResult;

            byte[] KeyBytes = Encoding.UTF8.GetBytes("CRGID10288018CRGCEL3114409465ABPID75082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034PAOCEL3124896735CRGID10288018CRGCEL3114409465YCID1007275830YCCEL310792247375082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034");

            for (int i = 0; i < n; i++)
            {
                clearBytes[i] ^= KeyBytes[i];
            }

            for (int i = 0; i < n - 4; i += 2)
            {
                clearBytes[i] ^= clearBytes[n - 3];
                clearBytes[i + 1] ^= clearBytes[n - 4];
            }

            for (int i = 0; i < n - 2; i += 2)
            {
                clearBytes[i] ^= clearBytes[n - 1];
                clearBytes[i + 1] ^= clearBytes[n - 2];
            }

            sText = Encoding.UTF8.GetString(clearBytes);

            if (sText.Substring(0, 1) != "Ð")
                return mResult;

            for (int i = 0; i < 248; i++)
            {
                checkSum += sText[i+8];
            }

            int checkSumOK = Convert.ToInt32(sText.Substring(1, 6));

            if (checkSumOK != checkSum)
                return mResult;

            string mDecrypt = sText.Substring(8, 248).Trim();
            mResult = Serializer.Deserialize<ST_MyEncrypt>(mDecrypt);
            
            return mResult;
        }

        public class ST_MyEncrypt
        {
            public string Login { get; set; }
            public string Email { get; set; }
            public int Group { get; set; } //0:Concesionario, 1:MinTIC, 2:ANE
            public string Date { get; set; }
            public ST_MyEncrypt()
            {
                this.Login = "";
                this.Email = "";
                this.Group = 0;
                this.Date = DateTime.Now.AddYears(-10).ToString("yyyyMMddHHmmss");
            }
            public ST_MyEncrypt(string Login, string Email, int Group)
            {
                this.Login = Login;
                this.Email = Email;
                this.Group = Group;
                this.Date = DateTime.Now.AddYears(-10).ToString("yyyyMMddHHmmss");
            }
            public DateTime getExpireDate(){
                return DateTime.ParseExact(this.Date, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
            }
        }

    }
}
