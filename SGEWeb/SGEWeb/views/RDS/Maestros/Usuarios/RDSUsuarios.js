﻿SGEWeb.RDSUsuarios = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var title = JSON.parse(params.settings.substring(5)).title;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var Cantidades = ko.observable(0);
    var MyPopUp = {
        Show: ko.observable(false),
        Title: ko.observable(''),
        USER: ko.observable({}),
        CRUDType: ko.observable(3),
        USR_ROLES: ko.observableArray([]),
        ROLE1_DISABLED: ko.observable(false),
        ROLE2_DISABLED: ko.observable(false),
        ROLE4_DISABLED: ko.observable(false),
        ROLE8_DISABLED: ko.observable(false),
        ROLE16_DISABLED: ko.observable(false),
        ROLE32_DISABLED: ko.observable(false),
        ROLE128_DISABLED: ko.observable(false),
        USR_ASSIGNABLE: ko.observableArray([]),
        ASSIGNABLE1_DISABLED: ko.observable(false),
        ASSIGNABLE2_DISABLED: ko.observable(false),
        ASSIGNABLE4_DISABLED: ko.observable(false),
        ASSIGNABLE8_DISABLED: ko.observable(false),
        ASSIGNABLE16_DISABLED: ko.observable(false),
        ASSIGNABLE32_DISABLED: ko.observable(false),
        ASSIGNABLE128_DISABLED: ko.observable(false)
    };

    var MyPopUpSignature = {
        Show: ko.observable(false),
        USER: ko.observable({}),
        FileSignature: {
            Name: ko.observable('Seleccionar firma digital'),
            Data: undefined,
            Cargado: ko.observable(false)
        }
    };
    
    var MyPopUpPassword = {
        Show: ko.observable(false),
        USER: ko.observable({}),
        USR_PASSWORD1: ko.observable(),
        USR_PASSWORD2: ko.observable(),
        DISABLE: ko.observable()
    };

    
    function GetUsuarios(USR_ID_FILTER, USR_GROUP_FILTER, USR_ROLE_FILTER, USR_STATUS_FILTER) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetUsuarios",
            data: JSON.stringify({
                USR_ID_FILTER: USR_ID_FILTER,
                USR_GROUP_FILTER: USR_GROUP_FILTER,
                USR_ROLE_FILTER: USR_ROLE_FILTER,
                USR_STATUS_FILTER: USR_STATUS_FILTER
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetUsuariosResult;

                MyDataSource(result.filter(function (Item) {
                    return ((ROLE == enumROLES.Revisor ? enumROLES.Funcionario : ROLE) > Item.USR_ROLE) || (Item.USR_ID == SGEWeb.app.IDUserWeb) || (ROLE == enumROLES.Administrador);
                }));

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDUsuario(User) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDUsuario",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                User: User
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDUsuarioResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUp.Show(false);
                    Refrescar();
                }
                GuardarDisabled(false);
                
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDUsuarioSignature(User, FileSignature) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDUsuarioSignature",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                User: User,
                FileSignature: FileSignature
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDUsuarioSignatureResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUpSignature.Show(false);
                    SGEWeb.app.User.USR_HAS_SIGN = 1;
                    Refrescar();
                }
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }
    
    function ResetUsuarioPassword(User) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ResetUsuarioPassword",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                User: User
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.ResetUsuarioPasswordResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUp.Show(false);
                    Refrescar();
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CambioUsuarioPassword(User, USR_NEW_PASSWORD) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CambioUsuarioPassword",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                User: User,
                USR_NEW_PASSWORD: USR_NEW_PASSWORD
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CambioUsuarioPasswordResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUpPassword.Show(false);
                }
                GuardarDisabled(false);
                MyPopUpPassword.DISABLE(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
                MyPopUpPassword.DISABLE(false);
            }
        });
    }

    function onHeaderTemplate(container, options) {
        switch (options.column.name) {
            case 'USR_ASSIGNABLE1':
                $('<i/>').addClass('ta ta-funcionario ta-lg')
                .prop('title', 'Asignable como funcionario')
                .css('cursor', 'pointer')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'USR_ASSIGNABLE2':
                $('<i/>').addClass('ta ta-revisor ta-lg')
                .prop('title', 'Asignable como revisor')
                .css('cursor', 'pointer')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'USR_ASSIGNABLE4':
                $('<i/>').addClass('ta ta-coordinador ta-lg')
                .prop('title', 'Asignable como coordinador')
                .css('cursor', 'pointer')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'USR_ASSIGNABLE8':
                $('<i/>').addClass('ta ta-subdirector ta-lg')
                .prop('title', 'Asignable como subdirector')
                .css('cursor', 'pointer')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, options) {
        switch (options.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar usuario')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Firma':
                    $('<i/>').addClass('ta ta-signature1 ta-lg')
                    .prop('title', options.data.USR_HAS_SIGN == 1 ? 'Firma digital cargada' : 'Sin firma digital')
                    .css('color', options.data.USR_HAS_SIGN == 1 ? 'black' : 'lightgray')
                    .css('cursor', 'default')
                    .appendTo(container);
                
                break;
            case 'USR_ASSIGNABLE1':
            case 'USR_ASSIGNABLE2':
            case 'USR_ASSIGNABLE4':
            case 'USR_ASSIGNABLE8':
            case 'USR_ASSIGNABLE16':
            case 'USR_ASSIGNABLE32':
            case 'USR_ASSIGNABLE128':
                if(options.value == 0) break;
                $('<i/>').addClass(options.column.icon)
                .prop('title', options.value == 1 ? 'Asignable' : 'No asignable')
                .css('color', options.value == 1 ? 'black' : 'lightgray')
                .css('cursor', 'default')
                .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSUsuarios') {
                switch (e.column.name) {
                    case 'Editar':
                        MyPopUp.USER(ko.mapping.fromJS(ko.toJS(e.data)));
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title('Editar usuario');
                        MyPopUp.Show(true);

                        MyPopUp.USR_ROLES([]);
                        if (e.data.USR_ROLE & enumROLES.Funcionario) MyPopUp.USR_ROLES().push(enumROLES.Funcionario);
                        if (e.data.USR_ROLE & enumROLES.Revisor) MyPopUp.USR_ROLES().push(enumROLES.Revisor);
                        if (e.data.USR_ROLE & enumROLES.Coordinador) MyPopUp.USR_ROLES().push(enumROLES.Coordinador);
                        if (e.data.USR_ROLE & enumROLES.Subdirector) MyPopUp.USR_ROLES().push(enumROLES.Subdirector);
                        if (e.data.USR_ROLE & enumROLES.Asesor) MyPopUp.USR_ROLES().push(enumROLES.Asesor);
                        if (e.data.USR_ROLE & enumROLES.Director) MyPopUp.USR_ROLES().push(enumROLES.Director);
                        if (e.data.USR_ROLE & enumROLES.Administrador) MyPopUp.USR_ROLES().push(enumROLES.Administrador);
                        MyPopUp.USR_ASSIGNABLE([]);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Funcionario) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Funcionario);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Revisor) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Revisor);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Coordinador) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Coordinador);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Subdirector) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Subdirector);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Asesor) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Asesor);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Director) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Director);
                        if (e.data.USR_ASSIGNABLE & enumROLES.Administrador) MyPopUp.USR_ASSIGNABLE().push(enumROLES.Administrador);

                        $("#ButtonGroupRoles").dxButtonGroup('instance').option('selectedItemKeys', MyPopUp.USR_ROLES());
                        $("#ButtonGroupAsignable").dxButtonGroup('instance').option('selectedItemKeys', MyPopUp.USR_ASSIGNABLE());

                        MyPopUp.ASSIGNABLE1_DISABLED(true);
                        MyPopUp.ASSIGNABLE2_DISABLED(true);
                        MyPopUp.ASSIGNABLE4_DISABLED(true);
                        MyPopUp.ASSIGNABLE8_DISABLED(true);
                        MyPopUp.ASSIGNABLE16_DISABLED(true);
                        MyPopUp.ASSIGNABLE32_DISABLED(true);
                        MyPopUp.ASSIGNABLE128_DISABLED(true);

                        MyPopUp.ROLE1_DISABLED(true);
                        MyPopUp.ROLE2_DISABLED(true);
                        MyPopUp.ROLE4_DISABLED(true);
                        MyPopUp.ROLE8_DISABLED(true);
                        MyPopUp.ROLE16_DISABLED(true);
                        MyPopUp.ROLE32_DISABLED(true);
                        MyPopUp.ROLE128_DISABLED(true);

                        if (e.data.USR_ID == SGEWeb.app.IDUserWeb && ROLE != enumROLES.Administrador)
                            break;

                        if ((e.data.USR_ROLE & enumROLES.Funcionario) != 0) MyPopUp.ASSIGNABLE1_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Revisor) != 0) MyPopUp.ASSIGNABLE2_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Coordinador) != 0) MyPopUp.ASSIGNABLE4_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Subdirector) != 0) MyPopUp.ASSIGNABLE8_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Asesor) != 0) MyPopUp.ASSIGNABLE16_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Director) != 0) MyPopUp.ASSIGNABLE32_DISABLED(false);
                        if ((e.data.USR_ROLE & enumROLES.Administrador) != 0) MyPopUp.ASSIGNABLE128_DISABLED(false);


                        switch (ROLE) {
                            case enumROLES.Funcionario:
                                break;
                            case enumROLES.Revisor:
                                break;
                            case enumROLES.Coordinador:
                                MyPopUp.ROLE1_DISABLED(false);
                                MyPopUp.ROLE2_DISABLED(false);
                                break;
                            case enumROLES.Subdirector:
                                MyPopUp.ROLE1_DISABLED(false);
                                MyPopUp.ROLE2_DISABLED(false);
                                MyPopUp.ROLE4_DISABLED(false);
                                break;
                            case enumROLES.Asesor:
                                MyPopUp.ROLE1_DISABLED(false);
                                MyPopUp.ROLE2_DISABLED(false);
                                MyPopUp.ROLE4_DISABLED(false);
                                MyPopUp.ROLE8_DISABLED(false);
                                break;
                            case enumROLES.Director:
                                MyPopUp.ROLE1_DISABLED(false);
                                MyPopUp.ROLE2_DISABLED(false);
                                MyPopUp.ROLE4_DISABLED(false);
                                MyPopUp.ROLE8_DISABLED(false);
                                MyPopUp.ROLE16_DISABLED(false);
                                break;

                            case enumROLES.Administrador:
                                MyPopUp.ROLE1_DISABLED(false);
                                MyPopUp.ROLE2_DISABLED(false);
                                MyPopUp.ROLE4_DISABLED(false);
                                MyPopUp.ROLE8_DISABLED(false);
                                MyPopUp.ROLE16_DISABLED(false);
                                MyPopUp.ROLE32_DISABLED(false);
                                MyPopUp.ROLE128_DISABLED(false);
                                break;
                        }
                        break;

                }

            } 
        }
    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function Excel() {
        var grid = $("#GridRDSUsuarios").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function NuevoUsuario() {
        MyPopUp.USER(ko.mapping.fromJS(ko.toJS({ USR_ID: -1, USR_LOGIN: '', USR_NAME: '', USR_EMAIL: '', USR_GROUP: GROUP, USR_ROLE: 0, USR_ASSIGNABLE: 0, USR_STATUS: 1 })));
        MyPopUp.Title('Crear usuario');
        MyPopUp.CRUDType(1);
        MyPopUp.Show(true);

        MyPopUp.USR_ROLES([]);
        MyPopUp.USR_ASSIGNABLE([]);

        $("#ButtonGroupRoles").dxButtonGroup('instance').option('selectedItemKeys', MyPopUp.USR_ROLES());
        $("#ButtonGroupAsignable").dxButtonGroup('instance').option('selectedItemKeys', MyPopUp.USR_ASSIGNABLE());

        MyPopUp.ASSIGNABLE1_DISABLED(true);
        MyPopUp.ASSIGNABLE2_DISABLED(true);
        MyPopUp.ASSIGNABLE4_DISABLED(true);
        MyPopUp.ASSIGNABLE8_DISABLED(true);
        MyPopUp.ASSIGNABLE16_DISABLED(true);
        MyPopUp.ASSIGNABLE32_DISABLED(true);
        MyPopUp.ASSIGNABLE128_DISABLED(true);

        MyPopUp.ROLE1_DISABLED(true);
        MyPopUp.ROLE2_DISABLED(true);
        MyPopUp.ROLE4_DISABLED(true);
        MyPopUp.ROLE8_DISABLED(true);
        MyPopUp.ROLE16_DISABLED(true);
        MyPopUp.ROLE32_DISABLED(true);
        MyPopUp.ROLE128_DISABLED(true);

        switch (ROLE) {
            case enumROLES.Funcionario:
                break;
            case enumROLES.Revisor:
                break;
            case enumROLES.Coordinador:
                MyPopUp.ROLE1_DISABLED(false);
                MyPopUp.ROLE2_DISABLED(false);
                break;
            case enumROLES.Subdirector:
                MyPopUp.ROLE1_DISABLED(false);
                MyPopUp.ROLE2_DISABLED(false);
                MyPopUp.ROLE4_DISABLED(false);
                break;
            case enumROLES.Asesor:
                break;
            case enumROLES.Director:
                MyPopUp.ROLE1_DISABLED(false);
                MyPopUp.ROLE2_DISABLED(false);
                MyPopUp.ROLE4_DISABLED(false);
                MyPopUp.ROLE8_DISABLED(false);
                MyPopUp.ROLE16_DISABLED(false);
                break;
            case enumROLES.Administrador:
                MyPopUp.ROLE1_DISABLED(false);
                MyPopUp.ROLE2_DISABLED(false);
                MyPopUp.ROLE4_DISABLED(false);
                MyPopUp.ROLE8_DISABLED(false);
                MyPopUp.ROLE16_DISABLED(false);
                MyPopUp.ROLE32_DISABLED(false);
                MyPopUp.ROLE128_DISABLED(false);

                break;
        }
    }

    function OnGuardar() {
        if (IsNullOrEmpty(MyPopUp.USER().USR_LOGIN()) || MyPopUp.USER().USR_LOGIN().length <= 4) {
            DevExpress.ui.notify('El login de ser mayor a 4 caracteres.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.USER().USR_NAME()) || MyPopUp.USER().USR_NAME().length <= 6) {
            DevExpress.ui.notify('El nombre de ser mayor a 6 caracteres.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.USER().USR_EMAIL())) {
            DevExpress.ui.notify('El correo es requerido.', "warning", 3000);
            return;
        }
        if (!validateEmail(MyPopUp.USER().USR_EMAIL())) {
            DevExpress.ui.notify('El correo es inválido.', "warning", 3000);
            return;
        }
        if (MyPopUp.USR_ROLES().length == 0) {
            DevExpress.ui.notify('Por lo menos un Role debe estar seleccionado.', "warning", 3000);
            return;
        }


        var User = ko.toJS(MyPopUp.USER());

        User.USR_ROLE = 0;
        MyPopUp.USR_ROLES().forEach(function (entry) {
            User.USR_ROLE += entry;
        });

        User.USR_ASSIGNABLE = 0;
        MyPopUp.USR_ASSIGNABLE().forEach(function (entry) {
            User.USR_ASSIGNABLE += entry;
        });

        if ((User.USR_ROLE & enumROLES.Administrador) == enumROLES.Administrador) {
            User.USR_GROUP = 3;
        }

        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);


        CRUDUsuario(User);
        
    }

    function OnGuardarSignature() {
        var User = ko.toJS(MyPopUpSignature.USER());

        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);
        CRUDUsuarioSignature(User, MyPopUpSignature.FileSignature.Data);
    }

    function CargarFile(data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (e.target.id == 'FileSignature') {
                if (file.type.indexOf("image/png") != 0) {
                    DevExpress.ui.notify('Formato de archivo inválido. Formato válido (png).', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }

                if (file.name.length > 50) {
                    DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + 50 + ' caracteres', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }

                if (file.size > (200000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + 200 + 'KB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }

                MyPopUpSignature.FileSignature.Name("Firma digital cargada");
            }

            var fileReader = new FileReader();
            fileReader.onload = function () {
                if (e.target.id == 'FileSignature') {
                    MyPopUpSignature.FileSignature.Data = fileReader.result;
                    MyPopUpSignature.FileSignature.Cargado(true);
                }
            };

            fileReader.readAsDataURL(file);

            ElementFile.value = null;
        }
    }

    function onRolesClick(e) {


        if (ArrayExists(MyPopUp.USR_ROLES(), e.itemData.ID)) {

            switch (e.itemData.ID) {
                case enumROLES.Funcionario:
                    MyPopUp.ASSIGNABLE1_DISABLED(false);
                    break;
                case enumROLES.Revisor:
                    MyPopUp.ASSIGNABLE2_DISABLED(false);
                    break;
                case enumROLES.Coordinador:
                    MyPopUp.ASSIGNABLE4_DISABLED(false);
                    break;
                case enumROLES.Subdirector:
                    MyPopUp.ASSIGNABLE8_DISABLED(false);
                    break;
                case enumROLES.Asesor:
                    MyPopUp.ASSIGNABLE16_DISABLED(false);
                    break;
                case enumROLES.Director:
                    MyPopUp.ASSIGNABLE32_DISABLED(false);
                    break;
                case enumROLES.Administrador:
                    MyPopUp.ASSIGNABLE128_DISABLED(false);
                    break;
            }

        } else {

            switch (e.itemData.ID) {
                case enumROLES.Funcionario:
                    MyPopUp.ASSIGNABLE1_DISABLED(true);
                    break;
                case enumROLES.Revisor:
                    MyPopUp.ASSIGNABLE2_DISABLED(true);
                    break;
                case enumROLES.Coordinador:
                    MyPopUp.ASSIGNABLE4_DISABLED(true);
                    break;
                case enumROLES.Subdirector:
                    MyPopUp.ASSIGNABLE8_DISABLED(true);
                    break;
                case enumROLES.Asesor:
                    MyPopUp.ASSIGNABLE16_DISABLED(true);
                    break;
                case enumROLES.Director:
                    MyPopUp.ASSIGNABLE32_DISABLED(true);
                    break;
                case enumROLES.Administrador:
                    MyPopUp.ASSIGNABLE128_DISABLED(true);
                    break;
            }

            MyPopUp.USR_ASSIGNABLE(MyPopUp.USR_ASSIGNABLE().filter(function (Item) {
                return Item != e.itemData.ID;
            }));
            $("#ButtonGroupAsignable").dxButtonGroup('instance').option('selectedItemKeys', MyPopUp.USR_ASSIGNABLE());

        }
       
    }

    function FirmaDigital() {
        
        MyPopUpSignature.FileSignature.Name('Seleccionar firma digital');
        MyPopUpSignature.FileSignature.Cargado(false);
        MyPopUpSignature.FileSignature.Data = undefined;
        MyPopUpSignature.USER(ko.mapping.fromJS(ko.toJS(ListObjectFindGetElement(MyDataSource(), 'USR_ID', SGEWeb.app.IDUserWeb))));
        MyPopUpSignature.Show(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetUsuarios((ROLE == 1 ? SGEWeb.app.IDUserWeb : -1), GROUP, 0xFF, 3);
    }
    
    function OnResetPassword() {

        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea reiniciar la contraseña del usuario seleccionado. Se enviará un correo con la nueva contraseña?'), SGEWeb.app.Name);
        result.done(function (dialogResult) {
            if (dialogResult) {
                var User = ko.toJS(MyPopUp.USER());
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingVisible(true);

                ResetUsuarioPassword(User);
            }
        });
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 13) {
            $('#btnPassword').dxButton('instance').focus();
            OnCambiarPassword();
        }
        return true;
    }

    function CambiarPassword() {
        MyPopUpPassword.USER(ko.mapping.fromJS(ko.toJS(ListObjectFindGetElement(MyDataSource(), 'USR_ID', SGEWeb.app.IDUserWeb))));
        MyPopUpPassword.USR_PASSWORD1('');
        MyPopUpPassword.USR_PASSWORD2('');
        MyPopUpPassword.DISABLE(false);
        MyPopUpPassword.Show(true);
    }

    function OnCambiarPassword(e) {
        MyPopUpPassword.DISABLE(true);
        
        if (IsNullOrEmpty(MyPopUpPassword.USR_PASSWORD1()) || MyPopUpPassword.USR_PASSWORD1().length < 6) {
            DevExpress.ui.notify("La contraseña es requerida y debe tener mínimo 6 caracteres.", "warning", 2000);
            MyPopUpPassword.DISABLE(false);
            return;
        }
        if (IsNullOrEmpty(MyPopUpPassword.USR_PASSWORD2())) {
            DevExpress.ui.notify("La confirmación de la contraseña es requerida", "warning", 2000);
            MyPopUpPassword.DISABLE(false);
            return;
        }

        if (MyPopUpPassword.USR_PASSWORD1() != MyPopUpPassword.USR_PASSWORD2()) {
            DevExpress.ui.notify("La contraseña no coincide", "warning", 2000);
            MyPopUpPassword.DISABLE(false);
            return;
        }

        var User = ko.toJS(MyPopUpPassword.USER());
        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        CambioUsuarioPassword(User, MyPopUpPassword.USR_PASSWORD1());

    }

    Refrescar();

    var viewModel = {
        title: title,
        CellClick: CellClick,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        onContentReady: onContentReady,
        Excel: Excel,
        Refrescar: Refrescar,
        NuevoUsuario: NuevoUsuario,
        MyDataSource: MyDataSource,
        loadingVisible: loadingVisible,
        GuardarDisabled: GuardarDisabled,
        Cantidades: Cantidades,
        OnGuardar: OnGuardar,
        OnGuardarSignature:OnGuardarSignature,
        MyPopUp: MyPopUp,
        MyPopUpSignature:MyPopUpSignature,
        onRolesClick: onRolesClick,
        CargarFile: CargarFile,
        FirmaDigital: FirmaDigital,
        GROUP: GROUP,
        ROLE: ROLE,
        OnResetPassword: OnResetPassword,
        onPwdKeyDown: onPwdKeyDown,
        MyPopUpPassword: MyPopUpPassword,
        CambiarPassword:CambiarPassword,
        OnCambiarPassword: OnCambiarPassword,
    };

    return viewModel;
};