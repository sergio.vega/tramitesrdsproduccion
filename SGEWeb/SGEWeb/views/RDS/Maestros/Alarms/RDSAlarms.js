﻿SGEWeb.RDSAlarms = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var GuardarDisabled = ko.observable(false);

    var MyPopUpAlarm = {
        Show: ko.observable(false),
        ALARM_STEP: ko.observable(-1),
        ALARM_DESCRIPTION: ko.observable(),
        ALARM_PRE_EXPIRE: ko.observable(),
        ALARM_EXPIRE: ko.observable(),
    };

    function GetAlarms() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetAlarms",
            data: JSON.stringify({

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetAlarmsResult;

                MyDataSource(result);
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDAlarms(Alarm) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDAlarms",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Alarm: Alarm
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDAlarmsResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                    MyPopUpAlarm.Show(false);
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    var onItemClick = function (e) {
        MyPopUpAlarm.ALARM_STEP(e.itemData.ALARM_STEP);
        MyPopUpAlarm.ALARM_DESCRIPTION (e.itemData.ALARM_DESCRIPTION);
        MyPopUpAlarm.ALARM_PRE_EXPIRE (e.itemData.ALARM_PRE_EXPIRE);
        MyPopUpAlarm.ALARM_EXPIRE (e.itemData.ALARM_EXPIRE);
        MyPopUpAlarm.Show(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetAlarms();
    }

    function OnGuardarAlarma() {
        if (MyPopUpAlarm.ALARM_PRE_EXPIRE() == null) {
            DevExpress.ui.notify('El valor por vencer es requerido', "warning", 3000);
            return;
        }
        if (MyPopUpAlarm.ALARM_EXPIRE() == null) {
            DevExpress.ui.notify('El valor vencido es requerido', "warning", 3000);
            return;
        }


        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var Alarm = { 'ALARM_STEP': MyPopUpAlarm.ALARM_STEP(), 'ALARM_DESCRIPTION': MyPopUpAlarm.ALARM_DESCRIPTION(), 'ALARM_PRE_EXPIRE': MyPopUpAlarm.ALARM_PRE_EXPIRE(), 'ALARM_EXPIRE': MyPopUpAlarm.ALARM_EXPIRE() };

        CRUDAlarms(Alarm);

    }

    Refrescar();

    var viewModel = {
        MyDataSource: MyDataSource,
        Refrescar: Refrescar,
        loadingVisible: loadingVisible,
        GuardarDisabled: GuardarDisabled,
        onItemClick: onItemClick,
        MyPopUpAlarm: MyPopUpAlarm,
        OnGuardarAlarma: OnGuardarAlarma
    };

    return viewModel;
};