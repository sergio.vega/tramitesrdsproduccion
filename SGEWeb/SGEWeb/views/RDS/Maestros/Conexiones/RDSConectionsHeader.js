﻿SGEWeb.RDSConectionsHeader = function (params) {
    "use strict";

    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);
    var GuardarDisabled = ko.observable(false);
    var title = ko.observable('RDS Conexiones');

    var RangeArr = [{ ID: 0, text: 'Hoy'}, 
                    { ID: 1, text: 'Ayer' },
                    { ID: 2, text: 'Semana actual' },
                    { ID: 3, text: 'Semana anterior' },
                    { ID: 4, text: 'Este mes', selected: true },
                    { ID: 5, text: 'Mes pasado' },
                    { ID: 6, text: 'Trimestre actual' },
                    { ID: 7, text: 'Trimestre pasado' },
                    { ID: 8, text: 'Semestre actual' },
                    { ID: 9, text: 'Semestre pasado' },
                    { ID: 10, text: 'Año actual' },
                    { ID: 11, text: 'Año pasado' },
                    { ID: 12, text: 'Rango de fechas' }
    ];
    var ContextMenuRangoVisible = ko.observable(false);
    var ShowFechas = ko.observable(false);
    var NameRF = 'Este mes';
    var Date1 = Date.today().add({ months: -1 }).moveToFirstDayOfMonth().toString('yyyyMMdd');
    var Date2 = Date.today().add({ months: -1 }).moveToLastDayOfMonth().toString('yyyyMMdd');
    var selectedRangeID = 4;
    var selectedDateINI = ko.observable(undefined);
    var selectedDateEND = ko.observable(undefined);


    var MyPopUp = {
        Show: ko.observable(false),
        Title: ko.observable(''),
        ID: ko.observable(''),
        TYPE: ko.observable(''),
        GROUP: ko.observable(''),
        TEXT: ko.observable(''),
        CRUDType: ko.observable(0)
    };


    function GetConectionsHeader(DateIni, DateEnd) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConectionsHeader",
            data: JSON.stringify({
                DateIni: DateIni,
                DateEnd: DateEnd
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConectionsHeaderResult;

                MyDataSource(result);

                title('RDS Conexiones ' + NameRF);


                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onCellTemplate(container, e) {

        switch (e.column.name) {
            case 'Detalle':
                $('<i/>').addClass('ta ta-plug ta-lg')
                .prop('title', 'Ver detalle de conexiones')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'USR_ROLE':
                $('<i/>').addClass(ListObjectGetAttribute(SGEWeb.app.ROLE, 'value', GetMaxRole(e.data.USR_ROLE), 'Icon') + ' ta-lg')
                .prop('title', GetRoles(e.data.USR_ROLE))
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'USR_OS':
                $('<i/>').addClass(ListObjectGetAttribute(SGEWeb.app.OS, 'value', e.data.USR_OS, 'Icon') + ' ta-lg')
                .prop('title', e.data.USR_OS)
                .css('cursor', 'default')
                .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span>')
                .appendTo(container);
                container.css('text-align', 'center')
                break;
            case 'USR_BROWSER':
                $('<i/>').addClass(ListObjectGetAttribute(SGEWeb.app.BROWSER, 'value', e.data.USR_BROWSER, 'Icon') + ' ta-lg')
                .prop('title', e.data.USR_BROWSER)
                .css('cursor', 'default')
                .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span>')
                .appendTo(container);
                container.css('text-align', 'center')
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSConectionsHeader') {
                switch (e.column.name) {
                    case 'Detalle':
                        SGEWeb.app.navigate({ view: "RDSConectionsDetails", id: -1, settings: { USR_ID: e.row.data.USR_ID, Date1: Date1, Date2: Date2 } });
                        break;
                }
            }
        }
    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function Excel() {
        var grid = $("#GridRDSConectionsHeader").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetConectionsHeader(Date1, Date2);
    }

    function ContextMenuRangoClick(e) {

        selectedRangeID = e.itemData.ID;

        switch (e.itemData.ID) {
            case 0: //Hoy
                selectedDateINI(new Date());
                selectedDateEND(new Date());
                break;
            case 1: //Ayer
                selectedDateINI(Date.today().add({ days: -1 }));
                selectedDateEND(Date.today().add({ days: -1 }));
                break;
            case 2: //Esta Semana
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: +1 }));
                selectedDateEND(Date.today());
                break;
            case 3: //Semana Pasada
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: -6 }));
                selectedDateEND(Date.today().moveToDayOfWeek(0, -1));
                break;
            case 4: //Este mes
                selectedDateINI(Date.today().moveToFirstDayOfMonth());
                selectedDateEND(Date.today());
                break;
            case 5: //Mes pasado
                selectedDateINI(Date.today().add({ months: -1 }).moveToFirstDayOfMonth());
                selectedDateEND(Date.today().add({ months: -1 }).moveToLastDayOfMonth());
                break;
            case 6: //Este trimestre
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                selectedDateINI(new Date((new Date).getFullYear(), 3 * quarter - 3, 1));
                selectedDateEND(Date.today());
                break;
            case 7: //Trimestre pasado
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                var year = quarter == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                quarter = quarter == 1 ? 4 : quarter - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 3 * quarter - 3, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 3 * quarter - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 8: //Este Semestre
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                selectedDateINI(new Date((new Date).getFullYear(), 6 * Semestre - 6, 1));
                selectedDateEND(Date.today());
                break;
            case 9: //Semestre pasado
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                var year = Semestre == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                Semestre = Semestre == 1 ? 2 : Semestre - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 6 * Semestre - 6, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 6 * Semestre - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 10: //Este Año
                selectedDateINI(new Date((new Date).getFullYear(), 0, 1));
                selectedDateEND(new Date());
                break;
            case 11: //Año pasado
                selectedDateINI(new Date((new Date).getFullYear() - 1, 0, 1));
                selectedDateEND(new Date((new Date).getFullYear() - 1, 11, 31));
                break;
            case 12: //Rango de fechas
                ShowFechas(true);
                return;
                break;
        }

        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = e.itemData.text;

        Refrescar();
    }

    function CustomDateClick() {
        if (selectedDateINI() > selectedDateEND()) {
            DevExpress.ui.notify('La fecha inicial debe ser menor o igual que la fecha final', "warning", 3000);
            return;
        }

        ShowFechas(false);
        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = selectedDateINI().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateINI().getMonth()]) + '-' + selectedDateEND().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateEND().getMonth()]);
        Refrescar();
    }

    ContextMenuRangoClick({ itemData: RangeArr[ListObjectFindIndex(RangeArr, 'ID', selectedRangeID)] });

    var viewModel = {
        title: title,
        Refrescar: Refrescar,
        MyDataSource: MyDataSource,
        GuardarDisabled: GuardarDisabled,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        onContentReady: onContentReady,
        Excel: Excel,
        MyPopUp: MyPopUp,
        onCellTemplate:onCellTemplate,
        ContextMenuRangoVisible: ContextMenuRangoVisible,
        ContextMenuRangoClick: ContextMenuRangoClick,
        RangeArr: RangeArr,
        selectedDateINI: selectedDateINI,
        selectedDateEND: selectedDateEND,
        ShowFechas: ShowFechas,
        CustomDateClick: CustomDateClick,
        CellClick: CellClick
    };

    return viewModel;
};