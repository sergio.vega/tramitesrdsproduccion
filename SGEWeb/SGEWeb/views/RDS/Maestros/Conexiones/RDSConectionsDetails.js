﻿SGEWeb.RDSConectionsDetails = function (params) {
    "use strict";

    var USR_ID = JSON.parse(params.settings.substring(5)).USR_ID;
    var Date1 = JSON.parse(params.settings.substring(5)).Date1;
    var Date2 = JSON.parse(params.settings.substring(5)).Date2;

    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);

    var MyPopUp = {
        Show: ko.observable(false),
        Title: ko.observable(''),
        ID: ko.observable(''),
        TYPE: ko.observable(''),
        GROUP: ko.observable(''),
        TEXT: ko.observable(''),
        CRUDType: ko.observable(0)
    };


    function GetConectionsDetails(USR_ID, DateIni, DateEnd) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConectionsDetails",
            data: JSON.stringify({
                USR_ID:USR_ID,
                DateIni: DateIni,
                DateEnd: DateEnd
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConectionsDetailsResult;

                MyDataSource(result);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onCellTemplate(container, e) {

        switch (e.column.name) {
            case 'TOK_OS':
                $('<i/>').addClass(ListObjectGetAttribute(SGEWeb.app.OS, 'value', e.data.TOK_OS, 'Icon') + ' ta-lg')
                .prop('title', e.data.TOK_OS)
                .css('cursor', 'default')
                .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span>')
                .appendTo(container);
                container.css('text-align', 'center')
                break;
            case 'TOK_BROWSER':
                $('<i/>').addClass(ListObjectGetAttribute(SGEWeb.app.BROWSER, 'value', e.data.TOK_BROWSER, 'Icon') + ' ta-lg')
                .prop('title', e.data.TOK_BROWSER)
                .css('cursor', 'default')
                .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span>')
                .appendTo(container);
                container.css('text-align', 'center')
                break;
        }
    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function Excel() {
        var grid = $("#GridRDSConectionsDetails").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetConectionsDetails(USR_ID, Date1, Date2);
    }


    Refrescar();

    var viewModel = {
        Refrescar: Refrescar,
        MyDataSource: MyDataSource,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        onContentReady: onContentReady,
        Excel: Excel,
        onCellTemplate: onCellTemplate,
    };

    return viewModel;
};