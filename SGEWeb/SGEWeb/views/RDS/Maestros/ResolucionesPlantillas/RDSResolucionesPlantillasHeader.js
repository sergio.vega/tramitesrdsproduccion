﻿SGEWeb.RDSResolucionesPlantillasHeader = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);
    var GuardarDisabled = ko.observable(false);


    var MyPopUp = {
        ResPlt: ko.observable({}),
        SOL_TYPES : ko.observableArray([]),
        Show: ko.observable(false),
        Title: ko.observable(''),
        CRUDType: ko.observable(0)
    };


    function GetResolucionesPlantillas(SOL_TYPE_ID_FILTER, RES_PLT_STATUS_FILTER) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetResolucionesPlantillas",
            data: JSON.stringify({
                SOL_TYPE_ID_FILTER: SOL_TYPE_ID_FILTER,
                RES_PLT_STATUS_FILTER: RES_PLT_STATUS_FILTER
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                MyDataSource(msg.GetResolucionesPlantillasResult);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetResolucionesCombos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetResolucionesCombos",
            data: JSON.stringify({

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                SGEWeb.app.CombosResolucion = msg.GetResolucionesCombosResult;

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDResolucionesPlantillas(ResPlt) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDResolucionesPlantillas",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ResPlt: ResPlt
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDResolucionesPlantillasResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUp.Show(false);
                    Refrescar();
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                .prop('title', 'Eliminar')
                .css('cursor', 'default')
                .appendTo(container);
                break;
            case 'View':
                $('<i/>').addClass('ta ta-eye ta-lg')
                .prop('title', 'Ver plantilla')
                .css('cursor', 'default')
                .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                .prop('title', 'Eliminar')
                .css('cursor', 'pointer')
                .css('color', 'black')
                .appendTo(container);
                break;
            case 'View':
                $('<i/>').addClass('ta ta-eye ta-lg')
                .prop('title', 'Ver plantilla')
                .css('cursor', 'pointer')
                .css('color', 'black')
                .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSResolucionesPlantillas') {
                switch (e.column.name) {
                    case 'Editar':
                        MyPopUp.SOL_TYPES(SGEWeb.app.SOL_TYPES.filter(function (Item) {
                            return Item.SOL_TYPE_CLASS == enumSOL_TYPES_CLASS.AdministrativoTecnico;
                        }));

                        MyPopUp.ResPlt(ko.mapping.fromJS(ko.toJS(e.data)));
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title('Editar plantilla de resolución');
                        MyPopUp.Show(true);
                        break;

                    case 'View':
                        SGEWeb.app.PlantillaResolucion = JSON.stringify(e.data);
                        SGEWeb.app.navigate({ view: "RDSResolucionesPlantillasDetail", id: -1, settings: { title: 'ABC' } });
                        break;

                    case 'Eliminar':
                        if (e.data.TXT_SYSTEM) {
                            e.event.stopPropagation();
                            return;
                        }

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea eliminar la plantilla de resolución, podría estar siendo usada?"), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                SGEWeb.app.DisabledToolBar(true);
                                GuardarDisabled(true);
                                loadingVisible(true);
                                
                                var ResPlt = ko.toJS(e.data);
                                ResPlt.RES_PLT_CRUD = 4;
                                CRUDResolucionesPlantillas(ResPlt);
                            }
                        });
                        break;
                }
            }
        }
    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function Excel() {
        var grid = $("#GridRDSResolucionesPlantillas").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetResolucionesPlantillas(-1, -1);
    }

    function OnGuardar() {

        if (IsNullOrEmpty(MyPopUp.ResPlt().RES_PLT_NAME())) {
            DevExpress.ui.notify('El nombre de la plantilla es requerido.', "warning", 3000);
            return;
        }
        if (MyPopUp.ResPlt().SOL_TYPE_ID() == -1) {
            DevExpress.ui.notify('El tipo de solicitud de la plantilla es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.ResPlt().RES_PLT_PARSER())) {
            DevExpress.ui.notify('El parser de la plantilla es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.ResPlt().RES_PLT_CODIGO())) {
            DevExpress.ui.notify('El código de la plantilla es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.ResPlt().RES_PLT_VERSION())) {
            DevExpress.ui.notify('La versión de la plantilla es requerida.', "warning", 3000);
            return;
        }
        if (MyPopUp.ResPlt().RES_PLT_STATUS() == -1) {
            DevExpress.ui.notify('El estado de la plantilla es requerido.', "warning", 3000);
            return;
        }

        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var ResPlt = ko.toJS(MyPopUp.ResPlt());
        ResPlt.RES_PLT_CRUD = MyPopUp.CRUDType();
        CRUDResolucionesPlantillas(ResPlt);

    }

    function OnCopyClipBoardTitle() {
        copyToClipboard(MyPopUp.ID());
        DevExpress.ui.notify("Se copió el título al Clipboard", "success", 2000);
    }

    function OnCopyClipBoardText() {
        copyToClipboard(MyPopUp.TEXT());
        DevExpress.ui.notify("Se copió el texto al Clipboard", "success", 2000);
    }

    function NewPlantilla() {
        var ResPlt = {
            
            RES_PLT_NAME: '',
            SOL_TYPE_ID: -1,
            RES_PLT_CODIGO: '',
            RES_PLT_VERSION: '',
            RES_PLT_STATUS: -1,
            RES_PLT_TITULO: '',
            RES_PLT_CONTENT: '',
            RES_PLT_PARSER: '',
        };
        MyPopUp.SOL_TYPES(SGEWeb.app.SOL_TYPES.filter(function (Item) {
            return Item.SOL_TYPE_CLASS == enumSOL_TYPES_CLASS.AdministrativoTecnico;
        }));
        MyPopUp.ResPlt(ko.mapping.fromJS(ko.toJS(ResPlt)));
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nueva plantilla de resolución");
        MyPopUp.Show(true);
    }

    function handleViewShown(e) {
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    GetResolucionesCombos();
    Refrescar();

    var viewModel = {
        viewShown: handleViewShown,
        Refrescar: Refrescar,
        MyDataSource: MyDataSource,
        GuardarDisabled: GuardarDisabled,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        Excel: Excel,
        OnGuardar: OnGuardar,
        MyPopUp: MyPopUp,
        OnCopyClipBoardTitle: OnCopyClipBoardTitle,
        OnCopyClipBoardText: OnCopyClipBoardText,
        NewPlantilla: NewPlantilla

    };
    return viewModel;
};