﻿SGEWeb.RDSResolucionesPlantillasDetail = function (params) {
    "use strict";
    var loadingVisible = ko.observable(true);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var PlantillaResolucion = JSON.parse(SGEWeb.app.PlantillaResolucion);
 
    if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
        CKEDITOR.tools.enableHtml5Elements(document);

    CKEDITOR.config.width = '100%';

    function CKEDITORCargarConfiguracion(config) {
        config.toolbar = [
            { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source'] },
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
            { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'CopyFormatting', 'RemoveFormat'] },
            { name: 'insert', items: ['SpecialChar', 'Table'] },

            { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'styles', items: ['Font', 'FontSize'] },
            { name: 'colors', items: ['TextColor', 'BGColor'] },
            '/',
            { items: ['ComboInfoConcesionario'] }, { items: ['ComboInfoAdministrativa'] }, { items: ['ConboInfoTecnica'] }, { items: ['ComboInfoRadicados'] }, { items: ['ComboInfoResolucion'] }, { items: ['ComboInfoAntecedentes'] }
        ];
        config.extraPlugins = 'ComboInfoAdministrativa,ConboInfoTecnica,ComboInfoRadicados,ComboInfoResolucion,ComboInfoConcesionario,ComboInfoAntecedentes';
        config.removePlugins = 'lite,flash,elementspath';

        config.font_names = "Arial/Arial, Helvetica, sans-serif;Arial Narrow/Arial Narrow;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
        config.fontSize_sizes = "8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;15/15pt;16/16pt;18/18pt;20/20pt;22/22pt;24/24pt;26/26pt;28/28pt;36/36pt;48/48pt;72/72pt";

        config.language = 'es';
        config.language_list = ["es:Español"];
        config.scayt_sLang = "es_ES";
        config.Administrativo = JSON.parse(SGEWeb.app.CombosResolucion.InformacionAdministrativa);
        config.Tecnica = JSON.parse(SGEWeb.app.CombosResolucion.InformacionTecnica);
        config.Radicados = JSON.parse(SGEWeb.app.CombosResolucion.InformacionRadicados);
        config.Resolucion = JSON.parse(SGEWeb.app.CombosResolucion.InformacionResolucion);
        config.Concesionario = JSON.parse(SGEWeb.app.CombosResolucion.InformacionConcesionario);
        config.Antecedentes = JSON.parse(SGEWeb.app.CombosResolucion.InformacionAntecedentes);
        config.height = '1000'; //Debe ser grande para que no se vea el efecto del resize
    }

    var StartCKEditor = (function (elementID) {
        var wysiwygareaAvailable = isWysiwygareaAvailable(),
            isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

        return function (elementID) {
            var editor = CKEDITOR.document.getById(elementID);

            var PlantillaResolucionEditor = $('#PlantillaResolucionEditor');

            if (wysiwygareaAvailable) {
                CKEDITOR.replace(elementID, {
                    on: {
                        'instanceReady': function (evt) {
                            evt.editor.resize("100%", PlantillaResolucionEditor.innerHeight(), false);
                        }
                    }
                }
               );
            } else {
                editor.setAttribute('contenteditable', 'true');
                CKEDITOR.inline(elementID);
            }
            CKEDITOR.instances[elementID].config.CustomConfig = CKEDITORCargarConfiguracion;
        };

        function isWysiwygareaAvailable() {
            if (CKEDITOR.revision == ('%RE' + 'V%')) {
                return true;
            }
            return !!CKEDITOR.plugins.get('wysiwygarea');
        }
    })();

    function CRUDResolucionesPlantillas(ResPlt) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDResolucionesPlantillas",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ResPlt: ResPlt
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDResolucionesPlantillasResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    SGEWeb.app.NeedRefresh = true;
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function OnSave() {

        loadingMessage('Guardando...');
        loadingVisible(true);
        GuardarDisabled(true);

        var ResPlt = {
            RES_PLT_UID: PlantillaResolucion.RES_PLT_UID,
            RES_PLT_NAME: PlantillaResolucion.RES_PLT_NAME,
            SOL_TYPE_ID: PlantillaResolucion.SOL_TYPE_ID,
            RES_PLT_CODIGO: PlantillaResolucion.RES_PLT_CODIGO,
            RES_PLT_VERSION: PlantillaResolucion.RES_PLT_VERSION,
            RES_PLT_STATUS: PlantillaResolucion.RES_PLT_STATUS,
            RES_PLT_PARSER: PlantillaResolucion.RES_PLT_PARSER,
            RES_PLT_CONTENT: CKEDITOR.instances['PlantillaResolucionEditor'].getData(),
            RES_PLT_CRUD: 3
        };

        CRUDResolucionesPlantillas(ResPlt);
    }

    function OnPdf() {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=PTRES&FUID=' + PlantillaResolucion.RES_PLT_UID + '&MRC=0&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function handleViewShown(e) {
        StartCKEditor('PlantillaResolucionEditor');
        CKEDITOR.instances['PlantillaResolucionEditor'].setData(PlantillaResolucion.RES_PLT_CONTENT);
        setTimeout(function () {
            loadingVisible(false);
        }, 500);
    }

    function handleviewDisposing(e) {
        $(window).off("resize", EditorResizer);

        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].destroy();
        }
    }

    function EditorResizer() {
        var PlantillaResolucionEditor = $('#PlantillaResolucionEditor');
        CKEDITOR.instances['PlantillaResolucionEditor'].resize('100%', PlantillaResolucionEditor.innerHeight(), false);
    }

    $(window).resize(EditorResizer);

    var viewModel = {
        viewShown: handleViewShown,
        viewDisposing: handleviewDisposing,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        OnSave: OnSave,
        title: PlantillaResolucion.RES_PLT_NAME,
        OnPdf: OnPdf
    };

    return viewModel;
};