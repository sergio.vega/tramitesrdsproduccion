﻿SGEWeb.RDSTexts = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);
    var GuardarDisabled = ko.observable(false);
    

    var MyPopUp = {
        Show: ko.observable(false),
        Title: ko.observable(''),
        ID: ko.observable(''),
        TYPE: ko.observable(''),
        GROUP: ko.observable(''),
        TEXT: ko.observable(''),
        CRUDType: ko.observable(0),
        SPELL: ko.observable(true)
    };


    function GetTexts() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetTexts",
            data: JSON.stringify({

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetTextsResult;

                MyDataSource(result);

                //MyDataSource(result);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDTexts(Text) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDTexts",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Text: Text
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDTextsResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    if (MyPopUp.CRUDType() == 1) {
                        MyPopUp.Title(MyPopUp.ID());
                        MyPopUp.CRUDType(3);
                    }
                    Refrescar();
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                .prop('title', 'Eliminar')
                .css('cursor', 'default')   
                .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                .prop('title', 'Eliminar')
                .css('cursor', 'pointer')
                .css('color', e.data.TXT_SYSTEM ? 'lightgrey' : 'black')
                .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSTexts') {
                switch (e.column.name) {
                    case 'Editar':
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title(e.data.TXT_ID);
                        MyPopUp.ID(e.data.TXT_ID);
                        MyPopUp.TYPE(e.data.TXT_TYPE);
                        MyPopUp.GROUP(e.data.TXT_GROUP);
                        MyPopUp.TEXT(e.data.TXT_TEXT);
                        MyPopUp.Show(true);
                        break;
                    case 'Eliminar':
                        if (e.data.TXT_SYSTEM) {
                            e.event.stopPropagation();
                            return;
                        }

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea eliminar el texto, podría estar siendo usado?"), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                SGEWeb.app.DisabledToolBar(true);
                                GuardarDisabled(true);
                                loadingVisible(true);

                                var Text = { 'TXT_ID': e.data.TXT_ID, 'TXT_TEXT': "", 'TXT_CRUD': 4 };
                                CRUDTexts(Text);
                            }
                        });
                        break;
                }
            }
        }
    }

    function CellDblClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSTexts') {
                switch (e.column.name) {
                    case 'ID':
                        copyToClipboard(e.data.TXT_ID);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'TYPE':
                        copyToClipboard(e.data.TXT_TYPE);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'GROUP':
                        copyToClipboard(e.data.TXT_GROUP);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'TEXT':
                        copyToClipboard(e.data.TXT_TEXT);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                }
            }
        }

    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function Excel() {
        var grid = $("#GridRDSTexts").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetTexts();
    }

    function OnGuardar() {
 
        if (MyPopUp.CRUDType() == 1) {
            if(IsNullOrEmpty(MyPopUp.ID())){
                DevExpress.ui.notify("El Key no puede estar vacío", "error", 2000);
                return;
            }
        }
        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var Text = { 'TXT_ID': MyPopUp.ID(), 'TXT_TYPE': MyPopUp.TYPE(), 'TXT_GROUP': MyPopUp.GROUP(), 'TXT_TEXT': MyPopUp.TEXT(), 'TXT_CRUD': MyPopUp.CRUDType() };
        CRUDTexts(Text);
    }

    function OnCopyClipBoardTitle() {
        copyToClipboard(MyPopUp.ID());
        DevExpress.ui.notify("Se copió el título al Clipboard", "success", 2000);
    }

    function OnCopyClipBoardText() {
        copyToClipboard(MyPopUp.TEXT());
        DevExpress.ui.notify("Se copió el texto al Clipboard", "success", 2000);
    }

    function NewText() {
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nuevo texto");
        MyPopUp.ID("");
        MyPopUp.TYPE("");
        MyPopUp.GROUP("");
        MyPopUp.TEXT("");
        MyPopUp.Show(true);
    }

    Refrescar();

    var viewModel = {
        Refrescar: Refrescar,
        MyDataSource: MyDataSource,
        GuardarDisabled: GuardarDisabled,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        onHeaderTemplate:onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        CellDblClick: CellDblClick,
        onContentReady: onContentReady,
        Excel: Excel,
        OnGuardar: OnGuardar,
        MyPopUp: MyPopUp,
        OnCopyClipBoardTitle: OnCopyClipBoardTitle,
        OnCopyClipBoardText: OnCopyClipBoardText,
        NewText: NewText
    };

    return viewModel;
};