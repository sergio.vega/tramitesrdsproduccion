﻿SGEWeb.RDSImages = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var GuardarDisabled = ko.observable(false);

    var MyPopUpImage = {
        Show: ko.observable(false),
        CRUDType: ko.observable(0),
        IMG_ID: ko.observable(''),
        IMG_SYSTEM: ko.observable(false),
        FileImage: {
            Name: ko.observable('Seleccionar imagen'),
            Data: undefined,
            Content: undefined,
            Cargado: ko.observable(false)
        }
    };

    function GetImages() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetImages",
            data: JSON.stringify({

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetImagesResult;

                MyDataSource(result);
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function CRUDImages(Image) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDImages",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Image: Image
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDImagesResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                    MyPopUpImage.Show(false);
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    var onItemClick = function (e) {
        MyPopUpImage.FileImage.Data = undefined;
        MyPopUpImage.FileImage.Cargado(false);
        MyPopUpImage.FileImage.Name('Seleccionar imagen');
        MyPopUpImage.IMG_ID(e.itemData.IMG_ID);
        MyPopUpImage.IMG_SYSTEM(e.itemData.IMG_SYSTEM);
        MyPopUpImage.CRUDType(3);
        MyPopUpImage.Show(true);
    }

    function NewImage() {
        MyPopUpImage.FileImage.Data = undefined;
        MyPopUpImage.FileImage.Cargado(false);
        MyPopUpImage.FileImage.Name('Seleccionar imagen');
        MyPopUpImage.IMG_ID('');
        MyPopUpImage.IMG_SYSTEM(false);
        MyPopUpImage.CRUDType(1);
        MyPopUpImage.Show(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetImages();
    }

    function CargarFile(data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (e.target.id == 'FileImage') {
                if (file.type.indexOf("image/png") != 0 && file.type.indexOf("image/jpeg") != 0) {
                    DevExpress.ui.notify('Formato de archivo inválido. Formato válido (png, jpg).', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }

                if (file.name.length > 50) {
                    DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + 50 + ' caracteres', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }

                if (file.size > (1 * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + 1 + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }

                MyPopUpImage.FileImage.Content = file.type;
                MyPopUpImage.FileImage.Name(file.name);
            }

            var fileReader = new FileReader();
            fileReader.onload = function () {
                MyPopUpImage.FileImage.Data = fileReader.result;
                MyPopUpImage.FileImage.Cargado(true);
            };

            fileReader.readAsDataURL(file);

            ElementFile.value = null;
        }
    }

    function OnGuardarImage() {
        if (MyPopUpImage.CRUDType() == 1) {
            if (IsNullOrEmpty(MyPopUpImage.IMG_ID())) {
                DevExpress.ui.notify("El nombre de la imagen no puede estar vacío", "error", 2000);
                return;
            }

        }
        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var Image = { 'IMG_ID': MyPopUpImage.IMG_ID(), 'IMG_CONTENT_TYPE': MyPopUpImage.FileImage.Content, 'IMG_CRUD': MyPopUpImage.CRUDType(), 'IMG_FILE': MyPopUpImage.FileImage.Data };

        CRUDImages(Image);

    }

    function OnDeleteImage() {
        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea eliminar la imagen, podría estar siendo usada?"), SGEWeb.app.Name);
        result.done(function (dialogResult) {
            if (dialogResult) {
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingVisible(true);

                var Image = { 'IMG_ID': MyPopUpImage.IMG_ID(), 'IMG_CONTENT_TYPE': '', 'IMG_CRUD': 4 };
                CRUDImages(Image);

            }
        });
    }

    Refrescar();

    var viewModel = {
        MyDataSource: MyDataSource,
        Refrescar: Refrescar,
        loadingVisible: loadingVisible,
        GuardarDisabled: GuardarDisabled,
        NewImage: NewImage,
        onItemClick: onItemClick,
        MyPopUpImage: MyPopUpImage,
        OnGuardarImage: OnGuardarImage,
        CargarFile: CargarFile,
        OnDeleteImage: OnDeleteImage
    };

    return viewModel;
};