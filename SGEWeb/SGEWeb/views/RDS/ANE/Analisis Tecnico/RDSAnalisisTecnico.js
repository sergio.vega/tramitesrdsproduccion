﻿SGEWeb.RDSAnalisisTecnico = function (params) {
    SGEWeb.app.NeedRefresh = false;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var Solicitud = JSON.parse(SGEWeb.app.Solicitud);
    var CURRENT = ko.mapping.fromJS(Solicitud.CURRENT);
    var MyPopUp = { Show: ko.observable(false), Title: ko.observable(''), STATE_ID: ko.observable(), COMMENT: ko.observable(), UID: ko.observable(), NAME: ko.observable() };
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var PTNRSOriginal = ko.observable(Solicitud.AnalisisTecnico.TECH_PTNRS_UPDATED);

    var USR_ADMIN = ko.mapping.fromJS(Solicitud.USR_ADMIN);
    var USR_TECH = ko.mapping.fromJS(Solicitud.USR_TECH);

    var Tecnico = ko.observableArray([]);
    Tecnico().push(ko.mapping.fromJS(Solicitud.AnalisisTecnico));
    Tecnico()[0].TECH_NAME = ko.observable('Análisis técnico');
    
    var ComunicadoTecnico = ko.observableArray([]);
    if (Solicitud.ComunicadoTecnico != null) {
        ComunicadoTecnico.push(ko.mapping.fromJS(Solicitud.ComunicadoTecnico));
    }

    var TechDocsList = ko.observableArray([]);

    function CalculateTechDocsList() {
        TechDocsList([]);

        Solicitud.TechDocs.forEach(function (entry) {
            if (entry.TECH_TYPE == enumDOC_TECH_TYPE.Cuadro) {
                if (Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID == enumROLE1_STATE.Aprobado) {
                    TechDocsList.push(ko.mapping.fromJS(entry));
                }
            }
            else {
                TechDocsList.push(ko.mapping.fromJS(entry));
            }
        });
    }
   
    CalculateTechDocsList();


    var FileSoporte = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Soporte,
        EXTENSIONS: ['.xlsx'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(true),
        UID: null,
        MAX_SIZE: 100,
        MAX_NAME_LENGTH: 50
    };

    var FileConcepto = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Concepto,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(true),
        UID: null,
        MAX_SIZE: 100,
        MAX_NAME_LENGTH: 50
    };

    var FileCuadro = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Cuadro,
        EXTENSIONS: ['.pdf'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(false),
        UID: null,
        MAX_SIZE: 100,
        MAX_NAME_LENGTH: 50,
        VALIDATE_NAME : true
    };




    function CalculateTechDocs() {
        var Soporte = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileSoporte.TYPE_ID);
        if (Soporte != null) {
            FileSoporte.UID = Soporte.TECH_UID;
            FileSoporte.NAME(Soporte.TECH_NAME);
            FileSoporte.CONTENT_TYPE(Soporte.TECH_CONTENT_TYPE);
            FileSoporte.CHANGED(Soporte.TECH_CHANGED);
        }
        var Concepto = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileConcepto.TYPE_ID);
        if (Concepto != null) {
            FileConcepto.UID = Concepto.TECH_UID;
            FileConcepto.NAME(Concepto.TECH_NAME);
            FileConcepto.CONTENT_TYPE(Concepto.TECH_CONTENT_TYPE);
            FileConcepto.CHANGED(Concepto.TECH_CHANGED);
        }
        var Cuadro = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileCuadro.TYPE_ID);
        if (Cuadro != null) {
            FileCuadro.UID = Cuadro.TECH_UID;
            FileCuadro.NAME(Cuadro.TECH_NAME);
            FileCuadro.CONTENT_TYPE(Cuadro.TECH_CONTENT_TYPE);
            FileCuadro.CHANGED(Cuadro.TECH_CHANGED);

            if (Tecnico()[0].TECH_ROLE1_STATE_ID() == enumROLE1_STATE.Aprobado) {
                FileCuadro.VISIBLE(true);
            } else {
                FileCuadro.VISIBLE(false);
            }
        }

        FileSoporte.LOADED(false);
        FileConcepto.LOADED(false);
        FileCuadro.LOADED(false);

        FileSoporte.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);
        FileConcepto.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);
        FileCuadro.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);

    }

    CalculateTechDocs()


    var AnalisisTecnicoCompleto = ko.computed(function () {
        var Resultado = true;
        switch (ROLE) {
            case enumROLES.Funcionario:
                Tecnico().forEach(function (entry) {
                    if (entry.TECH_ROLE1_STATE_ID() == enumROLE1_STATE.SinDefinir || !entry.TECH_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                //if (!FileSoporte.CHANGED() || !FileConcepto.CHANGED() || !FileCuadro.CHANGED())
                if (!FileSoporte.CHANGED() || !FileConcepto.CHANGED() || (Tecnico()[0].TECH_ROLE1_STATE_ID()==enumROLE1_STATE.Aprobado && !FileCuadro.CHANGED()))
                     Resultado = false;
                break;
            case enumROLES.Revisor:
                Tecnico().forEach(function (entry) {
                    if (entry.TECH_ROLE2_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.TECH_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Coordinador:
                Tecnico().forEach(function (entry) {
                    if (entry.TECH_ROLE4_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.TECH_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Subdirector:
                Tecnico().forEach(function (entry) {
                    if (entry.TECH_ROLE8_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.TECH_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
        }
        return Resultado;
    });

    var TecnicoDisable = ko.computed(function () {
        if ((CURRENT.GROUP() != enumGROUPS.ANE) || (CURRENT.STEP() != enumSTEPS.Tecnico) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
            return true;
        return false;
    });

    var CanEdit = ko.computed(function () {
        if ((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == enumROLES.Funcionario)
            return true;
        return false;
    });

    var BotonPNTRSVisible = ko.computed(function () {
        if (!CanEdit()) {
            if (ROLE == enumROLES.Funcionario) {
                if (PTNRSOriginal() == false && Tecnico()[0].TECH_PTNRS_UPDATED() == true) {
                    return true;
                }
            }
        }
        return false;
    });

    var SwitchPNTRSEnable = ko.computed(function () {
        if (CanEdit()) {
            return false;
        }
        if (ROLE == enumROLES.Funcionario) {
            if (PTNRSOriginal() == false) {
                return false;
            }
        }
        
        return true;
    });

    function EvaluateIcon(FileObject) {
        var Icon = '';

        if ((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0)
            return 'ta ta-empty';

        if (FileObject.CHANGED()) Icon = 'ta ta-chulo';
        else Icon = 'ta ta-empty';
        return Icon;
    }

    function GuardarTramiteTecnico(SolicitudParameter, bGenerarComunicado, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GuardarTramiteTecnico",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ROLE: ROLE,
                Solicitud: SolicitudParameter,
                bGenerarComunicado: bGenerarComunicado,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);

                var result = msg.GuardarTramiteTecnicoResult;

                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);

                    Solicitud = result.Solicitud;
                    
                    if (BotonPNTRSVisible()) {
                        PTNRSOriginal(Solicitud.AnalisisTecnico.TECH_PTNRS_UPDATED);
                    }

                    if (bGenerarComunicado) {

                        ko.mapping.fromJS(Solicitud.USR_ADMIN, USR_ADMIN);
                        ko.mapping.fromJS(Solicitud.USR_TECH, USR_TECH);

                        Tecnico().forEach(function (entry) {
                            entry.TECH_ROLEX_CHANGED(false);
                        });

                        Tecnico()[0].TECH_MODIFIED_DATE(Solicitud.AnalisisTecnico.TECH_MODIFIED_DATE);

                        CURRENT.GROUP(Solicitud.CURRENT.GROUP);
                        CURRENT.ROLE(Solicitud.CURRENT.ROLE);
                        CURRENT.STEP(Solicitud.CURRENT.STEP);

                        var grid = $("#GridTecnico").dxDataGrid('instance');
                        if (grid != undefined) {
                            grid.repaint();
                        }
                        
                        if (ROLE == enumROLES.Funcionario) {

                            PTNRSOriginal(Solicitud.AnalisisTecnico.TECH_PTNRS_UPDATED);

                            if (Solicitud.ComunicadoTecnico != null) {
                                ComunicadoTecnico([]);
                                ComunicadoTecnico.push(ko.mapping.fromJS(Solicitud.ComunicadoTecnico));
                                var list = $("#ListComunicadoTecnico").dxList('instance');
                                if (list != undefined) 
                                    list.reload();
                            }

                            CalculateTechDocsList();
                            var listDocs = $("#ListTechDocuments").dxList('instance');
                            if (listDocs != undefined)
                                listDocs.reload();

                        }
                    }

                    CalculateTechDocs();
                }
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 5000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function GetOpacity(enumROLE, ROLEX_CHANGED) {
        if (CURRENT.GROUP() == enumGROUPS.MinTIC) {
            if ((CURRENT.ROLE() + 8) > enumROLE)
                return 1;
            if ((CURRENT.ROLE() + 8) < enumROLE)
                return 0.3;
            if (ROLEX_CHANGED)
                return 1;
            else
                return 0.3;
        }

        if (CURRENT.ROLE() > enumROLE)
            return 1;
        if (CURRENT.ROLE() < enumROLE)
            return 0.3;
        if (ROLEX_CHANGED)
            return 1;
        else
            return 0.3;
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Funcionario':
                $('<i/>').addClass(ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Icon)
                .prop('title', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Text + (IsNullOrEmpty(e.data.TECH_ROLE1_COMMENT()) ? '' : ': ' + e.data.TECH_ROLE1_COMMENT()))
                .css('cursor', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Cursor)
                .css('color', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Color)
                .css('opacity', GetOpacity(enumROLES.Funcionario, e.data.TECH_ROLEX_CHANGED()))
                .appendTo(container);
                break;

            case 'Revisor':
            case 'Coordinador':
            case 'Subdirector':
                $('<i/>').addClass(ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Icon)
                .prop('title', ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Text + (IsNullOrEmpty(e.data[GetTechComment(e.column.name)]()) ? '' : ': ' + e.data[GetTechComment(e.column.name)]()))
                .css('cursor', 'default')
                .css('color', ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Color)
                .css('margin-left', '5px')
                .css('opacity', GetOpacity(enumROLES[e.column.name], e.data.TECH_ROLEX_CHANGED()))
                .appendTo(container);
                break;

            case 'MinTIC':
                $('<i/>').addClass(ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Icon)
                .prop('title', ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Text + (IsNullOrEmpty(e.data.TECH_ROLE_MINTIC_COMMENT()) ? '' : ': ' + e.data.TECH_ROLE_MINTIC_COMMENT()))
                .css('cursor', 'default')
                .css('color', ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Color)
                .css('margin-left', '5px')
                .css('opacity', GetOpacity(enumROLES.Subdirector + 8, e.data.TECH_ROLEX_CHANGED()))
                .appendTo(container);
                break;

            case 'Rechazar':
            case 'Requerir':
            case 'Aprobar':
            case 'Devolver':
            case 'Aceptar':
                var Disable_Real = (CURRENT.GROUP() != enumGROUPS.ANE) || (CURRENT.STEP() != enumSTEPS.Tecnico) || (ROLE != CURRENT.ROLE());
                if (e.column.name=='Requerir' && Solicitud.SOL_TYPE_ID == 7)//Prorroga de concesión no requiere en análisis técnico
                    Disable_Real = true;
                $('<i/>').addClass(e.column.icon)
                .prop('title', e.column.caption + ' análisis')
                .css('cursor', (Disable_Real ? 'default' : 'pointer'))
                .css('color', Disable_Real ? 'Black' : e.column.color)
                .css('opacity', (Disable_Real ? 0.3 : 1))
                .appendTo(container);
                break;

                defaul:
                    break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridTecnico') {
                switch (e.column.name) {
                    case 'Rechazar':
                    case 'Requerir':
                    case 'Aprobar':
                        if (TecnicoDisable()) {
                            e.event.stopPropagation();
                            break;
                        }
                        if (e.column.name == 'Requerir' && Solicitud.SOL_TYPE_ID == 7)//Prorroga de concesión no requiere en análisis técnico
                        {
                            e.event.stopPropagation();
                            break;
                        }
                        MyPopUp.UID(e.data.SOL_UID());
                        MyPopUp.STATE_ID(e.column.ID);
                        MyPopUp.NAME(e.data.TECH_NAME());
                        MyPopUp.COMMENT('');
                        if (e.data.TECH_ROLE1_STATE_ID() == e.column.ID)
                            MyPopUp.COMMENT(e.data.TECH_ROLE1_COMMENT());
                        MyPopUp.Title(e.column.caption);
                        MyPopUp.Show(true);
                        break;

                    case 'Devolver':
                    case 'Aceptar':
                        if (TecnicoDisable()) {
                            e.event.stopPropagation();
                            break;
                        }
                        if (ROLE < enumROLES.Subdirector && e.data[GetTechStateID(GetNextRole(ROLE))]() == enumROLE248_STATE.Aceptado) {
                            e.event.stopPropagation();
                            break;
                        }

                        //Cuando acepta para que no aparezca el popup, quitar este bloque para que si aparezca el popup
                        if (e.column.ID == 1) {
                            e.data.TECH_ROLEX_CHANGED(true);
                            e.data[GetTechComment(ROLE)]('');
                            e.data[GetTechStateID(ROLE)](e.column.ID);
                            e.data.TECH_STATE_ID.valueHasMutated();
                            break;
                        }
                        MyPopUp.UID(e.data.SOL_UID());
                        MyPopUp.STATE_ID(e.column.ID);
                        MyPopUp.NAME(e.data.TECH_NAME());
                        MyPopUp.COMMENT('');
                        if (e.data[GetTechStateID(ROLE)]() == e.column.ID)
                            MyPopUp.COMMENT(e.data[GetTechComment(ROLE)]());
                        MyPopUp.Title(e.column.caption);
                        MyPopUp.Show(true);
                        break;
                }
            }
        }
    }

    function PrepararFiles() {
        var Files = [];
        if (FileSoporte.LOADED())
            Files.push({ TYPE_ID: FileSoporte.TYPE_ID, NAME: FileSoporte.NAME(), CONTENT_TYPE: FileSoporte.CONTENT_TYPE_LOCAL(), DATA: FileSoporte.DATA });
        if (FileConcepto.LOADED())
            Files.push({ TYPE_ID: FileConcepto.TYPE_ID, NAME: FileConcepto.NAME(), CONTENT_TYPE: FileConcepto.CONTENT_TYPE_LOCAL(), DATA: FileConcepto.DATA });
        if (FileCuadro.LOADED())
            Files.push({ TYPE_ID: FileCuadro.TYPE_ID, NAME: FileCuadro.NAME(), CONTENT_TYPE: FileCuadro.CONTENT_TYPE_LOCAL(), DATA: FileCuadro.DATA });

        return Files;
    }

    function GuardarTramite() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Guardando trámite...");
        loadingVisible(true);
        GuardarDisabled(true);
        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
        SGEWeb.app.NeedRefresh = true;

        GuardarTramiteTecnico(Solicitud, false, PrepararFiles());
    }

    function ContinuarTramite() {
        switch (ROLE) {
            case enumROLES.Funcionario:
                if (AnalisisTecnicoCompleto() == false) {
                    DevExpress.ui.notify('Análisis técnico incompleto. Debe aprobar, requerir o rechazar el análisis técnico y subir todos los documentos.', "warning", 3000);
                    return;
                }
                var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                result.done(function (dialogResult) {
                    if (dialogResult) {
                        loadingMessage("Continuando trámite...");
                        loadingVisible(true);
                        GuardarDisabled(true);
                        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                        SGEWeb.app.NeedRefresh = true;
                        GuardarTramiteTecnico(Solicitud, true, PrepararFiles());
                    }
                });
                break;

            case enumROLES.Revisor:
            case enumROLES.Coordinador:
                if (AnalisisTecnicoCompleto() == false) {
                    DevExpress.ui.notify('Análisis técnico incompleto. Debe aceptar o devolver el análisis técnico.', "warning", 3000);
                    return;
                }
                var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                result.done(function (dialogResult) {
                    if (dialogResult) {
                        loadingMessage("Continuando trámite...");
                        loadingVisible(true);
                        GuardarDisabled(true);
                        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                        SGEWeb.app.NeedRefresh = true;
                        GuardarTramiteTecnico(Solicitud, true, PrepararFiles());
                    }
                });
                break;

            case enumROLES.Subdirector:
                if (AnalisisTecnicoCompleto() == false) {
                    DevExpress.ui.notify('Análisis técnico incompleto. Debe aceptar o devolver el análisis técnico.', "warning", 3000);
                    return;
                }
                var Devolver = KoListObjectExists(Tecnico(), "TECH_ROLE8_STATE_ID", enumROLE248_STATE.Devuelto);

                if (!Devolver) {
                    if (SGEWeb.app.User.USR_HAS_SIGN == 0) {
                        DevExpress.ui.notify('El usuario no tiene firma digital registrada en el sistema. En administración de usuarios puede cargar la firma.', "warning", 5000);
                        return;
                    }
                }

                var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                result.done(function (dialogResult) {
                    if (dialogResult) {

                        loadingMessage('Continuando trámite...');
                        loadingVisible(true);
                        GuardarDisabled(true);
                        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                        SGEWeb.app.NeedRefresh = true;
                        GuardarTramiteTecnico(Solicitud, true, PrepararFiles());
                    }
                });
                break;
        }

    }

    function OnGuardarComentario() {

        if (((ROLE == enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE1_STATE.Aprobado) || (ROLE != enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE248_STATE.Aceptado)) && (MyPopUp.COMMENT() == null || MyPopUp.COMMENT() == undefined || MyPopUp.COMMENT().length < 10)) {
            DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
            return;
        }

        var TecnicoItem = KoListObjectFindGetElement(Tecnico(), 'SOL_UID', MyPopUp.UID());

        TecnicoItem.TECH_ROLEX_CHANGED(true);

        switch (ROLE) {
            case enumROLES.Funcionario:
                if (MyPopUp.STATE_ID() == enumROLE1_STATE.Aprobado) {
                    FileCuadro.VISIBLE(true);
                } else {
                    FileCuadro.VISIBLE(false);
                }
                TecnicoItem.TECH_ROLE1_COMMENT(MyPopUp.COMMENT());
                TecnicoItem.TECH_ROLE1_STATE_ID(MyPopUp.STATE_ID());
                TecnicoItem.TECH_STATE_ID.valueHasMutated();
                break;
            case enumROLES.Revisor:
                TecnicoItem.TECH_ROLE2_COMMENT(MyPopUp.COMMENT());
                TecnicoItem.TECH_ROLE2_STATE_ID(MyPopUp.STATE_ID());
                TecnicoItem.TECH_STATE_ID.valueHasMutated();
                break;
            case enumROLES.Coordinador:
                TecnicoItem.TECH_ROLE4_COMMENT(MyPopUp.COMMENT());
                TecnicoItem.TECH_ROLE4_STATE_ID(MyPopUp.STATE_ID());
                TecnicoItem.TECH_STATE_ID.valueHasMutated();
                break;
            case enumROLES.Subdirector:
                TecnicoItem.TECH_ROLE8_COMMENT(MyPopUp.COMMENT());
                TecnicoItem.TECH_ROLE8_STATE_ID(MyPopUp.STATE_ID());
                TecnicoItem.TECH_STATE_ID.valueHasMutated();
                break;
        }
        MyPopUp.Show(false);


    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            if (FileObject.VALIDATE_NAME == true) {
                if (!file.name.endsWith(' - ' + Solicitud.Expediente.SERV_NUMBER + '.pdf')) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }
                var suffix = file.name.indexOf(' - ' + Solicitud.Expediente.SERV_NUMBER + '.pdf');
                if (suffix > 10 || suffix < 1) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }

                if (!isNormalInteger(file.name.substr(0, suffix))) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
                FileObject.CHANGED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }

    function OpenFile(data, e) {

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TEC&FUID=' + data.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");

    }

    function OnOpenTechDoc(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TEC&FUID=' + e.itemData.TECH_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function OnOpenComunicadoTecnico(e) {
       window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=COM&FUID=' + e.itemData.COM_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function GuardarPTNRS() {
        GuardarTramite();
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    var viewModel = {
        OnGuardarComentario: OnGuardarComentario,
        MyPopUp: MyPopUp,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        Solicitud: Solicitud,
        Tecnico: Tecnico,
        GuardarTramite: GuardarTramite,
        ContinuarTramite: ContinuarTramite,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        ROLE: ROLE,
        CURRENT:CURRENT,
        TecnicoDisable: TecnicoDisable,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        FileSoporte: FileSoporte,
        FileConcepto:FileConcepto,
        FileCuadro: FileCuadro,
        EvaluateIcon: EvaluateIcon,
        OnOpenComunicadoTecnico: OnOpenComunicadoTecnico,
        ComunicadoTecnico: ComunicadoTecnico,
        OnOpenTechDoc: OnOpenTechDoc,
        CanEdit: CanEdit,
        TechDocsList: TechDocsList,
        GuardarPTNRS: GuardarPTNRS,
        BotonPNTRSVisible: BotonPNTRSVisible,
        SwitchPNTRSEnable: SwitchPNTRSEnable,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        USR_ADMIN: USR_ADMIN,
        USR_TECH: USR_TECH
    };

    return viewModel;
};