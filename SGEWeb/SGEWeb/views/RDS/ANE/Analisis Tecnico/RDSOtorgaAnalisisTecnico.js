﻿SGEWeb.RDSOtorgaAnalisisTecnico = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var Solicitud = JSON.parse(SGEWeb.app.Solicitud);
    var SaltarOpened = ROLE == 1 ? false : true;
    var CURRENT = ko.mapping.fromJS(Solicitud.CURRENT);
    var MyPopUp = { Show: ko.observable(false), Title: ko.observable(''), Who: '', STATE_ID: ko.observable(), COMMENT: ko.observable(), UID: ko.observable(), NAME: ko.observable() };
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    
    var USR_ADMIN = ko.mapping.fromJS(Solicitud.USR_ADMIN);
    var USR_TECH = ko.mapping.fromJS(Solicitud.USR_TECH);

    var Tecnico = ko.mapping.fromJS(Solicitud.TechAnexos);

    var GridResolucionInstance = null;
    var Resolucion = ko.observableArray([]);
    Resolucion().push(ko.mapping.fromJS(Solicitud.AnalisisResolucion));
    Resolucion()[0].RES_NAME = ko.observable('Análisis resolución');
    Resolucion()[0].RES_CREATED_DATE = ko.observable(Solicitud.Resolucion.RES_CREATED_DATE);
    Resolucion()[0].RES_MODIFIED_DATE = ko.observable(Solicitud.Resolucion.RES_MODIFIED_DATE);

    var DocumentoResolucion = ko.observableArray([
        {
            RES_IS_CREATED: ko.observable(Solicitud.Resolucion.RES_IS_CREATED),
            RES_NAME: ko.observable(Solicitud.Resolucion.RES_NAME + '.pdf'),
        }
    ]);

    var TechDocsList = ko.observableArray([]);

    function CalculateTechDocsList() {
        TechDocsList([]);

        Solicitud.TechDocs.forEach(function (entry) {
            TechDocsList.push(ko.mapping.fromJS(entry));            
        });
    }

    CalculateTechDocsList();

    var ComunicadoTecnico = ko.observableArray([]);
    if (Solicitud.ComunicadoTecnico != null) {
        ComunicadoTecnico.push(ko.mapping.fromJS(Solicitud.ComunicadoTecnico));
    }

    var FileSoporte = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Soporte,
        EXTENSIONS: ['.xlsx'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(true),
        UID: null,
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };

    var FileConcepto = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Concepto,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(true),
        UID: null,
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };

    var FileCuadro = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE: ko.observable(''),
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumDOC_TECH_TYPE.Cuadro,
        EXTENSIONS: ['.pdf'],
        DISABLED: ko.observable(true),
        CHANGED: ko.observable(false),
        VISIBLE: ko.observable(false),
        UID: null,
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50,
        VALIDATE_NAME: true
    };

    function CalculateTechDocs() {
        var Soporte = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileSoporte.TYPE_ID);
        if (Soporte != null) {
            FileSoporte.UID = Soporte.TECH_UID;
            FileSoporte.NAME(Soporte.TECH_NAME);
            FileSoporte.CONTENT_TYPE(Soporte.TECH_CONTENT_TYPE);
            FileSoporte.CHANGED(Soporte.TECH_CHANGED);
        }
        var Concepto = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileConcepto.TYPE_ID);
        if (Concepto != null) {
            FileConcepto.UID = Concepto.TECH_UID;
            FileConcepto.NAME(Concepto.TECH_NAME);
            FileConcepto.CONTENT_TYPE(Concepto.TECH_CONTENT_TYPE);
            FileConcepto.CHANGED(Concepto.TECH_CHANGED);
        }
        var Cuadro = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', FileCuadro.TYPE_ID);
        if (Cuadro != null) {
            FileCuadro.UID = Cuadro.TECH_UID;
            FileCuadro.NAME(Cuadro.TECH_NAME);
            FileCuadro.CONTENT_TYPE(Cuadro.TECH_CONTENT_TYPE);
            FileCuadro.CHANGED(Cuadro.TECH_CHANGED);

            Tecnico().forEach(function (entry) {
                if (entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.Aprobado) {
                        FileCuadro.VISIBLE(true);
                }
                else {
                    FileCuadro.VISIBLE(false);
                }
                }); 
        }

        FileSoporte.LOADED(false);
        FileConcepto.LOADED(false);
        FileCuadro.LOADED(false);

        FileSoporte.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);
        FileConcepto.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);
        FileCuadro.DISABLED((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0);

    }

    CalculateTechDocs()

    var CanEdit = ko.computed(function () {
        if ((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == enumROLES.Funcionario)
            return true;
        return false;
    });


    function PreSelectedTab() {
        var Resultado = 0;
        switch (CURRENT.STEP()) {
            case enumSTEPS.Tecnico:
                Resultado = 2;
                break;
            case enumSTEPS.EnCierre:
            case enumSTEPS.Terminada:
                if (Solicitud.SOL_TYPE_CLASS == enumSOL_TYPES_CLASS.AdministrativoOnly)
                    Resultado = 1;
                else
                    Resultado = 3;
                break;
        }
        return Resultado;
    };

    var AnalisisTecnicoCompleto = ko.computed(function () {
        var Resultado = true;
        switch (ROLE) {
            case enumROLES.Funcionario:
                Tecnico().forEach(function (entry) {
                    if (entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.SinDefinir || entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.Radicado) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Coordinador:
                Tecnico().forEach(function (entry) {
                    if (entry.ANX_ROLE4_STATE_ID() == enumROLE248_STATE.SinDefinir) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Subdirector:
                Tecnico().forEach(function (entry) {
                    if (entry.ANX_ROLE8_STATE_ID() == enumROLE248_STATE.SinDefinir ) {
                        Resultado = false;
                    }
                });
                break;
        }
        return Resultado;
    });

    var AnalisisResolucionCompleto = ko.computed(function () {
        var Resultado = true;
        if (!DocumentoResolucion()[0].RES_IS_CREATED())
            return false;
        if (ROLE != enumROLES.Administrador) {
            Resolucion().forEach(function (entry) {
                if (entry[GetResStateID(ROLE)]() == enumROLE248_STATE.SinDefinir || !entry.RES_ROLEX_CHANGED()) {
                    Resultado = false;
                }
            });
        }

        return Resultado;
    });


    var TecnicoDisable = ko.computed(function () {
        if (CURRENT.GROUP() != enumGROUPS.ANE || ((CURRENT.STEP() != enumSTEPS.Tecnico)) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
            return true;
        return false;
    });

    //var TecnicoDisable = ko.computed(function () {
    //    if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Tecnico) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
    //        return true;
    //    return false;
    //});

    function GuardarOtorgaTramiteTecnico(SolicitudParameter, bGenerarComunicado, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GuardarOtorgaTramiteTecnico",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ROLE: ROLE,
                Solicitud: SolicitudParameter,
                bGenerarComunicado: bGenerarComunicado,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);

                var result = msg.GuardarOtorgaTramiteTecnicoResult;

                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);

                    Solicitud = result.Solicitud;

                    if (bGenerarComunicado) {
                        ko.mapping.fromJS(Solicitud.USR_ADMIN, USR_ADMIN);
                        ko.mapping.fromJS(Solicitud.USR_TECH, USR_TECH);

                        CURRENT.GROUP(Solicitud.CURRENT.GROUP);
                        CURRENT.ROLE(Solicitud.CURRENT.ROLE);
                        CURRENT.STEP(Solicitud.CURRENT.STEP);

                        Resolucion()[0].RES_ROLEX_CHANGED(Solicitud.AnalisisResolucion.RES_ROLEX_CHANGED);

                        Tecnico().forEach(function (entry) {
                            entry.ANX_ROLEX_CHANGED(ListObjectGetAttribute(Solicitud.TechAnexos, 'ANX_UID', entry.ANX_UID(), 'ANX_ROLEX_CHANGED'));
                        });

                        var gridTecnico = $("#GridTecnico").dxDataGrid('instance');
                        if (gridTecnico != undefined) {
                            gridTecnico.repaint();
                        }
                                                
                        if (ROLE == enumROLES.Funcionario) {
                            if (Solicitud.ComunicadoTecnico != null) {
                                ComunicadoTecnico([]);
                                ComunicadoTecnico.push(ko.mapping.fromJS(Solicitud.ComunicadoTecnico));
                                var list = $("#ListComunicadoTecnico").dxList('instance');
                                if (list != undefined)
                                    list.reload();
                            }
                        }

                    }
                }
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 5000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    
    function GetResolucion(SOL_UID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetResolucion",
            data: JSON.stringify({
                SOL_UID: SOL_UID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                Solicitud.Resolucion = msg.GetResolucionResult;
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetOpacityAdmin(enumROLE, ROLEX_CHANGED) {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
                if (CURRENT.ROLE() > enumROLE)
                    return 1;
                if (CURRENT.ROLE() < enumROLE)
                    return 0.3;
                if (ROLEX_CHANGED)
                    return 1;
                else
                    return 0.3;
                break;

            case enumSTEPS.Tecnico:
            case enumSTEPS.Resolucion:
                return 1;
                break;
        }
    }


    function onCellTemplate(container, e) {
        switch (e.component._$element[0].id) {
            case 'GridTecnico':
                switch (e.column.name) {
                    case 'Funcionario':
                        $('<i/>').addClass(ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Icon)
                        .prop('title', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Text + (IsNullOrEmpty(e.data.ANX_ROLE1_COMMENT()) ? '' : ': ' + e.data.ANX_ROLE1_COMMENT()))
                        .css('cursor', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Cursor)
                        .css('color', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Color)
                        .css('opacity', GetOpacityAdmin(enumROLES.Funcionario, e.data.ANX_ROLEX_CHANGED()))
                        .appendTo(container);
                        break;
                    case 'Revisor':
                    case 'Coordinador':
                    case 'Subdirector':
                        $('<i/>').addClass(ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Icon)
                        .prop('title', ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Text + (IsNullOrEmpty(e.data[GetAdminComment(e.column.name)]()) ? '' : ': ' + e.data[GetAdminComment(e.column.name)]()))
                        .css('cursor', 'default')
                        .css('color', ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Color)
                        .css('margin-left', '5px')
                        .css('opacity', GetOpacityAdmin(enumROLES[e.column.name], e.data.ANX_ROLEX_CHANGED()))
                        .appendTo(container);
                        break;
                    case 'Ver':
                        $('<i/>').addClass('ta ta-eye ta-lg')
                        .prop('title', 'Ver documento')
                        .css('cursor', 'pointer')
                        .appendTo(container);
                        break;
                    case 'Rechazar':
                    case 'Requerir':
                    case 'Aprobar':
                    case 'Devolver':
                    case 'Aceptar':
                        var Disable_Real = (ROLE != CURRENT.ROLE()) ||
                                            (ROLE == enumROLES.Funcionario && e.data.ANX_ROLE2_STATE_ID() == enumROLE248_STATE.Aceptado) ||
                                            (ROLE == enumROLES.Revisor && e.data.ANX_ROLE4_STATE_ID() == enumROLE248_STATE.Aceptado) ||
                                            (ROLE == enumROLES.Coordinador && e.data.ANX_ROLE8_STATE_ID() == enumROLE248_STATE.Aceptado);
                        var Disable_Parcial = e.data.ANX_OPENED() == 0 && !SaltarOpened;
                        $('<i/>').addClass(e.column.icon)
                        .prop('title', e.column.caption + ' documento')
                        .css('cursor', (Disable_Real ? 'default' : (Disable_Parcial ? 'default' : 'pointer')))
                        .css('color', Disable_Real ? 'Black' : e.column.color)
                        .css('opacity', (Disable_Real ? 0.3 : (Disable_Parcial ? 0.3 : 1)))
                        .appendTo(container);
                        break;
                }
                break;            
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            switch (e.element[0].id) {
                case 'GridTecnico':
                    switch (e.column.name) {
                        case 'Ver':
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TANX&FUID=' + e.data.ANX_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                            e.data.ANX_OPENED(1);
                            break;
                        case 'Rechazar':
                        case 'Requerir':
                        case 'Aprobar':
                            if ((e.data.ANX_OPENED() == 0 && !SaltarOpened) || TecnicoDisable() || e.data.ANX_ROLE4_STATE_ID() == 1) {
                                e.event.stopPropagation();
                                break;
                            }
                            MyPopUp.UID(e.data.ANX_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.ANX_TYPE_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data.ANX_ROLE1_STATE_ID() == e.column.ID)
                                MyPopUp.COMMENT(e.data.ANX_ROLE1_COMMENT());

                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Tecnico';
                            MyPopUp.Show(true);
                            break;

                        case 'Devolver':
                        case 'Aceptar':
                            if ((e.data.ANX_OPENED() == 0 && !SaltarOpened) || TecnicoDisable()) {
                                e.event.stopPropagation();
                                break;
                            }

                            if (ROLE < enumROLES.Subdirector && e.data[GetAdminStateID(GetNextRole(ROLE))]() == enumROLE248_STATE.Aceptado) {
                                e.event.stopPropagation();
                                break;
                            }
                            //Cuando acepta para que no aparezca el popup, quitar este bloque para que si aparezca el popup
                            //if (e.column.ID == enumROLE248_STATE.Aceptado) {
                            //    e.data.ANX_ROLEX_CHANGED(true);
                            //    e.data[GetAdminComment(ROLE)]('');
                            //    e.data[GetAdminStateID(ROLE)](e.column.ID);
                            //    e.data.ANX_STATE_ID.valueHasMutated();
                            //    break;
                            //}
                            MyPopUp.UID(e.data.ANX_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.ANX_TYPE_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data[GetAdminStateID(ROLE)]() == e.column.ID)
                                MyPopUp.COMMENT(e.data[GetAdminComment(ROLE)]());
                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Tecnico';
                            MyPopUp.Show(true);
                            break;
                    }
                    break;
                                    
                case 'GridResolucion':
                    switch (e.column.name) {
                        case 'Editar':
                            if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Resolucion) || (ROLE != CURRENT.ROLE())) {
                                e.event.stopPropagation();
                                break;
                            }
                            SGEWeb.app.CallBackFunction = CallbackResolucion;
                            SGEWeb.app.Solicitud = JSON.stringify(Solicitud);
                            SGEWeb.app.navigate({ view: "RDSResoluciones", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                            break;

                        case 'Devolver':
                        case 'Aceptar':
                        case 'Revision':
                            if (ResolucionDisable()) {
                                e.event.stopPropagation();
                                break;
                            }
                            if (e.column.ID.In([enumROLE248_STATE.Aceptado, enumROLE248_STATE.Revision])) {
                                e.data.RES_ROLEX_CHANGED(true);
                                e.data[GetResComment(ROLE)]('');
                                e.data[GetResStateID(ROLE)](e.column.ID);
                                e.data.RES_STATE_ID.valueHasMutated();
                                break;
                            }
                            MyPopUp.UID(e.data.SOL_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.RES_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data[GetResStateID(ROLE)]() == e.column.ID)
                                MyPopUp.COMMENT(e.data[GetResComment(ROLE)]());

                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Resolucion';
                            MyPopUp.Show(true);
                            break;
                    }
                    break;
            }
        }
    }

    function GuardarTramite() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Guardando trámite...");
        loadingVisible(true);
        GuardarDisabled(true);
        Solicitud.TechAnexos = ko.toJS(Tecnico);
        SGEWeb.app.NeedRefresh = true;
        GuardarOtorgaTramiteTecnico(Solicitud, false, PrepararFiles());
    }

    function ContinuarTramite() {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Tecnico:
                switch (ROLE) {
                    case enumROLES.Funcionario:                       
                        if (AnalisisTecnicoCompleto() == false) {
                            DevExpress.ui.notify('Análisis técnico incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.TechAnexos = ko.toJS(Tecnico);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarOtorgaTramiteTecnico(Solicitud, true, PrepararFiles());
                            }
                        });
                        break;

                    case enumROLES.Revisor:
                        if (AnalisisTecnicoCompleto() == false) {
                            DevExpress.ui.notify('Análisis técnico incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.TechAnexos = ko.toJS(Tecnico);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarOtorgaTramiteTecnico(Solicitud, true, PrepararFiles());
                            }
                        });
                        break;

                    case enumROLES.Coordinador:
                        if (AnalisisTecnicoCompleto() == false) {
                            DevExpress.ui.notify('Análisis técnico incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.TechAnexos = ko.toJS(Tecnico);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarOtorgaTramiteTecnico(Solicitud, true, PrepararFiles());
                            }
                        });
                        break;

                    case enumROLES.Subdirector:
                        if (AnalisisTecnicoCompleto() == false) {
                            DevExpress.ui.notify('Análisis técnico incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }

                        var Devolver = KoListObjectExists(Tecnico(), "ANX_ROLE8_STATE_ID", enumROLE248_STATE.Devuelto);

                        if (!Devolver) {
                            if (SGEWeb.app.User.USR_HAS_SIGN == 0) {
                                DevExpress.ui.notify('El usuario no tiene firma digital registrada en el sistema. En administración de usuarios puede cargar la firma.', "warning", 5000);
                                return;
                            }
                        }

                        if (Solicitud.Expediente.SERV_STATUS.toLowerCase().In(['ect', 'even', 'eres'])) {
                            DevExpress.ui.notify('El expediente se encuentra en un estado no válido para continuar con la solicitud. Favor actualizar en Manager para continuar.', "warning", 5000);
                            return;
                        }

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {

                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.TechAnexos = ko.toJS(Tecnico);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarOtorgaTramiteTecnico(Solicitud, true, PrepararFiles());
                            }
                        });
                        break;
                }
                break;
        }
    }

   function OnGuardarComentario() {
        switch (MyPopUp.Who) {
            case 'Tecnico':
                {
                    if (((ROLE == enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE1_STATE.Aprobado) || (ROLE != enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE248_STATE.Aceptado)) && (MyPopUp.COMMENT() == null || MyPopUp.COMMENT() == undefined || MyPopUp.COMMENT().length < 10)) {
                        DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                        return;
                    }

                    var TecnicoItem = KoListObjectFindGetElement(Tecnico(), 'ANX_UID', MyPopUp.UID());

                    TecnicoItem.ANX_ROLEX_CHANGED(true);

                    switch (ROLE) {
                        case enumROLES.Funcionario:
                            TecnicoItem.ANX_ROLE1_COMMENT(MyPopUp.COMMENT());
                            TecnicoItem.ANX_ROLE1_STATE_ID(MyPopUp.STATE_ID());
                            TecnicoItem.ANX_STATE_ID.valueHasMutated();
                            if (MyPopUp.STATE_ID() == enumROLE1_STATE.Aprobado) {
                                Tecnico().forEach(function (entry) {
                                    if (entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.Aprobado || entry.ANX_UID() == TecnicoItem.ANX_UID()) {
                                        FileCuadro.VISIBLE(true);
                                    } else {
                                        FileCuadro.VISIBLE(false);
                                    }
                                });
                            } else {
                                FileCuadro.VISIBLE(false);
                            }                            
                            break;
                        case enumROLES.Revisor:
                            TecnicoItem.ANX_ROLE2_COMMENT(MyPopUp.COMMENT());
                            TecnicoItem.ANX_ROLE2_STATE_ID(MyPopUp.STATE_ID());
                            TecnicoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                        case enumROLES.Coordinador:
                            TecnicoItem.ANX_ROLE4_COMMENT(MyPopUp.COMMENT());
                            TecnicoItem.ANX_ROLE4_STATE_ID(MyPopUp.STATE_ID());
                            TecnicoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                        case enumROLES.Subdirector:
                            TecnicoItem.ANX_ROLE8_COMMENT(MyPopUp.COMMENT());
                            TecnicoItem.ANX_ROLE8_STATE_ID(MyPopUp.STATE_ID());
                            TecnicoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                    }
                    MyPopUp.Show(false);
                }
                break;           
        }

    }

     
    function OnOpenComunicadoTecnico(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=COM&FUID=' + e.itemData.COM_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function GridInitialized(e) {
        GridResolucionInstance = e.component;
    }

    function CallbackResolucion(ResolucionParameter) {
        Solicitud.Resolucion = ResolucionParameter;

        DocumentoResolucion()[0].RES_NAME(Solicitud.Resolucion.RES_NAME + '.pdf');
        DocumentoResolucion()[0].RES_IS_CREATED(Solicitud.Resolucion.RES_IS_CREATED);

        Resolucion()[0].RES_CREATED_DATE(Solicitud.Resolucion.RES_CREATED_DATE);
        Resolucion()[0].RES_MODIFIED_DATE(Solicitud.Resolucion.RES_MODIFIED_DATE);

        if (GridResolucionInstance != null) {
            GridResolucionInstance.repaint();
        }
    }

    if (Solicitud.Resolucion.RES_IS_CREATED) {
        GetResolucion(Solicitud.SOL_UID);
    }

    var PreSelectedTabIndex = PreSelectedTab();

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            if (FileObject.VALIDATE_NAME == true) {
                if (!file.name.endsWith(' - ' + Solicitud.Expediente.SERV_NUMBER + '.pdf')) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }
                var suffix = file.name.indexOf(' - ' + Solicitud.Expediente.SERV_NUMBER + '.pdf');
                if (suffix > 10 || suffix < 1) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }

                if (!isNormalInteger(file.name.substr(0, suffix))) {
                    DevExpress.ui.notify('El nombre del archivo debe tener la forma "Número del CT – Número del expediente"', "warning", 5000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
                FileObject.CHANGED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }

    function OpenFile(data, e) {

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TEC&FUID=' + data.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");

    }

    function PrepararFiles() {
        var Files = [];
        if (FileSoporte.LOADED())
            Files.push({ TYPE_ID: FileSoporte.TYPE_ID, NAME: FileSoporte.NAME(), CONTENT_TYPE: FileSoporte.CONTENT_TYPE_LOCAL(), DATA: FileSoporte.DATA });
        if (FileConcepto.LOADED())
            Files.push({ TYPE_ID: FileConcepto.TYPE_ID, NAME: FileConcepto.NAME(), CONTENT_TYPE: FileConcepto.CONTENT_TYPE_LOCAL(), DATA: FileConcepto.DATA });
        if (FileCuadro.LOADED())
            Files.push({ TYPE_ID: FileCuadro.TYPE_ID, NAME: FileCuadro.NAME(), CONTENT_TYPE: FileCuadro.CONTENT_TYPE_LOCAL(), DATA: FileCuadro.DATA });

        return Files;
    }

    function EvaluateIcon(FileObject) {
        var Icon = '';

        if ((ROLE & CURRENT.ROLE() & enumROLES.Funcionario) == 0)
            return 'ta ta-empty';

        if (FileObject.CHANGED()) Icon = 'ta ta-chulo';
        else Icon = 'ta ta-empty';
        return Icon;
    }

    function OnOpenTechDoc(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TEC&FUID=' + e.itemData.TECH_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function AprobarTodo() {
        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea aceptar todos los documentos?'), SGEWeb.app.Name);
        result.done(function (dialogResult) {
            if (dialogResult) {
                var gridTecnico = $("#GridTecnico").dxDataGrid('instance');
                var i = gridTecnico.totalCount();
                if (i > 0) {
                    var DatosTec = gridTecnico.getDataSource();
                    DatosTec._items.forEach(function (entry) {
                        entry.ANX_ROLEX_CHANGED(true);
                        entry.ANX_ROLE8_STATE_ID(enumROLE248_STATE.Aceptado);
                        entry.ANX_STATE_ID.valueHasMutated();
                    });
                }
            }
        });
        
    }

    var viewModel = {
        OnGuardarComentario: OnGuardarComentario,
        MyPopUp: MyPopUp,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        Solicitud: Solicitud,
        //Administrativo: Administrativo,
        Tecnico: Tecnico,
        Resolucion: Resolucion,
        GuardarTramite: GuardarTramite,
        ContinuarTramite: ContinuarTramite,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        ROLE: ROLE,
        CURRENT: CURRENT,
        //AdministrativoDisable: AdministrativoDisable,
        TecnicoDisable :TecnicoDisable,
        ComunicadoTecnico: ComunicadoTecnico,
        OnOpenComunicadoTecnico: OnOpenComunicadoTecnico,
        GridInitialized: GridInitialized,
        PreSelectedTabIndex: PreSelectedTabIndex,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        USR_ADMIN: USR_ADMIN,
        USR_TECH: USR_TECH,
        FileSoporte: FileSoporte,
        FileConcepto: FileConcepto,
        FileCuadro: FileCuadro,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CanEdit: CanEdit,
        EvaluateIcon: EvaluateIcon,
        TechDocsList: TechDocsList,
        OnOpenTechDoc: OnOpenTechDoc,
        AprobarTodo: AprobarTodo,
    };

    return viewModel;
};