﻿SGEWeb.RDSExpedientes = function (params) {
    "use strict";
    var title = JSON.parse(params.settings.substring(5)).title;
    var Operador = JSON.parse(params.settings.substring(5)).Operador;
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var ShowPopUp = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var CRUDType = -1; //1: Nuevo, 3: Edit
    var CRUDTitle = ko.observable('');
    var Cantidades = ko.observable(0);
    var ShowPopUpSolicitud = ko.observable(false);
    var SelectedSolicitud = ko.observable(-1);
    var UserCC = ko.observable('');
    var Expediente = { SERV_ID: -1, SERV_NUMBER:-1, SERV_NAME: undefined };
    var UserSelected = ko.observable({ CONT_ID: -1, CONT_NAME: '', CONT_NUMBER: '', CONTT_TITLE: '', CONT_TEL: '' });
    var NullValue='No disponible';
    var TiposSolicitudesPosibles = ko.observableArray([]);
     
    var EmailContacto1 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: 'required', message: 'Email requerido' },
                              { type: "email", message: "Email inválido" }]
        }
    });
    var EmailContacto2 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;
    var EmailContacto3 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;

    var CrearSolicitudDisable = ko.computed(function () {
        
        if (UserSelected().CONT_ID != -1 && SelectedSolicitud() != -1
            && EmailContacto1.dxValidator.validate().isValid
            && EmailContacto2.dxValidator.validate().isValid
            && EmailContacto3.dxValidator.validate().isValid
            )
            return false;
        return true;
    });

    function GetExpedientes(Nit) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetExpedientes",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Nit:Nit
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetExpedientesResult;
                MyDataSource(result.Expedientes);

                TiposSolicitudesPosibles(result.SOL_TYPES);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function onCellTemplate(container, options) {
        if (options.columnIndex == 1) {
            $('<i/>').addClass('ta ta-editar ta-lg')
            .prop('title', 'Nueva solicitud')
            .css('cursor', 'pointer')
            .appendTo(container);
        }
    }

    function onContentReady() {
        var grid = $("#GridRDSExpedientes").dxDataGrid('instance');
        if (grid != undefined) {
            Cantidades(Globalizeformat(grid.totalCount(), "n0"));
            SGEWeb.app.DisabledToolBar(false);
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSExpedientes') {
                if (e.columnIndex == 1) {

                    Expediente = e.data;

                    SelectedSolicitud(-1);
                    //UserCC('1020773512');
                    UserCC('');

                    UserSelected({ CONT_ID: -1, CONT_NAME: '', CONT_NUMBER: '', CONT_TITLE: '', CONT_TEL: '' });
                    
                    EmailContacto1.dxValidator.reset();
                    EmailContacto1(''); EmailContacto2(''); EmailContacto3('');
                    ShowPopUpSolicitud(true);
                }

            } 
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetExpedientes(SGEWeb.app.Nit);
    }

    function Excel() {
        var grid = $("#GridRDSExpedientes").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function MyDetail(_, options) {
        
        return $("<div>").dxTabPanel({
            items: [{
                title: "Cubrimiento",
                icon: 'ta ta-antena1 ta-lg',
                template: CreateCubrimientoTemplate(options.data.RedesCubrimiento),
                disabled: options.data.RedesCubrimiento.length == 0
            }, {
                title: "Punto a punto",
                icon: 'ta ta-tower ta-lg',
                template: CreatePuntoPuntoTemplate(options.data.RedesPPCubrimiento),
                disabled: options.data.RedesPPCubrimiento.length == 0
            }, {
                title: "Transmoviles",
                icon: 'ta ta-transmovil ta-lg',
                template: CreateTranmovilTemplate(options.data.RedesTranmoviles),
                disabled: options.data.RedesTranmoviles.length == 0
            }, {
                title: "Punto a punto Transmoviles",
                icon: 'ta ta-tower ta-lg',
                template: CreatePuntoPuntoTemplate(options.data.RedesPPTranmoviles),
                disabled: options.data.RedesPPTranmoviles.length == 0
            }],
            
            onSelectionChanged: function (e) {
                //options.component.refresh();
               // options.component.updateDimensions();
                //options.component.repaintRows(options.rowIndex);
            }
        });
    }

    function CreateCubrimientoTemplate(data) {

        var Cadena = '<table style="margin-left:65px;" border=1>';
        
        Cadena += '<tr>';
        Cadena += '<td>';

        data.forEach(function (entry) {
            Cadena += '</BR>';
            Cadena += '<div style="text-align:center; margin-bottom:10px;"><b>CUBRIMIENTO - RED ' + IsNull(entry.LIC_NUMBER, NullValue) + '</b></div>'
            Cadena += '<div style="width:300px;"><b>Acimut&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.AZIMUTH) ? NullValue : entry.AZIMUTH + '°') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Altura sitio&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.ASL) ? NullValue : Globalizeformat(entry.ASL, "n0") + 'm') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Altura torre&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.AGL) ? NullValue : Globalizeformat(entry.AGL, "n0") + 'm') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Clase estación&nbsp; </b><span style="float:right;">' + IsNull(entry.STATION_CLASS, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Emisión&nbsp; </b><span style="float:right;">' + IsNull(entry.DESIG_EMISSION, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Modulación&nbsp; </b><span style="float:right;">' + IsNull(entry.MODULATION, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Ganancia&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.GAIN) ? NullValue : entry.GAIN + 'dB') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Origen&nbsp; </b><span style="float:right;">' + IsNull(entry.ORIGIN, NullValue) + '</span></div>';

            Cadena += '<div style="width:300px;"><b>Norte&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.NORTH) ? NullValue : Globalizeformat(entry.NORTH, "n0") + 'mN') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Este&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.EAST) ? NullValue : Globalizeformat(entry.EAST, "n0") + 'mE') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Latitud&nbsp; </b><span style="float:right;">' + IsNull(entry.LATITUDEGSM, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Longitud&nbsp; </b><span style="float:right;">' + IsNull(entry.LONGITUDEGSM, NullValue) + '</span></div>';

            Cadena += '<div style="width:300px;"><b>Perdidas&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.LOSSES) ? NullValue : entry.LOSSES + 'dB') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Potencia operación&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.POWER) ? NullValue : entry.POWER + 'dBW') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Potencia antena&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.PWR_ANT) ? NullValue : entry.PWR_ANT + 'dBW')  + '</span></div>';

        });

        Cadena += '</BR>';
        Cadena += '</td>';
        Cadena += '</tr>';
        Cadena += '</table>'

        return $("<div>").html(Cadena);
    }

    function CreatePuntoPuntoTemplate(data) {
        var Cadena = '<table style="margin-left:65px;" border=1>';

        Cadena += '<tr>';
        Cadena += '</BR>';

        var contador = 0;
        data.forEach(function (entry) {
            if(contador++==0)
                Cadena += '<td>';
            else
                Cadena += '<td style="padding-left:100px;">';

            Cadena += '<div style="text-align:center; margin-bottom:10px;"><b>PARTE A - RED ' + IsNull(entry.LIC_NUMBER, NullValue) + ' </b></div>'
            Cadena += '<div style="width:300px;"><b>Departamento&nbsp; </b><span style="float:right;">' +  IsNull(entry.DEPTO_A, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Ciudad&nbsp; </b><span style="float:right;">' +  IsNull(entry.CITY_A, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Dirección&nbsp; </b><span style="float:right;" title="' + IsNull(entry.ADDRESS_A, NullValue) + '">' + Limit(IsNull(entry.ADDRESS_A, NullValue), 30) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Latitud&nbsp; </b><span style="float:right;">' +  IsNull(entry.LATITUDEGSM_A, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Longitud&nbsp; </b><span style="float:right;">' + IsNull(entry.LONGITUDEGSM_A, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Acimut&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.AZIMUTH_A) ? NullValue : entry.AZIMUTH_A + '°')  + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Altura torre&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.AGL_A) ? NullValue : Globalizeformat(entry.AGL_A, "n0") + 'm') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Tilt&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.TILT_A) ? NullValue : entry.TILT_A + '°') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Frecuencia&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.FREQUENCY_A) ? NullValue : entry.FREQUENCY_A + 'MHz')  + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Polarización&nbsp; </b><span style="float:right;">' +  IsNull(entry.POLAR_A, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Ganancia&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.GAIN_A) ? NullValue : entry.GAIN_A + 'dB') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Potencia&nbsp; </b><span style="float:right;">' +  (IsNullV2(entry.POWER_A) ? NullValue : entry.POWER_A + 'dBW')  + '</span></div>';
            Cadena += '</BR>'
            Cadena += '</td>';

            Cadena += '<td style="padding-left:100px;vertical-align:top;">';
            Cadena += '<div style="text-align:center; margin-bottom:10px;"><b>PARTE B - RED ' + IsNull(entry.LIC_NUMBER, NullValue) + ' </b></div>'
            Cadena += '<div style="width:300px;"><b>Departamento&nbsp; </b><span style="float:right;">' +  IsNull(entry.DEPTO_B, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Ciudad&nbsp; </b><span style="float:right;">' +  IsNull(entry.CITY_B, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Dirección&nbsp; </b><span style="float:right;" title="' + IsNull(entry.ADDRESS_B, NullValue) + '">' + Limit(IsNull(entry.ADDRESS_B, NullValue), 30) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Latitud&nbsp; </b><span style="float:right;">' + IsNull(entry.LATITUDEGSM_B, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Longitud&nbsp; </b><span style="float:right;">' + IsNull(entry.LONGITUDEGSM_B, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Acimut&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.AZIMUTH_B) ? NullValue : entry.AZIMUTH_B + '°') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Altura torre&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.AGL_B) ? NullValue : Globalizeformat(entry.AGL_B, "n0") + 'm') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Tilt&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.TILT_B) ? NullValue : entry.TILT_B + '°') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Frecuencia&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.FREQUENCY_B) ? NullValue : entry.FREQUENCY_B + 'MHz') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Polarización&nbsp; </b><span style="float:right;">' + IsNull(entry.POLAR_B, NullValue) + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Ganancia&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.GAIN_B) ? NullValue : entry.GAIN_B + 'dB') + '</span></div>';
            Cadena += '<div style="width:300px;"><b>Potencia&nbsp; </b><span style="float:right;">' + (IsNullV2(entry.POWER_B) ? NullValue : entry.POWER_B + 'dBW') + '</span></div>';

            Cadena += '</BR>'
            Cadena += '</td>';

        });

        Cadena += '</tr>';
        Cadena += '</table>'

        return $("<div>").html(Cadena);
    }

    function CreateTranmovilTemplate(data) {
        return function () {
            return $("<div>").dxDataGrid(
            {
                columnFixing: { enabled: false },
                loadPanel: { enabled: true, text: 'Cargando' },
                noDataText: 'No hay datos',
                dataSource: data,
                columns: [{ dataField: 'LIC_NUMBER', caption: 'Red', dataType: 'number' },
                          { dataField: 'CATEGORY', caption: 'Categoria', dataType: 'string' },
                          { dataField: 'AGL', caption: 'Altura antena', dataType: 'number' },
                          { dataField: 'BW', caption: 'BW', dataType: 'number' },
                          { dataField: 'AZIMUTH', caption: 'Azimut', dataType: 'number' },
                          { dataField: 'ADDRESS', caption: 'Dirección', dataType: 'string' },
                          { dataField: 'DESIG_EMISSION', caption: 'Emisión', dataType: 'string' },
                          { dataField: 'TX_LOW_FREQ', caption: 'Freq Tx baja', dataType: 'number' },
                          { dataField: 'TX_HIGH_FREQ', caption: 'Freq Tx alta', dataType: 'number' },
                          { dataField: 'RX_LOW_FREQ', caption: 'Freq Rx baja', dataType: 'number' },
                          { dataField: 'RX_HIGH_FREQ', caption: 'Freq Rx alta', dataType: 'number' },
                          { dataField: 'GAIN', caption: 'Ganancia', dataType: 'number' },
                          { dataField: 'LATITUDE', caption: 'Latitud', dataType: 'number' },
                          { dataField: 'LONGITUDE', caption: 'Longitud', dataType: 'number' },
                          { dataField: 'POWER', caption: 'Potencia', dataType: 'number' },
                          { dataField: 'PWR_ANT', caption: 'Potencia antena', dataType: 'number' }
                ],
                showBorders: true,
                sorting: { mode: 'multiple' },
                pager: { visible: true },
                paging: { pageSize: 30 },
                remoteOperations: false,
                scrolling: { mode: 'standard' },
                width: '100%', height: '100%',
                allowColumnResizing: true,
                allowSorting: true,
                allowFiltering: true,
                filterRow: { visible: false },
                headerFilter: { visible: false },
                selection: { mode: 'single' },
                columnAutoWidth: true,
                rowAlternationEnabled: true
            });
        };

    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function popupHidden(e) {


    }

    function popupShowing(e) {
      //  $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function BuscarUsuario() {

        var Portafolio = ListObjectFindGetElement(MyDataSource(), 'SERV_ID', Expediente.SERV_ID);

        var Contacto = ListObjectFindGetElement(Portafolio.Contactos, 'CONT_NUMBER', UserCC());
        if (Contacto != null) {
            UserSelected(Contacto);
        } else {
            DevExpress.ui.notify('El número de identificación no está registrado para realizar este trámite. Favor corregir e intentar de nuevo.', "warning", 3000);
        }

    }
    
    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }

    function CrearSolicitud() {
        
        var Emails = [Operador.CUS_EMAIL, EmailContacto1(), EmailContacto2(), EmailContacto3()];

        if (SelectedSolicitud() == -1) {
            DevExpress.ui.notify('Favor seleccionar un tipo de solicitud.', "warning", 5000);
        }
        else if (SelectedSolicitud() == 2) {
            ShowPopUpSolicitud(false);
            SGEWeb.app.Expediente = JSON.stringify(Expediente);
            SGEWeb.app.navigate({ view: "RDSSolicitudJuntaProgramacion", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: SelectedSolicitud(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else if (SelectedSolicitud() == 7) {
            ShowPopUpSolicitud(false);
            SGEWeb.app.Expediente = JSON.stringify(Expediente);
            SGEWeb.app.navigate({ view: "RDSSolicitudProrroga", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: SelectedSolicitud(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else {
            DevExpress.ui.notify('Tipo de solicitud no implementada.', "warning", 3000);
        }

    }

    function handleViewShown(e) {
        if (e.direction == 'backward') {
            //SGEWeb.app.navigationManager.back();
        } 
    }

    Refrescar();

    var viewModel = {
        title:title,
        viewShown: handleViewShown,
        MyDataSource: MyDataSource,
        loadingVisible: loadingVisible,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        Refrescar: Refrescar,
        Excel: Excel,
        Cantidades: Cantidades,
        MyDetail: MyDetail,

        ShowPopUpSolicitud: ShowPopUpSolicitud,
        SelectedSolicitud: SelectedSolicitud,
        onPwdKeyDown: onPwdKeyDown,
        popupHidden: popupHidden,
        popupShowing: popupShowing,
        UserCC: UserCC,
        BuscarUsuario: BuscarUsuario,
        keyPressAction: keyPressAction,
        UserSelected: UserSelected,
        Operador: Operador,
        EmailContacto1: EmailContacto1,
        EmailContacto2: EmailContacto2,
        EmailContacto3: EmailContacto3,
        CrearSolicitudDisable: CrearSolicitudDisable,
        CrearSolicitud: CrearSolicitud,
        TiposSolicitudesPosibles: TiposSolicitudesPosibles,
    };

    return viewModel;
};