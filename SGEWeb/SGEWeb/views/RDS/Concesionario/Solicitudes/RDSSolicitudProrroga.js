﻿SGEWeb.RDSSolicitudProrroga = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var Expediente = JSON.parse(SGEWeb.app.Expediente);
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var GuardarDisabled = ko.observable(false);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }

    var BotonCancelar = [
     { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
     { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
     { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
     { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la subsanación?' },
     { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
     { CRUD: 'Dummy', Text: 'Crear solicitud', Icon: 'floppy' },
     { CRUD: enumCRUD.Create, Text: 'Crear solicitud', Icon: 'floppy', Method: CrearSolicitud },
     { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
     { CRUD: enumCRUD.Upate, Text: 'Subsanar solicitud', Icon: 'floppy', Method: SubsanarSolicitud },
     { CRUD: enumCRUD.Delete, Text: 'Cancelar solicitud', Icon: 'floppy' },
    ];

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    var FileCartaProrroga = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: 15,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 30,
        MAX_NAME_LENGTH: 50
    };

    if (CRUD.In([enumCRUD.Update, enumCRUD.Read])) {

        Solicitud = JSON.parse(SGEWeb.app.Solicitud);
        SOL_ENDED = Solicitud.SOL_ENDED;

        var Prorroga = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', FileCartaProrroga.TYPE_ID);
        FileCartaProrroga.UID = Prorroga.ANX_UID;
        FileCartaProrroga.STATE_ID = Prorroga.ANX_STATE_ID;
        FileCartaProrroga.CONTENT_TYPE(Prorroga.ANX_CONTENT_TYPE),
        FileCartaProrroga.NAME(Prorroga.ANX_NAME);
        FileCartaProrroga.COMMENT = Prorroga.ANX_COMMENT;
        if (FileCartaProrroga.STATE_ID != enumROLE1_STATE.Requerido)
            FileCartaProrroga.LOADED(true);

    }

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerdio: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var CrearSolicitudDisable = ko.computed(function () {
        if (FileCartaProrroga.LOADED() && !GuardarDisabled())
            return false;
        return true;
    });

    function CrearConcesionarioSolicitud(SOL_TYPE, Expediente, Contacto, Emails, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearConcesionarioSolicitud",
            data: JSON.stringify({
                SOL_TYPE: SOL_TYPE,
                Expediente: Expediente,
                Contacto: Contacto,
                Emails: Emails,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function SubsanarConcesionarioSolicitud(Solicitud, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }

    function OpenFile(data, e) {

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + data.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");

    }

    function CrearSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Creando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];

        Files.push({ TYPE_ID: FileCartaProrroga.TYPE_ID, NAME: FileCartaProrroga.NAME(), CONTENT_TYPE: FileCartaProrroga.CONTENT_TYPE_LOCAL(), DATA: FileCartaProrroga.DATA });

        var SOL_TYPE = ListObjectFindGetElement(SGEWeb.app.SOL_TYPES, 'SOL_TYPE_ID', SOL_TYPE_ID);

        CrearConcesionarioSolicitud(SOL_TYPE, Expediente, Contacto, Emails, Files);
    }

    function SubsanarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Subsanando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        if (FileCartaProrroga.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileCartaProrroga.UID, TYPE_ID: FileCartaProrroga.TYPE_ID, NAME: FileCartaProrroga.NAME(), CONTENT_TYPE: FileCartaProrroga.CONTENT_TYPE_LOCAL(), DATA: FileCartaProrroga.DATA });

        SubsanarConcesionarioSolicitud(Solicitud, Files);
    }

    function CancelarSolicitud() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function MostrarAlertaVencimiento() {

        var Message = CrearDialogHtml('ta ta-info', 'Recuerde que debe solicitar la <B>Prórroga de Concesión</B> con mínimo seis (6) meses de anticipación al vencimiento del término inicial.', 'width:400px;');
        DevExpress.ui.dialog.alert(Message, SGEWeb.app.Name);
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    if(CRUD==enumCRUD.Create)
        MostrarAlertaVencimiento();

    var viewModel = {
        Expediente: Expediente,
        Contacto: Contacto,
        FileCartaProrroga: FileCartaProrroga,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CrearSolicitud: CrearSolicitud,
        CancelarSolicitud: CancelarSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility

    };


    return viewModel;
};