﻿SGEWeb.RDSSolicitudOtorgaTecnico = function (params) {
    "use strict";

    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var GuardarDisabled = ko.observable(false);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ListaFiles = ko.observableArray([]);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var Expediente = ko.observable(undefined);

    var BotonCancelar = [
         { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
         { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
         { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
         { CRUD: enumCRUD.Update, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la subsanación?' },
         { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
         { CRUD: 'Dummy', Text: 'Crear solicitud', Icon: 'floppy' },
         { CRUD: enumCRUD.Create, Text: 'Crear solicitud', Icon: 'floppy' },
         { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
         { CRUD: enumCRUD.Update, Text: 'Subsanar solicitud', Icon: 'floppy', Method: SubsanarSolicitud },
         { CRUD: enumCRUD.Delete, Text: 'Cancelar solicitud', Icon: 'floppy' },
    ];

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    var FileEstudioTecnico = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaEstudioTecnico,
        EXTENSIONS: ['.pdf'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };
    var FileAeronautica = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaConceptoAeronautica,
        //TYPE_ID: 64,
        EXTENSIONS: ['.pdf'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };
    var FilePlaneacionMun = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaPlaneacionMunicipal,
        //TYPE_ID: 65,
        EXTENSIONS: ['.pdf'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };
    var FileAnexoTecnico = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaAnexoTecnico,
        //TYPE_ID: 66,
        EXTENSIONS: ['.xlsx', '.xlsm'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };
    var FileManualEstilos = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaManualEstilo,
        //TYPE_ID: 67,
        EXTENSIONS: ['.pdf'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };
    var FileJuntaProgramacion = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: enumANEX_TYPE.OtorgaJuntaProgramacion,
        //TYPE_ID: 68,
        EXTENSIONS: ['.pdf'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 20,
        MAX_NAME_LENGTH: 50
    };

    if (CRUD.In([enumCRUD.Update, enumCRUD.Read])) {

        Solicitud = JSON.parse(SGEWeb.app.Solicitud);
        SOL_ENDED = Solicitud.SOL_ENDED;
        Expediente(Solicitud.Expediente);

        if (Solicitud.TechAnexos.length > 0) {
            var EstudioTecnico = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileEstudioTecnico.TYPE_ID);
            FileEstudioTecnico.UID = EstudioTecnico.ANX_UID;
            FileEstudioTecnico.STATE_ID = EstudioTecnico.ANX_STATE_ID;
            FileEstudioTecnico.CONTENT_TYPE(EstudioTecnico.ANX_CONTENT_TYPE),
            FileEstudioTecnico.NAME(EstudioTecnico.ANX_NAME);
            FileEstudioTecnico.COMMENT = EstudioTecnico.ANX_COMMENT;
            if (FileEstudioTecnico.STATE_ID != enumROLE1_STATE.Requerido)
                FileEstudioTecnico.LOADED(true);

            var Aeronautica = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileAeronautica.TYPE_ID);
            FileAeronautica.UID = Aeronautica.ANX_UID;
            FileAeronautica.STATE_ID = Aeronautica.ANX_STATE_ID;
            FileAeronautica.CONTENT_TYPE(Aeronautica.ANX_CONTENT_TYPE),
            FileAeronautica.NAME(Aeronautica.ANX_NAME);
            FileAeronautica.COMMENT = Aeronautica.ANX_COMMENT;
            if (FileAeronautica.STATE_ID != enumROLE1_STATE.Requerido)
                FileAeronautica.LOADED(true);

            if (ListObjectExists(Solicitud.TechAnexos, 'ANX_TYPE_ID', FilePlaneacionMun.TYPE_ID)) {
                var PlaneacionMun = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FilePlaneacionMun.TYPE_ID);
                FilePlaneacionMun.UID = PlaneacionMun.ANX_UID;
                FilePlaneacionMun.STATE_ID = PlaneacionMun.ANX_STATE_ID;
                FilePlaneacionMun.CONTENT_TYPE(PlaneacionMun.ANX_CONTENT_TYPE),
                FilePlaneacionMun.NAME(PlaneacionMun.ANX_NAME);
                FilePlaneacionMun.COMMENT = PlaneacionMun.ANX_COMMENT;
                if (FilePlaneacionMun.STATE_ID != enumROLE1_STATE.Requerido)
                    FilePlaneacionMun.LOADED(true);
            }

            var AnexoTecnico = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileAnexoTecnico.TYPE_ID);
            FileAnexoTecnico.UID = AnexoTecnico.ANX_UID;
            FileAnexoTecnico.STATE_ID = AnexoTecnico.ANX_STATE_ID;
            FileAnexoTecnico.CONTENT_TYPE(AnexoTecnico.ANX_CONTENT_TYPE),
            FileAnexoTecnico.NAME(AnexoTecnico.ANX_NAME);
            FileAnexoTecnico.COMMENT = AnexoTecnico.ANX_COMMENT;
            if (FileAnexoTecnico.STATE_ID != enumROLE1_STATE.Requerido)
                FileAnexoTecnico.LOADED(true);

            if (ListObjectExists(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileManualEstilos.TYPE_ID)) {
                var ManualEstilos = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileManualEstilos.TYPE_ID);
                FileManualEstilos.UID = ManualEstilos.ANX_UID;
                FileManualEstilos.STATE_ID = ManualEstilos.ANX_STATE_ID;
                FileManualEstilos.CONTENT_TYPE(ManualEstilos.ANX_CONTENT_TYPE),
                FileManualEstilos.NAME(ManualEstilos.ANX_NAME);
                FileManualEstilos.COMMENT = ManualEstilos.ANX_COMMENT;
                if (FileManualEstilos.STATE_ID != enumROLE1_STATE.Requerido)
                    FileManualEstilos.LOADED(true);
            }

            if (ListObjectExists(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileJuntaProgramacion.TYPE_ID)) {
                var JuntaProgramacion = ListObjectFindGetElement(Solicitud.TechAnexos, 'ANX_TYPE_ID', FileJuntaProgramacion.TYPE_ID);
                FileJuntaProgramacion.UID = JuntaProgramacion.ANX_UID;
                FileJuntaProgramacion.STATE_ID = JuntaProgramacion.ANX_STATE_ID;
                FileJuntaProgramacion.CONTENT_TYPE(JuntaProgramacion.ANX_CONTENT_TYPE),
                FileJuntaProgramacion.NAME(JuntaProgramacion.ANX_NAME);
                FileJuntaProgramacion.COMMENT = JuntaProgramacion.ANX_COMMENT;
                if (FileJuntaProgramacion.STATE_ID != enumROLE1_STATE.Requerido)
                    FileJuntaProgramacion.LOADED(true);
            }
        }
    }

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    var CrearSolicitudDisable = ko.computed(function () {
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComercial) {
            if (FileEstudioTecnico.LOADED() && FileAeronautica.LOADED() && FilePlaneacionMun.LOADED() && FileAnexoTecnico.LOADED() && !GuardarDisabled())
                return false;
        }
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
            if (FileEstudioTecnico.LOADED() && FileAeronautica.LOADED() && FilePlaneacionMun.LOADED() && FileManualEstilos.LOADED() && FileAnexoTecnico.LOADED() && !GuardarDisabled())
                return false;
        }
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitaria || SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos) {
            if (FileEstudioTecnico.LOADED() && FileAeronautica.LOADED() && FileJuntaProgramacion.LOADED() && FileAnexoTecnico.LOADED() && !GuardarDisabled())
                return false;
        }
        return true;
    });

    function GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDatosCreacionOtorgaEmisora",
            data: JSON.stringify({
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetDatosCreacionOtorgaEmisoraResult;
                if (CRUD.In([enumCRUD.Update, enumCRUD.Read])) {

                    Solicitud = JSON.parse(SGEWeb.app.Solicitud);
                    SOL_ENDED = Solicitud.SOL_ENDED;
                    Expediente(Solicitud.Expediente);
                }

                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }


    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerdio: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    function SubsanarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Subsanando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        if (FileEstudioTecnico.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileEstudioTecnico.UID, TYPE_ID: FileEstudioTecnico.TYPE_ID, NAME: FileEstudioTecnico.NAME(), CONTENT_TYPE: FileEstudioTecnico.CONTENT_TYPE_LOCAL(), DATA: FileEstudioTecnico.DATA });
        if (FileAeronautica.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileAeronautica.UID, TYPE_ID: FileAeronautica.TYPE_ID, NAME: FileAeronautica.NAME(), CONTENT_TYPE: FileAeronautica.CONTENT_TYPE_LOCAL(), DATA: FileAeronautica.DATA });
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComercial || SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
            if (FilePlaneacionMun.STATE_ID == enumROLE1_STATE.Requerido)
                Files.push({ UID: FilePlaneacionMun.UID, TYPE_ID: FilePlaneacionMun.TYPE_ID, NAME: FilePlaneacionMun.NAME(), CONTENT_TYPE: FilePlaneacionMun.CONTENT_TYPE_LOCAL(), DATA: FilePlaneacionMun.DATA });
        }
        if (FileAnexoTecnico.STATE_ID == enumROLE1_STATE.Requerido) {
            Files.push({ UID: FileAnexoTecnico.UID, TYPE_ID: FileAnexoTecnico.TYPE_ID, NAME: FileAnexoTecnico.NAME(), CONTENT_TYPE: FileAnexoTecnico.CONTENT_TYPE_LOCAL(), DATA: FileAnexoTecnico.DATA });
        }
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
            if (FileManualEstilos.STATE_ID == enumROLE1_STATE.Requerido)
                Files.push({ UID: FileManualEstilos.UID, TYPE_ID: FileManualEstilos.TYPE_ID, NAME: FileManualEstilos.NAME(), CONTENT_TYPE: FileManualEstilos.CONTENT_TYPE_LOCAL(), DATA: FileManualEstilos.DATA });
        }
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitaria || SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos) {
            if (FileJuntaProgramacion.STATE_ID == enumROLE1_STATE.Requerido)
                Files.push({ UID: FileJuntaProgramacion.UID, TYPE_ID: FileJuntaProgramacion.TYPE_ID, NAME: FileJuntaProgramacion.NAME(), CONTENT_TYPE: FileJuntaProgramacion.CONTENT_TYPE_LOCAL(), DATA: FileJuntaProgramacion.DATA });
        }

        SubsanarConcesionarioOtorgaTecnico(Solicitud, Files);
    }

    function SubsanarConcesionarioOtorgaTecnico(Solicitud, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioOtorgaTecnico",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioOtorgaTecnicoResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }
    function CancelarSolicitud() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }


    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'COMMENT':

                $('<i/>').addClass('ta ta-question-circle ta-2x')
                    .prop('title', 'Archivo(s) permitido(s) (' + e.data.EXTENSIONS.join() + ').')
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('float', 'left')
                    .css('color', '#6f91cb')
                    .appendTo(container);

                $('<i/>').addClass('ta-2x')
                    .addClass(ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'icon', ''))
                    .prop('title', 'Ver archivo')
                    .prop('visibility', (e.data.UID != null ? 'visible' : 'hidden'))
                    .bind('click', { FileObject: e.data }, OpenFile)
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('margin-right', '5px')
                    .css('cursor', 'pointer')
                    .css('float', 'left')
                    .css('color', ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'color', ''))
                    .appendTo(container);

                break;
            case 'Upload':
                var label = $('<label/>');
                label.addClass('custom-file-upload')
                    .prop('title', 'Seleccionar archivo')
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .css('position', 'relative')
                    .css('width', '400px')
                    .css('margin-top', '10px');
                label.disabled = false;

                var input = $('<input/>');
                input.prop('id', e.data.ID)
                    .prop('type', 'file')
                    .prop('accept', e.data.EXTENSIONS.join())
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .bind('change', { FileObject: e.data }, CargarFile)
                    .css('z-index', '999')
                    .css('position', 'absolute')
                    .css('top', '0px')
                    .css('visibility', 'hidden');
                input.appendTo(label);

                var info = $('<i/>');
                info.addClass('ta ta-attachment ta-lg')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(label);

                var span = $('<span/>');
                span.prop('textContent', e.data.NAME())
                    .appendTo(label);

                label.appendTo(container);
                break;

            case 'DESC':
                var div = $('<b/>');
                div.prop('textContent', e.data.DESC())
                    .css('vertical-align', 'middle')
                    .css('fontWeight', 'bold')
                    .css('word-wrap', 'break-word')
                    .appendTo(container);

                container.css('vertical-align', 'middle')
                    .css('word-wrap', 'break-word');
                break;
        }
    }

    function OpenFile(data, e) {

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TANX&FUID=' + data.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");

    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID);
    }

    Refrescar();

    var viewModel = {
        Contacto: Contacto,
        CRUD: CRUD,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        FileEstudioTecnico: FileEstudioTecnico,
        FileAeronautica: FileAeronautica,
        FilePlaneacionMun: FilePlaneacionMun,
        FileAnexoTecnico: FileAnexoTecnico,
        FileJuntaProgramacion: FileJuntaProgramacion,
        FileManualEstilos: FileManualEstilos,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        Expediente: Expediente,
        ListaFiles: ListaFiles,
        SOL_ENDED: SOL_ENDED,
        onCellTemplate: onCellTemplate,
        CancelarSolicitud: CancelarSolicitud,
        GuardarDisabled: GuardarDisabled,
        CrearSolicitudDisable: CrearSolicitudDisable,
        SOL_TYPE_ID: SOL_TYPE_ID,
        enumSOL_TYPES: enumSOL_TYPES,
    }


    return viewModel;
};