﻿SGEWeb.RDSSolicitudJuntaProgramacion = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var Expediente = JSON.parse(SGEWeb.app.Expediente);
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var GuardarDisabled = ko.observable(false);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }

    var BotonCancelar = [
         { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
         { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
         { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
         { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la subsanación?' },
         { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
     { CRUD: 'Dummy', Text: 'Crear solicitud', Icon: 'floppy' },
     { CRUD: enumCRUD.Create, Text: 'Crear solicitud', Icon: 'floppy', Method: CrearSolicitud },
     { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
     { CRUD: enumCRUD.Upate, Text: 'Subsanar solicitud', Icon: 'floppy', Method: SubsanarSolicitud},
     { CRUD: enumCRUD.Delete, Text: 'Cancelar solicitud', Icon: 'floppy'},
    ];

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    var FileActa = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: 7,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 30,
        MAX_NAME_LENGTH: 50
    };
    var FileParrilla = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: 8,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 30,
        MAX_NAME_LENGTH: 50
    };
    var FileSonoro = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        CONTENT_TYPE: ko.observable(''),
        LOADED: ko.observable(false),
        STATE_ID: 0,
        TYPE_ID: 9,
        EXTENSIONS: ['.mp3'],
        UID: null,
        COMMENT: '',
        MAX_SIZE: 30,
        MAX_NAME_LENGTH: 50
    };

    if(CRUD.In([enumCRUD.Update, enumCRUD.Read])){

        Solicitud = JSON.parse(SGEWeb.app.Solicitud);
        SOL_ENDED = Solicitud.SOL_ENDED;

        var Acta = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', FileActa.TYPE_ID);
        FileActa.UID = Acta.ANX_UID;
        FileActa.STATE_ID = Acta.ANX_STATE_ID;
        FileActa.CONTENT_TYPE(Acta.ANX_CONTENT_TYPE),
        FileActa.NAME(Acta.ANX_NAME);
        FileActa.COMMENT = Acta.ANX_COMMENT;
        if (FileActa.STATE_ID != enumROLE1_STATE.Requerido)
            FileActa.LOADED(true);

        var Parrilla = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', FileParrilla.TYPE_ID);
        FileParrilla.UID = Parrilla.ANX_UID;
        FileParrilla.STATE_ID = Parrilla.ANX_STATE_ID;
        FileParrilla.CONTENT_TYPE(Parrilla.ANX_CONTENT_TYPE),
        FileParrilla.NAME(Parrilla.ANX_NAME);
        FileParrilla.COMMENT = Parrilla.ANX_COMMENT;
        if (FileParrilla.STATE_ID != enumROLE1_STATE.Requerido)
            FileParrilla.LOADED(true);

        var Sonoro = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', FileSonoro.TYPE_ID);
        FileSonoro.UID = Sonoro.ANX_UID;
        FileSonoro.STATE_ID = Sonoro.ANX_STATE_ID;
        FileSonoro.CONTENT_TYPE(Sonoro.ANX_CONTENT_TYPE),
        FileSonoro.NAME(Sonoro.ANX_NAME);
        FileSonoro.COMMENT = Sonoro.ANX_COMMENT;
        if (FileSonoro.STATE_ID  != enumROLE1_STATE.Requerido)
            FileSonoro.LOADED(true);

    }


    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerdio: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var CrearSolicitudDisable = ko.computed(function () {
        if (FileActa.LOADED() && FileParrilla.LOADED() && FileSonoro.LOADED() && !GuardarDisabled())
            return false;
        return true;
    });

    function CrearConcesionarioSolicitud(SOL_TYPE, Expediente, Contacto, Emails, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearConcesionarioSolicitud",
            data: JSON.stringify({
                SOL_TYPE: SOL_TYPE,
                Expediente: Expediente,
                Contacto: Contacto,
                Emails: Emails,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function SubsanarConcesionarioSolicitud(Solicitud, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }

    function OpenFile(data, e) {

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + data.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");

    }

    function CrearSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Creando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];

        Files.push({ TYPE_ID: FileActa.TYPE_ID, NAME: FileActa.NAME(), CONTENT_TYPE: FileActa.CONTENT_TYPE_LOCAL(), DATA: FileActa.DATA });
        Files.push({ TYPE_ID: FileParrilla.TYPE_ID, NAME: FileParrilla.NAME(), CONTENT_TYPE: FileParrilla.CONTENT_TYPE_LOCAL(), DATA: FileParrilla.DATA });
        Files.push({ TYPE_ID: FileSonoro.TYPE_ID, NAME: FileSonoro.NAME(), CONTENT_TYPE: FileSonoro.CONTENT_TYPE_LOCAL(), DATA: FileSonoro.DATA });

        var SOL_TYPE = ListObjectFindGetElement(SGEWeb.app.SOL_TYPES, 'SOL_TYPE_ID', SOL_TYPE_ID);

        CrearConcesionarioSolicitud(SOL_TYPE, Expediente, Contacto, Emails, Files);
    }

    function SubsanarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Subsanando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        if (FileActa.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileActa.UID, TYPE_ID: FileActa.TYPE_ID, NAME: FileActa.NAME(), CONTENT_TYPE: FileActa.CONTENT_TYPE_LOCAL(), DATA: FileActa.DATA });
        if (FileParrilla.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileParrilla.UID, TYPE_ID: FileParrilla.TYPE_ID, NAME: FileParrilla.NAME(), CONTENT_TYPE: FileParrilla.CONTENT_TYPE_LOCAL(), DATA: FileParrilla.DATA });
        if (FileSonoro.STATE_ID == enumROLE1_STATE.Requerido)
            Files.push({ UID: FileSonoro.UID, TYPE_ID: FileSonoro.TYPE_ID, NAME: FileSonoro.NAME(), CONTENT_TYPE: FileSonoro.CONTENT_TYPE_LOCAL(), DATA: FileSonoro.DATA });

        SubsanarConcesionarioSolicitud(Solicitud, Files);
    }

    function CancelarSolicitud() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }


    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    var viewModel = {
        Expediente: Expediente,
        Contacto: Contacto,
        FileActa: FileActa,
        FileParrilla: FileParrilla,
        FileSonoro: FileSonoro,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CrearSolicitud: CrearSolicitud,
        CancelarSolicitud: CancelarSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        loadingVisible: loadingVisible,
        loadingMessage:loadingMessage,
        GuardarDisabled: GuardarDisabled,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility
    };

    return viewModel;
};