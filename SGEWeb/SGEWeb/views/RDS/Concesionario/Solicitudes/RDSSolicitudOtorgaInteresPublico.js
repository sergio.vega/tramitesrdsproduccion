﻿SGEWeb.RDSSolicitudOtorgaInteresPublico = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var SOL_TYPE = ListObjectFindGetElement(SGEWeb.app.SOL_TYPES, 'SOL_TYPE_ID', SOL_TYPE_ID);
    var title = ko.observable(SOL_TYPE.SOL_TYPE_NAME);
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var GuardarDisabled = ko.observable(false);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var DepartamentosPlanBro = ko.observableArray([]);
    var SelectedDepartamentoPlanBro = ko.observable(-1);
    var MunicipiosPlanBro = ko.observableArray([]);
    var SelectedMunicipioPlanBro = ko.observable(-1);
    var Distintivos = ko.observableArray([]);
    var SelectedDistintivo = ko.observable(-1);
    var ListaFiles = ko.observableArray([]);
    var ListaFilesPond = ko.observableArray([]);
    var Expediente = ko.observable(undefined);
    var TiposGestionServicio = ['Directa', 'Indirecta'];
    var SelectedGestionServicio = ko.observable('Directa');
    var TiposClasificacion = [{ Id: "RPNC", Descripcion: "Emisoras de la Radio Pública Nacional de Colombia" },
        { Id: "FP", Descripcion: "Emisoras de la Fuerza Pública" },
        { Id: "ET", Descripcion: "Emisoras Territoriales" },
        { Id: "ED", Descripcion: "Emisoras Educativas" },
        { Id: "EDU", Descripcion: "Emisoras Educativas Universitarias" },
        { Id: "APD", Descripcion: "Emisoras para atención y prevención de desastres" }
        ];
    var TipoClasificacionSeleccionada = ko.observable("RPNC");
    var Clasificacion = ko.observable("");
    var TiposNivelCubrimiento = [{ Id: "A", Descripcion: "Zonal Clase A" },
        { Id: "B", Descripcion: "Zonal Clase B" },
        { Id: "C", Descripcion: "Zonal restringido Clase C" },
    ];
    var TipoNivelCubrimientoSeleccionado = ko.observable("A");
    var NivelCubrimiento = ko.observable("");
    var TiposTecnologia = [
        { Id: "FM", Descripcion: "Frecuencia Modulada (F.M.)" },
        { Id: "DG", Descripcion: "Digital y Nuevas tecnologías" },
    ];
    var TipoTecnologiaSeleccionada = ko.observable("FM");
    var Tecnologia = ko.observable("");
    var ConoceDistintivo = ko.observable(false);
    var CallSign = ko.observable(undefined);

    var BotonCancelar = [
        { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
        { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
        { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
        { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la subsanación?' },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
        { CRUD: 'Dummy', Text: 'Crear solicitud', Icon: 'floppy' },
        { CRUD: enumCRUD.Create, Text: 'Crear solicitud', Icon: 'floppy', Method: CrearSolicitud },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: 'Subsanar solicitud', Icon: 'floppy', Method: SubsanarSolicitud },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar solicitud', Icon: 'floppy' },
    ];

    var BotonGuardar= [
        { CRUD: 'Dummy', Text: 'Guardar solicitud', Icon: 'floppy' },
        { CRUD: enumCRUD.Create, Text: 'Guardar solicitud', Icon: 'floppy', Method: GuardarSolicitud },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Delete, Text: '', Icon: 'floppy' },
    ];
    
    var Solicitud = undefined;
    var SOL_ENDED = 0;

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    function EvaluateTitle2(STATE_ID, COMMENT) {
        var Title = null;
        switch (STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var CrearSolicitudDisable = ko.computed(function () {
        var loaded = true;
        ListaFiles().forEach(function (file) {
            if ([enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(file.STATE_ID) || BotonEnviar[CRUD].CRUD == enumCRUD.Create) 
                loaded = loaded && file.LOADED();
        });

            //var loaded = true;
        //ListaFilesPond().forEach(function (file) {
        //    if ([enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(file.STATE_ID) || BotonEnviar[CRUD].CRUD == enumCRUD.Create) 
        //        loaded = loaded && file.LOADED();
        //    });

        var datosClasificacionServicioCompletos = false;
        if (SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
            datosClasificacionServicioCompletos = SelectedMunicipioPlanBro() != -1;
        }
        else {
            datosClasificacionServicioCompletos = SelectedDistintivo() != -1;
        }

        if (loaded && !GuardarDisabled() && (CRUD != enumCRUD.Create || (datosClasificacionServicioCompletos)))
            return false;
        return true;
    });
        
    //function BotonDisable() {

    //    if (BotonEnviar == enumCRUD.Create) {

    
    //    if (BotonEnviar == enumCRUD.Update) {

    //var SubsanarSolicitudDisable = ko.computed(function () {
    //    var loaded = true;
    //    ListaFiles().forEach(function (file) {
    //        loaded = loaded && file.LOADED();
    //    });

    //    var loaded = true;
    //    ListaFilesPond().forEach(function (file) {
    //        loaded = loaded && file.LOADED();
    //    });
        
    //    if (loaded)
    //        return false;
    //    return true;
    //        });
    //    }
    //}
    function CrearConcesionarioSolicitudOtorga(SOL_TYPE, Solicitud, LIC_NUMBER, NitOperador, Plan, Contacto, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, callSign) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearConcesionarioSolicitudOtorga",
            data: JSON.stringify({
                SOL_TYPE: SOL_TYPE,
                SolicitudTemp: Solicitud,
                LIC_NUMBER: LIC_NUMBER,
                NitOperador: NitOperador,
                Plan: Plan,
                Contacto: Contacto,
                Emails: Emails,
                Files: Files,
                FilesPond: FilesPond,
                NombreDepartamentoPlanBro: NombreDepartamentoPlanBro,
                NombreMunicipioPlanBro: NombreMunicipioPlanBro,
                CodeAreaMunicipioPlanBro: CodeAreaMunicipioPlanBro, 
                GestionServicio: SelectedGestionServicio(),
                Clasificacion: TipoClasificacionSeleccionada(),
                NivelCubrimiento: TipoNivelCubrimientoSeleccionado(),
                Tecnologia: TipoTecnologiaSeleccionada(),
                CallSign: callSign,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearConcesionarioSolicitudOtorgaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function GuardarConcesionarioSolicitudOtorga(SOL_TYPE, Solicitud, LIC_NUMBER, NitOperador, Plan, Contacto, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, callSign) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GuardarConcesionarioSolicitudOtorga",
            data: JSON.stringify({
                SOL_TYPE: SOL_TYPE,
                SolicitudTemp: Solicitud,
                LIC_NUMBER: LIC_NUMBER,
                NitOperador: NitOperador,
                Plan: Plan,
                Contacto: Contacto,
                Emails: Emails,
                Files: Files,
                FilesPond: FilesPond,
                NombreDepartamentoPlanBro: NombreDepartamentoPlanBro,
                NombreMunicipioPlanBro: NombreMunicipioPlanBro,
                CodeAreaMunicipioPlanBro: CodeAreaMunicipioPlanBro,
                GestionServicio: SelectedGestionServicio(),
                Clasificacion: TipoClasificacionSeleccionada(),
                NivelCubrimiento: TipoNivelCubrimientoSeleccionado(),
                Tecnologia: TipoTecnologiaSeleccionada(),
                CallSign: callSign,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.GuardarConcesionarioSolicitudOtorgaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function SubsanarConcesionarioSolicitud(Solicitud, Files, FilesPond) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files,
                FilesPond: FilesPond
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(e) {
        var FileObject = e.data.FileObject;
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;

            $("#GridOtorgaFiles").dxDataGrid('instance').refresh();
            $("#GridOtorgaFilesPonderables").dxDataGrid('instance').refresh();
        }
    }

    function OpenFile(e) {
        var FileObject = e.data.FileObject;
        if (Solicitud.SOL_STATE_ID == 1)
            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANXT&FUID=' + FileObject.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
        else
            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + FileObject.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function CrearSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Creando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);
        var Files = [];
        ListaFiles().forEach(function (file) {
            Files.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
        });
        var FilesPond = [];
        ListaFilesPond().forEach(function (file) {
            if (!IsNullOrEmpty(file.DATA)) {
                FilesPond.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });

        var LIC_NUMBER = 1;
        var Plan = $("#DivDistintivos").dxSelectBox('instance').option('selectedItem');
        var NombreDepartamentoPlanBro = $("#DivDepartamentosPlanBro").dxSelectBox('instance').option('selectedItem').NAME;
        var NombreMunicipioPlanBro = $("#DivMunicipiosPlanBro").dxSelectBox('instance').option('selectedItem').NAME;
        var CodeAreaMunicipioPlanBro = $("#DivMunicipiosPlanBro").dxSelectBox('instance').option('selectedItem').CODE_AREA;
        var callSign = ConoceDistintivo() ? CallSign() : undefined;

        Solicitud = null;
        if (!IsNullOrEmpty(SGEWeb.app.Solicitud)) {
            Solicitud = JSON.parse(SGEWeb.app.Solicitud);
        }
        CrearConcesionarioSolicitudOtorga(SOL_TYPE, Solicitud, LIC_NUMBER, SGEWeb.app.Nit, Plan, Contacto, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, callSign);
    }

    function GuardarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Guardando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        ListaFiles().forEach(function (file) {
            if (!IsNullOrEmpty(file.DATA)) {
                Files.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });
        var FilesPond = [];
        ListaFilesPond().forEach(function (file) {
            if (!IsNullOrEmpty(file.DATA)) {
                FilesPond.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });

        var LIC_NUMBER = 1;
        var Plan = $("#DivDistintivos").dxSelectBox('instance').option('selectedItem');
        var NombreDepartamentoPlanBro = $("#DivDepartamentosPlanBro").dxSelectBox('instance').option('selectedItem');
        NombreDepartamentoPlanBro = NombreDepartamentoPlanBro != null ? NombreDepartamentoPlanBro.NAME : "";
        var NombreMunicipioPlanBro = $("#DivMunicipiosPlanBro").dxSelectBox('instance').option('selectedItem')
        NombreMunicipioPlanBro = NombreMunicipioPlanBro != null ? NombreMunicipioPlanBro.NAME : "";
        var CodeAreaMunicipioPlanBro = NombreMunicipioPlanBro != null ? NombreMunicipioPlanBro.CODE_AREA : "";
        var callSign = ConoceDistintivo() ? CallSign() : undefined;

        Solicitud = null;
        if (!IsNullOrEmpty(SGEWeb.app.Solicitud)) {
            Solicitud = JSON.parse(SGEWeb.app.Solicitud);
        }
        GuardarConcesionarioSolicitudOtorga(SOL_TYPE, Solicitud, LIC_NUMBER, SGEWeb.app.Nit, Plan, Contacto, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, callSign);           
    }

    function SubsanarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Subsanando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        ListaFiles().forEach(function (file) {
            if (file.STATE_ID == enumROLE1_STATE.Requerido) {
                Files.push({ UID: file.UID, TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });
        var FilesPond = [];
        ListaFilesPond().forEach(function (file) {
            if (file.STATE_ID == enumROLE1_STATE.Requerido) {
                FilesPond.push({ UID: file.UID, TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });

        SubsanarConcesionarioSolicitud(Solicitud, Files, FilesPond);
    }

    function CancelarSolicitud() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function onKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'COMMENT':

                $('<i/>').addClass('ta ta-question-circle ta-2x')
                    .prop('title', 'Archivo(s) permitido(s) (' + e.data.EXTENSIONS.join() + ') y Tamaño máximo del archivo (' + e.data.MAX_SIZE + 'MB).')
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('float', 'left')
                    .css('color', '#6f91cb')
                    .appendTo(container);

                $('<i/>').addClass('ta-2x')
                    .addClass(ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'icon', ''))
                    .prop('title', 'Ver archivo')
                    .prop('visibility', (e.data.UID != null ? 'visible' : 'hidden'))
                    .bind('click', { FileObject: e.data }, OpenFile)
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('margin-right', '5px')
                    .css('cursor', 'pointer')
                    .css('float', 'left')
                    .css('color', ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'color', ''))
                    .appendTo(container);

                break;
            case 'Upload':
                var label = $('<label/>');
                label.addClass('custom-file-upload')
                    .prop('title', 'Seleccionar archivo')
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .css('position', 'relative')
                    .css('width', '400px')
                    .css('margin-top', '10px');
                label.disabled = false;

                var input = $('<input/>');
                input.prop('id', e.data.ID)
                    .prop('type', 'file')
                    .prop('accept', e.data.EXTENSIONS.join())
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .bind('change', { FileObject: e.data }, CargarFile)
                    .css('z-index', '999')
                    .css('position', 'absolute')
                    .css('top', '0px')
                    .css('visibility', 'hidden');
                input.appendTo(label);

                var info = $('<i/>');
                info.addClass('ta ta-attachment ta-lg')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(label);

                var span = $('<span/>');
                span.prop('textContent', e.data.NAME())
                    .appendTo(label);

                label.appendTo(container);
                break;

            case 'DESC':
                var div = $('<b/>');
                div.prop('textContent', e.data.DESC())
                    .css('vertical-align', 'middle')
                    .css('fontWeight', 'bold')
                    .css('word-wrap', 'break-word')
                    .appendTo(container);

                container.css('vertical-align', 'middle')
                    .css('word-wrap', 'break-word');
                break;
        }
    }

    var onDepartamentoPlanBroChanged = ko.computed(function () {
        loadingMessage("Leyendo municipios...");
        loadingVisible(true);
        GetMunicipiosOtorga(SelectedDepartamentoPlanBro());
    });

    function GetMunicipiosOtorga(CODE_AREA_DEPARTAMENTO) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMunicipiosOtorga",
            data: JSON.stringify({
                CODE_AREA_DEPARTAMENTO: CODE_AREA_DEPARTAMENTO,
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetMunicipiosOtorgaResult;
                MunicipiosPlanBro(result);
                SelectedMunicipioPlanBro(-1);
                if (Expediente() != undefined) { 
                    var municipio = ListObjectFindGetElement(MunicipiosPlanBro(), 'NAME', Expediente().CITY);
                    SelectedMunicipioPlanBro(municipio != null ? municipio.CODE_AREA : -1);
                } 
                SelectedDistintivo(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    var onMunicipioPlanBroChanged = ko.computed(function () {
        loadingMessage("Leyendo distintivos...");
        loadingVisible(true);
        GetDistintivos(SelectedMunicipioPlanBro());
    });

    function GetDistintivos(CODE_AREA_MUNICIPIO) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDistintivos",
            data: JSON.stringify({
                CODE_AREA_MUNICIPIO: CODE_AREA_MUNICIPIO,
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetDistintivosResult;
                Distintivos(result);
                SelectedDistintivo(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID, SOL_UID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDatosCreacionOtorgaEmisora",
            data: JSON.stringify({
                SOL_TYPE_ID: SOL_TYPE_ID,
                SOL_UID: SOL_UID,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetDatosCreacionOtorgaEmisoraResult;
                DepartamentosPlanBro(result.DepartamentosPlanBro);
                SelectedDepartamentoPlanBro(-1);
                SelectedMunicipioPlanBro(-1);
                SelectedDistintivo(-1);

                ListaFiles([]);
                for (var i = 0; i < result.TiposArchivos.length; i++) {
                    var tipoArchivo = result.TiposArchivos[i];
                    var file = {
                        ID: 'FileOtorga' + tipoArchivo.ID,
                        DESC: ko.observable(tipoArchivo.DESC),
                        NAME: ko.observable('Seleccionar archivo'),
                        DATA: undefined,
                        CONTENT_TYPE_LOCAL: ko.observable(''),
                        CONTENT_TYPE: ko.observable(''),
                        LOADED: ko.observable(false),
                        STATE_ID: 0,
                        TYPE_ID: tipoArchivo.ID,
                        EXTENSIONS: tipoArchivo.EXTENSIONS.split(','),
                        UID: null,
                        COMMENT: '',
                        MAX_SIZE: tipoArchivo.MAX_SIZE,
                        MAX_NAME_LENGTH: tipoArchivo.MAX_NAME_LENGTH,
                        Title: EvaluateTitle2(0, ''),
                    };
                    ListaFiles.push(file);
                }
                ListaFilesPond([]);
                for (var i = 0; i < result.TiposArchivosPond.length; i++) {
                    var tipoArchivo = result.TiposArchivosPond[i];
                    var file = {
                        ID: 'FileOtorga' + tipoArchivo.ID,
                        DESC: ko.observable(tipoArchivo.DESC),
                        NAME: ko.observable('Seleccionar archivo'),
                        DATA: undefined,
                        CONTENT_TYPE_LOCAL: ko.observable(''),
                        CONTENT_TYPE: ko.observable(''),
                        LOADED: ko.observable(false),
                        STATE_ID: 0,
                        TYPE_ID: tipoArchivo.ID,
                        EXTENSIONS: tipoArchivo.EXTENSIONS.split(','),
                        UID: null,
                        COMMENT: '',
                        MAX_SIZE: tipoArchivo.MAX_SIZE,
                        MAX_NAME_LENGTH: tipoArchivo.MAX_NAME_LENGTH,
                        Title: EvaluateTitle2(0, ''),
                    };
                    ListaFilesPond.push(file);
                }

                if (CRUD.In([enumCRUD.Create, enumCRUD.Update, enumCRUD.Read])) {

                    Solicitud = JSON.parse(SGEWeb.app.Solicitud);
                    if (!IsNullOrEmpty(Solicitud)) {

                        SOL_ENDED = Solicitud.SOL_ENDED;
                        Expediente(Solicitud.Expediente);

                        SelectedGestionServicio(Expediente().CUST_TXT3 == 'INDIRECTA' ? 'Indirecta' : 'Directa');
                        SelectedDepartamentoPlanBro(!IsNullOrEmpty(Expediente().DEPTO) ? ListObjectFindGetElement(DepartamentosPlanBro(), 'NAME', Expediente().DEPTO).CODE_AREA : undefined);
                        TipoClasificacionSeleccionada(Solicitud.Clasificacion != undefined ? ListObjectFindGetElement(TiposClasificacion, 'Id', Solicitud.Clasificacion).Id : undefined);
                        TipoNivelCubrimientoSeleccionado(Solicitud.NivelCubrimiento != undefined ? ListObjectFindGetElement(TiposNivelCubrimiento, 'Id', Solicitud.NivelCubrimiento).Id : undefined);
                        TipoTecnologiaSeleccionada(Solicitud.Tecnologia != undefined ? ListObjectFindGetElement(TiposTecnologia, 'Id', Solicitud.Tecnologia).Id : undefined);
                        CallSign(Expediente().CALL_SIGN);
                        ConoceDistintivo(!IsNullOrEmpty(CallSign()));

                        var filesToPop = [];
                        ListaFiles().forEach(function (file) {
                            var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', file.TYPE_ID);
                            if (element != null) {
                                file.UID = element.ANX_UID;
                                file.STATE_ID = element.ANX_STATE_ID;
                                file.CONTENT_TYPE(element.ANX_CONTENT_TYPE);
                                file.CONTENT_TYPE_LOCAL(element.ANX_CONTENT_TYPE);
                                file.NAME(element.ANX_NAME);
                                file.COMMENT = element.ANX_COMMENT;
                                file.DATA = "data:" + element.ANX_CONTENT_TYPE + ";base64," + element.ANX_FILE;
                                if (file.STATE_ID != enumROLE1_STATE.Requerido)
                                    file.LOADED(true);
                                file.Title = EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT);
                            }
                            else
                            {
                                if (!CRUD.In([enumCRUD.Create])) {
                                    filesToPop.push(file);
                                }
                            }
                        });

                        var filesToPopPond = [];
                        ListaFilesPond().forEach(function (file) {
                            var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', file.TYPE_ID);
                            if (element != null) {
                                file.UID = element.ANX_UID;
                                file.STATE_ID = element.ANX_STATE_ID;
                                file.CONTENT_TYPE(element.ANX_CONTENT_TYPE);
                                file.CONTENT_TYPE_LOCAL(element.ANX_CONTENT_TYPE);
                                file.NAME(element.ANX_NAME);
                                file.COMMENT = element.ANX_COMMENT;
                                file.DATA = "data:" + element.ANX_CONTENT_TYPE + ";base64," + element.ANX_FILE;
                                if (file.STATE_ID != enumROLE1_STATE.Requerido)
                                    file.LOADED(true);
                                file.Title = EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT);
                            }
                            else
                            {
                                if (!CRUD.In([enumCRUD.Create])) {
                                    filesToPopPond.push(file);
                                }
                            }
                        });

                        filesToPop.forEach(function (file) {
                            ListaFiles().pop(file);
                        });                

                        filesToPopPond.forEach(function (file) {
                            ListaFilesPond().pop(file);
                        });

                        var tiposAnexos = result.TiposAnexosPond;
                        tiposAnexos.forEach(function (tipoAnexo) {
                            var file = ListObjectFindGetElement(ListaFilesPond(), 'TYPE_ID', tipoAnexo.ID);
                            var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', tipoAnexo.ID);
                            if (IsNullOrEmpty(file) && element != null) {                            

                                var file = {
                                    ID: 'FileOtorga' + tipoAnexo.ID,
                                    DESC: ko.observable(tipoAnexo.DESC),
                                    NAME: ko.observable(element.ANX_NAME),
                                    DATA: undefined,
                                    CONTENT_TYPE_LOCAL: ko.observable(''),
                                    CONTENT_TYPE: ko.observable(element.ANX_CONTENT_TYPE),
                                    LOADED: ko.observable(element.ANX_STATE_ID != enumROLE1_STATE.Requerido ? true : false),
                                    STATE_ID: element.ANX_STATE_ID,
                                    TYPE_ID: tipoAnexo.ID,
                                    EXTENSIONS: tipoAnexo.EXTENSIONS.split(','),
                                    UID: element.ANX_UID,
                                    COMMENT: element.ANX_COMMENT,
                                    MAX_SIZE: tipoAnexo.MAX_SIZE,
                                    MAX_NAME_LENGTH: tipoAnexo.MAX_NAME_LENGTH,
                                    Title: EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT),
                                };
                                ListaFilesPond.push(file);
                            }
                        });

                        var tiposAnexos = result.TiposAnexos;
                        tiposAnexos.forEach(function (tipoAnexo) {
                            var file = ListObjectFindGetElement(ListaFiles(), 'TYPE_ID', tipoAnexo.ID);
                            var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', tipoAnexo.ID);
                            if (IsNullOrEmpty(file) && element != null) {                           

                                var file = {
                                    ID: 'FileOtorga' + tipoAnexo.ID,
                                    DESC: ko.observable(tipoAnexo.DESC),
                                    NAME: ko.observable(element.ANX_NAME),
                                    DATA: undefined,
                                    CONTENT_TYPE_LOCAL: ko.observable(''),
                                    CONTENT_TYPE: ko.observable(element.ANX_CONTENT_TYPE),
                                    LOADED: ko.observable(element.ANX_STATE_ID != enumROLE1_STATE.Requerido ? true : false),
                                    STATE_ID: element.ANX_STATE_ID,
                                    TYPE_ID: tipoAnexo.ID,
                                    EXTENSIONS: tipoAnexo.EXTENSIONS.split(','),
                                    UID: element.ANX_UID,
                                    COMMENT: element.ANX_COMMENT,
                                    MAX_SIZE: tipoAnexo.MAX_SIZE,
                                    MAX_NAME_LENGTH: tipoAnexo.MAX_NAME_LENGTH,
                                    Title: EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT),
                                };
                                ListaFiles.push(file);
                            }
                        });
                    }
                }
                loadingVisible(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        var SOL_UID = null;
        if (!IsNullOrEmpty(SGEWeb.app.Solicitud)) {
            var solicitud = JSON.parse(SGEWeb.app.Solicitud);
            SOL_UID = solicitud.SOL_UID;
        }
        GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID, SOL_UID);
    }

    Refrescar();


    var viewModel = {
        Contacto: Contacto,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CrearSolicitud: CrearSolicitud,
        GuardarSolicitud: GuardarSolicitud,
        CancelarSolicitud: CancelarSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        BotonGuardar: BotonGuardar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        DepartamentosPlanBro: DepartamentosPlanBro,
        SelectedDepartamentoPlanBro: SelectedDepartamentoPlanBro,
        MunicipiosPlanBro: MunicipiosPlanBro,
        SelectedMunicipioPlanBro: SelectedMunicipioPlanBro,
        Distintivos: Distintivos,
        SelectedDistintivo: SelectedDistintivo,
        onDepartamentoPlanBroChanged: onDepartamentoPlanBroChanged,
        onMunicipioPlanBroChanged: onMunicipioPlanBroChanged,
        onKeyDown: onKeyDown,
        ListaFiles: ListaFiles,
        ListaFilesPond: ListaFilesPond,
        onCellTemplate: onCellTemplate,
        Expediente: Expediente,
        title: title,
        SOL_TYPE_ID: SOL_TYPE_ID,
        TiposGestionServicio: TiposGestionServicio,
        SelectedGestionServicio: SelectedGestionServicio,
        TiposClasificacion: TiposClasificacion,
        TipoClasificacionSeleccionada: TipoClasificacionSeleccionada,
        Clasificacion: Clasificacion,
        TiposNivelCubrimiento: TiposNivelCubrimiento,
        TipoNivelCubrimientoSeleccionado: TipoNivelCubrimientoSeleccionado,
        NivelCubrimiento: NivelCubrimiento,
        TiposTecnologia: TiposTecnologia,
        TipoTecnologiaSeleccionada: TipoTecnologiaSeleccionada,
        Tecnologia: Tecnologia,
        ConoceDistintivo: ConoceDistintivo,
        CallSign: CallSign,
        //SubsanarSolicitudDisable: SubsanarSolicitudDisable,
    };

    return viewModel;
};