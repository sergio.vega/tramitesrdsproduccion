﻿SGEWeb.RDSRegistroUsuario = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) };
    var Departamentos = ko.observableArray([]);
    var SelectedDepartamento = ko.observable(-1);
    var Municipios = ko.observableArray([]);
    var MunicipiosEmpresa = ko.observableArray([]);
    var Distintivos = ko.observableArray([]);
    var SelectedDistintivo = ko.observable(-1);
    var ListaFiles = ko.observableArray([]);
    var Expediente = ko.observable(undefined);
    var PopupIdent = {
        Show: ko.observable(true),
        DISABLE: ko.observable()
    };
    var IdLugarForzado = undefined;


    var enumTipoPersona = { Natural: "Persona natural", Juridica: "Persona jurídica" };
    var TiposPersona = ko.observableArray([enumTipoPersona.Natural, enumTipoPersona.Juridica]);
    var TipoPersonaSeleccionada = ko.observable(enumTipoPersona.Natural);
    var enumTipoDocumentoNatural = { CC: "Cédula de ciudadanía", CE: "Cédula de extranjería" };
    var TiposDocumentoPersonaNatural = ko.observableArray([enumTipoDocumentoNatural.CC, enumTipoDocumentoNatural.CE]);
    var TipoDocumentoNaturalSeleccionado = ko.observable(enumTipoDocumentoNatural.CC);
    var enumTipoDocumentoJuridica = { NIT: "NIT" };
    var TiposDocumentoPersonaJuridica = ko.observableArray([enumTipoDocumentoJuridica.NIT]);
    var TipoDocumentoJuridicaSeleccionado = ko.observable(enumTipoDocumentoJuridica.NIT);
    var buttonIndicator = undefined;
    var InfoRegistroUsuario =
    {
        Identificacion: ko.observable(''),
        NombreRazonSocial: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        IdLugar: ko.observable(undefined).extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        Ciudad: '',
        Departamento: '',
        CodeLugar: '',
        DireccionCorrespondencia: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        Telefono: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        Email: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }, { type: "email", message: "Email inválido" }] } }),
        TipoDocumento: '',
        NombreComercial: ko.observable(''),
        NombreRepresentante: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        IdLugarEmpresa: ko.observable(undefined).extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        CiudadEmpresa: '',
        DepartamentoEmpresa: '',
        CodeLugarEmpresa: '',
        DireccionEmpresa: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }] } }),
        IdentificacionRepresentante: ko.observable('').extend({ dxValidator: { validationRules: [{ type: 'required', message: 'Campo requerido' }, { type: "numeric", message: "Identificación inválida" }] } }),
        NombreApoderado: ko.observable(''),
        IdentificacionApoderado: ko.observable(''),
        Contrasena: ko.observable(''),
        EsPersonaNatural: false,
    };

    var ContrasenaValida = ko.computed(function () {
        return !IsNullOrEmpty(InfoRegistroUsuario.Contrasena()) && InfoRegistroUsuario.Contrasena().length >= 6;
    });
    
    var ContrasenaConfirmacion = ko.observable('');
    var ContrasenasCoinciden = ko.computed(function () {
        return ContrasenaConfirmacion() == InfoRegistroUsuario.Contrasena();
    });

    var EmailConfirmacion = ko.observable('');
    var EmailsCoinciden = ko.computed(function () {
        return EmailConfirmacion() == InfoRegistroUsuario.Email();
    });
    

    var SelectedDepartamentoEmpresa = ko.observable(-1);
    var SelectedMunicipioEmpresa = ko.observable(-1);
    
    var TipoDocumentoRepresentanteSeleccionado = ko.observable(enumTipoDocumentoNatural.CC);
    var TipoDocumentoApoderadoSeleccionado = ko.observable(enumTipoDocumentoNatural.CC);

    var TipoDocumentoSeleccionado = ko.computed(function () {
        switch (TipoPersonaSeleccionada) {
            case enumTipoPersona.Natural:
                return TipoDocumentoNaturalSeleccionado;
            case enumTipoPersona.Juridica:
                return TipoDocumentoJuridicaSeleccionado;
            default:
                return '';
        }
    });

    var MaxLenghtIdent = ko.computed(function () {
        switch (TipoPersonaSeleccionada()) {
            case enumTipoPersona.Natural:
                switch (TipoDocumentoNaturalSeleccionado()) {
                    case enumTipoDocumentoNatural.CC:
                        return 10;
                    case enumTipoDocumentoNatural.CE:
                        return 7;
                };
            case enumTipoPersona.Juridica:
                switch (TipoDocumentoJuridicaSeleccionado()) {
                    case enumTipoDocumentoJuridica.NIT:
                        return 9;
                };
        }
    });

    var MaskIdent = ko.computed(function () {
        var mask = Array(MaxLenghtIdent() + 1).join("0");
        return mask;
    });

    var BotonCancelar = [
        { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
        { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: '¿Desea cancelar el registro del usuario?' },
        { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
        { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: '¿Desea cancelar la modificación del usuario?' },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
        { CRUD: 'Dummy', Text: 'Registrar usuario', Icon: 'floppy' },
        {
            CRUD: enumCRUD.Create, Text: 'Registrar usuario', Icon: 'floppy', ShowAlert: true,
            AlertMessage:
                '<p>Señor usuario, le recomendamos validar que todos los campos hayan sido diligenciados correctamente,' + 
                'ya que de esto depende la efectiva comunicación durante el proceso.</p> ' +
                '<p>Recuerde, al diligenciar esta información usted está de acuerdo de ser notificado electrónicamente.</p>' +
                '<p><center>¿Desea continuar?</center></p>', Method: RegistrarUsuario
        },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: 'Actualizar usuario', Icon: 'floppy', Method: ActualizarUsuario },
        { CRUD: enumCRUD.Delete, Text: 'Eliminar usuario', Icon: 'floppy' },
    ];

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    function EvaluateTitle2(STATE_ID, COMMENT) {
        var Title = null;
        switch (STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var RegistrarUsuarioDisable = ko.computed(function () {

        if (TipoPersonaSeleccionada() == enumTipoPersona.Natural) {
            if (InfoRegistroUsuario.NombreRazonSocial.dxValidator.validate().isValid
                && InfoRegistroUsuario.IdLugar.dxValidator.validate().isValid
                && InfoRegistroUsuario.DireccionCorrespondencia.dxValidator.validate().isValid
                && InfoRegistroUsuario.Telefono.dxValidator.validate().isValid
                && InfoRegistroUsuario.Email.dxValidator.validate().isValid
                && ContrasenasCoinciden()
                && ContrasenaValida()
            )
            return false;
        return true;
        }
        else {
            if (InfoRegistroUsuario.NombreRazonSocial.dxValidator.validate().isValid
                && InfoRegistroUsuario.IdLugar.dxValidator.validate().isValid
                && InfoRegistroUsuario.DireccionCorrespondencia.dxValidator.validate().isValid
                && InfoRegistroUsuario.Telefono.dxValidator.validate().isValid
                && InfoRegistroUsuario.Email.dxValidator.validate().isValid
                && InfoRegistroUsuario.IdLugarEmpresa.dxValidator.validate().isValid
                && InfoRegistroUsuario.DireccionEmpresa.dxValidator.validate().isValid
                && InfoRegistroUsuario.NombreRepresentante.dxValidator.validate().isValid
                && InfoRegistroUsuario.IdentificacionRepresentante.dxValidator.validate().isValid
                && ContrasenasCoinciden()
                && ContrasenaValida()
            )
                return false;
            return true;
        }
    });

    function CrearUsuario(infoUsuario) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearUsuario",
            data: JSON.stringify({
                infoUsuario: infoUsuario,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearUsuarioResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    var res = DevExpress.ui.dialog.alert("El registro ha sido exitoso. Para continuar con el proceso debe acceder al SGE con su número de identificación y la contraseña ingresada, <br /> y adjuntar la información jurídica correspondiente que se detalla en la Resolución MinTIC No. 410 de 2019", SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = false;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function SubsanarConcesionarioSolicitud(Solicitud, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(e) {
        var FileObject = e.data.FileObject;
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;

            $("#GridOtorgaComercialFiles").dxDataGrid('instance').refresh();
        }
    }

    function OpenFile(e) {
        var FileObject = e.data.FileObject;

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + FileObject.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function RegistrarUsuario() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Registrando usuario...');
        loadingVisible(true);

        InfoRegistroUsuario.TipoDocumento = GetTipoDocumento();
        InfoRegistroUsuario.EsPersonaNatural = TipoPersonaSeleccionada() == enumTipoPersona.Natural;
        InfoRegistroUsuario.TipoDocumentoRepresentante = TipoDocumentoRepresentanteSeleccionado() == enumTipoDocumentoNatural.CC ? "C.C." : "C.E.";
        InfoRegistroUsuario.TipoDocumentoApoderado = TipoDocumentoApoderadoSeleccionado() == enumTipoDocumentoNatural.CC ? "C.C." : "C.E.";

        var municipio = Municipios().filter(function (obj) { return obj.ID === InfoRegistroUsuario.IdLugar() })[0];
        var departamento = Departamentos().filter(function (obj) { return obj.CODE_AREA === SelectedDepartamento() })[0];
        InfoRegistroUsuario.Ciudad = municipio.NAME;
        InfoRegistroUsuario.CodeLugar = municipio.CODE_AREA;
        InfoRegistroUsuario.Departamento = departamento.NAME;
        if (TipoPersonaSeleccionada() == enumTipoPersona.Juridica) {
            var municipioEmpresa = MunicipiosEmpresa().filter(function (obj) { return obj.ID === InfoRegistroUsuario.IdLugarEmpresa() })[0];
            var departamentoEmpresa = Departamentos().filter(function (obj) { return obj.CODE_AREA === SelectedDepartamentoEmpresa() })[0];
            InfoRegistroUsuario.CiudadEmpresa = municipioEmpresa.NAME;
            InfoRegistroUsuario.CodeLugarEmpresa = municipioEmpresa.CODE_AREA;
            InfoRegistroUsuario.DepartamentoEmpresa = departamentoEmpresa.NAME;
        }
        CrearUsuario(ko.toJS(InfoRegistroUsuario));
    }
    
    function GetTipoDocumento() {
        switch (TipoPersonaSeleccionada()) {
            case enumTipoPersona.Natural:
                switch (TipoDocumentoNaturalSeleccionado()) {
                    case enumTipoDocumentoNatural.CC:
                        return "C.C.";
                    case enumTipoDocumentoNatural.CE:
                        return "C.E.";
                };
            case enumTipoPersona.Juridica:
                switch (TipoDocumentoJuridicaSeleccionado()) {
                    case enumTipoDocumentoJuridica.NIT:
                        return "Nit";
                };
        }
    }

    function ActualizarUsuario() {

    }

    function CancelarRegistro() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function ConfirmarRegistro() {
        if (BotonEnviar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonEnviar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    BotonEnviar[CRUD].Method();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function OnVolver() {
        SGEWeb.app.navigationManager.back();
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function onKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }


    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'COMMENT':

                $('<i/>').addClass('ta ta-question-circle ta-2x')
                    .prop('title', 'Archivo(s) permitido(s) (' + e.data.EXTENSIONS.join() + ') y Tamaño máximo del archivo (' + e.data.MAX_SIZE + 'MB).')
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('float', 'right')
                    .css('color', '#6f91cb')
                    .appendTo(container);

                $('<i/>').addClass('ta-2x')
                    .addClass(ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'icon', ''))
                    .prop('title', 'Ver archivo')
                    .prop('visibility', (e.data.UID != null ? 'visible' : 'hidden'))
                    .bind('click', { FileObject: e.data }, OpenFile)
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-right', '5px')
                    .css('cursor', 'pointer')
                    .css('float', 'right')
                    .css('color', ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'color', ''))
                    .appendTo(container);

                break;
            case 'Upload':
                var label = $('<label/>');
                label.addClass('custom-file-upload')
                    .prop('title', 'Seleccionar archivo')
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .css('position', 'relative')
                    .css('width', '400px')
                    .css('margin-top', '10px');
                label.disabled = false;

                var input = $('<input/>');
                input.prop('id', e.data.ID)
                    .prop('type', 'file')
                    .prop('accept', e.data.EXTENSIONS.join())
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .bind('change', { FileObject: e.data }, CargarFile)
                    .css('z-index', '999')
                    .css('position', 'absolute')
                    .css('top', '0px')
                    .css('visibility', 'hidden');
                input.appendTo(label);

                var info = $('<i/>');
                info.addClass('ta ta-attachment ta-lg')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(label);

                var span = $('<span/>');
                span.prop('textContent', e.data.NAME())
                    .appendTo(label);

                label.appendTo(container);
                break;

            case 'DESC':
                var div = $('<b/>');
                div.prop('textContent', e.data.DESC())
                    .css('vertical-align', 'middle')
                    .css('fontWeight', 'bold')
                    .css('word-wrap', 'break-word')
                    .appendTo(container);

                container.css('vertical-align', 'middle')
                    .css('word-wrap', 'break-word');
                break;
        }
    }

    function GetDistintivos(CODE_AREA_MUNICIPIO) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDistintivos",
            data: JSON.stringify({
                CODE_AREA_MUNICIPIO: CODE_AREA_MUNICIPIO,
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetDistintivosResult;
                Distintivos(result);
                SelectedDistintivo(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetDepartamentos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDepartamentos",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetDepartamentosResult;
                Departamentos(result);
                SelectedDepartamento(-1);
                InfoRegistroUsuario.IdLugar(undefined);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function OnTemplateAutenticar(data, container) {
        $("<i class='dx-icon ta ta-caret ta-lg'></i><span class='dx-button-text'>" + data.text + "</span><div class='button-indicator' style='height: 32px; width: 32px; display: inline-block; vertical-align: text-top; margin-left: 10px; margin-top:-4px;'></div>").appendTo(container);
        buttonIndicator = container.find(".button-indicator").dxLoadIndicator({
            visible: false
        }).dxLoadIndicator("instance");
    }

    function ContinuarSiUsuarioNoExiste(){
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ExisteManifestacionUsuario",
            data: JSON.stringify({
                IDENT: InfoRegistroUsuario.Identificacion(),
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var existeUsuario = msg.ExisteManifestacionUsuarioResult.ExisteUsuario;
                if (existeUsuario) {
                    DevExpress.ui.dialog.alert(CrearDialogHtml('ta ta-warning', 'El usuario ya se encuentra registrado'), SGEWeb.app.Name);
                }
                else {
                    var infoManifestacion = msg.ExisteManifestacionUsuarioResult.InfoRegistroManfestacion;
                    if (!IsNullOrEmpty(infoManifestacion)) {
                        InfoRegistroUsuario.Identificacion(infoManifestacion.Identificacion);
                        InfoRegistroUsuario.NombreRazonSocial(infoManifestacion.NombreRazonSocial);
                        SelectedDepartamento(infoManifestacion.CodeAreaDepartamento)
                        IdLugarForzado = infoManifestacion.IdLugar;
                        InfoRegistroUsuario.DireccionCorrespondencia(infoManifestacion.DireccionCorrespondencia);
                        InfoRegistroUsuario.Telefono(infoManifestacion.Telefono);
                        InfoRegistroUsuario.Email(infoManifestacion.Email);
                        InfoRegistroUsuario.NombreRepresentante(infoManifestacion.NombreRepresentante);
                        InfoRegistroUsuario.IdentificacionRepresentante(infoManifestacion.IdentificacionRepresentante);
                    }

                    PopupIdent.DISABLE(true);
                    PopupIdent.Show(false);
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function OnContinuar(e) {
        ContinuarSiUsuarioNoExiste();
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 13) {
            $('#btnContinuar').dxButton('instance').focus();
            OnContinuar();
        }
        return true;
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }

    var onDepartamentoChanged = ko.computed(function () {
        loadingMessage("Leyendo municipios...");
        loadingVisible(true);
        GetMunicipios(SelectedDepartamento(), Municipios, InfoRegistroUsuario.IdLugar);
    });

    function GetMunicipios(CODE_AREA_DEPARTAMENTO, variableMunicipios, variableLugar) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMunicipios",
            data: JSON.stringify({
                CODE_AREA_DEPARTAMENTO: CODE_AREA_DEPARTAMENTO
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetMunicipiosResult;
                variableMunicipios(result);
                variableLugar(IdLugarForzado);
                IdLugarForzado = undefined;
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    var onDepartamentoEmpresaChanged = ko.computed(function () {
        loadingMessage("Leyendo municipios...");
        loadingVisible(true);
        GetMunicipios(SelectedDepartamentoEmpresa(), MunicipiosEmpresa, InfoRegistroUsuario.IdLugarEmpresa);
    });

    function onTipoDocumentoChanged(data) {
        if (InfoRegistroUsuario.Identificacion().length > MaxLenghtIdent()) {
            InfoRegistroUsuario.Identificacion(InfoRegistroUsuario.Identificacion().substring(0, MaxLenghtIdent()));
        }
    }

    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetDepartamentos();
    }

    Refrescar();

    var viewModel = {
        Contacto: Contacto,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        RegistrarUsuario: RegistrarUsuario,
        CancelarRegistro: CancelarRegistro,
        ConfirmarRegistro: ConfirmarRegistro,
        RegistrarUsuarioDisable: RegistrarUsuarioDisable,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        Departamentos: Departamentos,
        SelectedDepartamento: SelectedDepartamento,
        Municipios: Municipios,
        Distintivos: Distintivos,
        SelectedDistintivo: SelectedDistintivo,
        onKeyDown: onKeyDown,
        ListaFiles: ListaFiles,
        onCellTemplate: onCellTemplate,
        Expediente: Expediente,
        PopupIdent: PopupIdent,
        TiposPersona: TiposPersona,
        TipoPersonaSeleccionada: TipoPersonaSeleccionada,
        TiposDocumentoPersonaNatural: TiposDocumentoPersonaNatural,
        TipoDocumentoNaturalSeleccionado: TipoDocumentoNaturalSeleccionado,
        TiposDocumentoPersonaJuridica: TiposDocumentoPersonaJuridica,
        TipoDocumentoJuridicaSeleccionado: TipoDocumentoJuridicaSeleccionado,
        OnTemplateAutenticar: OnTemplateAutenticar,
        OnContinuar: OnContinuar,
        onPwdKeyDown: onPwdKeyDown,
        enumTipoPersona: enumTipoPersona,
        enumTipoDocumentoNatural: enumTipoDocumentoNatural,
        enumTipoDocumentoJuridica: enumTipoDocumentoJuridica,
        TipoDocumentoSeleccionado: TipoDocumentoSeleccionado,
        MaxLenghtIdent: MaxLenghtIdent,
        MaskIdent: MaskIdent, 
        OnVolver: OnVolver,
        keyPressAction: keyPressAction,
        InfoRegistroUsuario: InfoRegistroUsuario,
        onDepartamentoChanged: onDepartamentoChanged,
        TipoDocumentoRepresentanteSeleccionado: TipoDocumentoRepresentanteSeleccionado,
        SelectedDepartamentoEmpresa: SelectedDepartamentoEmpresa,
        SelectedMunicipioEmpresa: SelectedMunicipioEmpresa,
        onTipoDocumentoChanged: onTipoDocumentoChanged,
        TipoDocumentoApoderadoSeleccionado: TipoDocumentoApoderadoSeleccionado,
        ContrasenaConfirmacion: ContrasenaConfirmacion,
        ContrasenasCoinciden: ContrasenasCoinciden,
        ContrasenaValida: ContrasenaValida,
        EmailConfirmacion: EmailConfirmacion,
        EmailsCoinciden: EmailsCoinciden,
        onDepartamentoEmpresaChanged: onDepartamentoEmpresaChanged,
        MunicipiosEmpresa: MunicipiosEmpresa,
    };

    return viewModel;
};