﻿SGEWeb.RDSConcesionarioSolicitudes = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var MyDataSourceDocs = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var ShowPopUp = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var CRUDType = -1; //1: Nuevo, 3: Edit
    var CRUDTitle = ko.observable('');
    var Cantidades = ko.observable(0);
    var CantidadesDocs = ko.observable(0);
    var ShowPopUpDocuments = ko.observable(false);
    var Operador = ko.observable(undefined);
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var SelectedSolicitud = undefined;
    var isLoading = ko.observable(false);
    var UserSelected = ko.observable({ CONT_ID: -1, CONT_NAME: '', CONT_NUMBER: '', CONTT_TITLE: '', CONT_TEL: '' });
    var UserCC = ko.observable('');
    var Contactos = ko.observableArray([]);
    var TiposSolicitudesPosibles = ko.observableArray([]);
    var ShowPopUpOtorga = ko.observable(false);
    var CusEmailOperador = ko.observable(undefined);
    var TipoSolicitudACrear = ko.observable(-1);
    var ConcesionarioIsFirstTime = true;

    var Alarmas = { Encurso: ko.observable(undefined), Requeridas: ko.observable(undefined), Porvencer: ko.observable(undefined), EnCierre: ko.observable(undefined), Terminadas: ko.observable(undefined), Total: ko.observable(0) };
    
    var EmailContacto1 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: 'required', message: 'Email requerido' },
            { type: "email", message: "Email inválido" }]
        }
    });
    var EmailContacto2 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;
    var EmailContacto3 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;

    var CrearSolicitudDisable = ko.computed(function () {

        if (UserSelected().CONT_ID != -1 && TipoSolicitudACrear() != -1
            && EmailContacto1.dxValidator.validate().isValid
            && EmailContacto2.dxValidator.validate().isValid
            && EmailContacto3.dxValidator.validate().isValid
        )
            return false;
        return true;
    });

    DevExpress.config({
        floatingActionButtonConfig: {
            position: {
                my: "right bottom",
                at: "right bottom",
                of: "#app-container",
                offset: "-16 -74"
            },
            label: "Nueva solicitud"
        }
    });
    
    function CRUDTypeEmisorasPosibles(SOL_STATE, SOL_TYPE_ID) {
        var CRUD = enumCRUD.Read;
        var emisora = ListObjectFindGetElement(TiposSolicitudesPosibles(), "SOL_TYPE_ID", SOL_TYPE_ID);
        if (!IsNullOrEmpty(emisora) && !emisora.disabled) {
            CRUD = EvaluateCRUDType(SOL_STATE);
        }
        else {
            if (SOL_STATE == enumSOL_STATES.SinRadicar) {
                var msg = '<p> Señor usuario: </p> ' +
                    '<p>La fecha de presentación de las propuestas ha finalizado, por lo tanto no podrá radicar el tramite.</p>';
                DevExpress.ui.dialog.alert(CrearDialogHtml('ta ta-info', msg), SGEWeb.app.Name);
            }
            else
                CRUD = EvaluateCRUDType(SOL_STATE);
        }
        return CRUD;
    }

    function GetSolicitudesConcesionario(Nit) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesConcesionario",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Nit: Nit
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetSolicitudesConcesionarioResult;
                MyDataSource(result.Solicitudes);
                Operador = result.Operador;
                TiposSolicitudesPosibles(result.SOL_TYPES);
                title(Operador.CUS_NAME);
                CusEmailOperador(Operador.CUS_EMAIL);
                Contactos(result.Contactos);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CancelarConcesionarioSolicitud(Solicitud) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CancelarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CancelarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        Refrescar();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function AplazarConcesionarioSolicitud(Solicitud) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/AplazarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.AplazarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        Refrescar();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSConcesionario') {
            if(!isLoading())
                loadingVisible(false);

            Cantidades(Globalizeformat(e.component.totalCount(), "n0"));

            var GridSource = e.component.getDataSource();
            Alarmas.Encurso(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Encurso));
            Alarmas.Requeridas(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Requeridas));
            Alarmas.Porvencer(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Porvencer));
            Alarmas.EnCierre(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.EnCierre));
            Alarmas.Terminadas(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Terminadas));
            //No debería haber vencidas ya que al vencerse se desiste
            Alarmas.Total(e.component.totalCount() - ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Vencidas));

            if (Alarmas.Requeridas() > 0) {
                if (ConcesionarioIsFirstTime) {
                    var Message = "";
                    if (Alarmas.Requeridas() == 1)
                        Message = CrearDialogHtml('ta ta-info', 'El concesionario presenta ' + Alarmas.Requeridas() + ' solicitud requerida pendiente de subsanar.');
                    else
                        Message = CrearDialogHtml('ta ta-info', 'El concesionario presenta ' + Alarmas.Requeridas() + ' solicitudes requeridas pendientes de subsanar.');

                    DevExpress.ui.dialog.alert(Message, SGEWeb.app.Name);
                    ConcesionarioIsFirstTime = false;
                }
            }
            

            SGEWeb.app.DisabledToolBar(false);

        } else {
            CantidadesDocs(Globalizeformat(e.component.totalCount(), "n0"));
            SGEWeb.app.DisabledToolBar(false);
        }
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar solicitud')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Cancelar':
                $('<i/>').addClass('ta ta-cancel ta-lg')
                .prop('title', 'Cancelar solicitud')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                .prop('title', 'Documentos')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Aplazar':
                $('<i/>').addClass('ta ta-alarm ta-lg')
                .prop('title', 'Aplazar requerimiento')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
                
            case 'Alarma':
                $('<i/>').addClass('ta ta-bell ta-lg')
                .prop('title', 'Alarmas')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;

        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Editar solicitud')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Documents':
                var DOCS_NO_OPENED=ListObjectExists(e.data.Documents,'DOC_OPENED',0);
                $('<i/>').addClass('ta ta-attachment ta-lg')
                .prop('title', DOCS_NO_OPENED ? 'Ver documentos (hay documentos sin revisar)' : 'Ver Documentos')
                .css('cursor', 'pointer')
                .css('color', DOCS_NO_OPENED ? '#D00000' : 'black')
                .appendTo(container);
                break;

            case 'Cancelar':
                if (e.data.SOL_ENDED==0) {
                    $('<i/>').addClass('ta ta-cancel ta-lg')
                    .prop('title', 'Cancelar solicitud')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                }
                break;

            case 'Aplazar':
                if (e.data.SOL_ENDED==0 && e.data.SOL_STATE_ID == enumSOL_STATES.Requerido && [enumALARM_REQUIRED.Requeridas, enumALARM_REQUIRED.Porvencer].Exists(e.data.SOL_ALARM_STATE)) {
                    $('<i/>').addClass('ta ta-alarm ta-lg')
                    .prop('title', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'Aplazar requerimiento' : 'El requerimiento ya fue aplazado')
                    .css('cursor', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'pointer':'default')
                    .css('color', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'black' : '#D00000')
                    .appendTo(container);
                }
                break;

            case 'Alarma':
                $('<i/>').addClass(SGEWeb.app.ALARM_REQUIRED[e.data.SOL_ALARM_STATE].Icon + ' ta-lg')
                .prop('title', SGEWeb.app.ALARM_REQUIRED[e.data.SOL_ALARM_STATE].textsingle)
                .css('cursor', 'default')
                .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>')
                .appendTo(container);
                container.css('text-align', 'center')
                break;

            case 'Ver Documento':
                if (e.data.DOC_OPENED() == 0) {
                    $('<i/>').addClass('ta ta-eye ta-lg')
                    .prop('title', 'Sin abrir')
                    .css('cursor', 'pointer')
                    .css('color', '#D00000')
                    .appendTo(container);
                } else {
                    $('<i/>').addClass('ta ta-eye ta-lg')
                    .prop('title', 'Abierto')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(container);
                }
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSConcesionario') {
                switch (e.column.name) {
                    case 'Documents':
                        SelectedSolicitud = e.data;
                        MyDataSourceDocs(ko.mapping.fromJS(e.data.Documents));
                        ShowPopUpDocuments(true);
                        break;

                    case 'Editar':
                        var Emails = [Operador.CUS_EMAIL, EmailContacto1(), EmailContacto2(), EmailContacto3()];
                        var CRUDAux = enumCRUD.Read;
                        switch (e.data.SOL_TYPE_ID) {
                            case 2:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                SGEWeb.app.navigate({ view: "RDSSolicitudJuntaProgramacion", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails} });
                                break;

                            case 7:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                SGEWeb.app.navigate({ view: "RDSSolicitudProrroga", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                break;

                            case enumSOL_TYPES.OtorgaEmisoraComercial:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                if (e.data.CURRENT.STEP == 1)
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaTecnico", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                else
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaEmisora", id: -1, settings: { title: 'ABC', CRUD: CRUDTypeEmisorasPosibles(e.data.SOL_STATE_ID, e.data.SOL_TYPE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                break;
                            case enumSOL_TYPES.OtorgaEmisoraComunitaria:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                if (e.data.CURRENT.STEP == 1)
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaTecnico", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                else
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaComunitaria", id: -1, settings: { title: 'ABC', CRUD: CRUDTypeEmisorasPosibles(e.data.SOL_STATE_ID, e.data.SOL_TYPE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                break;
                            case enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                if (e.data.CURRENT.STEP == 1)
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaTecnico", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                else
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaComunitariaGruposEtnicos", id: -1, settings: { title: 'ABC', CRUD: CRUDTypeEmisorasPosibles(e.data.SOL_STATE_ID, e.data.SOL_TYPE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                break;
                            case enumSOL_TYPES.OtorgaEmisoraDeInteresPublico:
                                $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
                                $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
                                SGEWeb.app.Solicitud = JSON.stringify(e.data);
                                SGEWeb.app.Expediente = JSON.stringify(e.data.Expediente);
                                if (e.data.CURRENT.STEP == 1)
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaTecnico", id: -1, settings: { title: 'ABC', CRUD: EvaluateCRUDType(e.data.SOL_STATE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                else
                                    SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaInteresPublico", id: -1, settings: { title: 'ABC', CRUD: CRUDTypeEmisorasPosibles(e.data.SOL_STATE_ID, e.data.SOL_TYPE_ID), SOL_TYPE_ID: e.data.SOL_TYPE_ID, Contacto: e.data.Contacto, Emails: Emails } });
                                break;
                        }
                        break;

                    case 'Cancelar':
                        if (e.data.SOL_ENDED!=0) {
                            e.event.stopPropagation();
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea cancelar la solicitud?"), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                SGEWeb.app.DisabledToolBar(true);
                                loadingVisible(true);
                                loadingMessage('Cancelando solicitud...');
                                CancelarConcesionarioSolicitud(e.data);
                            }
                        });
                        break;

                    case 'Aplazar':
                        if (e.data.SOL_ENDED!=0 || e.data.SOL_REQUIRED_POSTPONE!=0 || e.data.SOL_STATE_ID != enumSOL_STATES.Requerido || [enumALARM_REQUIRED.Requeridas, enumALARM_REQUIRED.Porvencer].NoExists(e.data.SOL_ALARM_STATE)) {
                            e.event.stopPropagation();
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea solicitar aplazamiento del requerimiento?"), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                SGEWeb.app.DisabledToolBar(true);
                                loadingVisible(true);
                                loadingMessage('Solicitando aplazamiento...');
                                AplazarConcesionarioSolicitud(e.data);
                            }
                        });
                        break;

                }

            } else if (e.element[0].id == 'GridDocuments') {
                switch (e.column.name) {
                    case 'Ver Documento':
                        if (e.data.DOC_OPENED() == 0) {
                            e.data.DOC_OPENED(1);
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID() + '&MRC=1&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        } else {
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID() + '&MRC=0&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        }
                        break;
                }
            }
        }
    }

    function popupShowing(e) {
        $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function popupHidden(e) {
        SelectedSolicitud.Documents = ko.toJS(MyDataSourceDocs())
        var grid = $("#RDSConcesionario").dxDataGrid('instance');
        if (grid != undefined) {
            grid.repaintRows(grid.getRowIndexByKey(SelectedSolicitud.SOL_UID));
        }

        MyDataSourceDocs([]);

        var grid = $("#GridDocuments").dxDataGrid('instance');

        if (grid) {
            grid.clearFilter();
            grid.clearSorting();
            grid.pageIndex(0);
        }
    }

    function popupOtorgaHidden(e) {


    }

    function popupOtorgaShowing(e) {
    }

    function Excel() {
        var grid = $("#RDSConcesionario").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function NuevaSolicitudOtros() {
        $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
        $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
        SGEWeb.app.navigate({ view: "RDSExpedientes", id: -1, settings: { title: title(), Operador: Operador } });
    }

    function NuevaSolicitudOtorga() {
        TipoSolicitudACrear(-1);
        EmailContacto1.dxValidator.reset();
        EmailContacto1(''); EmailContacto2(''); EmailContacto3('');
        SGEWeb.app.Solicitud = null;
        ShowPopUpOtorga(true);
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }

    function CrearSolicitud() {

        var Emails = [CusEmailOperador, EmailContacto1(), EmailContacto2(), EmailContacto3()];

        if (TipoSolicitudACrear() == -1) {
            DevExpress.ui.notify('Favor seleccionar un tipo de solicitud.', "warning", 5000);
        }
        else if (TipoSolicitudACrear() == enumSOL_TYPES.OtorgaEmisoraComercial) {
            ShowPopUpOtorga(false);
            $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
            $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
            SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaEmisora", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: TipoSolicitudACrear(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else if (TipoSolicitudACrear() == enumSOL_TYPES.OtorgaEmisoraComunitaria) {
            ShowPopUpOtorga(false);
            $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
            $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
            SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaComunitaria", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: TipoSolicitudACrear(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else if (TipoSolicitudACrear() == enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos) {
            ShowPopUpOtorga(false);
            $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
            $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
            SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaComunitariaGruposEtnicos", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: TipoSolicitudACrear(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else if (TipoSolicitudACrear() == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
            ShowPopUpOtorga(false);
            $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
            $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
            SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaInteresPublico", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: TipoSolicitudACrear(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else {
            DevExpress.ui.notify('Tipo de solicitud no implementada.', "warning", 3000);
        }
    }

    function BuscarUsuario() {
        var Contacto = ListObjectFindGetElement(Contactos(), 'CONT_NUMBER', UserCC());
        if (Contacto != null) {
            UserSelected(Contacto);
        } else {
            DevExpress.ui.notify('El número de identificación no está registrado para realizar este trámite. Favor corregir e intentar de nuevo.', "warning", 3000);
        }

    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', true);
        $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', true);
        if (e.direction == 'backward') {
            if(SGEWeb.app.NeedRefresh){
                SGEWeb.app.NeedRefresh=false;
                Refrescar();
            }
        } 
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetSolicitudesConcesionario(SGEWeb.app.Nit);
        isLoading(true);
    }

    Refrescar();
    

    var viewModel = {
        viewShown: handleViewShown,
        MyDataSource: MyDataSource,
        MyDataSourceDocs: MyDataSourceDocs,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        Excel: Excel,
        Cantidades: Cantidades,
        CantidadesDocs:CantidadesDocs,
        ShowPopUpDocuments: ShowPopUpDocuments,
        popupShowing: popupShowing,
        popupHidden: popupHidden,
        title: title,
        Refrescar: Refrescar,
        Alarmas: Alarmas,
        isLoading: isLoading,
        NuevaSolicitudOtros: NuevaSolicitudOtros,
        NuevaSolicitudOtorga: NuevaSolicitudOtorga,
        TiposSolicitudesPosibles: TiposSolicitudesPosibles,
        popupOtorgaShowing: popupOtorgaShowing,
        popupOtorgaHidden: popupOtorgaHidden,
        ShowPopUpOtorga: ShowPopUpOtorga,
        CusEmailOperador: CusEmailOperador,
        TipoSolicitudACrear: TipoSolicitudACrear,
        EmailContacto1: EmailContacto1,
        EmailContacto2: EmailContacto2,
        EmailContacto3: EmailContacto3,
        CrearSolicitud: CrearSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        UserSelected: UserSelected,
        keyPressAction: keyPressAction,
        UserCC: UserCC,
        BuscarUsuario: BuscarUsuario,
        onPwdKeyDown: onPwdKeyDown,
        Operador: Operador,
        SelectedSolicitud: SelectedSolicitud,
        Contactos: Contactos,
    };

    return viewModel;
};