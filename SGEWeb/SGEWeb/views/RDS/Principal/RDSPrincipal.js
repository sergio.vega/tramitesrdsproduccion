﻿SGEWeb.RDSPrincipal = function (params) {
    "use strict";
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var UserText = ko.observable();
    var UserRole = ko.observable();
    var MenuArr = ko.observableArray([]);
    var ListDataArr = ko.observableArray();
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var IsAdministrator = ko.observable(false);
    var buttonIndicator = undefined;
    var MyPopUpLogin = {
        Show: ko.observable(false),
        USR_LOGIN: ko.observable(),
        USR_PASSWORD: ko.observable(),
        DISABLE: ko.observable()
    };

    SGEWeb.app.DisabledToolBar(true);

    var ListData = [
        //INICIO DASHBOARD
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },

        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Director, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Cuadro de control', Desc: 'Gráfica de las concesiones y solicitudes realizadas.', IMG: 'ta ta-pie ta-3x', view: 'RDSDashboardPrincipal' },
        //FIN DASHBOARD
        { ID: 1, GROUP: enumGROUPS.Concesionario, ROLE: enumROLES.Concesionario, Unico: false, EXTRA: 0, name: 'Solicitudes del concesionario', Desc: 'Listado de solicitudes del concesionario', IMG: 'ta ta-clipboard ta-3x', view: 'RDSConcesionarioSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: false, EXTRA: 0, name: 'Mis solicitudes como funcionario', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-funcionario ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Revisor, Unico: false, EXTRA: 0, name: 'Mis solicitudes como revisor', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-revisor ta-3x', view: 'RDSSolicitudes' },

        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: false, EXTRA: 0, name: 'Solicitudes físicas de prorroga', Desc: 'Permite cargar solicitudes de prórroga radicadas previamente por el concesionario.', IMG: 'ta ta-crono ta-3x', view: 'RDSFuncionarioSolicitudProrroga' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: false, EXTRA: 0, name: 'Solicitudes físicas de otorga grupos étnicos', Desc: 'Permite cargar solicitudes de otorga grupos étnicos radicadas previamente por el concesionario.', IMG: 'ta ta-crono ta-3x', view: 'RDSFuncionarioSolicitudOtorgaGE' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: false, EXTRA: 0, name: 'Mis observaciones como funcionario', Desc: 'Listado de observaciones asignadas a mí.', IMG: 'ta ta-funcionario ta-3x', view: 'RDSObservaciones' },

        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Mis solicitudes como coordinador', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-coordinador ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumROLES.Funcionario, name: 'Conteo de devoluciones por funcionario', Desc: 'Muestra información acerca de la cantidad de devoluciones a funcionarios en un periodo de tiempo', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSConteos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Configuración apertura de procesos', Desc: 'Permite configurar la apertura de procesos de selección de radio', IMG: 'ta ta-box2 ta-3x', view: 'RDSConfiguracionAperturaProcesos' },
        //{ ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumROLES.Administrador, name: 'Configuración de procesos', Desc: 'Permite configurar los de procesos de radio', IMG: 'ta ta-box2 ta-3x', view: 'RDSProcesos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumROLES.Administrador, name: 'Configuración de notificaciones', Desc: 'Permite configurar las notificaciones', IMG: 'ta ta-box2 ta-3x', view: 'RDSNotificaciones' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumSOL_TYPES.OtorgaEmisoraComercial, name: 'Asignar viabilidad de emisoras comerciales', Desc: 'Permite dar viabilidad a los canales con solicitud de otorga de emisoras comerciales', IMG: 'ta ta-handshake ta-3x', view: 'RDSViabilidadCanalesOtorga' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumSOL_TYPES.OtorgaEmisoraComunitaria, name: 'Asignar viabilidad de emisoras comunitarias', Desc: 'Permite dar viabilidad a los canales con solicitud de otorga de emisoras comunitarias', IMG: 'ta ta-handshake ta-3x', view: 'RDSViabilidadCanalesOtorga' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos, name: 'Asignar viabilidad de emisoras comunitarias grupos étnicos', Desc: 'Permite dar viabilidad a los canales con solicitud de otorga de emisoras comunitarias grupos étnicos', IMG: 'ta ta-handshake ta-3x', view: 'RDSViabilidadCanalesOtorga' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumSOL_TYPES.OtorgaEmisoraDeInteresPublico, name: 'Asignar viabilidad de emisoras de interés público', Desc: 'Permite dar viabilidad a los canales con solicitud de otorga de emisoras de interés público', IMG: 'ta ta-handshake ta-3x', view: 'RDSViabilidadCanalesOtorga' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Documentos solicitados', Desc: 'Listado de documentos requeridos', IMG: 'ta ta-txt ta-3x', view: 'RDSTiposDeDocumentos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Configuración para recepción de observaciones', Desc: 'Creación y edición de configuración para recepción de observaciones', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSConfiguracionAperturaObservaciones' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Mis observaciones como coordinador', Desc: 'Listado de observaciones asignadas a mí.', IMG: 'ta ta-funcionario ta-3x', view: 'RDSObservaciones' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Generar Informe de respuestas de observaciones', Desc: 'Documento De observaciones respondidas por convocatoria', IMG: 'ta ta-word ta-3x', view: 'RDSInformeRespuestasObservaciones' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Plantillas de observaciones', Desc: 'Permite crear y editar plantillas de observaciones', IMG: 'ta ta-resolucion ta-3x', view: 'RDSObservacionesPlantillasHeader' },

        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: 0, name: 'Mis solicitudes como subdirector', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-subdirector ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: enumROLES.Funcionario, name: 'Conteo de devoluciones por funcionario', Desc: 'Muestra información acerca de la cantidad de devoluciones a funcionarios en un periodo de tiempo', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSConteos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: enumROLES.Coordinador, name: 'Conteo de devoluciones por coordinador', Desc: 'Muestra información acerca de la cantidad de devoluciones a coordinador en un periodo de tiempo', IMG: 'ta ta-pie ta-3x', view: 'RDSConteos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: 0, name: 'Configuración apertura de procesos', Desc: 'Permite configurar la apertura de procesos de selección de radio', IMG: 'ta ta-box2 ta-3x', view: 'RDSConfiguracionAperturaProcesos' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: 0, name: 'Documentos solicitados', Desc: 'Listado de documentos requeridos', IMG: 'ta ta-txt ta-3x', view: 'RDSTiposDeDocumentos' },

        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: false, EXTRA: 0, name: 'Mis solicitudes como director', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-director ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Asesor, Unico: false, EXTRA: 0, name: 'Mis solicitudes como asesor', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-asesor ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Mis solicitudes como administrador MinTIC', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-subdirector ta-3x', view: 'RDSSolicitudes' },

        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Revisor, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Asesor, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },
        { ID: 4, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Histórico de solicitudes', Desc: 'Listado de solicitudes finalizadas.', IMG: 'ta ta-archivador3 ta-3x', view: 'RDSHistoricoSolicitudes' },

        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },
        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Revisor, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },
        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },
        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },
        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },
        { ID: 5, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Estado cartera concesionario', Desc: 'Permite verificar estado de cartera del concesionario.', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSCarteraConsecionario' },

        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Administración de usuarios MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: true, EXTRA: 0, name: 'Administración de usuarios MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Asesor, Unico: true, EXTRA: 0, name: 'Administración de usuarios MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Administración de usuarios MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Administración de usuarios MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Revisor, Unico: true, EXTRA: 0, name: 'Administración de usuario MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 2, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Administración de usuario MinTIC', Desc: 'Creación y edición de usuarios del sistema MinTIC', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },

        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Funcionario, Unico: false, EXTRA: 0, name: 'Mis solicitudes como funcionario', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-funcionario ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Revisor, Unico: false, EXTRA: 0, name: 'Mis solicitudes como revisor', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-revisor ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Mis solicitudes como coordinador', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-coordinador ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: enumROLES.Funcionario, name: 'Conteo de devoluciones por funcionario', Desc: 'Muestra información acerca de la cantidad de devoluciones a funcionarios en un periodo de tiempo', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSConteos' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: 0, name: 'Mis solicitudes como subdirector', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-subdirector ta-3x', view: 'RDSSolicitudes' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Subdirector, Unico: false, EXTRA: enumROLES.Funcionario, name: 'Conteo de devoluciones por funcionario', Desc: 'Muestra información acerca de la cantidad de devoluciones a funcionarios en un periodo de tiempo', IMG: 'ta ta-Bars4 ta-3x', view: 'RDSConteos' },
        { ID: 1, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Mis solicitudes como administrador ANE', Desc: 'Listado de solicitudes asignadas a mí.', IMG: 'ta ta-subdirector ta-3x', view: 'RDSSolicitudes' },

        { ID: 3, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Administrador, Unico: true, EXTRA: 0, name: 'Administración de usuarios ANE', Desc: 'Creación y edición de usuarios del sistema ANE', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 3, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Subdirector, Unico: true, EXTRA: 0, name: 'Administración de usuarios ANE', Desc: 'Creación y edición de usuarios del sistema ANE', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 3, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Coordinador, Unico: true, EXTRA: 0, name: 'Administración de usuarios ANE', Desc: 'Creación y edición de usuarios del sistema ANE', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 3, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Revisor, Unico: true, EXTRA: 0, name: 'Administración de usuario ANE', Desc: 'Creación y edición de usuarios del sistema ANE', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },
        { ID: 3, GROUP: enumGROUPS.ANE, ROLE: enumROLES.Funcionario, Unico: true, EXTRA: 0, name: 'Administración de usuario', Desc: 'Creación y edición de usuarios del sistema ANE', IMG: 'ta ta-users1 ta-3x', view: 'RDSUsuarios' },

        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Coordinador, Unico: false, EXTRA: 0, name: 'Visualización de conexiones del sistema', Desc: 'Permite visualizar las conexiones del sistema', IMG: 'ta ta-plug ta-3x', view: 'RDSConectionsHeader' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Director, Unico: false, EXTRA: 0, name: 'Visualización de conexiones del sistema', Desc: 'Permite visualizar las conexiones del sistema', IMG: 'ta ta-plug ta-3x', view: 'RDSConectionsHeader' },


        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Edición de textos del sistema', Desc: 'Permite editar textos usados por el sistema', IMG: 'ta ta-pencil1 ta-3x', view: 'RDSTexts' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Edición de imagenes del sistema', Desc: 'Permite editar imagenes usadas por el sistema', IMG: 'ta ta-bmp ta-3x', view: 'RDSImages' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Edición de alarmas del sistema', Desc: 'Permite editar las alarmas usadas por el sistema', IMG: 'ta ta-alarm ta-3x', view: 'RDSAlarms' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Visualización de eventos del sistema', Desc: 'Permite visualizar los eventos del sistema', IMG: 'ta ta-log1 ta-3x', view: 'RDSEvents' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Visualización de conexiones del sistema', Desc: 'Permite visualizar las conexiones del sistema', IMG: 'ta ta-plug ta-3x', view: 'RDSConectionsHeader' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Plantillas de resoluciones', Desc: 'Permite crear y editar plantillas de resoluciones', IMG: 'ta ta-resolucion ta-3x', view: 'RDSResolucionesPlantillasHeader' },
        { ID: 1, GROUP: enumGROUPS.MinTIC, ROLE: enumROLES.Administrador, Unico: false, EXTRA: 0, name: 'Mis observaciones como administrador MinTIC', Desc: 'Listado de observaciones asignadas a mí.', IMG: 'ta ta-funcionario ta-3x', view: 'RDSObservaciones' },
    ];

    function Autenticar(USR_LOGIN, USR_PASSWORD) {
        SGEWeb.app.LOGIN_IN_INTERNAL(true);
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/Autenticar",
            data: JSON.stringify({
                USR_LOGIN: USR_LOGIN,
                USR_PASSWORD: USR_PASSWORD,
                APP_VERSION: SGEWeb.app.Version
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            success: function (msg) {//On Successfull service call

                var result = msg.AutenticarResult;

                if (result.USR_ID == 0) {
                    DevExpress.ui.notify('Usuario o contraseña inválida', "warning", 2000);  //NO
                    buttonIndicator.option("visible", false);
                } else if (result.USR_ID == -1) {
                    DevExpress.ui.notify('Ha ocurrido un error al procesar esta acción', "error", 2000); //NO
                    buttonIndicator.option("visible", false);
                } else {

                    ProcessUser(result);
                    SGEWeb.app.Token = result.TOKEN
                    SGEWeb.app.LOGGED_IN(true);
                    MyPopUpLogin.Show(false);
                    buttonIndicator.option("visible", false);
                    SGEWeb.app.DisabledToolBar(false);
                    Refrescar();
                }

                MyPopUpLogin.DISABLE(false);
            },
            error: function (result) { // When Service call fails
                DevExpress.ui.notify('Ha ocurrido un error al procesar esta acción', "error", 2000);
                buttonIndicator.option("visible", false);
                MyPopUpLogin.DISABLE(false);
            }
        });
    }

    function AutenticarFO(CREDENCIALES_FO) {
        SGEWeb.app.LOGIN_IN_INTERNAL(false);
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/AutenticarFO",
            data: JSON.stringify({
                CREDENCIALES_FO: CREDENCIALES_FO,
                APP_VERSION: SGEWeb.app.Version
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            success: function (msg) {//On Successfull service call
                loadingVisible(false);
                var result = msg.AutenticarFOResult;

                if (result.USR_ID == 0) {
                    DevExpress.ui.notify('Usuario o contraseña inválida', "warning", 2000);  //NO
                    ShowLogin();
                } else if (result.USR_ID == -1) {
                    DevExpress.ui.notify('Ha ocurrido un error al procesar esta acción', "error", 2000); //NO
                    ShowLogin();
                } else if (result.USR_ID == -2) {
                    DevExpress.ui.notify('La credencial caducó. Favor ingresar de nuevo desde FrontOffice.', "warning", 3000); //NO
                    ShowLogin();
                } else if (result.USR_ID == -3) {
                    DevExpress.ui.notify('Credencial inválida', "warning", 3000); //NO
                    ShowLogin();

                } else {
                    ProcessUser(result);
                    SGEWeb.app.Token = result.TOKEN
                    SGEWeb.app.LOGGED_IN(true);
                    MyPopUpLogin.Show(false);
                    SGEWeb.app.DisabledToolBar(false);
                    Refrescar();
                }

                MyPopUpLogin.DISABLE(false);
            },
            error: function (result) { // When Service call fails
                DevExpress.ui.notify('Ha ocurrido un error al procesar esta acción', "error", 2000);
                MyPopUpLogin.DISABLE(false);
                loadingVisible(false);
                ShowLogin();
            }
        });
    }

    function GetUsuarios(USR_ID_FILTER, USR_GROUP_FILTER, USR_ROLE_FILTER, USR_STATUS_FILTER) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetUsuarios",
            data: JSON.stringify({
                USR_ID_FILTER: USR_ID_FILTER,
                USR_GROUP_FILTER: USR_GROUP_FILTER,
                USR_ROLE_FILTER: USR_ROLE_FILTER,
                USR_STATUS_FILTER: USR_STATUS_FILTER
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetUsuariosResult;

                var MenuTemp = result;

                MenuTemp = result.sort(function (a, b) {
                    var x = a.USR_NAME.toLowerCase();
                    var y = b.USR_NAME.toLowerCase();
                    if (x < y) { return -1; }
                    if (x > y) { return 1; }
                    return 0;
                });

                SGEWeb.app.Users = result.filter(function (Item) {
                    return Item.USR_ROLE != 0;
                }).sort(function (a, b) {
                    var x = a.USR_NAME.toLowerCase();
                    var y = b.USR_NAME.toLowerCase();
                    if (x < y) { return -1; }
                    if (x > y) { return 1; }
                    return 0;
                });

                MenuTemp.forEach(function (entry) {
                    entry.text = entry.USR_NAME;
                });

                //if (SGEWeb.app.IDUserWeb != -1) {
                //    SGEWeb.app.User = ListObjectFindGetElement(result, 'USR_ID', SGEWeb.app.IDUserWeb);
                //}
                MenuArr(MenuTemp);

                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    LogOut();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                //loadingVisible(false);
            }
        });
    }

    function GetMainData() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMainData",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetMainDataResult

                SGEWeb.app.SOL_TYPES = result.SOL_TYPES;

                var Footer = JSON.parse(result.HtmlFooter);
                SGEWeb.app.FOOTER.Parrafo1(Footer.Parrafo1);
                SGEWeb.app.FOOTER.Parrafo2(Footer.Parrafo2);
                SGEWeb.app.FOOTER.Parrafo3(Footer.Parrafo3);
                SGEWeb.app.FOOTER.Parrafo4(Footer.Parrafo4);
                SGEWeb.app.FOOTER.Parrafo5(Footer.Parrafo5);
                SGEWeb.app.FOOTER.Parrafo6(Footer.Parrafo6);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    LogOut();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
            }
        });
    }

    function ProcesarDesistimientos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ProcesarDesistimientos",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.ProcesarDesistimientosResult;
                if (result)
                    DevExpress.ui.notify('Operación exitosa.', "success", 2000);
                else
                    DevExpress.ui.notify('Error en la operación.', "warning", 2000);
                loadingVisible(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    LogOut();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                SGEWeb.app.DisabledToolBar(true);
                Refrescar();
            }
        }
        if (SGEWeb.app.LOGGED_IN() == false) {
            var CredencialesFO = GetURLParameter('ID');
            if (CredencialesFO != undefined) {
                loadingMessage('Cargando...');
                loadingVisible(true);
                SGEWeb.app.LOGIN_IN_INTERNAL(false);
                AutenticarFO(CredencialesFO);
            } else {
                SGEWeb.app.LOGIN_IN_INTERNAL(true);
                ShowLogin();
            }
        }
    }

    function ShowLogin() {
        MyPopUpLogin.USR_LOGIN(undefined);
        MyPopUpLogin.USR_PASSWORD('Tesamerica123');
        //MyPopUpLogin.USR_LOGIN('MINTIC@lcardona');
        //MyPopUpLogin.USR_LOGIN('MINTIC@lgranada');
        //MyPopUpLogin.USR_PASSWORD(undefined);
        if (SGEWeb.app.LOGIN_IN_INTERNAL())
            MyPopUpLogin.Show(true);
    }

    var onItemClick = function (e) {
        switch (e.itemData.ID) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                SGEWeb.app.navigate({ view: e.itemData.view, id: e.itemData.ID, settings: { title: e.itemData.name, ROLE: e.itemData.ROLE, GROUP: e.itemData.GROUP, EXTRA: e.itemData.EXTRA } });
                break;
        }
    }

    function ProcessUser(e) {
        if (e.USR_ID == undefined)
            return;

        SGEWeb.app.IDUserWeb = e.USR_ID;
        SGEWeb.app.User = e;

        if (e.USR_ROLE == 0) { //Concesionario
            SGEWeb.app.Nit = e.USR_LOGIN;
        } else {
            SGEWeb.app.Nit = undefined;
        }

        UserText(e.USR_NAME);
        var USR_ROLE = GetMaxRole(e.USR_ROLE);
        if ((USR_ROLE & enumROLES.Administrador) == enumROLES.Administrador)
            IsAdministrator(true);
        else
            IsAdministrator(false);

        SGEWeb.app.UserIcon(ListObjectGetAttribute(SGEWeb.app.ROLE, 'value', GetMaxRole(e.USR_ROLE), 'Icon'));
        UserRole(GetRoles(e.USR_ROLE));

        var list = $("#ListPrincipal").dxList("instance");
        if (list) {
            list.beginUpdate();
            ListDataArr([]);

            ListData.forEach(function (entry) {
                if ((e.USR_GROUP & entry.GROUP && (e.USR_ROLE & entry.ROLE)) || (e.USR_ROLE == 0 && entry.ROLE == 0)) {
                    if (entry.Unico) {
                        if (!ListObjectExists(ListDataArr(), 'ID', entry.ID))
                            ListDataArr().push(entry);
                    } else {
                        ListDataArr().push(entry);
                    }
                }
            });

            list.reload();
            list.endUpdate();
        }

        SGEWeb.app.VisorEmail = e.USR_EMAIL;
        SGEWeb.app.VisorName = UserText();
        SGEWeb.app.VisorRole = e.USR_ROLE;

        DevExpress.ui.notify("Bienvenido " + e.USR_NAME + ' (' + UserRole() + ')', "success", 2000);
    }

    function MenuClick(e) {
        ProcessUser(e.itemData);
    }

    function Refrescar() {
        GetUsuarios(-1, -1, 0xFF, 1);
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function LogOut() {

        IsAdministrator(false);

        var list = $("#ListPrincipal").dxList("instance");
        if (list) {
            list.beginUpdate();
            ListDataArr([]);
            list.reload();
            list.endUpdate();
        }
        UserRole('');
        SGEWeb.app.UserIcon('');
        UserText('');

        for (var prop in SGEWeb.app.viewCache._viewCache._viewCache._cache) {
            var myView = SGEWeb.app.viewCache._viewCache._viewCache.getView(prop);
            if (myView.uri != 'RDSPrincipal')
                SGEWeb.app.viewCache.removeView(prop);
        }

        for (var prop in SGEWeb.app.navigationManager.navigationStacks) {
            var nItems = SGEWeb.app.navigationManager.navigationStacks[prop].items.length;
            for (var i = 1; i < nItems; i++) {
                SGEWeb.app.navigationManager.navigationStacks[prop].items.pop();
            }
            SGEWeb.app.navigationManager.navigationStacks[prop].currentIndex = 0;
        }

        SGEWeb.app.IDUserWeb = -1;
        SGEWeb.app.Token = undefined;
        SGEWeb.app.LOGGED_IN(false);

        ShowLogin();
    }

    function OnAutenticar(e) {
        MyPopUpLogin.DISABLE(true);
        if (IsNullOrEmpty(MyPopUpLogin.USR_LOGIN())) {
            DevExpress.ui.notify("El usuario es requerido", "warning", 2000);
            MyPopUpLogin.DISABLE(false);
            return;
        }
        if (IsNullOrEmpty(MyPopUpLogin.USR_PASSWORD())) {
            DevExpress.ui.notify("La contraseña es requerida", "warning", 2000);
            MyPopUpLogin.DISABLE(false);
            return;
        }
        loadingMessage('Cargando...');

        buttonIndicator.option("visible", true);
        Autenticar(MyPopUpLogin.USR_LOGIN(), MyPopUpLogin.USR_PASSWORD());
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 13) {
            $('#btnLogin').dxButton('instance').focus();
            OnAutenticar();
        }
        return true;
    }

    function OnProcessDesistimientos() {
        loadingMessage('Procesando...');
        loadingVisible(true);
        ProcesarDesistimientos();

    }

    function OnTemplateAutenticar(data, container) {
        $("<i class='dx-icon ta ta-login ta-lg'></i><span class='dx-button-text'>" + data.text + "</span><div class='button-indicator' style='height: 32px; width: 32px; display: inline-block; vertical-align: text-top; margin-left: 10px; margin-top:-4px;'></div>").appendTo(container);
        buttonIndicator = container.find(".button-indicator").dxLoadIndicator({
            visible: false
        }).dxLoadIndicator("instance");
    }

    function OnAmbientePruebas() {
        DevExpress.ui.notify('Ambiente de pruebas. Versión ' + SGEWeb.app.Version, "success", 3000);
    }

    function OnActiveUser() {
        DevExpress.ui.notify("Usuario activo: " + UserText() + ' (' + UserRole() + ')', "success", 2000);
    }

    function OnRegistrarse() {
        MyPopUpLogin.Show(false);
        buttonIndicator.option("visible", false);
        SGEWeb.app.DisabledToolBar(false);

        SGEWeb.app.navigate({ view: "RDSRegistroUsuario", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create } });
    }

    SGEWeb.app.CallBackActiveUser = OnActiveUser;

    var locale = 'es';

    Globalize.load(getIdiomaDates(locale));
    Globalize.loadMessages(getIdiomaMessages(locale));
    Globalize.load(Idiomasupplemental);
    Globalize.locale(locale);

    GetMainData();

    var viewModel = {
        viewShown: handleViewShown,
        ListDataArr: ListDataArr,
        onItemClick: onItemClick,
        MenuArr: MenuArr,
        MenuVisible: ko.observable(false),
        MenuClick: MenuClick,
        UserText: UserText,
        UserRole: UserRole,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        MyPopUpLogin: MyPopUpLogin,
        OnAutenticar: OnAutenticar,
        LogOut: LogOut,
        onPwdKeyDown: onPwdKeyDown,
        OnProcessDesistimientos: OnProcessDesistimientos,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        IsAdministrator: IsAdministrator,
        OnTemplateAutenticar: OnTemplateAutenticar,
        OnAmbientePruebas: OnAmbientePruebas,
        OnRegistrarse: OnRegistrarse,
    };

    return viewModel;
};