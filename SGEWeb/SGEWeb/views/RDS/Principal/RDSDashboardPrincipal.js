﻿SGEWeb.RDSDashboardPrincipal = function (params) {
    "use strict";
    var loadingVisible = ko.observable(false);

    var dateIni = ko.observable(new Date(new Date().getFullYear(), 0, 1));
    var dateFin = ko.observable(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
    var departamentos = ko.observableArray([])
    var municipios = ko.observableArray([])
    var tiposTecnologia = ["AM", "FM"];
    var tiposTramite = ko.observableArray([])

    var selectedDepartamento = ko.observable(-1);
    var selectedMunicipio = ko.observable(-1);
    var selectedTipoTecnologia = ko.observable();
    var selectedTipoTramite = ko.observable(-1);

    var solicitudesxTramiteGraph = ko.observableArray([{}]);
    var etapaSolicitudGraph = ko.observableArray([{}]);
    var tipoEmisoraGraph = ko.observableArray([{}]);
    var concesionariosPendienteTramite = ko.observableArray([{}]);
    var concesionesVencimientoGraph = ko.observableArray([{}]);
    var concesionesVencimientoDetail = ko.observableArray([{}]);

    var recaudos = ko.observableArray([{}]); //VALIDAR

    var contadorVisible = 0; //Cantidad de elementos que se cargaran en la vista (Varia si es refresco de pagina o filtro).
    var tipoCarga = 0; //Si es carga por refresco por pagina o carga por filtro.

    var bytesExcel = ko.observableArray([{}]); //Arreglo de bytes recuperado del backend y con el que se genera el excel que se descarga del lado cliente.



    function GetDepartamentos() {
        $.ajax({
            type: "GET", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDepartamentosDashboard",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                departamentos(msg.GetDepartamentosDashboardResult);
                evaluarContador();
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetMunicipiosXDepto(Code_Depto) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMunicipiosDashboard",
            data: JSON.stringify({
                departamentoID: Code_Depto
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                municipios(msg.GetMunicipiosDashboardResult);
                evaluarContador();
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    var onDepartamentoChanged = ko.computed(function () {
        if (selectedDepartamento() == null) {
            selectedDepartamento(-1);
            selectedMunicipio(-1);
        }

        if (selectedDepartamento() != -1) {
            GetMunicipiosXDepto(selectedDepartamento());
        }
        else if (selectedDepartamento() == -1) {
            municipios([]);
        }
    });

    function GetTipoTramite() {
        $.ajax({
            type: "GET", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetTipoTramite",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                tiposTramite(msg.GetTipoTramiteResult);
                evaluarContador();
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function GetSolicitudesxTramiteGraph() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesxTramiteGraph",
            data: JSON.stringify({
                fechaIni: dateIni(),
                fechaFin: dateFin(),
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia(),
                codeTramite: selectedTipoTramite()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                solicitudesxTramiteGraph(msg.GetSolicitudesxTramiteGraphResult);
                evaluarContador();
                mostrarMensajeGraficaVacia(solicitudesxTramiteGraph(), "TipoTramite");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetSolicitudesxEtapaGraph() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesxEtapaGraph",
            data: JSON.stringify({
                fechaIni: dateIni(),
                fechaFin: dateFin(),
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia(),
                codeTramite: selectedTipoTramite()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                etapaSolicitudGraph(msg.GetSolicitudesxEtapaGraphResult);
                evaluarContador();
                mostrarMensajeGraficaVacia(etapaSolicitudGraph(), "EtapaSolicitud");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function GetConcesionesxTipoEmisoraGraph() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConcesionesxTipoEmisoraGraph",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                tipoEmisoraGraph(msg.GetConcesionesxTipoEmisoraGraphResult);

                var totalEmisoras = 0;
                for (var i = 0; i < tipoEmisoraGraph().length; i++)
                    totalEmisoras += tipoEmisoraGraph()[i].cantidad;
                //tipoEmisoraGraph().map(etipoEmisoraGraphlemnto => { totalEmisoras += elemnto.cantidad; });
                $("#lblTotalEmisoras").text(totalEmisoras);

                evaluarContador();
                mostrarMensajeGraficaVacia(tipoEmisoraGraph(), "TipoEmisora");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetReportesPendientesxIngresar() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetReportesPendientesxIngresar",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                concesionariosPendienteTramite(msg.GetReportesPendientesxIngresarResult);
                evaluarContador();
                mostrarMensajeGraficaVacia(concesionariosPendienteTramite(), "PendientesTramite");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function GetConcesionesxVencimientoGraph() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConcesionesVencimientoGraph",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                concesionesVencimientoGraph(msg.GetConcesionesVencimientoGraphResult);
                evaluarContador();
                mostrarMensajeGraficaVacia(concesionesVencimientoGraph(), "ConcesionesVencimiento");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetConcesionesxVencimientoDetail() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConcesionesVencimientoDetail",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                concesionesVencimientoDetail(msg.GetConcesionesVencimientoDetailResult);
                evaluarContador();
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    //VALIDAR
    function GetRecaudos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetRecaudos",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                recaudos(msg.GetRecaudosResult);
                evaluarContador();
                mostrarMensajeGraficaVacia(concesionesVencimientoGraph(), "Recaudos");
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }



    function descargarSolicitudes() {
        loadingVisible(true);
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ExportarSolicitudes",
            data: JSON.stringify({
                fechaIni: dateIni(),
                fechaFin: dateFin(),
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia(),
                codeTramite: selectedTipoTramite()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                bytesExcel(msg.ExportarSolicitudesResult);
                generarArchivoDescargable(bytesExcel(), "Detalle Solicitudes.xlsx");
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function descargarConcesiones() {
        loadingVisible(true);
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ExportarConcesiones",
            data: JSON.stringify({
                codeDepta: selectedDepartamento(),
                codeMuni: selectedMunicipio(),
                codeTecno: selectedTipoTecnologia()
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                bytesExcel(msg.ExportarConcesionesResult);
                generarArchivoDescargable(bytesExcel(), "Detalle Concesiones.xlsx");
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                //SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function refrescar() {
        loadingVisible(true);
        contadorVisible = 0;
        tipoCarga = 8;

        GetDepartamentos();
        GetTipoTramite();

        GetSolicitudesxTramiteGraph();
        GetSolicitudesxEtapaGraph();

        GetConcesionesxTipoEmisoraGraph();
        GetReportesPendientesxIngresar();

        GetConcesionesxVencimientoGraph();
        GetConcesionesxVencimientoDetail();

        //VALIDAR
        //GetRecaudos();
    }

    function filtrar() {
        loadingVisible(true);
        contadorVisible = 0;
        tipoCarga = 6;
        selectedMunicipio(selectedMunicipio() == null ? -1 : selectedMunicipio())
        selectedTipoTramite(selectedTipoTramite() == null ? -1 : selectedTipoTramite())
        GetSolicitudesxTramiteGraph();
        GetSolicitudesxEtapaGraph();
        GetConcesionesxTipoEmisoraGraph();
        GetReportesPendientesxIngresar();
        GetConcesionesxVencimientoGraph();
        GetConcesionesxVencimientoDetail();
    }


    function generarArchivoDescargable(bytesExcel, nombreExcel) {
        var byteArray = new Uint8Array(bytesExcel);
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([byteArray], { type: 'application/octet-stream' }));
        a.download = nombreExcel;

        // Append anchor to body.
        document.body.appendChild(a)
        a.click();
        // Remove anchor from body
        document.body.removeChild(a)
    }

    function evaluarContador() {
        contadorVisible++;
        if (contadorVisible == tipoCarga) {
            loadingVisible(false);
            contadorVisible = 0;
            tipoCarga = 0;
        }
    }

    function mostrarMensajeGraficaVacia(dataSource, idElemento) {
        if (dataSource.length == 0) {
            $("#divEmpty" + idElemento).attr("style", "display:block;");
            $("#chart" + idElemento).attr("style", "display:none;");
        } else {
            $("#divEmpty" + idElemento).attr("style", "display:none")
            $("#chart" + idElemento).attr("style", "display:block;");

        }
    }
    function AutenticarVisorDeEspectro() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/AutenticarVisorDeEspectro",
            data: JSON.stringify({

                USR_EMAIL: SGEWeb.app.VisorEmail,
                USR_NAME: SGEWeb.app.VisorName,
                USR_ROLE: SGEWeb.app.VisorRole,
                USR_IDENT: "",
                SECRETE_KEY: "F4209FA7-FAF6-4BF0-8A5E-783BE439DF9E"

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.AutenticarVisorDeEspectroResult;
                //DevExpress.ui.notify('Token Autenticación' + result.Token, "info", 5000);
                window.open(SGEWeb.app.UrlVisorDeEspectro + result.Token);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    LogOut();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
            }
        });
    }

    refrescar();

    var viewModel = {
        LoadingVisible: loadingVisible,

        DateIni: dateIni,
        DateFin: dateFin,
        Departamentos: departamentos,
        Municipios: municipios,
        TiposTecnologia: tiposTecnologia,
        TiposTramite: tiposTramite,

        AutenticarVisorDeEspectro: AutenticarVisorDeEspectro,
        SelectedDepartamento: selectedDepartamento,
        SelectedMunicipio: selectedMunicipio,
        SelectedTipoTecnologia: selectedTipoTecnologia,
        SelectedTipoTramite: selectedTipoTramite,

        SolicitudesxTramiteGraph: solicitudesxTramiteGraph,
        EtapaSolicitudGraph: etapaSolicitudGraph,
        TipoEmisoraGraph: tipoEmisoraGraph,
        ConcesionariosPendienteTramite: concesionariosPendienteTramite,
        ConcesionesVencimientoGraph: concesionesVencimientoGraph,
        ConcesionesVencimientoDetail: concesionesVencimientoDetail,
        Recaudos: recaudos, //VALIDAR

        OnDepartamentoChanged: onDepartamentoChanged,
        Refrescar: refrescar,
        Refrescar1: refrescar,
        Filtrar: filtrar,
        DescargarSolicitudes: descargarSolicitudes,
        DescargarConcesiones: descargarConcesiones,

    };
    return viewModel;
};

