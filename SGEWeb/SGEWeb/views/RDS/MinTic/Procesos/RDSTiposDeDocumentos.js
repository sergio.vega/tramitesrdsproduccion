﻿SGEWeb.RDSTiposDeDocumentos = function (params) {
    "use strict";
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var ConfiguracionesTipoDocumentoProcesoOtorga = ko.observableArray([]);
    var GuardarDisabled = ko.observable(false);
    var isLoading = ko.observable(false);
    var MyPopUp = {
        Doc: ko.observable({}),
        Show: ko.observable(false),
        Title: ko.observable(''),
        CRUDType: ko.observable(0),
    };

    function GetConfiguracionesDocumentos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConfiguracionesProcesos",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConfiguracionesProcesosResult;
                ConfiguracionesTipoDocumentoProcesoOtorga(result.TiposDocumentos);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }
    function CRUDDoc(Docum) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDDoc",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Docum: Docum
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDDocResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    MyPopUp.Show(false);
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                }
                GuardarDisabled(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSConfiguracionProcesos') {
            if (!isLoading())
                loadingVisible(false);
        }

        SGEWeb.app.DisabledToolBar(false);
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }
    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSConfiguracionProcesos') {
                switch (e.column.name) {
                    case 'Editar':

                        MyPopUp.Doc(ko.mapping.fromJS(ko.toJS(e.data)));
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title('Editar Documento');
                        MyPopUp.Show(true);
                    break;
                }
            }
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionesDocumentos();
        isLoading(true);
    }
    function NewDoc() {
        var Doc = {
            ID_ANX_SOL: '0',
            TIPO_ANX_SOL: '',
            DES_TIPO_ANX_SOL: '',
            EXTENSIONS: '.pdf,.doc,.docx',
            MAX_SIZE: '30',
            MAX_NAME_LENGTH: '50',
        };

        MyPopUp.Doc(ko.mapping.fromJS(ko.toJS(Doc)));
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nuevo Documento");
        MyPopUp.Show(true);
    }
    function OnGuardar() {
        
        if (IsNullOrEmpty(MyPopUp.Doc().TIPO_ANX_SOL())) {
            DevExpress.ui.notify("El tipo de documento es requerido", "error", 2000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.Doc().EXTENSIONS())) {
            DevExpress.ui.notify("Las del documento son requeridas", "error", 2000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.Doc().MAX_SIZE())) {
            DevExpress.ui.notify("El tamaño de documento es requerida", "error", 2000);
            return;
        }
        if (IsNullOrEmpty(MyPopUp.Doc().MAX_NAME_LENGTH())) {
            DevExpress.ui.notify("la longitud de nombre de documento es requerida", "error", 2000);
            return;
        }

        var DocAux = GetElementIgnoringDiacriticsUpperCase(ConfiguracionesTipoDocumentoProcesoOtorga(), 'TIPO_ANX_SOL', MyPopUp.Doc().TIPO_ANX_SOL());
        if (DocAux != null && DocAux.ID_ANX_SOL != MyPopUp.Doc().ID_ANX_SOL()) {
            DevExpress.ui.notify('Este tipo de documento ya se encuentra registrado', "warning", 3000);
            return;
        }

        
        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var Docum = ko.toJS(MyPopUp.Doc());
        Docum.DOC_CRUD = MyPopUp.CRUDType();
        CRUDDoc(Docum);
    }

    Refrescar();
    var viewModel = {
        GetConfiguracionesDocumentos: GetConfiguracionesDocumentos,
        ConfiguracionesTipoDocumentoProcesoOtorga: ConfiguracionesTipoDocumentoProcesoOtorga,
        Refrescar: Refrescar,
        loadingMessage: loadingMessage,
        isLoading: isLoading,
        loadingVisible: loadingVisible,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        NewDoc: NewDoc,
        MyPopUp: MyPopUp,
        OnGuardar: OnGuardar,
        GuardarDisabled: GuardarDisabled,

    };

    return viewModel;
};