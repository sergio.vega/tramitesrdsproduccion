﻿SGEWeb.RDSConfiguracionAperturaProcesos = function (params) {
    "use strict";
    var ConfiguracionesProcesosOtorga = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var isLoading = ko.observable(false);
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var Viabilidades = ko.observableArray([]);

    function GetConfiguracionesProcesos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConfiguracionesProcesos",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConfiguracionesProcesosResult;
                ConfiguracionesProcesosOtorga(result.Configuraciones);
                SGEWeb.app.TiposDocumentosOtorga = result.TiposDocumentos;
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetViabilidades(ConfiguracionProcesoOtorga) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetViabilidades",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ConfiguracionProcesoOtorga: ConfiguracionProcesoOtorga
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                
                var result = msg.GetViabilidadesResult;
                Viabilidades(result);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
                var continuar = true;
                var i = 0;
                for (i = 0; i < result.length; i++) {

                    if (result[i].IdEstadoViabilidad != 3) {
                        continuar = false;
                        break;
                    }
                }
                if (continuar) {

                    SGEWeb.app.navigate({ view: "RDSConfigurarProceso", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Update, ROLE: ROLE, GROUP: GROUP } });
                }
                else {

                    DevExpress.ui.notify('La viabilidad de todas las solicitudes no se ha realizado', "error", 5000);
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSConfiguracionProcesos') {
            if (!isLoading())
                loadingVisible(false);
        }

        SGEWeb.app.DisabledToolBar(false);
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarProceso':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar proceso')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarProceso':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar proceso')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'FechaInicioRecepcionSolicitudes':
                if (e.data.SOL_TYPE == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
                    $('<div/>').text('Permanente').appendTo(container);
                }
                else {
                    $('<div/>').text(e.text).appendTo(container);
                }
                break;
            case 'FechaFinRecepcionSolicitudes':
                if (e.data.SOL_TYPE == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
                }
                else {
                    $('<div/>').text(e.text).appendTo(container);
                }
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSConfiguracionProcesos') {
                switch (e.column.name) {
                    case 'ConfigurarProceso':


                        SGEWeb.app.ConfiguracionProcesoOtorga = e.data;
                        GetViabilidades(SGEWeb.app.ConfiguracionProcesoOtorga);

                       // SGEWeb.app.navigate({ view: "RDSConfigurarProceso", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Update, ROLE: ROLE, GROUP: GROUP } });
                        break;
                }

            }
        }
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }


    function ViabilidadAutomatica(e) {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionesProcesos();
        isLoading(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionesProcesos();
        //GetViabilidades();
        isLoading(true);
    }

    Refrescar();


    var viewModel = {
        viewShown: handleViewShown,
        ConfiguracionesProcesosOtorga: ConfiguracionesProcesosOtorga,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        ViabilidadAutomatica: ViabilidadAutomatica,
        Viabilidades: Viabilidades,
    };

    return viewModel;
};