﻿SGEWeb.RDSCalificarSolicitud = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_UID = JSON.parse(params.settings.substring(5)).SOL_UID;
    var Evaluaciones = JSON.parse(params.settings.substring(5)).Evaluaciones;

    var Factores = JSON.parse(Evaluaciones);
    var TodosCalificados = ko.observable(false);
    var Puntaje = ko.observable(0);

    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) };

    var MyPopUp = {
        Datos: {
            Id: undefined,
            Descripcion: ko.observable(''),
            Min: ko.observable(0),
            Max: ko.observable(100),
            Porcentaje: ko.observable(1),
            Calificacion: ko.observable(null),
            Peso: ko.observable(null),
        },
        Show: ko.observable(false),
        Title: ko.observable(''),
        CRUDType: ko.observable(0)
    };
    var GridRDSFactores = undefined;

    var BotonCancelar = [
        { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la calificación?' },
        { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: '¿Desea cancelar la creación de la calificación?' },
        { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
        { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: '¿Desea cancelar la modificación de la calificación?' },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
        { CRUD: 'Dummy', Text: 'Crear calificación', Icon: 'floppy' },
        { CRUD: enumCRUD.Create, Text: 'Crear calificación', Icon: 'floppy', Method: CrearCalificacion },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: 'Actualizar calificación', Icon: 'floppy', Method: ActualizarCalificacion },
        { CRUD: enumCRUD.Delete, Text: 'Eliminar calificación', Icon: 'floppy' },
    ];

    function CrearCalificacion() {
    }

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    function EvaluateTitle2(STATE_ID, COMMENT) {
        var Title = null;
        switch (STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var AlmacenarConfiguracionDisable = ko.computed(function () {

        if (TodosCalificados() == true) {
            return false;
        }
        else {
            return true;
        }
    });

    function ActualizarCalificacionSolicitud(CalificacionSolicitud, puntaje) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ActualizarCalificacionSolicitud",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                SOL_UID: SOL_UID,
                CalificacionSolicitud: CalificacionSolicitud,
                Puntaje: puntaje,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.ActualizarCalificacionSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    var res = DevExpress.ui.dialog.alert('La calificación de la solicitud se ha realizado correctamente', SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function CargarFile(e) {
        var FileObject = e.data.FileObject;
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;

            $("#GridOtorgaComercialFiles").dxDataGrid('instance').refresh();
        }
    }

    function OpenFile(e) {
        var FileObject = e.data.FileObject;

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + FileObject.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    
    function ActualizarCalificacion() {
        var calificacionSolicitud = JSON.stringify(Factores);

        ActualizarCalificacionSolicitud(calificacionSolicitud, Puntaje());
    }

    function CancelarRegistro() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function OnVolver() {
        SGEWeb.app.navigationManager.back();
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function onKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }


    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'COMMENT':

                $('<i/>').addClass('ta ta-question-circle ta-2x')
                    .prop('title', 'Archivo(s) permitido(s) (' + e.data.EXTENSIONS.join() + ') y Tamaño máximo del archivo (' + e.data.MAX_SIZE + 'MB).')
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('float', 'right')
                    .css('color', '#6f91cb')
                    .appendTo(container);

                $('<i/>').addClass('ta-2x')
                    .addClass(ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'icon', ''))
                    .prop('title', 'Ver archivo')
                    .prop('visibility', (e.data.UID != null ? 'visible' : 'hidden'))
                    .bind('click', { FileObject: e.data }, OpenFile)
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-right', '5px')
                    .css('cursor', 'pointer')
                    .css('float', 'right')
                    .css('color', ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'color', ''))
                    .appendTo(container);

                break;
            case 'Upload':
                var label = $('<label/>');
                label.addClass('custom-file-upload')
                    .prop('title', 'Seleccionar archivo')
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .css('position', 'relative')
                    .css('width', '400px')
                    .css('margin-top', '10px');
                label.disabled = false;

                var input = $('<input/>');
                input.prop('id', e.data.ID)
                    .prop('type', 'file')
                    .prop('accept', e.data.EXTENSIONS.join())
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .bind('change', { FileObject: e.data }, CargarFile)
                    .css('z-index', '999')
                    .css('position', 'absolute')
                    .css('top', '0px')
                    .css('visibility', 'hidden');
                input.appendTo(label);

                var info = $('<i/>');
                info.addClass('ta ta-attachment ta-lg')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(label);

                var span = $('<span/>');
                span.prop('textContent', e.data.NAME())
                    .appendTo(label);

                label.appendTo(container);
                break;

            case 'DESC':
                var div = $('<b/>');
                div.prop('textContent', e.data.DESC())
                    .css('vertical-align', 'middle')
                    .css('fontWeight', 'bold')
                    .css('word-wrap', 'break-word')
                    .appendTo(container);

                container.css('vertical-align', 'middle')
                    .css('word-wrap', 'break-word');
                break;

            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                    .prop('title', 'Eliminar')
                    .css('cursor', 'pointer')
                    .css('color', e.data.TXT_SYSTEM ? 'lightgrey' : 'black')
                    .appendTo(container);
                break;
        }
    }

    function OnTemplateAutenticar(data, container) {
        $("<i class='dx-icon ta ta-caret ta-lg'></i><span class='dx-button-text'>" + data.text + "</span><div class='button-indicator' style='height: 32px; width: 32px; display: inline-block; vertical-align: text-top; margin-left: 10px; margin-top:-4px;'></div>").appendTo(container);
        buttonIndicator = container.find(".button-indicator").dxLoadIndicator({
            visible: false
        }).dxLoadIndicator("instance");
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 13) {
            $('#btnContinuar').dxButton('instance').focus();
            OnContinuar();
        }
        return true;
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }


    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSFactores') {
                switch (e.column.name) {
                    case 'Editar':
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title("Editar factor de evaluación");
                        MyPopUp.Datos.Id = e.data.Id;
                        MyPopUp.Datos.Descripcion(e.data.Descripcion);
                        MyPopUp.Datos.Min(e.data.Min);
                        MyPopUp.Datos.Max(e.data.Max);
                        MyPopUp.Datos.Porcentaje(e.data.Porcentaje);
                        MyPopUp.Datos.Calificacion(e.data.Calificacion);
                        MyPopUp.Show(true);
                        break;
                }
            }
        }
    }

    function CellDblClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSTexts') {
                switch (e.column.name) {
                    case 'ID':
                        copyToClipboard(e.data.TXT_ID);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'TYPE':
                        copyToClipboard(e.data.TXT_TYPE);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'GROUP':
                        copyToClipboard(e.data.TXT_GROUP);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                    case 'TEXT':
                        copyToClipboard(e.data.TXT_TEXT);
                        DevExpress.ui.notify("Se copió la celda al Clipboard", "success", 2000);
                        break;
                }
            }
        }

    }

    function NewFactor() {
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nuevo factor de evaluación");
        MyPopUp.Datos.Id = Factores.length;
        MyPopUp.Datos.Descripcion("");
        MyPopUp.Datos.Min(0);
        MyPopUp.Datos.Max(100);
        MyPopUp.Datos.Porcentaje(1);
        MyPopUp.Datos.Calificacion(null);
        MyPopUp.Show(true);
    }

    function OnGuardar() {

        if (MyPopUp.Datos.Max() < MyPopUp.Datos.Calificacion() || MyPopUp.Datos.Min() > MyPopUp.Datos.Calificacion()) {
            DevExpress.ui.notify('El valor debe esta etre el mínimo y el máximo.', "warning", 3000);
            return;
        }

        MyPopUp.Show(false);

        switch (MyPopUp.CRUDType()) {
            case 3:
                var factorAEditar = Factores.filter(function (obj) { return obj.Id === MyPopUp.Datos.Id })[0];

                factorAEditar.Calificacion = MyPopUp.Datos.Calificacion();
                factorAEditar.Peso = factorAEditar.Calificacion * factorAEditar.Porcentaje / factorAEditar.Max;
                break;
        }

        GridRDSFactores.option('dataSource', Factores);
        ActualizarVariablesCalificacion();
    }

    function ActualizarVariablesCalificacion() {
        var todosCalificados = true;
        Factores.forEach(function (factor) {
            if (IsNullOrEmpty(factor.Calificacion)) {
                todosCalificados = false;
            }
        });
        TodosCalificados(todosCalificados);

        var puntaje = 0;
        Factores.forEach(function (factor) {
            if (!IsNullOrEmpty(factor.Peso)) {
                puntaje += factor.Peso;
            }
        });
        Puntaje(puntaje);
    }


    function handleViewShown(e) {
        if (GridRDSFactores == undefined) {
            GridRDSFactores = $("#GridRDSFactores").dxDataGrid('instance');
            GridRDSFactores.option('dataSource', Factores);
            ActualizarVariablesCalificacion();
        }
    }

    function onContentReady(e) {
        SGEWeb.app.DisabledToolBar(false);
    }

    var viewModel = {
        viewShown: handleViewShown,
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CancelarRegistro: CancelarRegistro,
        AlmacenarConfiguracionDisable: AlmacenarConfiguracionDisable,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        onKeyDown: onKeyDown,
        onCellTemplate: onCellTemplate,
        OnTemplateAutenticar: OnTemplateAutenticar,
        onPwdKeyDown: onPwdKeyDown,
        OnVolver: OnVolver,
        keyPressAction: keyPressAction,
        onHeaderTemplate: onHeaderTemplate,
        CellClick: CellClick,
        CellDblClick: CellDblClick,
        onContentReady: onContentReady,
        MyPopUp: MyPopUp,
        NewFactor: NewFactor,
        OnGuardar: OnGuardar,
        Puntaje: Puntaje,
    };

    return viewModel;
};