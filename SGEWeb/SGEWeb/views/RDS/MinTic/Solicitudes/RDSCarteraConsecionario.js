﻿SGEWeb.RDSCarteraConsecionario = function (params) {
    "use strict";
    var NitConsecionario = ko.observable();
    var EstadoCartera = ko.observable();
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }

    function VerificarCartera() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage("Buscando estado cartera...");
        GuardarDisabled(true);
        VerificarCarteraAlDia(NitConsecionario())
    }

    function VerificarCarteraAlDia(NIT) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/VerificarCarteraAlDia",
            data: JSON.stringify({
                CUS_IDENT: NIT
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.VerificarCarteraAlDiaResult;
                if (result.Error == true) {
                    EstadoCartera(result.ErrorMessage);
                    //Financiero.FIN_SEVEN_ALDIA(undefined);
                } else {
                    if (result.Estado == true)
                        EstadoCartera("NO posee obligaciones vencidas");
                    else
                        EstadoCartera("SI posee obligaciones vencidas");

                }

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    2
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function CancelarSolicitud() {
        SGEWeb.app.navigationManager.back();
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }


    var viewModel = {
        NitConsecionario: NitConsecionario,
        EstadoCartera: EstadoCartera,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        keyPressAction: keyPressAction,
        VerificarCartera: VerificarCartera,
        CancelarSolicitud: CancelarSolicitud,
    };

    return viewModel;
};