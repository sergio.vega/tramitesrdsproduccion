﻿SGEWeb.RDSFuncionarioSolicitudProrroga = function (params) {
    "use strict";

    var RadicadoFecha = ko.observable();
    var RadicadoCodigo = ko.observable();
    var RadicadoEstado = ko.observable(false);
    var ExpedinteCodigo = ko.observable();
    var bEnviarCorreoConcesionario = ko.observable(false);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    
    var ExpedienteSelected = ko.observable({ SERV_ID: -1, SERV_NUMBER: '', SERV_NAME: '', DEPTO: '', CITY: '', CALL_SIGN: '', STOP_DATE: '' });

    var FileCartaProrroga = {
        NAME: ko.observable('Seleccionar archivo'),
        DATA: undefined,
        CONTENT_TYPE_LOCAL: ko.observable(''),
        LOADED: ko.observable(false),
        TYPE_ID: 15,
        EXTENSIONS: ['.pdf', '.doc', '.docx'],
        MAX_SIZE: 30,
        MAX_NAME_LENGTH: 50
    };


    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);

    var CrearSolicitudDisable = ko.computed(function () {
        if (FileCartaProrroga.LOADED() && !GuardarDisabled() && RadicadoEstado() && RadicadoFecha() != undefined && ExpedienteSelected().SERV_ID != -1)
            return false;
        return true;
    });

    function CrearFuncionarioSolicitudProrroga(RadicadoCodigo, RadicadoFecha, Expediente, bEnviarCorreoConcesionario, Files) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearFuncionarioSolicitudProrroga",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                RadicadoCodigo: RadicadoCodigo,
                RadicadoFecha: RadicadoFecha,
                Expediente: Expediente,
                bEnviarCorreoConcesionario: bEnviarCorreoConcesionario,
                Files: Files
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearFuncionarioSolicitudProrrogaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function GetExpediente(ExpedienteCodigo) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetExpediente",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ExpedienteCodigo: ExpedienteCodigo
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetExpedienteResult;

                if (result != null) {
                    ExpedienteSelected(result);
                } else {
                    ExpedienteSelected({ SERV_ID: -1, SERV_NUMBER: '', SERV_NAME: '', DEPTO: '', CITY: '', CALL_SIGN: '', STOP_DATE: '' });
                    DevExpress.ui.notify('El expediente no fue encontrado.', "warning", 3000);
                }
                

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function GetRadicado(RadicadoCodigo) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetRadicado",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                RadicadoCodigo: RadicadoCodigo
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetRadicadoResult;

                if (result.Error == true) {
                    DevExpress.ui.notify('El sistema de radicado no está disponible.', "error", 3000);
                    RadicadoEstado(false);
                } else {
                    RadicadoEstado(result.Estado);
                    if (result.Estado == false) {
                        DevExpress.ui.notify(result.EstadoMessage, "warning", 3000);
                    }
                }


                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        //if (!/[0-9]/.test(str))
        //    event.preventDefault();
    }

    function CargarFile(FileObject, data, e) {
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;
        }
    }

    function CrearSolicitud() {

        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage("Creando solicitud...");
        GuardarDisabled(true);

        var Files = [];

        Files.push({ TYPE_ID: FileCartaProrroga.TYPE_ID, NAME: FileCartaProrroga.NAME(), CONTENT_TYPE: FileCartaProrroga.CONTENT_TYPE_LOCAL(), DATA: FileCartaProrroga.DATA });

        CrearFuncionarioSolicitudProrroga(RadicadoCodigo(), RadicadoFecha().toString('yyyyMMdd'), ExpedienteSelected(), bEnviarCorreoConcesionario(), Files);
    }

    function CancelarSolicitud() {
        
        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea cancelar la solicitud?'), SGEWeb.app.Name);
        result.done(function (dialogResult) {
            if (dialogResult) {
                SGEWeb.app.navigationManager.back();
            }
        });

    }

    function BuscarRadicado() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Buscando radicado...");
        loadingVisible(true);
        GetRadicado(RadicadoCodigo());
    }

    function BuscarExpediente() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Buscando expediente...");
        loadingVisible(true);
        GetExpediente(ExpedinteCodigo());
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    var viewModel = {
        RadicadoCodigo: RadicadoCodigo,
        RadicadoFecha: RadicadoFecha,
        RadicadoEstado:RadicadoEstado,
        onPwdKeyDown: onPwdKeyDown,
        keyPressAction: keyPressAction,
        ExpedinteCodigo: ExpedinteCodigo,
        FileCartaProrroga: FileCartaProrroga,
        CargarFile: CargarFile,
        loadingVisible: loadingVisible,
        loadingMessage:loadingMessage,
        GuardarDisabled: GuardarDisabled,
        CrearSolicitudDisable:CrearSolicitudDisable,
        CrearSolicitud:CrearSolicitud,
        CancelarSolicitud: CancelarSolicitud,
        BuscarRadicado: BuscarRadicado,
        BuscarExpediente: BuscarExpediente,
        ExpedienteSelected: ExpedienteSelected,
        bEnviarCorreoConcesionario: bEnviarCorreoConcesionario,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility
    };

    return viewModel;
};