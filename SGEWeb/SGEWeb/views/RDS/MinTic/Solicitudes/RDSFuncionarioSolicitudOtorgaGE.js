﻿SGEWeb.RDSFuncionarioSolicitudOtorgaGE = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var CRUD = enumCRUD.Create;
    var SOL_TYPE_ID = enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos;
    var SOL_TYPE = ListObjectFindGetElement(SGEWeb.app.SOL_TYPES, 'SOL_TYPE_ID', SOL_TYPE_ID);
    var title = ko.observable(SOL_TYPE.SOL_TYPE_NAME);
    var Emails = [];
    var GuardarDisabled = ko.observable(false);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var DepartamentosPlanBro = ko.observableArray([]);
    var SelectedDepartamentoPlanBro = ko.observable(-1);
    var MunicipiosPlanBro = ko.observableArray([]);
    var SelectedMunicipioPlanBro = ko.observable(-1);
    var Distintivos = ko.observableArray([]);
    var SelectedDistintivo = ko.observable(-1);
    var ListaFiles = ko.observableArray([]);
    var Expediente = ko.observable(undefined);
    var DepartamentosComunidad = ko.observableArray([]);
    var SelectedDepartamentoComunidad = ko.observable(-1);
    var MunicipiosComunidad = ko.observableArray([]);
    var ListaFilesPond = ko.observableArray([]);
    var DatosComunidad =
    {
        NombreComunidad: ko.observable(''),
        IdLugarComunidad: ko.observable(undefined),
        CiudadComunidad: ko.observable(''),
        DepartamentoComunidad: ko.observable(''),
        CodeLugarComunidad: ko.observable(''),
        DireccionComunidad: ko.observable(''),
        HayTerritorio: ko.observable(false),
        Territorio: ko.observable(''),
        ResolucionMinInterior: ko.observable(''),
        GrupoEtnico: ko.observable("IN"),
        NombrePuebloComunidad: ko.observable(''),
        NombreResguardo: ko.observable(''),
        NombreCabildo: ko.observable(''),
        NombreKumpania: ko.observable(''),
        NombreConsejo: ko.observable(''),
        NombreOrganizacionBase: ko.observable(''),
    };
    var NoMunicipalizado = ko.observable(false);
    var TiposGrupoEtnico = [{ Id: "IN", Descripcion: "Indígena" },
    { Id: "RR", Descripcion: "Rrom" },
    { Id: "NG", Descripcion: "Negro" },
    { Id: "AF", Descripcion: "Afrocolombiano" },
    { Id: "RZ", Descripcion: "Raizal" },
    { Id: "PQ", Descripcion: "Palenquero" },
    ];
    var TipoGrupoEtnicoSeleccionado = ko.observable("IN");
    var GrupoEtnico = ko.observable("");
    var Identificacion = ko.observable("");
    var NombreRazonSocial = ko.observable("");
    var PopupIdent = {
        Show: ko.observable(true),
        DISABLE: ko.observable()
    };
    var bEnviarCorreoConcesionario = ko.observable(false);
    var RadicadoFecha = ko.observable();
    var RadicadoCodigo = ko.observable();
    var RadicadoEstado = ko.observable(false);


    var BotonCancelar = [
        { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
        { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la solicitud?' },
        { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
        { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la subsanación?' },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonEnviar = [
        { CRUD: 'Dummy', Text: 'Crear solicitud', Icon: 'floppy' },
        { CRUD: enumCRUD.Create, Text: 'Crear solicitud', Icon: 'floppy', Method: CrearSolicitud },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: 'Subsanar solicitud', Icon: 'floppy', Method: SubsanarSolicitud },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar solicitud', Icon: 'floppy' },
    ];

    var Solicitud = undefined;
    var SOL_ENDED = 0;

    function EvaluateIcon(FileObject) {
        var Icon = '';
        if (CRUD == enumCRUD.Read)
            return '';

        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-empty';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Icon = 'ta ta-chulo';
                else Icon = 'ta ta-requerir';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Icon = 'ta ta-chulo';
                break;
            case enumROLE1_STATE.Rechazado:
                Icon = 'ta ta-cancel';
                break;
            case enumROLE1_STATE.NoRevision:
                Icon = 'ta ta-empty';
                break;
        }
        return Icon;
    }

    function EvaluateColor(FileObject) {
        var Color = '';
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.SinDefinir:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'transparent';
                break;
            case enumROLE1_STATE.Requerido:
                if (FileObject.LOADED()) Color = 'Green';
                else Color = 'Orange';
                break;
            case enumROLE1_STATE.Aprobado:
            case enumROLE1_STATE.Radicado:
            case enumROLE1_STATE.Subsanado:
                Color = 'Green';
                break;
            case enumROLE1_STATE.Rechazado:
                Color = '#D00000';
                break;
            case enumROLE1_STATE.NoRevision:
                Color = 'transparent';
                break;
        }
        return Color;
    }

    function EvaluateTitle(FileObject) {
        var Title = null;
        switch (FileObject.STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + FileObject.COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    function EvaluateTitle2(STATE_ID, COMMENT) {
        var Title = null;
        switch (STATE_ID) {
            case enumROLE1_STATE.Requerido:
                Title = 'Requerido: ' + COMMENT;
                break;
            case enumROLE1_STATE.Rechazado:
                Title = 'Rechazado';
                break;
        }
        return Title;
    }

    var CrearSolicitudDisable = ko.computed(function () {

        var loaded = true;
        ListaFiles().forEach(function (file) {
            loaded = loaded && file.LOADED();
        });
        //var loaded = true;
        ////ListaFilesPond().forEach(function (file) {
        ////    loaded = loaded && file.LOADED();
        ////});

        var datosClasificacionServicioCompletos = SelectedDistintivo() != -1;

        DatosComunidad.GrupoEtnico(TipoGrupoEtnicoSeleccionado());

        var datosComunidadCompletos = !IsNullOrEmpty(DatosComunidad.NombreComunidad())
            && !IsNullOrEmpty(DatosComunidad.DireccionComunidad())
            && ((!NoMunicipalizado() && !IsNullOrEmpty(DatosComunidad.IdLugarComunidad())) || (NoMunicipalizado() && !IsNullOrEmpty(DatosComunidad.Territorio()) && SelectedDepartamentoComunidad() != -1));

        var datosGruposEtnicosCompletos = !IsNullOrEmpty(DatosComunidad.ResolucionMinInterior())
            && !IsNullOrEmpty(DatosComunidad.GrupoEtnico())
            &&
            (
                (DatosComunidad.GrupoEtnico() == "IN" && !IsNullOrEmpty(DatosComunidad.NombrePuebloComunidad()))
                || (DatosComunidad.GrupoEtnico() == "RR" && !IsNullOrEmpty(DatosComunidad.NombreKumpania()))
                || ((DatosComunidad.GrupoEtnico() == "NG" || DatosComunidad.GrupoEtnico() == "AF" || DatosComunidad.GrupoEtnico() == "RZ" || DatosComunidad.GrupoEtnico() == "PQ") && !IsNullOrEmpty(DatosComunidad.NombreConsejo()))
            );

        //var datosRadicadoCompletos = RadicadoEstado() && RadicadoFecha() != undefined;

        if (loaded & !GuardarDisabled() && datosClasificacionServicioCompletos && datosComunidadCompletos && datosGruposEtnicosCompletos) //&& datosRadicadoCompletos)
            return false;
        return true;
    });

    function CrearFuncionarioSolicitudOtorga(SOL_TYPE, LIC_NUMBER, NitOperador, Plan, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, datosComunidad,
        RadicadoCodigo, RadicadoFecha, bEnviarCorreoConcesionario) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            timeout: 180000,
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CrearFuncionarioSolicitudOtorga",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                SOL_TYPE: SOL_TYPE,
                LIC_NUMBER: LIC_NUMBER,
                NitOperador: NitOperador,
                Plan: Plan,
                Emails: Emails,
                Files: Files,
                FilesPond: FilesPond,
                NombreDepartamentoPlanBro: NombreDepartamentoPlanBro,
                NombreMunicipiolanBro: NombreMunicipioPlanBro,
                CodeAreaMunicipioPlanBro: CodeAreaMunicipioPlanBro,
                DatosComunidad: datosComunidad,
                RadicadoCodigo: RadicadoCodigo,
                RadicadoFecha: RadicadoFecha,
                bEnviarCorreoConcesionario: bEnviarCorreoConcesionario,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.CrearFuncionarioSolicitudOtorgaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function GetRadicado(RadicadoCodigo) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetRadicado",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                RadicadoCodigo: RadicadoCodigo
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetRadicadoResult;

                if (result.Error == true) {
                    DevExpress.ui.notify('El sistema de radicado Alfa no está disponible.', "error", 3000);
                    RadicadoEstado(false);
                } else {
                    RadicadoEstado(result.Estado);
                    if (result.Estado == false) {
                        DevExpress.ui.notify(result.EstadoMessage, "warning", 3000);
                    }
                }


                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function BuscarRadicado() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Buscando radicado...");
        loadingVisible(true);
        GetRadicado(RadicadoCodigo());
    }

    function SubsanarConcesionarioSolicitud(Solicitud, Files, FilesPond) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/SubsanarConcesionarioSolicitud",
            data: JSON.stringify({
                Solicitud: Solicitud,
                Files: Files,
                FilesPond: FilesPond,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.SubsanarConcesionarioSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CargarFile(e) {
        var FileObject = e.data.FileObject;
        var ElementFile;
        ElementFile = document.querySelector('#' + e.target.id);

        if (ElementFile.value != "") {
            var file = ElementFile.files[0];

            if (!getFilesContentType(FileObject.EXTENSIONS).Exists(file.type)) {
                DevExpress.ui.notify('Formato de archivo inválido. Formato(s) válido(s) (' + FileObject.EXTENSIONS.join(', ') + ').', "warning", 5000);
                ElementFile.value = null;
                return;
            }

            if (file.name.length > FileObject.MAX_NAME_LENGTH) {
                DevExpress.ui.notify('El nombre del archivo excede el tamaño máximo permitido de ' + FileObject.MAX_NAME_LENGTH + ' caracteres', "warning", 3000);
                ElementFile.value = null;
                return;
            }

            if (FileObject.MAX_SIZE != -1) {
                if (file.size > (FileObject.MAX_SIZE * 1000000)) {
                    DevExpress.ui.notify('El tamaño máximo del archivo es ' + FileObject.MAX_SIZE + 'MB.', "warning", 3000);
                    ElementFile.value = null;
                    return;
                }
            }

            FileObject.NAME(file.name);
            FileObject.CONTENT_TYPE_LOCAL(file.type);

            var fileReader = new FileReader();
            fileReader.onload = function () {
                FileObject.DATA = fileReader.result;
                FileObject.LOADED(true);
            };

            fileReader.readAsDataURL(file);
            ElementFile.value = null;

            $("#GridOtorgaFiles").dxDataGrid('instance').refresh();
            $("#GridOtorgaFilesPonderables").dxDataGrid('instance').refresh();
        }
    }

    function OpenFile(e) {
        var FileObject = e.data.FileObject;

        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + FileObject.UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function CrearSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Creando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        ListaFiles().forEach(function (file) {
            Files.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
        });

        var FilesPond = [];
        ListaFilesPond().forEach(function (file) {
            FilesPond.push({ TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
        });

        var LIC_NUMBER = 1;
        var plan = $("#DivDistintivos").dxSelectBox('instance').option('selectedItem');
        var NombreDepartamentoPlanBro = $("#DivDepartamentosPlanBro").dxSelectBox('instance').option('selectedItem').NAME;
        var NombreMunicipioPlanBro = $("#DivMunicipiosPlanBro").dxSelectBox('instance').option('selectedItem').NAME;
        var CodeAreaMunicipioPlanBro = $("#DivMunicipiosPlanBro").dxSelectBox('instance').option('selectedItem').CODE_AREA;

        var municipioComunidad = NoMunicipalizado() ? undefined : MunicipiosComunidad().filter(function (obj) { return obj.ID === DatosComunidad.IdLugarComunidad() })[0];
        var departamentoComunidad = DepartamentosComunidad().filter(function (obj) { return obj.CODE_AREA === SelectedDepartamentoComunidad() })[0];
        DatosComunidad.CiudadComunidad(IsNullOrEmpty(municipioComunidad) ? undefined : municipioComunidad.NAME);
        DatosComunidad.CodeLugarComunidad(IsNullOrEmpty(municipioComunidad) ? undefined : municipioComunidad.CODE_AREA);
        DatosComunidad.DepartamentoComunidad(departamentoComunidad.NAME);
        if (!NoMunicipalizado()) DatosComunidad.Territorio(undefined);

        CrearFuncionarioSolicitudOtorga(SOL_TYPE, LIC_NUMBER, Identificacion(), plan, Emails, Files, FilesPond, NombreDepartamentoPlanBro, NombreMunicipioPlanBro, CodeAreaMunicipioPlanBro, ko.toJS(DatosComunidad),
            RadicadoCodigo(), RadicadoFecha().toString('yyyyMMdd'), bEnviarCorreoConcesionario());
    }

    function SubsanarSolicitud() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Subsanando solicitud...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Files = [];
        ListaFiles().forEach(function (file) {
            if (file.STATE_ID == enumROLE1_STATE.Requerido) {
                Files.push({ UID: file.UID, TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });

        var FilesPond = [];
        ListaFilesPond().forEach(function (file) {
            if (file.STATE_ID == enumROLE1_STATE.Requerido) {
                FilesPond.push({ UID: file.UID, TYPE_ID: file.TYPE_ID, NAME: file.NAME(), CONTENT_TYPE: file.CONTENT_TYPE_LOCAL(), DATA: file.DATA });
            }
        });

        SubsanarConcesionarioSolicitud(Solicitud, Files, FilesPond);
    }

    function CancelarSolicitud() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }


    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function onKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }


    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'COMMENT':

                $('<i/>').addClass('ta ta-question-circle ta-2x')
                    .prop('title', 'Archivo(s) permitido(s) (' + e.data.EXTENSIONS.join() + ') y Tamaño máximo del archivo (' + e.data.MAX_SIZE + 'MB).')
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('float', 'left')
                    .css('color', '#6f91cb')
                    .appendTo(container);

                $('<i/>').addClass('ta-2x')
                    .addClass(ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'icon', ''))
                    .prop('title', 'Ver archivo')
                    .prop('visibility', (e.data.UID != null ? 'visible' : 'hidden'))
                    .bind('click', { FileObject: e.data }, OpenFile)
                    .css('cursor', 'default')
                    .css('margin-top', '14px')
                    .css('margin-left', '10px')
                    .css('margin-right', '5px')
                    .css('cursor', 'pointer')
                    .css('float', 'left')
                    .css('color', ListObjectGetAttribute(FILE_TYPES, 'content_type', e.data.CONTENT_TYPE(), 'color', ''))
                    .appendTo(container);

                break;
            case 'Upload':
                var label = $('<label/>');
                label.addClass('custom-file-upload')
                    .prop('title', 'Seleccionar archivo')
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .css('position', 'relative')
                    .css('width', '400px')
                    .css('margin-top', '10px');
                label.disabled = false;

                var input = $('<input/>');
                input.prop('id', e.data.ID)
                    .prop('type', 'file')
                    .prop('accept', e.data.EXTENSIONS.join())
                    .prop('disabled', SOL_ENDED != 0 || [enumROLE1_STATE.SinDefinir, enumROLE1_STATE.Requerido].NoExists(e.data.STATE_ID))
                    .bind('change', { FileObject: e.data }, CargarFile)
                    .css('z-index', '999')
                    .css('position', 'absolute')
                    .css('top', '0px')
                    .css('visibility', 'hidden');
                input.appendTo(label);

                var info = $('<i/>');
                info.addClass('ta ta-attachment ta-lg')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(label);

                var span = $('<span/>');
                span.prop('textContent', e.data.NAME())
                    .appendTo(label);

                label.appendTo(container);
                break;

            case 'DESC':
                var div = $('<b/>');
                div.prop('textContent', e.data.DESC())
                    .css('vertical-align', 'middle')
                    .css('fontWeight', 'bold')
                    .css('word-wrap', 'break-word')
                    .appendTo(container);

                container.css('vertical-align', 'middle')
                    .css('word-wrap', 'break-word');
                break;
        }
    }

    var onDepartamentoPlanBroChanged = ko.computed(function () {
        loadingMessage("Leyendo municipios...");
        loadingVisible(true);
        GetMunicipiosOtorga(SelectedDepartamentoPlanBro());
    });

    function GetMunicipiosOtorga(CODE_AREA_DEPARTAMENTO) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMunicipiosOtorga",
            data: JSON.stringify({
                CODE_AREA_DEPARTAMENTO: CODE_AREA_DEPARTAMENTO,
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetMunicipiosOtorgaResult;
                MunicipiosPlanBro(result);
                SelectedMunicipioPlanBro(-1);
                SelectedDistintivo(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    var onMunicipioPlanBroChanged = ko.computed(function () {
        loadingMessage("Leyendo distintivos...");
        loadingVisible(true);
        GetDistintivos(SelectedMunicipioPlanBro());
    });

    function GetDistintivos(CODE_AREA_MUNICIPIO) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDistintivos",
            data: JSON.stringify({
                CODE_AREA_MUNICIPIO: CODE_AREA_MUNICIPIO,
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetDistintivosResult;
                Distintivos(result);
                SelectedDistintivo(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetDepartamentosComunidad() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDepartamentos",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetDepartamentosResult;
                DepartamentosComunidad(result);
                SelectedDepartamentoComunidad(-1);
                DatosComunidad.IdLugarComunidad(undefined);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    var onDepartamentoComunidadChanged = ko.computed(function () {
        loadingMessage("Leyendo municipios...");
        loadingVisible(true);
        GetMunicipiosComunidad(SelectedDepartamentoComunidad(), MunicipiosComunidad);
    });

    function GetMunicipiosComunidad(CODE_AREA_DEPARTAMENTO, variableMunicipios) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetMunicipios",
            data: JSON.stringify({
                CODE_AREA_DEPARTAMENTO: CODE_AREA_DEPARTAMENTO
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetMunicipiosResult;
                variableMunicipios(result);
                DatosComunidad.IdLugarComunidad(undefined);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetDatosCreacionOtorgaEmisora",
            data: JSON.stringify({
                SOL_TYPE_ID: SOL_TYPE_ID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetDatosCreacionOtorgaEmisoraResult;
                DepartamentosPlanBro(result.DepartamentosPlanBro);
                SelectedDepartamentoPlanBro(-1);
                SelectedMunicipioPlanBro(-1);
                SelectedDistintivo(-1);

                ListaFiles([]);
                for (var i = 0; i < result.TiposArchivos.length; i++) {
                    var tipoArchivo = result.TiposArchivos[i];
                    var file = {
                        ID: 'FileOtorga' + tipoArchivo.ID,
                        DESC: ko.observable(tipoArchivo.DESC),
                        NAME: ko.observable('Seleccionar archivo'),
                        DATA: undefined,
                        CONTENT_TYPE_LOCAL: ko.observable(''),
                        CONTENT_TYPE: ko.observable(''),
                        LOADED: ko.observable(false),
                        STATE_ID: 0,
                        TYPE_ID: tipoArchivo.ID,
                        EXTENSIONS: tipoArchivo.EXTENSIONS.split(','),
                        UID: null,
                        COMMENT: '',
                        MAX_SIZE: tipoArchivo.MAX_SIZE,
                        MAX_NAME_LENGTH: tipoArchivo.MAX_NAME_LENGTH,
                        Title: EvaluateTitle2(0, ''),
                    };
                    ListaFiles.push(file);
                }

                ListaFilesPond([]);
                for (var i = 0; i < result.TiposArchivosPond.length; i++) {
                    var tipoArchivo = result.TiposArchivosPond[i];
                    var file = {
                        ID: 'FileOtorga' + tipoArchivo.ID,
                        DESC: ko.observable(tipoArchivo.DESC),
                        NAME: ko.observable('Seleccionar archivo'),
                        DATA: undefined,
                        CONTENT_TYPE_LOCAL: ko.observable(''),
                        CONTENT_TYPE: ko.observable(''),
                        LOADED: ko.observable(false),
                        STATE_ID: 0,
                        TYPE_ID: tipoArchivo.ID,
                        EXTENSIONS: tipoArchivo.EXTENSIONS.split(','),
                        UID: null,
                        COMMENT: '',
                        MAX_SIZE: tipoArchivo.MAX_SIZE,
                        MAX_NAME_LENGTH: tipoArchivo.MAX_NAME_LENGTH,
                        Title: EvaluateTitle2(0, ''),
                    };
                    ListaFilesPond.push(file);
                }

                if (CRUD.In([enumCRUD.Update, enumCRUD.Read])) {

                    Solicitud = JSON.parse(SGEWeb.app.Solicitud);
                    SOL_ENDED = Solicitud.SOL_ENDED;
                    Expediente(Solicitud.Expediente);

                    DatosComunidad.NombreComunidad(Solicitud.DatosComunidad.NombreComunidad);
                    DatosComunidad.DepartamentoComunidad(Solicitud.DatosComunidad.DepartamentoComunidad);
                    DatosComunidad.CiudadComunidad(Solicitud.DatosComunidad.CiudadComunidad);
                    DatosComunidad.DireccionComunidad(Solicitud.DatosComunidad.DireccionComunidad);
                    DatosComunidad.Territorio(Solicitud.DatosComunidad.Territorio);
                    NoMunicipalizado(!IsNullOrEmpty(Solicitud.DatosComunidad.Territorio));
                    DatosComunidad.ResolucionMinInterior(Solicitud.DatosComunidad.ResolucionMinInterior);
                    DatosComunidad.GrupoEtnico(Solicitud.DatosComunidad.GrupoEtnico);
                    DatosComunidad.NombrePuebloComunidad(Solicitud.DatosComunidad.NombrePuebloComunidad);
                    DatosComunidad.NombreResguardo(Solicitud.DatosComunidad.NombreResguardo);
                    DatosComunidad.NombreCabildo(Solicitud.DatosComunidad.NombreCabildo);
                    DatosComunidad.NombreKumpania(Solicitud.DatosComunidad.NombreKumpania);
                    DatosComunidad.NombreConsejo(Solicitud.DatosComunidad.NombreConsejo);
                    DatosComunidad.NombreOrganizacionBase(Solicitud.DatosComunidad.NombreOrganizacionBase);
                    GrupoEtnico(Solicitud.DatosComunidad.GrupoEtnico != undefined ? ListObjectFindGetElement(TiposGrupoEtnico, 'Id', Solicitud.DatosComunidad.GrupoEtnico).Descripcion : undefined);
                    TipoGrupoEtnicoSeleccionado(Solicitud.DatosComunidad.GrupoEtnico);
                    ListaFiles().forEach(function (file) {
                        var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', file.TYPE_ID);
                        file.UID = element.ANX_UID;
                        file.STATE_ID = element.ANX_STATE_ID;
                        file.CONTENT_TYPE(element.ANX_CONTENT_TYPE),
                            file.NAME(element.ANX_NAME);
                        file.COMMENT = element.ANX_COMMENT;
                        if (file.STATE_ID != enumROLE1_STATE.Requerido)
                            file.LOADED(true);
                        file.Title = EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT);
                    });

                    ListaFilesPond().forEach(function (file) {
                        var element = ListObjectFindGetElement(Solicitud.Anexos, 'ANX_TYPE_ID', file.TYPE_ID);
                        file.UID = element.ANX_UID;
                        file.STATE_ID = element.ANX_STATE_ID;
                        file.CONTENT_TYPE(element.ANX_CONTENT_TYPE),
                            file.NAME(element.ANX_NAME);
                        file.COMMENT = element.ANX_COMMENT;
                        if (file.STATE_ID != enumROLE1_STATE.Requerido)
                            file.LOADED(true);
                        file.Title = EvaluateTitle2(element.ANX_STATE_ID, element.ANX_COMMENT);
                    });
                }

                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }

    function OnVolver() {
        SGEWeb.app.navigationManager.back();
    }

    function OnContinuar(e) {
        ContinuarSiOperadorExiste();
    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 13) {
            $('#btnContinuar').dxButton('instance').focus();
            OnContinuar();
        }
        return true;
    }

    function ContinuarSiOperadorExiste() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetOperador",
            data: JSON.stringify({
                NitOperador: Identificacion(),
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var operador = msg.GetOperadorResult;
                if (IsNullOrEmpty(operador)) {
                    DevExpress.ui.dialog.alert(CrearDialogHtml('ta ta-warning', 'El usuario no existe'), SGEWeb.app.Name);
                }
                else {
                    NombreRazonSocial(operador.CUS_NAME);

                    PopupIdent.DISABLE(true);
                    PopupIdent.Show(false);
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    
    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetDepartamentosComunidad();
        GetDatosCreacionOtorgaEmisora(SOL_TYPE_ID);
    }

    Refrescar();


    var viewModel = {
        CargarFile: CargarFile,
        OpenFile: OpenFile,
        CrearSolicitud: CrearSolicitud,
        CancelarSolicitud: CancelarSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        EvaluateIcon: EvaluateIcon,
        EvaluateColor: EvaluateColor,
        EvaluateTitle: EvaluateTitle,
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonEnviar: BotonEnviar,
        SOL_ENDED: SOL_ENDED,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        DepartamentosPlanBro: DepartamentosPlanBro,
        SelectedDepartamentoPlanBro: SelectedDepartamentoPlanBro,
        MunicipiosPlanBro: MunicipiosPlanBro,
        SelectedMunicipioPlanBro: SelectedMunicipioPlanBro,
        Distintivos: Distintivos,
        SelectedDistintivo: SelectedDistintivo,
        onDepartamentoPlanBroChanged: onDepartamentoPlanBroChanged,
        onMunicipioPlanBroChanged: onMunicipioPlanBroChanged,
        onKeyDown: onKeyDown,
        ListaFiles: ListaFiles,
        onCellTemplate: onCellTemplate,
        Expediente: Expediente,
        title: title,
        SOL_TYPE_ID: SOL_TYPE_ID,
        DepartamentosComunidad: DepartamentosComunidad,
        SelectedDepartamentoComunidad: SelectedDepartamentoComunidad,
        MunicipiosComunidad: MunicipiosComunidad,
        onDepartamentoComunidadChanged: onDepartamentoComunidadChanged,
        DatosComunidad: DatosComunidad,
        NoMunicipalizado: NoMunicipalizado,
        TiposGrupoEtnico: TiposGrupoEtnico,
        TipoGrupoEtnicoSeleccionado: TipoGrupoEtnicoSeleccionado,
        GrupoEtnico: GrupoEtnico,
        Identificacion: Identificacion,
        NombreRazonSocial: NombreRazonSocial,
        PopupIdent: PopupIdent,
        keyPressAction: keyPressAction,
        OnVolver: OnVolver,
        OnContinuar: OnContinuar,
        onPwdKeyDown: onPwdKeyDown,
        BuscarRadicado: BuscarRadicado,
        RadicadoCodigo: RadicadoCodigo,
        RadicadoFecha: RadicadoFecha,
        RadicadoEstado: RadicadoEstado,
        bEnviarCorreoConcesionario: bEnviarCorreoConcesionario,
        ListaFilesPond: ListaFilesPond,
    };

    return viewModel;
};