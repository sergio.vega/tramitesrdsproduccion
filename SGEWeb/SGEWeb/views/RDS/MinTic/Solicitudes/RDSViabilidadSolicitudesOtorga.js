﻿SGEWeb.RDSViabilidadSolicitudesOtorga = function (params) {
    "use strict";
    var SolicitudesCanal = ko.observableArray([]);
    var MyDataSourceDocs = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var ShowPopUp = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var CRUDType = -1; //1: Nuevo, 3: Edit
    var CRUDTitle = ko.observable('');
    var Cantidades = ko.observable(0);
    var CantidadesDocs = ko.observable(0);
    var ShowPopUpDocuments = ko.observable(false);
    var Operador = ko.observable(undefined);
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var SelectedSolicitud = undefined;
    var isLoading = ko.observable(false);
    var UserSelected = ko.observable({ CONT_ID: -1, CONT_NAME: '', CONT_NUMBER: '', CONTT_TITLE: '', CONT_TEL: '' });
    var UserCC = ko.observable('');
    var Contactos = ko.observableArray([]);
    var TiposSolicitudesPosibles = ko.observableArray([]);
    var ShowPopUpOtorga = ko.observable(false);
    var CusEmailOperador = ko.observable(undefined);
    var TipoSolicitudACrear = ko.observable(-1);
    var ConcesionarioIsFirstTime = true;
    var ID_PLAN_BRO = JSON.parse(params.settings.substring(5)).ID_PLAN_BRO;
    var CALL_SIGN = JSON.parse(params.settings.substring(5)).CALL_SIGN;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var ViabilidadPendiente = JSON.parse(params.settings.substring(5)).IdEstadoViabilidad == enumEstadoViabilidad.ViabilidadPendiente;
    var PermitirAsignarViabilidad = ko.observable(ViabilidadPendiente);
    var RadicadoDeSolicitudAsignada = JSON.parse(params.settings.substring(5)).RadicadoDeSolicitudAsignada;
    var PuntajeMinimoViabilidadAutomatica = 0;
    var PuntajeMaximo;
    var TodosCalificados;
    var Empatados;
    var SolicitudGanadora;

    var MyPopUpDesempate = {

        Show: ko.observable(false),
        Razon: ko.observable(''),
        Data: ko.observable
    };

    var PermitirViabilidadAutomatica = ko.observable(false);

    var Alarmas = { Encurso: ko.observable(undefined), Requeridas: ko.observable(undefined), Porvencer: ko.observable(undefined), EnCierre: ko.observable(undefined), Terminadas: ko.observable(undefined), Total: ko.observable(0) };

    var EmailContacto1 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: 'required', message: 'Email requerido' },
            { type: "email", message: "Email inválido" }]
        }
    });
    var EmailContacto2 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;
    var EmailContacto3 = ko.observable('').extend({
        dxValidator: {
            validationRules: [{ type: "email", message: "Email inválido" }]
        }
    });;

    var CrearSolicitudDisable = ko.computed(function () {

        if (UserSelected().CONT_ID != -1 && TipoSolicitudACrear() != -1
            && EmailContacto1.dxValidator.validate().isValid
            && EmailContacto2.dxValidator.validate().isValid
            && EmailContacto3.dxValidator.validate().isValid
        )
            return false;
        return true;
    });

    DevExpress.config({
        floatingActionButtonConfig: {
            position: {
                my: "right bottom",
                at: "right bottom",
                of: "#app-container",
                offset: "-16 -74"
            },
            label: "Nueva solicitud"
        }
    });

    function GetSolicitudesCanal() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesCanal",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                USR_GROUP: GROUP,
                USR_ROLE: ROLE,
                CALL_SIGN: CALL_SIGN,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetSolicitudesCanalResult;
                SolicitudesCanal(result.Solicitudes);
                PuntajeMinimoViabilidadAutomatica = result.PuntajeMinimoViabilidadAutomatica;

                PuntajeMaximo = -1111;
                TodosCalificados = true;
                Empatados = -1111;
                SolicitudesCanal().forEach(function (solicitud) {
                    if (IsNullOrEmpty(solicitud.Puntaje)) {
                        TodosCalificados = false;
                    }
                    else {
                        if (solicitud.Puntaje > PuntajeMaximo) {
                            SolicitudGanadora = solicitud;
                            PuntajeMaximo = solicitud.Puntaje;
                            Empatados = 1;
                        }
                        else if (solicitud.Puntaje == PuntajeMaximo) {
                            Empatados++;
                            SolicitudGanadora = null;
                        }
                    }
                });

                PermitirViabilidadAutomatica(TodosCalificados && PuntajeMaximo >= PuntajeMinimoViabilidadAutomatica && Empatados == 1);

                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function ViabilidadAutomatica(e) {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        DarViabilidadSolicitud(SolicitudGanadora)
        isLoading(true);
    }

    function DarViabilidadSolicitud(Solicitud, razonDesempate) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/DarViabilidadSolicitud",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Solicitud: Solicitud,
                RazonDesempate: razonDesempate,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.DarViabilidadSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    PermitirAsignarViabilidad(false);
                    res.done(function (dialogResult) {
                        Refrescar();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'GridRDSSolicitudes') {
            if (!isLoading())
                loadingVisible(false);

            Cantidades(Globalizeformat(e.component.totalCount(), "n0"));

            var GridSource = e.component.getDataSource();
            Alarmas.Encurso(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Encurso));
            Alarmas.Requeridas(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Requeridas));
            Alarmas.Porvencer(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Porvencer));
            Alarmas.EnCierre(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.EnCierre));
            Alarmas.Terminadas(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Terminadas));
            //No debería haber vencidas ya que al vencerse se desiste
            Alarmas.Total(e.component.totalCount() - ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM_REQUIRED.Vencidas));

            SGEWeb.app.DisabledToolBar(false);
        } else {
            SGEWeb.app.DisabledToolBar(false);
        }
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Viabilidad':
                $('<i/>').addClass('ta ta-chulo ta-lg')
                    .prop('title', 'Dar viabilidad')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Evaluar':
                $('<i/>').addClass('ta ta-clipboard1 ta-lg')
                    .prop('title', 'Evaluar')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar solicitud')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Cancelar':
                $('<i/>').addClass('ta ta-cancel ta-lg')
                    .prop('title', 'Cancelar solicitud')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                    .prop('title', 'Documentos')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Aplazar':
                $('<i/>').addClass('ta ta-alarm ta-lg')
                    .prop('title', 'Aplazar requerimiento')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;

            case 'Alarma':
                $('<i/>').addClass('ta ta-bell ta-lg')
                    .prop('title', 'Alarmas')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;

        }
    }

    function GetColorViabilidad(radicado, radicadoAsignada, permitirAsignarViabilidad) {
        var color = 'Grey';
        if (!IsNullOrEmpty(radicadoAsignada) && radicado == radicadoAsignada) {
            color = 'Green';
        }
        else {
            if (permitirAsignarViabilidad) color = 'Black';
        }
        return color;
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Viabilidad':
                var color = GetColorViabilidad(e.data.Radicado, RadicadoDeSolicitudAsignada, PermitirAsignarViabilidad());

                $('<i/>').addClass('ta ta-chulo ta-lg')
                    .prop('title', 'Dar viabilidad')
                    .css('cursor', 'pointer')
                    .css('color', color)
                    .appendTo(container);
                break;
            case 'Evaluar':
                $('<i/>').addClass('ta ta-clipboard1 ta-lg')
                    .prop('title', 'Evaluar')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar solicitud')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Documents':
                var DOCS_NO_OPENED = ListObjectExists(e.data.Documents, 'DOC_OPENED', 0);
                $('<i/>').addClass('ta ta-attachment ta-lg')
                    .prop('title', DOCS_NO_OPENED ? 'Ver documentos (hay documentos sin revisar)' : 'Ver Documentos')
                    .css('cursor', 'pointer')
                    .css('color', DOCS_NO_OPENED ? '#D00000' : 'black')
                    .appendTo(container);
                break;

            case 'Cancelar':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-cancel ta-lg')
                        .prop('title', 'Cancelar solicitud')
                        .css('cursor', 'pointer')
                        .appendTo(container);
                }
                break;

            case 'Aplazar':
                if (e.data.SOL_ENDED == 0 && e.data.SOL_STATE_ID == enumSOL_STATES.Requerido && [enumALARM_REQUIRED.Requeridas, enumALARM_REQUIRED.Porvencer].Exists(e.data.SOL_ALARM_STATE)) {
                    $('<i/>').addClass('ta ta-alarm ta-lg')
                        .prop('title', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'Aplazar requerimiento' : 'El requerimiento ya fue aplazado')
                        .css('cursor', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'pointer' : 'default')
                        .css('color', e.data.SOL_REQUIRED_POSTPONE == 0 ? 'black' : '#D00000')
                        .appendTo(container);
                }
                break;

            case 'Ver Documento':
                if (e.data.DOC_OPENED() == 0) {
                    $('<i/>').addClass('ta ta-eye ta-lg')
                        .prop('title', 'Sin abrir')
                        .css('cursor', 'pointer')
                        .css('color', '#D00000')
                        .appendTo(container);
                } else {
                    $('<i/>').addClass('ta ta-eye ta-lg')
                        .prop('title', 'Abierto')
                        .css('cursor', 'pointer')
                        .css('color', 'black')
                        .appendTo(container);
                }
                break;
        }
    }

    var MyData = undefined;
    var razónDesempate = null;

    function OnGuardarRazon() {
        var mensaje = "¿Desea dar viabilidad a esta solicitud?";

        razónDesempate = MyPopUpDesempate.Razon();
        if (razónDesempate != null) {
            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', mensaje), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.DisabledToolBar(true);
                    loadingVisible(true);
                    loadingMessage('Dando viabilidad a solicitud...');
                    DarViabilidadSolicitud(MyData, razónDesempate);
                }
            });
        }
        MyPopUpDesempate.Show(false);
    }


    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSSolicitudes') {
                switch (e.column.name) {
                    case 'Viabilidad':
                        if (PermitirAsignarViabilidad()) {
                            if (TodosCalificados) {
                                if (e.data.Puntaje == PuntajeMaximo) {
                                    var mensaje = "¿Desea dar viabilidad a esta solicitud?";
                                    if (PuntajeMaximo < PuntajeMinimoViabilidadAutomatica) {
                                        mensaje = "El puntaje obtenido es menor al mínimo establecido para la calificación automática ¿Desea dar viabilidad de todas formas?";
                                    }

                                    if (Empatados > 1) {


                                        MyData = e.data;
                                        MyPopUpDesempate.Show(true);
                                    }

                                    else  {
                                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', mensaje), SGEWeb.app.Name);
                                        result.done(function (dialogResult) {
                                            if (dialogResult) {
                                                SGEWeb.app.DisabledToolBar(true);
                                                loadingVisible(true);
                                                loadingMessage('Dando viabilidad a solicitud...');
                                                DarViabilidadSolicitud(e.data, razónDesempate);
                                            }
                                        });
                                    }
                                }
                                else {
                                    DevExpress.ui.dialog.alert('Esta solicitud no obtuvo el puntaje máximo', SGEWeb.app.Name);
                                }
                            }
                            else {
                                DevExpress.ui.dialog.alert('Debe calificar todas las solicitudes antes de asignar la viabilidad', SGEWeb.app.Name);
                            }
                        }
                        else {
                            DevExpress.ui.dialog.alert('El estado actual no permite asignar la viabilidad', SGEWeb.app.Name);
                        }
                        break;
                    case 'Evaluar':
                        if (IsNullOrEmpty(RadicadoDeSolicitudAsignada)) {
                            SGEWeb.app.DisabledToolBar(true);
                            SGEWeb.app.navigate({ view: "RDSCalificarSolicitud", id: -1, settings: { title: title(), SOL_UID: e.data.SOL_UID, Evaluaciones: e.data.Evaluaciones, CRUD: enumCRUD.Update } });
                        }
                        else {
                            DevExpress.ui.dialog.alert('El estado actual no permite editar las evaluaciones', SGEWeb.app.Name);
                        }
                        break;
                }

            } else if (e.element[0].id == 'GridDocuments') {
                switch (e.column.name) {
                    case 'Ver Documento':
                        if (e.data.DOC_OPENED() == 0) {
                            e.data.DOC_OPENED(1);
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID() + '&MRC=1&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        } else {
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID() + '&MRC=0&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        }
                        break;
                }
            }
        }
    }

    function popupShowing(e) {
        $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function popupHidden(e) {
        SelectedSolicitud.Documents = ko.toJS(MyDataSourceDocs())
        var grid = $("#RDSConcesionario").dxDataGrid('instance');
        if (grid != undefined) {
            grid.repaintRows(grid.getRowIndexByKey(SelectedSolicitud.SOL_UID));
        }

        MyDataSourceDocs([]);

        var grid = $("#GridDocuments").dxDataGrid('instance');

        if (grid) {
            grid.clearFilter();
            grid.clearSorting();
            grid.pageIndex(0);
        }
    }

    function popupOtorgaHidden(e) {


    }

    function popupOtorgaShowing(e) {
    }

    function Excel() {
        var grid = $("#GridRDSSolicitudes").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function NuevaSolicitudOtros() {
        $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
        $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
        SGEWeb.app.navigate({ view: "RDSExpedientes", id: -1, settings: { title: title(), Operador: Operador } });
    }

    function NuevaSolicitudOtorga() {
        TipoSolicitudACrear(-1);
        //UserCC('1020773512');
        EmailContacto1.dxValidator.reset();
        EmailContacto1(''); EmailContacto2(''); EmailContacto3('');

        ShowPopUpOtorga(true);
    }

    function keyPressAction(e) {
        var event = e.jQueryEvent,
            str = String.fromCharCode(event.keyCode);
        if (!/[0-9]/.test(str))
            event.preventDefault();
    }

    function CrearSolicitud() {

        var Emails = [CusEmailOperador, EmailContacto1(), EmailContacto2(), EmailContacto3()];

        if (TipoSolicitudACrear() == -1) {
            DevExpress.ui.notify('Favor seleccionar un tipo de solicitud.', "warning", 5000);
        }
        else if (TipoSolicitudACrear() == 14) {
            ShowPopUpOtorga(false);
            $("#FABaddOtras").dxSpeedDialAction('instance').option('visible', false);
            $("#FABaddOtorga").dxSpeedDialAction('instance').option('visible', false);
            SGEWeb.app.navigate({ view: "RDSSolicitudOtorgaEmisora", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, SOL_TYPE_ID: TipoSolicitudACrear(), Contacto: ko.toJS(UserSelected), Emails: Emails } });
        }
        else {
            DevExpress.ui.notify('Tipo de solicitud no implementada.', "warning", 3000);
        }
    }

    function BuscarUsuario() {
        var Contacto = ListObjectFindGetElement(Contactos(), 'CONT_NUMBER', UserCC());
        if (Contacto != null) {
            UserSelected(Contacto);
        } else {
            DevExpress.ui.notify('El número de identificación no está registrado para realizar este trámite. Favor corregir e intentar de nuevo.', "warning", 3000);
        }

    }

    function onPwdKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetSolicitudesCanal();
        isLoading(true);
    }



    Refrescar();


    var viewModel = {
        viewShown: handleViewShown,
        SolicitudesCanal: SolicitudesCanal,
        MyDataSourceDocs: MyDataSourceDocs,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        Excel: Excel,
        Cantidades: Cantidades,
        CantidadesDocs: CantidadesDocs,
        ShowPopUpDocuments: ShowPopUpDocuments,
        popupShowing: popupShowing,
        popupHidden: popupHidden,
        title: title,
        Refrescar: Refrescar,
        Alarmas: Alarmas,
        isLoading: isLoading,
        NuevaSolicitudOtros: NuevaSolicitudOtros,
        NuevaSolicitudOtorga: NuevaSolicitudOtorga,
        TiposSolicitudesPosibles: TiposSolicitudesPosibles,
        popupOtorgaShowing: popupOtorgaShowing,
        popupOtorgaHidden: popupOtorgaHidden,
        ShowPopUpOtorga: ShowPopUpOtorga,
        CusEmailOperador: CusEmailOperador,
        TipoSolicitudACrear: TipoSolicitudACrear,
        EmailContacto1: EmailContacto1,
        EmailContacto2: EmailContacto2,
        EmailContacto3: EmailContacto3,
        CrearSolicitud: CrearSolicitud,
        CrearSolicitudDisable: CrearSolicitudDisable,
        UserSelected: UserSelected,
        keyPressAction: keyPressAction,
        UserCC: UserCC,
        BuscarUsuario: BuscarUsuario,
        onPwdKeyDown: onPwdKeyDown,
        Operador: Operador,
        SelectedSolicitud: SelectedSolicitud,
        Contactos: Contactos,
        ROLE: ROLE,
        GROUP: GROUP,
        PermitirAsignarViabilidad: PermitirAsignarViabilidad,
        ViabilidadAutomatica: ViabilidadAutomatica,
        PermitirViabilidadAutomatica: PermitirViabilidadAutomatica,
        MyPopUpDesempate: MyPopUpDesempate,
        OnGuardarRazon: OnGuardarRazon,
        razónDesempate: razónDesempate,
    
    };

    return viewModel;
};