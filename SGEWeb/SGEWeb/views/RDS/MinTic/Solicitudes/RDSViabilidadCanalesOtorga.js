﻿SGEWeb.RDSViabilidadCanalesOtorga = function (params) {
    "use strict";
    var CanalesProcesosOtorga = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var isLoading = ko.observable(false);
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var SOL_TYPE = JSON.parse(params.settings.substring(5)).EXTRA;

    function GetCanalesProcesos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetCanalesProcesos",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                SOL_TYPE: SOL_TYPE,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetCanalesProcesosResult;
                CanalesProcesosOtorga(result);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSViabilidadCanales') {
            if (!isLoading())
                loadingVisible(false);
        } 

        SGEWeb.app.DisabledToolBar(false);
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'AnalizarViabilidad':
                $('<i/>').addClass('ta ta-check1 ta-lg')
                    .prop('title', 'Analizar viabilidad')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    var ColoresCanales = [
        { ID: enumEstadoViabilidad.SinSolicitudes, Color: 'Black' },
        { ID: enumEstadoViabilidad.EnTramite, Color: 'Black' },
        { ID: enumEstadoViabilidad.ViabilidadPendiente, Color: 'Gold' },
        { ID: enumEstadoViabilidad.Asignado, Color: 'Green' },
        { ID: enumEstadoViabilidad.Desierto, Color: 'Grey' },
    ];

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'AnalizarViabilidad':
                $('<i/>').addClass('ta ta-check1 ta-lg')
                    .prop('title', 'Analizar viabilidad')
                    .css('cursor', 'pointer')
                    .css('color', ColoresCanales[e.data.IdEstadoViabilidad].Color)
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSViabilidadCanales') {
                switch (e.column.name) {
                    case 'AnalizarViabilidad':
                        SGEWeb.app.navigate({ view: "RDSViabilidadSolicitudesOtorga", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP, ID_PLAN_BRO: e.data.ID_PLAN_BRO, CALL_SIGN: e.data.CALL_SIGN, IdEstadoViabilidad: e.data.IdEstadoViabilidad, RadicadoDeSolicitudAsignada: e.data.RAD_ALFANET } });
                        break;
                }

            }
        }
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    
    function ViabilidadAutomatica(e) {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetCanalesProcesos();
        isLoading(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetCanalesProcesos();
        isLoading(true);
    }

    Refrescar();


    var viewModel = {
        viewShown: handleViewShown,
        CanalesProcesosOtorga: CanalesProcesosOtorga,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        ViabilidadAutomatica: ViabilidadAutomatica,
    };

    return viewModel;
};