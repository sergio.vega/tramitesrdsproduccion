﻿SGEWeb.RDSLogs = function (params) {
    "use strict";
    var title = JSON.parse(params.settings.substring(5)).title;
    var SOL_UID = JSON.parse(params.settings.substring(5)).SOL_UID;
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);

    function GetEventLogs(SOL_UID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetEventLogs",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                SOL_UID: SOL_UID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetEventLogsResult;
                MyDataSource(result);
//                title(Operador.CUS_NAME);
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Icon':
                $('<i/>').addClass('ta ta-' + e.data.LOG_ICON + ' ta-lg')
                .css('cursor', 'defaul')
                .appendTo(container);
                break;
        }
    }

    function Excel() {
        var grid = $("#RDSLogs").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetEventLogs(SOL_UID);
    }

    Refrescar();

    var viewModel = {
        title: title,
        MyDataSource: MyDataSource,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        Refrescar: Refrescar,
        Excel: Excel,
        onContentReady: onContentReady,
        onCellTemplate: onCellTemplate
    };

    return viewModel;
};