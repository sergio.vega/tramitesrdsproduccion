﻿SGEWeb.RDSNotificacionesConfig = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var title = ko.observable("Configuración Notificación");
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var ID_NOTIFIC = JSON.parse(params.settings.substring(5)).ID_NOTIFIC;
    var DsProcesos = ko.observableArray([]);
    var DsEtapas = ko.observableArray([]);
    var DsRoles = ko.observableArray([]);
    var DsTipoNotificaciones = ko.observableArray([]);
    var DsConceptos = ko.observableArray([]);
    var Notificacion = undefined;
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("");
    var ancho = ko.observable(750);
    var EnvioAutodisable = ko.observable(true);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var SelectedNotificacion =
    {
        Proceso: ko.observable(-1),
        Etapa: ko.observable(-1),
        Rol: ko.observable(-1),
        TipoNotificacion: ko.observable(-1),
        Concepto: ko.observable(-1),
        MomentoEnvio: ko.observable(1),
        Remitente: ko.observable("MINTIC"),
        Asunto: ko.observable(""),
        CuerpoCorreo: ko.observable(''),
        EnvioAuto: ko.observable(false),
        NumReenvios: ko.observable(0),
        Periodicidad: ko.observable(0),
    }
    if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
        CKEDITOR.tools.enableHtml5Elements(document);

    CKEDITOR.config.width = ancho;

    function CKEDITORCargarConfiguracion(config) {
        config.toolbar = [
            { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source'] },
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
            { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'CopyFormatting', 'RemoveFormat'] },
            { name: 'insert', items: ['SpecialChar', 'Table'] },

            { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'styles', items: ['Font', 'FontSize'] },
            { name: 'colors', items: ['TextColor', 'BGColor'] },
            '/',
        ];
        config.removePlugins = 'lite,flash,elementspath';

        config.font_names = "Arial/Arial, Helvetica, sans-serif;Arial Narrow/Arial Narrow;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
        config.fontSize_sizes = "8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;15/15pt;16/16pt;18/18pt;20/20pt;22/22pt;24/24pt;26/26pt;28/28pt;36/36pt;48/48pt;72/72pt";

        config.language = 'es';
        config.language_list = ["es:Español"];
        config.scayt_sLang = "es_ES";
        config.height = '400';
    }

    var StartCKEditor = (function (elementID) {
        var wysiwygareaAvailable = isWysiwygareaAvailable(),
            isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

        return function (elementID) {
            var editor = CKEDITOR.document.getById(elementID);

            var CorreoEditor = $('#CorreoEditor');

            if (wysiwygareaAvailable) {
                CKEDITOR.replace(elementID, {
                    on: {
                        'instanceReady': function (evt) {
                            evt.editor.resize(ancho(), CorreoEditor.innerHeight(), false);
                        }
                    }
                }
                );
            } else {
                editor.setAttribute('contenteditable', 'true');
                CKEDITOR.inline(elementID);
            }
            CKEDITOR.instances[elementID].config.CustomConfig = CKEDITORCargarConfiguracion;
        };

        function isWysiwygareaAvailable() {
            if (CKEDITOR.revision == ('%RE' + 'V%')) {
                return true;
            }
            return !!CKEDITOR.plugins.get('wysiwygarea');
        }
    })();

    var DsMomentoEnvio = [{ Id: 1, Descripcion: "Principio de la Etapa" },
    { Id: 2, Descripcion: "Final de la Etapa" },
    ];

    var DsParametros = [{ ID_PARAMETRO: 1, NOMBRE_PARAMETRO: "{Razon Social}" },
    { ID_PARAMETRO: 2, NOMBRE_PARAMETRO: "{NIT}" },
    { ID_PARAMETRO: 3, NOMBRE_PARAMETRO: "{Nombre Representante Legal}" },
    { ID_PARAMETRO: 4, NOMBRE_PARAMETRO: "{Nro Radicado}" },
    { ID_PARAMETRO: 5, NOMBRE_PARAMETRO: "{Fecha Radicado}" },
    { ID_PARAMETRO: 6, NOMBRE_PARAMETRO: "{Expediente}" },
    { ID_PARAMETRO: 7, NOMBRE_PARAMETRO: "{Proceso}" },
    { ID_PARAMETRO: 8, NOMBRE_PARAMETRO: "{Etapa}" },
    { ID_PARAMETRO: 9, NOMBRE_PARAMETRO: "{Fecha límite de requerimiento}" },
    { ID_PARAMETRO: 10, NOMBRE_PARAMETRO: "{Fecha Limite para radicar la prórroga}" },
    { ID_PARAMETRO: 11, NOMBRE_PARAMETRO: "{Motivos de requerimiento}" },
    ];

    var onProcesoChanged = ko.computed(function () {
        loadingMessage("Leyendo etapas...");
        loadingVisible(true);
        SelectedNotificacion.Etapa(-1);
        SelectedNotificacion.TipoNotificacion(-1)
        SelectedNotificacion.Rol(-1);
        SelectedNotificacion.Concepto(-1);
        GetEtapas(SelectedNotificacion.Proceso());
    });

    var onEtapaChanged = ko.computed(function () {
        if (SelectedNotificacion.Proceso() > 0 && SelectedNotificacion.Etapa() > 0) {
            loadingMessage("Leyendo tipo notificación...");
            loadingVisible(true);
            GetTipoNotificacion(SelectedNotificacion.Proceso(), SelectedNotificacion.Etapa());
        }
    });

    var onTipoNotifChanged = ko.computed(function () {
        if (SelectedNotificacion.Proceso() > 0 && SelectedNotificacion.Etapa() > 0 && SelectedNotificacion.TipoNotificacion() > 0) {
            loadingMessage("Leyendo roles...");
            loadingVisible(true);
            GetRolNotificacion(SelectedNotificacion.TipoNotificacion(), SelectedNotificacion.Proceso(), SelectedNotificacion.Etapa());
        }
    });

    var onRolChanged = ko.computed(function () {
        if (SelectedNotificacion.Proceso() > 0 && SelectedNotificacion.Etapa() > 0 && SelectedNotificacion.Rol() > 0) {
            loadingMessage("Leyendo conceptos...");
            loadingVisible(true);
            GetConceptos(SelectedNotificacion.Rol(), SelectedNotificacion.Proceso(), SelectedNotificacion.Etapa());
        }
    });

    var onEnvioAutoChanged = ko.computed(function () {
        if (SelectedNotificacion.EnvioAuto())
            EnvioAutodisable(false);
        else {
            EnvioAutodisable(true);
            SelectedNotificacion.NumReenvios(0);
            SelectedNotificacion.Periodicidad(0);
        }
    });

    var BotonCancelar = [
        { CRUD: 'Dummy', Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la notificación?' },
        { CRUD: enumCRUD.Create, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la configuración de la notificación?' },
        { CRUD: enumCRUD.Read, Text: 'Volver', Icon: 'ta ta-requerir1', ShowAlert: false },
        { CRUD: enumCRUD.Upate, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true, AlertMessage: 'Desea cancelar la configuración de la notificación?' },
        { CRUD: enumCRUD.Delete, Text: 'Cancelar', Icon: 'ta ta-cancel', ShowAlert: true },
    ];

    var BotonGuardar = [
        { CRUD: 'Dummy', Text: 'Guardar notificación', Icon: 'floppy' },
        { CRUD: enumCRUD.Create, Text: 'Guardar notificación', Icon: 'floppy', Method: GuardarNotificacion },
        { CRUD: enumCRUD.Read, Text: '', Icon: 'floppy' },
        { CRUD: enumCRUD.Upate, Text: 'Actualizar notificación', Icon: 'floppy', Method: GuardarNotificacion },
        { CRUD: enumCRUD.Delete, Text: '', Icon: 'floppy' },
    ];

    function GetNotificacion(IdNotificacion) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetNotificacion",
            data: JSON.stringify({
                IdNotificacion: IdNotificacion
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetNotificacionResult;
                Notificacion = result;
                GetProcesosNotif();
                if (CRUD.In([enumCRUD.Update])) {
                    if (!IsNullOrEmpty(SGEWeb.app.Notificacion)) {
                        Notificacion = JSON.parse(SGEWeb.app.Notificacion);
                        SelectedNotificacion.Proceso(Notificacion.ID_PROCESO);
                        SelectedNotificacion.Etapa(Notificacion.ID_ETAPA);
                        SelectedNotificacion.Rol(Notificacion.ID_ROL);
                        SelectedNotificacion.TipoNotificacion(Notificacion.TIPO_NOTIFICACION);
                        SelectedNotificacion.Concepto(Notificacion.ID_CONCEPTO);
                        SelectedNotificacion.MomentoEnvio(Notificacion.MOMENTO_ENVIO);
                        SelectedNotificacion.Remitente(Notificacion.REMITENTE);
                        SelectedNotificacion.Asunto(Notificacion.ASUNTO);
                        SelectedNotificacion.CuerpoCorreo(Notificacion.CUERPO_CORREO);
                        SelectedNotificacion.EnvioAuto(Notificacion.ENVIO_AUTOMATICO);
                        SelectedNotificacion.NumReenvios(Notificacion.NUMERO_REENVIO);
                        SelectedNotificacion.Periodicidad(Notificacion.PERIODICIDAD_REENVIO);
                    }
                }
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetTipoNotificacion(IdProceso, IdEtapa) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetTipoNotificacion",
            data: JSON.stringify({
                IdProceso: IdProceso,
                IdEtapa: IdEtapa
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetTipoNotificacionResult;
                DsTipoNotificaciones(result);
                //SelectedNotificacion.TipoNotificacion(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetEtapas(IdProceso) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetEtapas",
            data: JSON.stringify({
                IdProceso: IdProceso
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetEtapasResult;
                DsEtapas(result);
                //SelectedNotificacion.Etapa(-1);
                DsTipoNotificaciones([]);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetProcesosNotif() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetProcesosNotif",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetProcesosNotifResult;
                DsProcesos(result);
                //SelectedNotificacion.Proceso(-1);
                //if (CRUD.In([enumCRUD.Update])) {
                //    if (!IsNullOrEmpty(SGEWeb.app.Notificacion)) {
                //        Notificacion = JSON.parse(SGEWeb.app.Notificacion);
                //        SelectedNotificacion.Proceso(Notificacion.ID_PROCESO);
                //        SelectedNotificacion.Etapa(Notificacion.ID_ETAPA);
                //        SelectedNotificacion.Rol(Notificacion.ID_ROL);
                //        SelectedNotificacion.TipoNotificacion(Notificacion.TIPO_NOTIFICACION);
                //        SelectedNotificacion.Concepto(Notificacion.ID_CONCEPTO);
                //}
                //}
                loadingVisible(false);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetConceptos(IdRol, IdProceso, IdEtapa) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConceptos",
            data: JSON.stringify({
                IdRol: IdRol,
                IdProceso: IdProceso,
                IdEtapa: IdEtapa
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetConceptosResult;
                DsConceptos(result);
                //SelectedNotificacion.Concepto(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetRolNotificacion(IdTipoNotificacion, IdProceso, IdEtapa) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetRolNotificacion",
            data: JSON.stringify({
                IdTipoNotif: IdTipoNotificacion,
                IdProceso: IdProceso,
                IdEtapa: IdEtapa
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GetRolNotificacionResult;
                DsRoles(result);
                //SelectedNotificacion.Rol(-1);
                loadingVisible(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GuardarNotificacion() {
        Notificacion.ID_PROCESO = SelectedNotificacion.Proceso();
        Notificacion.ID_ETAPA = SelectedNotificacion.Etapa();
        Notificacion.TIPO_NOTIFICACION = SelectedNotificacion.TipoNotificacion();
        Notificacion.ASUNTO = SelectedNotificacion.Asunto();
        Notificacion.CUERPO_CORREO = CKEDITOR.instances['CorreoEditor'].getData();
        Notificacion.ENVIO_AUTOMATICO = SelectedNotificacion.EnvioAuto();
        Notificacion.NUMERO_REENVIO = SelectedNotificacion.NumReenvios();
        Notificacion.PERIODICIDAD_REENVIO = SelectedNotificacion.Periodicidad();
        Notificacion.ID_CONCEPTO = SelectedNotificacion.Concepto();
        Notificacion.ID_ROL = SelectedNotificacion.Rol();
        Notificacion.REMITENTE = SelectedNotificacion.Remitente();
        Notificacion.MOMENTO_ENVIO = SelectedNotificacion.MomentoEnvio();
        ActualizarNotificacion(Notificacion);
    }

    function handleViewShown(e) {
        StartCKEditor('CorreoEditor');
        CKEDITOR.instances['CorreoEditor'].setData(SelectedNotificacion.CuerpoCorreo());
        setTimeout(function () {
            loadingVisible(false);
        }, 500);
    }

    function handleviewDisposing(e) {
        $(window).off("resize", EditorResizer);

        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].destroy();
        }
    }

    function EditorResizer() {
        var CorreoEditor = $('#CorreoEditor');
        CKEDITOR.instances['CorreoEditor'].resize('100%', CorreoEditor.innerHeight(), false);
    }

    $(window).resize(EditorResizer);

    function ActualizarNotificacion(Notific) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ActualizarNotificacion",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Notificacion: Notific,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.ActualizarNotificacionResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    var res = DevExpress.ui.dialog.alert('La configuración de la notificación se ha realizado correctamente', SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }


    function onKeyDown(data, event) {
        if (data.jQueryEvent.keyCode == 8) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function onKeyDownNum(data, event) {
        //if (data.jQueryEvent.key in [",", "/", ".", "$", "[", "]", "e"]) {
        const str = data.jQueryEvent.char;
        if (/^[.,e-]$-/.test(str)) {
            data.event.preventDefault();
            data.jQueryEvent.stopPropagation();
        }
        return true;
    }

    function CancelarNotificacion() {
        if (BotonCancelar[CRUD].ShowAlert) {

            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', BotonCancelar[CRUD].AlertMessage), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    SGEWeb.app.navigationManager.back();
                }
            });
        } else {
            SGEWeb.app.navigationManager.back();
        }
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetNotificacion(ID_NOTIFIC);
        //GetProcesosNotif();
    }

    Refrescar();

    var viewModel = {
        //  Put the binding properties here
        CRUD: CRUD,
        BotonCancelar: BotonCancelar,
        BotonGuardar: BotonGuardar,
        DsProcesos: DsProcesos,
        DsEtapas: DsEtapas,
        DsRoles: DsRoles,
        DsTipoNotificaciones: DsTipoNotificaciones,
        DsConceptos: DsConceptos,
        DsParametros: DsParametros,
        Notificacion: Notificacion,
        SelectedNotificacion: SelectedNotificacion,
        onProcesoChanged: onProcesoChanged,
        onTipoNotifChanged: onTipoNotifChanged,
        onRolChanged: onRolChanged,
        onEtapaChanged: onEtapaChanged,
        onEnvioAutoChanged: onEnvioAutoChanged,
        onKeyDown: onKeyDown,
        onKeyDownNum: onKeyDownNum,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        CancelarNotificacion: CancelarNotificacion,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        DsMomentoEnvio: DsMomentoEnvio,
        ancho: ancho,
        EnvioAutodisable: EnvioAutodisable,
        viewShown: handleViewShown,
        viewDisposing: handleviewDisposing,
        title: title,
    };

    return viewModel;
};