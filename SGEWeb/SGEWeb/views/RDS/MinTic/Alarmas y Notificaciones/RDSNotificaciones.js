﻿SGEWeb.RDSNotificaciones = function (params) {
    "use strict";

    var dtsNotificaciones = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable("Configuración Notificaciones");
    var isLoading = ko.observable(false);
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;

    function GetNotificaciones() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetNotificaciones",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetNotificacionesResult;
                dtsNotificaciones(result);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarNotificacion':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar Notificación')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
        }
    }

    function NewNotificacion() {
        SGEWeb.app.navigate({ view: "RDSNotificacionesConfig", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Create, ROLE: ROLE, GROUP: GROUP, ID_NOTIFIC: '00000000-0000-0000-0000-000000000000' } });
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarNotificacion':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar Notificación')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetNotificaciones();
        //GetViabilidades();
        isLoading(true);
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSNotificacion') {
                switch (e.column.name) {
                    case 'ConfigurarNotificacion':
                        SGEWeb.app.Notificacion = JSON.stringify(e.data);
                        SGEWeb.app.navigate({ view: "RDSNotificacionesConfig", id: -1, settings: { title: 'ABC', CRUD: enumCRUD.Update, ROLE: ROLE, GROUP: GROUP, ID_NOTIFIC: e.data.ID_NOTIFICACIONES } });
                        break;
                }

            }
        }
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSNotificacion') {
            if (!isLoading())
                loadingVisible(false);
        }

        SGEWeb.app.DisabledToolBar(false);
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    Refrescar();

    var viewModel = {
        //  Put the binding properties here
        viewShown: handleViewShown,
        dtsNotificaciones: dtsNotificaciones,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        CellClick: CellClick,
        onContentReady: onContentReady,
        NewNotificacion: NewNotificacion,
    };

    return viewModel;
};