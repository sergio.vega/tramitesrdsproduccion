﻿SGEWeb.RDSProcesoEtapa = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var IdProceso = JSON.parse(params.settings.substring(5)).IdProceso;
    var dsEtapas = ko.observableArray([]);
    var NombreProceso = ko.observable("");
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var isLoading = ko.observable(false);
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var MaxEtapa = ko.observable(0);
    var EtapaBorrada = ko.observable(0);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) };
    var MyPopUp = {
        Datos: {
            IdEtapa: undefined,
            NombreEtapa: ko.observable(''),
        },
        Show: ko.observable(false),
        Title: ko.observable(''),
        CRUDType: ko.observable(0)
    };

    function ActualizarEtapas(ConfigEtapas) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ActualizarEtapas",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                IdProceso: IdProceso,
                NameProceso: NombreProceso(),
                ConfEtapas: ConfigEtapas
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.ActualizarEtapasResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    var res = DevExpress.ui.dialog.alert('La configuración del proceso se ha realizado correctamente', SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetProcesosEtapa() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetProcesosEtapa",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                IdProceso: IdProceso
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetProcesosEtapaResult;
                dsEtapas(result.Etapa);
                NombreProceso(result.NombreProceso)
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                    .prop('title', 'Eliminar')
                    .css('cursor', 'default')
                    .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                    .prop('title', 'Eliminar')
                    .css('cursor', 'pointer')
                    .css('color', e.data.TXT_SYSTEM ? 'lightgrey' : 'black')
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSEtapas') {
                switch (e.column.name) {
                    case 'Editar':
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title("Editar Etapa");
                        MyPopUp.Datos.IdEtapa = e.data.IdEtapa;
                        MyPopUp.Datos.NombreEtapa(e.data.NombreEtapa);
                        MyPopUp.Show(true);
                        break;
                    case 'Eliminar':
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', '¿Está seguro de eliminar la Etapa?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                EtapaBorrada(e.data.IdEtapa);
                                var factorAEliminar = dsEtapas().filter(function (obj) { return obj.IdEtapa === e.data.IdEtapa })[0];
                                var index = dsEtapas().indexOf(factorAEliminar);
                                if (index > -1) {
                                    dsEtapas.splice(index, 1);
                                }
                            }
                        })
                        //GridRDSFactores.option('dataSource', dsEtapas);
                        break;
                }
            }
        }
    }

    function CancelarRegistro() {
        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea cancelar la modificación?'), SGEWeb.app.Name);
        result.done(function (dialogResult) {
            if (dialogResult) {
                SGEWeb.app.navigationManager.back();
            }
        });
    }

    function OnVolver() {
        SGEWeb.app.navigationManager.back();
    }

    function onContentReady(e) {
        if (e.element[0].id == 'GridRDSEtapas') {
            if (!isLoading())
                loadingVisible(false);
        }
        SGEWeb.app.DisabledToolBar(false);
    }

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    function ActualizarProceso() {
        var dataConfiguracionEtapas = ko.toJS(dsEtapas);
        ActualizarEtapas(dataConfiguracionEtapas);
    }

    function OnGuardar() {
        if (IsNullOrEmpty(MyPopUp.Datos.NombreEtapa())) {
            DevExpress.ui.notify('La etapa es requerida.', "warning", 3000);
            return;
        }
        MyPopUp.Show(false);

        switch (MyPopUp.CRUDType()) {
            case 1:
                CalculaMaxEtapa();
                dsEtapas.push({
                    IdEtapa: MaxEtapa(),
                    NombreEtapa: MyPopUp.Datos.NombreEtapa(),
                });
                break;
            case 3:
                var factorAEditar = dsEtapas().filter(function (obj) { return obj.IdEtapa === MyPopUp.Datos.IdEtapa })[0];
                factorAEditar.NombreEtapa = MyPopUp.Datos.NombreEtapa();
                break;
        }

        //GridRDSEtapas.option('dataSource', dsEtapas);
    }

    function CalculaMaxEtapa() {
        var max = 0;
        dsEtapas().forEach(function (etapa) {
            if (max < etapa.IdEtapa)
                max = etapa.IdEtapa;
        });
        MaxEtapa(max + 1);
        if (EtapaBorrada() == MaxEtapa())
            MaxEtapa(EtapaBorrada() + 1)
    }

    function NewEtapa() {
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nueva Etapa");
        MyPopUp.Datos.Id = dsEtapas.length;
        MyPopUp.Datos.NombreEtapa("");
        MyPopUp.Show(true);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetProcesosEtapa();
        isLoading(true);
    }

    Refrescar();


    var viewModel = {
        NombreProceso: NombreProceso,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        CellClick: CellClick,
        onContentReady: onContentReady,
        dsEtapas: dsEtapas,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        NewEtapa: NewEtapa,
        OnGuardar: OnGuardar,
        MyPopUp: MyPopUp,
        CancelarRegistro: CancelarRegistro,
        OnVolver: OnVolver,
        ActualizarProceso: ActualizarProceso,
        //  Put the binding properties here
    };

    return viewModel;
};