﻿SGEWeb.RDSProcesos = function (params) {
    "use strict";
    var ConfiguracionesProcesos = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var isLoading = ko.observable(false);
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;

    function GetProcesos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetProcesos",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetProcesosResult;
                ConfiguracionesProcesos(result);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarProceso':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar proceso')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
        }
    }


    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'ConfigurarProceso':
                $('<i/>').addClass('ta ta-tecnico ta-lg')
                    .prop('title', 'Configurar proceso')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetProcesos();
        //GetViabilidades();
        isLoading(true);
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSProcesos') {
                switch (e.column.name) {
                    case 'ConfigurarProceso':
                        SGEWeb.app.navigate({ view: "RDSProcesoEtapa", id: -1, settings: { title: 'ABC', IdProceso: e.data.IdProceso, ROLE: ROLE, GROUP: GROUP } });
                        break;
                }

            }
        }
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSProcesos') {
            if (!isLoading())
                loadingVisible(false);
        }

        SGEWeb.app.DisabledToolBar(false);
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    Refrescar();

    var viewModel = {
        //  Put the binding properties here
        viewShown: handleViewShown,
        ConfiguracionesProcesos: ConfiguracionesProcesos,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        CellClick: CellClick,
        onContentReady: onContentReady,
    };

    return viewModel;
};