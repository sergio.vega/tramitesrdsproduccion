﻿SGEWeb.RDSResoluciones = function (params) {
    "use strict";
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var Solicitud = JSON.parse(SGEWeb.app.Solicitud);
    var MyDataSourcePlts = ko.observableArray([]);
    var loadingVisible = ko.observable(true);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var MyPopUpPlts = { Show: ko.observable(false), PltSelected: ko.observable() };
    var MyPopUpVersion = { Show: ko.observable(false), RES_CODIGO: ko.observable(), RES_VERSION: ko.observable()};
    var ControlCambioID = ListObjectGetAttribute(SGEWeb.app.ROLE, 'value', ROLE, 'ControlCambioID', 7);
    var PLANTILLA_LOADED = ko.observable(Solicitud.Resolucion.RES_IS_CREATED);
    var RES_IS_CREATED = ko.observable(Solicitud.Resolucion.RES_IS_CREATED);
    var StatusBarVersion = ko.observable(IsNullOrEmpty(Solicitud.Resolucion.RES_CODIGO) ? '' : Solicitud.Resolucion.RES_CODIGO + ' (' + Solicitud.Resolucion.RES_VERSION + ')');
    var liteRef = undefined;
    if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
        CKEDITOR.tools.enableHtml5Elements(document);

    CKEDITOR.config.width = '100%';

    function CKEDITORCargarConfiguracion(config) {
        config.toolbar = [
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
            { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
            { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'CopyFormatting', 'RemoveFormat'] },
            { name: 'insert', items: ['SpecialChar', 'Table'] },
            { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'styles', items: ['Font', 'FontSize'] },
            { name: 'colors', items: ['TextColor', 'BGColor'] },
            { name: 'lite', items: ['lite-toggletracking', 'lite-toggleshow', 'lite-acceptall', 'lite-rejectall', 'lite-acceptone', 'lite-rejectone', 'lite-toggletooltips'] },
        ];

     
        config.removePlugins = 'flash,elementspath';

        config.font_names = "Arial/Arial, Helvetica, sans-serif;Arial Narrow/Arial Narrow;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
        config.fontSize_sizes = "8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;15/15pt;16/16pt;18/18pt;20/20pt;22/22pt;24/24pt;26/26pt;28/28pt;36/36pt;48/48pt;72/72pt";

        config.entities = false;
        config.entities_greek = false;
        config.entities_latin = true;
        config.entities_processNumerical = false;

        config.language = 'es';
        config.language_list = ["es:Español"];
        config.scayt_sLang = "es_ES";

        var lite = config.lite = config.lite || {};
        lite.lang = 'es';
        lite.isTracking = true;
        lite.userName = SGEWeb.app.User.USR_NAME;
        lite.userId = SGEWeb.app.User.USR_ID;
        lite.userStyles = {};
        lite.userStyles[SGEWeb.app.User.USR_ID] = ControlCambioID;
        lite.tooltipTemplate = "%a por %u<BR>%t";
        config.height = '1000'; //Debe ser grande para que no se vea el efecto del resize
        config.resize_enabled = false;



        //if(ROLE==enumROLES.Funcionario)
    }

    var StartCKEditor = (function (elementID) {
        var wysiwygareaAvailable = isWysiwygareaAvailable(),
            isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

        return function (elementID) {
            var editor = CKEDITOR.document.getById(elementID);

            var ResolucionEditor = $('#ResolucionEditor');

            if (wysiwygareaAvailable) {
                CKEDITOR.replace(elementID, {
                    on: {
                        'instanceReady': function (evt) {
                            evt.editor.resize("100%", ResolucionEditor.innerHeight()-35, false);
                            this.setReadOnly(!Solicitud.Resolucion.RES_IS_CREATED);
                            liteRef = evt.editor.plugins.lite.findPlugin(evt.editor);
                            if (ROLE == enumROLES.Funcionario) {
                                if (Solicitud.AnalisisResolucion.RES_ROLE2_STATE_ID == enumROLE248_STATE.SinDefinir) {
                                    liteRef.toggleTracking(false, false);
                                }
                            }
                        }
                    }
                }
               );

            } else {
                editor.setAttribute('contenteditable', 'true');
                CKEDITOR.inline(elementID);
            }
            CKEDITOR.instances[elementID].config.CustomConfig = CKEDITORCargarConfiguracion;
        };

        function isWysiwygareaAvailable() {
            if (CKEDITOR.revision == ('%RE' + 'V%')) {
                return true;
            }
            return !!CKEDITOR.plugins.get('wysiwygarea');
        }
    })();

    function GetResolucionesPlantillas(SOL_TYPE_ID_FILTER, RES_PLT_STATUS_FILTER) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetResolucionesPlantillas",
            data: JSON.stringify({
                SOL_TYPE_ID_FILTER: SOL_TYPE_ID_FILTER,
                RES_PLT_STATUS_FILTER: RES_PLT_STATUS_FILTER
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                MyDataSourcePlts(msg.GetResolucionesPlantillasResult);
                MyPopUpPlts.PltSelected(null);
                MyPopUpPlts.Show(true);
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function ReplaceResolucionPlantilla(ResPlt, Solicitud) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ReplaceResolucionPlantilla",
            data: JSON.stringify({
                ResPlt: ResPlt,
                Solicitud: Solicitud
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.ReplaceResolucionPlantillaResult;

                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].setReadOnly(false);
                }

                CKEDITOR.instances['ResolucionEditor'].setData(result.RES_PLT_CONTENT);
                Solicitud.Resolucion.RES_CODIGO = result.RES_PLT_CODIGO;
                Solicitud.Resolucion.RES_VERSION = result.RES_PLT_VERSION;
                StatusBarVersion(Solicitud.Resolucion.RES_CODIGO + ' (' + Solicitud.Resolucion.RES_VERSION + ')');
                PLANTILLA_LOADED(true);
                setTimeout(function () {
                    liteRef.toggleTracking(false, false);
                }, 1000);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDResolucion(Resolucion) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDResolucion",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Resolucion: Resolucion
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDResolucionResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    Solicitud.Resolucion = result.Resolucion;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    SGEWeb.app.NeedRefresh = true;
                    SGEWeb.app.CallBackFunction(Solicitud.Resolucion);
                    RES_IS_CREATED(true);
                }
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function OnSelectPlantilla() {
        if (MyPopUpPlts.PltSelected() == null) {
            DevExpress.ui.notify('Debe seleccionar una plantilla de resolución.', "warning", 5000);
            return;
        }
        if (Solicitud.Resolucion.RES_IS_CREATED) {
            var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Cargar una plantilla sobrescribe la resolución existente. Desea continuar?'), SGEWeb.app.Name);
            result.done(function (dialogResult) {
                if (dialogResult) {
                    var ResPlt = ListObjectFindGetElement(MyDataSourcePlts(), 'RES_PLT_UID', MyPopUpPlts.PltSelected());
                    MyPopUpPlts.Show(false);
                    loadingMessage('Cargando...');
                    loadingVisible(true);
                    ReplaceResolucionPlantilla(ResPlt, Solicitud);
                } else {
                    MyPopUpPlts.Show(false);
                }
            });
        } else {
            var ResPlt = ListObjectFindGetElement(MyDataSourcePlts(), 'RES_PLT_UID', MyPopUpPlts.PltSelected());
            MyPopUpPlts.Show(false);
            loadingMessage('Cargando...');
            loadingVisible(true);
            ReplaceResolucionPlantilla(ResPlt, Solicitud);
        }
    }

    function OnOpenPlantilla() {
        //SGEWeb.app.CallBackFunction('Carlos Guingue');
        loadingVisible(true);
        GetResolucionesPlantillas(Solicitud.SOL_TYPE_ID, 1);
    }

    function OnSave() {

        loadingMessage('Guardando...');
        loadingVisible(true);
        GuardarDisabled(true);

        var Resol = {
            SOL_UID: Solicitud.SOL_UID,
            RES_NAME: 'Resolución ' + Solicitud.SOL_TYPE_NAME + ' (' + Solicitud.Expediente.SERV_NUMBER + ')',
            RES_CONTENT: CKEDITOR.instances['ResolucionEditor'].getData(),
            RES_IS_CREATED : Solicitud.Resolucion.RES_IS_CREATED,
            RES_CODIGO: Solicitud.Resolucion.RES_CODIGO,
            RES_VERSION: Solicitud.Resolucion.RES_VERSION,
            RES_TRACKING_COUNT: liteRef.countChanges()
        };

        CRUDResolucion(Resol);
    }

    function OnPdf() {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=RES&FUID=' + Solicitud.SOL_UID + '&MRC=0&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function OnVersionClick() {
        MyPopUpVersion.RES_CODIGO(Solicitud.Resolucion.RES_CODIGO);
        MyPopUpVersion.RES_VERSION(Solicitud.Resolucion.RES_VERSION);
        MyPopUpVersion.Show(true);
    }

    function OnGuardarVersion() {
        if (IsNullOrEmpty(MyPopUpVersion.RES_CODIGO())) {
            DevExpress.ui.notify('El código de la resolución es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(MyPopUpVersion.RES_VERSION())) {
            DevExpress.ui.notify('La versión de la resolución es requerida.', "warning", 3000);
            return;
        }
        Solicitud.Resolucion.RES_CODIGO = MyPopUpVersion.RES_CODIGO();
        Solicitud.Resolucion.RES_VERSION = MyPopUpVersion.RES_VERSION();
        StatusBarVersion(Solicitud.Resolucion.RES_CODIGO + ' (' + Solicitud.Resolucion.RES_VERSION + ')');
        MyPopUpVersion.Show(false);
    }

    function handleviewDisposing(e) {
        $(window).off("resize", EditorResizer);

        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].destroy();
        } 
    }

    function handleViewShown(e) {
        StartCKEditor('ResolucionEditor');
        CKEDITOR.instances['ResolucionEditor'].setData(Solicitud.Resolucion.RES_CONTENT);
        if (!Solicitud.Resolucion.RES_IS_CREATED) {
            OnOpenPlantilla();
        } else {
            setTimeout(function () {
                loadingVisible(false);
            }, 500);
        }
    }

    function EditorResizer() {
        var ResolucionEditor = $('#ResolucionEditor');
        CKEDITOR.instances['ResolucionEditor'].resize('100%', ResolucionEditor.innerHeight()-35, false);
    }

    function onKeyDownQ(a,b,c,d,e,f,g,h){
        a=a;
    }

    $(window).resize(EditorResizer);

    var viewModel = {
        viewShown: handleViewShown,
        viewDisposing: handleviewDisposing,
        ROLE: ROLE,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        OnSave: OnSave,
        title: 'Resolución ' + Solicitud.SOL_TYPE_NAME  + ' (' + Solicitud.Expediente.SERV_NUMBER + ')',
        OnPdf: OnPdf,
        OnSelectPlantilla: OnSelectPlantilla,
        MyDataSourcePlts: MyDataSourcePlts,
        MyPopUpPlts: MyPopUpPlts,
        MyPopUpVersion:MyPopUpVersion,
        OnOpenPlantilla: OnOpenPlantilla,
        RES_IS_CREATED: RES_IS_CREATED,
        PLANTILLA_LOADED: PLANTILLA_LOADED,
        OnVersionClick: OnVersionClick,
        StatusBarVersion: StatusBarVersion,
        OnGuardarVersion: OnGuardarVersion,
        onKeyDownQ: onKeyDownQ
    };

    return viewModel;
};
