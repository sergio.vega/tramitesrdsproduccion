﻿SGEWeb.RDSObservaciones = function (params) {
    "use strict";
    var title = JSON.parse(params.settings.substring(5)).title;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var Cantidades = ko.observable(0);
    var IsFilter = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var isLoading = ko.observable(false);
    var MyPopUpReasign = { Show: ko.observable(false), Title: ko.observable(''), Text: ko.observable(''), UserList: ko.observableArray([]), UserSelected: ko.observable(null), ColumnROLE: ko.observable(0), Observacion: undefined, TextVisible: ko.observable(true), SubType: 0, ColumnROLE_TEXT: ko.observable('') };

    function GetObservacionesMinTIC() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetObservacionesMinTIC",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                USR_ROLE: ROLE,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetObservacionesMinTICResult;
                MyDataSource(result.Observaciones);
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function ReasignarObservacion(Reasign) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ReasignarObservacion",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Observacion: MyPopUpReasign.Observacion,
                Reasign: Reasign
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);

                var result = msg.ReasignarObservacionResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                }
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'GridRDSObservaciones') {
            if (!isLoading())
                loadingVisible(false);
            Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
          SGEWeb.app.DisabledToolBar(false);
        } 
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Responder':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Responder')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'ReasignRole1':
                $('<i/>').addClass('ta ta-funcionario ta-lg')
                .prop('title', 'Reasignación funcionario')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Responder':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Responder')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;

            case 'ReasignRole1':
                if (e.data.ESTADO == "Pendiente") {
                    $('<i/>').addClass('ta ta-funcionario ta-lg')
                    .prop('title', 'Observaciones de reasignación funcionario')
                    .css('cursor', 'pointer')
                    .css('color', e.data.USR_ROLE1_REASIGN ? 'black' : 'lightgrey')
                    .appendTo(container);
                }
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSObservaciones') {
                switch (e.column.name) {
                    case 'Responder':
                        SGEWeb.app.Observaciones = e.data;
                        SGEWeb.app.navigate({ view: "RDSRespuestaObservacion", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                        break;

                    case 'ReasignRole1':
                        if (e.data.ESTADO != "Pendiente") {
                            e.event.stopPropagation();
                            return;
                        }
                        if (ROLE != enumROLES.Funcionario) {
                            MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                                if (GROUP == enumGROUPS.MinTIC)
                                    return (Item.USR_ID != e.data.ID_FUNCIONARIO)  && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Funcionario);
                            }));
                        }
                        MyPopUpReasign.ColumnROLE(enumROLES.Funcionario);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Observacion = e.data;
                        MyPopUpReasign.Title(ROLE == enumROLES.Funcionario || e.data.USR_ROLE1_REASIGN ? 'Petición de reasignación' : 'Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('funcionario');
                        MyPopUpReasign.TextVisible(ROLE == enumROLES.Funcionario || e.data.USR_ROLE1_REASIGN ? true : false);
                        MyPopUpReasign.Text(e.data.USR_ROLE1_REASIGN_COMMENT);
                        MyPopUpReasign.SubType = e.data.USR_ROLE1_REASIGN ? enumREASIGN_TYPE.Aprobar : enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;
                    
                }
            } 
        }
    }

    function Excel() {
        var grid = $("#GridRDSObservaciones").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Cargando...');
        loadingVisible(true);
        GetObservacionesMinTIC();
        GuardarDisabled(false);
        isLoading(true);
    }

    function popupShowing(e) {
        $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    
    
    function Filtrar() {
        var grid = $("#GridRDSObservaciones").dxDataGrid('instance');
        if (grid != undefined) {
            if (IsFilter()) {
                IsFilter(false);
                grid.clearFilter('dataSource');
            } else {
                IsFilter(true);
                grid.filter([["ROL_ACTUAL", ROLE]]);
            }
        }
    }

    function OnColumnChooser() {
        var grid = $("#GridRDSObservaciones").dxDataGrid('instance');
        if (grid != undefined)
            grid.showColumnChooser();
    }

    function OnCustomLoad() {
        var state = localStorage.getItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb);
        if (state) {
            state = JSON.parse(state);
        }
        return state;
    }

    function OnCustomSave(state) {
        for (var i = 0; i < state.columns.length; i++) {
            state.columns[i].filterValue = null;
            state.columns[i].filterValues = null;
            state.columns[i].sortIndex = null;
            state.columns[i].sortOrder = null;
        }
        localStorage.setItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb, JSON.stringify(state));

    }

    function OnReasignar(e) {

        switch (e.model.options.name) {

            case 'Solicitar':
                if (IsNullOrEmpty(MyPopUpReasign.Text()) || MyPopUpReasign.Text().length < 10) {
                    DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                    return;
                }

                var Reasign = {
                    TYPE: enumREASIGN_TYPE.Solicitar,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    ID_OBSERVACION: MyPopUpReasign.Observacion.ID_OBSERVACION,
                    USR_ID: SGEWeb.app.IDUserWeb,
                    COMMENT: MyPopUpReasign.Text(),
                    NEW_USR_ID: -1
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarObservacion(Reasign);
                MyPopUpReasign.Show(false);
                break;

            case 'Rechazar':
                var Reasign = {
                    TYPE: enumREASIGN_TYPE.Rechazar,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    ID_OBSERVACION: MyPopUpReasign.Observacion.ID_OBSERVACION,
                    USR_ID: MyPopUpReasign.Observacion.ID_FUNCIONARIO,
                    COMMENT: '',
                    NEW_USR_ID: -1
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarObservacion(Reasign);
                MyPopUpReasign.Show(false);
                break;

            case 'Aceptar':
                if (MyPopUpReasign.UserSelected() == null) {
                    DevExpress.ui.notify('Debe seleccionar un usuario. Si la lista está vacia, es porque no hay usuarios disponibles para ser reasignados.', "warning", 5000);
                    return;
                }

                var Reasign = {
                    TYPE: MyPopUpReasign.SubType,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    ID_OBSERVACION: MyPopUpReasign.Observacion.ID_OBSERVACION,
                    USR_ID: MyPopUpReasign.Observacion.ID_FUNCIONARIO,
                    COMMENT: '',
                    NEW_USR_ID: MyPopUpReasign.UserSelected()
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarObservacion(Reasign);
                MyPopUpReasign.Show(false);
                break;

        }

    }

    Refrescar();

    var viewModel = {
        viewShown: handleViewShown,
        title: title,
        MyDataSource: MyDataSource,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onContentReady: onContentReady,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        Excel: Excel,
        Refrescar: Refrescar,
        Cantidades: Cantidades,
        ROLE: ROLE,
        GuardarDisabled: GuardarDisabled,
        onHeaderTemplate: onHeaderTemplate,
        GROUP: GROUP,
        Filtrar: Filtrar,
        IsFilter: IsFilter,
        OnColumnChooser: OnColumnChooser,
        OnCustomLoad: OnCustomLoad,
        OnCustomSave: OnCustomSave,
        isLoading: isLoading,
        MyPopUpReasign: MyPopUpReasign,
        OnReasignar: OnReasignar,

    };

    return viewModel;

};