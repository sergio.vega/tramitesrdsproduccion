﻿SGEWeb.RDSRespuestaObservacion = function (params) {
    SGEWeb.app.NeedRefresh = false;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var CRUD = JSON.parse(params.settings.substring(5)).CRUD;
    var SOL_TYPE_ID = JSON.parse(params.settings.substring(5)).SOL_TYPE_ID;
    var Contacto = JSON.parse(params.settings.substring(5)).Contacto;
    var Emails = JSON.parse(params.settings.substring(5)).Emails;
    var Observaciones = ko.mapping.fromJS(SGEWeb.app.Observaciones);
    Observaciones.FECHA_OBSERVACION(StringToDate(Observaciones.FECHA_OBSERVACION()));
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Radicando...");
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true), Observacion: ko.observable(true) }
    var Adjuntos = ko.mapping.fromJS(Observaciones.Adjuntos);
    var GuardarDisabled = ko.observable(false);

    var BotonDisabled = ko.computed(function () {
        if (ROLE == enumROLES.Funcionario) {
            if (Observaciones.ROL_ACTUAL() == enumROLES.Funcionario && Observaciones.ESTADO() == 'Pendiente') {
                GuardarDisabled(false);
                return false;
            }

        }
        if (ROLE == enumROLES.Coordinador) {
            if (Observaciones.ROL_ACTUAL() == enumROLES.Coordinador && Observaciones.ESTADO() == 'Por Aprobar') {
                GuardarDisabled(false);
                return false;
            }
        }
        GuardarDisabled(true);
        return true;
    });

    function GuardarObservacion() {
        var estadoObs = '';
        SGEWeb.app.DisabledToolBar(true);
        if (Observaciones.ROL_ACTUAL() == enumROLES.Coordinador) {
            loadingMessage('Devolviendo respuesta...');
            estadoObs = 'Devuelta';
        }
        else if (Observaciones.ROL_ACTUAL() == enumROLES.Funcionario){
            loadingMessage('Guardando respuesta...');
            estadoObs = '';
        }
        loadingVisible(true);
        GuardarDisabled(true);
        Observaciones.ESTADO('Pendiente');
        Observaciones.ROL_ACTUAL(1);
        var respObservacion = ko.toJS(Observaciones);
        ResponderObservacionOtorga(respObservacion, false, estadoObs);
    }

    function ResponderObservacion() {
        SGEWeb.app.DisabledToolBar(true);
        if (Observaciones.ROL_ACTUAL() == enumROLES.Coordinador) {
            loadingMessage('Aprobando respuesta...');
            Observaciones.ESTADO('Resuelta');
            estadoObs = 'Resuelta';
        }
        else if (Observaciones.ROL_ACTUAL() == enumROLES.Funcionario) {
            if (Observaciones.RESPUESTA() == "" || Observaciones.RESPUESTA() == null) {
                DevExpress.ui.notify('La respuesta de la Observación no puede quedar vacia.', "warning", 3000);
                SGEWeb.app.DisabledToolBar(false);
                return;
            }

            loadingMessage('Respondiendo observación...');
            Observaciones.ESTADO('Por Aprobar');
            Observaciones.FECHA_RESPUESTA(null);
            estadoObs = '';
        }
        loadingVisible(true);
        GuardarDisabled(true);        
        Observaciones.ROL_ACTUAL(4);
        var respObservacion = ko.toJS(Observaciones);
        if (Observaciones.ROL_ACTUAL() == enumROLES.Coordinador)
            ResponderObservacionOtorga(respObservacion, true, estadoObs);
        else if (Observaciones.ROL_ACTUAL() == enumROLES.Funcionario)
            ResponderObservacionOtorga(respObservacion, false, estadoObs);
    }

    function ResponderObservacionOtorga(Observacion, RespondePregunta, estadoObs) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ResponderObservacionOtorga",
            data: JSON.stringify({
                Observacion: Observacion,
                RespondePregunta: RespondePregunta,
                estadoObs: estadoObs
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

                var result = msg.ResponderObservacionOtorgaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                    GuardarDisabled(false);
                } else {

                    var res = DevExpress.ui.dialog.alert(result.Message, SGEWeb.app.Name);

                    res.done(function (dialogResult) {
                        SGEWeb.app.NeedRefresh = true;
                        SGEWeb.app.navigationManager.back();
                        GuardarDisabled(false);
                    });
                }
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }


    function CellClick(e) {
        if (e.rowType == 'data') {
            switch (e.element[0].id) {
                case 'GridAdjuntos':
                    switch (e.column.name) {
                        case 'Ver':
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=OBS&FUID=' + e.data.ID_IMAGEN() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                            break;
                    }
                    break;
            }
        }
    }


    function onCellTemplate(container, e) {
        switch (e.component._$element[0].id) {
            case 'GridAdjuntos':
                switch (e.column.name) {
                    case 'Ver':
                        $('<i/>').addClass('ta ta-eye ta-lg')
                        .prop('title', 'Ver documento')
                        .css('cursor', 'pointer')
                        .appendTo(container);
                        break;
                }
                break;
        }
    }


    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
            case 'Observacion':
                ToggleVisibility.Observacion(!ToggleVisibility.Observacion());
                break;
        }
    }


    var viewModel = {
        Contacto: Contacto,
        Observaciones: Observaciones,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        onCellTemplate: onCellTemplate,
        ResponderObservacion: ResponderObservacion,
        GuardarObservacion: GuardarObservacion,
        SOL_TYPE_ID: SOL_TYPE_ID,
        enumSOL_TYPES: enumSOL_TYPES,
        Adjuntos: Adjuntos,
        CellClick: CellClick,
        GuardarDisabled: GuardarDisabled,
        ROLE: ROLE,
    }


    return viewModel;
};