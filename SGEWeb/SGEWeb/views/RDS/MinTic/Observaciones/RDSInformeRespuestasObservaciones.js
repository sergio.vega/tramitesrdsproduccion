﻿SGEWeb.RDSInformeRespuestasObservaciones = function (params) {
    "use strict";
    var ConfiguracionObservacionesOtorga = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var title = ko.observable(SGEWeb.app.User.USER_NAME);
    var isLoading = ko.observable(false);

    function GetConfiguracionObservaciones() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConfiguracionObservaciones",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConfiguracionObservacionesResult;
                ConfiguracionObservacionesOtorga(result.Configuraciones);
                SGEWeb.app.Configuraciones = result.Configuraciones;
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GenerarInformeRespuestasObservaciones(Convocatoria) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GenerarInformeRespuestasObservaciones",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Convocatoria: Convocatoria,
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call
                var result = msg.GenerarInformeRespuestasObservacionesResult;
                if (result != null) {
                    DescargarWord(result);
                } else {
                    DevExpress.ui.notify('No hay Respuestas a observaciones de esta convocatoria', "warning", 3000);
                }
                loadingVisible(false);
            },
            error: function (result) {
                DevExpress.ui.notify('No se pudo generar el informe', "error", 3000);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'RDSInformeRespuestasObservaciones') {
            if (!isLoading())
                loadingVisible(false);
        }

        SGEWeb.app.DisabledToolBar(false);
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'GenerarInforme':
                $('<i/>').addClass('ta ta-word ta-lg')
                    .prop('title', 'Generar informe')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'GenerarInforme':
                $('<i/>').addClass('ta ta-word ta-lg')
                    .prop('title', 'Generar informe')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'RDSInformeRespuestasObservaciones') {
                switch (e.column.name) {
                    case 'GenerarInforme':
                        var convocatoria = ko.toJS(e.data);
                        GenerarInformeRespuestasObservaciones(convocatoria)
                        break;
                }

            }
        }
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }


    function ViabilidadAutomatica(e) {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionObservaciones();
        isLoading(true);
    }

    function DescargarWord(byte) {
        var byteArray = new Uint8Array(byte);
        const blob = new Blob([byteArray]);
        const fileName = "Informe de Respuestas a Observaciones.docx";
        if (navigator.msSaveBlob) {
            navigator.msSaveBlob(blob, fileName);
        } else {
            const link = document.createElement('a');
            if (link.download !== undefined) {
                const url = URL.createObjectURL(blob);
                link.setAttribute('href', url);
                link.setAttribute('download', fileName);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionObservaciones();
        isLoading(true);
    }

    Refrescar();


    var viewModel = {
        viewShown: handleViewShown,
        ConfiguracionObservacionesOtorga: ConfiguracionObservacionesOtorga,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        title: title,
        Refrescar: Refrescar,
        isLoading: isLoading,
        ViabilidadAutomatica: ViabilidadAutomatica,
    };

    return viewModel;
};