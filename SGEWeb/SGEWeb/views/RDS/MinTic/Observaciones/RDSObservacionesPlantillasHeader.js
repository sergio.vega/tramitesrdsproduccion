﻿SGEWeb.RDSObservacionesPlantillasHeader = function (params) {
    "use strict";
    var MyDataSource = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var Cantidades = ko.observable(0);
    var GuardarDisabled = ko.observable(false);

    var MyPopUp = {
        ObsPlt: ko.observable({}),
        Show: ko.observable(false),
        Editar: ko.observable(false),
        Title: ko.observable(''),
        CRUDType: ko.observable(0)
    };

    function GetObservacionesPlantillas() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetObservacionesPlantillas",
            data: JSON.stringify({
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                MyDataSource(msg.GetObservacionesPlantillasResult);

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetObservacionesCombos() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetObservacionesCombos",
            data: JSON.stringify({

            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                SGEWeb.app.CombosObservaciones = msg.GetObservacionesCombosResult;

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDObservacionesPlantillas(ObsPlt) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDObservacionesPlantillas",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ObsPlt: ObsPlt
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);
                var result = msg.CRUDObservacionesPlantillasResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    MyPopUp.Show(false);
                    Refrescar();
                }
                GuardarDisabled(false)

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function Excel() {
        var grid = $("#GridRDSObservacionesPlantillas").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function NewPlantilla() {
        var ObsPlt = {
            ID_PLANTILLA: '',
            NOMBRE: '',
            PLT_HTML: '',
        };

        MyPopUp.ObsPlt(ko.mapping.fromJS(ko.toJS(ObsPlt)));
        MyPopUp.CRUDType(1);
        MyPopUp.Title("Nueva plantilla de Observación");
        MyPopUp.Show(true);
        MyPopUp.Editar(false);

        DevExpress.ui.notify('El identificador no se podrá modificar', "info", 5000);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetObservacionesPlantillas();
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                    .prop('title', 'Eliminar')
                    .css('cursor', 'default')
                    .appendTo(container);
                break;
            case 'View':
                $('<i/>').addClass('ta ta-eye ta-lg')
                    .prop('title', 'Ver plantilla')
                    .css('cursor', 'default')
                    .appendTo(container);
                break;
        }
    }
    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Editar':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Editar')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Eliminar':
                $('<i/>').addClass('ta ta-trash ta-lg')
                    .prop('title', 'Eliminar')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(container);
                break;
            case 'View':
                $('<i/>').addClass('ta ta-eye ta-lg')
                    .prop('title', 'Ver plantilla')
                    .css('cursor', 'pointer')
                    .css('color', 'black')
                    .appendTo(container);
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSObservacionesPlantillas') {
                switch (e.column.name) {
                    case 'Editar':

                        MyPopUp.ObsPlt(ko.mapping.fromJS(ko.toJS(e.data)));
                        MyPopUp.CRUDType(3);
                        MyPopUp.Title('Editar plantilla de Observación');
                        MyPopUp.Show(true);
                        MyPopUp.Editar(true);
                        break;

                    case 'View':
                        SGEWeb.app.PlantillaObservacion = JSON.stringify(e.data);
                        SGEWeb.app.navigate({ view: "RDSObservacionesPlantillasDetail", id: -1, settings: { title: 'ABC' } });
                        break;

                    case 'Eliminar':                        

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', "Desea eliminar la plantilla de Observación, podría estar siendo usada?"), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                SGEWeb.app.DisabledToolBar(true);
                                GuardarDisabled(true);
                                loadingVisible(true);

                                var ObsPlt = ko.toJS(e.data);
                                ObsPlt.PLT_CRUD = 4;
                                CRUDObservacionesPlantillas(ObsPlt);
                            }
                        });
                        break;
                }
            }
        }
    }

    function onContentReady(e) {
        Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
        SGEWeb.app.DisabledToolBar(false);
    }

    function OnGuardar() {

        if (IsNullOrEmpty(MyPopUp.ObsPlt().ID_PLANTILLA())) {
            DevExpress.ui.notify('El Identificador de la plantilla es requerido.', "warning", 3000);
            return;
        }

        if (IsNullOrEmpty(MyPopUp.ObsPlt().NOMBRE())) {
            DevExpress.ui.notify('El nombre de la plantilla es requerido.', "warning", 3000);
            return;
        }
        

        SGEWeb.app.DisabledToolBar(true);
        GuardarDisabled(true);
        loadingVisible(true);

        var ObsPlt = ko.toJS(MyPopUp.ObsPlt());
        ObsPlt.PLT_CRUD = MyPopUp.CRUDType();
        CRUDObservacionesPlantillas(ObsPlt);

    }

    function handleViewShown(e) {
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    GetObservacionesCombos();
    Refrescar();

    var viewModel = {
        viewShown: handleViewShown,
        Refrescar: Refrescar,
        MyDataSource: MyDataSource,
        GuardarDisabled: GuardarDisabled,
        loadingVisible: loadingVisible,
        Cantidades: Cantidades,
        onHeaderTemplate: onHeaderTemplate,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        onContentReady: onContentReady,
        Excel: Excel,
        OnGuardar: OnGuardar,
        MyPopUp: MyPopUp,
        //OnCopyClipBoardTitle: OnCopyClipBoardTitle,
        //OnCopyClipBoardText: OnCopyClipBoardText,
        NewPlantilla: NewPlantilla
    };
    return viewModel;
};