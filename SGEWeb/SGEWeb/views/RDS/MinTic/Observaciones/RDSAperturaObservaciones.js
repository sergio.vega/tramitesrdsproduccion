﻿SGEWeb.RDSAperturaObservaciones = function (params) {
    "use strict";

    var Configuracion = ko.mapping.fromJS(SGEWeb.app.ConfigObservacionOtorga);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var categorias = ko.observableArray(SGEWeb.app.Categorias);
    var configuracionesGlobales = ko.observableArray(SGEWeb.app.Configuraciones);
    var IdsCategoriasSeleccionadas = $.map(ko.toJS(Configuracion.Categorias()), function (item) { return item.ID_CATEGORIA; });
    var etapas = ko.observableArray(ko.toJS(Configuracion.Etapas()));
    var FechaMin = ko.observable(new Date());
    var FechaInicioAnterior = ko.observable(new Date());
    var FechaFinAnterior = ko.observable(new Date());

    var PopUpCrearEtapa = {
        Etapa: ko.observable({}),
        Show: ko.observable(false),
        Title: ko.observable(''),
    };

    var PopUpCRUDCategorias = {
        Show: ko.observable(false),
        Title: ko.observable(''),
    };

    var PopUpCrearCategoria = {
        Categoria: ko.observable({}),
        Show: ko.observable(false),
        Title: ko.observable(''),
    };

    function EliminarObjeto(array, valor, key) {
        var aux = ko.toJS(array);
        if (key != null) {
            aux.forEach(function (item, index) {
                if (item[key] == valor) {
                    array.splice(index, 1);
                }
            });
        } else {
            aux.forEach(function (item, index) {
                if (item == valor) {
                    array.splice(index, 1);
                }
            });
        }
    }

    function BuscarObjeto(array, valor, key) {
        var aux = ko.toJS(array);
        var objeto = null;
        if (key != null) {
            aux.forEach(function (item) {                
                var itemAux = item[key];
                if (typeof (itemAux) == 'string') {
                    itemAux = itemAux.toUpperCase();
                }
                if (itemAux == valor) {
                    objeto = item;
                }                
            });
        }        
        return objeto;
    }

    function ConvertirAFecha(cadenaFecha) {
        var formato = cadenaFecha.replace(/(\d{4})(\d{2})(\d{2})/g, '$1-$2-$3');
        var fecha = new Date(formato);
        fecha.setMinutes(fecha.getMinutes() + fecha.getTimezoneOffset());
        return fecha;
    }

    function ordenarEtapas() {
        var etapasAux = ko.toJS(etapas);
        var ordenado = etapasAux.sort(function (a, b) {
            var x = a['FECHA_INICIO'],
                y = b['FECHA_INICIO'];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
        etapas(ordenado);
    }

    function convertirFechasEtapas() {
        var etapasAux = ko.toJS(etapas);
        if (etapasAux.length != 0) {
            etapasAux.forEach(function (Etapa) {
                if (typeof (Etapa.FECHA_INICIO) == 'string') {
                    Etapa.FECHA_INICIO = new Date(ConvertirAFecha(Etapa.FECHA_INICIO));
                }
                if (typeof (Etapa.FECHA_FIN) == 'string') {
                    Etapa.FECHA_FIN = new Date(ConvertirAFecha(Etapa.FECHA_FIN));
                }
                EliminarObjeto(etapas, Etapa.ID_ETAPA, 'ID_ETAPA');
                etapas.push(Etapa);
            });
        }
    }

    function CalcularFecha(fechaAnterior, dias) {
        var Fecha = new Date(fechaAnterior);
        Fecha.setDate(Fecha.getDate() + dias);       
        return Fecha;
    }    

    function CalculoDias(fechaAnterior, fechaActualizada) {
        var Dias = 0;
        var fAnterior = CalcularFecha(fechaAnterior, 0).getDate();
        var fActualizada = CalcularFecha(fechaActualizada, 0).getDate();
        Dias = fActualizada - fAnterior;
        return Dias;
    }

    function GetConfiguracionObservaciones() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConfiguracionObservaciones",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConfiguracionObservacionesResult;
                configuracionesGlobales(result.Configuraciones);
                categorias(result.Categorias);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDCategoria(Categoria) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDCategoria",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Categoria: Categoria
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.CRUDCategoriaResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                }
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CRUDConvocatorias(Convocatoria) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/CRUDConvocatorias",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Convocatoria: Convocatoria
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.CRUDConvocatoriasResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    SGEWeb.app.NeedRefresh = true;
                    DevExpress.ui.notify(result.Message, "success", 2000);
                    SGEWeb.app.back();
                    Refrescar();
                }
                GuardarDisabled(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function CrearEtapa() {
        var auxEtapas = ko.toJS(etapas);
        var ultimaFechaEtapas = new Date();
        FechaMin(ultimaFechaEtapas);
        if (auxEtapas.length != 0) {
            ultimaFechaEtapas = auxEtapas[auxEtapas.length - 1].FECHA_FIN;
            FechaMin(CalcularFecha(ultimaFechaEtapas, 1));
        }        
        PopUpCrearEtapa.Title("Crear Etapa");
        PopUpCrearEtapa.Etapa(ko.mapping.fromJS(ko.toJS(
            {
                ID_ETAPA: -1,
                ID_CONVOCATORIA: Configuracion.ID,
                NOMBRE_ETAPA: '',
                FECHA_INICIO: FechaMin,
                FECHA_FIN: '',
                DESCRIPCION: '',
            })));
        PopUpCrearEtapa.Show(true);
    }

    function CrearCategoria() {

        PopUpCrearCategoria.Title("Crear Categoría");
        PopUpCrearCategoria.Categoria(ko.mapping.fromJS(ko.toJS(
            {
                CRUD: -1,
                ID_CATEGORIA: -1,
                ID_ETAPA: 0,
                NOMBRE_CATEGORIA: '',
            })));
        PopUpCrearCategoria.Show(true);
    }

    function InsertarEtapa() {

        if (IsNullOrEmpty(PopUpCrearEtapa.Etapa().NOMBRE_ETAPA())) {
            DevExpress.ui.notify('El nombre de la etapa es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(PopUpCrearEtapa.Etapa().FECHA_INICIO())) {
            DevExpress.ui.notify('La Fecha de inicio de la etapa es requerida.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(PopUpCrearEtapa.Etapa().FECHA_FIN())) {
            DevExpress.ui.notify('La Fecha de fin de la etapa es requerida.', "warning", 3000);
            return;
        }
        var etapaAux = BuscarObjeto(etapas, PopUpCrearEtapa.Etapa().NOMBRE_ETAPA().toUpperCase(), 'NOMBRE_ETAPA');     
        if (etapaAux != null) {
            DevExpress.ui.notify('Este nombre de etapa ya existe.', "warning", 3000);
            return;
        }
        PopUpCrearEtapa.Show(false);
        var Etapa = ko.toJS(PopUpCrearEtapa.Etapa());
        var idTemp = (ko.toJS(etapas).length + 1) * -1;
        Etapa.ID_ETAPA = idTemp;
        Etapa.CRUD = 1;
        etapas.push(Etapa);
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function InsertarCategoria() {
        if (IsNullOrEmpty(PopUpCrearCategoria.Categoria().NOMBRE_CATEGORIA())) {
            DevExpress.ui.notify('El nombre de la categoría es requerido.', "warning", 3000);
            return;
        }
        var categoriaAux = BuscarObjeto(categorias, PopUpCrearCategoria.Categoria().NOMBRE_CATEGORIA().toUpperCase(), 'NOMBRE_CATEGORIA');       
        if (categoriaAux != null) {
        DevExpress.ui.notify('Este nombre de categoría ya existe.', "warning", 3000);
        return;
        }

        PopUpCrearCategoria.Show(false);
        PopUpCrearCategoria.Categoria().CRUD = 1;
        var Categoria = ko.toJS(PopUpCrearCategoria.Categoria());
        CRUDCategoria(Categoria);
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function EditarEtapas(Etapa) {
        var fechaEtapaAnterior = new Date();
        FechaMin(fechaEtapaAnterior);  
        var auxEtapas = ko.toJS(etapas);
        if (auxEtapas.length > 1) {
            auxEtapas.forEach(function (auxEtapa, index) {
                if (auxEtapa['ID_ETAPA'] == Etapa.ID_ETAPA && index > 0) {
                    fechaEtapaAnterior = auxEtapas[index - 1].FECHA_FIN;
                    FechaMin(CalcularFecha(fechaEtapaAnterior, 1));    
                }
            });                       
        } 
        FechaInicioAnterior(Etapa.FECHA_INICIO);
        FechaFinAnterior(Etapa.FECHA_FIN);
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function EditarCategorias() {
        PopUpCRUDCategorias.Title("Editar Categorías");
        PopUpCRUDCategorias.Show(true);
    }

    function ActualizarFechasPosteriores(nDias, Id) {
        var etapasAux = ko.toJS(etapas);        
        var i = null;
        etapasAux.forEach(function (Etapa, index) {
            if (Etapa.ID_ETAPA == Id) {
                i = index;
            }
            if (index > i && i != null) {
                Etapa.FECHA_INICIO = CalcularFecha(Etapa.FECHA_INICIO, nDias);
                Etapa.FECHA_FIN = CalcularFecha(Etapa.FECHA_FIN, nDias);
            }
            EliminarObjeto(etapas, Etapa.ID_ETAPA, 'ID_ETAPA');
            etapas.push(Etapa);
        });
    }

    function EditarEtapa(Etapa) {

        var etapaAux = BuscarObjeto(etapas, Etapa.ID_ETAPA, 'ID_ETAPA');
        if (etapaAux != null) {
            etapaAux = Etapa;
        }

        var Dias = CalculoDias(FechaFinAnterior(), Etapa.FECHA_FIN);
        if (Dias != 0) {
            ActualizarFechasPosteriores(Dias, Etapa.ID_ETAPA);
        }
        ordenarEtapas();
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function EditarCategoria(Categoria) {
        if (IsNullOrEmpty(Categoria.NOMBRE_CATEGORIA)) {
            DevExpress.ui.notify('El nombre de la categoría es requerido.', "warning", 3000);
            return;
        }
        Categoria.CRUD = 3;
        CRUDCategoria(Categoria);
        GuardarDisabled(true);
        loadingVisible(true);        
    }

    function EliminarEtapa(Etapa) {

        EliminarObjeto(etapas, Etapa.ID_ETAPA, 'ID_ETAPA');
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function EliminarCategoria(Categoria) {       
        Refrescar();
        var otrasConvocatorias = categoriasOtrasConvocatorias();
        var categoria = BuscarObjeto(otrasConvocatorias, Categoria.ID_CATEGORIA, 'ID_CATEGORIA');
        if (categoria == null) {
            Categoria.CRUD = 4;
            EliminarObjeto(Configuracion.Categorias, Categoria.ID_CATEGORIA, 'ID_CATEGORIA');
            EliminarObjeto(IdsCategoriasSeleccionadas, Categoria.ID_CATEGORIA, null);
            CRUDCategoria(Categoria);
        } else {
            DevExpress.ui.notify('Esta categoría no se puede eliminar, se usa en otra convocatoria.', "warning", 3000);
            return;
        }        
        GuardarDisabled(true);
        loadingVisible(true);
    }

    function categoriasOtrasConvocatorias() {
        var OtrasConvocatorias = [];
        var Aux = ko.toJS(configuracionesGlobales);       
        Aux.forEach(function (Config) {
            var id = ko.toJS(Configuracion.ID);
            if (Config.ID != id) {
                ko.toJS(Config.Categorias).forEach(function (categoria) {
                    
                    var AuxCategoria =
                        {
                            'ID': Config.ID,
                            'ID_CATEGORIA': categoria.ID_CATEGORIA,
                        };
                    OtrasConvocatorias.push(AuxCategoria);
                });

            }
        });
        return OtrasConvocatorias;
    }

    function GuardarConvocatoria() {

        if (IsNullOrEmpty(Configuracion.CODIGO_CONVOCATORIA())) {
            DevExpress.ui.notify('El código de la convocatoria es requerido.', "warning", 3000);
            return;
        }
        if (IsNullOrEmpty(Configuracion.ANIO_CONVOCATORIA())) {
            DevExpress.ui.notify('El año de la convocatoria es requerido.', "warning", 3000);
            return;
        }

        var Convocatoria = ko.toJS(Configuracion);
        Convocatoria.CRUD = 3;
        Convocatoria.Etapas = ko.toJS(etapas);
        CRUDConvocatorias(Convocatoria);
    }

    function Refrescar() {
        loadingVisible(true);
        loadingMessage('Cargando...');
        GetConfiguracionObservaciones();
        convertirFechasEtapas();
    }

    Refrescar();

    var viewModel = {
        FechaMin: FechaMin,
        GuardarDisabled: GuardarDisabled,
        Configuracion: Configuracion,
        IdsCategoriasSeleccionadas: IdsCategoriasSeleccionadas,
        categorias: categorias,
        etapas: etapas,
        GetConfiguracionObservaciones: GetConfiguracionObservaciones,
        GuardarConvocatoria: GuardarConvocatoria,
        CrearEtapa: CrearEtapa,
        CrearCategoria: CrearCategoria,
        InsertarEtapa: InsertarEtapa,
        InsertarCategoria: InsertarCategoria,
        EditarEtapas: EditarEtapas,
        EditarCategorias: EditarCategorias,
        EditarEtapa: EditarEtapa,
        EditarCategoria: EditarCategoria,
        EliminarEtapa: EliminarEtapa,
        EliminarCategoria: EliminarCategoria,
        PopUpCrearEtapa: PopUpCrearEtapa,
        PopUpCRUDCategorias: PopUpCRUDCategorias,
        PopUpCrearCategoria: PopUpCrearCategoria,
        FechaFinAnterior: FechaFinAnterior,
    };

    return viewModel;
};