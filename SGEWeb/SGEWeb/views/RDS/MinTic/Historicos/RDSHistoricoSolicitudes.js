﻿SGEWeb.RDSHistoricoSolicitudes = function (params) {
    "use strict";

    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var title = ko.observable(JSON.parse(params.settings.substring(5)).title);
    var MyDataSource = ko.observableArray([]);
    var MyDataSourceDocs = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var Cantidades = ko.observable(0);
    var CantidadesDocs = ko.observable(0);
    var ShowPopUpDocs = ko.observable(false);
    var GuardarDisabled = ko.observable(false);
    var isLoading = ko.observable(false);

    var RangeArr = [{ ID: 2, text: 'Semana actual' },
                    { ID: 3, text: 'Semana anterior' },
                    { ID: 4, text: 'Este mes' },
                    { ID: 5, text: 'Mes pasado' },
                    { ID: 6, text: 'Trimestre actual' },
                    { ID: 7, text: 'Trimestre pasado' },
                    { ID: 8, text: 'Semestre actual' },
                    { ID: 9, text: 'Semestre pasado' },
                    { ID: 10, text: 'Año actual', selected: true },
                    { ID: 11, text: 'Año pasado' },
                    { ID: 12, text: 'Rango de fechas' }
    ];
    var ContextMenuRangoVisible = ko.observable(false);
    var ShowFechas = ko.observable(false);
    var NameRF = 'Año actual';
    var Date1 = Date.today().add({ months: -1 }).moveToFirstDayOfMonth().toString('yyyyMMdd');
    var Date2 = Date.today().add({ months: -1 }).moveToLastDayOfMonth().toString('yyyyMMdd');
    var selectedRangeID = 10;
    var selectedDateINI = ko.observable(undefined);
    var selectedDateEND = ko.observable(undefined);

    function GetSolicitudesMinTIC(DateIni, DateEnd) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesMinTIC",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                USR_GROUP: GROUP,
                USR_ROLE: ROLE,
                SOL_ENDED: 2,
                DateIni: DateIni,
                DateEnd: DateEnd
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetSolicitudesMinTICResult;
                MyDataSource(result);
                title('Histórico de solicitudes ' + NameRF);
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function onContentReady(e) {
        if (e.element[0].id == 'GridRDSHistoricoSolicitudes') {
            if (!isLoading())
                loadingVisible(false);
            Cantidades(Globalizeformat(e.component.totalCount(), "n0"));
            SGEWeb.app.DisabledToolBar(false);
        } else {
            CantidadesDocs(Globalizeformat(e.component.totalCount(), "n0"));
            SGEWeb.app.DisabledToolBar(false);
        }
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Análisis':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', 'Análisis')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                .prop('title', 'Documentos')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Logs':
                $('<i/>').addClass('ta ta-log1 ta-lg')
                .prop('title', 'Log de eventos')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
            case 'Info':
                $('<i/>').addClass('ta ta-info ta-lg')
                .prop('title', 'Información')
                .css('cursor', 'default')
                .css('margin-top', '3px')
                .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Análisis':
                $('<i/>').addClass('ta ta-editar ta-lg')
                .prop('title', GROUP == enumGROUPS.MinTIC ? 'Análisis administrativo y financiero' : 'Análisis técnico')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                .prop('title', 'Ver documentos')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Ver Documento':
                $('<i/>').addClass('ta ta-eye ta-lg')
                .prop('title', 'Ver documento')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Logs':
                $('<i/>').addClass('ta ta-log1 ta-lg')
                .prop('title', 'Log de eventos')
                .css('cursor', 'pointer')
                .appendTo(container);
                break;
            case 'Info':
                $("<div id=" + e.data.SOL_UID + "></div>").appendTo(container);

                $("<i id=" + e.data.SOL_UID + "s" + "></i>").addClass('ta ta-info ta-lg')
                .css('cursor', 'pointer')
                .appendTo(container);

                var Cadena = '<table style="margin-top:5px;" border=1>';
                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-add ta-lg"/><B style="margin-left:5px;margin-right:5px;">CREADO POR</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>' + (e.data.SOL_CREATOR == 0 ? 'Concesionario' : 'Funcionario') + '</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">&nbsp;</td></tr>';

                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-antena2 ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + e.data.Expediente.Operador.CUS_NAME.toUpperCase() + '</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Nit</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.Operador.CUS_IDENT + '</td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Email</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.Operador.CUS_EMAIL + '</td></tr>';

                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-antena1 ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + e.data.Expediente.SERV_NAME.toUpperCase() + '</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Expediente</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.SERV_NUMBER + '</td></tr>';


                e.data.Expediente.Contactos.forEach(function (entry) {
                    Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-user ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + entry.CONT_TITLE.toUpperCase() + '</B></td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Nombre</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_NAME + '</td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Cédula</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_NUMBER + '</td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Teléfono</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_TEL + '</td></tr>';
                });
                Cadena += '</table>'

                $("#" + e.data.SOL_UID).dxTooltip({
                    target: "#" + e.data.SOL_UID + "s",
                    showEvent: "dxhoverstart",
                    hideEvent: "dxhoverend",
                    position: "right",
                    contentTemplate: function (contentElement) {
                        contentElement.html(Cadena);
                    }
                });
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSHistoricoSolicitudes') {
                switch (e.column.name) {
                    case 'Análisis':
                        SGEWeb.app.Solicitud = JSON.stringify(e.data);
                        switch (GROUP) {
                            case enumGROUPS.MinTIC:
                                if (e.data.SOL_IS_OTORGA) {
                                    SGEWeb.app.navigate({ view: "RDSOtorgaAnalisisAdministrativo", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                }
                                else {
                                SGEWeb.app.navigate({ view: "RDSAnalisisAdministrativo", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                }
                                break;
                            case enumGROUPS.ANE:
                                SGEWeb.app.navigate({ view: "RDSAnalisisTecnico", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                break;
                        }
                        break;
                    case 'Logs':
                        SGEWeb.app.navigate({ view: "RDSLogs", id: -1, settings: { title: 'Log de eventos ' + e.data.Expediente.SERV_NAME + ' (' + e.data.SOL_TYPE_NAME + ')', SOL_UID: e.data.SOL_UID } });
                        break;

                    case 'Documents':
                        MyDataSourceDocs(e.data.Documents);
                        ShowPopUpDocs(true);
                        break;
                }
            } else if (e.element[0].id == 'GridDocuments') {
                switch (e.column.name) {
                    case 'Ver Documento':
                        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        break;
                }

            }
        }
    }

    function Excel() {
        var grid = $("#GridRDSHistoricoSolicitudes").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Cargando...');
        loadingVisible(true);
        GetSolicitudesMinTIC(Date1, Date2);
        isLoading(true);
    }

    function popupShowing(e) {
        $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function popupHidden(e) {

        MyDataSourceDocs([]);

        var grid = $("#GridDocuments").dxDataGrid('instance');

        if (grid) {
            grid.clearFilter();
            grid.clearSorting();
            grid.pageIndex(0);
        }
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
    }

    function OnColumnChooser() {
        var grid = $("#GridRDSHistoricoSolicitudes").dxDataGrid('instance');
        if (grid != undefined)
            grid.showColumnChooser();

    }

    function OnCustomLoad() {
        var state = localStorage.getItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb);
        if (state) {
            state = JSON.parse(state);
        }
        return state;
    }

    function OnCustomSave(state) {
        for (var i = 0; i < state.columns.length; i++) {
            state.columns[i].filterValue = null;
            state.columns[i].filterValues = null;
            state.columns[i].sortIndex = null;
            state.columns[i].sortOrder = null;
        }
        localStorage.setItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb, JSON.stringify(state));
                            
    }

    function ContextMenuRangoClick(e) {

        selectedRangeID = e.itemData.ID;

        switch (e.itemData.ID) {
            case 2: //Esta Semana
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: +1 }));
                selectedDateEND(Date.today());
                break;
            case 3: //Semana Pasada
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: -6 }));
                selectedDateEND(Date.today().moveToDayOfWeek(0, -1));
                break;
            case 4: //Este mes
                selectedDateINI(Date.today().moveToFirstDayOfMonth());
                selectedDateEND(Date.today());
                break;
            case 5: //Mes pasado
                selectedDateINI(Date.today().add({ months: -1 }).moveToFirstDayOfMonth());
                selectedDateEND(Date.today().add({ months: -1 }).moveToLastDayOfMonth());
                break;
            case 6: //Este trimestre
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                selectedDateINI(new Date((new Date).getFullYear(), 3 * quarter - 3, 1));
                selectedDateEND(Date.today());
                break;
            case 7: //Trimestre pasado
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                var year = quarter == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                quarter = quarter == 1 ? 4 : quarter - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 3 * quarter - 3, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 3 * quarter - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 8: //Este Semestre
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                selectedDateINI(new Date((new Date).getFullYear(), 6 * Semestre - 6, 1));
                selectedDateEND(Date.today());
                break;
            case 9: //Semestre pasado
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                var year = Semestre == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                Semestre = Semestre == 1 ? 2 : Semestre - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 6 * Semestre - 6, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 6 * Semestre - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 10: //Este Año
                selectedDateINI(new Date((new Date).getFullYear(), 0, 1));
                selectedDateEND(new Date());
                break;
            case 11: //Año pasado
                selectedDateINI(new Date((new Date).getFullYear() - 1, 0, 1));
                selectedDateEND(new Date((new Date).getFullYear() - 1, 11, 31));
                break;
            case 12: //Rango de fechas
                ShowFechas(true);
                return;
                break;
        }

        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = e.itemData.text;

        Refrescar();
    }

    function CustomDateClick() {
        if (selectedDateINI() > selectedDateEND()) {
            DevExpress.ui.notify('La fecha inicial debe ser menor o igual que la fecha final', "warning", 3000);
            return;
        }

        ShowFechas(false);
        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = selectedDateINI().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateINI().getMonth()]) + '-' + selectedDateEND().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateEND().getMonth()]);
        Refrescar();
    }

    ContextMenuRangoClick({ itemData: RangeArr[ListObjectFindIndex(RangeArr, 'ID', selectedRangeID)] });

    var viewModel = {
        viewShown: handleViewShown,
        title: title,
        MyDataSource: MyDataSource,
        MyDataSourceDocs: MyDataSourceDocs,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onContentReady: onContentReady,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        Excel: Excel,
        Refrescar: Refrescar,
        Cantidades: Cantidades,
        CantidadesDocs: CantidadesDocs,
        ShowPopUpDocs: ShowPopUpDocs,
        popupShowing: popupShowing,
        popupHidden: popupHidden,
        ROLE: ROLE,
        GuardarDisabled: GuardarDisabled,
        onHeaderTemplate: onHeaderTemplate,
        GROUP: GROUP,
        OnColumnChooser: OnColumnChooser,
        OnCustomLoad: OnCustomLoad,
        OnCustomSave: OnCustomSave,

        ContextMenuRangoVisible: ContextMenuRangoVisible,
        ContextMenuRangoClick:ContextMenuRangoClick,
        RangeArr: RangeArr,
        selectedDateINI: selectedDateINI,
        selectedDateEND: selectedDateEND,
        ShowFechas: ShowFechas,
        CustomDateClick: CustomDateClick,
        isLoading: isLoading

    };

    return viewModel;
};