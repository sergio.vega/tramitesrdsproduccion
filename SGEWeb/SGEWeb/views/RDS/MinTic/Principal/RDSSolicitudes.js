﻿SGEWeb.RDSSolicitudes = function (params) {
    "use strict";
    var title = JSON.parse(params.settings.substring(5)).title;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;

    var MyDataSource = ko.observableArray([]);
    var ConfiguracionesProcesosOtorga = ko.observableArray([]);
    var MyDataSourceDocs = ko.observableArray([]);
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var Cantidades = ko.observable(0);
    var CantidadesDocs = ko.observable(0);
    var IsFilter = ko.observable(false);
    var ShowPopUpDocs = ko.observable(false);
    var MyPopUpReasign = { Show: ko.observable(false), Title: ko.observable(''), Text: ko.observable(''), UserList: ko.observableArray([]), UserSelected: ko.observable(null), ColumnROLE: ko.observable(0), Solicitud: undefined, TextVisible: ko.observable(true), SubType: 0, ColumnROLE_TEXT: ko.observable('') };
    var GuardarDisabled = ko.observable(false);
    var Alarmas = { Administrativo: ko.observable(' '), Tecnico: ko.observable(' '), Resolucion: ko.observable(' '), EnCurso: ko.observable(undefined), Porvencer: ko.observable(undefined), Vencidas: ko.observable(undefined), EnCierre: ko.observable(undefined), Total: ko.observable(0), Subsanadas: ko.observable(undefined), Requeridas: ko.observable(undefined) };
    var isLoading = ko.observable(false);
    var ShowPopUpRecordatorio = ko.observable(false);
    var TiposSolicitudesPosibles = ko.observableArray([]);
    var Recordatorios = ko.observableArray([]);
    var CoordinadorIsFirstTime = true;
    var NumNotificaciones = 0;
    var TipoBoton = ko.observable('normal');


    function GetSolicitudesMinTIC() {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetSolicitudesMinTIC",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                USR_GROUP: GROUP,
                USR_ROLE: ROLE,
                SOL_ENDED: 0,
                DateIni: '',
                DateEnd: ''
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetSolicitudesMinTICResult;
                MyDataSource(result.Solicitudes);
                TiposSolicitudesPosibles(result.SOL_TYPES);
                if (result.Alarmas.length != 0) {
                    if (result.Alarmas[0].Radicado != 0) {
                        Recordatorios(result.Alarmas);
                        TipoBoton('danger');
                    }
                }
                SGEWeb.app.DisabledToolBar(false);
                isLoading(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function ReasignarSolicitud(Reasign) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/ReasignarSolicitud",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Solicitud: MyPopUpReasign.Solicitud,
                Reasign: Reasign
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);

                var result = msg.ReasignarSolicitudResult;
                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {

                    DevExpress.ui.notify(result.Message, "success", 2000);
                    Refrescar();
                }
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }


    function onContentReady(e) {
        if (e.element[0].id == 'GridRDSSolicitudes') {
            if (!isLoading())
                loadingVisible(false);
            Cantidades(Globalizeformat(e.component.totalCount(), "n0"));

            var GridSource = e.component.getDataSource();

            Alarmas.Administrativo(ListObjectCountTwoLevels(GridSource.items(), 'CURRENT', 'STEP', enumSTEPS.Administrativo));
            Alarmas.Tecnico(ListObjectCountTwoLevels(GridSource.items(), 'CURRENT', 'STEP', enumSTEPS.Tecnico));
            Alarmas.Resolucion(ListObjectCountTwoLevels(GridSource.items(), 'CURRENT', 'STEP', enumSTEPS.Resolucion));


            Alarmas.EnCurso(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM.EnCurso));
            Alarmas.Porvencer(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM.Porvencer));
            Alarmas.Vencidas(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM.Vencidas));
            Alarmas.EnCierre(ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM.EnCierre));
            Alarmas.Subsanadas(ListObjectCountOneLevel(GridSource.items(), 'SOL_STATE_ID', enumSOL_STATES.Subsanado));
            Alarmas.Requeridas(ListObjectCountOneLevel(GridSource.items(), 'SOL_STATE_ID', enumSOL_STATES.Requerido));
            Alarmas.Total(e.component.totalCount() - ListObjectCountOneLevel(GridSource.items(), 'SOL_ALARM_STATE', enumALARM.Terminadas));

            if (ROLE == enumROLES.Coordinador && GROUP == enumGROUPS.MinTIC) {
                var FiltroEnResolucion = GridSource.items().filter(function (Item) {
                    return (Item.CURRENT.STEP == enumSTEPS.Resolucion);
                });

                var RevisoresSinAsignar = ListObjectCountThreeLevels(FiltroEnResolucion, 'USR_ADMIN', 'USR_ROLE2', 'USR_ID', -1);

                if (RevisoresSinAsignar > 0) {
                    if (CoordinadorIsFirstTime) {
                        var Message = "";
                        if (RevisoresSinAsignar == 1)
                            Message = CrearDialogHtml('ta ta-info', 'Hay 1 solicitud en resolución sin revisor asignado.');
                        else
                            Message = CrearDialogHtml('ta ta-info', 'Hay ' + RevisoresSinAsignar + ' solicitudes en resolución sin revisor asignado.');

                        DevExpress.ui.dialog.alert(Message, SGEWeb.app.Name);
                        CoordinadorIsFirstTime = false;
                    }
                }
            }

            SGEWeb.app.DisabledToolBar(false);


        } else {
            CantidadesDocs(Globalizeformat(e.component.totalCount(), "n0"));
            SGEWeb.app.DisabledToolBar(false);
        }
    }

    function onHeaderTemplate(container, e) {
        switch (e.column.name) {
            case 'Análisis':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', 'Análisis')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                    .prop('title', 'Documentos')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Logs':
                $('<i/>').addClass('ta ta-log1 ta-lg')
                    .prop('title', 'Log de eventos')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Info':
                $('<i/>').addClass('ta ta-info ta-lg')
                    .prop('title', 'Información')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'Alarma':
                $('<i/>').addClass('ta ta-bell ta-lg')
                    .prop('title', 'Alarmas')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole1':
                $('<i/>').addClass('ta ta-funcionario ta-lg')
                    .prop('title', 'Solicitudes de reasignación funcionario')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole2':
                $('<i/>').addClass('ta ta-revisor ta-lg')
                    .prop('title', 'Solicitudes de reasignación revisor')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole4':
                $('<i/>').addClass('ta ta-coordinador ta-lg')
                    .prop('title', 'Solicitudes de reasignación coordinador')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole8':
                $('<i/>').addClass('ta ta-subdirector ta-lg')
                    .prop('title', 'Reasignación subdirector')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole16':
                $('<i/>').addClass('ta ta-asesor ta-lg')
                    .prop('title', 'Reasignación asesor')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
            case 'ReasignRole32':
                $('<i/>').addClass('ta ta-director ta-lg')
                    .prop('title', 'Reasignación director')
                    .css('cursor', 'default')
                    .css('margin-top', '3px')
                    .appendTo(container);
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.column.name) {
            case 'Análisis':
                $('<i/>').addClass('ta ta-editar ta-lg')
                    .prop('title', GROUP == enumGROUPS.MinTIC ? 'Análisis' : 'Análisis técnico')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Documents':
                $('<i/>').addClass('ta ta-attachment ta-lg')
                    .prop('title', 'Ver documentos')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Ver Documento':
                $('<i/>').addClass('ta ta-eye ta-lg')
                    .prop('title', 'Ver documento')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Logs':
                $('<i/>').addClass('ta ta-log1 ta-lg')
                    .prop('title', 'Log de eventos')
                    .css('cursor', 'pointer')
                    .appendTo(container);
                break;
            case 'Info':
                $("<div id=" + e.data.SOL_UID + "></div>").appendTo(container);

                $("<i id=" + e.data.SOL_UID + "s" + "></i>").addClass('ta ta-info ta-lg')
                    .css('cursor', 'default')
                    .appendTo(container);

                var Cadena = '<table style="margin-top:5px;" border=1>';
                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-add ta-lg"/><B style="margin-left:5px;margin-right:5px;">CREADO POR</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>' + (e.data.SOL_CREATOR == 0 ? 'Concesionario' : 'Funcionario') + '</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">&nbsp;</td></tr>';

                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-antena2 ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + e.data.Expediente.Operador.CUS_NAME.toUpperCase() + '</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Nit</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.Operador.CUS_IDENT + '</td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Email</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.Operador.CUS_EMAIL + '</td></tr>';

                Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-antena1 ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + e.data.Expediente.SERV_NAME.toUpperCase() + '</B></td></tr>';
                Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Expediente</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + e.data.Expediente.SERV_NUMBER + '</td></tr>';

                e.data.Expediente.Contactos.forEach(function (entry) {
                    Cadena += '<tr><td colspan="2" align=left style="height:24px;background:#eeeeee;" ><i style="margin-left:3px" class="ta ta-user ta-lg"/><B style="margin-left:5px;margin-right:5px;">' + entry.CONT_TITLE.toUpperCase() + '</B></td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Nombre</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_NAME + '</td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Cédula</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_NUMBER + '</td></tr>';
                    Cadena += '<tr><td align=left style="padding-top:3px;padding-bottom:3px;padding-left:10px;"><b>Teléfono</b></td><td align=left style="padding-top:3px;padding-bottom:3px;">' + entry.CONT_TEL + '</td></tr>';
                });
                Cadena += '</table>'

                $("#" + e.data.SOL_UID).dxTooltip({
                    target: "#" + e.data.SOL_UID + "s",
                    showEvent: "dxhoverstart",
                    hideEvent: "dxhoverend",
                    position: "right",
                    contentTemplate: function (contentElement) {
                        contentElement.html(Cadena);
                    }
                });
                break;
            case 'Alarma':
                $('<i/>').addClass(SGEWeb.app.ALARM[e.data.SOL_ALARM_STATE].Icon + ' ta-lg')
                    .prop('title', SGEWeb.app.ALARM[e.data.SOL_ALARM_STATE].textsingle)
                    .css('cursor', 'default')
                    .append('<span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span>')
                    .appendTo(container);
                container.css('text-align', 'center')
                break;
            case 'ReasignRole1':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-funcionario ta-lg')
                        .prop('title', 'Solicitudes de reasignación funcionario')
                        .css('cursor', 'pointer')
                        .css('color', e.data.Reasign.USR_ROLE1_REASIGN ? 'black' : 'lightgrey')
                        .appendTo(container);
                }
                break;
            case 'ReasignRole2':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-revisor ta-lg')
                        .prop('title', 'Solicitudes de reasignación revisor')
                        .css('cursor', 'pointer')
                        .css('color', e.data.Reasign.USR_ROLE2_REASIGN ? 'black' : 'lightgrey')
                        .appendTo(container);
                }
                break;
            case 'ReasignRole4':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-coordinador ta-lg')
                        .prop('title', 'Solicitudes de reasignación coordinador')
                        .css('cursor', 'pointer')
                        .css('color', e.data.Reasign.USR_ROLE4_REASIGN ? 'black' : 'lightgrey')
                        .appendTo(container);
                }
                break;
            case 'ReasignRole8':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-subdirector ta-lg')
                        .prop('title', 'Reasignación subdirector')
                        .css('cursor', 'pointer')
                        .css('color', 'lightgrey')
                        .appendTo(container);
                }
                break;
            case 'ReasignRole16':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-asesor ta-lg')
                        .prop('title', 'Reasignación asesor')
                        .css('cursor', 'pointer')
                        .css('color', e.data.Reasign.USR_ROLE16_REASIGN ? 'black' : 'lightgrey')
                        .appendTo(container);
                }
                break;
            case 'ReasignRole32':
                if (e.data.SOL_ENDED == 0) {
                    $('<i/>').addClass('ta ta-director ta-lg')
                        .prop('title', 'Reasignación director')
                        .css('cursor', 'pointer')
                        .css('color', 'lightgrey')
                        .appendTo(container);
                }
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            if (e.element[0].id == 'GridRDSSolicitudes') {
                switch (e.column.name) {
                    case 'Análisis':
                        SGEWeb.app.Solicitud = JSON.stringify(e.data);

                        var tipoDeSolicitud = TiposSolicitudesPosibles().filter(function (obj) { return obj.SOL_TYPE_ID === e.data.SOL_TYPE_ID })[0];

                        if (tipoDeSolicitud.disabled || tipoDeSolicitud.SOL_TYPE_ID == 17 || tipoDeSolicitud.SOL_TYPE_ID == 2 || tipoDeSolicitud.SOL_TYPE_ID == 7) {
                            switch (GROUP) {
                                case enumGROUPS.MinTIC:
                                    if (e.data.SOL_IS_OTORGA) {
                                        SGEWeb.app.navigate({ view: "RDSOtorgaAnalisisAdministrativo", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                    }
                                    else {
                                        SGEWeb.app.navigate({ view: "RDSAnalisisAdministrativo", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                    }
                                    break;
                                case enumGROUPS.ANE:
                                    if (e.data.SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComercial || e.data.SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitaria ||
                                        e.data.SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos || e.data.SOL_TYPE_ID == enumSOL_TYPES.OtorgaEmisoraDeInteresPublico) {
                                        SGEWeb.app.navigate({ view: "RDSOtorgaAnalisisTecnico", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                    }
                                    else {
                                        SGEWeb.app.navigate({ view: "RDSAnalisisTecnico", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                                    }
                                    break;
                            }
                        }
                        else {
                            DevExpress.ui.notify('La solicitud estará disponible una vez finalice la recepción de solicitudes del proceso.', "warning", 3000);

                        }

                        break;

                    case 'Logs':
                        SGEWeb.app.navigate({ view: "RDSLogs", id: -1, settings: { title: 'Log de eventos ' + e.data.Expediente.SERV_NAME + ' (' + e.data.SOL_TYPE_NAME + ')', SOL_UID: e.data.SOL_UID } });
                        break;

                    case 'Documents':
                        MyDataSourceDocs(e.data.Documents);
                        ShowPopUpDocs(true);
                        break;

                    case 'ReasignRole1':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        if (ROLE != enumROLES.Funcionario) {
                            MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                                if (GROUP == enumGROUPS.MinTIC)
                                    return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE1.USR_ID) && (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE2.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Funcionario);
                                if (GROUP == enumGROUPS.ANE)
                                    return (Item.USR_ID != e.data.USR_TECH.USR_ROLE1.USR_ID) && (Item.USR_ID != e.data.USR_TECH.USR_ROLE2.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Funcionario);

                            }));
                        }
                        MyPopUpReasign.ColumnROLE(enumROLES.Funcionario);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title(ROLE == enumROLES.Funcionario || e.data.Reasign.USR_ROLE1_REASIGN ? 'Petición de reasignación' : 'Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('funcionario');
                        MyPopUpReasign.TextVisible(ROLE == enumROLES.Funcionario || e.data.Reasign.USR_ROLE1_REASIGN ? true : false);
                        MyPopUpReasign.Text(e.data.Reasign.USR_ROLE1_REASIGN_COMMENT);
                        MyPopUpReasign.SubType = e.data.Reasign.USR_ROLE1_REASIGN ? enumREASIGN_TYPE.Aprobar : enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;

                    case 'ReasignRole2':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        if (ROLE != enumROLES.Revisor) {
                            MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                                if (GROUP == enumGROUPS.MinTIC)
                                    return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE1.USR_ID) && (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE2.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Revisor);
                                if (GROUP == enumGROUPS.ANE)
                                    return (Item.USR_ID != e.data.USR_TECH.USR_ROLE1.USR_ID) && (Item.USR_ID != e.data.USR_TECH.USR_ROLE2.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Revisor);
                            }));
                        }
                        MyPopUpReasign.ColumnROLE(enumROLES.Revisor);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title(ROLE == enumROLES.Revisor || e.data.Reasign.USR_ROLE2_REASIGN ? 'Petición de reasignación' : 'Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('revisor');
                        MyPopUpReasign.TextVisible(ROLE == enumROLES.Revisor || e.data.Reasign.USR_ROLE2_REASIGN ? true : false);
                        MyPopUpReasign.Text(e.data.Reasign.USR_ROLE2_REASIGN_COMMENT);
                        MyPopUpReasign.SubType = e.data.Reasign.USR_ROLE2_REASIGN ? enumREASIGN_TYPE.Aprobar : enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;

                    case 'ReasignRole4':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        if (ROLE != enumROLES.Coordinador) {
                            MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                                if (GROUP == enumGROUPS.MinTIC)
                                    return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE4.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Coordinador);
                                if (GROUP == enumGROUPS.ANE)
                                    return (Item.USR_ID != e.data.USR_TECH.USR_ROLE4.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Coordinador);

                            }));
                        }
                        MyPopUpReasign.ColumnROLE(enumROLES.Coordinador);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title(ROLE == enumROLES.Coordinador || e.data.Reasign.USR_ROLE4_REASIGN ? 'Petición de reasignación' : 'Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('coordinador');
                        MyPopUpReasign.TextVisible(ROLE == enumROLES.Coordinador || e.data.Reasign.USR_ROLE4_REASIGN ? true : false);
                        MyPopUpReasign.Text(e.data.Reasign.USR_ROLE4_REASIGN_COMMENT);
                        MyPopUpReasign.SubType = e.data.Reasign.USR_ROLE4_REASIGN ? enumREASIGN_TYPE.Aprobar : enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;

                    case 'ReasignRole8':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                            if (GROUP == enumGROUPS.MinTIC)
                                return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE8.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Subdirector);
                            if (GROUP == enumGROUPS.ANE)
                                return (Item.USR_ID != e.data.USR_TECH.USR_ROLE8.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Subdirector);
                        }));
                        MyPopUpReasign.ColumnROLE(enumROLES.Subdirector);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title('Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('subdirector');
                        MyPopUpReasign.TextVisible(false);
                        MyPopUpReasign.Text('');
                        MyPopUpReasign.SubType = enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;

                    case 'ReasignRole16':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                            return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE16.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Asesor);
                        }));
                        MyPopUpReasign.ColumnROLE(enumROLES.Asesor);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title('Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('asesor');
                        MyPopUpReasign.TextVisible(ROLE == enumROLES.Asesor || e.data.Reasign.USR_ROLE16_REASIGN ? true : false);
                        MyPopUpReasign.Text(e.data.Reasign.USR_ROLE16_REASIGN_COMMENT);
                        MyPopUpReasign.SubType = e.data.Reasign.USR_ROLE16_REASIGN ? enumREASIGN_TYPE.Aprobar : enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;

                    case 'ReasignRole32':
                        if (e.data.SOL_ENDED != 0) {
                            e.event.stopPropagation();
                            return;
                        }
                        MyPopUpReasign.UserList(SGEWeb.app.Users.filter(function (Item) {
                            return (Item.USR_ID != e.data.USR_ADMIN.USR_ROLE32.USR_ID) && (Item.USR_GROUP & GROUP) && (Item.USR_ROLE & Item.USR_ASSIGNABLE & enumROLES.Director);
                        }));
                        MyPopUpReasign.ColumnROLE(enumROLES.Director);
                        MyPopUpReasign.UserSelected(null);
                        MyPopUpReasign.Solicitud = e.data;
                        MyPopUpReasign.Title('Reasignación sin petición');
                        MyPopUpReasign.ColumnROLE_TEXT('director');
                        MyPopUpReasign.TextVisible(false);
                        MyPopUpReasign.Text('');
                        MyPopUpReasign.SubType = enumREASIGN_TYPE.Reasignar;
                        MyPopUpReasign.Show(true);
                        break;
                }
            } else if (e.element[0].id == 'GridDocuments') {
                switch (e.column.name) {
                    case 'Ver Documento':
                        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=DOC&FUID=' + e.data.DOC_UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                        break;
                }

            }
        }
    }

    function Excel() {
        var grid = $("#GridRDSSolicitudes").dxDataGrid('instance');
        if (grid != undefined)
            grid.exportToExcel(false);
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage('Cargando...');
        loadingVisible(true);
        GetSolicitudesMinTIC();
        isLoading(true);
    }

    function popupShowing(e) {
        $(e.component._container()[0].childNodes[1]).addClass('myPopupAnexosClass');
    }

    function popupHidden(e) {

        MyDataSourceDocs([]);

        var grid = $("#GridDocuments").dxDataGrid('instance');

        if (grid) {
            grid.clearFilter();
            grid.clearSorting();
            grid.pageIndex(0);
        }
    }

    function handleViewShown(e) {
        e.viewInfo.layoutController.slideOut.option('swipeEnabled', false);
        if (e.direction == 'backward') {
            if (SGEWeb.app.NeedRefresh) {
                SGEWeb.app.NeedRefresh = false;
                Refrescar();
            }
        }
    }

    function OnReasignar(e) {

        switch (e.model.options.name) {

            case 'Solicitar':
                if (IsNullOrEmpty(MyPopUpReasign.Text()) || MyPopUpReasign.Text().length < 10) {
                    DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                    return;
                }

                var Reasign = {
                    TYPE: enumREASIGN_TYPE.Solicitar,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    SOL_UID: MyPopUpReasign.Solicitud.SOL_UID,
                    USR_ID: SGEWeb.app.IDUserWeb,
                    COMMENT: MyPopUpReasign.Text(),
                    NEW_USR_ID: -1
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarSolicitud(Reasign);
                MyPopUpReasign.Show(false);
                break;

            case 'Rechazar':
                var Reasign = {
                    TYPE: enumREASIGN_TYPE.Rechazar,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    SOL_UID: MyPopUpReasign.Solicitud.SOL_UID,
                    USR_ID: GetReasignUserID(MyPopUpReasign.ColumnROLE()),
                    COMMENT: '',
                    NEW_USR_ID: -1
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarSolicitud(Reasign);
                MyPopUpReasign.Show(false);
                break;

            case 'Aceptar':
                if (MyPopUpReasign.UserSelected() == null) {
                    DevExpress.ui.notify('Debe seleccionar un usuario. Si la lista está vacia, es porque no hay usuarios disponibles para ser reasignados.', "warning", 5000);
                    return;
                }

                var Reasign = {
                    TYPE: MyPopUpReasign.SubType,
                    USR_GROUP: GROUP,
                    ROLE: ROLE,
                    ColumnROLE: MyPopUpReasign.ColumnROLE(),
                    SOL_UID: MyPopUpReasign.Solicitud.SOL_UID,
                    USR_ID: GetReasignUserID(MyPopUpReasign.ColumnROLE()),
                    COMMENT: '',
                    NEW_USR_ID: MyPopUpReasign.UserSelected()
                };
                SGEWeb.app.DisabledToolBar(true);
                GuardarDisabled(true);
                loadingMessage('Procesando...');
                loadingVisible(true);
                ReasignarSolicitud(Reasign);
                MyPopUpReasign.Show(false);
                break;

        }

    }

    function GetReasignUserID(ColumnROLE) {
        var USR_ID = -1;
        switch (ROLE) {
            case enumROLES.Coordinador:
                switch (ColumnROLE) {
                    case enumROLES.Funcionario:
                        if (GROUP == enumGROUPS.MinTIC)
                            USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE1.USR_ID;
                        if (GROUP == enumGROUPS.ANE)
                            USR_ID = MyPopUpReasign.Solicitud.USR_TECH.USR_ROLE1.USR_ID;
                        break;
                    case enumROLES.Revisor:
                        if (GROUP == enumGROUPS.MinTIC)
                            USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE2.USR_ID;
                        if (GROUP == enumGROUPS.ANE)
                            USR_ID = MyPopUpReasign.Solicitud.USR_TECH.USR_ROLE2.USR_ID;
                        break;
                }
                break;

            case enumROLES.Subdirector:
                if (GROUP == enumGROUPS.MinTIC)
                    USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE4.USR_ID;
                if (GROUP == enumGROUPS.ANE)
                    USR_ID = MyPopUpReasign.Solicitud.USR_TECH.USR_ROLE4.USR_ID;
                break;

            case enumROLES.Director:
                USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE16.USR_ID;
                break;

            case enumROLES.Administrador:
                switch (ColumnROLE) {
                    case enumROLES.Subdirector:
                        if (GROUP == enumGROUPS.MinTIC)
                            USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE8.USR_ID;
                        if (GROUP == enumGROUPS.ANE)
                            USR_ID = MyPopUpReasign.Solicitud.USR_TECH.USR_ROLE8.USR_ID;
                        break;
                    case enumROLES.Director:
                        USR_ID = MyPopUpReasign.Solicitud.USR_ADMIN.USR_ROLE32.USR_ID;
                        break;
                }
                break;
        }
        return USR_ID;
    }

    function Filtrar() {
        var grid = $("#GridRDSSolicitudes").dxDataGrid('instance');
        if (grid != undefined) {
            if (IsFilter()) {
                IsFilter(false);
                grid.clearFilter('dataSource');
            } else {
                IsFilter(true);
                grid.filter([["CURRENT.GROUP", "=", GROUP], "and", ["CURRENT.ROLE", ROLE]]);
            }
        }
    }

    function OnColumnChooser() {
        var grid = $("#GridRDSSolicitudes").dxDataGrid('instance');
        if (grid != undefined)
            grid.showColumnChooser();

    }

    function Notificaciones() {
        ShowPopUpRecordatorio(true);

    }


    function OnCustomLoad() {
        var state = localStorage.getItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb);
        if (state) {
            state = JSON.parse(state);
        }
        return state;
    }

    function OnCustomSave(state) {
        for (var i = 0; i < state.columns.length; i++) {
            state.columns[i].filterValue = null;
            state.columns[i].filterValues = null;
            state.columns[i].sortIndex = null;
            state.columns[i].sortOrder = null;
        }
        localStorage.setItem(this.storageKey + '_' + ROLE + '_' + SGEWeb.app.IDUserWeb, JSON.stringify(state));

    }


    Refrescar();

    var viewModel = {

        viewShown: handleViewShown,
        ConfiguracionesProcesosOtorga: ConfiguracionesProcesosOtorga,
        title: title,
        MyDataSource: MyDataSource,
        MyDataSourceDocs: MyDataSourceDocs,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        onContentReady: onContentReady,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        Excel: Excel,
        Refrescar: Refrescar,
        Cantidades: Cantidades,
        CantidadesDocs: CantidadesDocs,
        ShowPopUpDocs: ShowPopUpDocs,
        popupShowing: popupShowing,
        popupHidden: popupHidden,
        ROLE: ROLE,
        MyPopUpReasign: MyPopUpReasign,
        GuardarDisabled: GuardarDisabled,
        OnReasignar: OnReasignar,
        onHeaderTemplate: onHeaderTemplate,
        GROUP: GROUP,
        Filtrar: Filtrar,
        IsFilter: IsFilter,
        OnColumnChooser: OnColumnChooser,
        OnCustomLoad: OnCustomLoad,
        Notificaciones: Notificaciones,
        OnCustomSave: OnCustomSave,
        Alarmas: Alarmas,
        isLoading: isLoading,
        TiposSolicitudesPosibles: TiposSolicitudesPosibles,
        Recordatorios: Recordatorios,
        ShowPopUpRecordatorio: ShowPopUpRecordatorio,
        TipoBoton: TipoBoton,
    };

    return viewModel;
};