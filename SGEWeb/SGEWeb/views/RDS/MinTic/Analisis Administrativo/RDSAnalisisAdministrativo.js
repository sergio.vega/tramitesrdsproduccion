﻿SGEWeb.RDSAnalisisAdministrativo = function (params) {
    "use strict";
    SGEWeb.app.NeedRefresh = false;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var Solicitud = JSON.parse(SGEWeb.app.Solicitud);
    var SaltarOpened = ROLE == 1 ? false : true;
    var CURRENT = ko.mapping.fromJS(Solicitud.CURRENT);
    var MyPopUp = { Show: ko.observable(false), Title: ko.observable(''), Who: '', STATE_ID: ko.observable(), COMMENT: ko.observable(), UID: ko.observable(), NAME: ko.observable() };
    var loadingVisible = ko.observable(false);
    var loadingMessage = ko.observable("Cargando...");
    var GuardarDisabled = ko.observable(false);
    var MyDataSource = ko.observableArray([]);
    var ToggleVisibility = { Logo: ko.observable(true), Concesionario: ko.observable(true), Paso: ko.observable(true), Footer: ko.observable(true) }
    var Financiero = ko.mapping.fromJS(Solicitud.Financiero);
    Financiero.FIN_ESTADO_CUENTA_DATE(StringToDate(Financiero.FIN_ESTADO_CUENTA_DATE()));
    Financiero.FIN_REGISTRO_ALFA_DATE(StringToDate(Financiero.FIN_REGISTRO_ALFA_DATE()));
    var Administrativo = ko.mapping.fromJS(Solicitud.Anexos);

    var USR_ADMIN = ko.mapping.fromJS(Solicitud.USR_ADMIN);
    var USR_TECH = ko.mapping.fromJS(Solicitud.USR_TECH);

    var Tecnico = ko.observableArray([]);
    Tecnico().push(ko.mapping.fromJS(Solicitud.AnalisisTecnico));
    Tecnico()[0].TECH_NAME = ko.observable('Análisis técnico');

    var GridResolucionInstance = null;
    var Resolucion = ko.observableArray([]);
    Resolucion().push(ko.mapping.fromJS(Solicitud.AnalisisResolucion));
    Resolucion()[0].RES_NAME = ko.observable('Análisis resolución');
    Resolucion()[0].RES_CREATED_DATE = ko.observable(Solicitud.Resolucion.RES_CREATED_DATE);
    Resolucion()[0].RES_MODIFIED_DATE = ko.observable(Solicitud.Resolucion.RES_MODIFIED_DATE);

    var DocumentoResolucion = ko.observableArray([
        {
            RES_IS_CREATED: ko.observable(Solicitud.Resolucion.RES_IS_CREATED),
            RES_NAME: ko.observable(Solicitud.Resolucion.RES_NAME + '.docx'),
        }
    ]);

    var ComunicadoAdministrativo = ko.observableArray([]);
    if (Solicitud.ComunicadoAdministrativo != null) {
        ComunicadoAdministrativo.push(ko.mapping.fromJS(Solicitud.ComunicadoAdministrativo));
    }

    var ComunicadoTecnico = ko.observableArray([]);
    if (Solicitud.ComunicadoTecnico != null) {
        ComunicadoTecnico.push(ko.mapping.fromJS(Solicitud.ComunicadoTecnico));
    }

    var Cuadro = ListObjectFindGetElement(Solicitud.TechDocs, 'TECH_TYPE', enumDOC_TECH_TYPE.Cuadro);
    if (Cuadro != null) {
        if (Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID == enumROLE1_STATE.Aprobado)
            Cuadro.visible = true;
        else
            Cuadro.visible = false;
    }

    function PreSelectedTab() {
        var Resultado = 0;
        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
                if (!AnalisisFinancieroCompleto())
                    Resultado = 0;
                else
                    Resultado = 1;
                break;
            case enumSTEPS.Tecnico:
                Resultado = 2;
                break;
            case enumSTEPS.Resolucion:
                Resultado = 3;
                break;
            case enumSTEPS.EnCierre:
            case enumSTEPS.Terminada:
                if (Solicitud.SOL_TYPE_CLASS == enumSOL_TYPES_CLASS.AdministrativoOnly)
                    Resultado = 1;
                else
                    Resultado = 3;
                break;
        }
        return Resultado;
    };

    var AnalisisAdministrativoCompleto = ko.computed(function () {
        var Resultado = true;
        switch (ROLE) {
            case enumROLES.Funcionario:
                Administrativo().forEach(function (entry) {
                    if (entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.SinDefinir || entry.ANX_ROLE1_STATE_ID() == enumROLE1_STATE.Radicado || !entry.ANX_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Coordinador:
                Administrativo().forEach(function (entry) {
                    if (entry.ANX_ROLE4_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.ANX_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
            case enumROLES.Subdirector:
                Administrativo().forEach(function (entry) {
                    if (entry.ANX_ROLE8_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.ANX_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
        }
        return Resultado;
    });

    var AnalisisFinancieroCompleto = ko.computed(function () {
        var Resultado = true;
        if (IsNullOrEmpty(Financiero.FIN_SEVEN_ALDIA())) {
            Resultado = false;
        } else if (Financiero.FIN_SEVEN_ALDIA() == true) {
            if (IsNullOrEmpty(Financiero.FIN_ESTADO_CUENTA_NUMBER()) || IsNullOrEmpty(Financiero.FIN_REGISTRO_ALFA_NUMBER()) || IsNullOrEmpty(Financiero.FIN_ESTADO_CUENTA_DATE()) || IsNullOrEmpty(Financiero.FIN_REGISTRO_ALFA_DATE())) {
                Resultado = false;
            }
        }
        return Resultado;
    });

    var AnalisisTecnicoCompleto = ko.computed(function () {
        var Resultado = true;
        switch (ROLE) {
            case enumROLES.Subdirector:
                Tecnico().forEach(function (entry) {
                    if (entry.TECH_ROLE_MINTIC_STATE_ID() == enumROLE248_STATE.SinDefinir || !entry.TECH_ROLEX_CHANGED()) {
                        Resultado = false;
                    }
                });
                break;
        }
        return Resultado;
    });

    var AnalisisResolucionCompleto = ko.computed(function () {
        var Resultado = true;
        if (!DocumentoResolucion()[0].RES_IS_CREATED())
            return false;
        if (ROLE != enumROLES.Administrador) {
            Resolucion().forEach(function (entry) {
                if (entry[GetResStateID(ROLE)]() == enumROLE248_STATE.SinDefinir || !entry.RES_ROLEX_CHANGED()) {
                    Resultado = false;
                }
            });
        }

        return Resultado;
    });

    var FinancieroDisable = ko.computed(function () {
        if (CURRENT.GROUP() != enumGROUPS.MinTIC || (CURRENT.STEP() != enumSTEPS.Administrativo) || ROLE != enumROLES.Funcionario || ROLE != CURRENT.ROLE() || GuardarDisabled() == true)
            return true;
        return false;
    });

    var AdministrativoDisable = ko.computed(function () {
        if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || ([enumSTEPS.Administrativo, enumSTEPS.Tecnico, enumSTEPS.Resolucion].NoExists(CURRENT.STEP())) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
            return true;
        return false;
    });

    var TecnicoDisable = ko.computed(function () {
        if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Tecnico) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
            return true;
        return false;
    });

    var ResolucionDisable = ko.computed(function () {
        if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Resolucion) || (ROLE != CURRENT.ROLE()) || (GuardarDisabled() == true))
            return true;
        return false;
    });

    function GuardarTramiteAdministrativo(SolicitudParameter, bGenerarComunicado) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GuardarTramiteAdministrativo",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                ROLE: ROLE,
                Solicitud: SolicitudParameter,
                bGenerarComunicado: bGenerarComunicado
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                loadingVisible(false);

                var result = msg.GuardarTramiteAdministrativoResult;

                if (result.Status != 1) {
                    DevExpress.ui.notify(result.Message, "error", 2000);
                } else {
                    DevExpress.ui.notify(result.Message, "success", 2000);

                    Solicitud = result.Solicitud;

                    if (bGenerarComunicado) {
                        ko.mapping.fromJS(Solicitud.USR_ADMIN, USR_ADMIN);
                        ko.mapping.fromJS(Solicitud.USR_TECH, USR_TECH);

                        CURRENT.GROUP(Solicitud.CURRENT.GROUP);
                        CURRENT.ROLE(Solicitud.CURRENT.ROLE);
                        CURRENT.STEP(Solicitud.CURRENT.STEP);

                        Tecnico()[0].TECH_ROLEX_CHANGED(Solicitud.AnalisisTecnico.TECH_ROLEX_CHANGED);
                        Resolucion()[0].RES_ROLEX_CHANGED(Solicitud.AnalisisResolucion.RES_ROLEX_CHANGED);

                        Administrativo().forEach(function (entry) {
                            entry.ANX_ROLEX_CHANGED(ListObjectGetAttribute(Solicitud.Anexos, 'ANX_UID', entry.ANX_UID(), 'ANX_ROLEX_CHANGED'));
                        });

                        var gridAdministrativo = $("#GridAdministrativo").dxDataGrid('instance');
                        if (gridAdministrativo != undefined) {
                            gridAdministrativo.repaint();
                        }

                        var gridTecnico = $("#GridTecnico").dxDataGrid('instance');
                        if (gridTecnico != undefined) {
                            gridTecnico.repaint();
                        }

                        var gridResolucion = $("#GridResolucion").dxDataGrid('instance');
                        if (gridResolucion != undefined) {
                            gridResolucion.repaint();
                        }

                        if (ROLE == enumROLES.Funcionario) {
                            if (Solicitud.ComunicadoAdministrativo != null) {
                                ComunicadoAdministrativo([]);
                                ComunicadoAdministrativo.push(ko.mapping.fromJS(Solicitud.ComunicadoAdministrativo));
                                var list = $("#ListComunicadoAdministrativo").dxList('instance');
                                if (list != undefined)
                                    list.reload();
                            }
                        }

                    }
                }
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);
            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 5000);
                }
                SGEWeb.app.DisabledToolBar(false);
                GuardarDisabled(false);
                loadingVisible(false);
            }
        });
    }

    function GetAnalisisFinanciero(Solicitud) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetAnalisisFinanciero",
            data: JSON.stringify({
                IDUserWeb: SGEWeb.app.IDUserWeb,
                Solicitud: Solicitud
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetAnalisisFinancieroResult;
                if (result.Error == true) {
                    DevExpress.ui.notify(result.ErrorMessage, "error", 3000);
                    //Financiero.FIN_SEVEN_ALDIA(undefined);
                } else {
                    Financiero.FIN_SEVEN_ALDIA(result.Estado)
                }

                loadingVisible(false);
                GuardarDisabled(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    2
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
                GuardarDisabled(false);
            }
        });
    }

    function GetResolucion(SOL_UID) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetResolucion",
            data: JSON.stringify({
                SOL_UID: SOL_UID
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                Solicitud.Resolucion = msg.GetResolucionResult;
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function GetOpacityAdmin(enumROLE, ROLEX_CHANGED) {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
                if (CURRENT.ROLE() > enumROLE)
                    return 1;
                if (CURRENT.ROLE() < enumROLE)
                    return 0.3;
                if (ROLEX_CHANGED)
                    return 1;
                else
                    return 0.3;
                break;

            case enumSTEPS.Tecnico:
            case enumSTEPS.Resolucion:
                return 1;
                break;
        }
    }

    function GetOpacityTech(enumROLE, ROLEX_CHANGED) {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
            case enumSTEPS.Resolucion:
                return 1;
                break;

            case enumSTEPS.Tecnico:
                if (CURRENT.GROUP() == enumGROUPS.MinTIC) {
                    if ((CURRENT.ROLE() + 8) > enumROLE)
                        return 1;
                    if ((CURRENT.ROLE() + 8) < enumROLE)
                        return 0.3;
                    if (ROLEX_CHANGED)
                        return 1;
                    else
                        return 0.3;
                } else {
                    if (CURRENT.ROLE() > enumROLE)
                        return 1;
                    if (CURRENT.ROLE() < enumROLE)
                        return 0.3;
                    if (ROLEX_CHANGED)
                        return 1;
                    else
                        return 0.3;
                }
                break;
        }
    }

    function GetOpacityResolucion(enumROLE, ROLEX_CHANGED) {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
            case enumSTEPS.Tecnico:
                return 1;
                break;

            case enumSTEPS.Resolucion:
                if ([enumROLES.Asesor, enumROLES.Director].Exists(enumROLE) && [enumROLES.Asesor, enumROLES.Director].Exists(CURRENT.ROLE())) {
                    if (CURRENT.ROLE() < enumROLE)
                        return 1;
                    if (CURRENT.ROLE() > enumROLE)
                        return 0.3;
                } else {
                    if (CURRENT.ROLE() > enumROLE)
                        return 1;
                    if (CURRENT.ROLE() < enumROLE)
                        return 0.3;
                }
                if (ROLEX_CHANGED)
                    return 1;
                else
                    return 0.3;
                break;
        }
    }

    function onCellTemplate(container, e) {
        switch (e.component._$element[0].id) {
            case 'GridAdministrativo':
                switch (e.column.name) {
                    case 'Funcionario':
                        $('<i/>').addClass(ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Icon)
                            .prop('title', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Text + (IsNullOrEmpty(e.data.ANX_ROLE1_COMMENT()) ? '' : ': ' + e.data.ANX_ROLE1_COMMENT()))
                            .css('cursor', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Cursor)
                            .css('color', ROLE1_STATES[e.data.ANX_ROLE1_STATE_ID()].Color)
                            .css('opacity', GetOpacityAdmin(enumROLES.Funcionario, e.data.ANX_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;
                    case 'Coordinador':
                    case 'Subdirector':
                        $('<i/>').addClass(ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Icon)
                            .prop('title', ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Text + (IsNullOrEmpty(e.data[GetAdminComment(e.column.name)]()) ? '' : ': ' + e.data[GetAdminComment(e.column.name)]()))
                            .css('cursor', 'default')
                            .css('color', ROLE248_STATES[e.data[GetAdminStateID(e.column.name)]()].Color)
                            .css('margin-left', '5px')
                            .css('opacity', GetOpacityAdmin(enumROLES[e.column.name], e.data.ANX_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;
                    case 'Ver':
                        $('<i/>').addClass('ta ta-eye ta-lg')
                            .prop('title', 'Ver documento')
                            .css('cursor', 'pointer')
                            .appendTo(container);
                        break;
                    case 'Rechazar':
                    case 'Requerir':
                    case 'Aprobar':
                    case 'Devolver':
                    case 'Aceptar':
                        var Disable_Real = (CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Administrativo) || (ROLE != CURRENT.ROLE()) ||
                            (ROLE == enumROLES.Funcionario && e.data.ANX_ROLE4_STATE_ID() == enumROLE248_STATE.Aceptado) ||
                            (ROLE == enumROLES.Coordinador && e.data.ANX_ROLE8_STATE_ID() == enumROLE248_STATE.Aceptado);
                        var Disable_Parcial = e.data.ANX_OPENED() == 0 && !SaltarOpened;
                        $('<i/>').addClass(e.column.icon)
                            .prop('title', e.column.caption + ' documento')
                            .css('cursor', (Disable_Real ? 'default' : (Disable_Parcial ? 'default' : 'pointer')))
                            .css('color', Disable_Real ? 'Black' : e.column.color)
                            .css('opacity', (Disable_Real ? 0.3 : (Disable_Parcial ? 0.3 : 1)))
                            .appendTo(container);
                        break;
                }
                break;

            case 'GridTecnico':
                switch (e.column.name) {
                    case 'Funcionario':
                        $('<i/>').addClass(ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Icon)
                            .prop('title', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Text + (IsNullOrEmpty(e.data.TECH_ROLE1_COMMENT()) ? '' : ': ' + e.data.TECH_ROLE1_COMMENT()))
                            .css('cursor', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Cursor)
                            .css('color', ROLE1_STATES[e.data.TECH_ROLE1_STATE_ID()].Color)
                            .css('opacity', GetOpacityTech(enumROLES.Funcionario, e.data.TECH_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;
                    case 'Revisor':
                    case 'Coordinador':
                    case 'Subdirector':
                        $('<i/>').addClass(ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Icon)
                            .prop('title', ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Text + (IsNullOrEmpty(e.data[GetTechComment(e.column.name)]()) ? '' : ': ' + e.data[GetTechComment(e.column.name)]()))
                            .css('cursor', 'default')
                            .css('color', ROLE248_STATES[e.data[GetTechStateID(e.column.name)]()].Color)
                            .css('margin-left', '5px')
                            .css('opacity', GetOpacityTech(enumROLES[e.column.name], e.data.TECH_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;
                    case 'MinTIC':
                        $('<i/>').addClass(ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Icon)
                            .prop('title', ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Text + (IsNullOrEmpty(e.data.TECH_ROLE_MINTIC_COMMENT()) ? '' : ': ' + e.data.TECH_ROLE_MINTIC_COMMENT()))
                            .css('cursor', 'default')
                            .css('color', ROLE248_STATES[e.data.TECH_ROLE_MINTIC_STATE_ID()].Color)
                            .css('margin-left', '5px')
                            .css('opacity', GetOpacityTech(enumROLES.Subdirector + 8, e.data.TECH_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;
                    case 'Devolver':
                    case 'Aceptar':
                        var Disable_Real = (CURRENT.GROUP() != enumGROUPS.MinTIC) || (ROLE != CURRENT.ROLE());
                        $('<i/>').addClass(e.column.icon)
                            .prop('title', e.column.caption + ' análisis')
                            .css('cursor', (Disable_Real ? 'default' : 'pointer'))
                            .css('color', Disable_Real ? 'Black' : e.column.color)
                            .css('opacity', (Disable_Real ? 0.3 : 1))
                            .appendTo(container);
                        break;
                }
                break;

            case '__hidden-bag-GridResolucion':
            case 'GridResolucion':
                switch (e.column.name) {
                    case 'Funcionario':
                    case 'Revisor':
                    case 'Coordinador':
                    case 'Subdirector':
                    case 'Director':
                    case 'Asesor':
                        $('<i/>').addClass(ROLE248_STATES[e.data[GetResStateID(e.column.name)]()].Icon)
                            .prop('title', ROLE248_STATES[e.data[GetResStateID(e.column.name)]()].Text + (IsNullOrEmpty(e.data[GetResComment(e.column.name)]()) ? '' : ': ' + e.data[GetResComment(e.column.name)]()))
                            .css('cursor', 'default')
                            .css('color', ROLE248_STATES[e.data[GetResStateID(e.column.name)]()].Color)
                            .css('margin-left', '5px')
                            .css('opacity', GetOpacityResolucion(enumROLES[e.column.name], e.data.RES_ROLEX_CHANGED()))
                            .appendTo(container);
                        break;

                    case 'Editar':
                        var Disable_Real = (CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Resolucion) || (ROLE != CURRENT.ROLE());
                        $('<i/>').addClass('ta ta-pencil1 ta-lg')
                            .prop('title', 'Editar resolución')
                            .css('cursor', (Disable_Real ? 'default' : 'pointer'))
                            .css('opacity', (Disable_Real ? 0.3 : 1))
                            .appendTo(container);
                        break;

                    case 'Devolver':
                    case 'Aceptar':
                    case 'Revision':
                        var Disable_Real = (CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Resolucion) || (ROLE != CURRENT.ROLE());
                        $('<i/>').addClass(e.column.icon)
                            .prop('title', e.column.caption + ' análisis')
                            .css('cursor', (Disable_Real ? 'default' : 'pointer'))
                            .css('color', Disable_Real ? 'Black' : e.column.color)
                            .css('opacity', (Disable_Real ? 0.3 : 1))
                            .appendTo(container);
                        break;
                }
                break;
        }
    }

    function CellClick(e) {
        if (e.rowType == 'data') {
            switch (e.element[0].id) {
                case 'GridAdministrativo':
                    switch (e.column.name) {
                        case 'Ver':
                            window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=ANX&FUID=' + e.data.ANX_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
                            e.data.ANX_OPENED(1);
                            break;
                        case 'Rechazar':
                        case 'Requerir':
                        case 'Aprobar':
                            if ((e.data.ANX_OPENED() == 0 && !SaltarOpened) || AdministrativoDisable() || e.data.ANX_ROLE4_STATE_ID() == 1) {
                                e.event.stopPropagation();
                                break;
                            }
                            MyPopUp.UID(e.data.ANX_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.ANX_TYPE_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data.ANX_ROLE1_STATE_ID() == e.column.ID)
                                MyPopUp.COMMENT(e.data.ANX_ROLE1_COMMENT());

                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Administrativo';
                            MyPopUp.Show(true);
                            break;

                        case 'Devolver':
                        case 'Aceptar':
                            if ((e.data.ANX_OPENED() == 0 && !SaltarOpened) || AdministrativoDisable()) {
                                e.event.stopPropagation();
                                break;
                            }

                            if (ROLE < enumROLES.Subdirector && e.data[GetAdminStateID(GetNextRole(ROLE))]() == enumROLE248_STATE.Aceptado) {
                                e.event.stopPropagation();
                                break;
                            }
                            //Cuando acepta para que no aparezca el popup, quitar este bloque para que si aparezca el popup
                            if (e.column.ID == enumROLE248_STATE.Aceptado) {
                                e.data.ANX_ROLEX_CHANGED(true);
                                e.data[GetAdminComment(ROLE)]('');
                                e.data[GetAdminStateID(ROLE)](e.column.ID);
                                e.data.ANX_STATE_ID.valueHasMutated();
                                break;
                            }
                            MyPopUp.UID(e.data.ANX_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.ANX_TYPE_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data[GetAdminStateID(ROLE)]() == e.column.ID)
                                MyPopUp.COMMENT(e.data[GetAdminComment(ROLE)]());
                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Administrativo';
                            MyPopUp.Show(true);
                            break;
                    }
                    break;

                case 'GridTecnico':
                    switch (e.column.name) {
                        case 'Devolver':
                        case 'Aceptar':
                            if (TecnicoDisable()) {
                                e.event.stopPropagation();
                                break;
                            }
                            switch (ROLE) {
                                case enumROLES.Subdirector:
                                    if (e.column.ID == enumROLE248_STATE.Aceptado) {
                                        e.data.TECH_ROLEX_CHANGED(true);
                                        e.data.TECH_ROLE_MINTIC_COMMENT('');
                                        e.data.TECH_ROLE_MINTIC_STATE_ID(e.column.ID);
                                        e.data.TECH_STATE_ID.valueHasMutated();
                                        break;
                                    }
                                    MyPopUp.UID(e.data.SOL_UID());
                                    MyPopUp.STATE_ID(e.column.ID);
                                    MyPopUp.NAME(e.data.TECH_NAME());
                                    MyPopUp.COMMENT('');
                                    if (e.data.TECH_ROLE_MINTIC_STATE_ID() == e.column.ID)
                                        MyPopUp.COMMENT(e.data.TECH_ROLE_MINTIC_COMMENT());
                                    MyPopUp.Title(e.column.caption);
                                    MyPopUp.Who = 'Tecnico';
                                    MyPopUp.Show(true);
                                    break;
                            }
                            break;
                    }
                    break;

                case 'GridResolucion':
                    switch (e.column.name) {
                        case 'Editar':
                            if ((CURRENT.GROUP() != enumGROUPS.MinTIC) || (CURRENT.STEP() != enumSTEPS.Resolucion) || (ROLE != CURRENT.ROLE())) {
                                e.event.stopPropagation();
                                break;
                            }
                            SGEWeb.app.CallBackFunction = CallbackResolucion;
                            SGEWeb.app.Solicitud = JSON.stringify(Solicitud);
                            SGEWeb.app.navigate({ view: "RDSResoluciones", id: -1, settings: { title: 'ABC', ROLE: ROLE, GROUP: GROUP } });
                            break;

                        case 'Devolver':
                        case 'Aceptar':
                        case 'Revision':
                            if (ResolucionDisable()) {
                                e.event.stopPropagation();
                                break;
                            }
                            if (e.column.ID.In([enumROLE248_STATE.Aceptado, enumROLE248_STATE.Revision])) {
                                e.data.RES_ROLEX_CHANGED(true);
                                e.data[GetResComment(ROLE)]('');
                                e.data[GetResStateID(ROLE)](e.column.ID);
                                e.data.RES_STATE_ID.valueHasMutated();
                                break;
                            }
                            MyPopUp.UID(e.data.SOL_UID());
                            MyPopUp.STATE_ID(e.column.ID);
                            MyPopUp.NAME(e.data.RES_NAME());
                            MyPopUp.COMMENT('');
                            if (e.data[GetResStateID(ROLE)]() == e.column.ID)
                                MyPopUp.COMMENT(e.data[GetResComment(ROLE)]());

                            MyPopUp.Title(e.column.caption);
                            MyPopUp.Who = 'Resolucion';
                            MyPopUp.Show(true);
                            break;
                    }
                    break;
            }
        }
    }

    function GuardarTramite() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Guardando trámite...");
        loadingVisible(true);
        GuardarDisabled(true);
        Solicitud.Financiero = ko.toJS(Financiero);
        Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
        Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
        Solicitud.Anexos = ko.toJS(Administrativo);
        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
        Solicitud.AnalisisResolucion = ko.toJS(Resolucion()[0]);
        SGEWeb.app.NeedRefresh = true;
        GuardarTramiteAdministrativo(Solicitud, false);
    }

    function ContinuarTramite() {

        switch (CURRENT.STEP()) {
            case enumSTEPS.Administrativo:
                switch (ROLE) {
                    case enumROLES.Funcionario:
                        if (AnalisisFinancieroCompleto() == false) {
                            DevExpress.ui.notify('Análisis financiero incompleto.', "warning", 3000);
                            return;
                        }
                        if (AnalisisAdministrativoCompleto() == false) {
                            DevExpress.ui.notify('Análisis administrativo incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.Financiero = ko.toJS(Financiero);
                                Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                                Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                                Solicitud.Anexos = ko.toJS(Administrativo);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarTramiteAdministrativo(Solicitud, true);
                            }
                        });
                        break;

                    case enumROLES.Coordinador:
                        if (AnalisisAdministrativoCompleto() == false) {
                            DevExpress.ui.notify('Análisis administrativo incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }
                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {
                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.Financiero = ko.toJS(Financiero);
                                Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                                Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                                Solicitud.Anexos = ko.toJS(Administrativo);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarTramiteAdministrativo(Solicitud, true);
                            }
                        });
                        break;

                    case enumROLES.Subdirector:
                        if (AnalisisAdministrativoCompleto() == false) {
                            DevExpress.ui.notify('Análisis administrativo incompleto. Todos los documentos deben ser evaluados.', "warning", 3000);
                            return;
                        }

                        var Devolver = KoListObjectExists(Administrativo(), "ANX_ROLE8_STATE_ID", enumROLE248_STATE.Devuelto);

                        if (!Devolver) {
                            if (SGEWeb.app.User.USR_HAS_SIGN == 0) {
                                DevExpress.ui.notify('El usuario no tiene firma digital registrada en el sistema. En administración de usuarios puede cargar la firma.', "warning", 5000);
                                return;
                            }
                        }

                        if (Solicitud.Expediente.SERV_STATUS.toLowerCase().In(['ect', 'even', 'eres'])) {
                            DevExpress.ui.notify('El expediente se encuentra en un estado no válido para continuar con la solicitud. Favor actualizar en Manager para continuar.', "warning", 5000);
                            return;
                        }

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {

                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.Financiero = ko.toJS(Financiero);
                                Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                                Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                                Solicitud.Anexos = ko.toJS(Administrativo);
                                Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarTramiteAdministrativo(Solicitud, true);
                            }
                        });
                        break;
                }
                break;

            case enumSTEPS.Tecnico:
                switch (ROLE) {
                    case enumROLES.Subdirector:
                        if (AnalisisTecnicoCompleto() == false) {
                            DevExpress.ui.notify('Análisis técnico incompleto. Debe aceptar o devolver el análisis técnico.', "warning", 3000);
                            return;
                        }

                        if (Tecnico()[0].TECH_ROLE_MINTIC_STATE_ID() == enumROLE248_STATE.Aceptado) {
                            if (!Tecnico()[0].TECH_PTNRS_UPDATED()) {
                                DevExpress.ui.notify('Para continuar es necesario que el plan técnico (PTNRS) esté actualizado.', "warning", 5000);
                                return;
                            }
                        }

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {

                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.Financiero = ko.toJS(Financiero);
                                Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                                Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                                Solicitud.Anexos = ko.toJS(Administrativo);
                                Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarTramiteAdministrativo(Solicitud, true);
                            }
                        });
                        break;

                   case enumROLES.Coordinador:

                        var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                        result.done(function (dialogResult) {
                            if (dialogResult) {

                                loadingMessage("Continuando trámite...");
                                loadingVisible(true);
                                GuardarDisabled(true);
                                Solicitud.Financiero = ko.toJS(Financiero);
                                Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                                Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                                Solicitud.Anexos = ko.toJS(Administrativo);
                                Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                                SGEWeb.app.NeedRefresh = true;
                                GuardarTramiteAdministrativo(Solicitud, true);
                            }
                        });
                        break;
                }
                break;

            case enumSTEPS.Resolucion:
                if (!DocumentoResolucion()[0].RES_IS_CREATED()) {
                    DevExpress.ui.notify('Análisis de la resolución incompleto. Debe crear la resolución y aceptar o devolver el análisis de la resolución.', "warning", 3000);
                    return;
                }

                if (AnalisisResolucionCompleto() == false) {
                    DevExpress.ui.notify('Análisis de la resolución incompleto. Debe aceptar o devolver el análisis de la resolución.', "warning", 3000);
                    return;
                }

                if (ROLE == enumROLES.Director) {
                    if (Resolucion()[0].RES_ROLE32_STATE_ID() == enumROLE248_STATE.Aceptado) {
                        if (Solicitud.Resolucion.RES_TRACKING_COUNT > 0) {
                            DevExpress.ui.notify('Para continuar es necesario resolver todos los cambios pendientes. Actualmente hay ' + Solicitud.Resolucion.RES_TRACKING_COUNT + ' cambio(s) sin resolver.', "warning", 3000);
                            return;
                        }
                    }
                }
                var result = DevExpress.ui.dialog.confirm(CrearDialogHtml('ta ta-question-circle', 'Desea continuar el trámite?'), SGEWeb.app.Name);
                result.done(function (dialogResult) {
                    if (dialogResult) {

                        loadingMessage("Continuando trámite...");
                        loadingVisible(true);
                        GuardarDisabled(true);
                        Solicitud.Financiero = ko.toJS(Financiero);
                        Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE = DateToString(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                        Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE = DateToString(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                        Solicitud.Anexos = ko.toJS(Administrativo);
                        Solicitud.AnalisisTecnico = ko.toJS(Tecnico()[0]);
                        Solicitud.AnalisisResolucion = ko.toJS(Resolucion()[0]);
                        SGEWeb.app.NeedRefresh = true;
                        GuardarTramiteAdministrativo(Solicitud, true);
                    }
                });
                break;
        }
    }

    function ConsultarSeven() {
        SGEWeb.app.DisabledToolBar(true);
        loadingMessage("Consultando...");
        loadingVisible(true);
        GuardarDisabled(true);
        GetAnalisisFinanciero(Solicitud);
    }

    function OnGuardarComentario() {
        switch (MyPopUp.Who) {
            case 'Administrativo':
                {
                    if (((ROLE == enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE1_STATE.Aprobado) || (ROLE != enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE248_STATE.Aceptado)) && (MyPopUp.COMMENT() == null || MyPopUp.COMMENT() == undefined || MyPopUp.COMMENT().length < 10)) {
                        DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                        return;
                    }

                    var AdministrativoItem = KoListObjectFindGetElement(Administrativo(), 'ANX_UID', MyPopUp.UID());

                    AdministrativoItem.ANX_ROLEX_CHANGED(true);

                    switch (ROLE) {
                        case enumROLES.Funcionario:
                            AdministrativoItem.ANX_ROLE1_COMMENT(MyPopUp.COMMENT());
                            AdministrativoItem.ANX_ROLE1_STATE_ID(MyPopUp.STATE_ID());
                            AdministrativoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                        case enumROLES.Coordinador:
                            AdministrativoItem.ANX_ROLE4_COMMENT(MyPopUp.COMMENT());
                            AdministrativoItem.ANX_ROLE4_STATE_ID(MyPopUp.STATE_ID());
                            AdministrativoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                        case enumROLES.Subdirector:
                            AdministrativoItem.ANX_ROLE8_COMMENT(MyPopUp.COMMENT());
                            AdministrativoItem.ANX_ROLE8_STATE_ID(MyPopUp.STATE_ID());
                            AdministrativoItem.ANX_STATE_ID.valueHasMutated();
                            break;
                    }
                    MyPopUp.Show(false);
                }
                break;

            case 'Tecnico':
                {
                    if (((ROLE == enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE1_STATE.Aprobado) || (ROLE != enumROLES.Funcionario && MyPopUp.STATE_ID() != enumROLE248_STATE.Aceptado)) && (MyPopUp.COMMENT() == null || MyPopUp.COMMENT() == undefined || MyPopUp.COMMENT().length < 10)) {
                        DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                        return;
                    }

                    var TecnicoItem = KoListObjectFindGetElement(Tecnico(), 'SOL_UID', MyPopUp.UID());

                    TecnicoItem.TECH_ROLEX_CHANGED(true);

                    switch (ROLE) {
                        case enumROLES.Subdirector:
                            TecnicoItem.TECH_ROLE_MINTIC_COMMENT(MyPopUp.COMMENT());
                            TecnicoItem.TECH_ROLE_MINTIC_STATE_ID(MyPopUp.STATE_ID());
                            TecnicoItem.TECH_STATE_ID.valueHasMutated();
                            break;
                    }
                    MyPopUp.Show(false);
                }
                break;

            case 'Resolucion':
                {
                    if ((MyPopUp.STATE_ID() != enumROLE248_STATE.Aceptado) && (MyPopUp.COMMENT() == null || MyPopUp.COMMENT() == undefined || MyPopUp.COMMENT().length < 10)) {
                        DevExpress.ui.notify('Es necesario escribir un comentario mayor a 10 caracteres.', "warning", 3000);
                        return;
                    }

                    var ResolucionItem = KoListObjectFindGetElement(Resolucion(), 'SOL_UID', MyPopUp.UID());

                    ResolucionItem.RES_ROLEX_CHANGED(true);

                    ResolucionItem['RES_ROLE' + ROLE + '_COMMENT'](MyPopUp.COMMENT());
                    ResolucionItem['RES_ROLE' + ROLE + '_STATE_ID'](MyPopUp.STATE_ID());
                    ResolucionItem.RES_STATE_ID.valueHasMutated();

                    MyPopUp.Show(false);
                }
                break;

        }

    }

    function OnOpenTechDoc(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=TEC&FUID=' + e.itemData.TECH_UID + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function OnOpenComunicadoAdministrativo(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=COM&FUID=' + e.itemData.COM_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function OnOpenComunicadoTecnico(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=COM&FUID=' + e.itemData.COM_UID() + '&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function OnOpenResolucion(e) {
        window.open(SGEWeb.app.RutaProxy + '/SGEProxy/FileHandler.axd?TYP=RES&FUID=' + Solicitud.SOL_UID + '&MRC=0&NOCACHE=' + SGEWeb.app.NewGuid(), "_blank");
    }

    function GridInitialized(e) {
        GridResolucionInstance = e.component;
    }

    function CallbackResolucion(ResolucionParameter) {
        Solicitud.Resolucion = ResolucionParameter;

        DocumentoResolucion()[0].RES_NAME(Solicitud.Resolucion.RES_NAME + '.pdf');
        DocumentoResolucion()[0].RES_IS_CREATED(Solicitud.Resolucion.RES_IS_CREATED);

        Resolucion()[0].RES_CREATED_DATE(Solicitud.Resolucion.RES_CREATED_DATE);
        Resolucion()[0].RES_MODIFIED_DATE(Solicitud.Resolucion.RES_MODIFIED_DATE);

        if (GridResolucionInstance != null) {
            GridResolucionInstance.repaint();
        }
    }

    if (Solicitud.Resolucion.RES_IS_CREATED) {
        GetResolucion(Solicitud.SOL_UID);
    }

    var PreSelectedTabIndex = PreSelectedTab();

    function OnToggleVisibility(e, data) {
        switch (e) {
            case 'Logo':
                ToggleVisibility.Logo(!ToggleVisibility.Logo());
                break;
            case 'Concesionario':
                ToggleVisibility.Concesionario(!ToggleVisibility.Concesionario());
                break;
            case 'Paso':
                ToggleVisibility.Paso(!ToggleVisibility.Paso());
                break;
            case 'Footer':
                ToggleVisibility.Footer(!ToggleVisibility.Footer());
                break;
        }
    }

    var viewModel = {
        OnGuardarComentario: OnGuardarComentario,
        MyPopUp: MyPopUp,
        loadingVisible: loadingVisible,
        loadingMessage: loadingMessage,
        GuardarDisabled: GuardarDisabled,
        Solicitud: Solicitud,
        Financiero: Financiero,
        Administrativo: Administrativo,
        Tecnico: Tecnico,
        Resolucion: Resolucion,
        GuardarTramite: GuardarTramite,
        ContinuarTramite: ContinuarTramite,
        ConsultarSeven: ConsultarSeven,
        onCellTemplate: onCellTemplate,
        CellClick: CellClick,
        ROLE: ROLE,
        CURRENT: CURRENT,
        FinancieroDisable: FinancieroDisable,
        AdministrativoDisable: AdministrativoDisable,
        OnOpenTechDoc: OnOpenTechDoc,
        OnOpenResolucion: OnOpenResolucion,
        ComunicadoAdministrativo: ComunicadoAdministrativo,
        ComunicadoTecnico: ComunicadoTecnico,
        OnOpenComunicadoAdministrativo: OnOpenComunicadoAdministrativo,
        OnOpenComunicadoTecnico: OnOpenComunicadoTecnico,
        GridInitialized: GridInitialized,
        DocumentoResolucion: DocumentoResolucion,
        PreSelectedTabIndex: PreSelectedTabIndex,
        OnToggleVisibility: OnToggleVisibility,
        ToggleVisibility: ToggleVisibility,
        USR_ADMIN: USR_ADMIN,
        USR_TECH: USR_TECH

    };

    return viewModel;
};