﻿SGEWeb.RDSConteos = function (params) {
    "use strict";
    var title = JSON.parse(params.settings.substring(5)).title;
    var GROUP = JSON.parse(params.settings.substring(5)).GROUP;
    var ROLE = JSON.parse(params.settings.substring(5)).ROLE;
    var EXTRA = JSON.parse(params.settings.substring(5)).EXTRA;

    var loadingVisible = ko.observable(false);
    var MyDataSource = [];
    var MaxCont = 1;
    
    var SelectedChart = ko.observable(EXTRA);
    var NameRF = 'Año actual';
    var Date1 = Date.today().add({ months: -1 }).moveToFirstDayOfMonth().toString('yyyyMMdd');
    var Date2 = Date.today().add({ months: -1 }).moveToLastDayOfMonth().toString('yyyyMMdd');
    var selectedRangeID = 10;
    var selectedDateINI = ko.observable(undefined);
    var selectedDateEND = ko.observable(undefined);

    var ContextMenuRangoVisible = ko.observable(false);
    var ShowFechas = ko.observable(false);

    var RangeArr = [{ ID: 2, text: 'Semana actual' },
                    { ID: 3, text: 'Semana anterior' },
                    { ID: 4, text: 'Este mes' },
                    { ID: 5, text: 'Mes pasado' },
                    { ID: 6, text: 'Trimestre actual' },
                    { ID: 7, text: 'Trimestre pasado' },
                    { ID: 8, text: 'Semestre actual' },
                    { ID: 9, text: 'Semestre pasado' },
                    { ID: 10, text: 'Año actual', selected: true },
                    { ID: 11, text: 'Año pasado' },
                    { ID: 12, text: 'Rango de fechas' }
    ];

    function GetConteoDevoluciones(USR_GROUP, USR_ROLE, DateIni, DateEnd) {
        $.ajax({
            type: "POST", //GET or POST or PUT or DELETE verb
            url: SGEWeb.app.Ruta + SGEWeb.app.WS2 + "/SGEWS.svc/GetConteoDevoluciones",
            data: JSON.stringify({
                USR_GROUP: USR_GROUP,
                USR_ROLE: USR_ROLE,
                DateIni: DateIni,
                DateEnd: DateEnd
            }),
            contentType: "application/json; charset=utf-8", // content type sent to server
            dataType: "json", //Expected data format from server
            processdata: true, //True or False
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Token", SGEWeb.app.Token);
            },
            success: function (msg) {//On Successfull service call

                var result = msg.GetConteoDevolucionesResult;
                MyDataSource = result;
                MaxCont = 1;
                MyDataSource.forEach(function (entry) {
                    if (entry.CONTEO > MaxCont)
                        MaxCont = entry.CONTEO;
                });
                MaxCont += 1;
                CargarGrafico();
                loadingVisible(false);
                SGEWeb.app.DisabledToolBar(false);

            },
            error: function (result) { // When Service call fails
                if (result.status == 403) {
                    DevExpress.ui.notify('Sesión expiró', "warning", 5000);
                    SGEWeb.app.ReLogin();
                } else {
                    DevExpress.ui.notify('Error' + ": " + result.statusText, "error", 3000);
                }
                SGEWeb.app.DisabledToolBar(false);
                loadingVisible(false);
            }
        });
    }

    function CargarGrafico() {
        SGEWeb.app.DisabledToolBar(false);

        $("#GraConteosBar").dxChart({
            title: { text: NameRF, margin: { top: 0 } },
            dataSource: MyDataSource,
            series: {
                argumentField: "USR_NAME",
                valueField: "CONTEO",
                name: (EXTRA == 1 ? "Funcionarios" : "Coordinadores"),
                type: "bar",
                color: '#6f91cb'
            },

            animation: { enabled: true },
            argumentAxis: {

                visible: true,
                placeholderSize: 35,
                opacity: 0.75,
                label: { alignment: 'center', font: { size: 12, color: 'black' } },
                tick: { visible: true, opacity: 0.75 }
            },
            valueAxis: {
                title: { text: 'Conteo devoluciones', font: { size: 20, color: 'black' } },
                visible: true,
                placeholderSize: 70,
                min: 0,
                max: MaxCont,
                tick: { visible: true, opacity: 0.75 },
            },
            tooltip: {
                customizeTooltip: function (point) {
                    // return { html: 'Carlos' };
                    return { html: '<i style="color:#6f91cb" class="ta ta-square"></i> ' + point.seriesName + '<BR>' + point.argumentText + '<BR>' + Globalizeformat(point.value, 'n0') + ' Devolución(es)' }
                },
                shared: true, enabled: true, zIndex: 1000
            },
            legend: {
                verticalAlignment: "bottom",
                horizontalAlignment: "center",
                itemTextPosition: "right",
                margin: { top: 10, bottom: 30 }
            },
            margin: { left: 15, right: 30, top: 50 }
        });

        $("#GraConteosPie").dxPieChart({
            title: { text: NameRF, margin: { top: 0 } },
            dataSource: MyDataSource,
            series: [
                {
                    argumentField: "USR_NAME",
                    valueField: "CONTEO",
                    label: {
                        visible: true,
                        connector: {
                            visible: true,
                            width: 1
                        },
                        customizeText: function (arg) {
                            return arg.valueText + " (" + arg.percentText + ")";
                        }
                    }
                }
            ],
            legend: {

                margin: { top: 10, bottom: 30, right: 50 }
            },
            margin: { left: 50, right: 50, top: 50, bottom: 50 }
        });

        if (SelectedChart() == 1) 
            $("#GraConteosBar").dxChart('instance').refresh();
        else 
            $("#GraConteosPie").dxPieChart('instance').refresh();
    }

    function ContextMenuRangoClick(e) {

        selectedRangeID = e.itemData.ID;

        switch (e.itemData.ID) {
            case 2: //Esta Semana
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: +1 }));
                selectedDateEND(Date.today());
                break;
            case 3: //Semana Pasada
                selectedDateINI(Date.today().moveToDayOfWeek(0, -1).add({ days: -6 }));
                selectedDateEND(Date.today().moveToDayOfWeek(0, -1));
                break;
            case 4: //Este mes
                selectedDateINI(Date.today().moveToFirstDayOfMonth());
                selectedDateEND(Date.today());
                break;
            case 5: //Mes pasado
                selectedDateINI(Date.today().add({ months: -1 }).moveToFirstDayOfMonth());
                selectedDateEND(Date.today().add({ months: -1 }).moveToLastDayOfMonth());
                break;
            case 6: //Este trimestre
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                selectedDateINI(new Date((new Date).getFullYear(), 3 * quarter - 3, 1));
                selectedDateEND(Date.today());
                break;
            case 7: //Trimestre pasado
                var quarter = Math.floor((new Date().getMonth() + 3) / 3);
                var year = quarter == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                quarter = quarter == 1 ? 4 : quarter - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 3 * quarter - 3, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 3 * quarter - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 8: //Este Semestre
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                selectedDateINI(new Date((new Date).getFullYear(), 6 * Semestre - 6, 1));
                selectedDateEND(Date.today());
                break;
            case 9: //Semestre pasado
                var Semestre = Math.floor((new Date().getMonth() + 6) / 6);
                var year = Semestre == 1 ? ((new Date).getFullYear() - 1) : ((new Date).getFullYear());
                Semestre = Semestre == 1 ? 2 : Semestre - 1;
                selectedDateINI(Date.today().set({ day: 1, month: 6 * Semestre - 6, year: year }));
                selectedDateEND(Date.today().set({ day: 15, month: 6 * Semestre - 1, year: year }).moveToLastDayOfMonth());
                break;
            case 10: //Este Año
                selectedDateINI(new Date((new Date).getFullYear(), 0, 1));
                selectedDateEND(new Date());
                break;
            case 11: //Año pasado
                selectedDateINI(new Date((new Date).getFullYear() - 1, 0, 1));
                selectedDateEND(new Date((new Date).getFullYear() - 1, 11, 31));
                break;
            case 12: //Rango de fechas
                ShowFechas(true);
                return;
                break;
        }

        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = e.itemData.text;

        Refrescar();
    }

    function CustomDateClick() {
        if (selectedDateINI() > selectedDateEND()) {
            DevExpress.ui.notify('La fecha inicial debe ser menor o igual que la fecha final', "warning", 3000);
            return;
        }

        ShowFechas(false);
        Date1 = selectedDateINI().toString('yyyyMMdd');
        Date2 = selectedDateEND().toString('yyyyMMdd');

        NameRF = selectedDateINI().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateINI().getMonth()]) + '-' + selectedDateEND().toString('dd/*/yyyy').replace('*', SGEWeb.app.months[selectedDateEND().getMonth()]);
        Refrescar();
    }

    function TipoGrafico() {
        if (SelectedChart() == 1) {
            SelectedChart(2);
            $("#GraConteosPie").dxPieChart('instance').refresh();
        } else {
            SelectedChart(1);
            $("#GraConteosBar").dxChart('instance').refresh();
        }
   
    }

    function Refrescar() {
        SGEWeb.app.DisabledToolBar(true);
        loadingVisible(true);
        GetConteoDevoluciones(GROUP, EXTRA, Date1, Date2);
    }

    ContextMenuRangoClick({ itemData: RangeArr[ListObjectFindIndex(RangeArr, 'ID', selectedRangeID)] });


    var viewModel = {
        title:title,
        loadingVisible: loadingVisible,
        Refrescar: Refrescar,
        RangeArr: RangeArr,
        ContextMenuRangoVisible: ContextMenuRangoVisible,
        ContextMenuRangoClick: ContextMenuRangoClick,
        selectedDateINI: selectedDateINI,
        selectedDateEND: selectedDateEND,
        ShowFechas: ShowFechas,
        CustomDateClick: CustomDateClick,
        SelectedChart: SelectedChart,
        TipoGrafico: TipoGrafico

    };

    return viewModel;
};