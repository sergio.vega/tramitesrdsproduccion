// NOTE object below must be a valid JSON
window.SGEWeb = $.extend(true, window.SGEWeb, {
  "config": {
    "layoutSet": "slideout",
    "navigation": [
      {
        "title": "RDSPrincipal",
        "onExecute": "#RDSPrincipal",
        "icon": "rdsprincipal"
      },
      {
        "title": "RDSUsuarios",
        "onExecute": "#RDSUsuarios",
        "icon": "rdsusuarios"
      },
      {
        "title": "RDSConteos",
        "onExecute": "#RDSConteos",
        "icon": "rdsconteos"
      },
      {
        "title": "RDSLogs",
        "onExecute": "#RDSLogs",
        "icon": "rdslogs"
      },
      {
        "title": "RDSTexts",
        "onExecute": "#RDSTexts",
        "icon": "rdstexts"
      },
      {
        "title": "RDSImages",
        "onExecute": "#RDSImages",
        "icon": "rdsimages"
      },
      {
        "title": "RDSResoluciones",
        "onExecute": "#RDSResoluciones",
        "icon": "rdsresoluciones"
      },
      {
        "title": "RDSResolucionesPlantillasHeader",
        "onExecute": "#RDSResolucionesPlantillasHeader",
        "icon": "rdsresolucionesplantillasheader"
      },
      {
        "title": "RDSResolucionesPlantillasDetail",
        "onExecute": "#RDSResolucionesPlantillasDetail",
        "icon": "rdsresolucionesplantillasdetail"
      },
      {
        "title": "RDSHistoricoSolicitudes",
        "onExecute": "#RDSHistoricoSolicitudes",
        "icon": "rdshistoricosolicitudes"
      },
      {
        "title": "RDSAlarms",
        "onExecute": "#RDSAlarms",
        "icon": "rdsalarms"
      },
      {
        "title": "RDSEvents",
        "onExecute": "#RDSEvents",
        "icon": "rdsevents"
      },
      {
        "title": "RDSConexionsHeader",
        "onExecute": "#RDSConexionsHeader",
        "icon": "rdsconexionsheader"
      },
      {
        "title": "RDSConectionsDetails",
        "onExecute": "#RDSConectionsDetails",
        "icon": "rdsconectionsdetails"
      },
      {
        "title": "RDSViabilidadCanalesOtorga",
        "onExecute": "#RDSViabilidadCanalesOtorga",
        "icon": "rdsviabilidadcanalesotorga"
      },
      {
        "title": "RDSViabilidadSolicitudesOtorga",
        "onExecute": "#RDSViabilidadSolicitudesOtorga",
        "icon": "rdsviabilidadsolicitudesotorga"
      },
      {
        "title": "RDSOtorgaAnalisisAdministrativo",
        "onExecute": "#RDSOtorgaAnalisisAdministrativo",
        "icon": "rdsotorgaanalisisadministrativo"
      },
      {
        "title": "RDSResolucionesViabilidad",
        "onExecute": "#RDSResolucionesViabilidad",
        "icon": "rdsresolucionesviabilidad"
      },
      {
        "title": "RDSOtorgaAnalisisTecnico",
        "onExecute": "#RDSOtorgaAnalisisTecnico",
        "icon": "rdsotorgaanalisistecnico"
      },
      {
        "title": "RDSRegistroUsuario",
        "onExecute": "#RDSRegistroUsuario",
        "icon": "rdsregistrousuario"
      },
      {
        "title": "RDSConfiguracionAperturaProcesos",
        "onExecute": "#RDSConfiguracionAperturaProcesos",
        "icon": "rdsconfiguracionaperturaprocesos"
      },
      {
        "title": "RDSConfigurarProceso",
        "onExecute": "#RDSConfigurarProceso",
        "icon": "rdsconfigurarproceso"
      },
      {
        "title": "RDSSolicitudOtorgaEmisora",
        "onExecute": "#RDSSolicitudOtorgaEmisora",
        "icon": "rdssolicitudotorgaemisora"
      },
      {
        "title": "RDSSolicitudOtorgaInteresPublico",
        "onExecute": "#RDSSolicitudOtorgaInteresPublico",
        "icon": "rdssolicitudotorgainterespublico"
      },
      {
        "title": "RDSSolicitudOtorgaComunitaria",
        "onExecute": "#RDSSolicitudOtorgaComunitaria",
        "icon": "rdssolicitudotorgacomunitaria"
      },
      {
        "title": "RDSSolicitudOtorgaComunitariaGruposEtnicos",
        "onExecute": "#RDSSolicitudOtorgaComunitariaGruposEtnicos",
        "icon": "rdssolicitudotorgacomunitariagruposetnicos"
      },
      {
        "title": "RDSFuncionarioSolicitudOtorgaGE",
        "onExecute": "#RDSFuncionarioSolicitudOtorgaGE",
        "icon": "rdsfuncionariosolicitudotorgage"
      },
      {
        "title": "RDSCalificarSolicitud",
        "onExecute": "#RDSCalificarSolicitud",
        "icon": "rdscalificarsolicitud"
      },
      {
        "title": "RDSConfiguracionAperturaObservaciones",
        "onExecute": "#RDSConfiguracionAperturaObservaciones",
        "icon": "rdsconfiguracionaperturaobservaciones"
      },
      {
        "title": "RDSAperturaObservaciones",
        "onExecute": "#RDSAperturaObservaciones",
        "icon": "rdsaperturaobservaciones"
      },
      {
        "title": "RDSObservaciones",
        "onExecute": "#RDSObservaciones",
        "icon": "rdsobservaciones"
      },
      {
        "title": "RDSRespuestaObservacion",
        "onExecute": "#RDSRespuestaObservacion",
        "icon": "rdsrespuestaobservacion"
      },
      {
        "title": "RDSInformeRespuestasObservaciones",
        "onExecute": "#RDSInformeRespuestasObservaciones",
        "icon": "rdsinformerespuestasobservaciones"
      },
      {
        "title": "RDSObservacionesPlantillasHeader",
        "onExecute": "#RDSObservacionesPlantillasHeader",
        "icon": "rdsobservacionesplantillasheader"
      },
      {
        "title": "RDSObservacionesPlantillasDetail",
        "onExecute": "#RDSObservacionesPlantillasDetail",
        "icon": "rdsobservacionesplantillasdetail"
      },
      {
        "title": "RDSCarteraConsecionario",
        "onExecute": "#RDSCarteraConsecionario",
        "icon": "rdscarteraconsecionario"
      }
    ],
    "commandMapping": {
      "generic-header-toolbar": {
        "commands": [
          {
            "id": "new",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "add",
              "text": "Adicionar"
            }
          },
          {
            "id": "refresh",
            "location": "after",
            "icon": "refresh",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "refresh",
              "text": "Refrescar"
            }
          },
          {
            "id": "calendar",
            "location": "after",
            "icon": "ta ta-calendar",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "ta ta-calendar",
              "text": "Per�odo"
            }
          },
          {
            "id": "firma",
            "location": "after",
            "icon": "ta ta-signature1",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "ta ta-signature1",
              "text": "Firma digital"
            }
          },
          {
            "id": "grafico",
            "location": "after",
            "icon": "ta ta-eye",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "ta ta-eye",
              "text": "Tipo gráfico"
            }
          },
          {
            "id": "save",
            "location": "after",
            "icon": "save",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "save",
              "text": "Guardar"
            }
          },
          {
            "id": "columnchooser",
            "location": "after",
            "icon": "save",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "columnchooser",
              "text": "Selección de columnas"
            }
          },
          {
            "id": "excel",
            "location": "after",
            "icon": "excel",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "excel",
              "text": "Excel"
            }
          },
          {
            "id": "instructivo",
            "location": "after",
            "icon": "",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "",
              "text": "Instructivo"
            }
          },
          {
            "id": "pdf",
            "location": "after",
            "icon": "ta ta-pdf",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "ta ta-pdf",
              "text": "pdf"
            }
          },
          {
            "id": "open",
            "location": "after",
            "icon": "ta ta-folder-open-o",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "ta ta-folder-open-o",
              "text": "Abrir"
            }
          },
          {
            "id": "menu",
            "location": "before",
            "icon": "menu"
          },
          {
            "id": "LogOut",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "Process",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "bugs",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "User",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "ChangePwd",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "filter",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "mode",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "Grid",
            "location": "after",
            "widget": "dxButton",
            "locateInMenu": "never"
          },
          {
            "id": "ViabilidadAutomatica",
            "location": "after",
            "icon": "'ta ta-lightbulb-o'",
            "widget": "dxButton",
            "locateInMenu": "never",
            "options": {
              "icon": "'ta ta-lightbulb-o'",
              "text": "Excel"
            }
          }
        ]
      }
    }
  }
});