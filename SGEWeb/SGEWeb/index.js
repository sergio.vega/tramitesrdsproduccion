﻿window.SGEWeb = window.SGEWeb || {};

$(function() {
    // Uncomment the line below to disable platform-specific look and feel and to use the Generic theme for all devices
    // DevExpress.devices.current({ platform: "generic" });
    // To customize the Generic theme, use the DevExtreme Theme Builder (http://js.devexpress.com/ThemeBuilder)
    // For details on how to use themes and the Theme Builder, refer to the http://js.devexpress.com/Documentation/Guide/#themes article

    DevExpress.devices.current({ platform: "generic" });

    if (DevExpress.devices.real().platform === "generic") {
        DevExpress.ui.themes.current('generic.light');
    }

    $(document).on("deviceready", function () {
        navigator.splashscreen.hide();
        $(document).on("backbutton", function () {
            DevExpress.processHardwareBackButton();
        });
    });

    function onNavigatingBack(e) {
        if (e.isHardwareButton && !SGEWeb.app.canBack()) {
            e.cancel = true;
            exitApp();
        }
        if (e.isHardwareButton) {
            e.cancel = true;
        }
    }

    function exitApp() {
        switch (DevExpress.devices.real().platform) {
            case "android":
                navigator.app.exitApp();
                break;
            case "win":
                MSApp.terminateApp('');
                break;
        }
    }



    SGEWeb.app = new DevExpress.framework.html.HtmlApplication({
        namespace: SGEWeb,
        layoutSet: DevExpress.framework.html.layoutSets[SGEWeb.config.layoutSet],
        navigation: SGEWeb.config.navigation,
        commandMapping: SGEWeb.config.commandMapping,
        viewCacheSize: 8
    });



    SGEWeb.app.on("navigating", function (e) {

        var params = SGEWeb.app.router.parse(e.uri),
            viewInfo = SGEWeb.app.getViewTemplateInfo(params.view);

        if (e.options.target == 'back') {
            for (var prop in SGEWeb.app.viewCache._viewCache._viewCache._cache) {
                if (SGEWeb.app.viewCache._viewCache._viewCache._cache[prop].previousViewInfo) {
                    if (SGEWeb.app.viewCache._viewCache._viewCache._cache[prop].previousViewInfo.viewName == viewInfo.name) {
                        if (SGEWeb.app.viewCache._viewCache._viewCache._cache[prop].viewName != 'RDSExpedientes') {
                            SGEWeb.app.viewCache.removeView(prop);
                        }
                    }
                }
            }

        }
    });


    //---------------------------------------------------------Configurar-----------------------------------------------------------------------------
    SGEWeb.app.Name = "SGE Web"; //PopUps, Alerts, etc
    SGEWeb.app.Version = '4.0.0.2A';
    SGEWeb.app.Nit = undefined;
    SGEWeb.app.IDUserWeb = -1;
    SGEWeb.app.User = undefined;
    SGEWeb.app.ModePruebas = true;

    
    SGEWeb.app.Ruta = '';
    SGEWeb.app.WS2 = 'http://localhost:54976';
    SGEWeb.app.RutaProxy = 'http://localhost/SGEProxy';
    SGEWeb.app.RutaProxy = 'http://localhost:53971/SGEProxy';
    SGEWeb.app.RutaLogs = 'http://localhost/SGELogs';

    
    //SGEWeb.app.Ruta = '..';
    //SGEWeb.app.WS2 = '/SGEWCF2';
    //SGEWeb.app.RutaProxy = '/SGEProxy';
    //SGEWeb.app.RutaLogs = '../SGELogs';

    //SGEWeb.app.Ruta = '..';
    //SGEWeb.app.WS2 = '/SGEWCF2';
    //SGEWeb.app.RutaProxy = '/TramitesRDS';
    //SGEWeb.app.RutaLogs = 'C:\SAGE\TramitesRDS\SGELogs';

    //---------------------------------------------------------Configurar-----------------------------------------------------------------------------
    SGEWeb.app.Token = undefined;
    SGEWeb.app.LOGIN_IN_INTERNAL = ko.observable(false);
    SGEWeb.app.NeedRefresh = false;
    SGEWeb.app.LOGGED_IN = ko.observable(false);
    SGEWeb.app.UserIcon = ko.observable('ta ta-empty');
    SGEWeb.app.CallBackActiveUser = undefined;
    SGEWeb.app.ConfiguracionProcesoOtorga = undefined;
    SGEWeb.app.TiposDocumentosOtorga = undefined;
    SGEWeb.app.Observaciones = undefined;
    SGEWeb.app.PlantillaObservacion = undefined;

    
    SGEWeb.app.DisabledToolBar = function (e) {
        setTimeout(function () {
            SGEWeb.app.DisabledToolBarVar(e);
        }, 100);
    }

    SGEWeb.app.DisabledToolBarVar = ko.observable(true);

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    SGEWeb.app.NewGuid = function () {
        return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
    }

    SGEWeb.app.SOL_TYPES = [];
    SGEWeb.app.Solicitud = undefined;
    SGEWeb.app.Expediente = undefined;
    SGEWeb.app.PlantillaResolucion = undefined;
    SGEWeb.app.CombosResolucion = undefined;
    SGEWeb.app.CombosObservaciones = undefined;
    SGEWeb.app.Users = [];
    SGEWeb.app.CallBackFunction = undefined;
    SGEWeb.app.PictureMain = SGEWeb.app.RutaProxy + '/SGEProxy/ImageHandler.axd?IMG_ID=PICTURE_MAIN&NOCACHE=' + SGEWeb.app.NewGuid();

    SGEWeb.app.COM_TYPE = [
        { value: 0, text: 'Sin definir', Icon: 'ta ta-empty ta-lg', Color: 'transparent', Cursor: 'default' },
        { value: 1, text: 'Aprobación', Icon: 'ta ta-aprobar ta-lg', Color: 'Green', Cursor: 'default' },
        { value: 2, text: 'Requerimiento', Icon: 'ta ta-requerir ta-lg', Color: 'Orange', Cursor: 'default' },
        { value: 3, text: 'Rechazo', Icon: 'ta ta-rechazar ta-lg', Color: '#D00000', Cursor: 'default' },
        { value: 4, text: 'Desistimiento', Icon: 'ta ta-rechazar0 ta-lg', Color: '#D00000', Cursor: 'default' }
    ];

    SGEWeb.app.STATUS = [
        { value: 1, text: 'Activo' },
        { value: 2, text: 'Inactivo' },
    ];

    SGEWeb.app.ASIGNABLE = [
        { value: 0, text: 'No Aplica' },
        { value: 1, text: 'Asignable' },
        { value: 2, text: 'No asignable' },
    ];

    SGEWeb.app.BOOL = [
        { value: 0, text: 'No' },
        { value: 1, text: 'Si' },
    ];

    SGEWeb.app.DOC_CLASS = [
        { value: 1, text: 'Radicado' },
        { value: 2, text: 'Registro' }
    ];

    SGEWeb.app.DOC_SOURCE = [
        { value: 0, text: 'Concesionario' },
        { value: 1, text: 'MinTIC' },
        { value: 2, text: 'ANE' }
    ];

    SGEWeb.app.CRUD = [
        { value: 0, text: 'NA' },
        { value: 1, text: 'Adición' },
        { value: 2, text: 'Lectura' },
        { value: 3, text: 'Edición' },
        { value: 4, text: 'Eliminación' }
    ];

    SGEWeb.app.GROUP = [
        { value: 0, text: 'Concesionario' },
        { value: 1, text: 'MinTIC' },
        { value: 2, text: 'ANE' },
        { value: 3, text: 'Administrador' },
    ];

    SGEWeb.app.STEP = [
        { value: -3, text: 'Es viable', TextLong: 'Resolución de viabilidad' },
        { value: -2, text: 'Viabilidad', TextLong: 'Análisis de viabilidad' },
        { value: 0, text: 'Administrativo', TextLong: 'Análisis administrativo' },
        { value: 1, text: 'Técnico', TextLong: 'Análisis técnico' },
        { value: 2, text: 'Resolución', TextLong: 'Resolución' },
        { value: 3, text: 'En cierre', TextLong: 'Solicitud en cierre' },
        { value: 4, text: 'Terminada', TextLong: 'Solicitud terminada' }
    ];

    SGEWeb.app.ALARM = [
        { value: 0, text: 'No aplica', Icon: 'ta ta-semaforo', textsingle: 'No aplica' },
        { value: 1, text: 'En curso', Icon: 'ta ta-semaforo-green', textsingle: 'En curso' },
        { value: 2, text: 'Por vencer', Icon: 'ta ta-semaforo-yellow', textsingle: 'Por vencer' },
        { value: 3, text: 'Vencidas', Icon: 'ta ta-semaforo-red', textsingle: 'Vencida' },
        { value: 4, text: 'En cierre', Icon: 'ta ta-semaforo-blue', textsingle: 'En cierre' },
        { value: 5, text: 'Terminadas', Icon: 'ta ta-semaforo-gray', textsingle: 'Terminada' },
    ];

    SGEWeb.app.ALARM_REQUIRED = [
        { value: 0, text: 'En curso', Icon: 'ta ta-semaforo-green', textsingle: 'En curso' },
        { value: 1, text: 'Requeridas', Icon: 'ta ta-semaforo-yellow', textsingle: 'Requerida' },
        { value: 2, text: 'Por vencer', Icon: 'ta ta-semaforo-red', textsingle: 'Por vencer' },
        { value: 3, text: 'Vencidas', Icon: 'ta ta-semaforo-red', textsingle: 'Vencida' },
        { value: 4, text: 'En cierre', Icon: 'ta ta-semaforo-blue', textsingle: 'En cierre' },
        { value: 5, text: 'Terminadas', Icon: 'ta ta-semaforo-gray', textsingle: 'Terminada' },
    ];

    SGEWeb.app.OS = [
        { value: 'Windows', text: 'Windows', Icon: 'ta ta-windows'},
        { value: 'Macintosh', text: 'Macintosh', Icon: 'ta ta-apple' },
        { value: 'iPad', text: 'iPad', Icon: 'ta ta-iphone' },
        { value: 'iPhone', text: 'iPhone', Icon: 'ta ta-iphone'},
        { value: 'Android', text: 'Android', Icon: 'ta ta-android' },
        { value: 'NA', text: 'NA', Icon: 'ta ta-laptop'},
    ];

    SGEWeb.app.BROWSER = [
        { value: 'Edge', text: 'Edge', Icon: 'ta ta-edge' },
        { value: 'Opera', text: 'Opera', Icon: 'ta ta-opera' },
        { value: 'IE', text: 'Internet Explorer', Icon: 'ta ta-explorer' },
        { value: 'Firefox', text: 'Firefox', Icon: 'ta ta-firefox' },
        { value: 'Chrome', text: 'Chrome', Icon: 'ta ta-chrome' },
        { value: 'Safari', text: 'Safari', Icon: 'ta ta-safari' },
        { value: 'Safari', text: 'Safari', Icon: 'ta ta-safari' },
        { value: 'Nativo', text: 'Nativo', Icon: 'ta ta-compass2' },
        { value: 'NA', text: 'NA', Icon: 'ta ta-compass2' },
    ];

    SGEWeb.app.ROLE = [
        { value: 0, text: 'Concesionario', Icon: 'ta ta-antena2' },
        { value: 1, text: 'Funcionario', ControlCambioID: 1, Icon: 'ta ta-funcionario' },
        { value: 2, text: 'Revisor', ControlCambioID: 2, Icon: 'ta ta-revisor' },
        { value: 4, text: 'Coordinador', ControlCambioID: 3, Icon: 'ta ta-coordinador' },
        { value: 8, text: 'Subdirector', ControlCambioID: 4, Icon: 'ta ta-subdirector' },
        { value: 16, text: 'Asesor', ControlCambioID: 5, Icon: 'ta ta-asesor' },
        { value: 32, text: 'Director', ControlCambioID: 6, Icon: 'ta ta-director' },
        { value: 128, text: 'Administrador', ControlCambioID: 7, Icon: 'ta ta-administrator' }
    ];

    SGEWeb.app.DOC_TYPES = [
        { value: 0, text: 'Solicitud' },
        { value: 1, text: 'Solicitud' },
        { value: 2, text: 'Subsanación Admin.' },
        { value: 3, text: 'Requermiento Admin.' },
        { value: 4, text: 'Rechazo Admin.' },
        { value: 5, text: 'Aprobación Admin.' },
        { value: 6, text: 'Cancelación' },
        { value: 7, text: 'Devolución Técnica' },
        { value: 8, text: 'Subsanación Técnica' },
        { value: 9, text: 'Requermiento Técnico' },
        { value: 10, text: 'Rechazo Técnico' },
        { value: 11, text: 'Aprobación Técnica' },
        { value: 12, text: 'Aplazamiento requerimiento' },
        { value: 13, text: 'Desistimiento' }
    ];

    SGEWeb.app.DOC_TECH_TYPE = [
        { ID: 0, Text: 'Sin definir' },
        { value: 1, text: 'Soporte técnico' },
        { value: 2, text: 'Concepto técnico' },
        { value: 3, text: 'Cuadro técnico' }
    ];

    SGEWeb.app.FOOTER =
        {
            Parrafo1: ko.observable('Ministerio de Tecnologías de la Información y las Comunicaciones'),
            Parrafo2: ko.observable('Edificio Murillo Toro Cra. 8a entre calles 12 y 13, Bogotá, Colombia - Código Postal 111711'),
            Parrafo3: ko.observable('Teléfono Conmutador: +57(1) 344 34 60 - Línea Gratuita: 01-800-0914014'),
            Parrafo4: ko.observable('Línea Anticorrupción: 01-800-0912667 - Correo Institucional: minticresponde@mintic.gov.co'),
            Parrafo5: ko.observable('Notificaciones judiciales - Correo Institucional: notificacionesjudicialesmintic@mintic.gov.co'),
            Parrafo6: ko.observable('Horario de Atención: Lunes a Viernes 8:30 am - 4:30 pm.')
        }

    SGEWeb.app.months = ['Jan', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];


    SGEWeb.app.ReLogin = function () {
        for (var prop in SGEWeb.app.viewCache._viewCache._viewCache._cache) {
            SGEWeb.app.viewCache.removeView(prop);
        }

        for (var prop in SGEWeb.app.navigationManager.navigationStacks) {
            var nItems = SGEWeb.app.navigationManager.navigationStacks[prop].items.length;
            for (i = 1; i < nItems; i++) {
                SGEWeb.app.navigationManager.navigationStacks[prop].items.pop();
            }
            SGEWeb.app.navigationManager.navigationStacks[prop].currentIndex = 0;
        }

        SGEWeb.app.IDUserWeb = -1;
        SGEWeb.app.Token = undefined;
        SGEWeb.app.LOGGED_IN(false);

        SGEWeb.app.navigate("RDSPrincipal", { root: true, target: 'current', clearHistory: true });
    }


    DevExpress.config({
        floatingActionButtonConfig: {
            shading: true,
            position: { at: 'right bottom', my: 'right bottom', offset: '-16 -74' }

        }
    });

    SGEWeb.app.router.register(":view/:id", { view: "RDSPrincipal", id: undefined });
    SGEWeb.app.on("navigatingBack", onNavigatingBack);
    SGEWeb.app.navigate();

    SGEWeb.app.on("initialized", function () {
        SGEWeb.app.LOGGED_IN(false);
    });

});
