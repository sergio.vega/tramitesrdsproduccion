﻿CKEDITOR.plugins.add('ComboInfoObsConvocatoria',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoObsConvocatoria',
		{
            label: 'INFORMACIÓN CONVOCATORIA',
            title: 'INFORMACIÓN CONVOCATORIA',
            voiceLabel: 'INFORMACIÓN CONVOCATORIA',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {		        
		        var self = this;
                editor.config.InfoObsConvocatoria.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});