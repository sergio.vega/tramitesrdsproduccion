﻿CKEDITOR.plugins.setLang( 'lite', 'es', {
	TOGGLE_TRACKING: "Toggle Tracking Changes",
	TOGGLE_SHOW: "Toggle Tracking Changes",
	ACCEPT_ALL: "Aceptar todos los cambios",
	REJECT_ALL: "Rechazar todos los cambios",
	ACCEPT_ONE: "Aceptar cambio",
	REJECT_ONE: "Rechazar cambio",
	START_TRACKING: "Iniciar control de cambios",
	STOP_TRACKING: "Detener control de cambios",
	PENDING_CHANGES: "El documento contiene cambios pendientes.\n Favor resolverlos antes de detener el control de cambios.",
	HIDE_TRACKED: "Ocultar cambios",
	SHOW_TRACKED: "Mostrar cambios",
	CHANGE_TYPE_ADDED: "Adicionado",
	CHANGE_TYPE_DELETED: "Eliminado",
	MONTHS: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
	NOW: "Ahora",
	MINUTE_AGO: "hace 1 minuto",
	MINUTES_AGO: "xMinutes Minutos",
	BY: "por",
	ON: "en",
	AT: "en",
	LITE_LABELS_DATE: function(day, month, year)
	{
		if(typeof(year) != 'undefined') {
			year = ", " + year;
		}
		else {
			year = "";
		}
		return this.MONTHS[month] + " " + day + year;
	}
});
