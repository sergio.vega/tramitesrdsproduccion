﻿CKEDITOR.plugins.add('ComboInfoAntecedentes',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoAntecedentes',
		{
		    label: 'INFORMACIÓN ANTECEDENTES',
		    title: 'INFORMACIÓN ANTECEDENTES',
		    voiceLabel: 'INFORMACIÓN ANTECEDENTES',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {
		        var self = this;
		        editor.config.Antecedentes.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});