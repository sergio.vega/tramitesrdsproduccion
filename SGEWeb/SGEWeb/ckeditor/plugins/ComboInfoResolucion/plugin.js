﻿CKEDITOR.plugins.add('ComboInfoResolucion',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoResolucion',
		{
		    label: 'INFORMACIÓN RESOLUCIÓN',
		    title: 'INFORMACIÓN RESOLUCIÓN',
		    voiceLabel: 'INFORMACIÓN RESOLUCIÓN',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {
		        var self = this;
		        editor.config.Resolucion.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});