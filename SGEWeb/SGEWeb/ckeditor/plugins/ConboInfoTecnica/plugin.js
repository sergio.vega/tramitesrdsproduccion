﻿CKEDITOR.plugins.add('ConboInfoTecnica',
{
    requires: ['richcombo'],
    init: function (editor) {
        var strings_infoConcesionario = [];
        
        editor.ui.addRichCombo('ConboInfoTecnica',
		{
		    label: 'INFORMACIÓN TÉCNICA',
		    title: 'INFORMACIÓN TÉCNICA',
		    voiceLabel: 'INFORMACIÓN TÉCNICA',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {
		        var self = this;
		        editor.config.Tecnica.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});