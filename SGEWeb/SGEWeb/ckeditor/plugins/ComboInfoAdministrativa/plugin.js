﻿CKEDITOR.plugins.add('ComboInfoAdministrativa',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoAdministrativa',
		{
		    label: 'INFORMACIÓN ADMINISTRATIVA',
		    title: 'INFORMACIÓN ADMINISTRATIVA',
		    voiceLabel: 'INFORMACIÓN ADMINISTRATIVA',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {		        
		        var self = this;
		        editor.config.Administrativo.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});