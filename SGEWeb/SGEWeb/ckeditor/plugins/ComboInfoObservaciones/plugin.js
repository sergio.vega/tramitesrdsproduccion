﻿CKEDITOR.plugins.add('ComboInfoObservaciones',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoObservaciones',
		{
            label: 'INFORMACIÓN OBSERVACIONES',
            title: 'INFORMACIÓN OBSERVACIONES',
            voiceLabel: 'INFORMACIÓN OBSERVACIONES',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {		        
		        var self = this;
                editor.config.InfoObservaciones.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});