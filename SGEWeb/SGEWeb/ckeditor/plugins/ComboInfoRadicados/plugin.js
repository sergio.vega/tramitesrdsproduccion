﻿CKEDITOR.plugins.add('ComboInfoRadicados',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoRadicados',
		{
		    label: 'INFORMACIÓN RADICADOS',
		    title: 'INFORMACIÓN RADICADOS',
		    voiceLabel: 'INFORMACIÓN RADICADOS',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {
		        var self = this;
		        editor.config.Radicados.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});