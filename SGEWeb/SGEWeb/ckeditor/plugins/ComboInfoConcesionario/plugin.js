﻿CKEDITOR.plugins.add('ComboInfoConcesionario',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoConcesionario',
		{
		    label: 'INFORMACIÓN CONCESIONARIO',
		    title: 'INFORMACIÓN CONCESIONARIO',
		    voiceLabel: 'INFORMACIÓN CONCESIONARIO',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {
		        var self = this;
		        editor.config.Concesionario.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});