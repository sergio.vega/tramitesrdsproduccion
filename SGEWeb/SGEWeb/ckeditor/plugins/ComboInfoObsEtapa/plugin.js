﻿CKEDITOR.plugins.add('ComboInfoObsEtapa',
{
    requires: ['richcombo'],
    init: function (editor) {
        
        editor.ui.addRichCombo('ComboInfoObsEtapa',
		{
            label: 'INFORMACIÓN ETAPA',
            title: 'INFORMACIÓN ETAPA',
            voiceLabel: 'INFORMACIÓN ETAPA',
		    class: 'mergefields',
		    className: 'mergefields',
		    multiSelect: false,
		    panel:
			{
			    css: [editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
			    voiceLabel: editor.lang.panelVoiceLabel
			},

		    init: function () {		        
		        var self = this;
                editor.config.InfoObsEtapa.forEach(function (entry) {
		            self.add(entry.key, entry.name, null);
		        });
		    },

		    onClick: function (value) {
		        editor.focus();
		        editor.fire('saveSnapshot');
		        editor.insertHtml(value);
		        editor.fire('saveSnapshot');
		    }
		});


    }
});