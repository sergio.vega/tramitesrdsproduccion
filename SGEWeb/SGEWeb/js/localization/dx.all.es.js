﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function (require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function (message) {
    message.load({
        es: {
            Yes: "Si",
            No: "No",
            Cancel: "Cancelar",
            Clear: "Limpiar",
            Done: "Hecho",
            Loading: "Cargando...",
            Select: "Seleccionar...",
            Search: "Buscar...",
            Back: "Atrás",
            OK: "OK"
        }
    })
}));
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function (require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function (message) {
    message.load({
        es: {
            "dxList-pageLoadingText": "Cargando...",
            "dxList-nextButtonText": "Siguiente",
            "dxList-selectAll": "Seleccionar todos"
        }
    })
}));
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function (require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function (message) { }));
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(function (require, exports, module) {
            factory(require("../message"))
        })
    }
    else {
        factory(DevExpress.localization.message)
    }
}(this, function (message) {
    message.load({
        es: {
            "dxScheduler-editorLabelTitle": "Asunto",
            "dxScheduler-editorLabelStartDate": "Fecha Inicial",
            "dxScheduler-editorLabelEndDate": "Fecha Final",
            "dxScheduler-editorLabelDescription": "Descripción",
            "dxScheduler-editorLabelRecurrence": "Repetir",
            "dxScheduler-openAppointment": "Abrir Cita",
            "dxScheduler-recurrenceNever": "Nunca",
            "dxScheduler-recurrenceDaily": "Diario",
            "dxScheduler-recurrenceWeekly": "Semanal",
            "dxScheduler-recurrenceMonthly": "Mensual",
            "dxScheduler-recurrenceYearly": "Anual",
            "dxScheduler-recurrenceEvery": "Cada",
            "dxScheduler-recurrenceEnd": "Terminar repeticion",
            "dxScheduler-recurrenceAfter": "Hasta",
            "dxScheduler-recurrenceOn": "El",
            "dxScheduler-recurrenceRepeatDaily": "dia(s)",
            "dxScheduler-recurrenceRepeatWeekly": "semana(s)",
            "dxScheduler-recurrenceRepeatMonthly": "mes(es)",
            "dxScheduler-recurrenceRepeatYearly": "a\u00f1o(s)",
            "dxScheduler-switcherDay": "Dia",
            "dxScheduler-switcherWeek": "Semana",
            "dxScheduler-switcherWorkWeek": "Semana laboral",
            "dxScheduler-switcherMonth": "Mes",
            "dxScheduler-switcherAgenda": "Agenda",
            "dxScheduler-switcherTimelineDay": "Timeline Day",
            "dxScheduler-switcherTimelineWeek": "Timeline Week",
            "dxScheduler-switcherTimelineWorkWeek": "Timeline Work week",
            "dxScheduler-switcherTimelineMonth": "Timeline Month",
            "dxScheduler-recurrenceRepeatOnDate": "en la fecha",
            "dxScheduler-recurrenceRepeatCount": "veces",
            "dxScheduler-allDay": "Todo el día",
            "dxScheduler-confirmRecurrenceEditMessage": "Desea editar solo esta cita o toda la series?",
            "dxScheduler-confirmRecurrenceDeleteMessage": "Desea eliminar solo esta cita o toda la series?",
            "dxScheduler-confirmRecurrenceEditSeries": "Editar series",
            "dxScheduler-confirmRecurrenceDeleteSeries": "Eliminar series",
            "dxScheduler-confirmRecurrenceEditOccurrence": "Editar cita",
            "dxScheduler-confirmRecurrenceDeleteOccurrence": "Eliminar cita",
            "dxScheduler-noTimezoneTitle": "Sin zona horaria",
        }
    })
}));