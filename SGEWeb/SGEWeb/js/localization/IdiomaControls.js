﻿
var Idiomasupplemental = {
    supplemental: {
        version: {
            _cldrVersion: "28",
            _unicodeVersion: "8.0.0",
            _number: "$Revision: 11965 $"
        },
        likelySubtags: {
            en: "en-Latn-US",
            es: "es-Latn-CO",
            us: "us-Latn-US",
            pt: "pt-Latn-BR"
        }
    }
};


function getIdiomaDates(locale) {
    var IdiomaDates = {
        main: {
            xx: {
                dates: {
                    calendars: {
                        gregorian: {
                            months: {
                                format: {
                                    abbreviated: {
                                        1: 'Ene',
                                        2: 'Feb',
                                        3: 'Mar',
                                        4: 'Abr',
                                        5: 'May',
                                        6: 'Jun',
                                        7: 'Jul',
                                        8: 'Ago',
                                        9: 'Sep',
                                        10: 'Oct',
                                        11: 'Nov',
                                        12: 'Dic'
                                    },
                                    narrow: {
                                        1: 'E',
                                        2: 'F',
                                        3: 'M',
                                        4: 'A',
                                        5: 'M',
                                        6: 'J',
                                        7: 'J',
                                        8: 'A',
                                        9: 'S',
                                        10: 'O',
                                        11: 'N',
                                        12: 'D'
                                    },
                                    wide: {
                                        1: 'Enero',
                                        2: 'Febrero',
                                        3: 'Marzo',
                                        4: 'Abril',
                                        5: 'Mayo',
                                        6: 'Junio',
                                        7: 'Julio',
                                        8: 'Agosto',
                                        9: 'Septiembre',
                                        10: 'Octubre',
                                        11: 'Noviembre',
                                        12: 'Diciembre'
                                    }
                                },
                                "stand-alone": {
                                    abbreviated: {
                                        1: 'Ene',
                                        2: 'Feb',
                                        3: 'Mar',
                                        4: 'Abr',
                                        5: 'May',
                                        6: 'Jun',
                                        7: 'Jul',
                                        8: 'Ago',
                                        9: 'Sep',
                                        10: 'Oct',
                                        11: 'Nov',
                                        12: 'Dic'
                                    },
                                    narrow: {
                                        1: 'E',
                                        2: 'F',
                                        3: 'M',
                                        4: 'A',
                                        5: 'M',
                                        6: 'J',
                                        7: 'J',
                                        8: 'A',
                                        9: 'S',
                                        10: 'O',
                                        11: 'N',
                                        12: 'D'
                                    },
                                    wide: {
                                        1: 'Enero',
                                        2: 'Febrero',
                                        3: 'Marzo',
                                        4: 'Abril',
                                        5: 'Mayo',
                                        6: 'Junio',
                                        7: 'Julio',
                                        8: 'Agosto',
                                        9: 'Septiembre',
                                        10: 'Octubre',
                                        11: 'Noviembre',
                                        12: 'Diciembre'
                                    }
                                }
                            },
                            days: {
                                format: {
                                    abbreviated: {
                                        sun: 'Dom',
                                        mon: 'Lun',
                                        tue: 'Mar',
                                        wed: 'Mié',
                                        thu: 'Jue',
                                        fri: 'Vie',
                                        sat: 'Sáb'
                                    },
                                    narrow: {
                                        sun: 'D',
                                        mon: 'L',
                                        tue: 'M',
                                        wed: 'X',
                                        thu: 'J',
                                        fri: 'V',
                                        sat: 'S'
                                    },
                                    short: {
                                        sun: 'DO',
                                        mon: 'LU',
                                        tue: 'MA',
                                        wed: 'MI',
                                        thu: 'JU',
                                        fri: 'VI',
                                        sat: 'SÁ'
                                    },
                                    wide: {
                                        sun: 'Domingo',
                                        mon: 'Lunes',
                                        tue: 'Martes',
                                        wed: 'Miércoles',
                                        thu: 'Jueves',
                                        fri: 'Viernes',
                                        sat: 'Sábado'
                                    }
                                },
                                "stand-alone": {
                                    abbreviated: {
                                        sun: 'Dom',
                                        mon: 'Lun',
                                        tue: 'Mar',
                                        wed: 'Mié',
                                        thu: 'Jue',
                                        fri: 'Vie',
                                        sat: 'Sáb'
                                    },
                                    narrow: {
                                        sun: 'D',
                                        mon: 'L',
                                        tue: 'M',
                                        wed: 'X',
                                        thu: 'J',
                                        fri: 'V',
                                        sat: 'S'
                                    },
                                    short: {
                                        sun: 'DO',
                                        mon: 'LU',
                                        tue: 'MA',
                                        wed: 'MI',
                                        thu: 'JU',
                                        fri: 'VI',
                                        sat: 'SÁ'
                                    },
                                    wide: {
                                        sun: 'Domingo',
                                        mon: 'Lunes',
                                        tue: 'Martes',
                                        wed: 'Miércoles',
                                        thu: 'Jueves',
                                        fri: 'Viernes',
                                        sat: 'Sábado'
                                    }
                                }
                            },
                            quarters: {
                                format: {
                                    abbreviated: {
                                        1: 'T1',
                                        2: 'T2',
                                        3: 'T3',
                                        4: 'T4'
                                    },
                                    narrow: {
                                        1: "1",
                                        2: "2",
                                        3: "3",
                                        4: "4"
                                    },
                                    wide: {
                                        1: '1º trimestre',
                                        2: '2º trimestre',
                                        3: '3º trimestre',
                                        4: '4º trimestre'
                                    }
                                },
                                "stand-alone": {
                                    abbreviated: {
                                        1: 'T1',
                                        2: 'T2',
                                        3: 'T3',
                                        4: 'T4'
                                    },
                                    narrow: {
                                        1: "1",
                                        2: "2",
                                        3: "3",
                                        4: "4"
                                    },
                                    wide: {
                                        1: '1º trimestre',
                                        2: '2º trimestre',
                                        3: '3º trimestre',
                                        4: '4º trimestre'
                                    }
                                }
                            },
                            dayPeriods: {
                                format: {
                                    abbreviated: {
                                        midnight: "midnight",
                                        am: 'a. m.',
                                        "am-alt-variant": 'am',
                                        noon: "noon",
                                        pm: 'p. m.',
                                        "pm-alt-variant": 'pm',
                                        morning1: "in the morning",
                                        afternoon1: "in the afternoon",
                                        evening1: "in the evening",
                                        night1: "at night"
                                    },
                                    narrow: {
                                        midnight: "mi",
                                        am: 'a',
                                        "am-alt-variant": 'am',
                                        noon: "n",
                                        pm: 'p',
                                        "pm-alt-variant": 'pm',
                                        morning1: "in the morning",
                                        afternoon1: "in the afternoon",
                                        evening1: "in the evening",
                                        night1: "at night"
                                    },
                                    wide: {
                                        midnight: "midnight",
                                        am: 'a. m.',
                                        "am-alt-variant": 'am',
                                        noon: "noon",
                                        pm: 'p. m.',
                                        "pm-alt-variant": 'pm',
                                        morning1: "in the morning",
                                        afternoon1: "in the afternoon",
                                        evening1: "in the evening",
                                        night1: "at night"
                                    }
                                },
                                "stand-alone": {
                                    abbreviated: {
                                        midnight: "midnight",
                                        am: 'a. m.',
                                        "am-alt-variant": 'am',
                                        noon: "noon",
                                        pm: 'p. m.',
                                        "pm-alt-variant": 'pm',
                                        morning1: "in the morning",
                                        afternoon1: "in the afternoon",
                                        evening1: "in the evening",
                                        night: "at night"
                                    },
                                    narrow: {
                                        midnight: "midnight",
                                        am: 'a. m.',
                                        "am-alt-variant": 'am',
                                        noon: "noon",
                                        pm: 'p. m.',
                                        "pm-alt-variant": 'pm',
                                        morning1: "in the morning",
                                        afternoon1: "in the afternoon",
                                        evening1: "in the evening",
                                        night1: "at night"
                                    },
                                    wide: {
                                        midnight: "midnight",
                                        am: 'a. m.',
                                        "am-alt-variant": 'am',
                                        noon: "noon",
                                        pm: 'p. m.',
                                        "pm-alt-variant": 'pm',
                                        morning1: "morning",
                                        afternoon1: "afternoon",
                                        evening1: "evening",
                                        night1: "night"
                                    }
                                }
                            },
                            eras: {
                                eraNames: {
                                    0: "Before Christ",
                                    1: "Anno Domini",
                                    "0-alt-variant": "Before Common Era",
                                    "1-alt-variant": "Common Era"
                                },
                                eraAbbr: {
                                    0: "BC",
                                    1: "AD",
                                    "0-alt-variant": "BCE",
                                    "1-alt-variant": "CE"
                                },
                                eraNarrow: {
                                    0: "B",
                                    1: "A",
                                    "0-alt-variant": "BCE",
                                    "1-alt-variant": "CE"
                                }
                            },
                            dateFormats: {
                                full: "EEEE, MMMM d, y",
                                long: "MMMM d, y",
                                medium: "MMM d, y",
                                short: "M/d/yy"
                            },
                            timeFormats: {
                                full: "H:mm:ss (zzzz)",
                                long: "H:mm:ss z",
                                medium: "H:mm:ss",
                                short: "H:mm",
                            },
                            dateTimeFormats: {
                                full: "{1}, {0}",
                                long: "{1}, {0}",
                                medium: "{1} {0}",
                                short: "{1} {0}",
                                availableFormats: {
                                    d: "d",
                                    E: "ccc",
                                    Ed: "d E",
                                    Ehm: "E h:mm a",
                                    EHm: "E HH:mm",
                                    Ehms: "E h:mm:ss a",
                                    EHms: "E HH:mm:ss",
                                    Gy: "y G",
                                    GyMMM: "MMM y G",
                                    GyMMMd: "MMM d, y G",
                                    GyMMMEd: "E, MMM d, y G",
                                    h: "h a",
                                    H: "HH",
                                    hm: "h:mm a",
                                    Hm: "HH:mm",
                                    hms: "h:mm:ss a",
                                    Hms: "HH:mm:ss",
                                    hmsv: "h:mm:ss a v",
                                    Hmsv: "HH:mm:ss v",
                                    hmv: "h:mm a v",
                                    Hmv: "HH:mm v",
                                    M: "L",
                                    Md: "M/d",
                                    MEd: "E, M/d",
                                    MMM: "LLL",
                                    MMMd: "MMM d",
                                    MMMEd: "E, MMM d",
                                    MMMMd: "MMMM d",
                                    ms: "mm:ss",
                                    y: "y",
                                    yM: "M/y",
                                    yMd: "M/d/y",
                                    yMEd: "E, M/d/y",
                                    yMMM: "MMM y",
                                    yMMMd: "MMM d, y",
                                    yMMMEd: "E, MMM d, y",
                                    yMMMM: "MMMM y",
                                    yQQQ: "QQQ y",
                                    yQQQQ: "QQQQ y"
                                }
                            }
                        }
                    }
                },
                numbers: {
                    defaultNumberingSystem: "latn",
                    otherNumberingSystems: {
                        native: "latn"
                    },
                    minimumGroupingDigits: "2",
                    "symbols-numberSystem-latn": {
                        decimal: ".",
                        group: ",",
                        list: ";",
                        percentSign: "%",
                        plusSign: "+",
                        minusSign: "-",
                        exponential: "E",
                        superscriptingExponent: "×",
                        perMille: "‰",
                        infinity: "∞",
                        nan: "NaN",
                        timeSeparator: ":"
                    },
                    "decimalFormats-numberSystem-latn": {
                        standard: "#,##0.###"
                    },
                    "scientificFormats-numberSystem-latn": {
                        standard: "#E0"
                    },
                    "percentFormats-numberSystem-latn": {
                        standard: "#,##0 %"
                    },
                    "currencyFormats-numberSystem-latn": {
                        currencySpacing: {
                            beforeCurrency: {
                                currencyMatch: "[:^S:]",
                                surroundingMatch: "[:digit:]",
                                insertBetween: " "
                            },
                            afterCurrency: {
                                currencyMatch: "[:^S:]",
                                surroundingMatch: "[:digit:]",
                                insertBetween: " "
                            }
                        },
                        standard: "¤#,##0.00",
                        accounting: "¤#,##0.00;(¤#,##0.00)"

                    },
                    currencies: {
                        ADP: {
                            symbol: "ADP"
                        },
                        AED: {
                            symbol: "AED"
                        },
                        AFA: {
                            symbol: "AFA"
                        },
                        AFN: {
                            symbol: "AFN"
                        },
                        ALL: {
                            symbol: "ALL"
                        },
                        AMD: {
                            symbol: "AMD"
                        },
                        ANG: {
                            symbol: "ANG"
                        },
                        AOA: {
                            symbol: "AOA",
                            "symbol-alt-narrow": "Kz"
                        },
                        AOK: {
                            symbol: "AOK"
                        },
                        AON: {
                            symbol: "AON"
                        },
                        AOR: {
                            symbol: "AOR"
                        },
                        ARA: {
                            symbol: "ARA"
                        },
                        ARL: {
                            symbol: "ARL"
                        },
                        ARM: {
                            symbol: "ARM"
                        },
                        ARP: {
                            symbol: "ARP"
                        },
                        ARS: {
                            symbol: "ARS",
                            "symbol-alt-narrow": "$"
                        },
                        ATS: {
                            symbol: "ATS"
                        },
                        AUD: {
                            symbol: "AUD",
                            "symbol-alt-narrow": "$"
                        },
                        AWG: {
                            symbol: "AWG"
                        },
                        AZM: {
                            symbol: "AZM"
                        },
                        AZN: {
                            symbol: "AZN"
                        },
                        BAD: {
                            symbol: "BAD"
                        },
                        BAM: {
                            symbol: "BAM",
                            "symbol-alt-narrow": "KM"
                        },
                        BAN: {
                            symbol: "BAN"
                        },
                        BBD: {
                            symbol: "BBD",
                            "symbol-alt-narrow": "$"
                        },
                        BDT: {
                            symbol: "BDT",
                            "symbol-alt-narrow": "৳"
                        },
                        BEC: {
                            symbol: "BEC"
                        },
                        BEF: {
                            symbol: "BEF"
                        },
                        BEL: {
                            symbol: "BEL"
                        },
                        BGL: {
                            symbol: "BGL"
                        },
                        BGM: {
                            symbol: "BGM"
                        },
                        BGN: {
                            symbol: "BGN"
                        },
                        BGO: {
                            symbol: "BGO"
                        },
                        BHD: {
                            symbol: "BHD"
                        },
                        BIF: {
                            symbol: "BIF"
                        },
                        BMD: {
                            symbol: "BMD",
                            "symbol-alt-narrow": "$"
                        },
                        BND: {
                            symbol: "BND",
                            "symbol-alt-narrow": "$"
                        },
                        BOB: {
                            symbol: "BOB",
                            "symbol-alt-narrow": "Bs"
                        },
                        BOL: {
                            symbol: "BOL"
                        },
                        BOP: {
                            symbol: "BOP"
                        },
                        BOV: {
                            symbol: "BOV"
                        },
                        BRB: {
                            symbol: "BRB"
                        },
                        BRC: {
                            symbol: "BRC"
                        },
                        BRE: {
                            symbol: "BRE"
                        },
                        BRL: {
                            symbol: "BRL",
                            "symbol-alt-narrow": "R$"
                        },
                        BRN: {
                            symbol: "BRN"
                        },
                        BRR: {
                            symbol: "BRR"
                        },
                        BRZ: {
                            symbol: "BRZ"
                        },
                        BSD: {
                            symbol: "BSD",
                            "symbol-alt-narrow": "$"
                        },
                        BTN: {
                            symbol: "BTN"
                        },
                        BUK: {
                            symbol: "BUK"
                        },
                        BWP: {
                            symbol: "BWP",
                            "symbol-alt-narrow": "P"
                        },
                        BYB: {
                            symbol: "BYB"
                        },
                        BYR: {
                            symbol: "BYR",
                            "symbol-alt-narrow": "р."
                        },
                        BZD: {
                            symbol: "BZD",
                            "symbol-alt-narrow": "$"
                        },
                        CAD: {
                            symbol: "CA$",
                            "symbol-alt-narrow": "$"
                        },
                        CDF: {
                            symbol: "CDF"
                        },
                        CHE: {
                            symbol: "CHE"
                        },
                        CHF: {
                            symbol: "CHF"
                        },
                        CHW: {
                            symbol: "CHW"
                        },
                        CLE: {
                            symbol: "CLE"
                        },
                        CLF: {
                            symbol: "CLF"
                        },
                        CLP: {
                            symbol: "CLP",
                            "symbol-alt-narrow": "$"
                        },
                        CNY: {
                            symbol: "CNY",
                            "symbol-alt-narrow": "¥"
                        },
                        COP: {
                            symbol: "COP",
                            "symbol-alt-narrow": "$"
                        },
                        COU: {
                            symbol: "COU"
                        },
                        CRC: {
                            symbol: "CRC",
                            "symbol-alt-narrow": "₡"
                        },
                        CSD: {
                            symbol: "CSD"
                        },
                        CSK: {
                            symbol: "CSK"
                        },
                        CUC: {
                            symbol: "CUC",
                            "symbol-alt-narrow": "$"
                        },
                        CUP: {
                            symbol: "CUP",
                            "symbol-alt-narrow": "$"
                        },
                        CVE: {
                            symbol: "CVE"
                        },
                        CYP: {
                            symbol: "CYP"
                        },
                        CZK: {
                            symbol: "CZK",
                            "symbol-alt-narrow": "Kč"
                        },
                        DDM: {
                            symbol: "DDM"
                        },
                        DEM: {
                            symbol: "DEM"
                        },
                        DJF: {
                            symbol: "DJF"
                        },
                        DKK: {
                            symbol: "DKK",
                            "symbol-alt-narrow": "kr"
                        },
                        DOP: {
                            symbol: "DOP",
                            "symbol-alt-narrow": "$"
                        },
                        DZD: {
                            symbol: "DZD"
                        },
                        ECS: {
                            symbol: "ECS"
                        },
                        ECV: {
                            symbol: "ECV"
                        },
                        EEK: {
                            symbol: "EEK"
                        },
                        EGP: {
                            symbol: "EGP",
                            "symbol-alt-narrow": "EGP"
                        },
                        ERN: {
                            symbol: "ERN"
                        },
                        ESA: {
                            symbol: "ESA"
                        },
                        ESB: {
                            symbol: "ESB"
                        },
                        ESP: {
                            symbol: "₧",
                            "symbol-alt-narrow": "₧"
                        },
                        ETB: {
                            symbol: "ETB"
                        },
                        EUR: {
                            symbol: "€",
                            "symbol-alt-narrow": "€"
                        },
                        FIM: {
                            symbol: "FIM"
                        },
                        FJD: {
                            symbol: "FJD",
                            "symbol-alt-narrow": "$"
                        },
                        FKP: {
                            symbol: "FKP",
                            "symbol-alt-narrow": "£"
                        },
                        FRF: {
                            symbol: "FRF"
                        },
                        GBP: {
                            symbol: "GBP",
                            "symbol-alt-narrow": "£"
                        },
                        GEK: {
                            symbol: "GEK"
                        },
                        GEL: {
                            symbol: "GEL",
                            "symbol-alt-narrow": "₾"
                        },
                        GHC: {
                            symbol: "GHC"
                        },
                        GHS: {
                            symbol: "GHS"
                        },
                        GIP: {
                            symbol: "GIP",
                            "symbol-alt-narrow": "£"
                        },
                        GMD: {
                            symbol: "GMD"
                        },
                        GNF: {
                            symbol: "GNF",
                            "symbol-alt-narrow": "FG"
                        },
                        GNS: {
                            symbol: "GNS"
                        },
                        GQE: {
                            symbol: "GQE"
                        },
                        GRD: {
                            symbol: "GRD"
                        },
                        GTQ: {
                            symbol: "GTQ",
                            "symbol-alt-narrow": "Q"
                        },
                        GWE: {
                            symbol: "GWE"
                        },
                        GWP: {
                            symbol: "GWP"
                        },
                        GYD: {
                            symbol: "GYD",
                            "symbol-alt-narrow": "$"
                        },
                        HKD: {
                            symbol: "HKD",
                            "symbol-alt-narrow": "$"
                        },
                        HNL: {
                            symbol: "HNL",
                            "symbol-alt-narrow": "L"
                        },
                        HRD: {
                            symbol: "HRD"
                        },
                        HRK: {
                            symbol: "HRK",
                            "symbol-alt-narrow": "kn"
                        },
                        HTG: {
                            symbol: "HTG"
                        },
                        HUF: {
                            symbol: "HUF",
                            "symbol-alt-narrow": "Ft"
                        },
                        IDR: {
                            symbol: "IDR",
                            "symbol-alt-narrow": "Rp"
                        },
                        IEP: {
                            symbol: "IEP"
                        },
                        ILP: {
                            symbol: "ILP"
                        },
                        ILS: {
                            symbol: "ILS",
                            "symbol-alt-narrow": "₪"
                        },
                        INR: {
                            symbol: "INR",
                            "symbol-alt-narrow": "₹"
                        },
                        IQD: {
                            symbol: "IQD"
                        },
                        IRR: {
                            symbol: "IRR"
                        },
                        ISK: {
                            symbol: "ISK",
                            "symbol-alt-narrow": "kr"
                        },
                        ITL: {
                            symbol: "ITL"
                        },
                        JMD: {
                            symbol: "JMD",
                            "symbol-alt-narrow": "$"
                        },
                        JOD: {
                            symbol: "JOD"
                        },
                        JPY: {
                            symbol: "JPY",
                            "symbol-alt-narrow": "¥"
                        },
                        KES: {
                            symbol: "KES"
                        },
                        KGS: {
                            symbol: "KGS"
                        },
                        KHR: {
                            symbol: "KHR",
                            "symbol-alt-narrow": "៛"
                        },
                        KMF: {
                            symbol: "KMF",
                            "symbol-alt-narrow": "CF"
                        },
                        KPW: {
                            symbol: "KPW",
                            "symbol-alt-narrow": "₩"
                        },
                        KRH: {
                            symbol: "KRH"
                        },
                        KRO: {
                            symbol: "KRO"
                        },
                        KRW: {
                            symbol: "KRW",
                            "symbol-alt-narrow": "₩"
                        },
                        KWD: {
                            symbol: "KWD"
                        },
                        KYD: {
                            symbol: "KYD",
                            "symbol-alt-narrow": "$"
                        },
                        KZT: {
                            symbol: "KZT",
                            "symbol-alt-narrow": "₸"
                        },
                        LAK: {
                            symbol: "LAK",
                            "symbol-alt-narrow": "₭"
                        },
                        LBP: {
                            symbol: "LBP",
                            "symbol-alt-narrow": "L£"
                        },
                        LKR: {
                            symbol: "LKR",
                            "symbol-alt-narrow": "Rs"
                        },
                        LRD: {
                            symbol: "LRD",
                            "symbol-alt-narrow": "$"
                        },
                        LSL: {
                            symbol: "LSL"
                        },
                        LTL: {
                            symbol: "LTL",
                            "symbol-alt-narrow": "Lt"
                        },
                        LTT: {
                            symbol: "LTT"
                        },
                        LUC: {
                            symbol: "LUC"
                        },
                        LUF: {
                            symbol: "LUF"
                        },
                        LUL: {
                            symbol: "LUL"
                        },
                        LVL: {
                            symbol: "LVL",
                            "symbol-alt-narrow": "Ls"
                        },
                        LVR: {
                            symbol: "LVR"
                        },
                        LYD: {
                            symbol: "LYD"
                        },
                        MAD: {
                            symbol: "MAD"
                        },
                        MAF: {
                            symbol: "MAF"
                        },
                        MCF: {
                            symbol: "MCF"
                        },
                        MDC: {
                            symbol: "MDC"
                        },
                        MDL: {
                            symbol: "MDL"
                        },
                        MGA: {
                            symbol: "MGA",
                            "symbol-alt-narrow": "Ar"
                        },
                        MGF: {
                            symbol: "MGF"
                        },
                        MKD: {
                            symbol: "MKD"
                        },
                        MKN: {
                            symbol: "MKN"
                        },
                        MLF: {
                            symbol: "MLF"
                        },
                        MMK: {
                            symbol: "MMK",
                            "symbol-alt-narrow": "K"
                        },
                        MNT: {
                            symbol: "MNT",
                            "symbol-alt-narrow": "₮"
                        },
                        MOP: {
                            symbol: "MOP"
                        },
                        MRO: {
                            symbol: "MRO"
                        },
                        MTL: {
                            symbol: "MTL"
                        },
                        MTP: {
                            symbol: "MTP"
                        },
                        MUR: {
                            symbol: "MUR",
                            "symbol-alt-narrow": "Rs"
                        },
                        MVR: {
                            symbol: "MVR"
                        },
                        MWK: {
                            symbol: "MWK"
                        },
                        MXN: {
                            symbol: "MXN",
                            "symbol-alt-narrow": "$"
                        },
                        MXP: {
                            symbol: "MXP"
                        },
                        MXV: {
                            symbol: "MXV"
                        },
                        MYR: {
                            symbol: "MYR",
                            "symbol-alt-narrow": "RM"
                        },
                        MZE: {
                            symbol: "MZE"
                        },
                        MZM: {
                            symbol: "MZM"
                        },
                        MZN: {
                            symbol: "MZN"
                        },
                        NAD: {
                            symbol: "NAD",
                            "symbol-alt-narrow": "$"
                        },
                        NGN: {
                            symbol: "NGN",
                            "symbol-alt-narrow": "₦"
                        },
                        NIC: {
                            symbol: "NIC"
                        },
                        NIO: {
                            symbol: "NIO",
                            "symbol-alt-narrow": "C$"
                        },
                        NLG: {
                            symbol: "NLG"
                        },
                        NOK: {
                            symbol: "NOK",
                            "symbol-alt-narrow": "kr"
                        },
                        NPR: {
                            symbol: "NPR",
                            "symbol-alt-narrow": "Rs"
                        },
                        NZD: {
                            symbol: "NZD",
                            "symbol-alt-narrow": "$"
                        },
                        OMR: {
                            symbol: "OMR"
                        },
                        PAB: {
                            symbol: "PAB"
                        },
                        PEI: {
                            symbol: "PEI"
                        },
                        PEN: {
                            symbol: "PEN"
                        },
                        PES: {
                            symbol: "PES"
                        },
                        PGK: {
                            symbol: "PGK"
                        },
                        PHP: {
                            symbol: "PHP",
                            "symbol-alt-narrow": "₱"
                        },
                        PKR: {
                            symbol: "PKR",
                            "symbol-alt-narrow": "Rs"
                        },
                        PLN: {
                            symbol: "PLN",
                            "symbol-alt-narrow": "zł"
                        },
                        PLZ: {
                            symbol: "PLZ"
                        },
                        PTE: {
                            symbol: "PTE"
                        },
                        PYG: {
                            symbol: "PYG",
                            "symbol-alt-narrow": "₲"
                        },
                        QAR: {
                            symbol: "QAR"
                        },
                        RHD: {
                            symbol: "RHD"
                        },
                        ROL: {
                            symbol: "ROL"
                        },
                        RON: {
                            symbol: "RON"
                        },
                        RSD: {
                            symbol: "RSD"
                        },
                        RUB: {
                            symbol: "RUB",
                            "symbol-alt-narrow": "₽"
                        },
                        RUR: {
                            symbol: "RUR",
                            "symbol-alt-narrow": "р."
                        },
                        RWF: {
                            symbol: "RWF",
                            "symbol-alt-narrow": "RF"
                        },
                        SAR: {
                            symbol: "SAR"
                        },
                        SBD: {
                            symbol: "SBD",
                            "symbol-alt-narrow": "$"
                        },
                        SCR: {
                            symbol: "SCR"
                        },
                        SDD: {
                            symbol: "SDD"
                        },
                        SDG: {
                            symbol: "SDG"
                        },
                        SDP: {
                            symbol: "SDP"
                        },
                        SEK: {
                            symbol: "SEK",
                            "symbol-alt-narrow": "kr"
                        },
                        SGD: {
                            symbol: "SGD",
                            "symbol-alt-narrow": "$"
                        },
                        SHP: {
                            symbol: "SHP",
                            "symbol-alt-narrow": "£"
                        },
                        SIT: {
                            symbol: "SIT"
                        },
                        SKK: {
                            symbol: "SKK"
                        },
                        SLL: {
                            symbol: "SLL"
                        },
                        SOS: {
                            symbol: "SOS"
                        },
                        SRD: {
                            symbol: "SRD",
                            "symbol-alt-narrow": "$"
                        },
                        SRG: {
                            symbol: "SRG"
                        },
                        SSP: {
                            symbol: "SSP",
                            "symbol-alt-narrow": "£"
                        },
                        STD: {
                            symbol: "STD",
                            "symbol-alt-narrow": "Db"
                        },
                        SUR: {
                            symbol: "SUR"
                        },
                        SVC: {
                            symbol: "SVC"
                        },
                        SYP: {
                            symbol: "SYP",
                            "symbol-alt-narrow": "£"
                        },
                        SZL: {
                            symbol: "SZL"
                        },
                        THB: {
                            symbol: "฿",
                            "symbol-alt-narrow": "฿"
                        },
                        TJR: {
                            symbol: "TJR"
                        },
                        TJS: {
                            symbol: "TJS"
                        },
                        TMM: {
                            symbol: "TMM"
                        },
                        TMT: {
                            symbol: "TMT"
                        },
                        TND: {
                            symbol: "TND"
                        },
                        TOP: {
                            symbol: "TOP",
                            "symbol-alt-narrow": "T$"
                        },
                        TPE: {
                            symbol: "TPE"
                        },
                        TRL: {
                            symbol: "TRL"
                        },
                        TRY: {
                            symbol: "TRY",
                            "symbol-alt-narrow": "₺"
                        },
                        TTD: {
                            symbol: "TTD",
                            "symbol-alt-narrow": "$"
                        },
                        TWD: {
                            symbol: "TWD",
                            "symbol-alt-narrow": "NT$"
                        },
                        TZS: {
                            symbol: "TZS"
                        },
                        UAH: {
                            symbol: "UAH",
                            "symbol-alt-narrow": "₴"
                        },
                        UAK: {
                            symbol: "UAK"
                        },
                        UGS: {
                            symbol: "UGS"
                        },
                        UGX: {
                            symbol: "UGX"
                        },
                        USD: {
                            symbol: "$",
                            "symbol-alt-narrow": "$"
                        },
                        USN: {
                            symbol: "USN"
                        },
                        USS: {
                            symbol: "USS"
                        },
                        UYI: {
                            symbol: "UYI"
                        },
                        UYP: {
                            symbol: "UYP"
                        },
                        UYU: {
                            symbol: "UYU",
                            "symbol-alt-narrow": "$"
                        },
                        UZS: {
                            symbol: "UZS"
                        },
                        VEB: {
                            symbol: "VEB"
                        },
                        VEF: {
                            symbol: "VEF",
                            "symbol-alt-narrow": "Bs"
                        },
                        VND: {
                            symbol: "₫",
                            "symbol-alt-narrow": "₫"
                        },
                        VNN: {
                            symbol: "VNN"
                        },
                        VUV: {
                            symbol: "VUV"
                        },
                        WST: {
                            symbol: "WST"
                        },
                        XAF: {
                            symbol: "XAF"
                        },
                        XAG: {
                            symbol: "XAG"
                        },
                        XAU: {
                            symbol: "XAU"
                        },
                        XBA: {
                            symbol: "XBA"
                        },
                        XBB: {
                            symbol: "XBB"
                        },
                        XBC: {
                            symbol: "XBC"
                        },
                        XBD: {
                            symbol: "XBD"
                        },
                        XCD: {
                            symbol: "XCD",
                            "symbol-alt-narrow": "$"
                        },
                        XDR: {
                            symbol: "XDR"
                        },
                        XEU: {
                            symbol: "XEU"
                        },
                        XFO: {
                            symbol: "XFO"
                        },
                        XFU: {
                            symbol: "XFU"
                        },
                        XOF: {
                            symbol: "XOF"
                        },
                        XPD: {
                            symbol: "XPD"
                        },
                        XPF: {
                            symbol: "CFPF"
                        },
                        XPT: {
                            symbol: "XPT"
                        },
                        XRE: {
                            symbol: "XRE"
                        },
                        XSU: {
                            symbol: "XSU"
                        },
                        XTS: {
                            symbol: "XTS"
                        },
                        XUA: {
                            symbol: "XUA"
                        },
                        XXX: {
                            symbol: "XXX"
                        },
                        YDD: {
                            symbol: "YDD"
                        },
                        YER: {
                            symbol: "YER"
                        },
                        YUD: {
                            symbol: "YUD"
                        },
                        YUM: {
                            symbol: "YUM"
                        },
                        YUN: {
                            symbol: "YUN"
                        },
                        YUR: {
                            symbol: "YUR"
                        },
                        ZAL: {
                            symbol: "ZAL"
                        },
                        ZAR: {
                            symbol: "ZAR",
                            "symbol-alt-narrow": "R"
                        },
                        ZMK: {
                            symbol: "ZMK"
                        },
                        ZMW: {
                            symbol: "ZMW",
                            "symbol-alt-narrow": "ZK"
                        },
                        ZRN: {
                            symbol: "ZRN"
                        },
                        ZRZ: {
                            symbol: "ZRZ"
                        },
                        ZWD: {
                            symbol: "ZWD"
                        },
                        ZWL: {
                            symbol: "ZWL"
                        },
                        ZWR: {
                            symbol: "ZWR"
                        }
                    }
                }
            }
        }
    };

    var Ret = { main: {} };
    Ret.main[locale] = IdiomaDates.main.xx;
    return Ret;
}

function getIdiomaMessages(locale) {
    var IdiomaMessages =
        {
            xx: {
                Yes: 'Si',
                No: 'No',
                Cancel: 'Cancelar',
                Clear: 'Limpiar',
                Done: 'Hecho',
                Loading: 'Cargando...',
                Select: 'Seleccionar...',
                Search: 'Buscar',
                Back: 'Volver',
                OK: 'Aceptar',
                "dxCollectionWidget-noDataText": 'Sin datos para mostrar',
                "validation-required": 'Obligatorio',
                "validation-required-formatted": '{0} es obligatorio',
                "validation-numeric": 'Valor debe ser un número',
                "validation-numeric-formatted": '{0} debe ser un número',
                "validation-range": 'Valor fuera de rango',
                "validation-range-formatted": '{0} fuera de rango',
                "validation-stringLength": 'El largo del valor es incorrecto',
                "validation-stringLength-formatted": 'El largo de {0} es incorrecto',
                "validation-custom": 'Valor inválido',
                "validation-custom-formatted": '{0} inválido',
                "validation-compare": 'Valores no coinciden',
                "validation-compare-formatted": '{0} no coinciden',
                "validation-pattern": 'Valor no coincide con el patrón',
                "validation-pattern-formatted": '{0} no coincide con el patrón',
                "validation-email": 'Email inválido',
                "validation-email-formatted": '{0} inválido',
                "validation-mask": 'Valor inválido',
                "dxLookup-searchPlaceholder": 'Cantidad mínima de caracteres: {0}',
                "dxList-pullingDownText": 'Desliza hacia abajo para actualizar...',
                "dxList-pulledDownText": 'Suelta para actualizar...',
                "dxList-refreshingText": 'Actualizando...',
                "dxList-pageLoadingText": 'Cargando...',
                "dxList-nextButtonText": 'Más',
                "dxList-selectAll": 'Seleccionar Todo',
                "dxListEditDecorator-delete": 'Eliminar',
                "dxListEditDecorator-more": 'Más',
                "dxScrollView-pullingDownText": 'Desliza hacia abajo para actualizar...',
                "dxScrollView-pulledDownText": 'Suelta para actualizar...',
                "dxScrollView-refreshingText": 'Actualizando...',
                "dxScrollView-reachBottomText": 'Cargando...',
                "dxDateBox-simulatedDataPickerTitleTime": 'Seleccione hora',
                "dxDateBox-simulatedDataPickerTitleDate": 'Seleccione fecha',
                "dxDateBox-simulatedDataPickerTitleDateTime": 'Seleccione fecha y hora',
                "dxDateBox-validation-datetime": 'Valor debe ser una fecha u hora',
                "dxFileUploader-selectFile": 'Seleccionar archivo',
                "dxFileUploader-dropFile": 'o Arrastre un archivo aquí',
                "dxFileUploader-bytes": 'bytes',
                "dxFileUploader-kb": 'kb',
                "dxFileUploader-Mb": 'Mb',
                "dxFileUploader-Gb": 'Gb',
                "dxFileUploader-upload": 'Subir',
                "dxFileUploader-uploaded": 'Subido',
                "dxFileUploader-readyToUpload": 'Listo para subir',
                "dxFileUploader-uploadFailedMessage": 'Súbida fallo',
                "dxRangeSlider-ariaFrom": 'Desde',
                "dxRangeSlider-ariaTill": 'Hasta',
                "dxSwitch-onText": 'ENCENDIDO',
                "dxSwitch-offText": 'APAGADO',
                "dxForm-optionalMark": 'opcional',
                "dxForm-requiredMessage": '{0} es obligatorio',
                "dxNumberBox-invalidValueMessage": 'Valor debe ser un número',
                "dxDataGrid-columnChooserTitle": 'Selector de Columnas',
                "dxDataGrid-columnChooserEmptyText": 'Arrastra una columna aquí para esconderla',
                "dxDataGrid-groupContinuesMessage": 'Continúa en la página siguiente',
                "dxDataGrid-groupContinuedMessage": 'Continuación de la página anterior',
                "dxDataGrid-groupHeaderText": 'Agrupar por esta columna',
                "dxDataGrid-ungroupHeaderText": 'Desagrupar',
                "dxDataGrid-ungroupAllText": 'Desagrupar Todo',
                "dxDataGrid-editingEditRow": 'Editar',
                "dxDataGrid-editingSaveRowChanges": 'Guardar',
                "dxDataGrid-editingCancelRowChanges": 'Cancelar',
                "dxDataGrid-editingDeleteRow": 'Eliminar',
                "dxDataGrid-editingUndeleteRow": 'Recuperar',
                "dxDataGrid-editingConfirmDeleteMessage": '¿Está seguro que desea eliminar este registro?',
                "dxDataGrid-validationCancelChanges": 'Cancelar cambios',
                "dxDataGrid-groupPanelEmptyText": 'Arrastra una columna aquí para agrupar por ella',
                "dxDataGrid-noDataText": 'Sin datos',
                "dxDataGrid-searchPanelPlaceholder": 'Buscar...',
                "dxDataGrid-filterRowShowAllText": '(Todo)',
                "dxDataGrid-filterRowResetOperationText": 'Reestablecer',
                "dxDataGrid-filterRowOperationEquals": 'Igual',
                "dxDataGrid-filterRowOperationNotEquals": 'No es igual',
                "dxDataGrid-filterRowOperationLess": 'Menor que',
                "dxDataGrid-filterRowOperationLessOrEquals": 'Menor que o igual a',
                "dxDataGrid-filterRowOperationGreater": 'Mayor que',
                "dxDataGrid-filterRowOperationGreaterOrEquals": 'Mayor que o igual a',
                "dxDataGrid-filterRowOperationStartsWith": 'Empieza con',
                "dxDataGrid-filterRowOperationContains": 'Contiene',
                "dxDataGrid-filterRowOperationNotContains": 'No contiene',
                "dxDataGrid-filterRowOperationEndsWith": 'Termina con',
                "dxDataGrid-filterRowOperationBetween": 'Entre',
                "dxDataGrid-filterRowOperationBetweenStartText": 'Inicio',
                "dxDataGrid-filterRowOperationBetweenEndText": 'Fin',
                "dxDataGrid-applyFilterText": 'Filtrar',
                "dxDataGrid-trueText": 'Verdadero',
                "dxDataGrid-falseText": 'Falso',
                "dxDataGrid-sortingAscendingText": 'Orden Ascendente',
                "dxDataGrid-sortingDescendingText": 'Orden Descendente',
                "dxDataGrid-sortingClearText": 'Limpiar Ordenamiento',
                "dxDataGrid-editingSaveAllChanges": 'Guardar cambios',
                "dxDataGrid-editingCancelAllChanges": 'Descartar cambios',
                "dxDataGrid-editingAddRow": 'Agregar una fila',
                "dxDataGrid-summaryMin": 'Mín: {0}',
                "dxDataGrid-summaryMinOtherColumn": 'Mín de {1} es {0}',
                "dxDataGrid-summaryMax": 'Máx: {0}',
                "dxDataGrid-summaryMaxOtherColumn": 'Máx de {1} es {0}',
                "dxDataGrid-summaryAvg": 'Prom: {0}',
                "dxDataGrid-summaryAvgOtherColumn": 'Prom de {1} es {0}',
                "dxDataGrid-summarySum": 'Suma: {0}',
                "dxDataGrid-summarySumOtherColumn": 'Suma de {1} es {0}',
                "dxDataGrid-summaryCount": 'Recuento: {0}',
                "dxDataGrid-columnFixingFix": 'Anclar',
                "dxDataGrid-columnFixingUnfix": 'Desanclar',
                "dxDataGrid-columnFixingLeftPosition": 'A la izquierda',
                "dxDataGrid-columnFixingRightPosition": 'A la derecha',
                "dxDataGrid-exportTo": 'Exportar',
                "dxDataGrid-exportToExcel": 'Exportar a archivo Excel',
                "dxDataGrid-excelFormat": 'Archivo Excel',
                "dxDataGrid-selectedRows": 'Filas seleccionadas',
                "dxDataGrid-exportSelectedRows": 'Exportar filas seleccionadas',
                "dxDataGrid-exportAll": 'Exportar todo',
                "dxDataGrid-headerFilterEmptyValue": '(En Blanco)',
                "dxDataGrid-headerFilterOK": 'Aceptar',
                "dxDataGrid-headerFilterCancel": 'Cancelar',
                "dxDataGrid-ariaColumn": 'Columna',
                "dxDataGrid-ariaValue": 'Valor',
                "dxDataGrid-ariaFilterCell": 'Celda de filtro',
                "dxDataGrid-ariaCollapse": 'Colapsar',
                "dxDataGrid-ariaExpand": 'Expandir',
                "dxDataGrid-ariaDataGrid": 'Rejilla de datos',
                "dxDataGrid-ariaSearchInGrid": 'Buscar en la rejilla de datos',
                "dxDataGrid-ariaSelectAll": 'Seleccionar todo',
                "dxDataGrid-ariaSelectRow": 'Seleccionar fila',
                "dxPager-infoText": 'Página {0} de {1} ({2} elementos)',
                "dxPager-pagesCountText": 'de',
                "dxPivotGrid-grandTotal": 'Gran Total',
                "dxPivotGrid-total": '{0} Total',
                "dxPivotGrid-fieldChooserTitle": 'Selector de Campos',
                "dxPivotGrid-showFieldChooser": 'Mostrar Selector de Campos',
                "dxPivotGrid-expandAll": 'Expandir Todo',
                "dxPivotGrid-collapseAll": 'Colapsar Todo',
                "dxPivotGrid-sortColumnBySummary": 'Ordenar "{0}" por Esta Columna',
                "dxPivotGrid-sortRowBySummary": 'Ordenar "{0}" por Esta Fila',
                "dxPivotGrid-removeAllSorting": 'Remover Ordenamiento',
                "dxPivotGrid-rowFields": 'Campos de Fila',
                "dxPivotGrid-columnFields": 'Campos de Columna',
                "dxPivotGrid-dataFields": 'Capos de Dato',
                "dxPivotGrid-filterFields": 'Campos de Filtro',
                "dxPivotGrid-allFields": 'Todos los Campos',
                "dxPivotGrid-columnFieldArea": 'Arrastra Campos de Columna Aquí',
                "dxPivotGrid-dataFieldArea": 'Arrastra Campos de Dato Aquí',
                "dxPivotGrid-rowFieldArea": 'Arrastra Campos de Fila Aquí',
                "dxPivotGrid-filterFieldArea": 'Arrastra Campos de Filtro Aquí',
                "dxScheduler-editorLabelTitle": 'Asunto',
                "dxScheduler-editorLabelStartDate": 'Fecha Inicio',
                "dxScheduler-editorLabelEndDate": 'Fecha Término',
                "dxScheduler-editorLabelDescription": 'Descripción',
                "dxScheduler-editorLabelRecurrence": 'Repetir',
                "dxScheduler-openAppointment": 'Abrir cita',
                "dxScheduler-recurrenceNever": 'Nunca',
                "dxScheduler-recurrenceDaily": 'Diario',
                "dxScheduler-recurrenceWeekly": 'Semanal',
                "dxScheduler-recurrenceMonthly": 'Mensual',
                "dxScheduler-recurrenceYearly": 'Anual',
                "dxScheduler-recurrenceEvery": 'Cada',
                "dxScheduler-recurrenceEnd": 'Terminar repetición',
                "dxScheduler-recurrenceAfter": 'Después',
                "dxScheduler-recurrenceOn": 'En',
                "dxScheduler-recurrenceRepeatDaily": 'día(s)',
                "dxScheduler-recurrenceRepeatWeekly": 'semana(s)',
                "dxScheduler-recurrenceRepeatMonthly": 'mes(es)',
                "dxScheduler-recurrenceRepeatYearly": 'año(s)',
                "dxScheduler-switcherDay": 'Día',
                "dxScheduler-switcherWeek": 'Semana',
                "dxScheduler-switcherWorkWeek": 'Semana Laboral',
                "dxScheduler-switcherMonth": 'Mes',
                "dxScheduler-switcherAgenda": 'Agenda',
                "dxScheduler-switcherTimelineDay": 'Línea de tiempo Día',
                "dxScheduler-switcherTimelineWeek": 'Línea de tiempo Semana',
                "dxScheduler-switcherTimelineWorkWeek": 'Línea de tiempo Semana Laboral',
                "dxScheduler-switcherTimelineMonth": 'Línea de tiempo Mes',
                "dxScheduler-recurrenceRepeatOnDate": 'en la fecha',
                "dxScheduler-recurrenceRepeatCount": 'ocurrencia(s)',
                "dxScheduler-allDay": 'Todo el día',
                "dxScheduler-confirmRecurrenceEditMessage": '¿Quiere editar solo esta cita o toda la serie?',
                "dxScheduler-confirmRecurrenceDeleteMessage": '¿Quiere eliminar solo esta cita o toda la serie?',
                "dxScheduler-confirmRecurrenceEditSeries": 'Editar serie',
                "dxScheduler-confirmRecurrenceDeleteSeries": 'Eliminar serie',
                "dxScheduler-confirmRecurrenceEditOccurrence": 'Editar cita',
                "dxScheduler-confirmRecurrenceDeleteOccurrence": 'Eliminar cita',
                "dxScheduler-noTimezoneTitle": 'Sin zona horaria',
                "dxCalendar-todayButtonText": 'Hoy',
                "dxCalendar-ariaWidgetName": 'Calendario',
                "dxColorView-ariaRed": 'Rojo',
                "dxColorView-ariaGreen": 'Verde',
                "dxColorView-ariaBlue": 'Azul',
                "dxColorView-ariaAlpha": 'Transparencia',
                "dxColorView-ariaHex": 'Código de color',
                "vizExport-printingButtonText": 'Imprimir',
                "vizExport-titleMenuText": 'Exportar/Imprimir',
                "vizExport-exportButtonText": 'Archivo {0}',


            }
        };

    var Ret = {};
    Ret[locale] = IdiomaMessages.xx;
    return Ret;
}

