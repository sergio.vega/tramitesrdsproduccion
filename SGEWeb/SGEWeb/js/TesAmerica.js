﻿function EvaluateCRUDType(SOL_STATE) {
    var CRUD = enumCRUD.Read;
   
    switch (SOL_STATE) {
        case enumSOL_STATES.SinRadicar:
            CRUD = enumCRUD.Create;
            break;
        case enumSOL_STATES.Requerido:
            CRUD = enumCRUD.Update;
            break;
        case enumSOL_STATES.Entrámite:
        case enumSOL_STATES.Radicada:
        case enumSOL_STATES.Subsanado:
        case enumSOL_STATES.Cerrada:
        case enumSOL_STATES.Finalizada:
        case enumSOL_STATES.Cancelada:
        case enumSOL_STATES.Rechazada:
            CRUD = enumCRUD.Read;
            break;
    }

    return CRUD;
}

function Limit(Valor, Longitud) {
    if (Valor.length > Longitud)
        return Valor.substring(0, Longitud) + '...';
    else
        return Valor;
}


function IsNull(Valor, Defecto) {
    if (Valor == null)
        return Defecto;
    else
        return Valor;
}


function IsNullV2(Valor) {
    if (Valor == null)
        return true;
    else
        return false;
}

function CrearDialogHtml(Icon, Text, Style) {
    var cadHTML = "<table " + (Style != undefined ? "style='" + Style + "'" : "") + "><tr><td><i style='margin-right:5px;color:#6f91cb;' class='" + Icon + " ta-3x'></i></td><td style='padding-left:5px;'>" + Text + "</td></tr></table>";
    return cadHTML;
}

function DateToString(Fecha){
    if (Fecha == null || Fecha==undefined)
        return '';
    else
        return Fecha.toString('yyyyMMdd');
}

function IsNullOrEmpty(data) {
    if (data == null || data == undefined || data === '')
        return true;
    return false;
}

function StringToDate(Fecha) {
    if (Fecha == null || Fecha == undefined || Fecha=='')
        return null;
    else
        return Date.parseExact(Fecha, "yyyyMMdd")
}

function Globalizeformat(Valor, formato) {

    var F0 = formato.substr(0, 1).toLowerCase();
    var F1 = parseInt(formato.substr(1, 1));

    if (F0 == 'p')
        Valor *= 100.0;

    var Ret = Globalize.formatNumber(Valor, { minimumFractionDigits: F1, maximumFractionDigits: F1 });

    if (F0 == 'p')
        Ret = Ret + '%';

    return Ret;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function GetPermiso(index) {
    return (SGEWeb.app.Permisos.substr(index, 1) == '1');
}

function ArrayExists(Arr, Value) {
    for (var i = 0; i < Arr.length; i++) {
        if (Arr[i] == Value)
            return true;
    }
    return false;
}

function ListObjectExists(List, Attribute, Cad2Find) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute] == Cad2Find)
            return true;
    }
    return false;
}

function ListObjectFindIndex(List, Attribute, Cad2Find, Default) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute] == Cad2Find)
            return i;
    }
    return Default;
}

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
};

String.prototype.In = function (ArrayValues) {
    for (i = 0; i < ArrayValues.length; i++) {
        if (ArrayValues[i] == this)
            return true;
    }
    return false;
};

String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

Array.prototype.Exists = function (Value) {
    for (i = 0; i < this.length; i++) {
        if (this[i] == Value)
            return true;
    }
    return false;
}

Array.prototype.NoExists = function (Value) {
    for (i = 0; i < this.length; i++) {
        if (this[i] == Value)
            return false;
    }
    return true;
}

Array.prototype.In = function (Value) {
    for (i = 0; i < this.length; i++) {
        if (this[i] == Value)
            return true;
    }
    return false;
}

Number.prototype.In = function (ArrayValues) {

    for (i = 0; i < ArrayValues.length; i++) {
        if (ArrayValues[i] ==this)
            return true;
    }
    return false;
}

Number.prototype.Greater = function (Value) {
    return this > Value;
}

Number.prototype.GreaterOrEqual = function (Value) {
    return this >= Value;
}

Number.prototype.LessOrEqual = function (Value) {
    return this <= Value;
}

var WhitoutDiacritics = (function () {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛÇÑãàáäâèéëêìíïîòóöôùúüûñç",
          to = "AAAAAEEEEIIIIOOOOUUUUCNaaaaaeeeeiiiioooouuuunc",
        mapping = {};

    for (var i = 0, j = from.length; i < j; i++)
        mapping[from.charAt(i)] = to.charAt(i);

    return function (str) {
        var ret = [];
        for (var i = 0, j = str.length; i < j; i++) {
            var c = str.charAt(i);
            if (mapping.hasOwnProperty(str.charAt(i)))
                ret.push(mapping[c]);
            else
                ret.push(c);
        }
        return ret.join('');
    }
})();

function isNormalInteger(str) {
    return /^\+?(0|[1-9]\d*)$/.test(str);
}

function ListObjectFindGetElement(List, Attribute, Cad2Find) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute] == Cad2Find)
            return List[i];
    }
    return null;
}

function GetElementIgnoringDiacriticsUpperCase(List, Attribute, Cad2Find) {
    for (var i = 0; i < List.length; i++) {
        var listItem = WhitoutDiacritics(List[i][Attribute]).toLowerCase();
        var findItem = WhitoutDiacritics(Cad2Find).toLowerCase();
        if (listItem == findItem)
            return List[i];
    }
    return null;
}

function KoListObjectFindGetElement(List, Attribute, Cad2Find) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute]() == Cad2Find)
            return List[i];
    }
    return null;
}

function KoListObjectExists(List, Attribute, Cad2Find) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute]() == Cad2Find)
            return true;
    }
    return false;
}

function ListObjectCountThreeLevels(List, Attribute1, Attribute2, Attribute3, Cad2Find) {
    var Result = 0;
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute1][Attribute2][Attribute3] == Cad2Find)
            Result++;
    }
    return Result;
}

function ListObjectCountTwoLevels(List, Attribute1, Attribute2, Cad2Find) {
    var Result = 0;
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute1][Attribute2] == Cad2Find)
            Result++;
    }
    return Result;
}

function ListObjectCountOneLevel(List, Attribute, Cad2Find) {
    var Result = 0;
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute]== Cad2Find)
            Result++;
    }
    return Result;
}

function ListObjectFindIndexDuo(List, Attribute1, Cad2Find1, Attribute2, Cad2Find2) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute1] == Cad2Find1) {
            if (List[i][Attribute2] == Cad2Find2) {
                return i;
            }
        }
    }
    return -1;
}

function ListObjectGetAttribute(List, Attribute2Find, Cad2Find, Attribute2Return) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute2Find] == Cad2Find) {
            return List[i][Attribute2Return];
        }
    }
    return undefined;
}

function ListObjectGetAttribute(List, Attribute2Find, Cad2Find, Attribute2Return, Default) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute2Find] == Cad2Find) {
            return List[i][Attribute2Return];
        }
    }
    return Default;
}

function ListObjectGetAttributeDuo(List, Attribute2Find1, Cad2Find1, Attribute2Find2, Cad2Find2, Attribute2Return) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute2Find1] == Cad2Find1) {
            if (List[i][Attribute2Find2] == Cad2Find2) {
                return List[i][Attribute2Return];
            }
        }
    }
    return undefined;
}

function ListObjectGetAttributeDuoNot2(List, Attribute2Find1, Cad2Find1, Attribute2Find2, Cad2Find2, Attribute2Return) {
    for (var i = 0; i < List.length; i++) {
        if (List[i][Attribute2Find1] == Cad2Find1) {
            if (List[i][Attribute2Find2] != Cad2Find2) {
                return List[i][Attribute2Return];
            }
        }
    }
    return undefined;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
    return undefined;
}

function isGuid(s) {
    var pattern = new RegExp('^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$', 'i');
    return pattern.test(s);
}

Meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

function onInitGlobal(e) {
    e.component.registerKeyHandler("escape", function (arg) {
        arg.stopPropagation();
    })

    e.component.registerKeyHandler("backspace", function (arg) {
        arg.stopPropagation();
    })
}

function onPwdKeyDownGlobal(data, event) {
    if (data.jQueryEvent.keyCode == 8) {
        data.event.preventDefault();
        data.jQueryEvent.stopPropagation();
    }
    return true;
}

function keyPressActionGlobal(e) {
    var event = e.jQueryEvent,
        str = String.fromCharCode(event.keyCode);
    if (!/[0-9]/.test(str))
        event.preventDefault();
}

function copyToClipboard(text) {
    text = text.replace(/\n/g, '\r\n');


    if (window.clipboardData && window.clipboardData.setData) {
        // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
        return clipboardData.setData("Text", text);

    }
    else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
        var textarea = document.createElement("textarea");
        textarea.textContent = text;
        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
        }
        catch (ex) {
            console.warn("Copy to clipboard failed.", ex);
            return false;
        }
        finally {
            document.body.removeChild(textarea);
        }
    }
}

var MONTHS = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

function LITE_LABELS_DATE(day, month, year) {
    if (typeof (year) != 'undefined') {
        year = ", " + year;
    }
    else {
        year = "";
    }

    return MONTHS[month] + " " + day + year;
}

function GetAdminStateID(name) {
    if (!isNaN(name))
        return 'ANX_ROLE' + name + '_STATE_ID';
    else
        return 'ANX_ROLE' + enumROLES[name] + '_STATE_ID';
}

function GetAdminComment(name) {
    if (!isNaN(name))
        return 'ANX_ROLE' + name + '_COMMENT';
    else
        return 'ANX_ROLE' + enumROLES[name] + '_COMMENT';
}

function GetTechStateID(name) {
    if (!isNaN(name))
        return 'TECH_ROLE' + name + '_STATE_ID';
    else
        return 'TECH_ROLE' + enumROLES[name] + '_STATE_ID';
}

function GetTechComment(name) {
    if (!isNaN(name))
        return 'TECH_ROLE' + name + '_COMMENT';
    else
        return 'TECH_ROLE' + enumROLES[name] + '_COMMENT';
}

function GetResStateID(name) {
    if (!isNaN(name))
        return 'RES_ROLE' + name + '_STATE_ID';
    else
        return 'RES_ROLE' + enumROLES[name] + '_STATE_ID';
}

function GetResComment(name) {
    if (!isNaN(name))
        return 'RES_ROLE' + name + '_COMMENT';
    else
        return 'RES_ROLE' + enumROLES[name] + '_COMMENT';
}

function GetNextRole(Valor) {
    var NextRole = Valor;

    switch (Valor) {
        case 1:
            NextRole = 2;
            break;
        case 2:
            NextRole = 4;
            break;
        case 4:
            NextRole = 8;
            break;
        case 8:
            NextRole = 16;
            break;
        case 16:
            NextRole = 32;
            break;
        case 32:
            NextRole = 128;
            break;
    }
    return NextRole;
}

function GetMaxRole(Valor) {

    var MaxRole = enumROLES.Concesionario;

    if ((Valor & enumROLES.Funcionario) == enumROLES.Funcionario)
        MaxRole = enumROLES.Funcionario;
    if ((Valor & enumROLES.Revisor) == enumROLES.Revisor)
        MaxRole = enumROLES.Revisor;
    if ((Valor & enumROLES.Coordinador) == enumROLES.Coordinador)
        MaxRole = enumROLES.Coordinador;
    if ((Valor & enumROLES.Subdirector) == enumROLES.Subdirector)
        MaxRole = enumROLES.Subdirector;
    if ((Valor & enumROLES.Asesor) == enumROLES.Asesor)
        MaxRole = enumROLES.Asesor;
    if ((Valor & enumROLES.Director) == enumROLES.Director)
        MaxRole = enumROLES.Director;
    if ((Valor & enumROLES.Administrador) == enumROLES.Administrador)
        MaxRole = enumROLES.Administrador;

    return MaxRole;
}

function GetRoles(Valor) {

    var Roles = [];
    if (Valor == 0)
        Roles.push('Concesionario');
    if ((Valor & enumROLES.Funcionario) == enumROLES.Funcionario)
        Roles.push('Funcionario');
    if ((Valor & enumROLES.Revisor) == enumROLES.Revisor)
        Roles.push('Revisor');
    if ((Valor & enumROLES.Coordinador) == enumROLES.Coordinador)
        Roles.push('Coordinador');
    if ((Valor & enumROLES.Subdirector) == enumROLES.Subdirector)
        Roles.push('Subdirector');
    if ((Valor & enumROLES.Asesor) == enumROLES.Asesor)
        Roles.push('Asesor');
    if ((Valor & enumROLES.Director) == enumROLES.Director)
        Roles.push('Director');
    if ((Valor & enumROLES.Administrador) == enumROLES.Administrador)
        Roles.push('Administrador');


    return Roles.join(', ');
}

function OnTesAmerica() {
    window.open('https://www.tesamerica.com', "_blank");
}

