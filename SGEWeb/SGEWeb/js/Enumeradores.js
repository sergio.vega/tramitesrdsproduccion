﻿var enumREASIGN_TYPE =
{
    Solicitar: 1,
    Rechazar: 2,
    Aprobar: 3,
    Reasignar: 4 //Aprobar sin solicitud
};

var enumGROUPS =
{
    Concesionario: 0,
    MinTIC: 1,
    ANE: 2
};

var enumSTEPS =
{
    EsViable: -3,
    Viabilidad: -2,
    Administrativo: 0,
    Tecnico: 1,
    Resolucion: 2,
    EnCierre: 3,
    Terminada: 4
};

var enumROLES =
{
    Concesionario: 0,
    Funcionario: 1,
    Revisor: 2,
    Coordinador: 4,
    Subdirector: 8,
    Asesor: 16,
    Director: 32,
    Administrador: 128
};

var enumALARM =
{
    EnCurso: 1,
    Porvencer: 2,
    Vencidas: 3,
    EnCierre: 4,
    Terminadas: 5
};

var enumALARM_REQUIRED =
{
    Encurso: 0,
    Requeridas: 1,
    Porvencer: 2,
    Vencidas: 3,
    EnCierre: 4,
    Terminadas: 5
};

var enumCRUD =
{
    Create: 1,
    Read: 2,
    Update: 3,
    Delete: 4
};

var enumDOC_TECH_TYPE =
{
    Soporte: 1,
    Concepto: 2,
    Cuadro: 3
};

var enumROLE1_STATE =
{
    SinDefinir: 0,
    Radicado: 1,
    Requerido: 2,
    Subsanado: 3,
    Aprobado: 4,
    Rechazado: 5,
    NoRevision: 6
};

var enumROLE248_STATE =
{
    SinDefinir: 0,
    Aceptado: 1,
    Devuelto: 2,
    Revision: 3
};

var enumSOL_TYPES =
{
    ComunicacionArriendoEmisora: 1,
    JuntaDeProgramacion: 2,
    ManualDeEstilo: 3,
    FusionesEIntegraciones: 4,
    SuspensionesTemporales: 5,
    ArchivoDeLaConcesion: 6,
    ProrrogaConcesion: 7,
    ModParametrosTecnicos: 8,
    Interferencias: 9,
    CesionExpediente: 10,
    RedTransmovil: 11,
    CancelacionRedes: 12,
    OtorgaEmisoraComercial: 14,
    OtorgaEmisoraComunitaria: 15,
    OtorgaEmisoraComunitariaGruposEtnicos: 16,
    OtorgaEmisoraDeInteresPublico: 17,
};

var enumANEX_TYPE =
{
    OtorgaEstudioTecnico: 57,
    OtorgaConceptoAeronautica: 58,
    OtorgaPlaneacionMunicipal: 59,
    OtorgaAnexoTecnico: 60,
    OtorgaManualEstilo: 61,
    OtorgaJuntaProgramacion: 62
    //OtorgaEstudioTecnico: 63,
    //OtorgaConceptoAeronautica: 64,
    //OtorgaPlaneacionMunicipal: 65,
    //OtorgaAnexoTecnico: 66,
    //OtorgaManualEstilo:  67,
    //OtorgaJuntaProgramacion: 68
};

var ROLE1_STATES = [
    { ID: 0, Text: 'Sin definir', Icon: 'ta ta-empty ta-lg', Color: 'transparent', Cursor: 'default' },
    { ID: 1, Text: 'Radicado', Icon: 'ta ta-empty ta-lg', Color: 'transparent', Cursor: 'default' },
    { ID: 2, Text: 'Requerido', Icon: 'ta ta-requerir ta-lg', Color: 'Orange', Cursor: 'default' },
    { ID: 3, Text: 'Subsanado', Icon: 'ta ta-subsanar ta-lg', Color: 'Blue', Cursor: 'default' },
    { ID: 4, Text: 'Aprobado', Icon: 'ta ta-aprobar ta-lg', Color: 'Green', Cursor: 'default' },
    { ID: 5, Text: 'Rechazado', Icon: 'ta ta-rechazar ta-lg', Color: '#D00000', Cursor: 'default' },
    { ID: 6, Text: 'NoRevision', Icon: 'ta ta-empty ta-lg', Color: 'transparent', Cursor: 'default' }
];

var ROLE248_STATES =
    [
        { ID: 0, Text: 'Sin definir', Icon: 'ta ta-empty ta-lg', Color: 'transparent', Cursor: 'default' },
        { ID: 1, Text: 'Aceptado', Icon: 'ta ta-plus ta-lg', Color: 'Green', Cursor: 'default' },
        { ID: 2, Text: 'Devuelto', Icon: 'ta ta-minus ta-lg', Color: '#D00000', Cursor: 'default' },
        { ID: 3, Text: 'Revisión', Icon: 'ta ta-revision ta-lg', Color: 'green', Cursor: 'default' }
    ];

var enumSOL_STATES =
{
    SinRadicar: 1,
    Radicada: 2,
    Entrámite: 3,
    Requerido: 4,
    Subsanado: 5,
    Cerrada: 6,
    Finalizada: 7,
    Cancelada: 8,
    Rechazada: 9,
    Desistida: 10
};

var enumSOL_TYPES_CLASS =
{
    AdministrativoOnly: 1,
    AdministrativoTecnico: 2
};

var enumEstadoViabilidad =
{
    SinSolicitudes: 0,
    EnTramite: 1,
    ViabilidadPendiente: 2,
    Asignado: 3,
    Desierto: 4,
};

var FILE_TYPES = [
    { type: 'Txt', ext: '.txt', color: 'black', icon: 'ta ta-txt', content_type: 'text/plain' },
    { type: 'Pdf', ext: '.pdf', color: '#DA4437', icon: 'ta ta-pdf', content_type: 'application/pdf' },
    { type: 'Word', ext: '.doc', color: '#295495', icon: 'ta ta-word', content_type: 'application/msword' },
    { type: 'Word', ext: '.docx', color: '#295495', icon: 'ta ta-word', content_type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' },
    { type: 'Excel', ext: '.xlsx', color: '#286C40', icon: 'ta ta-excel', content_type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' },
    { type: 'Excel', ext: '.xlsm', color: '#286C40', icon: 'ta ta-excel', content_type: 'application/vnd.ms-excel.sheet.macroEnabled.12' },
    { type: 'Mp3', ext: '.mp3', color: 'goldenrod', icon: 'ta ta-mp3', content_type: 'audio/mpeg' },
    { type: 'jpg', ext: '.jpg', color: 'goldenrod', icon: 'ta ta-jpg', content_type: 'image/jpeg' }
];

function getFilesContentType(arrExt) {
    var Result = [];

    for (var i = 0; i < arrExt.length; i++) {
        var Item = ListObjectGetAttribute(FILE_TYPES, 'ext', arrExt[i], 'content_type');
        if (Item != undefined)
            Result.push(Item);
    }

    return Result;
}
var enumProcesos = {
    Otorga: 1,
    Prorroga: 2,
    JuntaProgramación: 3,
    HorasPrime: 4,
    DerechosAutor: 5
}

var enumEtapas = {
    RecepcionSolicitudesOtorga: 101,
    EvalAdminOtorga: 102,
    RequerimientosOtorga: 103,
    Viabilidad: 104,
    EvalFinancieraOtorga: 105,
    EvalTecnica: 106,
    ResolLicenciaOtorga: 107,
    AnalisisTecnico: 108,
    SubdirectorMinTICApruebaOtorga: 109,
    SubdirectorMinTICNoApruebaOtorga: 110,
    NotificRadicarProrroga: 201,
    RecepcionSolicitudesProrroga: 202,
    RadicacionProrroga: 203,
    RequerimientosProrroga: 204,
    EvalFinancieraProrroga: 205,
    ResolLicenciaProrroga: 206,
    ExpedProximosVencer: 207,
    CargaDocumentTecnicos: 208,
    SubdirectorMinTICApruebaProrroga: 209,
    SubdirectorMinTICNoApruebaProrroga: 210,
    NotificRadicarJunta: 301,
    RecepcionSolicitudesJunta: 302,
    EvaluacionAdministrativa: 303,
    RequerimientosJunta: 304,
    SolicitudSubsanada: 305,
    NotificacionCargarHoras: 401,
    CargaHoras: 402,
    NotificacionDerechosAutor: 501,
    CargaDerechosAutor: 502,
}

var enumRolesNotif = {
    Concesionario: 1,
    FuncionarioMintic: 2,
    CoordinadorMintic: 3,
    SubdirectorMintic: 4,
    FuncionarioANE: 5,
    RevisorANE: 6,
    CoordinadorANE: 7,
    SubdirectorANE: 8,
}

var enumConceptos = {
    OtoSolicitudGuardasSinRadicadas: 1,
    OtoSolicitudRadicada: 2,
    OtoRespuestaSolicitud: 3,
    OtoSubsanacionNoIngresada: 4,
    OtoSubsanacionGuardadaSinRadicar: 5,
    OtoSubsanacionRadicada: 6,
    OtoSolicitudRechazadaPorVencimiento: 7,
    OtoRespuestaAprobacionRechazo: 8,
    OtoPorNoPago: 9,
    OtoInfTecnicaNoIngresada: 10,
    OtoInfTecnicaGuardadaSinRadicar: 11,
    OtoInfTecnicaRadicada: 12,
    OtoTramitefinalizadoCuandoAZNotifiqueResolucion1: 13,
    OtoSubsanacionRadicada: 14,
    OtoSolicitudRechazadaPorVencimiento2: 15,
    OtoTramitefinalizadoCuandoAZNotifiqueResolucion2: 16,
    OtoSubsanacionNoRealizada: 17,
    OtoSubsanacionRadicada: 18,
    OtoSolicitudRechazadaPorVencimiento5: 19,
    OtoSolicitudRechazadaPorVencimiento7: 20,
    OtoSolicitudRechazadaPorVencimiento8: 21,
    OtoSubdirectorMinticAprobado: 22,
    OtoSubdirectorMinticRechazo: 23,
    ProExpedienteAVencer: 24,
    ProSolicitudRadicada: 25,
    ProRespuestaAprobacionRechazo: 26,
    ProSubsanacionNoIngresada: 27,
    ProSubsanacionGuardadaSinRadicar: 28,
    ProSubsanacionRadicada: 29,
    ProSolicitudRechazadaPorVencimiento: 30,
    ProPorNoPago: 31,
    ProTramiteFinalizadoCuandoAZNotifiqueResolucion1: 32,
    ProSubsanaciónRadicada: 33,
    ProSolicitudRechazadaPorVencimiento: 34,
    ProTramiteFinalizadoCuandoAZNotifiqueResolucion2: 35,
    ProExpedientesAVencer: 36,
    ProDocumentosTecnicosNoCargados: 37,
    ProSubdirectorMinticAprobado: 38,
    ProSubdirectorMinticRechazo: 39,
    JunDebeCargarReportes: 40,
    JunSolicitudRadicada: 41,
    JunRespuesta: 42,
    JunSubsanacionNoIngresada: 43,
    JunSubsanacionPendiente: 44,
    JunSubsanacionRadicada1: 45,
    JunSolicitudRechazadaPorVencimiento1: 46,
    JunSubsanacionRadicada2: 47,
    JunSolicitudRechazadaPorVencimiento2: 48,
    JunTodasSolicitudesEvaluadasAdmin: 49,
    JunTodasSolicitudesEvaluadasSubsanada: 50,
    HorasCargarReportes: 51,
    HorasConfirmaReporteIngresado: 52,
    HorasHorasCargadasSistema: 53,
    AutorCargarDerechosAutor: 54,
    AutorDerechosAutorCargadosMintic: 55,
    AutorDerechosAutorCargadosANE: 56,

}





