

INSERT INTO [RADIO].[TEXTS]
           ([TXT_ID]
           ,[TXT_TYPE]
           ,[TXT_GROUP]
           ,[TXT_TEXT]
           ,[TXT_SYSTEM])
     VALUES
           ('MAIL_REGISTRO_USUARIO'
           ,'MAIL'
           ,'CONCESIONARIO'
           ,'<HEADER>
<ADDRESS>{NEW_USER_EMAIL}</ADDRESS>
<SUBJECT>Registro exitoso SGE</SUBJECT>
</HEADER>

<BODY>
<BR>
Se�or(a) {NEW_USER_NAME}.
<BR><BR>
Se informa que su usuario ha sido creado correctamente en el Sistema de Gesti�n de Espectro-SGE.
<BR><BR>
Usuario: {NEW_USER_LOGIN}
<BR>
Contrase�a: {NEW_USER_PASSWORD}
<BR><BR>
Para continuar con el proceso, debe acceder al SGE con el Usuario y Contrase�a asignadas y adjuntar la informaci�n jur�dica correspondiente que se detalla en la Resoluci�n MinTIC No. 410 de 2019.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
</BODY>'
           ,0)
GO


