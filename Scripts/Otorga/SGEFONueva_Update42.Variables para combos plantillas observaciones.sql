USE [SageFrontOfficePruebas]
GO

INSERT INTO [RADIO].[TEXTS]
           ([TXT_ID]
           ,[TXT_TYPE]
           ,[TXT_GROUP]
           ,[TXT_TEXT]
           ,[TXT_SYSTEM])
     VALUES
           ('COMBO_INFORMACION_OBSERVACIONES'
           ,'COMBO'
           ,'OBSERVACIONES'
           ,'[{"key": "{{Lista Observaciones}}", "name": "Inicio de lista de observaciones"},
        {"key": "{{/Lista}}", "name": "Fin de lista de observaciones"},
        {"key": "{{ConsecutivoObs}}", "name": "Consecutivo de la Observación"},		
        {"key": "{{CategoriaObs}}", "name": "Categoría de la Observación"},	
        {"key": "{{FechaObs}}", "name": "Fecha de la Observación"},	
        {"key": "{{DocRemitenteObs}}", "name": "Número de documento del Remitente de la Observación"},			
        {"key": "{{RemitenteObs}}", "name": "Remitente de la Observación"},		
        {"key": "{{CargoRemitenteObs}}", "name": "Cargo del Remitente de la Observación"},
        {"key": "{{EmailRemitenteObs}}", "name": "Email del Remitente de la Observación"},		
        {"key": "{{Observacion}}", "name": "Texto de la Observación"},	
        {"key": "{{EstadoObs}}", "name": "Estado de la Observación"},		
        {"key": "{{RespuestaObs}}", "name": "Respuesta a la Observación"}]'
           ,0)
GO
GO

INSERT INTO [RADIO].[TEXTS]
           ([TXT_ID]
           ,[TXT_TYPE]
           ,[TXT_GROUP]
           ,[TXT_TEXT]
           ,[TXT_SYSTEM])
     VALUES
           ('COMBO_INFORMACION_OBSERVACIONES_CONVOCATORIA'
           ,'COMBO'
           ,'OBSERVACIONES'
           ,'[{"key": "{{NombreConvocatoria}}", "name": "Nombre de la Convocatoria"},	
        {"key": "{{CodigoConvocatoria}}", "name": "Código de la Convocatoria"},	
        {"key": "{{AnioConvocatoria}}", "name": "Año de la Convocatoria"}]'
           ,0)
GO
GO

INSERT INTO [RADIO].[TEXTS]
           ([TXT_ID]
           ,[TXT_TYPE]
           ,[TXT_GROUP]
           ,[TXT_TEXT]
           ,[TXT_SYSTEM])
     VALUES
           ('COMBO_INFORMACION_OBSERVACIONES_ETAPA'
           ,'COMBO'
           ,'OBSERVACIONES'
           ,'[{"key": "{{NombreEtapa}}", "name": "Nombre de la Etapa"},	
        {"key": "{{FechaInicioEtapa}}", "name": "Fecha de inicio de la Etapa"},		
        {"key": "{{FechaFinEtapa}}", "name": "Fecha de fin de la Etapa"},	
        {"key": "{{DescripcionEtapa}}", "name": "Descripción de la Etapa"},
        {"key": "{{NumeroParticipantes}}", "name": "Número de Total de participantes"},		
        {"key": "{{NumeroComentarios}}", "name": "Número total de comentarios recibidos"}]'
           ,0)
GO
