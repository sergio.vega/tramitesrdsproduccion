GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] DROP CONSTRAINT [DF__SOLICITUD__SOL_E__017F0B4C]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] DROP CONSTRAINT [DF__SOLICITUD__STEP___008AE713]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] DROP CONSTRAINT [DF__SOLICITUD__SOL_R__7F96C2DA]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] DROP CONSTRAINT [DF__SOLICITUD__SOL_C__7EA29EA1]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[RADIO].[SOLICITUDES_TEMP]') AND type in (N'U'))
DROP TABLE [RADIO].[SOLICITUDES_TEMP]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[SOLICITUDES_TEMP](
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[RAD_ALFANET] [nvarchar](50) NULL,
	[SOL_CREATOR] [int] NULL,
	[SOL_CREATED_DATE] [datetime] NULL,
	[SOL_MODIFIED_DATE] [datetime] NULL,
	[SOL_LAST_STATE_DATE] [datetime] NULL,
	[SOL_REQUIRED_DATE] [datetime] NULL,
	[SOL_REQUIRED_POSTPONE] [int] NULL,
	[CUS_ID] [int] NULL,
	[CUS_IDENT] [nvarchar](32) NULL,
	[CUS_NAME] [nvarchar](200) NULL,
	[SERV_ID] [int] NULL,
	[SERV_NUMBER] [int] NULL,
	[SOL_TYPE_ID] [int] NULL,
	[SOL_STATE_ID] [int] NULL,
	[CONT_ID] [int] NULL,
	[CONT_NAME] [nvarchar](150) NULL,
	[CONT_NUMBER] [nvarchar](16) NULL,
	[CONT_EMAIL] [nvarchar](250) NULL,
	[CONT_ROLE] [int] NULL,
	[SOL_NUMBER] [int] NULL,
	[GROUP_CURRENT] [int] NULL,
	[ROLE_CURRENT] [int] NULL,
	[STEP_CURRENT] [int] NULL,
	[FIN_SEVEN_ALDIA] [bit] NULL,
	[FIN_ESTADO_CUENTA_NUMBER] [nvarchar](16) NULL,
	[FIN_ESTADO_CUENTA_DATE] [datetime] NULL,
	[FIN_REGISTRO_ALFA_NUMBER] [nvarchar](16) NULL,
	[FIN_REGISTRO_ALFA_DATE] [datetime] NULL,
	[SOL_ENDED] [int] NULL,
	[CAMPOS_SOL] [nvarchar](max) NULL,
	[Clasificacion] [nvarchar](10) NULL,
	[NivelCubrimiento] [nvarchar](10) NULL,
	[Tecnologia] [nvarchar](10) NULL,
	[NombreComunidad] [nvarchar](200) NULL,
	[IdLugarComunidad] [numeric](9, 0) NULL,
	[DireccionComunidad] [nvarchar](300) NULL,
	[CiudadComunidad] [nvarchar](50) NULL,
	[DepartamentoComunidad] [nvarchar](50) NULL,
	[CodeLugarComunidad] [nvarchar](10) NULL,
	[Territorio] [nvarchar](50) NULL,
	[ResolucionMinInterior] [nvarchar](100) NULL,
	[GrupoEtnico] [nvarchar](10) NULL,
	[NombrePuebloComunidad] [nvarchar](100) NULL,
	[NombreResguardo] [nvarchar](100) NULL,
	[NombreCabildo] [nvarchar](100) NULL,
	[NombreKumpania] [nvarchar](100) NULL,
	[NombreConsejo] [nvarchar](100) NULL,
	[NombreOrganizacionBase] [nvarchar](100) NULL,
	[Evaluaciones] [nvarchar](max) NULL,
	[Puntaje] [numeric](18, 0) NULL,
	[RazonDesempate] [nvarchar](max) NULL,
	[COMMENT] [nvarchar](50) NULL,
	[AZ_DIRECTORIO] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[SOL_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] ADD  DEFAULT ((0)) FOR [SOL_CREATOR]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] ADD  DEFAULT ((0)) FOR [SOL_REQUIRED_POSTPONE]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] ADD  DEFAULT ((0)) FOR [STEP_CURRENT]
GO

ALTER TABLE [RADIO].[SOLICITUDES_TEMP] ADD  DEFAULT ((0)) FOR [SOL_ENDED]
GO


