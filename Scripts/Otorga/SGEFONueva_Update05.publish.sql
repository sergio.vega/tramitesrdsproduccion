﻿


GO
PRINT N'Creando [RADIO].[V_CANALES_PROCESOS]...';


GO
CREATE VIEW RADIO.V_CANALES_PROCESOS
AS
SELECT        RADIO.V_PLAN_BRO.ID AS ID_PLAN_BRO, RADIO.V_PLAN_BRO.STATE, RADIO.V_PLAN_BRO.CALL_SIGN, RADIO.V_PLAN_BRO.FREQ, RADIO.V_PLAN_BRO.CLASS, RADIO.V_PLAN_BRO.CUST_TXT3, 
                         RADIO.V_PLAN_BRO.MUNICIPIO, RADIO.V_PLAN_BRO.DEPARTAMENTO, RADIO.V_PLAN_BRO.POWER, RADIO.V_PLAN_BRO.ASL, RADIO.V_PLAN_BRO.STATION_CLASS, 
                         CASE WHEN FREQ > 10 THEN 'FM' ELSE 'AM' END AS MODULATION, RADIO.SOLICITUDES.SOL_TYPE_ID
FROM            RADIO.V_EXPEDIENTES INNER JOIN
                         RADIO.SOLICITUDES ON RADIO.V_EXPEDIENTES.SERV_ID = RADIO.SOLICITUDES.SERV_ID INNER JOIN
                         RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN
GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES]...';


GO
CREATE VIEW RADIO.V_CUSTOMERS_TEMPORALES
AS
SELECT        CONVERT(int, US.ID) AS CUS_ID, US.IDENT AS CUS_IDENT, US.NAME AS CUS_NAME, ISNULL(CNT.CUS_EMAIL, N'') AS CUS_EMAIL, US.STATUS AS CUS_STATUS, US.TYPE AS CUS_TYPE, US.IDENT_CHK AS CUS_IDENT_CHK, 
                         0 AS CUS_REGIST_NUM, US.REPR_FIRSTNAME AS CUS_REPR_FIRSTNAME, US.DATE_CREATED AS CUS_DATE_CREATED, US.CREATED_BY AS CUS_CREATED_BY, US.DATE_MODIFIED AS CUS_DATE_MODIFIED, 
                         US.MODIFIED_BY AS CUS_MODIFIED_BY, US.[LEVEL] AS CUS_LEVEL, CONVERT(bit, 1) AS IS_TEMP
FROM            RADIO.USERS_TEMPORALES AS US LEFT OUTER JOIN
                         RADIO.V_CUSTOMERS_TEMPORALES_EMAIL AS CNT ON US.IDENT = CNT.REGIST_NUM
GO
PRINT N'Creando [RADIO].[V_CANALES_PROCESOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_EXPEDIENTES (RADIO)"
            Begin Extent = 
               Top = 11
               Left = 409
               Bottom = 262
               Right = 584
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "SOLICITUDES (RADIO)"
            Begin Extent = 
               Top = 33
               Left = 21
               Bottom = 273
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "V_PLAN_BRO (RADIO)"
            Begin Extent = 
               Top = 36
               Left = 682
               Bottom = 243
               Right = 932
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 14
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CANALES_PROCESOS';


GO
PRINT N'Creando [RADIO].[V_CANALES_PROCESOS].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CANALES_PROCESOS';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 243
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CNT"
            Begin Extent = 
               Top = 6
               Left = 259
               Bottom = 110
               Right = 429
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_TEMPORALES';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_TEMPORALES';


GO
PRINT N'Actualización completada.';


GO
