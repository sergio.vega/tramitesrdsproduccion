GO
INSERT INTO [RADIO].[TEXTS]
           ([TXT_ID]
           ,[TXT_TYPE]
           ,[TXT_GROUP]
           ,[TXT_TEXT]
           ,[TXT_SYSTEM])
     VALUES
           ('OBSERVACIONES_SQL'
           ,'OBSERVACIONES'
           ,'SQL'
           ,'Declare @idConvocatoria int, @idEtapa int
set @idConvocatoria = {{IdConvocatoria}}
set @idEtapa = (Select top 1 E.ID_ETAPA from RADIO.OBSERVACIONES_ETAPAS E
				inner join radio.OBSERVACIONES O on E.ID_ETAPA = O.ETAPA and o.RESPUESTA is not null
				where E.ID_CONVOCATORIA = @idConvocatoria  order by E.ID_ETAPA desc)

--Convocatoria
Select 
NOMBRE_CONVOCATORIA ''{{NombreConvocatoria}}'', 
CODIGO_CONVOCATORIA ''{{CodigoConvocatoria}}'', 
ANIO_CONVOCATORIA ''{{AnioConvocatoria}}''
from RADIO.OBSERVACIONES_CONVOCATORIAS 
where id = @idConvocatoria 

--Etapa
Select 
NOMBRE_ETAPA ''{{NombreEtapa}}'', 
Format([FECHA_INICIO],''dd/MM/yyyy'') ''{{FechaInicioEtapa}}'', 
Format([FECHA_FIN],''dd/MM/yyyy'') ''{{FechaFinEtapa}}'', 
DESCRIPCION ''{{DescripcionEtapa}}''
from RADIO.OBSERVACIONES_ETAPAS
where ID_ETAPA = @idEtapa

Select 
Count(*) ''{{NumeroComentarios}}'',
Count (Distinct DOCUMENTO) ''{{NumeroParticipantes}}''
from RADIO.OBSERVACIONES 
where ID_CONVOCATORIA = @idConvocatoria  and ETAPA = @idEtapa and RESPUESTA is not null

--Observaciones
Select  
''*'' ''{{Lista Observaciones}}'',
CONSECUTIVO ''{{ConsecutivoObs}}'',
TIPO_EMISORA ''{{TipoEmisoraObs}}'',
TIPO_DOCUMENTO + '':'' + DOCUMENTO ''{{DocRemitenteObs}}'',
NOMBRE ''{{RemitenteObs}}'',
EMAIL ''{{EmailRemitenteObs}}'',
NOMBRE_ORGANIZACION ''{{OrganizacionRemitenteObs}}'',
CARGO ''{{CargoRemitenteObs}}'',
OBSERVACION ''{{Observacion}}'',
ESTADO ''{{EstadoObs}}'',
ID_FUNCIONARIO ''{{IdFuncionarioObs}}'',
ID_COORDINADOR ''{{IdCoordinadorObs}}'',
Format([FECHA_OBSERVACION],''dd/MM/yyyy'') ''{{FechaObs}}'',
RESPUESTA ''{{RespuestaObs}}'', 
Format([FECHA_RESPUESTA],''dd/MM/yyyy'') ''{{FechaRespuestaObs}}''
from RADIO.OBSERVACIONES 
where ID_CONVOCATORIA = @idConvocatoria  and ETAPA = @idEtapa and RESPUESTA is not null'
           ,0)
GO


