﻿
GO
PRINT N'Modificando [dbo].[ManifestacionEmisoraComercial]...';


GO
ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [CodDepartamento] NCHAR (2) NOT NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [ContactoDireccion] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [ContactoEmail] VARCHAR (MAX) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [ContactoNombres] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [ContactoTelefono] VARCHAR (15) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [InteresadoDireccion] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [InteresadoEmail] VARCHAR (MAX) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [InteresadoTelefono] VARCHAR (15) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [RazonSocial] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [RepLegalDireccion] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [RepLegalEmail] VARCHAR (MAX) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [RepLegalNombres] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComercial] ALTER COLUMN [RepLegalTelefono] VARCHAR (15) NULL;


GO
PRINT N'Modificando [dbo].[ManifestacionEmisoraComunitaria]...';


GO
ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [CodDepartamento] NCHAR (2) NOT NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [ContactoDireccion] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [ContactoEmail] VARCHAR (MAX) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [ContactoNombres] VARCHAR (100) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [ContactoTelefono] VARCHAR (15) NULL;

ALTER TABLE [dbo].[ManifestacionEmisoraComunitaria] ALTER COLUMN [RazonSocial] VARCHAR (200) NULL;


GO
PRINT N'Modificando [RADIO].[V_CUSTOMERS]...';


GO
ALTER VIEW RADIO.V_CUSTOMERS
AS
SELECT        CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH,         CUS_STATUS, CUS_TYPE, CUS_IDENT_CHK, CUS_REGIST_NUM, CUS_REPR_FIRSTNAME, CUS_DATE_CREATED, CUS_CREATED_BY, CUS_DATE_MODIFIED, 
                         CUS_MODIFIED_BY, CUS_LEVEL, IS_TEMP
FROM            RADIO.V_CUSTOMERS_MANAGER
UNION ALL
SELECT        CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, 0 AS CUS_BRANCH, CUS_STATUS, CUS_TYPE, CUS_IDENT_CHK, CUS_REGIST_NUM, CUS_REPR_FIRSTNAME, CUS_DATE_CREATED, CUS_CREATED_BY, CUS_DATE_MODIFIED, 
                         CUS_MODIFIED_BY, CUS_LEVEL, IS_TEMP
FROM            RADIO.V_CUSTOMERS_TEMPORALES
WHERE        (CUS_IDENT NOT IN
                             (SELECT        CUS_IDENT
                               FROM            RADIO.V_CUSTOMERS_MANAGER))
GO
PRINT N'Creando [RADIO].[V_INFO_REGISTRO_MANIFESTACION]...';


GO
CREATE VIEW RADIO.V_INFO_REGISTRO_MANIFESTACION
AS
SELECT        dbo.ManifestacionEmisoraComercial.Nit AS Identificacion, dbo.ManifestacionEmisoraComercial.RazonSocial AS NombreRazonSocial, dbo.ManifestacionEmisoraComercial.IdMunicipio AS IdLugar, 
                         dbo.ManifestacionEmisoraComercial.InteresadoDireccion AS DireccionCorrespondencia, dbo.ManifestacionEmisoraComercial.InteresadoTelefono AS Telefono, dbo.ManifestacionEmisoraComercial.InteresadoEmail AS Email, 
                         dbo.ManifestacionEmisoraComercial.TipoEmpresaId AS TipoDocumento, RADIO.V_MUNICIPIOS.NAME AS Ciudad, RADIO.V_DEPARTAMENTOS.NAME AS Departamento, RADIO.V_MUNICIPIOS.CODE_AREA AS CodeLugar, 
                         dbo.ManifestacionEmisoraComercial.RepLegalNombres AS NombreRepresentante, dbo.ManifestacionEmisoraComercial.RepLegalCedula AS IdentificacionRepresentante, RADIO.V_DEPARTAMENTOS.CODE_AREA AS CodeAreaDepartamento
FROM            dbo.ManifestacionEmisoraComercial INNER JOIN
                         RADIO.V_MUNICIPIOS ON dbo.ManifestacionEmisoraComercial.IdMunicipio = RADIO.V_MUNICIPIOS.ID INNER JOIN
                         RADIO.V_DEPARTAMENTOS ON RADIO.V_MUNICIPIOS.CODE_DEPARTAMENTO = RADIO.V_DEPARTAMENTOS.CODE_AREA
UNION ALL
SELECT        dbo.ManifestacionEmisoraComunitaria.Nit AS Identificacion, dbo.ManifestacionEmisoraComunitaria.RazonSocial AS NombreRazonSocial, dbo.ManifestacionEmisoraComunitaria.IdMunicipio AS IdLugar, 
                         dbo.ManifestacionEmisoraComunitaria.ContactoDireccion AS DireccionCorrespondencia, dbo.ManifestacionEmisoraComunitaria.ContactoTelefono AS Telefono, 
                         dbo.ManifestacionEmisoraComunitaria.ContactoEmail AS Email, NULL AS TipoDocumento, RADIO.V_MUNICIPIOS.NAME AS Ciudad, RADIO.V_DEPARTAMENTOS.NAME AS Departamento, 
                         RADIO.V_MUNICIPIOS.CODE_AREA AS CodeLugar, NULL AS NombreRepresentante, NULL AS IdentificacionRepresentante, RADIO.V_DEPARTAMENTOS.CODE_AREA AS CodeAreaDepartamento
FROM            dbo.ManifestacionEmisoraComunitaria INNER JOIN
                         RADIO.V_MUNICIPIOS ON dbo.ManifestacionEmisoraComunitaria.IdMunicipio = RADIO.V_MUNICIPIOS.ID INNER JOIN
                         RADIO.V_DEPARTAMENTOS ON RADIO.V_MUNICIPIOS.CODE_DEPARTAMENTO = RADIO.V_DEPARTAMENTOS.CODE_AREA
GO
PRINT N'Modificando [RADIO].[V_CUSTOMERS].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 296
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 90
         End
         Begin Table = "US1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 85
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CNT"
            Begin Extent = 
               Top = 6
               Left = 517
               Bottom = 103
               Right = 687
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 16
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS';


GO
PRINT N'Creando [RADIO].[V_INFO_REGISTRO_MANIFESTACION].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_INFO_REGISTRO_MANIFESTACION';


GO
PRINT N'Creando [RADIO].[V_INFO_REGISTRO_MANIFESTACION].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_INFO_REGISTRO_MANIFESTACION';


GO
PRINT N'Actualización completada.';


GO
