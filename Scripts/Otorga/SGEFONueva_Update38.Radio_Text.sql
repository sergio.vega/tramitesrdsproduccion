USE [SageFrontOfficePruebas]
GO

INSERT INTO RADIO.TEXTS 
VALUES('ASIGNACIONES_OBSERV','SQL', 'AUTOMATIC_ASSIGN',
' SELECT ROLE, USR_ID Into #T1 FROM (SELECT TOP (1) ROLE, USR_ID FROM RADIO.V_USUA_OBS_ROLE1 WHERE (USR_GROUP = 1) ORDER BY NOBS, RANDOM) AS T1
Select ROLE USR_ROLE, ASG.USR_ID, USR_NAME, USR_EMAIL
From
(
SELECT T_ROLES.ROLE, ISNULL(T_TODOS.USR_ID, - 1) AS USR_ID
FROM      
(
SELECT 1 AS ROLE UNION SELECT 4 AS ROLE
) AS T_ROLES
LEFT JOIN
(
SELECT ROLE, USR_ID FROM #T1 T1
UNION ALL
SELECT ROLE, USR_ID FROM (SELECT TOP (1) ROLE, USR_ID FROM RADIO.V_USUA_OBS_ROLE4 WHERE (USR_GROUP = 1) ORDER BY NOBS, RANDOM) T4

) AS T_TODOS ON T_ROLES.ROLE = T_TODOS.ROLE
) ASG
left Join RADIO.USUARIOS USR On ASG.USR_ID=USR.USR_ID;

Drop Table #T1',0)

INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_REASIGN_OBS_APROBAR', N'MAIL', N'REASIGN', N'<HEADER>
<ADDRESS>{TO.USR_EMAIL}</ADDRESS>
<SUBJECT>Aprobación solicitud de reasignación</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {TO.USR_NAME}. 
<BR><BR>
Se informa que {FROM.USR_NAME} ({FROM.USR_ROLE}) ha aceptado la solicitud de reasignación.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
<BODY>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_REASIGN_OBS_ASIGNAR', N'MAIL', N'REASIGN', N'<HEADER>
<ADDRESS>{TO.USR_EMAIL}</ADDRESS>
<SUBJECT>Asignación de observación</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {TO.USR_NAME} ({TO.USR_ROLE}). 
<BR><BR>
Se informa que {FROM.USR_NAME} ({FROM.USR_ROLE}) le ha asignado la observación.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
<BODY>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_REASIGN_OBS_DESASIGNAR', N'MAIL', N'REASIGN', N'<HEADER>
<ADDRESS>{TO.USR_EMAIL}</ADDRESS>
<SUBJECT>Desasignación de observación</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {TO.USR_NAME}. 
<BR><BR>
Se informa que {FROM.USR_NAME} ({FROM.USR_ROLE}) le ha asignado la observación a otro colaborador.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
<BODY>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_REASIGN_OBS_RECHAZAR', N'MAIL', N'REASIGN', N'<HEADER>
<ADDRESS>{TO.USR_EMAIL}</ADDRESS>
<SUBJECT>Rechazo solicitud de reasignación</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {TO.USR_NAME}. 
<BR><BR>
Se informa que {FROM.USR_NAME} ({FROM.USR_ROLE}) ha rechazado la solicitud de reasignación.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
<BODY>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_REASIGN_OBS_SOLICITAR', N'MAIL', N'REASIGN', N'<HEADER>
<ADDRESS>{TO.USR_EMAIL}</ADDRESS>
<SUBJECT>Solicitud de reasignación</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {TO.USR_NAME}. 
<BR><BR>
Se informa que {FROM.USR_NAME} ({FROM.USR_ROLE}) solicita reasignación de la observación.
<BR>
<B>Comentario: </B> {FROM.USR_COMMENT}
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
<BODY>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_OBS_HEADER', N'MAIL', N'OBSERVACION', N'<TABLE>
  <TR><TD><B>Consecutivo Observación</B></TD><TD>{CONSECUTIVO}</TD></TR>
  <TR><TD><B>Etapa</B></TD><TD>{ETAPA} </TD></TR>
  <TR><TD><B>Tipo Emisora</B></TD><TD>{TIPO_EMISORA}</TD></TR>
  <TR><TD><B>Categoría</B></TD><TD>{NOMBRE_CATEGORIA}</TD></TR>
  <TR><TD><B>Organización</B></TD><TD>{NOMBRE_ORGANIZACION}</TD></TR>
</TABLE>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'LOG_OBSERVACION_DEVOLVER', N'LOG', N'ADMINISTRATIVO', N'<HEADER>
<EVENT>Respuesta observación</EVENT>
<ICON>devolver</ICON>
<DESC>Respuesta de observación devuelta</DESC>
</HEADER>', 0)

INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'LOG_REASIGN_OBS_ASIGNAR', N'LOG', N'REASIGN', N'<HEADER>
<EVENT>Asignación de Observación</EVENT>
<ICON>usuarios</ICON>
<DESC>El usuario asignó la Observación a {TO.USR_NAME} ({TO.USR_ROLE})</DESC>
</HEADER>', 0)
INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'LOG_REASIGN_OBS_DESASIGNAR', N'LOG', N'REASIGN', N'<HEADER>
<EVENT>Desasignación de Observación</EVENT>
<ICON>user-minus</ICON>
<DESC>El usuario desasignó la observación a {TO.USR_NAME} ({TO.USR_ROLE})</DESC>
</HEADER>', 0)

INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'LOG_OBS_APROBADA', N'LOG', N'SOLICITUD', N'<HEADER>
<EVENT>Observación aprobada</EVENT>
<ICON>hand</ICON>
<DESC>La respuesta de la observación quedó aprobada</DESC>
</HEADER>', 0)

INSERT [RADIO].[TEXTS] ([TXT_ID], [TXT_TYPE], [TXT_GROUP], [TXT_TEXT], [TXT_SYSTEM]) VALUES (N'MAIL_OBSERVACION_DEVUELTO', N'MAIL', N'ADMINISTRATIVO', N'<HEADER>
<ADDRESS>{EMAIL1}</ADDRESS>
<SUBJECT>Respuesta observación devuelta</SUBJECT>
</HEADER>

<BODY>
{MAIL_OBS_HEADER}
<BR>
Señor(a) {NAME1} .
<BR><BR>
Se informa que {NAME4} (Coordinador) ha devuelto la respuesta a la observación para su revisión.
<BR><BR><BR>
{MAIL_MINTIC_SOL_FOOTER}
</BODY>', 0)

INSERT INTO  RADIO.TEXTS
Select 'ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR_OTORGA', 'ALFA', 'REGISTRO',
'<HEADER>
<LOGO>MINTIC_LOGO</LOGO>
<FILE>Registro_Requerir_Tecnico_{SOL_UID}_{AHORA_DET}.pdf</FILE>
<DETAIL>Notificación de requerimiento técnico de la solicitud: {SOL_TYPE_NAME}, correspondiente al expediente: {Expediente.SERV_NUMBER} del cliente: {Expediente.Operador.CUS_NAME}</DETAIL>
</HEADER>

<BODY>
<P LEFT>
<B>Bogotá, D. C., {AHORA}

<B>Señores:</B>
{Expediente.Operador.CUS_NAME}
{Expediente.Operador.CUS_IDENT}
Asunto: Requerimiento técnico solicitud {SOL_TYPE_NAME}
Radicado número: {Radicado}
Expediente número: {Expediente.SERV_NUMBER}
Emisora: {Expediente.SERV_NAME}</B>
</P>

<P JUSTIFIED>
Respetados señores:

Por medio de la presente, la Agencia Nacional del Espectro solicita al concesionario <B>{Expediente.Operador.CUS_NAME}</B> con No. de Nit <B>{Expediente.Operador.CUS_IDENT}</B>; el requerimiento de la solicitud realizada a través de <B>{Contacto.CONT_NAME}</B> quien en calidad de <B>{Contacto.CONT_TITLE}</B>, solicita <B>{SOL_TYPE_NAME}</B> para la concesión <B>{Expediente.SERV_NAME}</B>. La información requerida a continuación debe ser subsanada en su totalidad a través de la plataforma por la cual realizó el ingreso de la solicitud en un transcurso no mayor a 30 días calendario. 
</P>

<P LEFT>
<B>Parámetros Técnicos requeridos:</B>
Lista de parámetros requeridos
<LIST>TechRequerir</LIST>
</P>

<P CENTER>
Consulte en la página <B><A>www.mintic.gov.co</A></B>
<B>Importante:</B> El presente formato no constituye aprobación de la solicitud.
</P>
</BODY>

<FOOTER>
<SIGN>{MINTIC_SUBDIRECTOR_TITLE}</SIGN>
<TEXT>{MINTIC_PDF_FOOTER}</TEXT>
</FOOTER>', 0