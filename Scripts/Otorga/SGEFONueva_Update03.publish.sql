﻿

GO
PRINT N'Creando [RADIO].[V_CONTACTOS_SIN_EXPEDIENTE]...';


GO
CREATE VIEW RADIO.V_CONTACTOS_SIN_EXPEDIENTE
AS
SELECT        CONVERT(int, ICSM_PRUEBA.dbo.USERS_CNTCT.ID) AS CONT_ID, US.IDENT AS CUS_IDENT, ICSM_PRUEBA.dbo.USERS_CNTCT.FIRSTNAME AS CONT_NAME, CONVERT(int, ICSM_PRUEBA.dbo.USERS_CNTCT.ROLE) 
                         AS CONT_ROLE, ICSM_PRUEBA.dbo.USERS_CNTCT.TEL AS CONT_TEL, ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM AS CONT_NUMBER, ICSM_PRUEBA.dbo.USERS_CNTCT.TITLE AS CONT_TITLE, 
                         ICSM_PRUEBA.dbo.USERS_CNTCT.EMAIL AS CONT_EMAIL
FROM            ICSM_PRUEBA.dbo.USERS_CNTCT INNER JOIN
                         ICSM_PRUEBA.dbo.USERS AS US ON ICSM_PRUEBA.dbo.USERS_CNTCT.OPER_ID = US.ID
WHERE        (ICSM_PRUEBA.dbo.USERS_CNTCT.CUST_TXT4 = '1') AND (ICSM_PRUEBA.dbo.USERS_CNTCT.ROLE IN (1, 2, 8)) AND (ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM IS NOT NULL) AND 
                         (LEN(ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM) > 1) AND (US.STATUS <> 'uNAC')
UNION ALL
SELECT        CONVERT(int, RADIO.USERS_CNTCT_TEMPORALES.ID) AS CONT_ID, US.IDENT AS CUS_IDENT, RADIO.USERS_CNTCT_TEMPORALES.FIRSTNAME AS CONT_NAME, CONVERT(int, RADIO.USERS_CNTCT_TEMPORALES.ROLE) 
                         AS CONT_ROLE, RADIO.USERS_CNTCT_TEMPORALES.TEL AS CONT_TEL, RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM AS CONT_NUMBER, RADIO.USERS_CNTCT_TEMPORALES.TITLE AS CONT_TITLE, 
                         RADIO.USERS_CNTCT_TEMPORALES.EMAIL AS CONT_EMAIL
FROM            RADIO.USERS_CNTCT_TEMPORALES INNER JOIN
                         RADIO.USERS_TEMPORALES AS US ON RADIO.USERS_CNTCT_TEMPORALES.OPER_ID = US.ID
WHERE        (RADIO.USERS_CNTCT_TEMPORALES.CUST_TXT4 = '1') AND (RADIO.USERS_CNTCT_TEMPORALES.ROLE IN (1, 2, 8)) AND (RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM IS NOT NULL) AND 
                         (LEN(RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM) > 1) AND (US.STATUS <> 'uNAC')
GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_MANAGER]...';


GO
CREATE VIEW RADIO.V_CUSTOMERS_MANAGER
AS
SELECT        CONVERT(int, US.ID) AS CUS_ID, US.IDENT AS CUS_IDENT, US.NAME AS CUS_NAME, ISNULL(CNT.CUS_EMAIL, '') AS CUS_EMAIL, CONVERT(int, US.REGIST_NUM) AS CUS_BRANCH, US.STATUS AS CUS_STATUS, 
                         US.TYPE AS CUS_TYPE, US.IDENT_CHK AS CUS_IDENT_CHK, US.REGIST_NUM AS CUS_REGIST_NUM, US.REPR_FIRSTNAME AS CUS_REPR_FIRSTNAME, US.DATE_CREATED AS CUS_DATE_CREATED, 
                         US.CREATED_BY AS CUS_CREATED_BY, US.DATE_MODIFIED AS CUS_DATE_MODIFIED, US.MODIFIED_BY AS CUS_MODIFIED_BY, US.[LEVEL] AS CUS_LEVEL, CONVERT(bit, 0) AS IS_TEMP
FROM            ICSM_PRUEBA.dbo.USERS AS US INNER JOIN
                             (SELECT        US.IDENT
                               FROM            ICSM_PRUEBA.dbo.USERS AS US INNER JOIN
                                                         ICSM_PRUEBA.dbo.SERV_LICENCE AS SERV ON US.ID = SERV.OWNER_ID
                               WHERE        (US.STATUS <> 'uNAC') AND (SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B')) AND (SERV.CLASS IN (4, 13, 27))
                               GROUP BY US.IDENT) AS US1 ON US.IDENT = US1.IDENT AND US.REGIST_NUM = 0 LEFT OUTER JOIN
                         RADIO.V_CUSTOMERS_EMAIL AS CNT ON US.ID = CNT.OPER_ID
GO
PRINT N'Creando [RADIO].[V_DEPARTAMENTOS]...';


GO
CREATE VIEW RADIO.V_DEPARTAMENTOS
AS
SELECT        TOP (100) PERCENT ID, CONVERT(int, CODE) AS CODE_AREA, NAME
FROM            ICSM_PRUEBA.dbo.CITIES
WHERE        (CODE = PROVINCE) AND (CODE <> '00')
ORDER BY NAME
GO
PRINT N'Creando [RADIO].[V_MUNICIPIOS]...';


GO
CREATE VIEW RADIO.V_MUNICIPIOS
AS
SELECT        TOP (100) PERCENT ID, CONVERT(int, CODE) AS CODE_AREA, NAME, CONVERT(int, PROVINCE) AS CODE_DEPARTAMENTO
FROM            ICSM_PRUEBA.dbo.CITIES
WHERE        (CODE <> PROVINCE) AND (CODE <> '00')
ORDER BY PROVINCE, NAME
GO
PRINT N'Creando [RADIO].[V_PLAN_BRO]...';


GO
CREATE VIEW RADIO.[V_PLAN_BRO]
AS
SELECT        ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.ID, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.STATE, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.CALL_SIGN, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.FREQ, 
                         ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.CLASS, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.CUST_TXT3, CONVERT(int, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.CODE_AREA) AS CODE_AREA_MUNICIPIO, 
                         ICSM_PRUEBA.dbo.CITIES.NAME AS MUNICIPIO, CONVERT(int, ICSM_PRUEBA.dbo.CITIES.PROVINCE) AS CODE_AREA_DEPARTAMENTO, CITIES_1.NAME AS DEPARTAMENTO, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.POWER, 
                         ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.ASL, ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.STATION_CLASS
FROM            ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC INNER JOIN
                         ICSM_PRUEBA.dbo.CITIES ON ICSM_PRUEBA.dbo.PLAN_BRO_MINTIC.CODE_AREA = ICSM_PRUEBA.dbo.CITIES.CODE INNER JOIN
                         ICSM_PRUEBA.dbo.CITIES AS CITIES_1 ON ICSM_PRUEBA.dbo.CITIES.PROVINCE = CITIES_1.CODE
GO
PRINT N'Creando [RADIO].[V_PLAN_BRO_PROYECTADOS]...';


GO
CREATE VIEW RADIO.V_PLAN_BRO_PROYECTADOS
AS
SELECT        ID, STATE, CALL_SIGN, FREQ, CLASS, CUST_TXT3, CODE_AREA_MUNICIPIO, MUNICIPIO, CODE_AREA_DEPARTAMENTO, DEPARTAMENTO, POWER, ASL, STATION_CLASS
FROM            RADIO.V_PLAN_BRO
WHERE        (STATE = N'PROYECTADO')
GO
PRINT N'Creando [RADIO].[V_CONTACTOS_SIN_EXPEDIENTE].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[7] 2[46] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CONTACTOS_SIN_EXPEDIENTE';


GO
PRINT N'Creando [RADIO].[V_CONTACTOS_SIN_EXPEDIENTE].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CONTACTOS_SIN_EXPEDIENTE';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_MANAGER].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "US1"
            Begin Extent = 
               Top = 6
               Left = 271
               Bottom = 85
               Right = 441
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CNT"
            Begin Extent = 
               Top = 6
               Left = 479
               Bottom = 102
               Right = 649
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_MANAGER';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_MANAGER].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_MANAGER';


GO
PRINT N'Creando [RADIO].[V_DEPARTAMENTOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CITIES (ICSM_PRUEBA.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 241
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_DEPARTAMENTOS';


GO
PRINT N'Creando [RADIO].[V_DEPARTAMENTOS].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_DEPARTAMENTOS';


GO
PRINT N'Creando [RADIO].[V_MUNICIPIOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CITIES (ICSM_PRUEBA.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_MUNICIPIOS';


GO
PRINT N'Creando [RADIO].[V_MUNICIPIOS].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_MUNICIPIOS';


GO
PRINT N'Creando [RADIO].[V_PLAN_BRO].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PLAN_BRO_MINTIC (ICSM_PRUEBA.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 214
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CITIES (ICSM_PRUEBA.dbo)"
            Begin Extent = 
               Top = 6
               Left = 252
               Bottom = 136
               Right = 428
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CITIES_1"
            Begin Extent = 
               Top = 6
               Left = 466
               Bottom = 136
               Right = 642
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_PLAN_BRO';


GO
PRINT N'Creando [RADIO].[V_PLAN_BRO].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_PLAN_BRO';


GO
PRINT N'Creando [RADIO].[V_PLAN_BRO_PROYECTADOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_PLAN_BRO (RADIO)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 288
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_PLAN_BRO_PROYECTADOS';


GO
PRINT N'Creando [RADIO].[V_PLAN_BRO_PROYECTADOS].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_PLAN_BRO_PROYECTADOS';


GO
PRINT N'Actualización completada.';


GO
