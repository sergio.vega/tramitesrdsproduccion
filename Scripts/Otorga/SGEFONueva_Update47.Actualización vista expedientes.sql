
ALTER VIEW [RADIO].[V_EXPEDIENTES]
AS
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT  CUS_IDENT, US.NAME  CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL, '')  CUS_EMAIL, PLANB.STATION_CLASS  STATION_CLASS, PLANB.STATE  STATE, C.NAME  AS DEPTO, PLANB.CITY  CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN  CALL_SIGN, isnull(FM.NAME, 'NA')  SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'FM' MODULATION, SERV.STATUS  SERV_STATUS, SERV.CUST_TXT3  CUST_TXT3, CAST(0 AS bit) 
                         AS EsTemporal, CONVERT(int, PLANB.CODE_AREA) AS CODE_AREA_MUNICIPIO, PLANB.CODE_AREA AS MUNICIPIO
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].FM_STATION AS FM ON FM.LIC_ID = LIC.ID AND FM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = FM.CALL_SIGN /*And PLANB.NUMBER=SERV.NUMBER*/ JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = PLANB.CODE_AREA LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') AND LIC.NUMBER IS NOT NULL
UNION ALL
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT CUS_IDENT, US.NAME CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL, '') CUS_EMAIL, PLANB.STATION_CLASS  STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, isnull(AM.NAME, 'NA') SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'AM' MODULATION, SERV.STATUS SERV_STATUS, SERV.CUST_TXT3, CAST(0 AS bit) 
                         AS EsTemporal, CONVERT(int, PLANB.CODE_AREA) AS CODE_AREA_MUNICIPIO, PLANB.CODE_AREA AS MUNICIPIO
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].LFMF_STATION AS AM ON AM.LIC_ID = LIC.ID AND AM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = AM.CALL_SIGN JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = PLANB.CODE_AREA LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') AND LIC.NUMBER IS NOT NULL
UNION ALL
SELECT        SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, 
                         MODULATION, SERV_STATUS, CUST_TXT3, CAST(1 AS bit) AS EsTemporal, CODE_AREA_MUNICIPIO, CAST('11001' AS nvarchar) AS MUNICIPIO
FROM            [SageFrontOfficePruebas].RADIO.EXPEDIENTES_TEMPORALES

GO

