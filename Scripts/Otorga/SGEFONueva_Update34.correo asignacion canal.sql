insert into radio.TEXTS (TXT_ID,TXT_TYPE,TXT_GROUP, TXT_TEXT, TXT_SYSTEM) values ('MAIL_ASIGNACION_CANAL','MAIL','MINTIC','<HEADER>
<ADDRESS>{Contacto.CONT_EMAIL}</ADDRESS>
<SUBJECT>Radicado {Radicado} Solicitud {SOL_TYPE_NAME}</SUBJECT>
</HEADER>

<BODY>
Se�or usuario cordial saludo.
<BR><BR>
Le informamos que de acuerdo con su solicitud se le ha asignado temporalmente el canal {NEW_CALL_SIGN}, para dar continuidad con el proceso debe realizar el pago correspondiente e ingresar la documentaci�n t�cnica en la plataforma del Sistema de Gesti�n de Espectro- SGE en los plazos que indica la resoluci�n 415 de 2010.
<BR><BR><BR>
{MAIL_CONCESIONARIO_SOL_FOOTER}
</BODY>','false')