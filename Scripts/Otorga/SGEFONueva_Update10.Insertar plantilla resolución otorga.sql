
INSERT INTO [RADIO].[RESOLUCION_PLANTILLAS]
           ([RES_PLT_UID]
           ,[RES_PLT_NAME]
           ,[SOL_TYPE_ID]
           ,[RES_PLT_PARSER]
           ,[RES_PLT_CONTENT]
           ,[RES_PLT_CODIGO]
           ,[RES_PLT_VERSION]
           ,[RES_PLT_STATUS]
           ,[RES_PLT_PDF_FILE])
     SELECT NEWID()
      ,[RES_PLT_NAME]
      ,14
      ,[RES_PLT_PARSER]
      ,[RES_PLT_CONTENT]
      ,[RES_PLT_CODIGO]
      ,[RES_PLT_VERSION]
      ,[RES_PLT_STATUS]
      ,[RES_PLT_PDF_FILE]
  FROM [RADIO].[RESOLUCION_PLANTILLAS]
  WHERE RES_PLT_NAME = 'RDS Prorroga concesión comercial'
GO


