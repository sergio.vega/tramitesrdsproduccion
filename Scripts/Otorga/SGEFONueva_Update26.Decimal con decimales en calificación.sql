﻿


GO
/*
El tipo de la columna PuntajeMinimoViabilidadAutomatica de la tabla [RADIO].[CONFIGURACION_PROCESO] es actualmente  DECIMAL (18) NULL pero se está cambiando a  DECIMAL (18, 11) NULL. Puede que se pierdan datos.
*/

IF EXISTS (select top 1 1 from [RADIO].[CONFIGURACION_PROCESO])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
El tipo de la columna Puntaje de la tabla [RADIO].[SOLICITUDES] es actualmente  NUMERIC (18) NULL pero se está cambiando a  NUMERIC (18, 11) NULL. Puede que se pierdan datos.
*/

IF EXISTS (select top 1 1 from [RADIO].[SOLICITUDES])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
/*
Se está quitando la tabla [RADIO].[CALIFICACIONES_SOLICITUDES]. La implementación se detendrá si la tabla contiene datos.
*/

IF EXISTS (select top 1 1 from [RADIO].[CALIFICACIONES_SOLICITUDES])
    RAISERROR (N'Se detectaron filas. La actualización del esquema va a terminar debido a una posible pérdida de datos.', 16, 127) WITH NOWAIT

GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_SOL_ENDED]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_SOL_ENDED];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_CUS_IDENT]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_CUS_IDENT];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_STEP_CURRENT]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_STEP_CURRENT];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_SOL_REQUIRED_POSTPONE]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_SOL_REQUIRED_POSTPONE];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_CUS_ID]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_CUS_ID];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_ID_SOL]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_ID_SOL];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_RAD_ALFANET]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_RAD_ALFANET];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_FECHA_RAD_ALFANET]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_FECHA_RAD_ALFANET];


GO
PRINT N'Quitando [RADIO].[DF_SOLICITUDES_SOL_CREATOR]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [DF_SOLICITUDES_SOL_CREATOR];


GO
PRINT N'Quitando [RADIO].[FK_DOCUMENTOS_CONFIGURACION_CONFIGURACION_PROCESO]...';


GO
ALTER TABLE [RADIO].[DOCUMENTOS_CONFIGURACION] DROP CONSTRAINT [FK_DOCUMENTOS_CONFIGURACION_CONFIGURACION_PROCESO];


GO
PRINT N'Quitando [RADIO].[FK_RESOLUCIONES_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCIONES] DROP CONSTRAINT [FK_RESOLUCIONES_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_LOGS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[LOGS] DROP CONSTRAINT [FK_LOGS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_CONTEOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[CONTEOS] DROP CONSTRAINT [FK_CONTEOS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_CALIFICACIONES_SOLICITUDES_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[CALIFICACIONES_SOLICITUDES] DROP CONSTRAINT [FK_CALIFICACIONES_SOLICITUDES_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_RESOLUCION_ANALISIS_VIABILIDAD_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCION_ANALISIS_VIABILIDAD] DROP CONSTRAINT [FK_RESOLUCION_ANALISIS_VIABILIDAD_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_RESOLUCIONES_VIABILIDAD_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCIONES_VIABILIDAD] DROP CONSTRAINT [FK_RESOLUCIONES_VIABILIDAD_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_SOLICITUDES_ESTADOS_SOL]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [FK_SOLICITUDES_ESTADOS_SOL];


GO
PRINT N'Quitando [RADIO].[FK_SOLICITUDES_TIPOS_SOL]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] DROP CONSTRAINT [FK_SOLICITUDES_TIPOS_SOL];


GO
PRINT N'Quitando [RADIO].[FK_ANEXOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[ANEXOS] DROP CONSTRAINT [FK_ANEXOS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_SOL_USERS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[SOL_USERS] DROP CONSTRAINT [FK_SOL_USERS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_COMUNICADOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[COMUNICADOS] DROP CONSTRAINT [FK_COMUNICADOS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_DOCUMENTS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[DOCUMENTS] DROP CONSTRAINT [FK_DOCUMENTS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_TECH_ANALISIS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[TECH_ANALISIS] DROP CONSTRAINT [FK_TECH_ANALISIS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[FK_RESOLUCION_ANALISIS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] DROP CONSTRAINT [FK_RESOLUCION_ANALISIS_SOLICITUDES];


GO
PRINT N'Quitando [RADIO].[CALIFICACIONES_SOLICITUDES]...';


GO
DROP TABLE [RADIO].[CALIFICACIONES_SOLICITUDES];


GO
PRINT N'Iniciando recompilación de la tabla [RADIO].[CONFIGURACION_PROCESO]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [RADIO].[tmp_ms_xx_CONFIGURACION_PROCESO] (
    [ID]                                UNIQUEIDENTIFIER NOT NULL,
    [SOL_TYPE]                          INT              NOT NULL,
    [CLASS]                             NUMERIC (4)      NOT NULL,
    [NumeroResolucion]                  NVARCHAR (50)    NOT NULL,
    [FechaResolucion]                   DATETIME         NOT NULL,
    [Nombre]                            NVARCHAR (100)   NOT NULL,
    [ConfiguracionCalificaciones]       NVARCHAR (MAX)   NOT NULL,
    [FechaInicioRecepcionSolicitudes]   DATETIME         NOT NULL,
    [FechaFinRecepcionSolicitudes]      DATETIME         NOT NULL,
    [PuntajeMinimoViabilidadAutomatica] DECIMAL (18, 11) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CONFIGURACION_PROCESO1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [RADIO].[CONFIGURACION_PROCESO])
    BEGIN
        INSERT INTO [RADIO].[tmp_ms_xx_CONFIGURACION_PROCESO] ([ID], [SOL_TYPE], [CLASS], [NumeroResolucion], [FechaResolucion], [Nombre], [ConfiguracionCalificaciones], [FechaInicioRecepcionSolicitudes], [FechaFinRecepcionSolicitudes], [PuntajeMinimoViabilidadAutomatica])
        SELECT   [ID],
                 [SOL_TYPE],
                 [CLASS],
                 [NumeroResolucion],
                 [FechaResolucion],
                 [Nombre],
                 [ConfiguracionCalificaciones],
                 [FechaInicioRecepcionSolicitudes],
                 [FechaFinRecepcionSolicitudes],
                 CAST ([PuntajeMinimoViabilidadAutomatica] AS DECIMAL (18, 11))
        FROM     [RADIO].[CONFIGURACION_PROCESO]
        ORDER BY [ID] ASC;
    END

DROP TABLE [RADIO].[CONFIGURACION_PROCESO];

EXECUTE sp_rename N'[RADIO].[tmp_ms_xx_CONFIGURACION_PROCESO]', N'CONFIGURACION_PROCESO';

EXECUTE sp_rename N'[RADIO].[tmp_ms_xx_constraint_PK_CONFIGURACION_PROCESO1]', N'PK_CONFIGURACION_PROCESO', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Iniciando recompilación de la tabla [RADIO].[SOLICITUDES]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [RADIO].[tmp_ms_xx_SOLICITUDES] (
    [SOL_UID]                  UNIQUEIDENTIFIER CONSTRAINT [DF_SOLICITUDES_ID_SOL] DEFAULT (newid()) NOT NULL,
    [RAD_ALFANET]              NVARCHAR (50)    CONSTRAINT [DF_SOLICITUDES_RAD_ALFANET] DEFAULT (NULL) NULL,
    [SOL_CREATOR]              INT              CONSTRAINT [DF_SOLICITUDES_SOL_CREATOR] DEFAULT ((0)) NULL,
    [SOL_CREATED_DATE]         DATETIME         CONSTRAINT [DF_SOLICITUDES_FECHA_RAD_ALFANET] DEFAULT (NULL) NULL,
    [SOL_MODIFIED_DATE]        DATETIME         NULL,
    [SOL_LAST_STATE_DATE]      DATETIME         NULL,
    [SOL_REQUIRED_DATE]        DATETIME         NULL,
    [SOL_REQUIRED_POSTPONE]    INT              CONSTRAINT [DF_SOLICITUDES_SOL_REQUIRED_POSTPONE] DEFAULT ((0)) NOT NULL,
    [CUS_ID]                   INT              CONSTRAINT [DF_SOLICITUDES_CUS_ID] DEFAULT ((0)) NOT NULL,
    [CUS_IDENT]                NVARCHAR (32)    CONSTRAINT [DF_SOLICITUDES_CUS_IDENT] DEFAULT ('') NOT NULL,
    [CUS_NAME]                 NVARCHAR (200)   NOT NULL,
    [SERV_ID]                  INT              NOT NULL,
    [SERV_NUMBER]              INT              NOT NULL,
    [SOL_TYPE_ID]              INT              NOT NULL,
    [SOL_STATE_ID]             INT              NOT NULL,
    [CONT_ID]                  INT              NULL,
    [CONT_NAME]                NVARCHAR (150)   NULL,
    [CONT_NUMBER]              NVARCHAR (16)    NULL,
    [CONT_EMAIL]               NVARCHAR (250)   NULL,
    [CONT_ROLE]                INT              NULL,
    [SOL_NUMBER]               INT              NOT NULL,
    [GROUP_CURRENT]            INT              NULL,
    [ROLE_CURRENT]             INT              NULL,
    [STEP_CURRENT]             INT              CONSTRAINT [DF_SOLICITUDES_STEP_CURRENT] DEFAULT ((0)) NULL,
    [FIN_SEVEN_ALDIA]          BIT              NULL,
    [FIN_ESTADO_CUENTA_NUMBER] NVARCHAR (16)    NULL,
    [FIN_ESTADO_CUENTA_DATE]   DATETIME         NULL,
    [FIN_REGISTRO_ALFA_NUMBER] NVARCHAR (16)    NULL,
    [FIN_REGISTRO_ALFA_DATE]   DATETIME         NULL,
    [SOL_ENDED]                INT              CONSTRAINT [DF_SOLICITUDES_SOL_ENDED] DEFAULT ((0)) NOT NULL,
    [CAMPOS_SOL]               NVARCHAR (MAX)   NULL,
    [Clasificacion]            NVARCHAR (10)    NULL,
    [NivelCubrimiento]         NVARCHAR (10)    NULL,
    [Tecnologia]               NVARCHAR (10)    NULL,
    [NombreComunidad]          NVARCHAR (200)   NULL,
    [IdLugarComunidad]         NUMERIC (9)      NULL,
    [DireccionComunidad]       NVARCHAR (300)   NULL,
    [CiudadComunidad]          NVARCHAR (50)    NULL,
    [DepartamentoComunidad]    NVARCHAR (50)    NULL,
    [CodeLugarComunidad]       NVARCHAR (10)    NULL,
    [Territorio]               NVARCHAR (50)    NULL,
    [ResolucionMinInterior]    NVARCHAR (100)   NULL,
    [GrupoEtnico]              NVARCHAR (10)    NULL,
    [NombrePuebloComunidad]    NVARCHAR (100)   NULL,
    [NombreResguardo]          NVARCHAR (100)   NULL,
    [NombreCabildo]            NVARCHAR (100)   NULL,
    [NombreKumpania]           NVARCHAR (100)   NULL,
    [NombreConsejo]            NVARCHAR (100)   NULL,
    [NombreOrganizacionBase]   NVARCHAR (100)   NULL,
    [Evaluaciones]             NVARCHAR (MAX)   NULL,
    [Puntaje]                  NUMERIC (18, 11) NULL,
    [RazonDesempate]           NVARCHAR (MAX)   NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_SOLICITUDES1] PRIMARY KEY CLUSTERED ([SOL_UID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [RADIO].[SOLICITUDES])
    BEGIN
        INSERT INTO [RADIO].[tmp_ms_xx_SOLICITUDES] ([SOL_UID], [RAD_ALFANET], [SOL_CREATOR], [SOL_CREATED_DATE], [SOL_MODIFIED_DATE], [SOL_LAST_STATE_DATE], [SOL_REQUIRED_DATE], [SOL_REQUIRED_POSTPONE], [CUS_ID], [CUS_IDENT], [CUS_NAME], [SERV_ID], [SERV_NUMBER], [SOL_TYPE_ID], [SOL_STATE_ID], [CONT_ID], [CONT_NAME], [CONT_NUMBER], [CONT_EMAIL], [CONT_ROLE], [SOL_NUMBER], [GROUP_CURRENT], [ROLE_CURRENT], [STEP_CURRENT], [FIN_SEVEN_ALDIA], [FIN_ESTADO_CUENTA_NUMBER], [FIN_ESTADO_CUENTA_DATE], [FIN_REGISTRO_ALFA_NUMBER], [FIN_REGISTRO_ALFA_DATE], [SOL_ENDED], [CAMPOS_SOL], [Clasificacion], [NivelCubrimiento], [Tecnologia], [NombreComunidad], [IdLugarComunidad], [DireccionComunidad], [CiudadComunidad], [DepartamentoComunidad], [CodeLugarComunidad], [Territorio], [ResolucionMinInterior], [GrupoEtnico], [NombrePuebloComunidad], [NombreResguardo], [NombreCabildo], [NombreKumpania], [NombreConsejo], [NombreOrganizacionBase], [Evaluaciones], [Puntaje])
        SELECT   [SOL_UID],
                 [RAD_ALFANET],
                 [SOL_CREATOR],
                 [SOL_CREATED_DATE],
                 [SOL_MODIFIED_DATE],
                 [SOL_LAST_STATE_DATE],
                 [SOL_REQUIRED_DATE],
                 [SOL_REQUIRED_POSTPONE],
                 [CUS_ID],
                 [CUS_IDENT],
                 [CUS_NAME],
                 [SERV_ID],
                 [SERV_NUMBER],
                 [SOL_TYPE_ID],
                 [SOL_STATE_ID],
                 [CONT_ID],
                 [CONT_NAME],
                 [CONT_NUMBER],
                 [CONT_EMAIL],
                 [CONT_ROLE],
                 [SOL_NUMBER],
                 [GROUP_CURRENT],
                 [ROLE_CURRENT],
                 [STEP_CURRENT],
                 [FIN_SEVEN_ALDIA],
                 [FIN_ESTADO_CUENTA_NUMBER],
                 [FIN_ESTADO_CUENTA_DATE],
                 [FIN_REGISTRO_ALFA_NUMBER],
                 [FIN_REGISTRO_ALFA_DATE],
                 [SOL_ENDED],
                 [CAMPOS_SOL],
                 [Clasificacion],
                 [NivelCubrimiento],
                 [Tecnologia],
                 [NombreComunidad],
                 [IdLugarComunidad],
                 [DireccionComunidad],
                 [CiudadComunidad],
                 [DepartamentoComunidad],
                 [CodeLugarComunidad],
                 [Territorio],
                 [ResolucionMinInterior],
                 [GrupoEtnico],
                 [NombrePuebloComunidad],
                 [NombreResguardo],
                 [NombreCabildo],
                 [NombreKumpania],
                 [NombreConsejo],
                 [NombreOrganizacionBase],
                 [Evaluaciones],
                 CAST ([Puntaje] AS NUMERIC (18, 11))
        FROM     [RADIO].[SOLICITUDES]
        ORDER BY [SOL_UID] ASC;
    END

DROP TABLE [RADIO].[SOLICITUDES];

EXECUTE sp_rename N'[RADIO].[tmp_ms_xx_SOLICITUDES]', N'SOLICITUDES';

EXECUTE sp_rename N'[RADIO].[tmp_ms_xx_constraint_PK_SOLICITUDES1]', N'PK_SOLICITUDES', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Creando [RADIO].[FK_DOCUMENTOS_CONFIGURACION_CONFIGURACION_PROCESO]...';


GO
ALTER TABLE [RADIO].[DOCUMENTOS_CONFIGURACION] WITH NOCHECK
    ADD CONSTRAINT [FK_DOCUMENTOS_CONFIGURACION_CONFIGURACION_PROCESO] FOREIGN KEY ([ID_CONFIGURACION_PROCESO]) REFERENCES [RADIO].[CONFIGURACION_PROCESO] ([ID]);


GO
PRINT N'Creando [RADIO].[FK_RESOLUCIONES_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCIONES] WITH NOCHECK
    ADD CONSTRAINT [FK_RESOLUCIONES_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_LOGS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[LOGS] WITH NOCHECK
    ADD CONSTRAINT [FK_LOGS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_CONTEOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[CONTEOS] WITH NOCHECK
    ADD CONSTRAINT [FK_CONTEOS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_RESOLUCION_ANALISIS_VIABILIDAD_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCION_ANALISIS_VIABILIDAD] WITH NOCHECK
    ADD CONSTRAINT [FK_RESOLUCION_ANALISIS_VIABILIDAD_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_RESOLUCIONES_VIABILIDAD_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCIONES_VIABILIDAD] WITH NOCHECK
    ADD CONSTRAINT [FK_RESOLUCIONES_VIABILIDAD_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_SOLICITUDES_ESTADOS_SOL]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] WITH NOCHECK
    ADD CONSTRAINT [FK_SOLICITUDES_ESTADOS_SOL] FOREIGN KEY ([SOL_STATE_ID]) REFERENCES [RADIO].[ESTADOS_SOL] ([SOL_STATE_ID]);


GO
PRINT N'Creando [RADIO].[FK_SOLICITUDES_TIPOS_SOL]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES] WITH NOCHECK
    ADD CONSTRAINT [FK_SOLICITUDES_TIPOS_SOL] FOREIGN KEY ([SOL_TYPE_ID]) REFERENCES [RADIO].[TIPOS_SOL] ([SOL_TYPE_ID]);


GO
PRINT N'Creando [RADIO].[FK_ANEXOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[ANEXOS] WITH NOCHECK
    ADD CONSTRAINT [FK_ANEXOS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_SOL_USERS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[SOL_USERS] WITH NOCHECK
    ADD CONSTRAINT [FK_SOL_USERS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_COMUNICADOS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[COMUNICADOS] WITH NOCHECK
    ADD CONSTRAINT [FK_COMUNICADOS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_DOCUMENTS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[DOCUMENTS] WITH NOCHECK
    ADD CONSTRAINT [FK_DOCUMENTS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_TECH_ANALISIS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[TECH_ANALISIS] WITH NOCHECK
    ADD CONSTRAINT [FK_TECH_ANALISIS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Creando [RADIO].[FK_RESOLUCION_ANALISIS_SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] WITH NOCHECK
    ADD CONSTRAINT [FK_RESOLUCION_ANALISIS_SOLICITUDES] FOREIGN KEY ([SOL_UID]) REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID]);


GO
PRINT N'Actualizando [RADIO].[V_CANALES_PROCESOS]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_CANALES_PROCESOS]';


GO
PRINT N'Actualizando [RADIO].[V_INFO_SOLICITUDES_VIABLES]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_INFO_SOLICITUDES_VIABLES]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE1]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE1]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE16]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE16]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE2]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE2]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE32]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE32]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE4]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE4]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE8]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE8]';


GO
PRINT N'Creando [RADIO].[CONFIGURACION_PROCESO].[SOL_TYPE].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: Emisora comunitaria; 2: Emisora comunitaria grupos étnicos; 3: Emisora comercial; 4: Emisora de interés público', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'CONFIGURACION_PROCESO', @level2type = N'COLUMN', @level2name = N'SOL_TYPE';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_UID].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identificador de la solicitud', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_UID';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[RAD_ALFANET].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Codigo de radicación en ALFANET', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'RAD_ALFANET';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_CREATOR].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: Concesionario
1: Funcionario MinTic', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_CREATOR';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_CREATED_DATE].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Fecha de radicación en ALFANET', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_CREATED_DATE';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_TYPE_ID].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identificador de la tabla RDS.TIPOS_SOL que indica el tipo de solicitud RDS', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_TYPE_ID';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_STATE_ID].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Identificador de la tabla RADIO.ESTADOS_SOL que indica el estado de solicitud RDS', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_STATE_ID';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_NUMBER].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Este campo guarda el consecutivo de solicitudes del operador', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_NUMBER';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[GROUP_CURRENT].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: Concesionario
1: MinTic
2: ANE', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'GROUP_CURRENT';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[ROLE_CURRENT].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'000: Concesionario
001: Funcionario
002: Revisor
004: Coordinador
008: Subdirector
016: Asesor
032: Director
128: Administrador
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'ROLE_CURRENT';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[STEP_CURRENT].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: Administrativo
1: Técnico
2: Resolución
3: En cierre
4: Terminada', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'STEP_CURRENT';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_ENDED].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: En Curso
1: En cierre
2: Terminada', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_ENDED';


GO
PRINT N'Creando [RADIO].[FK_ANEXOS_SOLICITUDES].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'solicitud a la que pertenece el anexo', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'ANEXOS', @level2type = N'CONSTRAINT', @level2name = N'FK_ANEXOS_SOLICITUDES';


GO
PRINT N'Actualizando [RADIO].[SP_CARGARCUADROTECNICO]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[SP_CARGARCUADROTECNICO]';


GO
PRINT N'Comprobando los datos existentes con las restricciones recién creadas';



GO
ALTER TABLE [RADIO].[DOCUMENTOS_CONFIGURACION] WITH CHECK CHECK CONSTRAINT [FK_DOCUMENTOS_CONFIGURACION_CONFIGURACION_PROCESO];

ALTER TABLE [RADIO].[RESOLUCIONES] WITH CHECK CHECK CONSTRAINT [FK_RESOLUCIONES_SOLICITUDES];

ALTER TABLE [RADIO].[LOGS] WITH CHECK CHECK CONSTRAINT [FK_LOGS_SOLICITUDES];

ALTER TABLE [RADIO].[CONTEOS] WITH CHECK CHECK CONSTRAINT [FK_CONTEOS_SOLICITUDES];

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS_VIABILIDAD] WITH CHECK CHECK CONSTRAINT [FK_RESOLUCION_ANALISIS_VIABILIDAD_SOLICITUDES];

ALTER TABLE [RADIO].[RESOLUCIONES_VIABILIDAD] WITH CHECK CHECK CONSTRAINT [FK_RESOLUCIONES_VIABILIDAD_SOLICITUDES];

ALTER TABLE [RADIO].[SOLICITUDES] WITH CHECK CHECK CONSTRAINT [FK_SOLICITUDES_ESTADOS_SOL];

ALTER TABLE [RADIO].[SOLICITUDES] WITH CHECK CHECK CONSTRAINT [FK_SOLICITUDES_TIPOS_SOL];

ALTER TABLE [RADIO].[ANEXOS] WITH CHECK CHECK CONSTRAINT [FK_ANEXOS_SOLICITUDES];

ALTER TABLE [RADIO].[SOL_USERS] WITH CHECK CHECK CONSTRAINT [FK_SOL_USERS_SOLICITUDES];

ALTER TABLE [RADIO].[COMUNICADOS] WITH CHECK CHECK CONSTRAINT [FK_COMUNICADOS_SOLICITUDES];

ALTER TABLE [RADIO].[DOCUMENTS] WITH CHECK CHECK CONSTRAINT [FK_DOCUMENTS_SOLICITUDES];

ALTER TABLE [RADIO].[TECH_ANALISIS] WITH CHECK CHECK CONSTRAINT [FK_TECH_ANALISIS_SOLICITUDES];

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] WITH CHECK CHECK CONSTRAINT [FK_RESOLUCION_ANALISIS_SOLICITUDES];


GO
PRINT N'Actualización completada.';


GO
