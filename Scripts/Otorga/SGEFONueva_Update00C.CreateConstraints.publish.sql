
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [CK_FrontOfficeStatus] CHECK  (([FrontOfficeStatus]='InProccess' OR [FrontOfficeStatus]='AdministrativeAnalisys' OR [FrontOfficeStatus]='TechnicalAnalisys' OR [FrontOfficeStatus]='Closing' OR [FrontOfficeStatus]='Solved' OR [FrontOfficeStatus]='PACO'))
GO

ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [CK_FrontOfficeStatus]
GO

ALTER TABLE [dbo].[Applications]  WITH NOCHECK ADD  CONSTRAINT [CK_ResolutionType] CHECK  (([ResolutionType]='Rejected' OR [ResolutionType]='Canceled' OR [ResolutionType]='Succesfull'))
GO

ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [CK_ResolutionType]
GO

