UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Identificación del Representante  Legal ' , DES_TIPO_ANX_SOL='Cédula de ciudadania; Cedula de extranjeria'WHERE ID_ANX_SOL=1
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Certificado de cámara de comercio' , DES_TIPO_ANX_SOL='Acreditación de la existencia y representación legal del proponente'WHERE ID_ANX_SOL=2
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Capacidad Financiera' , DES_TIPO_ANX_SOL='Documentos que validen la capacidad financiera del solicitante'WHERE ID_ANX_SOL=3
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Certificado de experiencia' , DES_TIPO_ANX_SOL=''WHERE ID_ANX_SOL=3
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Registro Unico Tributario- RUT' , DES_TIPO_ANX_SOL='Identificación tributaria'WHERE ID_ANX_SOL=4
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Carta de presentación de la propuesta' , DES_TIPO_ANX_SOL=''WHERE ID_ANX_SOL=5
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Certificado de antecedentes disciplinarios' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=6
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Proyecto de radiodifusión sonora' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=7
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Certificado de disponibilidad y registro presupuestal' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=8
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Carta de presentación de la propuesta' , DES_TIPO_ANX_SOL='lndica la manifestación de aceptación y cumplimiento de todas las especificaciones y condiciones técnicas y las demás consignadas en los términos de referencia sin condicionamiento alguno' WHERE ID_ANX_SOL=9
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Compromiso anticorrupción' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=10
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Certificación de cumplimiento del pago de contribuciones y aportes parafiscales' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=11
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Boletín de responsabilidades fiscales' , DES_TIPO_ANX_SOL='' WHERE ID_ANX_SOL=12
UPDATE RADIO.TIPO_ANEXO_SOL SET TIPO_ANX_SOL='Cartas de compromiso para integración de la junta de programación' , DES_TIPO_ANX_SOL='Carta en la cual el solicitante se compromete a participar en la Junta de Programación.' WHERE ID_ANX_SOL=13

