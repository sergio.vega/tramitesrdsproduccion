INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Estudio tecnico', 'Otorga Documento anexo Estudio tecnico', '.pdf,.doc,.docx', 30, 50, null, null)
GO

INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Concepto Aeronautica Civil ', 'Otorga documento anexo Concepto favorable de la Unidad Administrativa Especial de la Aeronautica Civil ', '.pdf,.doc,.docx', 30, 50, null, null)
GO

INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Certificado de planeacion municipal', 'Otorga documento anexo Certificado de planeacion municipal', '.pdf,.doc,.docx', 30, 50, null, null)
GO

INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Anexo tecnico', 'Otorga documento anexo Anexo tecnico', '.pdf,.doc,.docx', 30, 50, null, null)
GO

INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Manual de estilo', 'Otorga documento anexo Manual de estilo', '.pdf,.doc,.docx', 30, 50, null, null)
GO

INSERT INTO RADIO.TIPO_ANEXO_SOL
VALUES('Junta de Programacion', 'Otorga documento anexo Acta de constitución de la Junta de Programación', '.pdf,.doc,.docx', 30, 50, null, null)
GO