UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Doc. de Identificación del Representante  Legal - Cédula de ciudadania; Cedula de extranjeria' WHERE ID_ANX_SOL=1
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Cerficado de Cámara de Comercio: Acreditación de la existencia y representación legal del proponente' WHERE ID_ANX_SOL=2
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Capacidad Financiera- Documentos que validen la capacidad financiera del solicitante' WHERE ID_ANX_SOL=3
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Certificado de experiencia' WHERE ID_ANX_SOL=4
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Identificación tributaria: Registro Unico Tributario- RUT' WHERE ID_ANX_SOL=5
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Carta de presentación de la propuesta' WHERE ID_ANX_SOL=6
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Otros soportes: Documentos propios descritos en los terminos de referencia que no estén en el listado de los documentos predeterminados' WHERE ID_ANX_SOL=7
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Certificado de antecedentes disciplinarios de la Procuraduría General de la Nación' WHERE ID_ANX_SOL=8
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Presentación del proyecto de radiodifusión sonora' WHERE ID_ANX_SOL=9
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Documento que acredite la representación legal de la entidad.' WHERE ID_ANX_SOL=10
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Certificado de disponibilidad y registro presupuestal.' WHERE ID_ANX_SOL=11
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Proyecto para la instalación y operación de la estación del Servicio de Radiodifusión Sonora de Interés Público.' WHERE ID_ANX_SOL=12
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Carta de presentación de la propuesta:  lndica la manifestación de aceptación y cumplimiento de todas las especificaciones y condiciones técnicas y las demás consignadas en los términos de referencia sin condicionamiento alguno' WHERE ID_ANX_SOL=13
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Compromiso anticorrupción:  El proponente obrará con la transparencia y la moralidad que la Constitución Política y las leyes consagran' WHERE ID_ANX_SOL=14
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Cerficado de Cámara de Comercio: Acreditación de la existencia y representación legal del proponente' WHERE ID_ANX_SOL=15
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Certificación de cumplimiento del pago de contribuciones y aportes parafiscales' WHERE ID_ANX_SOL=16
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Identificación tributaria: Registro Unico Tributario- RUT' WHERE ID_ANX_SOL=17
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Boletín de responsabilidades fiscales' WHERE ID_ANX_SOL=18
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Certificado de antecedentes disciplinarios de la Procuraduría General de la Nación' WHERE ID_ANX_SOL=19
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Presentación del proyecto de radiodifusión sonora' WHERE ID_ANX_SOL=20
UPDATE RADIO.TIPO_ANEXO_SOL SET DES_TIPO_ANX_SOL='Cartas de compromiso para integración de la junta de programación: Carta en la cual el solicitante se compromete a participar en la Junta de Programación.' WHERE ID_ANX_SOL=21
