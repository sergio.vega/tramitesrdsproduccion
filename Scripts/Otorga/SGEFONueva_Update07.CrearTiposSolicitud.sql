UPDATE RADIO.TIPOS_SOL SET SOL_IS_OTORGA = 0


GO

INSERT INTO [RADIO].[TIPOS_SOL]
           ([SOL_TYPE_NAME]
           ,[SOL_TYPE_CLASS]
           ,[SOL_TYPE_DESC]
           ,[SOL_IMPLEMENTED]
           ,[SOL_IS_OTORGA])
     VALUES
           ('Otorga - Emisora comercial'
           ,2
           ,'Todos'
           ,1
           ,1)
GO

INSERT INTO [RADIO].[TIPOS_SOL]
           ([SOL_TYPE_NAME]
           ,[SOL_TYPE_CLASS]
           ,[SOL_TYPE_DESC]
           ,[SOL_IMPLEMENTED]
           ,[SOL_IS_OTORGA])
     VALUES
           ('Otorga - Emisora comunitaria'
           ,2
           ,'Todos'
           ,1
           ,1)
GO

INSERT INTO [RADIO].[TIPOS_SOL]
           ([SOL_TYPE_NAME]
           ,[SOL_TYPE_CLASS]
           ,[SOL_TYPE_DESC]
           ,[SOL_IMPLEMENTED]
           ,[SOL_IS_OTORGA])
     VALUES
           ('Otorga - Emisora comunitaria grupos �tnicos'
           ,2
           ,'Todos'
           ,1
           ,1)
GO

INSERT INTO [RADIO].[TIPOS_SOL]
           ([SOL_TYPE_NAME]
           ,[SOL_TYPE_CLASS]
           ,[SOL_TYPE_DESC]
           ,[SOL_IMPLEMENTED]
           ,[SOL_IS_OTORGA])
     VALUES
           ('Otorga - Emisora de inter�s p�blico'
           ,2
           ,'Todos'
           ,1
           ,1)
GO
