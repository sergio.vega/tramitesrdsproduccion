﻿
GO
PRINT N'Modificando [RADIO].[DOCUMENTS]...';


GO
ALTER TABLE [RADIO].[DOCUMENTS] ALTER COLUMN [DOC_NAME] NVARCHAR (100) NOT NULL;


GO
PRINT N'Modificando [RADIO].[SOLICITUDES]...';


GO
ALTER TABLE [RADIO].[SOLICITUDES]
    ADD [Clasificacion]          NVARCHAR (10)  NULL,
        [NivelCubrimiento]       NVARCHAR (10)  NULL,
        [Tecnologia]             NVARCHAR (10)  NULL,
        [NombreComunidad]        NVARCHAR (200) NULL,
        [IdLugarComunidad]       NUMERIC (9)    NULL,
        [DireccionComunidad]     NVARCHAR (300) NULL,
        [CiudadComunidad]        NVARCHAR (50)  NULL,
        [DepartamentoComunidad]  NVARCHAR (50)  NULL,
        [CodeLugarComunidad]     NVARCHAR (10)  NULL,
        [Territorio]             NVARCHAR (50)  NULL,
        [ResolucionMinInterior]  NVARCHAR (100) NULL,
        [GrupoEtnico]            NVARCHAR (10)  NULL,
        [NombrePuebloComunidad]  NVARCHAR (100) NULL,
        [NombreResguardo]        NVARCHAR (100) NULL,
        [NombreCabildo]          NVARCHAR (100) NULL,
        [NombreKumpania]         NVARCHAR (100) NULL,
        [NombreConsejo]          NVARCHAR (100) NULL,
        [NombreOrganizacionBase] NVARCHAR (100) NULL;


GO
PRINT N'Modificando [RADIO].[TIPO_ANEXO_SOL]...';


GO
ALTER TABLE [RADIO].[TIPO_ANEXO_SOL]
    ADD [EXTENSIONS]      NVARCHAR (50) NULL,
        [MAX_SIZE]        INT           NULL,
        [MAX_NAME_LENGTH] INT           NULL;


GO
PRINT N'Modificando [RADIO].[TIPOS_SOL]...';


GO
ALTER TABLE [RADIO].[TIPOS_SOL]
    ADD [SOL_IS_OTORGA] BIT NULL;


GO
PRINT N'Creando [RADIO].[TOKENS].[IX_TOKENS_TOKEN]...';


GO
CREATE NONCLUSTERED INDEX [IX_TOKENS_TOKEN]
    ON [RADIO].[TOKENS]([TOKEN] ASC);


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE1]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE1]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE16]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE16]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE2]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE2]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE32]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE32]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE4]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE4]';


GO
PRINT N'Actualizando [RADIO].[V_USUARIOS_ROLE8]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[V_USUARIOS_ROLE8]';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[ROLE_CURRENT].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'000: Concesionario
001: Funcionario
002: Revisor
004: Coordinador
008: Subdirector
016: Asesor
032: Director
128: Administrador
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'ROLE_CURRENT';


GO
PRINT N'Creando [RADIO].[SOLICITUDES].[SOL_ENDED].[MS_Description]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0: En Curso
1: En cierre
2: Terminada', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'TABLE', @level1name = N'SOLICITUDES', @level2type = N'COLUMN', @level2name = N'SOL_ENDED';


GO
PRINT N'Actualizando [RADIO].[SP_CARGARCUADROTECNICO]...';


GO
EXECUTE sp_refreshsqlmodule N'[RADIO].[SP_CARGARCUADROTECNICO]';


GO
PRINT N'Actualización completada.';


GO
