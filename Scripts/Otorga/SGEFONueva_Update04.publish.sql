﻿
GO
PRINT N'Modificando [RADIO].[V_CONTACTOS]...';


GO
ALTER VIEW RADIO.V_CONTACTOS
AS
SELECT        CONVERT(int, SERV.ID) AS SERV_ID, SERV.NUMBER AS SERV_NUMBER, CONVERT(int, ICSM_PRUEBA.dbo.USERS_CNTCT.ID) AS CONT_ID, US.IDENT AS CUS_IDENT, 
                         ICSM_PRUEBA.dbo.USERS_CNTCT.FIRSTNAME AS CONT_NAME, CONVERT(int, ICSM_PRUEBA.dbo.USERS_CNTCT.ROLE) AS CONT_ROLE, ICSM_PRUEBA.dbo.USERS_CNTCT.TEL AS CONT_TEL, 
                         ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM AS CONT_NUMBER
FROM            ICSM_PRUEBA.dbo.USERS_CNTCT INNER JOIN
                         ICSM_PRUEBA.dbo.SERV_LICENCE AS SERV ON ICSM_PRUEBA.dbo.USERS_CNTCT.OPER_ID = SERV.OWNER_ID INNER JOIN
                         ICSM_PRUEBA.dbo.USERS AS US ON US.ID = SERV.OWNER_ID
WHERE        (ICSM_PRUEBA.dbo.USERS_CNTCT.CUST_TXT4 = '1') AND (ICSM_PRUEBA.dbo.USERS_CNTCT.ROLE IN (1, 2, 8)) AND (ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM IS NOT NULL) AND 
                         (LEN(ICSM_PRUEBA.dbo.USERS_CNTCT.REGIST_NUM) > 1) AND (SERV.CLASS IN (4, 13, 27)) AND (US.STATUS <> 'uNAC') AND (SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B'))
UNION ALL
SELECT        SERV.SERV_ID, SERV.SERV_NUMBER, CONVERT(int, RADIO.USERS_CNTCT_TEMPORALES.ID) AS CONT_ID, US.IDENT AS CUS_IDENT, RADIO.USERS_CNTCT_TEMPORALES.FIRSTNAME AS CONT_NAME, CONVERT(int, 
                         RADIO.USERS_CNTCT_TEMPORALES.ROLE) AS CONT_ROLE, RADIO.USERS_CNTCT_TEMPORALES.TEL AS CONT_TEL, RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM AS CONT_NUMBER
FROM            RADIO.USERS_CNTCT_TEMPORALES INNER JOIN
                         RADIO.EXPEDIENTES_TEMPORALES AS SERV ON RADIO.USERS_CNTCT_TEMPORALES.OPER_ID = SERV.CUS_ID INNER JOIN
                         RADIO.USERS_TEMPORALES AS US ON US.ID = SERV.CUS_ID
WHERE        (RADIO.USERS_CNTCT_TEMPORALES.CUST_TXT4 = '1') AND (RADIO.USERS_CNTCT_TEMPORALES.ROLE IN (1, 2, 8)) AND (RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM IS NOT NULL) AND 
                         (LEN(RADIO.USERS_CNTCT_TEMPORALES.REGIST_NUM) > 1)
GO
PRINT N'Modificando [RADIO].[V_EXPEDIENTES]...';


GO
ALTER VIEW RADIO.V_EXPEDIENTES
AS
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT CUS_IDENT, US.NAME CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL, '') CUS_EMAIL, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, isnull(FM.NAME, 'NA') SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'FM' MODULATION, SERV.STATUS SERV_STATUS, SERV.CUST_TXT3, CAST(0 AS bit) AS EsTemporal
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].FM_STATION AS FM ON FM.LIC_ID = LIC.ID AND FM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = FM.CALL_SIGN /*And PLANB.NUMBER=SERV.NUMBER*/ JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') AND LIC.NUMBER IS NOT NULL
UNION ALL
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT CUS_IDENT, US.NAME CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL, '') CUS_EMAIL, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, isnull(AM.NAME, 'NA') SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'AM' MODULATION, SERV.STATUS SERV_STATUS, SERV.CUST_TXT3, CAST(0 AS bit) AS EsTemporal
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].LFMF_STATION AS AM ON AM.LIC_ID = LIC.ID AND AM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = AM.CALL_SIGN JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') AND LIC.NUMBER IS NOT NULL
UNION ALL
SELECT        SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, 
                         MODULATION, SERV_STATUS, CUST_TXT3, CAST(1 AS bit) AS EsTemporal
FROM            [SageFrontOfficePruebas].RADIO.EXPEDIENTES_TEMPORALES
GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES_EMAIL]...';


GO
CREATE VIEW RADIO.V_CUSTOMERS_TEMPORALES_EMAIL
AS
SELECT        REGIST_NUM, MAX(EMAIL) AS CUS_EMAIL
FROM            RADIO.USERS_CNTCT_TEMPORALES AS CNT
WHERE        (TITLE = 'EMP')
GROUP BY REGIST_NUM
GO
PRINT N'Creando [RADIO].[F_GetPuedeSolicitar]...';


GO

CREATE FUNCTION [RADIO].[F_GetPuedeSolicitar]
(
	@SOL_TYPE_ID int
)
RETURNS bit
AS
BEGIN
	DECLARE @nResult bit = 1

	IF @SOL_TYPE_ID = 17 -- OtorgaEmisoraDeInteresPublico
	BEGIN
		SET @nResult = 1
	END
	ELSE
	BEGIN
		DECLARE @fechaInicio DATETIME
		DECLARE @fechaFin DATETIME
		SELECT @fechaInicio = dateadd(day, datediff(day, 0, FechaInicioRecepcionSolicitudes),0), @fechaFin = dateadd(day, datediff(day, 0, FechaFinRecepcionSolicitudes),0)
		FROM            RADIO.CONFIGURACION_PROCESO
		WHERE        (SOL_TYPE = @SOL_TYPE_ID)

		-- Se ordenan las fechas
		IF @fechaInicio > @fechaFin 
		BEGIN
			DECLARE @fechaTmp DATETIME
			SET @fechaTmp = @fechaInicio
			SET @fechaInicio = @fechaFin
			SET @fechaFin = @fechaTmp
		END

		SET @nResult = CAST(CASE WHEN getdate() >= @fechaInicio AND getdate() < @fechaFin + 1  THEN 1 ELSE 0 END AS bit)
	END

	RETURN @nResult

END
GO
PRINT N'Modificando [RADIO].[V_CONTACTOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CONTACTOS';


GO
PRINT N'Modificando [RADIO].[V_EXPEDIENTES].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[15] 2[39] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 24
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_EXPEDIENTES';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES_EMAIL].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CNT"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 183
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_TEMPORALES_EMAIL';


GO
PRINT N'Creando [RADIO].[V_CUSTOMERS_TEMPORALES_EMAIL].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CUSTOMERS_TEMPORALES_EMAIL';


GO
PRINT N'Actualización completada.';


GO
