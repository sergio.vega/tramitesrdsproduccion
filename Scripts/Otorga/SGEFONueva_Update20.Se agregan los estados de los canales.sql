﻿


GO
PRINT N'Creando [RADIO].[IX_USERS_TEMPORALES]...';


GO
ALTER TABLE [RADIO].[USERS_TEMPORALES]
    ADD CONSTRAINT [IX_USERS_TEMPORALES] UNIQUE NONCLUSTERED ([IDENT] ASC);


GO
PRINT N'Creando [RADIO].[IX_USUARIOS]...';


GO
ALTER TABLE [RADIO].[USUARIOS]
    ADD CONSTRAINT [IX_USUARIOS] UNIQUE NONCLUSTERED ([USR_LOGIN] ASC);


GO
PRINT N'Modificando [RADIO].[V_CANALES_PROCESOS]...';


GO
ALTER VIEW RADIO.V_CANALES_PROCESOS
AS
SELECT        RADIO.V_PLAN_BRO.ID AS ID_PLAN_BRO, RADIO.V_PLAN_BRO.STATE, RADIO.V_PLAN_BRO.CALL_SIGN, RADIO.V_PLAN_BRO.FREQ, RADIO.V_PLAN_BRO.CLASS, RADIO.V_PLAN_BRO.CUST_TXT3, 
                         RADIO.V_PLAN_BRO.MUNICIPIO, RADIO.V_PLAN_BRO.DEPARTAMENTO, RADIO.V_PLAN_BRO.POWER, RADIO.V_PLAN_BRO.ASL, RADIO.V_PLAN_BRO.STATION_CLASS, 
                         CASE WHEN FREQ > 10 THEN 'FM' ELSE 'AM' END AS MODULATION, RADIO.SOLICITUDES.SOL_TYPE_ID
FROM            RADIO.V_EXPEDIENTES INNER JOIN
                         RADIO.SOLICITUDES ON RADIO.V_EXPEDIENTES.SERV_ID = RADIO.SOLICITUDES.SERV_ID INNER JOIN
                         RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN
GROUP BY RADIO.V_PLAN_BRO.ID, RADIO.V_PLAN_BRO.STATE, RADIO.V_PLAN_BRO.CALL_SIGN, RADIO.V_PLAN_BRO.FREQ, RADIO.V_PLAN_BRO.CLASS, RADIO.V_PLAN_BRO.CUST_TXT3, RADIO.V_PLAN_BRO.MUNICIPIO, 
                         RADIO.V_PLAN_BRO.DEPARTAMENTO, RADIO.V_PLAN_BRO.POWER, RADIO.V_PLAN_BRO.ASL, RADIO.V_PLAN_BRO.STATION_CLASS, RADIO.SOLICITUDES.SOL_TYPE_ID
GO
PRINT N'Creando [RADIO].[V_INFO_SOLICITUDES_VIABLES]...';


GO
CREATE VIEW RADIO.V_INFO_SOLICITUDES_VIABLES
AS
SELECT        RADIO.SOLICITUDES.SOL_UID, RADIO.V_CUSTOMERS.CUS_NAME, RADIO.V_CUSTOMERS.CUS_IDENT, RADIO.SOLICITUDES.RAD_ALFANET, RADIO.V_EXPEDIENTES.SERV_NUMBER, RADIO.V_EXPEDIENTES.SERV_STATUS, 
                         RADIO.V_EXPEDIENTES.CALL_SIGN
FROM            RADIO.SOLICITUDES INNER JOIN
                         RADIO.V_EXPEDIENTES ON RADIO.SOLICITUDES.SERV_ID = RADIO.V_EXPEDIENTES.SERV_ID INNER JOIN
                         RADIO.V_CUSTOMERS ON RADIO.V_EXPEDIENTES.CUS_ID = RADIO.V_CUSTOMERS.CUS_ID LEFT OUTER JOIN
                         RADIO.RESOLUCIONES_VIABILIDAD ON RADIO.SOLICITUDES.SOL_UID = RADIO.RESOLUCIONES_VIABILIDAD.SOL_UID
WHERE        (RADIO.RESOLUCIONES_VIABILIDAD.RES_IS_CREATED = 1) OR
                         (RADIO.SOLICITUDES.STEP_CURRENT = - 3)
GO
PRINT N'Creando [RADIO].[F_GetEstadoViabilidadCanal]...';


GO
-- =============================================
-- Description:	Retorna el estado de la viabildad del canal
-- 0 Indica que no se han realizado solicitudes
-- 1 Indica que se han realizado solicitudes y álguna no está lista para asignar la viabilidad
-- 2 Indica que todas las solicitudes están listas para asignar viabilidad
-- 3 Indica que se ha asignado el canal
-- 4 Indica que todas las solicitudes están terminadas y no se asigó el canal
-- =============================================
CREATE FUNCTION RADIO.F_GetEstadoViabilidadCanal
(
	@CallSign nvarchar(20)
)
RETURNS int
AS
BEGIN
	DECLARE @estadoViabilidad int = 0
	
	DECLARE @TotalSolicitudes int = 0
	DECLARE @Listas int = 0
	DECLARE @Asignado int = 0
	DECLARE @Terminadas int = 0

	SELECT @TotalSolicitudes = SUM(1), @Asignado = SUM(CASE WHEN SOLS.STATE = 'ASIGNADO' THEN 1 ELSE 0 END), @Terminadas = SUM(CASE WHEN SOLS.SOL_ENDED = 1 THEN 1 ELSE 0 END), @Listas = SUM(CASE WHEN (SOLS.STEP_CURRENT = -2 AND SOLS.SOL_ENDED = 0) THEN 1 ELSE 0 END)
	FROM 
		(
			SELECT RADIO.SOLICITUDES.SOL_STATE_ID, RADIO.SOLICITUDES.STEP_CURRENT, RADIO.V_PLAN_BRO.STATE, RADIO.SOLICITUDES.SOL_TYPE_ID, RADIO.ESTADOS_SOL.SOL_ENDED
			FROM RADIO.V_EXPEDIENTES INNER JOIN
					RADIO.SOLICITUDES ON RADIO.V_EXPEDIENTES.SERV_ID = RADIO.SOLICITUDES.SERV_ID INNER JOIN
					RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN INNER JOIN
                    RADIO.ESTADOS_SOL ON RADIO.SOLICITUDES.SOL_STATE_ID = RADIO.ESTADOS_SOL.SOL_STATE_ID
			WHERE (RADIO.SOLICITUDES.SOL_TYPE_ID IN (14, 15, 16, 17)) AND (RADIO.V_PLAN_BRO.CALL_SIGN = @CallSign)
		) AS SOLS

	IF (@TotalSolicitudes = 0) 
	BEGIN
		SET @estadoViabilidad = 0
	END
	ELSE
	BEGIN
		IF (@Asignado > 0) 
		BEGIN
			SET @estadoViabilidad = 3
		END
		ELSE
		BEGIN
			IF (@Terminadas = @TotalSolicitudes) 
			BEGIN
				SET @estadoViabilidad = 4
			END
			ELSE
			BEGIN
				IF (@Listas + @Terminadas = @TotalSolicitudes) 
				BEGIN
					SET @estadoViabilidad = 2
				END
				ELSE
				BEGIN
					SET @estadoViabilidad = 1
				END
			END
		END
	END

	RETURN @estadoViabilidad

END
GO
PRINT N'Modificando [RADIO].[V_CANALES_PROCESOS].[MS_DiagramPane1]...';


GO
EXECUTE sp_updateextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[21] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_EXPEDIENTES (RADIO)"
            Begin Extent = 
               Top = 11
               Left = 409
               Bottom = 262
               Right = 584
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SOLICITUDES (RADIO)"
            Begin Extent = 
               Top = 33
               Left = 21
               Bottom = 273
               Right = 279
            End
            DisplayFlags = 280
            TopColumn = 27
         End
         Begin Table = "V_PLAN_BRO (RADIO)"
            Begin Extent = 
               Top = 36
               Left = 682
               Bottom = 243
               Right = 932
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_CANALES_PROCESOS';


GO
PRINT N'Creando [RADIO].[V_INFO_SOLICITUDES_VIABLES].[MS_DiagramPane1]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SOLICITUDES (RADIO)"
            Begin Extent = 
               Top = 27
               Left = 676
               Bottom = 276
               Right = 934
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "V_EXPEDIENTES (RADIO)"
            Begin Extent = 
               Top = 27
               Left = 366
               Bottom = 234
               Right = 586
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "V_CUSTOMERS (RADIO)"
            Begin Extent = 
               Top = 21
               Left = 74
               Bottom = 215
               Right = 284
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RESOLUCIONES_VIABILIDAD (RADIO)"
            Begin Extent = 
               Top = 32
               Left = 1003
               Bottom = 162
               Right = 1217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
       ', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_INFO_SOLICITUDES_VIABLES';


GO
PRINT N'Creando [RADIO].[V_INFO_SOLICITUDES_VIABLES].[MS_DiagramPane2]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_INFO_SOLICITUDES_VIABLES';


GO
PRINT N'Creando [RADIO].[V_INFO_SOLICITUDES_VIABLES].[MS_DiagramPaneCount]...';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'RADIO', @level1type = N'VIEW', @level1name = N'V_INFO_SOLICITUDES_VIABLES';


GO
PRINT N'Actualización completada.';


GO
