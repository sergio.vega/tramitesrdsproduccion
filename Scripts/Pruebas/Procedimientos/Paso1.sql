USE [SageFrontOfficePruebas]
GO

/****** Object:  StoredProcedure [RADIO].[SP_CARGARCUADROTECNICO]    Script Date: 7/1/2020 7:11:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [RADIO].[SP_CARGARCUADROTECNICO] @SOL_UID uniqueidentifier
AS
--SET NOCOUNT ON

Declare @CuaTec_CuadroTecnico nvarchar(20)

SELECT TOP 1 @CuaTec_CuadroTecnico=TECH_NUMBER FROM RADIO.V_RESOLUCIONES WHERE SERV_ID=(Select SERV_ID From RADIO.SOLICITUDES Where SOL_UID=@SOL_UID) ORDER BY START_DATE Desc

IF @CuaTec_CuadroTecnico is not null And not EXISTS (SELECT * FROM CuadroTecnico.CUADROSTECNICOS WHERE CuaTec_CuadroTecnico = @CuaTec_CuadroTecnico)
	insert into CuadroTecnico.CUADROSTECNICOS (CuaTec_Id, CuaTec_Expediente, CuaTec_CuadroTecnico, CuaTec_File, CuaTec_Nombre, CuaTec_FechaCarga, Usuario)
	Select TECH_UID CuaTec_Id, convert(nvarchar, SERV_NUMBER) CuaTec_Expediente, @CuaTec_CuadroTecnico CuaTec_CuadroTecnico, TECH_FILE CuaTec_File, @CuaTec_CuadroTecnico + ' - '  + convert(nvarchar, SERV_NUMBER) + '.pdf' CuaTec_Nombre, getdate() CuaTec_FechaCarga, 0 Usuario
	From RADIO.SOLICITUDES SOL 
	Join Radio.TECH_DOCUMENTS DOC On SOL.SOL_UID=DOC.SOL_UID And DOC.TECH_TYPE=3
	Where SOL.SOL_UID=@SOL_UID






GO


