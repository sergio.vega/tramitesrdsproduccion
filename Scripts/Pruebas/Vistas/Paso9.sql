USE [SageFrontOfficePruebas]
GO

/****** Object:  View [RADIO].[V_USUARIOS]    Script Date: 16/06/2020 7:48:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_USUARIOS]
AS
SELECT        USO.SOL_UID, USO.USR_GROUP, USO.USR_ROLE1_ID, ISNULL(USR1.USR_NAME, 'Sin asignar') AS USR_ROLE1_NAME, USR1.USR_EMAIL AS USR_ROLE1_EMAIL, USO.USR_ROLE2_ID, ISNULL(USR2.USR_NAME, 
                         'Sin asignar') AS USR_ROLE2_NAME, USR2.USR_EMAIL AS USR_ROLE2_EMAIL, USO.USR_ROLE4_ID, ISNULL(USR4.USR_NAME, 'Sin asignar') AS USR_ROLE4_NAME, USR4.USR_EMAIL AS USR_ROLE4_EMAIL, 
                         USO.USR_ROLE8_ID, ISNULL(USR8.USR_NAME, 'Sin asignar') AS USR_ROLE8_NAME, USR8.USR_EMAIL AS USR_ROLE8_EMAIL, USO.USR_ROLE16_ID, ISNULL(USR16.USR_NAME, 'Sin asignar') AS USR_ROLE16_NAME, 
                         USR16.USR_EMAIL AS USR_ROLE16_EMAIL, USO.USR_ROLE32_ID, ISNULL(USR32.USR_NAME, 'Sin asignar') AS USR_ROLE32_NAME, USR32.USR_EMAIL AS USR_ROLE32_EMAIL
FROM            RADIO.SOL_USERS AS USO LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR1 ON USO.USR_ROLE1_ID = USR1.USR_ID LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR2 ON USO.USR_ROLE2_ID = USR2.USR_ID LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR4 ON USO.USR_ROLE4_ID = USR4.USR_ID LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR8 ON USO.USR_ROLE8_ID = USR8.USR_ID LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR16 ON USO.USR_ROLE16_ID = USR16.USR_ID LEFT OUTER JOIN
                         RADIO.USUARIOS AS USR32 ON USO.USR_ROLE32_ID = USR32.USR_ID

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "USO"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 311
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR1"
            Begin Extent = 
               Top = 6
               Left = 349
               Bottom = 136
               Right = 531
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR2"
            Begin Extent = 
               Top = 6
               Left = 569
               Bottom = 136
               Right = 751
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR4"
            Begin Extent = 
               Top = 6
               Left = 789
               Bottom = 136
               Right = 971
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR8"
            Begin Extent = 
               Top = 6
               Left = 1009
               Bottom = 136
               Right = 1191
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR16"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "USR32"
            Begin Extent = 
               Top = 138
               Left = 258
               Bottom = 268
               Right = 440
            End
            DisplayFlags = 280
            TopColumn ' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_USUARIOS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'= 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_USUARIOS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_USUARIOS'
GO


