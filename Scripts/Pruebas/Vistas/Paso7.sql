USE [SageFrontOfficePruebas]
GO

/****** Object:  View [RADIO].[V_RESOLUCIONES]    Script Date: 16/06/2020 7:48:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_RESOLUCIONES]
AS
SELECT        RES.ID AS RES_ID, RES.SRVLIC_ID AS SERV_ID, RES.RES_NUMBER, RES.RES_DATE, RES.START_DATE, RES.STOP_DATE, RES.TECH_NUMBER, RES.TECH_DATE, RES.RES_TYPEI, COMBO.LEGEN AS RES_TYPEI_NAME, 
                         RES.PROC_STATI, COMBO1.LEGEN AS PROC_STATI_NAME
FROM            ICSM_PRUEBA.dbo.SERV_LICENCE AS SERV INNER JOIN
                         ICSM_PRUEBA.dbo.USERS AS US ON US.ID = SERV.OWNER_ID INNER JOIN
                         ICSM_PRUEBA.dbo.P_RESOLUTION AS RES ON SERV.ID = RES.SRVLIC_ID LEFT OUTER JOIN
                         ICSM_PRUEBA.dbo.COMBO AS COMBO ON RES.RES_TYPEI = COMBO.ITEM AND COMBO.DOMAIN = 'RESOL_TYPETRA' AND COMBO.LANG = 'SPA' LEFT OUTER JOIN
                         ICSM_PRUEBA.dbo.COMBO AS COMBO1 ON RES.PROC_STATI = COMBO1.ITEM AND COMBO1.DOMAIN = 'RESOL_PROCSTAT_all' AND COMBO1.LANG = 'SPA'
WHERE        (SERV.CLASS IN (4, 13, 27)) AND (US.STATUS <> 'uNAC') AND (SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B'))

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SERV"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "RES"
            Begin Extent = 
               Top = 6
               Left = 517
               Bottom = 136
               Right = 701
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COMBO"
            Begin Extent = 
               Top = 6
               Left = 739
               Bottom = 136
               Right = 909
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COMBO1"
            Begin Extent = 
               Top = 6
               Left = 947
               Bottom = 136
               Right = 1117
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 13
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_RESOLUCIONES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_RESOLUCIONES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_RESOLUCIONES'
GO


