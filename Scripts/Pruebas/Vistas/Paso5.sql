USE [SageFrontOfficePruebas]
GO

/****** Object:  View [RADIO].[V_EXPEDIENTES]    Script Date: 7/3/2020 11:19:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_EXPEDIENTES]
AS
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT CUS_IDENT, US.NAME CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL,'') CUS_EMAIL, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, isnull(FM.NAME, 'NA') SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'FM' MODULATION, SERV.STATUS SERV_STATUS
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].FM_STATION AS FM ON FM.LIC_ID = LIC.ID AND FM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = FM.CALL_SIGN /*And PLANB.NUMBER=SERV.NUMBER*/ JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') And LIC.NUMBER is not null
UNION ALL
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, CONVERT(int, US.ID) AS CUS_ID, US.IDENT CUS_IDENT, US.NAME CUS_NAME, CONVERT(int, 
                         US.REGIST_NUM) CUS_BRANCH, isnull(CEM.CUS_EMAIL,'') CUS_EMAIL, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, CONVERT(float, PLANB.POWER) POWER, CONVERT(float, PLANB.ASL) ASL, 
                         PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, isnull(AM.NAME, 'NA') SERV_NAME, isnull(SERV.STOP_DATE, '19000101') STOP_DATE, 'AM' MODULATION, SERV.STATUS SERV_STATUS
FROM            [ICSM_PRUEBA].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PRUEBA].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PRUEBA].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PRUEBA].[dbo].LFMF_STATION AS AM ON AM.LIC_ID = LIC.ID AND AM.ID > 0 JOIN
                         [ICSM_PRUEBA].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = AM.CALL_SIGN JOIN
                         [ICSM_PRUEBA].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) LEFT JOIN
                         RADIO.V_CUSTOMERS_EMAIL CEM ON US.ID = CEM.OPER_ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4') And LIC.NUMBER is not null

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 21
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_EXPEDIENTES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_EXPEDIENTES'
GO


