USE [SageFrontOfficePruebas]
GO

/****** Object:  View [RADIO].[V_PUNTOAPUNTO]    Script Date: 16/06/2020 7:48:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_PUNTOAPUNTO]
AS
SELECT        CONVERT(int, SERV.ID) AS SERV_ID, SERV.NUMBER AS SERV_NUMBER, LIC.NUMBER AS LIC_NUMBER, US.IDENT AS CUS_IDENT, MW.DESIG_EM AS DESIG_EMISSION, MW.CLASS, CA.NAME AS DEPTO_A, 
                         SA.CITY AS CITY_A, SA.ADDRESS AS ADDRESS_A, SA.LATITUDE AS LATITUDE_A, SA.LONGITUDE AS LONGITUDE_A, RADIO.F_CoordenadasGMS(SA.LATITUDE, N'Latitude', N'.') AS LATITUDEGSM_A, 
                         RADIO.F_CoordenadasGMS(SA.LONGITUDE, N'Longitud', N'.') AS LONGITUDEGSM_A, MWSA.TX_FREQ AS FREQUENCY_A, MWSA.POWER AS POWER_A, MWSA.AGL1 AS AGL_A, SA.ASL AS ASL_A, MWSA.GAIN AS GAIN_A, 
                         MWSA.AZIMUTH AS AZIMUTH_A, MWSA.ANGLE_ELEV AS TILT_A, MWSA.POLAR AS POLAR_A, CB.NAME AS DEPTO_B, SB.CITY AS CITY_B, SB.ADDRESS AS ADDRESS_B, SB.LATITUDE AS LATITUDE_B, 
                         SB.LONGITUDE AS LONGITUDE_B, RADIO.F_CoordenadasGMS(SB.LATITUDE, N'Latitude', N'.') AS LATITUDEGSM_B, RADIO.F_CoordenadasGMS(SB.LONGITUDE, N'Longitud', N'.') AS LONGITUDEGSM_B, 
                         MWSB.TX_FREQ AS FREQUENCY_B, MWSB.POWER AS POWER_B, MWSB.AGL1 AS AGL_B, SB.ASL AS ASL_B, MWSB.GAIN AS GAIN_B, MWSB.AZIMUTH AS AZIMUTH_B, MWSB.ANGLE_ELEV AS TILT_B, 
                         MWSB.POLAR AS POLAR_B, CASE WHEN LIC.CUST_TXT4 = 'SI' THEN 1 ELSE 0 END AS TRASMOVIL
FROM            ICSM_PRUEBA.dbo.SERV_LICENCE AS SERV INNER JOIN
                         ICSM_PRUEBA.dbo.USERS AS US ON US.ID = SERV.OWNER_ID INNER JOIN
                         ICSM_PRUEBA.dbo.LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID INNER JOIN
                         ICSM_PRUEBA.dbo.MICROWA AS MW ON MW.LIC_ID = LIC.ID AND MW.ID > 0 INNER JOIN
                         ICSM_PRUEBA.dbo.MICROWS AS MWSA ON MWSA.MW_ID = MW.ID AND MWSA.ROLE = 'A' INNER JOIN
                         ICSM_PRUEBA.dbo.SITE AS SA ON MWSA.SITE_ID = SA.ID LEFT OUTER JOIN
                         ICSM_PRUEBA.dbo.CITIES AS CA ON CA.CODE = SA.VAC_DEPTO_CODE INNER JOIN
                         ICSM_PRUEBA.dbo.MICROWS AS MWSB ON MWSB.MW_ID = MW.ID AND MWSB.ROLE = 'B' INNER JOIN
                         ICSM_PRUEBA.dbo.SITE AS SB ON MWSB.SITE_ID = SB.ID LEFT OUTER JOIN
                         ICSM_PRUEBA.dbo.CITIES AS CB ON CB.CODE = SB.VAC_DEPTO_CODE
WHERE        (SERV.CLASS IN (4, 13, 27)) AND (US.STATUS <> 'uNAC') AND (SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B')) AND (LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4'))

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SERV"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 244
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 282
               Bottom = 136
               Right = 479
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LIC"
            Begin Extent = 
               Top = 6
               Left = 517
               Bottom = 136
               Right = 725
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MW"
            Begin Extent = 
               Top = 6
               Left = 763
               Bottom = 136
               Right = 994
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MWSA"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 225
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SA"
            Begin Extent = 
               Top = 138
               Left = 263
               Bottom = 268
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CA"
            Begin Extent = 
               Top = 138
               Left = 491
               Bottom = 268
               Right = 668
            End
            DisplayFlags = 280
            TopColumn = 0
     ' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_PUNTOAPUNTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    End
         Begin Table = "MWSB"
            Begin Extent = 
               Top = 138
               Left = 706
               Bottom = 268
               Right = 893
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SB"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 400
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CB"
            Begin Extent = 
               Top = 138
               Left = 931
               Bottom = 268
               Right = 1108
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 33
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_PUNTOAPUNTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_PUNTOAPUNTO'
GO


