USE [SageFrontOfficePruebas]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_GetAlarmState]    Script Date: 6/24/2020 11:26:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_GetAlarmState] (
    @SOL_LAST_STATE_DATE date,
	@ALARM_PRE_EXPIRE int,
	@ALARM_EXPIRE int,
	@SOL_ENDED int
)
RETURNS int
AS
BEGIN
	DECLARE @nDays int
	DECLARE @nResult int=1

	if @SOL_ENDED=1
	begin
		set @nResult=4
		return @nResult
	end

	if @SOL_ENDED=2
	begin
		set @nResult=5
		return @nResult
	end

	if @SOL_LAST_STATE_DATE is null
	begin
		set @nResult=0
		return @nResult
	end

	Set @nDays=	DATEDIFF(day, Convert(date, @SOL_LAST_STATE_DATE), Convert(date, getdate()))

	if (@nDays>=@ALARM_EXPIRE)
	begin
		set @nResult=3
		return @nResult
	end
	if (@nDays>=@ALARM_PRE_EXPIRE)
	begin
		set @nResult=2
		return @nResult
	end

	set @nResult=1
	return @nResult
END

GO


