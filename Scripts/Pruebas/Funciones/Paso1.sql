USE [SageFrontOfficePruebas]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_CoordenadasGMS]    Script Date: 16/06/2020 7:42:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_CoordenadasGMS] (
    @Coordenada float,
	@tipoCoordenada nvarchar(20),
	@Separador  nvarchar(1)=',' 
)
RETURNS NVARCHAR(100)
AS
BEGIN
	DECLARE @sGrados NVARCHAR(100)
	DECLARE @sMinutos NVARCHAR(100)
	DECLARE @sSegundos NVARCHAR(100)
    DECLARE @sValue NVARCHAR(100)

	SET @sGrados = Convert(nvarchar,floor(ABS(@Coordenada))) + '� '
	SET @sMinutos = Convert(nvarchar,Convert(int,ABS(@Coordenada)*60) % 60) + ''' '
	SET @sSegundos = Convert(nvarchar,convert(decimal(4,2),convert(decimal(17,10),ABS(@Coordenada)*3600) % 60)) + '"'
	SET @sSegundos = replace(@sSegundos,'.',@Separador)
	SET @sValue = @sGrados + @sMinutos + @sSegundos + Case When @tipoCoordenada='Latitude' Then CASE When @Coordenada>0 Then ' N' Else ' S' End Else CASE When @Coordenada>0 Then ' E' Else ' W' End End 
	

    RETURN @sValue
END

GO


