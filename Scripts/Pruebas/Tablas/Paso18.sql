USE [SageFrontOfficePruebas]
GO

/****** Object:  Table [RADIO].[CONTEOS]    Script Date: 16/06/2020 7:21:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[CONTEOS](
	[CON_ID] [int] IDENTITY(1,1) NOT NULL,
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[USR_GROUP] [int] NOT NULL,
	[USR_ROLE] [int] NOT NULL,
	[USR_ID] [int] NOT NULL,
	[CON_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_CONTEOS] PRIMARY KEY CLUSTERED 
(
	[CON_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [RADIO].[CONTEOS] ADD  CONSTRAINT [DF_CONTEOS_CON_DATE]  DEFAULT (getdate()) FOR [CON_DATE]
GO

ALTER TABLE [RADIO].[CONTEOS]  WITH CHECK ADD  CONSTRAINT [FK_CONTEOS_SOLICITUDES] FOREIGN KEY([SOL_UID])
REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID])
GO

ALTER TABLE [RADIO].[CONTEOS] CHECK CONSTRAINT [FK_CONTEOS_SOLICITUDES]
GO


