USE [SageFrontOfficePruebas]
GO

/****** Object:  Table [RADIO].[ANEXOS]    Script Date: 16/06/2020 6:59:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [RADIO].[ANEXOS](
	[ANX_UID] [uniqueidentifier] NOT NULL,
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[ANX_TYPE_ID] [int] NOT NULL,
	[ANX_STATE_ID] [int] NOT NULL,
	[ANX_COMMENT] [nvarchar](250) NULL,
	[ANX_NUMBER] [int] NOT NULL,
	[ANX_NAME] [nvarchar](150) NOT NULL,
	[ANX_CONTENT_TYPE] [nvarchar](100) NOT NULL,
	[ANX_CREATED_DATE] [datetime] NOT NULL,
	[ANX_MODIFIED_DATE] [datetime] NULL,
	[ANX_ROLE1_STATE_ID] [int] NOT NULL,
	[ANX_ROLE2_STATE_ID] [int] NOT NULL,
	[ANX_ROLE4_STATE_ID] [int] NOT NULL,
	[ANX_ROLE8_STATE_ID] [int] NOT NULL,
	[ANX_ROLE1_COMMENT] [nvarchar](250) NULL,
	[ANX_ROLE2_COMMENT] [nvarchar](250) NULL,
	[ANX_ROLE4_COMMENT] [nvarchar](250) NULL,
	[ANX_ROLE8_COMMENT] [nvarchar](250) NULL,
	[ANX_ROLEX_CHANGED] [bit] NOT NULL,
	[ANX_FILE] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_ANEXOS] PRIMARY KEY CLUSTERED 
(
	[ANX_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ID_TIPO_ANX]  DEFAULT ((1)) FOR [ANX_TYPE_ID]
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ANX_ROLE1_STATE_ID]  DEFAULT ((0)) FOR [ANX_ROLE1_STATE_ID]
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ANX_ROLE2_STATE_ID]  DEFAULT ((0)) FOR [ANX_ROLE2_STATE_ID]
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ANX_ROLE4_STATE_ID]  DEFAULT ((0)) FOR [ANX_ROLE4_STATE_ID]
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ANX_ROLE8_STATE_ID]  DEFAULT ((0)) FOR [ANX_ROLE8_STATE_ID]
GO

ALTER TABLE [RADIO].[ANEXOS] ADD  CONSTRAINT [DF_ANEXOS_ANX_ROLEX_CHANGE]  DEFAULT ((0)) FOR [ANX_ROLEX_CHANGED]
GO

ALTER TABLE [RADIO].[ANEXOS]  WITH CHECK ADD  CONSTRAINT [FK_ANEXOS_SOLICITUDES] FOREIGN KEY([SOL_UID])
REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID])
GO

ALTER TABLE [RADIO].[ANEXOS] CHECK CONSTRAINT [FK_ANEXOS_SOLICITUDES]
GO

ALTER TABLE [RADIO].[ANEXOS]  WITH CHECK ADD  CONSTRAINT [FK_ANEXOS_TIPO_ANEXO_SOL] FOREIGN KEY([ANX_TYPE_ID])
REFERENCES [RADIO].[TIPO_ANEXO_SOL] ([ID_ANX_SOL])
GO

ALTER TABLE [RADIO].[ANEXOS] CHECK CONSTRAINT [FK_ANEXOS_TIPO_ANEXO_SOL]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del archivo' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_UID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de documento en la solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_TYPE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado del documento' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'numero asignado de manera consecutiva segun la solicitud a la que pertenece' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_NUMBER'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del archivo anexado a solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_NAME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de archivo guardado (pdf,excel, xml etc)' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_CONTENT_TYPE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se radica el anexo en alfa' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_CREATED_DATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se realiza algun cambio al archivo' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_MODIFIED_DATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: SinDefinir
1: Radicado
2: Requerido
3: Subsanado   
4: Aceptado
5: Rechazado ' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_ROLE1_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: SinDefinir
1: Aceptado
2: Devuelto' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_ROLE2_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: SinDefinir
1: Aceptado
2: Devuelto' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_ROLE4_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: SinDefinir
1: Aceptado
2: Devuelto' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_ROLE8_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contenido del archivo anexado a la solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'COLUMN',@level2name=N'ANX_FILE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'solicitud a la que pertenece el anexo' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'CONSTRAINT',@level2name=N'FK_ANEXOS_SOLICITUDES'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'tipo de anexo solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ANEXOS', @level2type=N'CONSTRAINT',@level2name=N'FK_ANEXOS_TIPO_ANEXO_SOL'
GO


