USE [SageFrontOfficePruebas]
GO

/****** Object:  Table [RADIO].[DOCUMENTS]    Script Date: 16/06/2020 7:07:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [RADIO].[DOCUMENTS](
	[DOC_UID] [uniqueidentifier] NOT NULL,
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[DOC_CLASS] [int] NOT NULL,
	[DOC_TYPE] [int] NOT NULL,
	[DOC_ORIGIN] [int] NOT NULL,
	[DOC_DEST] [int] NOT NULL,
	[DOC_NUMBER] [nvarchar](16) NOT NULL,
	[DOC_NAME] [nvarchar](50) NOT NULL,
	[DOC_DATE] [datetime] NOT NULL,
	[DOC_OPENED] [int] NOT NULL,
	[DOC_FILE] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_DOCUMENTS] PRIMARY KEY CLUSTERED 
(
	[DOC_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [RADIO].[DOCUMENTS] ADD  CONSTRAINT [DF_DOCUMENTS_DOC_OPENED]  DEFAULT ((0)) FOR [DOC_OPENED]
GO

ALTER TABLE [RADIO].[DOCUMENTS]  WITH CHECK ADD  CONSTRAINT [FK_DOCUMENTS_SOLICITUDES] FOREIGN KEY([SOL_UID])
REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID])
GO

ALTER TABLE [RADIO].[DOCUMENTS] CHECK CONSTRAINT [FK_DOCUMENTS_SOLICITUDES]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: RADICADO
2: REGISTRO' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'DOCUMENTS', @level2type=N'COLUMN',@level2name=N'DOC_CLASS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Solicitud
2: Admin Subsanación 
3: Admin Requermiento
4: Admin Rechazo
5: Admin Aprobación
6: Cancelación
7:Tech Devolución
8: Tech Subsanacion
9: Tech Requermiento
10: Tech Rechazo
11: Tech Aprobacion
12: Aplazamiento' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'DOCUMENTS', @level2type=N'COLUMN',@level2name=N'DOC_TYPE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Concesionario
1: MinTic
2: ANE' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'DOCUMENTS', @level2type=N'COLUMN',@level2name=N'DOC_ORIGIN'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Concesionario
1: MinTic
2: ANE' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'DOCUMENTS', @level2type=N'COLUMN',@level2name=N'DOC_DEST'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: No
1: Si' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'DOCUMENTS', @level2type=N'COLUMN',@level2name=N'DOC_OPENED'
GO


