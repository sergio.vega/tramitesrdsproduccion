USE [SageFrontOfficePruebas]
GO

/****** Object:  Table [RADIO].[ESTADOS_SOL]    Script Date: 16/06/2020 6:56:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[ESTADOS_SOL](
	[SOL_STATE_ID] [int] NOT NULL,
	[SOL_STATE_NAME] [nvarchar](50) NOT NULL,
	[SOL_STATE_DESC] [nvarchar](max) NOT NULL,
	[SOL_STATE_ABR] [nvarchar](5) NULL,
	[SOL_ENDED] [bit] NOT NULL CONSTRAINT [DF_ESTADOS_SOL_SOL_ENDED]  DEFAULT ((0)),
 CONSTRAINT [PK_ESTADOS_SOL] PRIMARY KEY CLUSTERED 
(
	[SOL_STATE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del estado de la solicitud' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ESTADOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ESTADOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_STATE_NAME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción estado de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ESTADOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_STATE_DESC'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abreviatura estado solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'ESTADOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_STATE_ABR'
GO


