USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[RESOLUCION_ANALISIS]    Script Date: 16/06/2020 7:28:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[RESOLUCION_ANALISIS](
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[RES_ROLE1_STATE_ID] [int] NOT NULL,
	[RES_ROLE2_STATE_ID] [int] NOT NULL,
	[RES_ROLE4_STATE_ID] [int] NOT NULL,
	[RES_ROLE8_STATE_ID] [int] NOT NULL,
	[RES_ROLE16_STATE_ID] [int] NOT NULL,
	[RES_ROLE32_STATE_ID] [int] NOT NULL,
	[RES_ROLE1_COMMENT] [nvarchar](250) NULL,
	[RES_ROLE2_COMMENT] [nvarchar](250) NULL,
	[RES_ROLE4_COMMENT] [nvarchar](250) NULL,
	[RES_ROLE8_COMMENT] [nvarchar](250) NULL,
	[RES_ROLE16_COMMENT] [nvarchar](250) NULL,
	[RES_ROLE32_COMMENT] [nvarchar](250) NULL,
	[RES_ROLEX_CHANGED] [bit] NOT NULL,
 CONSTRAINT [PK_RESOLUCION_ANALISIS] PRIMARY KEY CLUSTERED 
(
	[SOL_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE1_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE1_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE2_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE2_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE4_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE4_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE8_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE8_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE16_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE16_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLE32_STATE_ID]  DEFAULT ((0)) FOR [RES_ROLE32_STATE_ID]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] ADD  CONSTRAINT [DF_RESOLUCION_ANALISIS_RES_ROLEX_CHANGED]  DEFAULT ((0)) FOR [RES_ROLEX_CHANGED]
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS]  WITH CHECK ADD  CONSTRAINT [FK_RESOLUCION_ANALISIS_SOLICITUDES] FOREIGN KEY([SOL_UID])
REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID])
GO

ALTER TABLE [RADIO].[RESOLUCION_ANALISIS] CHECK CONSTRAINT [FK_RESOLUCION_ANALISIS_SOLICITUDES]
GO


