USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[TIPO_ANEXO_SOL]    Script Date: 16/06/2020 7:00:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[TIPO_ANEXO_SOL](
	[ID_ANX_SOL] [int] IDENTITY(1,1) NOT NULL,
	[TIPO_ANX_SOL] [nvarchar](50) NOT NULL,
	[DES_TIPO_ANX_SOL] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TIPO_ANEXO_SOL] PRIMARY KEY CLUSTERED 
(
	[ID_ANX_SOL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del tipo de anexo de la solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPO_ANEXO_SOL', @level2type=N'COLUMN',@level2name=N'ID_ANX_SOL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de anexo solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPO_ANEXO_SOL', @level2type=N'COLUMN',@level2name=N'TIPO_ANX_SOL'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripción del tipo de anexo de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPO_ANEXO_SOL', @level2type=N'COLUMN',@level2name=N'DES_TIPO_ANX_SOL'
GO


