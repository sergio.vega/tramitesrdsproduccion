USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[TIPOS_SOL]    Script Date: 16/06/2020 6:57:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[TIPOS_SOL](
	[SOL_TYPE_ID] [int] IDENTITY(1,1) NOT NULL,
	[SOL_TYPE_NAME] [nvarchar](50) NOT NULL,
	[SOL_TYPE_CLASS] [int] NOT NULL CONSTRAINT [DF_TIPOS_SOL_SOL_TYPE_CLASS]  DEFAULT ((2)),
	[SOL_TYPE_DESC] [nvarchar](100) NOT NULL,
	[SOL_IMPLEMENTED] [bit] NOT NULL CONSTRAINT [DF_TIPOS_SOL_SOL_IMPLEMENTED]  DEFAULT ((0)),
 CONSTRAINT [PK_TIPOS_SOL] PRIMARY KEY CLUSTERED 
(
	[SOL_TYPE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del tipo de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_TYPE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_TYPE_NAME'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Administrativo
2: Administrativo y T�cnico' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_TYPE_CLASS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descripci�n tipo de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'TIPOS_SOL', @level2type=N'COLUMN',@level2name=N'SOL_TYPE_DESC'
GO


