USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[SOLICITUDES]    Script Date: 6/24/2020 9:42:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[SOLICITUDES](
	[SOL_UID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_SOLICITUDES_ID_SOL]  DEFAULT (newid()),
	[RAD_ALFANET] [nvarchar](50) NULL CONSTRAINT [DF_SOLICITUDES_RAD_ALFANET]  DEFAULT (NULL),
	[SOL_CREATOR] [int] NULL CONSTRAINT [DF_SOLICITUDES_SOL_CREATOR]  DEFAULT ((0)),
	[SOL_CREATED_DATE] [datetime] NULL CONSTRAINT [DF_SOLICITUDES_FECHA_RAD_ALFANET]  DEFAULT (NULL),
	[SOL_MODIFIED_DATE] [datetime] NULL,
	[SOL_LAST_STATE_DATE] [datetime] NULL,
	[SOL_REQUIRED_DATE] [datetime] NULL,
	[SOL_REQUIRED_POSTPONE] [int] NOT NULL CONSTRAINT [DF_SOLICITUDES_SOL_REQUIRED_POSTPONE]  DEFAULT ((0)),
	[CUS_ID] [int] NOT NULL CONSTRAINT [DF_SOLICITUDES_CUS_ID]  DEFAULT ((0)),
	[CUS_IDENT] [nvarchar](32) NOT NULL CONSTRAINT [DF_SOLICITUDES_CUS_IDENT]  DEFAULT (''),
	[CUS_NAME] [nvarchar](200) NOT NULL,
	[SERV_ID] [int] NOT NULL,
	[SERV_NUMBER] [int] NOT NULL,
	[SOL_TYPE_ID] [int] NOT NULL,
	[SOL_STATE_ID] [int] NOT NULL,
	[CONT_ID] [int] NULL,
	[CONT_NAME] [nvarchar](150) NULL,
	[CONT_NUMBER] [nvarchar](16) NULL,
	[CONT_EMAIL] [nvarchar](250) NULL,
	[CONT_ROLE] [int] NULL,
	[SOL_NUMBER] [int] NOT NULL,
	[GROUP_CURRENT] [int] NULL,
	[ROLE_CURRENT] [int] NULL,
	[STEP_CURRENT] [int] NULL CONSTRAINT [DF_SOLICITUDES_STEP_CURRENT]  DEFAULT ((0)),
	[FIN_SEVEN_ALDIA] [bit] NULL,
	[FIN_ESTADO_CUENTA_NUMBER] [nvarchar](16) NULL,
	[FIN_ESTADO_CUENTA_DATE] [datetime] NULL,
	[FIN_REGISTRO_ALFA_NUMBER] [nvarchar](16) NULL,
	[FIN_REGISTRO_ALFA_DATE] [datetime] NULL,
	[SOL_ENDED] [int] NOT NULL CONSTRAINT [DF_SOLICITUDES_SOL_ENDED]  DEFAULT ((0)),
	[CAMPOS_SOL] [nvarchar](max) NULL,
 CONSTRAINT [PK_SOLICITUDES] PRIMARY KEY CLUSTERED 
(
	[SOL_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [RADIO].[SOLICITUDES]  WITH CHECK ADD  CONSTRAINT [FK_SOLICITUDES_ESTADOS_SOL] FOREIGN KEY([SOL_STATE_ID])
REFERENCES [RADIO].[ESTADOS_SOL] ([SOL_STATE_ID])
GO

ALTER TABLE [RADIO].[SOLICITUDES] CHECK CONSTRAINT [FK_SOLICITUDES_ESTADOS_SOL]
GO

ALTER TABLE [RADIO].[SOLICITUDES]  WITH CHECK ADD  CONSTRAINT [FK_SOLICITUDES_TIPOS_SOL] FOREIGN KEY([SOL_TYPE_ID])
REFERENCES [RADIO].[TIPOS_SOL] ([SOL_TYPE_ID])
GO

ALTER TABLE [RADIO].[SOLICITUDES] CHECK CONSTRAINT [FK_SOLICITUDES_TIPOS_SOL]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la solicitud' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_UID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codigo de radicación en ALFANET' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'RAD_ALFANET'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Concesionario
1: Funcionario MinTic' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_CREATOR'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de radicación en ALFANET' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_CREATED_DATE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla RDS.TIPOS_SOL que indica el tipo de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_TYPE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la tabla RADIO.ESTADOS_SOL que indica el estado de solicitud RDS' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_STATE_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este campo guarda el consecutivo de solicitudes del operador' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_NUMBER'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Concesionario
1: MinTic
2: ANE' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'GROUP_CURRENT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Administrativo
1: Técnico
2: Resolución
3: Terminada' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'STEP_CURRENT'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: En Curso
1: En cierre
2: Terminada' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'SOLICITUDES', @level2type=N'COLUMN',@level2name=N'SOL_ENDED'
GO


