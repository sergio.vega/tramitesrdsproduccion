USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[EVENTS]    Script Date: 6/25/2020 7:57:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [RADIO].[EVENTS](
	[EVT_ID] [int] IDENTITY(1,1) NOT NULL,
	[USR_ID] [int] NOT NULL,
	[EVT_FUNCTION] [nvarchar](150) NOT NULL,
	[EVT_CRUD] [int] NOT NULL,
	[EVT_DATA_OLD] [nvarchar](max) NULL,
	[EVT_DATA_NEW] [nvarchar](max) NULL,
	[EVT_CREATED] [datetime] NOT NULL CONSTRAINT [DF_EVENTS_EVT_CREATED]  DEFAULT (getdate()),
 CONSTRAINT [PK_EVENTS] PRIMARY KEY CLUSTERED 
(
	[EVT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


