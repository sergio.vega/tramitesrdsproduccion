USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[USUARIOS]    Script Date: 16/06/2020 7:22:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [RADIO].[USUARIOS](
	[USR_ID] [int] IDENTITY(1,1) NOT NULL,
	[USR_LOGIN] [nvarchar](50) NOT NULL,
	[USR_PASSWORD] [nvarchar](128) NOT NULL CONSTRAINT [DF_USUARIOS_USR_PASSWORD]  DEFAULT (''),
	[USR_NAME] [nvarchar](150) NOT NULL,
	[USR_GROUP] [int] NOT NULL CONSTRAINT [DF_USUARIOS_USR_TYPE]  DEFAULT ((1)),
	[USR_ROLE] [int] NOT NULL,
	[USR_ASSIGNABLE] [int] NOT NULL,
	[USR_STATUS] [int] NOT NULL CONSTRAINT [DF_USUARIOS_ESTADO]  DEFAULT ((1)),
	[USR_EMAIL] [nvarchar](250) NULL,
	[USR_SIGN] [varbinary](max) NULL,
 CONSTRAINT [PK_USUARIOS_1] PRIMARY KEY CLUSTERED 
(
	[USR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Concesionario
1: MinTic
2: ANE
3: MinTic, ANE' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'USUARIOS', @level2type=N'COLUMN',@level2name=N'USR_GROUP'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'000: Concesionario
001: Funcionario
002: Revisor
004: Coordinador
008: Subdirector
016: Asesor
032: Director
128: Administrador
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'USUARIOS', @level2type=N'COLUMN',@level2name=N'USR_ROLE'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Activo
2: Inactivo
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'USUARIOS', @level2type=N'COLUMN',@level2name=N'USR_STATUS'
GO


