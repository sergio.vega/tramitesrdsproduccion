USE [SageFrontOffice]
GO

/****** Object:  Table [RADIO].[COMUNICADOS]    Script Date: 16/06/2020 7:03:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [RADIO].[COMUNICADOS](
	[COM_UID] [uniqueidentifier] NOT NULL,
	[SOL_UID] [uniqueidentifier] NOT NULL,
	[COM_CLASS] [int] NOT NULL,
	[COM_TYPE] [int] NOT NULL,
	[COM_NAME] [nvarchar](50) NOT NULL,
	[COM_DATE] [datetime] NOT NULL,
	[COM_FILE] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_COMUNICADOS] PRIMARY KEY CLUSTERED 
(
	[COM_UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [RADIO].[COMUNICADOS]  WITH CHECK ADD  CONSTRAINT [FK_COMUNICADOS_SOLICITUDES] FOREIGN KEY([SOL_UID])
REFERENCES [RADIO].[SOLICITUDES] ([SOL_UID])
GO

ALTER TABLE [RADIO].[COMUNICADOS] CHECK CONSTRAINT [FK_COMUNICADOS_SOLICITUDES]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Comunicado Administrativo y Financiero
2: Comunicado T�cnico
3: Comunicado Revisi�n T�cnica
4: Comunicado desistimiento' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'COMUNICADOS', @level2type=N'COLUMN',@level2name=N'COM_CLASS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Aprobado
2: Requerido
3: Rechazado
4: Devuelto' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'TABLE',@level1name=N'COMUNICADOS', @level2type=N'COLUMN',@level2name=N'COM_TYPE'
GO


