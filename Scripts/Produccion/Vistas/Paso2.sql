USE [SageFrontOffice]
GO

/****** Object:  View [RADIO].[V_CUBRIMIENTO]    Script Date: 16/06/2020 7:47:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_CUBRIMIENTO]
AS
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, US.IDENT AS CUS_IDENT, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, 
                         CONVERT(float, PLANB.POWER) POWER, CONVERT(float, FM.CUST_NBR2) ASL, CONVERT(float, PLANB.ASL) ASL1, PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, FM.NAME SERV_NAME, SERV.STOP_DATE, 
                         SIT.CUST_NBR1 AS NORTH, SIT.CUST_NBR2 AS EAST, SIT.LATITUDE, SIT.LONGITUDE, RADIO.F_CoordenadasGMS(SIT.LATITUDE,'Latitude', '.') LATITUDEGSM, RADIO.F_CoordenadasGMS(SIT.LONGITUDE,'Longitud', '.') LONGITUDEGSM, SIT.CUST_TXT1 AS ORIGIN, FM.DESIG_EM AS DESIG_EMISSION, FM.PWR_ANT AS PWR_ANT, FM.TX_LOSSES AS LOSSES, FM.AGL, 
                         FM.GAIN, FM.AZIMUTH, SIT.ADDRESS, 'FM' MODULATION
FROM            [ICSM_PROD].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PROD].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PROD].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PROD].[dbo].FM_STATION AS FM ON FM.LIC_ID = LIC.ID AND FM.ID > 0 JOIN
                         [ICSM_PROD].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = FM.CALL_SIGN /*And PLANB.NUMBER=SERV.NUMBER*/ JOIN
                         [ICSM_PROD].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) JOIN
                         [ICSM_PROD].[dbo].SITE SIT ON FM.SITE_ID = SIT.ID
WHERE        SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4')
UNION ALL
SELECT DISTINCT 
                         CONVERT(int, SERV.ID) SERV_ID, CONVERT(int, SERV.NUMBER) SERV_NUMBER, CONVERT(int, LIC.NUMBER) LIC_NUMBER, US.IDENT AS CUS_IDENT, PLANB.STATION_CLASS, PLANB.STATE, C.NAME AS DEPTO, PLANB.CITY, 
                         CONVERT(float, PLANB.POWER) POWER, CONVERT(float, AM.CUST_NBR2) ASL, CONVERT(float, PLANB.ASL) ASL1, PLANB.FREQ FREQUENCY, PLANB.CALL_SIGN, AM.NAME SERV_NAME, SERV.STOP_DATE, 
                         SIT.CUST_NBR1 AS NORTH, SIT.CUST_NBR2 AS EAST, SIT.LATITUDE, SIT.LONGITUDE, RADIO.F_CoordenadasGMS(SIT.LATITUDE,'Latitude', '.') LATITUDEGSM, RADIO.F_CoordenadasGMS(SIT.LONGITUDE,'Longitud', '.') LONGITUDEGSM, SIT.CUST_TXT1 AS ORIGIN, AMO.DESIG_EMISSION AS EMISION, AMO.PWR_KW AS PWR_ANT, 0.0 LOSSES, 
                         AMO.AGL, 0 GAIN, AMO.AZIMUTH AS AZIMUT, SIT.ADDRESS, 'AM' MODULATION
FROM            [ICSM_PROD].[dbo].SERV_LICENCE AS SERV JOIN
                         [ICSM_PROD].[dbo].USERS AS US ON US.ID = SERV.OWNER_ID JOIN
                         [ICSM_PROD].[dbo].LICENCE AS LIC ON LIC.SRVLIC_ID = SERV.ID JOIN
                         [ICSM_PROD].[dbo].LFMF_STATION AS AM ON AM.LIC_ID = LIC.ID AND AM.ID > 0 JOIN
                         [ICSM_PROD].[dbo].PLAN_BRO_MINTIC AS PLANB ON PLANB.CALL_SIGN = AM.CALL_SIGN JOIN
                         [ICSM_PROD].[dbo].CITIES AS C ON C.CODE = SUBSTRING(PLANB.CODE_AREA, 1, 2) JOIN
                         [ICSM_PROD].[dbo].SITE SIT ON AM.SITE_ID = SIT.ID JOIN
                         [ICSM_PROD].[dbo].LFMFS_OPER AMO ON AM.ID = AMO.STA_ID AND AMO.TYPE = 'HJ'
/*D�a(HJ), Noche(HN)*/ WHERE SERV.CLASS IN (4, 13, 27) AND US.STATUS <> 'uNAC' AND SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B') AND LIC.STATUS NOT IN ('rX3', 'rFAL', 'rX1', 'rVEN', 'rX4')

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 29
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_CUBRIMIENTO'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_CUBRIMIENTO'
GO


