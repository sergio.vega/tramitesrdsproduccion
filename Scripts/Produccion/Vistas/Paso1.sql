USE [SageFrontOffice]
GO

/****** Object:  View [RADIO].[V_CONTACTOS]    Script Date: 16/06/2020 7:46:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [RADIO].[V_CONTACTOS]
AS
SELECT        CONVERT(int, SERV.ID) AS SERV_ID, SERV.NUMBER AS SERV_NUMBER, CONVERT(int, ICSM_PROD.dbo.USERS_CNTCT.ID) AS CONT_ID, US.IDENT AS CUS_IDENT, 
                         ICSM_PROD.dbo.USERS_CNTCT.FIRSTNAME AS CONT_NAME, CONVERT(int, ICSM_PROD.dbo.USERS_CNTCT.ROLE) AS CONT_ROLE, ICSM_PROD.dbo.USERS_CNTCT.TEL AS CONT_TEL, 
                         ICSM_PROD.dbo.USERS_CNTCT.REGIST_NUM AS CONT_NUMBER
FROM            ICSM_PROD.dbo.USERS_CNTCT INNER JOIN
                         ICSM_PROD.dbo.SERV_LICENCE AS SERV ON ICSM_PROD.dbo.USERS_CNTCT.OPER_ID = SERV.OWNER_ID INNER JOIN
                         ICSM_PROD.dbo.USERS AS US ON US.ID = SERV.OWNER_ID
WHERE        (ICSM_PROD.dbo.USERS_CNTCT.CUST_TXT4 = '1') AND (ICSM_PROD.dbo.USERS_CNTCT.ROLE IN (1, 2)) AND (ICSM_PROD.dbo.USERS_CNTCT.REGIST_NUM IS NOT NULL) AND 
                         (LEN(ICSM_PROD.dbo.USERS_CNTCT.REGIST_NUM) > 1) AND (SERV.CLASS IN (4, 13, 27)) AND (US.STATUS <> 'uNAC') AND (SERV.STATUS NOT IN ('eX0', 'eVEN', 'eX1', 'eX3', 'eX1B', 'eX4', 'eX4B', 'eX2B'))

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "USERS_CNTCT (ICSM_PROD.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SERV"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 136
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "US"
            Begin Extent = 
               Top = 6
               Left = 497
               Bottom = 136
               Right = 694
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_CONTACTOS'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'RADIO', @level1type=N'VIEW',@level1name=N'V_CONTACTOS'
GO


