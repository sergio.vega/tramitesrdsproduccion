USE [SageFrontOffice]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_FloatToString]    Script Date: 16/06/2020 7:43:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_FloatToString] (
    @fValue float
)
RETURNS NVARCHAR(100)
AS
BEGIN
    DECLARE @sValue NVARCHAR(100)
	DECLARE @nValue numeric(15,2)
	SET @nValue= convert(numeric(16,2),@fValue)
	SET @sValue = convert(nvarchar,CONVERT(MONEY,@nValue),1)
	SET @sValue = replace(replace(replace(@sValue,'.',';'),',','.'),';',',')
	SET @sValue = replace(@sValue,',00','')
    RETURN @sValue
END

GO


