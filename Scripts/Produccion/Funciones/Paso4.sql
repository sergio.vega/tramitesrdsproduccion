USE [SageFrontOffice]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_GetAlarmStateConcesionario]    Script Date: 6/24/2020 11:12:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_GetAlarmStateConcesionario] (
    @SOL_REQUIRED_DATE date,
	@ALARM_PRE_EXPIRE int,
	@ALARM_EXPIRE int,
	@SOL_ENDED int,
	@SOL_REQUIRED_POSTPONE int
)
RETURNS int
AS
BEGIN
	DECLARE @nDays int
	DECLARE @nResult int=1

	if @SOL_ENDED=1
	begin
		set @nResult=4
		return @nResult
	end

	if @SOL_ENDED=2
	begin
		set @nResult=5
		return @nResult
	end

	if @SOL_REQUIRED_DATE is null
	begin
		set @nResult=0
		return @nResult
	end

	
	Set @nDays=	DATEDIFF(day, DATEADD(day,@SOL_REQUIRED_POSTPONE, @SOL_REQUIRED_DATE), Convert(date, getdate()))

	if (@nDays>=@ALARM_EXPIRE)
	begin
		set @nResult=3
		return @nResult
	end
	if (@nDays>=@ALARM_PRE_EXPIRE)
	begin
		set @nResult=2
		return @nResult
	end

	set @nResult=1
	return @nResult
END

GO


