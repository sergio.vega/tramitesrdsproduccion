USE [SageFrontOffice]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_GetMonthName]    Script Date: 16/06/2020 7:44:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_GetMonthName] (
    @Date DATETIME
)
RETURNS NVARCHAR(400)
AS
BEGIN
    DECLARE @i INT, @m INT,@mlist NVARCHAR(1000)
    SET @m = MONTH(@Date)
    SET @mlist = (SELECT Lower(months) FROM sys.syslanguages WHERE ALIAS = 'Spanish')
    SET @i = 1
    WHILE(@i < @m)
        BEGIN
           SET @mlist = REPLACE(@mlist, SUBSTRING(@mlist,1,CHARINDEX(',',@mlist)) ,'')
           SET @i = @i + 1
        END
    SET @mlist = (CASE CHARINDEX(',',@mlist) WHEN 0 THEN @mlist ELSE SUBSTRING(@mlist,0,CHARINDEX(',',@mlist) ) END )
    RETURN @mlist
END

GO


