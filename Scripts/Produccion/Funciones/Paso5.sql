USE [SageFrontOffice]
GO

/****** Object:  UserDefinedFunction [RADIO].[F_GetDateFormat]    Script Date: 16/06/2020 7:43:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [RADIO].[F_GetDateFormat] (
    @Date DATETIME
)
RETURNS NVARCHAR(100)
AS
BEGIN
    DECLARE @mDate NVARCHAR(100)
	SET @mDate = convert(nvarchar,Day(@Date)) + ' de ' + RADIO.F_GetMonthName(@Date) + ' de ' + convert(nvarchar,Year(@Date))
    RETURN @mDate
END

GO


