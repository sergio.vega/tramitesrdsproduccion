﻿using System;

namespace AlarmsServer.Email
{
    public class Email
    {
        public string Uid = "";
        public string From = "";
        public string To = "";
        public string Subject = "";
        public string HtmlBody = "";
        public string TextBody = "";
        public string MessageID = "";
        public DateTime Received = new DateTime();
    }

}
