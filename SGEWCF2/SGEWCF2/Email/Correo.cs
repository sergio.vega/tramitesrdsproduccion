﻿using System;
using System.Collections.Generic;
using LumiSoft.Net.Mail;
using LumiSoft.Net.POP3.Client;
using LumiSoft.Net.SMTP.Client;
using LumiSoft.Net.IMAP.Client;
using LumiSoft.Net.IMAP;
using LumiSoft.Net;
using System.IO;
using LumiSoft.Net.MIME;
using AlarmsServer.Email;
using System.Text;
using System.Configuration;
//using ReadPopMail.MailHelper.Email;
/*
 Stefan Holmberg
 http://aspcode.net
 
 */


namespace ReadPopMail.MailHelper
{
    public class Correo
    {
        private IMAP_Client oClient=null;
        static int SGEEmailPort = GetIntValue(ConfigurationManager.AppSettings["SGEEmailPort"], 25);  


        private static int GetIntValue(string sValor, int nDefault)
        {
            int nRet;
            if (!Int32.TryParse(sValor, out nRet))
                nRet = nDefault;
            return nRet;
        }

        public Correo(string server, int port, string user, string pwd)
        {
            try
            {
                oClient=new IMAP_Client();

                oClient.Connect(server, port, true);
                oClient.Login(user, pwd);
                oClient.SelectFolder("Inbox");
            }
            catch (Exception)
            {
                if (oClient != null)
                {
                    if(oClient.IsConnected)
                        oClient.Disconnect();
                    oClient.Dispose();
                }
                oClient = null;        
            }
        }

        public void Disconnect()
        {
            
            if (oClient != null)
            {
                if(oClient.IsConnected)
                    oClient.Disconnect();
                oClient.Dispose();
            }
            oClient = null;  
        }

        //public bool MoverMail(string ID)
        //{
        //    if (oClient == null) return false;
        //    if (oClient.IsConnected == false) return false;

        //    try
        //    {
        //        IMAP_SequenceSet MoveSet = new IMAP_SequenceSet();
        //        MoveSet.Parse(ID);
        //        oClient.MoveMessages(true, MoveSet, "Leidos", true);
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //    return true;
        //}


        public static bool SendMessage(string[] Cuentas, string[] CuentasCC, string[] CuentasBCC, string sSubject, string MensajeTxt, string MensajeHTML)
        {
            string AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            AppPath = AppPath.Replace(@"file:\", "") + @"\";

            Mail_Message message = new Mail_Message();
            message.MimeVersion = "1.0";
            message.MessageID = MIME_Utils.CreateMessageID();
            message.Date = DateTime.Now;
            message.From = new Mail_t_MailboxList();
            message.From.Add(new Mail_t_Mailbox("Soporte Tesamerica", "soporte.tesamerica@tesamerica.com.co"));

            message.To = new Mail_t_AddressList();
            foreach (var Cuenta in Cuentas)
            {
                message.To.Add(new Mail_t_Mailbox(null, Cuenta));
            }

            message.Cc = new Mail_t_AddressList();
            foreach (var CuentaCC in CuentasCC)
            {
                message.Cc.Add(new Mail_t_Mailbox(null, CuentaCC));
            }            

            message.Bcc = new Mail_t_AddressList();
            foreach (var CuentaBCC in CuentasBCC)
            {
                message.Bcc.Add(new Mail_t_Mailbox(null, CuentaBCC));
            }

            //----------------------------------------------------
            message.ReplyTo = new Mail_t_AddressList();
            message.ReplyTo.Add(new Mail_t_Mailbox("Soporte Tesamerica", "soporte.tesamerica@tesamerica.com.co"));


            message.Subject = sSubject;

            // multipart/mixed 
            MIME_h_ContentType contentType_multipartMixed = new MIME_h_ContentType(MIME_MediaTypes.Multipart.related);
            contentType_multipartMixed.Param_Boundary = Guid.NewGuid().ToString().Replace('-', '.');
            MIME_b_MultipartRelated multipartMixed = new MIME_b_MultipartRelated(contentType_multipartMixed);
            message.Body = multipartMixed;

            // multipart/alternative
            MIME_Entity entity_multipartAlternative = new MIME_Entity();
            MIME_h_ContentType contentType_multipartAlternative = new MIME_h_ContentType(MIME_MediaTypes.Multipart.alternative);
            contentType_multipartAlternative.Param_Boundary = Guid.NewGuid().ToString().Replace('-', '.');
            MIME_b_MultipartAlternative multipartAlternative = new MIME_b_MultipartAlternative(contentType_multipartAlternative);
            entity_multipartAlternative.Body = multipartAlternative;
            multipartMixed.BodyParts.Add(entity_multipartAlternative);

            // text/plain
            MIME_Entity entity_text_plain = new MIME_Entity();
            MIME_b_Text text_plain = new MIME_b_Text(MIME_MediaTypes.Text.plain);
            entity_text_plain.Body = text_plain;
            text_plain.SetText(MIME_TransferEncodings.QuotedPrintable, Encoding.UTF8, MensajeTxt);
            multipartAlternative.BodyParts.Add(entity_text_plain);

            // text/html
            MIME_Entity entity_text_html = new MIME_Entity();
            MIME_b_Text text_html = new MIME_b_Text(MIME_MediaTypes.Text.html);
            entity_text_html.Body = text_html;

            // text_html.SetText(MIME_TransferEncodings.SevenBit, Encoding.UTF8, "<html><body>This is mail message <u>html</u> body text. <img src=\"cid:12345\" /></body></html>");
            text_html.SetText(MIME_TransferEncodings.SevenBit, Encoding.UTF8, MensajeHTML);
            multipartAlternative.BodyParts.Add(entity_text_html);

            try
            {
                using (SMTP_Client smtpClient = new SMTP_Client())
                {
                    smtpClient.Timeout = 180000;
                    smtpClient.Connect("in-v3.mailjet.com", SGEEmailPort, false);
                    smtpClient.EhloHelo("in-v3.mailjet.com");
                    smtpClient.Auth(smtpClient.AuthGetStrongestMethod("adeaf25e41da3e7961c11248f491bff2", "5bef11bf6c00bd50d3f175813a079dde"));

                    try
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            message.ToStream(stream, null, null);
                            stream.Position = 0;

                            smtpClient.MailFrom(((Mail_t_Mailbox)message.From[0]).Address, stream.Length);

                            foreach (Mail_t_Address to in message.To)
                                smtpClient.RcptTo(((Mail_t_Mailbox)to).Address);

                            foreach (Mail_t_Address cc in message.Cc)
                                smtpClient.RcptTo(((Mail_t_Mailbox)cc).Address);

                            foreach (Mail_t_Address bcc in message.Bcc)
                                smtpClient.RcptTo(((Mail_t_Mailbox)bcc).Address);

                            smtpClient.SendMessage(stream);
                        }

                        smtpClient.Disconnect();
                    }
                    catch (Exception ex)
                    {

                        File.AppendAllText(AppPath + "Log.txt", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "\r\n" + ex.Message + "\r\n" + sSubject + "\r\n" + "--------1-------" + "\r\n");

                        smtpClient.Disconnect();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(AppPath + "Log.txt", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "\r\n" + ex.Message + "\r\n" + sSubject + "\r\n" + "--------2-------" + "\r\n");
                return false;
            }
            return true;
        }


        public static bool SendMessageAttachment(string[] Cuentas, string sSubject, string MensajeTxt, string MensajeHTML, string sFileName, MemoryStream sXML)
        {
            string AppPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            AppPath = AppPath.Replace(@"file:\", "") + @"\";

            Mail_Message message = new Mail_Message();
            message.MimeVersion = "1.0";
            message.MessageID = MIME_Utils.CreateMessageID();
            message.Date = DateTime.Now;
            message.From = new Mail_t_MailboxList();
            message.From.Add(new Mail_t_Mailbox("ClickPalm", "clickpalm@clickpalm.com"));
            message.To = new Mail_t_AddressList();

            foreach (var Cuenta in Cuentas)
            {
                message.To.Add(new Mail_t_Mailbox(null, Cuenta));
            }
           // message.Bcc = new Mail_t_AddressList();
           // message.Bcc.Add(new Mail_t_Mailbox(null, "clickidea@clickidea.com"));

            message.Cc = new Mail_t_AddressList();
            message.Cc.Add(new Mail_t_Mailbox(null, "alvaro.patino@tesamerica.com.co"));
            message.Cc.Add(new Mail_t_Mailbox(null, "alvaropatino@gmail.com"));

            //----------------------------------------------------
            message.ReplyTo = new Mail_t_AddressList();
            message.ReplyTo.Add(new Mail_t_Mailbox("Alvaro", "alvaro.patino@tesamerica.com.co"));


            string sSubjectISO = "=?ISO-8859-1?B?" + Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(sSubject)) + "=?=";
            message.Subject = sSubjectISO;



            // multipart/mixed 
            MIME_h_ContentType contentType_multipartMixed = new MIME_h_ContentType(MIME_MediaTypes.Multipart.mixed);
            contentType_multipartMixed.Param_Boundary = Guid.NewGuid().ToString().Replace('-', '.');
            MIME_b_MultipartMixed multipartMixed = new MIME_b_MultipartMixed(contentType_multipartMixed);
            message.Body = multipartMixed;

            // multipart/alternative
            MIME_Entity entity_multipartAlternative = new MIME_Entity();
            MIME_h_ContentType contentType_multipartAlternative = new MIME_h_ContentType(MIME_MediaTypes.Multipart.alternative);
            contentType_multipartAlternative.Param_Boundary = Guid.NewGuid().ToString().Replace('-', '.');
            MIME_b_MultipartAlternative multipartAlternative = new MIME_b_MultipartAlternative(contentType_multipartAlternative);
            entity_multipartAlternative.Body = multipartAlternative;
            multipartMixed.BodyParts.Add(entity_multipartAlternative);

            // text/plain
            MIME_Entity entity_text_plain = new MIME_Entity();
            MIME_b_Text text_plain = new MIME_b_Text(MIME_MediaTypes.Text.plain);
            entity_text_plain.Body = text_plain;
            text_plain.SetText(MIME_TransferEncodings.QuotedPrintable, Encoding.UTF8, MensajeTxt);
            multipartAlternative.BodyParts.Add(entity_text_plain);

            // text/html
            MIME_Entity entity_text_html = new MIME_Entity();
            MIME_b_Text text_html = new MIME_b_Text(MIME_MediaTypes.Text.html);
            entity_text_html.Body = text_html;
            // text_html.SetText(MIME_TransferEncodings.SevenBit, Encoding.UTF8, "<html><body>This is mail message <u>html</u> body text. <img src=\"cid:12345\" /></body></html>");
            text_html.SetText(MIME_TransferEncodings.SevenBit, Encoding.UTF8, MensajeHTML);
            multipartAlternative.BodyParts.Add(entity_text_html);

            /*
            MIME_Entity attachmentEntity = new MIME_Entity();
            MIME_h_ContentDisposition mh = new MIME_h_ContentDisposition(MIME_DispositionTypes.Attachment);
            mh.Param_FileName = sFileName;
            attachmentEntity.ContentDisposition = mh;
            MIME_b_Text aa = new MIME_b_Text(MIME_MediaTypes.Text.xml);
            attachmentEntity.Body = aa;
            aa.SetText(MIME_TransferEncodings.SevenBit, Encoding.UTF8, sXML);
            multipartAlternative.BodyParts.Add(attachmentEntity);
            */

            //   MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(sXML));



            MIME_Entity Adjunto = Mail_Message.CreateAttachment(sXML, sFileName);
            multipartMixed.BodyParts.Add(Adjunto);


            try
            {
                using (SMTP_Client smtpClient = new SMTP_Client())
                {
                    smtpClient.Timeout = 180000;

                    smtpClient.Connect("in-v3.mailjet.com", SGEEmailPort, false);
                    smtpClient.EhloHelo("in-v3.mailjet.com");
                    smtpClient.Auth(smtpClient.AuthGetStrongestMethod("a9de36681d6d6dd2d7da119769291af8", "c31f6fb7c5a0417d22198a56632da82e"));

                    try
                    {
                        using (MemoryStream stream = new MemoryStream())
                        {
                            message.ToStream(stream, null, null);
                            stream.Position = 0;

                            smtpClient.MailFrom(((Mail_t_Mailbox)message.From[0]).Address, stream.Length);
                            foreach (Mail_t_Address to in message.To)
                                smtpClient.RcptTo(((Mail_t_Mailbox)to).Address);
                            foreach (Mail_t_Address cc in message.Cc)
                                smtpClient.RcptTo(((Mail_t_Mailbox)cc).Address);


                            //foreach (Mail_t_Address bcc in message.Bcc)
                            //    smtpClient.RcptTo(((Mail_t_Mailbox)bcc).Address);

                            smtpClient.SendMessage(stream);
                        }

                        smtpClient.Disconnect();
                    }
                    catch (Exception ex)
                    {

                        File.AppendAllText(AppPath + "Log.txt", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "\r\n" + ex.Message + "\r\n" + sSubject + "\r\n" + "--------1-------" + "\r\n");

                        smtpClient.Disconnect();
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(AppPath + "Log.txt", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + "\r\n" + ex.Message + "\r\n" + sSubject + "\r\n" + "--------2-------" + "\r\n");
                return false;
            }
            return true;
        }

        //public List<Email> ReadMails()
        //{
        //    Email wrapper = new Email();
        //    var ret = new List<Email>();

        //    if (oClient == null) return null;

        //    try
        //    {
        //        IMAP_Client_FetchHandler fetchHandler = new IMAP_Client_FetchHandler();
        //        fetchHandler.NextMessage += new EventHandler(delegate(object s, EventArgs e)
        //        {
        //            wrapper = new Email();
        //        });

        //        fetchHandler.Envelope += new EventHandler<EventArgs<IMAP_Envelope>>(delegate(object s, EventArgs<IMAP_Envelope> e)
        //        {
        //            IMAP_Envelope envelope = e.Value;

        //            string from = "";
        //            if (envelope.From != null)
        //            {
        //                for (int i = 0; i < envelope.From.Length; i++)
        //                {
        //                    // Don't add ; for last item
        //                    if (i == envelope.From.Length - 1)
        //                    {
        //                       // from += envelope.From[i].ToString();
        //                        from += ((LumiSoft.Net.Mail.Mail_t_Mailbox)envelope.From[i]).Address;
        //                    }
        //                    else
        //                    {
        //                        //from += envelope.From[i].ToString() + ";";
        //                        from += ((LumiSoft.Net.Mail.Mail_t_Mailbox)envelope.From[i]).Address + ";";
        //                    }
        //                }
        //            }
        //            else
        //                from = "<none>";

        //            wrapper.From = from;
        //            wrapper.Subject = envelope.Subject != null ? envelope.Subject : "<none>";
        //            ret.Add(wrapper);
        //        });

        //        fetchHandler.Flags += new EventHandler<EventArgs<string[]>>(delegate(object s, EventArgs<string[]> e)
        //        {
        //        });
        //        fetchHandler.InternalDate += new EventHandler<EventArgs<DateTime>>(delegate(object s, EventArgs<DateTime> e)
        //        {
        //            //currentItem.SubItems[m_pTabPageMail_Messages.Columns["Received"].Index].Text = e.Value.ToString();
        //        });
        //        fetchHandler.Rfc822Size += new EventHandler<EventArgs<int>>(delegate(object s, EventArgs<int> e)
        //        {
        //            //currentItem.SubItems[m_pTabPageMail_Messages.Columns["Size"].Index].Text = ((decimal)(e.Value / (decimal)1000)).ToString("f2") + " kb";
        //        });
        //        fetchHandler.UID += new EventHandler<EventArgs<long>>(delegate(object s, EventArgs<long> e)
        //        {
        //            wrapper.Uid = e.Value.ToString();
        //        });

        //        fetchHandler.Rfc822 += new EventHandler<IMAP_Client_Fetch_Rfc822_EArgs>(delegate(object s, IMAP_Client_Fetch_Rfc822_EArgs e)
        //        {
        //            MemoryStream storeStream = new MemoryStream();
        //            e.Stream = storeStream;
        //            e.StoringCompleted += new EventHandler(delegate(object s1, EventArgs e1)
        //            {
        //                storeStream.Position = 0;
        //                Mail_Message mime = Mail_Message.ParseFromStream(storeStream);


        //                string sa = mime.BodyHtmlText;
        //                wrapper.MessageID = mime.MessageID;
        //                if (sa == null)
        //                {
        //                    wrapper.TextBody = mime.BodyText;
        //                }
        //                else
        //                {
        //                    wrapper.TextBody = mime.BodyText;
        //                    if (wrapper.TextBody == null) wrapper.TextBody = "";
        //                    wrapper.HtmlBody = sa;
        //                }
        //            });
        //        });

        //        IMAP_SequenceSet seqSet = new IMAP_SequenceSet();
        //        seqSet.Parse("1:*");
        //        oClient.Fetch(
        //            false,
        //            seqSet,
        //            new IMAP_Fetch_DataItem[]{
        //                new IMAP_Fetch_DataItem_Envelope(),
        //                //new IMAP_Fetch_DataItem_Flags(),
        //                //new IMAP_Fetch_DataItem_InternalDate(),
        //                //new IMAP_Fetch_DataItem_Rfc822Size(),
        //                new IMAP_Fetch_DataItem_Uid(),
        //                new IMAP_Fetch_DataItem_Rfc822()
        //            },
        //            fetchHandler
        //        );

        //        /*
        //        IMAP_SequenceSet MoveSet = new IMAP_SequenceSet();
        //        if (ret != null)
        //        {
        //            foreach (var email in ret)
        //            {
        //                MoveSet.Parse(email.Uid);
        //                oClient.MoveMessages(true, MoveSet, "Leidos", true);
        //            }
        //        }
        //        */
        //    }
        //    catch (Exception x)
        //    {
        //        //errors.Add("Error reading email - " + x.Message);
        //        return null;
        //    }

        //    //oClient.Disconnect();
        //    //oClient.Dispose();
        //    return ret;
        //}

    }
}
