﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.pipeline.css;
using System;
using System.IO;
using System.Text;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.pipeline.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.end;

namespace SGEWCF2.Helpers
{
    public class XMLWorkerHelper
    {
        public static ElementList ParseToElementList(String html, String css)
        {
            // CSS
            ICSSResolver cssResolver = new StyleAttrCSSResolver();
            if (css != null)
            {
                ICssFile cssFile = iTextSharp.tool.xml.XMLWorkerHelper.GetCSS(new MemoryStream(Encoding.Default.GetBytes(css)));
                cssResolver.AddCss(cssFile);
            }

            // HTML
            CssAppliers cssAppliers = new CssAppliersImpl(FontFactory.FontImp);
            HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
            htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
            htmlContext.AutoBookmark(false);

            // Pipelines
            ElementList elements = new ElementList();
            ElementHandlerPipeline end = new ElementHandlerPipeline(elements, null);
            HtmlPipeline htmlPipeline = new HtmlPipeline(htmlContext, end);
            CssResolverPipeline cssPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);

            // XML Worker
            XMLWorker worker = new XMLWorker(cssPipeline, true);
            XMLParser p = new XMLParser(worker);
            //p.Parse(new MemoryStream(Encoding.Default.GetBytes(html)));
            p.Parse(new StringReader(html));
            return elements;
        }

    }
}