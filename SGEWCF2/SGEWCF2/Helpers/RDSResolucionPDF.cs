﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using itext=iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Xml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Packaging;
using Xceed.Words.NET;

namespace SGEWCF2.Helpers
{
    public class RDSResolucionPDF
    {
        #region Variables

        private itext.Font fontNormal;
        private itext.Font fontBold;
        private itext.Font fontTabla;
        private itext.Font fontTablaBold;
        private itext.Font fontCursiva;
        private itext.Font fontBoldTitulo;
        private itext.Font fontFirmas;
        private float margenLeft = 30;
        private float margenRight = 25;
        private float margenTop = 32;
        private float margenBottom = 42;
        private ST_RDSResPlt ResPlt;
        private RDSDynamicSolicitud DynamicSolicitud;
        private bool firmaDividida = false;
        #endregion

        public RDSResolucionPDF(ref ST_RDSResPlt ResPlt, ref RDSDynamicSolicitud DynamicSolicitud)
        {
            fontNormal = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 11, itext.Font.NORMAL);
            fontBold = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 10, itext.Font.BOLD);
            fontTabla = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 9, itext.Font.NORMAL);
            fontTablaBold = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 9, itext.Font.BOLD);
            fontCursiva = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 10, itext.Font.ITALIC);
            fontBoldTitulo = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 12, itext.Font.BOLD);
            fontFirmas = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 8, itext.Font.NORMAL);
            this.ResPlt=ResPlt;
            this.DynamicSolicitud = DynamicSolicitud;
        }

        public MemoryStream GenerarResolucion(string Titulo, string Asunto)
        {
            float ml = iTextSharp.text.Utilities.MillimetersToPoints(margenLeft);
            float mr = iTextSharp.text.Utilities.MillimetersToPoints(margenRight);
            float mt = iTextSharp.text.Utilities.MillimetersToPoints(margenTop);
            float mb = iTextSharp.text.Utilities.MillimetersToPoints(margenBottom);
            
            MemoryStream mem = new MemoryStream();
            itext.Document documento = CrearDocumentoPDF();
            PdfWriter writer = PdfWriter.GetInstance(documento, mem);

            MemoryStream memSign = new MemoryStream();
            itext.Document documentoSign = CrearDocumentoPDF();
            PdfWriter writerSign = PdfWriter.GetInstance(documentoSign, memSign); 


            try
            {
                string pathFonts = Path.Combine(Path.GetTempPath(), HttpContext.Current.Server.MapPath(@"~/Fonts"));
                itext.FontFactory.RegisterDirectory(pathFonts);

                CrearEncabezados(ref documento, Titulo, Asunto);
                CrearEncabezados(ref documentoSign, Titulo, Asunto);

                writer.CloseStream = false;
                writer.PageEvent = new PDFFooter(ref ResPlt, ref DynamicSolicitud, margenLeft, margenRight, margenTop, margenBottom);
                writerSign.CloseStream = false;
                writerSign.PageEvent = new PDFFooter(ref ResPlt, ref DynamicSolicitud, margenLeft, margenRight, margenTop, margenBottom);

                documento.Open();
                documentoSign.Open();

                AddTitle(ref documento);
                AddTitle(ref documentoSign);

                documento.Add(new itext.Paragraph("\n"));
                documentoSign.Add(new itext.Paragraph("\n"));

                List<ST_Paragraph> lstParrafos = GetParagraphs(ResPlt.RES_PLT_CONTENT);
                ResPlt.RES_PLT_EPIGRAFE = GetEpigrafe(ref lstParrafos);
                
                AgregarInformacionHtml(documento, documentoSign, ResPlt.RES_PLT_CONTENT, writer);

            }
            catch (itext.DocumentException)
            {

            }
            catch (IOException)
            {

            }
            finally
            {
                writer.CloseStream = false;
                documento.Close();
                writerSign.CloseStream = false;
                documentoSign.Close();
            }
            if(firmaDividida)
                return memSign;
            else
                return mem;
        }

        public MemoryStream GenerarResolucionWord(string Titulo, string Asunto)
        {

            MemoryStream ms = new MemoryStream();
            try
            {
                List<ST_Paragraph> Paragrafo = GetParagraphs(ResPlt.RES_PLT_CONTENT);

                string epigrafeSinTags = GetEpigrafe(ref Paragrafo);
                epigrafeSinTags = Regex.Replace(epigrafeSinTags, "&nbsp;", string.Empty);
                string versionSinTags = "";
                string tituloSinTags = "";

                string html = Regex.Replace(ResPlt.RES_PLT_CONTENT, "&nbsp;", string.Empty);
                epigrafeSinTags = RemoveHTMLTags(epigrafeSinTags);
                epigrafeSinTags = RemoveHTMLPunctuation(epigrafeSinTags);
                using (ms)
                {
                    //DocX document = DocX.Load(@"D:\nuevo sge\SGEWeb\SGEWeb\contents\PlantillaRes.docx");
                    DocX document = DocX.Load(HttpContext.Current.Server.MapPath(@"~/Contents") + @"\PlantillaRes.docx");
                    {
                        document.DifferentFirstPage = true;

                        document.SaveAs(ms);
                        ms.Position = 0;

                        using (WordprocessingDocument myDoc = WordprocessingDocument.Open(ms, true))
                        {
                            MainDocumentPart mainPart = myDoc.MainDocumentPart;

                            if (html != null)
                            {
                                SaveHtmlDocument(@"<html><head/><body>" + html + "<br><br></body></html>", "AltChunkId5", ms, mainPart);
                            }


                            foreach (var headerPart in myDoc.MainDocumentPart.HeaderParts)
                            {

                                foreach (var currentText in headerPart.RootElement.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>())
                                {
                                    currentText.Text = currentText.Text.Replace("TITULORES", epigrafeSinTags);
                                }
                            }
                            mainPart.Document.Save();
                        }
                        return ms;
                    }
                }


            }
            catch (Exception e)
            {

                throw e;
            }


            return ms;

        }

        public static string RemoveHTMLPunctuation(string input)
        {
            string s1 = input;
            s1 = System.Net.WebUtility.HtmlDecode(s1);
            //replace html left right single double quotation marks
            s1 = Regex.Replace(s1, "€¦", "…");
            s1 = Regex.Replace(s1, "â€™", "'");
            s1 = Regex.Replace(s1, "€œ|€", "\"");
            //replace unicode right and left quotation marks with straight quotation
            string s2 = s1.Replace("&ldquo;", "\x201c");
            string s3 = s2.Replace("&rsquo;", "\x2019");
            string s4 = s3.Replace("&rdquo;", "\x201d");
            string s5 = s4.Replace("&hellip;", "\x2026");
            string s6 = s5.Replace("&nbsp;", "");
            s6 = s6.Replace("&laquo;", "");
            string s7 = s6.Replace("&quot;", "\"");
            string s8 = s7.Replace("&amp;", "&");
            s8 = Regex.Replace(s8, "&[a-z]+;", "");
            s8 = Regex.Replace(s8, "&#39;", "'");
            //remove non breaking space
            s8 = Regex.Replace(s8, "&#160;|Â", "");
            //add missing spaces after punctuation marks
            //s8 = Regex.Replace(s8, "([\\.\\?,;:])(\\w+)", "$1 $2");
            return s8;
        }

        public static string RemoveHTMLTags(string input)
        {
            string s1 = input;
            //remove script tag and everything within.
            s1 = Regex.Replace(s1, "\\<script\\s*[^><]+\\>[^><]*\\</\\s*script\\>", "");
            s1 = Regex.Replace(s1, "\\<\\s*br\\s*/*\\s*\\>", Environment.NewLine);
            //add new line for div p or li tag
            s1 = Regex.Replace(s1, "\\<\\s*/(div|p|li)\\s*\\s*\\>", Environment.NewLine);
            s1 = Regex.Replace(s1, "\\>=", "");
            string s2 = Regex.Replace(s1, "&ldquo;", "\x201c");
            string s3 = Regex.Replace(s2, "\\<[Aa]([^><]+|\\s*)\\>.*\\</\\s*[Aa]\\s*\\>", "");
            string s4 = Regex.Replace(s3, "\\<[^<>]+\\>", "");
            string s5 = Regex.Replace(s4, "\\|", "");
            //replace multiple lines with 1 line
            s5 = Regex.Replace(s5, "(\\r\\n|\\r|\\n){2,}", Environment.NewLine);
            //any annoying text put it here to replace from post text
            //s5 = Regex.Replace(s5, "Copyright (c) 2008 Saadia Malik", "");
            s5 = s5.Trim();
            return s5;
        }

        private itext.Document CrearDocumentoPDF()
        {
            float ml = iTextSharp.text.Utilities.MillimetersToPoints(margenLeft);
            float mr = iTextSharp.text.Utilities.MillimetersToPoints(margenRight);
            float mt = iTextSharp.text.Utilities.MillimetersToPoints(margenTop);
            float mb = iTextSharp.text.Utilities.MillimetersToPoints(margenBottom);
            itext.Document documento = new itext.Document();
            documento = new itext.Document(itext.PageSize.LEGAL, ml, mr, mt, mb);

            return documento;
        }

        public void SaveHtmlDocument(string html, string altChunkId, MemoryStream ms, MainDocumentPart mainPart)
        {

            AlternativeFormatImportPart chunk = mainPart.AddAlternativeFormatImportPart("application/xhtml+xml", altChunkId);

            using (Stream chunkStream = chunk.GetStream(FileMode.Create, FileAccess.Write))
            using (StreamWriter stringStream = new StreamWriter(chunkStream))
                stringStream.Write(html);

            AltChunk altChunk = new AltChunk();
            altChunk.Id = altChunkId;

            // this inserts altChunk after the last Paragraph
            mainPart.Document.Body.InsertBefore(altChunk, mainPart.Document.Body.Elements<DocumentFormat.OpenXml.Wordprocessing.Paragraph>().Last());

        }

        private void CrearEncabezados(ref itext.Document documento, string Titulo, string Asunto)
        {
            //documento.AddTitle("PLANTILLA RESOLUCIONES");
            //documento.AddSubject("plantilla resolución");
            //documento.AddKeywords("plantilla resolución SGE");
            documento.AddTitle(Titulo);
            documento.AddSubject(Asunto);
            documento.AddKeywords("plantilla resolución SGE");
            documento.AddCreator("Front Office SGE MinTIC");
            documento.AddAuthor("Tes America Andina S.A.S");
        }

        private void AddTitle(ref itext.Document documento)
        {
            documento.Add(new itext.Paragraph("\n\n\n"));
            itext.Paragraph nombreMinisterio = new itext.Paragraph(new itext.Phrase("MINISTERIO DE TECNOLOGÍAS DE LA INFORMACIÓN Y LAS", fontBoldTitulo));
            nombreMinisterio.Alignment = itext.Rectangle.ALIGN_CENTER;
            documento.Add(nombreMinisterio);
            itext.Paragraph nombreMinisterio1 = new itext.Paragraph(new itext.Phrase("COMUNICACIONES", fontBoldTitulo));
            nombreMinisterio1.Alignment = itext.Rectangle.ALIGN_CENTER;
            documento.Add(nombreMinisterio1);
        }

        private List<ST_Paragraph> GetParagraphs(string Cadena)
        {
            List<ST_Paragraph> lstString = new List<ST_Paragraph>();
            var Items = Cadena.Split(new string[] { "<p ", "</p>" }, StringSplitOptions.None);


            int Contador = 1;
            foreach (var Item in Items)
            {
                bool isParagraph = (Contador++ % 2 == 0);
                if (isParagraph)
                {
                    var Aligns = Item;
                    lstString.Add(new ST_Paragraph("<p " + Item + "</p>"));
                }
            }
            return lstString;
        }

        private string GetEpigrafe(ref List<ST_Paragraph> lstParrafos)
        {
            string CadResult = DynamicSolicitud.GetKeyTextValue("RESOLUCION_EPIGRAFE_NOTFOUND");
            string CadEpigrafeKeyWord = DynamicSolicitud.GetKeyTextValue("RESOLUCION_EPIGRAFE_KEYWORD");
            
            foreach (var item in lstParrafos)
            {
                if (item.Cadena.IndexOf(CadEpigrafeKeyWord, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    CadResult = item.Cadena;
                    break;
                }
            }
            return CadResult;
        }

        private string GetTableWidth(string CadenaHTML, ref List<int> lstWidths)
        {
            try
            {
                string CadResult = "";
                string CadTabla = "";

                int LIni0, LIni1;

                LIni0 = CadenaHTML.IndexOf("<table");
                if (LIni0 == -1)
                    return CadenaHTML;

                LIni1 = CadenaHTML.IndexOf(">", LIni0, StringComparison.CurrentCultureIgnoreCase);
                if (LIni1 == -1 || LIni0 > LIni1)
                    return CadenaHTML;

                CadTabla = CadenaHTML.Substring(LIni0, LIni1 - LIni0 + 1);

                bool HayBorde = false;
                for (int i = 0; i < 10; i++)
                {
                    if (CadTabla.IndexOf("border=\"" + i + "\"", StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        CadTabla = CadTabla.Replace("border=\"" + i + "\"", "border=\"0\"");
                        lstWidths.Add(i);
                        HayBorde = true;
                        break;
                    }
                }
                if(!HayBorde)
                    lstWidths.Add(0);

                CadResult = CadenaHTML.Substring(0, LIni0);
                CadResult += CadTabla.Replace("<table", "<Table");
                CadResult += CadenaHTML.Substring(LIni1 + 1);


                return GetTableWidth(CadResult, ref lstWidths);
            }
            catch
            {
                return CadenaHTML;
            }
        }

        private void AgregarInformacionHtml(itext.Document documento, itext.Document documentoSign, string CadenaHTML, PdfWriter writer)
        {
            List<int> lstWidths=new List<int>();

            CadenaHTML = GetTableWidth(CadenaHTML, ref lstWidths);

            ElementList HTMLParser = XMLWorkerHelper.ParseToElementList(CadenaHTML, DynamicSolicitud.GetKeyTextValue("RESOLUCION_STYLE"));


            string CadNotifiqueseKeyWord = DynamicSolicitud.GetKeyTextValue("RESOLUCION_NOTIFIQUESE_KEYWORD");

            bool bEncontroNotifiquese = false;
            int PosUltimoParrafoNoEmpty = HTMLParser.Count-1;
            for (int i = 0; i < HTMLParser.Count; i++)
            {
                if (HTMLParser[i].Type == 12) //Párrafo
                {
                    string Cadena = "";
                    foreach (var subItem in HTMLParser[i].Chunks)
                        Cadena += subItem.Content;
                    if (Cadena.IndexOf(CadNotifiqueseKeyWord, StringComparison.CurrentCultureIgnoreCase) != -1)
                    {
                        bEncontroNotifiquese = true;
                        break;
                    }
                    if (!String.IsNullOrEmpty(Cadena.Trim()))
                        PosUltimoParrafoNoEmpty = i;
                }
                else
                {
                    PosUltimoParrafoNoEmpty = i;
                }
            }

            if (!bEncontroNotifiquese)
            {
                PosUltimoParrafoNoEmpty = HTMLParser.Count;
            }

            int TableIndex = 0;

            foreach (itext.IElement item in HTMLParser.GetRange(0, PosUltimoParrafoNoEmpty))
            {
                if (item.Type == 23)
                {
                    float CellBorderWidth = 0.5f;
                    if (TableIndex < lstWidths.Count)
                        CellBorderWidth *= lstWidths[TableIndex++];

                    PdfPTable Tabla = ((PdfPTable)item);

                    foreach (var Row in Tabla.Rows)
                    {
                        foreach (var Cell in Row.GetCells())
                        {
                            if (Cell != null)
                            {
                                Cell.Border = itext.Rectangle.BOX;
                                Cell.BorderColor = itext.BaseColor.BLACK;
                                Cell.BorderWidth = CellBorderWidth;
                            }
                        }
                    }

                    PdfPTable TablaClone = new PdfPTable(Tabla);

                    int TablaRows = Tabla.Rows.Count;
                    float hRemains = writer.GetVerticalPosition(true) - documento.BottomMargin;
                    int RowsDeleted = 0;
                    while (hRemains < TablaClone.CalculateHeights() && TablaClone.Rows.Count > 1)
                    {
                        TablaClone.DeleteLastRow();
                        RowsDeleted++;
                    }
                    if (TablaClone.Rows.Count > 1)
                    {
                        documento.Add(TablaClone);
                        documentoSign.Add(TablaClone);
                    }
                    else
                    {
                        documento.Add(new itext.Paragraph("\n"));
                        documentoSign.Add(new itext.Paragraph("\n"));
                    }

                    for (int i = 0; i < (TablaRows - RowsDeleted - 1); i++)
                    {
                        Tabla.DeleteRow(1);
                    }
                    if (Tabla.Rows.Count > 1)
                    {
                        documento.Add(new itext.Paragraph(""));
                        documento.Add(Tabla);
                        documentoSign.Add(new itext.Paragraph(""));
                        documentoSign.Add(Tabla);
                    }
                }
                else
                {
                    documento.Add(item);
                    documentoSign.Add(item);
                }
            }


            int numPaginaAntes = writer.PageNumber;
            foreach (itext.IElement item in HTMLParser.GetRange(PosUltimoParrafoNoEmpty, HTMLParser.Count - PosUltimoParrafoNoEmpty ))
            {
                documento.Add(item);
            }

            if (numPaginaAntes != writer.PageNumber)
            {
                firmaDividida = true;
                documentoSign.NewPage();
                foreach (itext.IElement item in HTMLParser.GetRange(PosUltimoParrafoNoEmpty, HTMLParser.Count - PosUltimoParrafoNoEmpty ))
                {
                    documentoSign.Add(item);
                }
            }
           
        }

        public class PDFFooter : PdfPageEventHelper
        {
            protected PdfTemplate total;
            protected BaseFont font;
            protected ST_RDSResPlt ResPlt;
            protected RDSDynamicSolicitud DynamicSolicitud;
            float margenLeft1, margenRight1, margenTop1, margenBottom1;
            private itext.Font fontEspacio = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 7, itext.Font.NORMAL);
            /// <summary>
            /// Constructor básico
            /// </summary>
            public PDFFooter(ref ST_RDSResPlt ResPlt, ref RDSDynamicSolicitud DynamicSolicitud, float margenLeft, float margenRight, float margenTop, float margenBottom)
            {
                margenLeft1 = margenLeft;
                margenRight1 = margenRight;
                margenTop1 = margenTop;
                margenBottom1 = margenBottom;
                this.ResPlt = ResPlt;
                this.DynamicSolicitud = DynamicSolicitud;
            }

            //Write on top of document
            public override void OnOpenDocument(PdfWriter writer, itext.Document document)
            {
                base.OnOpenDocument(writer, document);
                total = writer.DirectContent.CreateTemplate(100, 100);
                font = itext.FontFactory.GetFont("arial narrow").GetCalculatedBaseFont(false);
            }

            //Write on start of each page
            public override void OnStartPage(PdfWriter writer, itext.Document document)
            {
                base.OnStartPage(writer, document);

                float ml = iTextSharp.text.Utilities.MillimetersToPoints(margenLeft1 - 10);
                float mr = iTextSharp.text.Utilities.MillimetersToPoints(223 - margenRight1);
                float mt = iTextSharp.text.Utilities.MillimetersToPoints(margenTop1 - 1);
                float mb = iTextSharp.text.Utilities.MillimetersToPoints(372 - margenBottom1);

                itext.Rectangle rect = new itext.Rectangle(ml, mb, mr, mt);
                rect.Border = itext.Rectangle.BOX;
                rect.BorderWidth = 3.5f;
                rect.BorderColor = itext.BaseColor.BLACK;
                writer.DirectContent.Rectangle(rect);
                
                if (writer.PageNumber == 1)
                {
                    itext.Rectangle rectLogo = new itext.Rectangle(263, mb, 366, mb);
                    rectLogo.Border = itext.Rectangle.BOX;
                    rectLogo.BorderWidth = 10f;
                    rectLogo.BorderColor = itext.BaseColor.WHITE;
                    writer.DirectContent.Rectangle(rectLogo);

                    itext.Image escudo = itext.Image.GetInstance(DynamicSolicitud.GetKeyImageValue("COLOMBIA_ESCUDO"));
                    escudo.ScalePercent(45, 45);
                    escudo.Alignment = iTextSharp.text.Image.ALIGN_MIDDLE;
                    escudo.SetAbsolutePosition(270f, 888f);
                    writer.DirectContent.AddImage(escudo);
                }
                else
                {
                    //Agregar texto antes del recuadro
                    PdfPTable tablaFooter = new PdfPTable(1);
                    tablaFooter.DefaultCell.HorizontalAlignment = itext.Element.ALIGN_CENTER;
                    tablaFooter.DefaultCell.VerticalAlignment = itext.Element.ALIGN_MIDDLE;
                    tablaFooter.DefaultCell.BorderWidth = 0;
                    tablaFooter.DefaultCell.Padding = 2;
                    tablaFooter.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_CENTER;
                    tablaFooter.DefaultCell.UseVariableBorders = false;
                    tablaFooter.TotalWidth = 500;

                    itext.Font font = itext.FontFactory.GetFont(itext.FontFactory.HELVETICA, 9, itext.Font.NORMAL);
                    string encabezado = "CONTINUACIÓN DE LA RESOLUCIÓN NÚMERO _______________ DE " + DateTime.Now.Year + " HOJA No. " + writer.PageNumber;
                    itext.Phrase phrase = new itext.Phrase(encabezado, font);
                    tablaFooter.AddCell(phrase);
                    tablaFooter.WriteSelectedRows(0, -1, 60f, 974f, writer.DirectContent);

                    //Agrega Epigrafe en el encabezado
                    if (ResPlt.RES_PLT_EPIGRAFE != null)
                    {
                        String htmlEpigrafe = ResPlt.RES_PLT_EPIGRAFE;
                        ElementList epigrafeParser = XMLWorkerHelper.ParseToElementList(htmlEpigrafe, "");

                        foreach (itext.IElement item in epigrafeParser)
                        {
                            document.Add(item);
                        }
                    }
                    
                    //Agregar línea horizontal
                    iTextSharp.text.pdf.draw.LineSeparator separador = new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, itext.BaseColor.BLACK, itext.Element.ALIGN_JUSTIFIED, 1);
                    separador.LineWidth = 0.5f;
                    itext.Chunk linea = new itext.Chunk(separador);
                    linea.Font = itext.FontFactory.GetFont("Calibri", 9, itext.Font.NORMAL);
                    document.Add(new itext.Paragraph(linea));

                    itext.Paragraph parrafo = new itext.Paragraph(new itext.Phrase("\n", fontEspacio));
                    document.Add(parrafo);
                }
            }

            // write on end of each page
            public override void OnEndPage(PdfWriter writer, itext.Document document)
            {
                // Se crea una tabla de una sola columna para escibir la información en texto del footer
                base.OnEndPage(writer, document);

                PdfContentByte cb = writer.DirectContent;
                cb.SaveState();
                string text = "Página " + writer.PageNumber + " de ";
                cb.BeginText();
                cb.SetFontAndSize(font, 8);
                float adjust = font.GetWidthPoint(text, 8);
                cb.SetTextMatrix(292f, 68f);
                cb.ShowText(text);
                cb.EndText();
                //cb.AddTemplate(total, 309f + adjust, 68f);
                cb.AddTemplate(total, 292f + adjust, 68f);
                cb.RestoreState();

                //Agregar texto antes del recuadro
                itext.Chunk versionDoc = new itext.Chunk();
                PdfPTable tablaFooter = new PdfPTable(1);
                tablaFooter.DefaultCell.HorizontalAlignment = itext.Element.ALIGN_RIGHT;
                tablaFooter.DefaultCell.VerticalAlignment = itext.Element.ALIGN_MIDDLE;
                tablaFooter.DefaultCell.BorderWidth = 0;
                tablaFooter.DefaultCell.Padding = 2;
                tablaFooter.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_RIGHT;
                tablaFooter.DefaultCell.UseVariableBorders = false;
                tablaFooter.TotalWidth = 100;

                //Agrega Epigrafe en el encabezado
                if (ResPlt.RES_PLT_VERSION != null)
                {
                    String htmlVersionDocumento = "<p style=\"text-align:right\"><span style=\"font-family:&quot;Arial Narrow&quot;font-size:8pt;\">" + ResPlt.RES_PLT_CODIGO + "</span></p>";
                    htmlVersionDocumento += "<p style=\"text-align:right\"><span style=\"font-family:&quot;Arial Narrow&quot;font-size:8pt;\">" + ResPlt.RES_PLT_VERSION + "</span></p>";

                    ElementList versionParser = XMLWorkerHelper.ParseToElementList(htmlVersionDocumento, "");
                    itext.Font fontVersion = itext.FontFactory.GetFont("arial narrow",8, itext.Font.NORMAL);

                    foreach (itext.IElement item in versionParser)
                    {
                        foreach (itext.Chunk valor in item.Chunks)
                        {
                            versionDoc = valor;
                            itext.Phrase phrase = new itext.Phrase(versionDoc.ToString(), fontVersion);
                            tablaFooter.AddCell(phrase);
                        }
                    }
                }

                tablaFooter.WriteSelectedRows(0, -1, 445f, 78f, writer.DirectContent);
            }

            //write on close of document
            public override void OnCloseDocument(PdfWriter writer, itext.Document document)
            {
                total.BeginText();
                total.SetFontAndSize(font, 8);
                total.SetTextMatrix(0, 0);
                int pageNumber = writer.PageNumber - 1;
                total.ShowText(Convert.ToString(pageNumber));
                total.EndText();
                base.OnCloseDocument(writer, document);
            }
        }

    }
}