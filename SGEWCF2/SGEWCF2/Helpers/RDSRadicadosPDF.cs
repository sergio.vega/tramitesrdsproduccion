﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SGEWCF2.Helpers
{
    class RDSRadicadosPDF
    {
        protected Font fontNormal;
        protected Font fontBoldLink;
        protected Font fontNormalLink;
        private Font fontBold;
        private static int _margenInferior;
        private string Code = string.Empty;
        private ST_RDSSolicitud Solicitud;
        private RDSDynamicSolicitud DynamicSolicitud;

        public RDSRadicadosPDF(ref RDSDynamicSolicitud DynamicSolicitud)
        {
            fontNormal = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL);
            fontBoldLink = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLUE);
            fontNormalLink = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.BLUE);
            fontBold = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD);
            _margenInferior = 100;
            this.DynamicSolicitud = DynamicSolicitud;
            this.Solicitud = DynamicSolicitud.GetSolicitud();
        }

        private void CrearEncabezados(ref Document documento, string Titulo, string Asunto)
        {
            documento.AddTitle(Titulo);
            documento.AddSubject(Asunto);
            documento.AddKeywords("Comunicado SGE");
            documento.AddCreator("Front Office SGE MinTIC");
            documento.AddAuthor("Tes America Andina S.A.S");
        }

        public MemoryStream GenerarRadicado(string NombreDocumento, string Titulo, string Asunto)
        {
            Document documento = new Document(PageSize.LETTER, 50, 50, 40, 50);
            MemoryStream mem = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(documento, mem);
            try
            {
                CrearEncabezados(ref documento, Titulo, Asunto);

                string sBody = DynamicSolicitud.GetBody(NombreDocumento);

                writer.PageEvent = new PDFFooter(DynamicSolicitud.GetFooterText(NombreDocumento));
                documento.Open();

                AddLogo(documento, DynamicSolicitud.GetHeaderLogo(NombreDocumento));


                documento.Add(new Paragraph("\n"));

                var Parrafos = DynamicSolicitud.GetParagraphs(sBody);
                foreach (var Parrafo in Parrafos)
                {
                    AddParrafo(ref documento, Parrafo);
                }
            }
            catch (DocumentException)
            {

            }
            catch (IOException)
            {

            }
            finally
            {
                writer.CloseStream = false;
                documento.Close();
            }
            return mem;
        }

        private void AddLogo(Document documento, string sLOGO)
        {
            if (string.IsNullOrEmpty(sLOGO) || !DynamicSolicitud.ImageKeyExists(sLOGO))
                return;

            PdfPTable tabla = new PdfPTable(3);
            tabla.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabla.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.DefaultCell.BorderWidth = 1;
            tabla.DefaultCell.Padding = 2;
            tabla.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_CENTER;
            tabla.DefaultCell.UseVariableBorders = false;
            tabla.WidthPercentage = 100;
            tabla.SetWidths(new int[] { 40, 20, 40 });

            Image pngLogoLargo = Image.GetInstance(DynamicSolicitud.GetKeyImageValue(sLOGO));
            //pngLogoLargo.ScalePercent(24f); //No funciona porque cuando se adiciona a la celda se le da fit=true

            PdfPCell cell = new PdfPCell(new Phrase(" "));
            cell.UseVariableBorders = true;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.UseVariableBorders = true;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            cell = new PdfPCell(pngLogoLargo, true);
            cell.UseVariableBorders = true;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            documento.Add(tabla);
        }

        private void AddParrafo(ref Document documento, ST_Paragraph ST_Parrafo)
        {
            Paragraph Parrafo = new Paragraph(new Phrase("", fontBold));

            List<ST_List> Listas = DynamicSolicitud.GetListas(ST_Parrafo.Cadena);
            string CadListas = "";

            foreach (var Lista in Listas)
            {
                if (Lista.isList)
                {
                    switch (Lista.Cadena)
                    {
                        case "AdminAprobar":
                            foreach (var Item in Solicitud.Anexos)
                                CadListas += Item.ANX_TYPE_NAME + ".\n";
                            break;
                        case "AdminRequerir":
                            foreach (var Item in Solicitud.Anexos.Where(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                                CadListas += Item.ANX_TYPE_NAME + ". " + Item.ANX_ROLE1_COMMENT + "\n";
                            break;
                        case "AdminRechazar":
                            if (Solicitud.Financiero.FIN_SEVEN_ALDIA == false)
                                CadListas += "El concesionario NO se encuentra al día con cartera.\n";
                            foreach (var Item in Solicitud.Anexos.Where(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Rechazado))
                                CadListas += Item.ANX_TYPE_NAME + ". " + Item.ANX_ROLE1_COMMENT + "\n";
                            break;
                    }
                }
                else
                {
                    CadListas += Lista.Cadena;
                }
            }

            List<ST_String> lstString = DynamicSolicitud.GetStringBold(CadListas);

            foreach (var Item in lstString)
            {
                if (Item.Bold)
                    if (Item.Anchor)
                        Parrafo.Add(new Phrase(Item.Cadena, fontBoldLink));
                    else
                        Parrafo.Add(new Phrase(Item.Cadena, fontBold));
                else
                    if (Item.Anchor)
                        Parrafo.Add(new Phrase(Item.Cadena, fontNormalLink));
                    else
                        Parrafo.Add(new Phrase(Item.Cadena, fontNormal));
            }

            switch (ST_Parrafo.Align)
            {
                case "LEFT":
                    Parrafo.Alignment = Rectangle.ALIGN_LEFT;
                    break;
                case "RIGHT":
                    Parrafo.Alignment = Rectangle.ALIGN_RIGHT;
                    break;
                case "JUSTIFIED":
                    Parrafo.Alignment = Rectangle.ALIGN_JUSTIFIED;
                    break;
                case "CENTER":
                    Parrafo.Alignment = Rectangle.ALIGN_CENTER;
                    break;
            }

            documento.Add(Parrafo);
        }

        public class PDFFooter : PdfPageEventHelper
        {
            private string PDF_FOOTER = "";

            public PDFFooter(string PDF_FOOTER)
            {
                this.PDF_FOOTER = PDF_FOOTER;
            }

            // write on top of document
            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                base.OnOpenDocument(writer, document);
            }

            // write on start of each page
            public override void OnStartPage(PdfWriter writer, Document document)
            {
                base.OnStartPage(writer, document);
            }

            // write on end of each page
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                base.OnEndPage(writer, document);

                if (string.IsNullOrEmpty(PDF_FOOTER))
                    return;

                PdfPTable tablaFooter = new PdfPTable(1);
                tablaFooter.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaFooter.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                tablaFooter.DefaultCell.BorderWidth = 0;
                tablaFooter.DefaultCell.Padding = 2;
                tablaFooter.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_LEFT;
                tablaFooter.DefaultCell.UseVariableBorders = false;
                tablaFooter.TotalWidth = 300;

                Font font = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL);

                Phrase phrase = new Phrase(PDF_FOOTER, font);
                tablaFooter.AddCell(phrase);
                tablaFooter.WriteSelectedRows(0, -1, document.LeftMargin, _margenInferior - 15, writer.DirectContent);
            }

            //write on close of document
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);
            }
        }
    }
}