﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace SGEWCF2.Helpers
{
    /// <summary>
    /// Esta clase mantiene la información que retorna la consulta de comunicados por trámite de Alfa
    /// </summary>
    public class ResultadoComunicadosXTramiteAlfa
    {
        private ResultadoComunicadosXTramiteAlfa(bool ocurrioError, string mensajeError, List<InfoComunicadoAlfa> comunicados)
        {
            _ocurrioError = ocurrioError;
            _mensajeError = mensajeError;
            _comunicados = comunicados;
        }

        /// <summary>
        /// Esta función retorna un objeto con la información de comunicados de Alfa a partir de la información en lista de xmls que retorna Alfa 
        /// </summary>
        public static ResultadoComunicadosXTramiteAlfa CrearResultadoComunicadosXTramiteAlfa(string[] xmlInfo)
        {
            // Con el primer item de la lista se sabe si ocurrió o no un error
            if (xmlInfo != null && xmlInfo.Count() > 0)
            {
                XmlDocument xmlResult = new XmlDocument();
                string tagError = "ERROR";

                // Revisa si viene informaci'on de error en la primera l'inea
                if (xmlInfo[0].Contains(tagError))
                {
                    string info = LimpiarPrimerElementoComunicadosXTramiteAlfa(xmlInfo[0]);

                    // Solo seguimos revisando si qued'o informaci'on en el string
                    xmlResult.LoadXml(info);

                    string mensajeError = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, tagError);
                    bool ocurrioError = (mensajeError != null);

                    if (ocurrioError)
                    {
                        return new ResultadoComunicadosXTramiteAlfa(true, mensajeError, null);
                    }
                }
            }
            else
            {
                return new ResultadoComunicadosXTramiteAlfa(true, "El servicio retornó una respuesta vacía", null);
            }


            // Si no hubo error, lee los comunicados y crea la isntancia del resultado
            List<InfoComunicadoAlfa> listaComunicados = new List<InfoComunicadoAlfa>();

            for (int i = 1; i < xmlInfo.Count(); i++)
            {
                // Cada línea trae la información de un comunicado
                XmlDocument xmlResult = new XmlDocument();
                string info = xmlInfo[i];
                // Si es el último, trae un </Root chambón, hay que quitarlo
                if (i == xmlInfo.Count() - 1) info = info.Replace("</Root>", string.Empty);
                xmlResult.LoadXml(info);

                string nit = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "NUI");
                string numeroDocumento = ResultadosWebServicesAlfaHelper.GetAttributeValue(xmlResult, "Documento", "Nro");
                string codigoTramite = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "Expediente");
                int tipoDocumento = int.Parse(ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "Tipo_Documento"));
                string detalleDocumento = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "Detalle_Documento");

                InfoComunicadoAlfa infoComunicado = new InfoComunicadoAlfa(nit, numeroDocumento, codigoTramite, tipoDocumento, detalleDocumento);
                listaComunicados.Add(infoComunicado);
            }


            return new ResultadoComunicadosXTramiteAlfa(false, null, listaComunicados);

        }

        /// <summary>
        /// La primera l'inea es un primer pedazo de un XML, que en algunos casos trae un mensaje de error
        /// </summary>
        private static string LimpiarPrimerElementoComunicadosXTramiteAlfa(string texto)
        {
            // La primera línea trae un <Root> chambón, hay que quitarlo
            string info = texto.Replace("< Root >", string.Empty);
            info = info.Replace("</Root>", string.Empty);
            // Cuando no trae el mensaje de error, viene solo el tag de la versi'on de xml y eso no lo entiende el XmlDocument
            // por esta raz'on se borra esta parte y se revisa si el string queda vac'io
            int indiceInicioTag = info.IndexOf("<?");
            if (indiceInicioTag >= 0)
            {
                int indiceFinTag = info.IndexOf(">");
                if (indiceFinTag > 0)
                {
                    info = info.Remove(indiceInicioTag, indiceFinTag + 1);
                }
                else
                {
                    throw new ApplicationException("El primer elemento de la lista no est'a bien formado");
                }

            }
            return info;
        }

        private bool _ocurrioError;

        public bool OcurrioError
        {
            get { return _ocurrioError; }
            set { _ocurrioError = value; }
        }

        private string _mensajeError;

        public string MensajeError
        {
            get { return _mensajeError; }
        }

        private List<InfoComunicadoAlfa> _comunicados;

        public List<InfoComunicadoAlfa> Comunicados
        {
            get { return _comunicados; }
        }
    }

    /// <summary>
    /// Esta clase contiene la información relacionada a un comunicado que retorna Alfa
    /// </summary>
    public class InfoComunicadoAlfa
    {
        private string _nit;

        public string Nit
        {
            get { return _nit; }
        }

        private string _numeroDocumento;

        public string NumeroDocumento
        {
            get { return _numeroDocumento; }
        }

        private string _codigoTramite;

        public string CodigoTramite
        {
            get { return _codigoTramite; }
        }

        private int _tipoDocumento;

        public TipoDocumentoAlfa TipoDocumento
        {
            get
            {
                switch (_tipoDocumento)
                {
                    case 1:
                        return TipoDocumentoAlfa.RadicadoEntrada;
                    case 2:
                        return TipoDocumentoAlfa.RegistroSalida;
                    default:
                        throw new NotSupportedException("Tipo de documento Alfa no reconocido");
                }
            }
        }

        public string DescripcionTipoDocumento
        {
            get
            {
                switch (TipoDocumento)
                {
                    case TipoDocumentoAlfa.RadicadoEntrada:
                        return "Radicado de Entrada";
                    case TipoDocumentoAlfa.RegistroSalida:
                        return "Registro de Salida";
                    default:
                        throw new NotSupportedException("Tipo de documento Alfa no reconocido");
                }
            }
        }

        private string _detalleDocumento;

        public string DetalleDocumento
        {
            get { return _detalleDocumento; }
        }

        private bool? _clienteRegistrado;

        /// <summary>
        /// Indica si el cliente está registrado en el sistema
        /// </summary>
        public bool? ClienteRegistrado
        {
            get { return _clienteRegistrado; }
            set { _clienteRegistrado = value; }
        }

        public InfoComunicadoAlfa(string nit, string numeroDocumento, string codigoTramite, int tipoDocumento, string detalleDocumento)
        {
            _nit = nit;
            _numeroDocumento = numeroDocumento;
            _codigoTramite = codigoTramite;
            _tipoDocumento = tipoDocumento;
            _detalleDocumento = detalleDocumento;
        }

    }

    /// <summary>
    /// Tipos de documentos que maneja Alfa
    /// </summary>
    public enum TipoDocumentoAlfa
    {
        RadicadoEntrada,
        RegistroSalida
    }
}