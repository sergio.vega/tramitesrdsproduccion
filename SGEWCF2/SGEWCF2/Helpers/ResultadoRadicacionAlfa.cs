﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace SGEWCF2.Helpers
{
    public class ResultadoRadicacionAlfa
    {
        public ResultadoRadicacionAlfa(string codigoRadicado, string codigoTramiteAlfa, int codigoError, string mensajeError)
        {
            _codigoRadicado = codigoRadicado;
            _codigoTramiteAlfa = codigoTramiteAlfa;
            _codigoError = codigoError;
            _mensajeError = mensajeError;
            _ocurrioErrorAlEnviarArchivos = false;
            _erroresEnvioArchivos = new List<string>();
        }

        /// <summary>
        /// Permite crear la clase que contiene los datos de un resultado de la radicación en AlfaNet
        /// </summary>
        /// <param name="xmlinfo"></param>
        /// <returns></returns>
        public static ResultadoRadicacionAlfa CrearResultadoRadicacionAlfados(string xmlinfo)
        {
            XmlDocument xmlResult = new XmlDocument();
            xmlResult.LoadXml(xmlinfo);

            string codigoRadicado = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "RadicadoCodigo");
            string codigoSubExpediente = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "ExpedienteCodigo");
            int codigoError = int.Parse(ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "CodigoError"));
            string mensajeError = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "MensajeError");

            return new ResultadoRadicacionAlfa(codigoRadicado, codigoSubExpediente, codigoError, mensajeError);
        }

        public static ResultadoRadicacionAlfa CrearResultadoRadicacionAlfa(string xmlinfo)
        {
            XmlDocument xmlResult = new XmlDocument();

            string codigoRadicado = DateTime.Now.ToString("hhmmss");
            string codigoSubExpediente = DateTime.Now.ToString("hhmmss");
            int codigoError = 0;
            string mensajeError = ResultadosWebServicesAlfaHelper.GetTagValue(xmlResult, "vacio");

            return new ResultadoRadicacionAlfa(codigoRadicado, codigoSubExpediente, codigoError, mensajeError);
        }

        private string _codigoRadicado;
        public string CodigoRadicado
        {
            get { return _codigoRadicado; }
        }

        private string _codigoTramiteAlfa;

        public string CodigoTramiteAlfa
        {
            get { return _codigoTramiteAlfa; }
        }

        private int _codigoError;

        public int CodigoError
        {
            get { return _codigoError; }
        }

        private string _mensajeError;

        public string MensajeError
        {
            get { return _mensajeError; }
        }

        private bool _ocurrioErrorAlEnviarArchivos;

        public bool OcurrioErrorAlEnviarArchivos
        {
            get { return _ocurrioErrorAlEnviarArchivos; }
        }

        private List<string> _erroresEnvioArchivos;

        /// <summary>
        /// Retorna una lista readOnly con los mensajes de error al enviar cada archivo que ha fallado
        /// </summary>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> ErroresEnvioArchivos
        {
            get { return _erroresEnvioArchivos.AsReadOnly(); }
        }

        /// <summary>
        /// Permite agregar un error a la lista de errores ocurridos al intentar enviar archivos
        /// </summary>
        public void AddErrorAlEnviarArchivo(string mensajeError)
        {
            _erroresEnvioArchivos.Add(mensajeError);
        }
    }

    public static class ResultadosWebServicesAlfaHelper
    {
        /// <summary>
        /// Retorna el valor de un tag, suponiendo que solo existe uno con ese nombre en el xml
        /// </summary>
        public static string GetTagValue(XmlDocument xmlResult, string tagName)
        {
            XmlNodeList nodes = xmlResult.GetElementsByTagName(tagName);
            System.Diagnostics.Debug.Assert(nodes.Count <= 1, "Se espera solo un nodo con este tag");
            if (nodes.Count == 0) return null;
            else return nodes[0].InnerText;
        }

        /// <summary>
        /// Retorna una característica de un tag, suponiendo que solo existe uno con ese nombre en el xml
        /// </summary>
        public static string GetAttributeValue(XmlDocument xmlResult, string tagName, string attribute)
        {
            XmlNodeList nodes = xmlResult.GetElementsByTagName(tagName);
            System.Diagnostics.Debug.Assert(nodes.Count == 1, "Se espera solo un nodo con este tag");
            XmlNode nodo = nodes[0].Attributes.GetNamedItem(attribute);
            return nodo.InnerText;
        }
    }
}