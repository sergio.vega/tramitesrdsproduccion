﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SGEWCF2.Helpers
{
    public class RDSDynamicObservacion
    {
        private ST_Helper Helper { get; set; }
        private dynamic DynamicObservacion;
        private ST_RDSObservacion Observacion;

        public Dictionary<string, string> TXT { get; set; }

        public RDSDynamicObservacion(ref ST_RDSObservacion Observacion, ref ST_Helper Helper)
        {
            this.Helper = Helper;

            AddKey("AHORA", DateTime.Now.ToString("dd/MM/yyyy"));
            AddKey("AHORA_DET", DateTime.Now.ToString("ddMMyyHHmmss"));

            var Serializer = new JavaScriptSerializer_TESExtension();
            var json = Serializer.Serialize(Observacion);
            this.Observacion = Observacion;
            this.DynamicObservacion = Serializer.Deserialize<dynamic>(json);
        }

        public RDSDynamicObservacion()
        {
            this.TXT = new Dictionary<string, string>();
        }

        public ST_RDSObservacion GetObservacion()
        {
            return Observacion;
        }

        public void AddKey(string key, string value)
        {
            if (Helper.TXT.ContainsKey(key))
                Helper.TXT[key] = value;
            else
                Helper.TXT.Add(key, value);
        }

        public void AddKey2(string key, string value)
        {
            if (TXT.ContainsKey(key))
                TXT[key] = value;
            else
                TXT.Add(key, value);
        }

        public string GetKey(string key)
        {
            if (TXT.ContainsKey(key))
                return TXT[key];
            else
                return key;
        }

        public string GetKeyTextValue(string key)
        {
            if (Helper.TXT.ContainsKey(key))
                return Helper.TXT[key];
            else
                return "";
        }

        public byte[] GetKeyImageValue(string key)
        {
            if (Helper.IMG.ContainsKey(key))
                return Helper.IMG[key];
            else
                return null;
        }

        public bool StringKeyExists(string key)
        {
            return Helper.TXT.ContainsKey(key);
        }

        public bool ImageKeyExists(string key)
        {
            return Helper.IMG.ContainsKey(key);
        }

        public string Format(string key, int Contador = 0)
        {
            return FormatString(GetKeyTextValue(key));
        }

        public string FormatString(string Cadena, int Contador = 0)
        {

            List<ST_String> lstString = GetStringParameters(Cadena);
            string CadResult = "";

            foreach (var Item in lstString)
            {
                if (Item.Evaluate)
                    CadResult += GetStringValue(Item.Cadena);
                else
                    CadResult += Item.Cadena;
            }

            lstString = GetStringParameters(CadResult);

            if (lstString.Any(bp => bp.Evaluate) && Contador < 5)
            {
                return FormatString(CadResult, ++Contador);
            }

            return CadResult;
        }

        public string GetStringValue(string key)
        {
            try
            {
                if (Helper.TXT.ContainsKey(key))
                    return Helper.TXT[key];

                key = key.Replace("[", "").Replace("]", "");

                var sParams = key.Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries);

                var oRet = DynamicObservacion;
                int nParam = 0;

                foreach (var Item in sParams)
                {

                    if (int.TryParse(Item, out nParam))
                        oRet = oRet[nParam];
                    else
                        oRet = oRet[Item];

                }
                return oRet.ToString();
            }
            catch
            {
                return "NA";
            }
        }

        private string GetHeader(string Cadena)
        {
            try
            {
                string sResult = Cadena.Split(new string[] { "<HEADER>", "</HEADER>" }, StringSplitOptions.None)[1];
                return sResult;
            }
            catch
            {
                return "";
            }
        }

        public string GetBody(string key)
        {
            try
            {
                string Cadena = GetStringValue(key);
                string sResult = Cadena.Split(new string[] { "<BODY>", "\n</BODY>", "</BODY>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        private string GetFooter(string Cadena)
        {
            try
            {
                string sResult = Cadena.Split(new string[] { "<FOOTER>", "</FOOTER>" }, StringSplitOptions.None)[1];
                return sResult;
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderLogo(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<LOGO>", "</LOGO>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderFile(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<FILE>", "</FILE>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderDetail(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<DETAIL>", "</DETAIL>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderSubject(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<SUBJECT>", "</SUBJECT>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderAddress(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<ADDRESS>", "</ADDRESS>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderEvent(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<EVENT>", "</EVENT>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderIcon(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<ICON>", "</ICON>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetHeaderDesc(string key)
        {
            try
            {
                string Cadena = GetHeader(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<DESC>", "</DESC>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetFooterText(string key)
        {
            try
            {
                string Cadena = GetFooter(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<TEXT>", "</TEXT>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public string GetFooterSign(string key)
        {
            try
            {
                string Cadena = GetFooter(GetStringValue(key));
                string sResult = Cadena.Split(new string[] { "<SIGN>", "</SIGN>" }, StringSplitOptions.None)[1];
                return FormatString(sResult);
            }
            catch
            {
                return "";
            }
        }

        public List<ST_Paragraph> GetParagraphs(string Cadena)
        {

            List<ST_Paragraph> lstString = new List<ST_Paragraph>();
            //Ojo importa el orden de; arreglo de strings
            var Items = Cadena.Split(new string[] { "\n<P ", "<P ", "\n<P", "<P", "\n</P>", "</P>" }, StringSplitOptions.None);


            int Contador = 1;
            foreach (var Item in Items)
            {
                bool isParagraph = (Contador++ % 2 == 0);
                if (isParagraph)
                {
                    var Aligns = Item.Split(new string[] { ">\n", ">" }, 2, StringSplitOptions.None);
                    lstString.Add(new ST_Paragraph(Aligns[1], isParagraph, Aligns[0]));
                }
                else
                {
                    lstString.Add(new ST_Paragraph(Item, isParagraph, ""));
                }

            }
            return lstString;
        }

        public List<ST_String> GetStringParameters(string Cadena)
        {
            List<ST_String> lstString = new List<ST_String>();

            var Item0 = Cadena.Split(new string[] { "{", "}" }, StringSplitOptions.None);

            int ContadorParam = 1;
            foreach (var Item in Item0)
            {
                lstString.Add(new ST_String(Item, (ContadorParam++ % 2 == 0), false, false));
            }

            return lstString;
        }

        public List<ST_List> GetListas(string Cadena)
        {
            List<ST_List> lstString = new List<ST_List>();

            var Item0 = Cadena.Split(new string[] { "<LIST>", "</LIST>" }, StringSplitOptions.None);

            int ContadorParam = 1;
            foreach (var Item in Item0)
            {
                lstString.Add(new ST_List(Item, (ContadorParam++ % 2 == 0)));
            }

            return lstString;
        }

        public List<ST_String> GetStringBold(string Cadena)
        {
            List<ST_String> lstString = new List<ST_String>();

            var Items = Cadena.Split(new string[] { "<B>", "</B>" }, StringSplitOptions.None);

            int ContadorBold = 1;
            foreach (var ItemBold in Items)
            {
                var lstAnchor = GetStringAnchors(ItemBold);
                foreach (var ItemAnchor in lstAnchor)
                {
                    lstString.Add(new ST_String(ItemAnchor.Cadena, false, (ContadorBold % 2 == 0), ItemAnchor.isAnchor));
                }
                ContadorBold++;
            }

            return lstString;
        }

        public List<ST_Anchor> GetStringAnchors(string Cadena)
        {
            List<ST_Anchor> lstAnchors = new List<ST_Anchor>();

            var Items = Cadena.Split(new string[] { "<A>", "</A>" }, StringSplitOptions.None);

            int ContadorAchor = 1;
            foreach (var Item in Items)
            {
                lstAnchors.Add(new ST_Anchor(Item, (ContadorAchor++ % 2 == 0)));
            }

            return lstAnchors;
        }

        public string ProcesarObservacionesListas(string CadOrigin)
        {
            try
            {
                string CadBlock = "";
                string CadResult = "";
                string CadLista = "";
                string CadListaName = "";
                string CadAux = "";

                int LIni0, LIni1, LEnd0, B00, B01, B10, B11;
                int ItemsCount = 0;

                LIni0 = CadOrigin.IndexOf("{{Lista", StringComparison.CurrentCultureIgnoreCase);
                if (LIni0 == -1)
                    return CadOrigin;

                LIni1 = CadOrigin.IndexOf("}}", LIni0, StringComparison.CurrentCultureIgnoreCase);
                if (LIni1 == -1 || LIni0 > LIni1)
                    return CadOrigin;

                CadLista = CadOrigin.Substring(LIni0, LIni1 - LIni0 + 2);
                CadListaName = CadLista.Substring(8, CadLista.Length - 10);



                LEnd0 = CadOrigin.IndexOf("{{/Lista}}", LIni1, StringComparison.CurrentCultureIgnoreCase);

                if (LEnd0 < LIni0)
                    return CadOrigin;


                if (!Int32.TryParse(GetKey(CadLista), out ItemsCount))
                    return CadOrigin;

                B00 = CadOrigin.LastIndexOf("\n", LIni0);
                B01 = CadOrigin.IndexOf("\n", LIni0);
                B10 = CadOrigin.LastIndexOf("\n", LEnd0);
                B11 = CadOrigin.IndexOf("\n", LEnd0);

                CadBlock = CadOrigin.Substring(B01, B10 - B01);
                CadBlock = PartesHtml(CadBlock);
                CadResult = CadOrigin.Substring(0, B00);

                if (CadBlock.Contains("<tr>"))
                {
                    CadAux = CadResult.Substring(CadResult.LastIndexOf("<tr>"), CadResult.Length - CadResult.LastIndexOf("<tr>"));
                    B00 = B00 - CadAux.Length;
                    CadResult = CadOrigin.Substring(0, B00);
                }

                for (int i = 0; i < ItemsCount; i++)
                {
                    CadResult += ProcesarObservacionesListasBlock(CadBlock, CadListaName, i);
                }
                CadResult += CadOrigin.Substring(B11);


                return ProcesarObservacionesListas(CadResult);
            }
            catch
            {
                return CadOrigin;
            }
        }

        private string ProcesarObservacionesListasBlock(string CadOrigin, string CadListaName, int Indice)
        {
            try
            {
                string CadResult = "";
                var Items = CadOrigin.Split(new string[] { "{{", "}}" }, StringSplitOptions.None);

                int Contador = 1;
                foreach (var Item in Items)
                {
                    bool isParametro = (Contador++ % 2 == 0);
                    if (isParametro)
                    {
                        CadResult += "{{" + Item + "}}[" + Indice + "]";
                    }
                    else
                    {
                        CadResult += Item;
                    }
                }
                return CadResult;
            }
            catch
            {
                return CadOrigin;
            }
        }

        private string PartesHtml(string html)
        {
            bool regilla = html.Contains("<tr>");
            if (regilla)
            {
                html = html.Replace("<tr>", "<fila><tr>");
                string[] separador = { "<fila>" };
                string[] partes = html.Split(separador, System.StringSplitOptions.RemoveEmptyEntries);
                html = "";
                var posicionfila = partes.Length - 2;
                for (int i = 0; i < posicionfila; i++)
                {
                    html = html + partes[i];
                }
                string fila = partes[posicionfila];
                return fila;
            }
            return html;
        }
    }
}