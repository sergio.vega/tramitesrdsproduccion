﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGEWCF2.Helpers
{
    public class RDSDynamicResolucion
    {
        public Dictionary<string, string> TXT { get; set; }

        public RDSDynamicResolucion()
        {
            this.TXT = new Dictionary<string, string>();
        }

        public void AddKey(string key, string value)
        {
            if (TXT.ContainsKey(key))
                TXT[key] = value;
            else
                TXT.Add(key, value);
        }

        public string GetKey(string key)
        {
            if (TXT.ContainsKey(key))
                return TXT[key];
            else
                return key;
        }

        public string ProcesarResolucionesListas(string CadOrigin)
        {
            try
            {
                string CadBlock = "";
                string CadResult = "";
                string CadLista = "";
                string CadListaName = "";

                int LIni0, LIni1, LEnd0, B00, B01, B10, B11;
                int ItemsCount = 0;

                LIni0 = CadOrigin.IndexOf("{{LISTA", StringComparison.CurrentCultureIgnoreCase);
                if (LIni0 == -1)
                    return CadOrigin;

                LIni1 = CadOrigin.IndexOf("}}", LIni0, StringComparison.CurrentCultureIgnoreCase);
                if (LIni1 == -1 || LIni0 > LIni1)
                    return CadOrigin;

                CadLista = CadOrigin.Substring(LIni0, LIni1 - LIni0 + 2);
                CadListaName = CadLista.Substring(8, CadLista.Length - 10);


                LEnd0 = CadOrigin.IndexOf("{{/LISTA}}", LIni1, StringComparison.CurrentCultureIgnoreCase);

                if (LEnd0 < LIni0)
                    return CadOrigin;


                if (!Int32.TryParse(GetKey(CadLista), out ItemsCount))
                    return CadOrigin;

                B00 = CadOrigin.LastIndexOf("\n", LIni0);
                B01 = CadOrigin.IndexOf("\n", LIni0);
                B10 = CadOrigin.LastIndexOf("\n", LEnd0);
                B11 = CadOrigin.IndexOf("\n", LEnd0);

                CadBlock = CadOrigin.Substring(B01, B10 - B01);
                CadResult = CadOrigin.Substring(0, B00);
                for (int i = 0; i < ItemsCount; i++)
                {
                    CadResult += ProcesarResolucionesListasBlock(CadBlock, CadListaName, i);
                }
                CadResult += CadOrigin.Substring(B11);


                return ProcesarResolucionesListas(CadResult);
            }
            catch
            {
                return CadOrigin;
            }
        }

        private string ProcesarResolucionesListasBlock(string CadOrigin, string CadListaName, int Indice)
        {
            try
            {
                string CadResult = "";
                var Items = CadOrigin.Split(new string[] { "{{", "}}" }, StringSplitOptions.None);

                int Contador = 1;
                foreach (var Item in Items)
                {
                    bool isParametro = (Contador++ % 2 == 0);
                    if (isParametro)
                    {
                        if (Item.StartsWith(CadListaName))
                            CadResult += "{{" + Item + "}}[" + Indice + "]";
                        else
                            CadResult += "{{" + Item + "}}";
                    }
                    else
                    {
                        CadResult += Item;
                    }
                }
                return CadResult;
            }
            catch
            {
                return CadOrigin;
            }
        }

    }
}