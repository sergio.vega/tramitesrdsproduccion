﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SGEWCF2.Helpers
{
    public class RDSComunicadosPDF
    {
        #region variables

        protected Font fontNormal;
        protected Font fontBoldLink;
        protected Font fontNormalLink;
        private Font fontBold;
        private static int _margenInferior;
        private ST_RDSSolicitud Solicitud;
        private RDSDynamicSolicitud DynamicSolicitud;
        
        #endregion

        public RDSComunicadosPDF(ref RDSDynamicSolicitud DynamicSolicitud)
        {
            fontNormal = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL);
            fontBoldLink = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.BLUE);
            fontNormalLink = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.NORMAL, BaseColor.BLUE);
            fontBold = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD);
            _margenInferior = 100;
            
            this.DynamicSolicitud = DynamicSolicitud;
            this.Solicitud = DynamicSolicitud.GetSolicitud();
        }

        public MemoryStream GenerarComunicado(ref ST_RDSComunicado Comunicado, bool Signed, string registroNumber = "######")
        {
            MemoryStream mem=new MemoryStream();

            DynamicSolicitud.AddKey("REGISTRO", registroNumber);

            switch ((COM_CLASS)Comunicado.COM_CLASS)
            {
                case COM_CLASS.Administrativo:
                    if (Solicitud.Financiero.FIN_SEVEN_ALDIA == false || Solicitud.Anexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Rechazado))
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR", COM_CLASS.Administrativo, Signed, "Comunicado Administrativo", "Rechazo");
                        Comunicado.COM_NAME = "Comunicado Administrativo Rechazo.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Rechazado;
                    }
                    else if (Solicitud.Anexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_REQUERIR", COM_CLASS.Administrativo, Signed, "Comunicado Administrativo", "Requerido");
                        Comunicado.COM_NAME = "Comunicado Administrativo Requerido.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Requerido;
                    }
                    else
                    {
                        if (Solicitud.SOL_TYPE_CLASS == (int)SOL_TYPES_CLASS.AdministrativoTecnico)
                            mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_ANE_ADMIN_APROBAR", COM_CLASS.Administrativo, Signed, "Comunicado Administrativo", "Aprobado");
                        else
                            mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR", COM_CLASS.Administrativo, Signed, "Comunicado Administrativo", "Aprobado");
                        Comunicado.COM_NAME = "Comunicado Administrativo Aprobado.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Aprobado;
                    }
                    break;

                case COM_CLASS.Revision:
                    mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_ANE_TECH_DEVOLVER", COM_CLASS.Revision, Signed, "Comunicado Técnico", "Devuelto");
                    Comunicado.COM_NAME = "Comunicado Técnico Devuelto.pdf";
                    Comunicado.COM_TYPE = (int)COM_TYPE.Devuelto;
                    break;

                case COM_CLASS.Tecnico:
                    switch ((ROLE1_STATE)Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID)
                    {
                        case ROLE1_STATE.Aprobado:
                            mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", COM_CLASS.Tecnico, Signed, "Comunicado Técnico", "Aprobado");
                            Comunicado.COM_NAME = "Comunicado Técnico Aprobado.pdf";
                            Comunicado.COM_TYPE = (int)COM_TYPE.Aprobado;
                            break;
                        case ROLE1_STATE.Rechazado:
                            mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", COM_CLASS.Tecnico, Signed, "Comunicado Técnico", "Rechazado");
                            Comunicado.COM_NAME = "Comunicado Técnico Rechazado.pdf";
                            Comunicado.COM_TYPE = (int)COM_TYPE.Rechazado;
                            break;
                        case ROLE1_STATE.Requerido:
                            mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR", COM_CLASS.Tecnico, Signed, "Comunicado Técnico", "Requerido");
                            Comunicado.COM_NAME = "Comunicado Técnico Requerido.pdf";
                            Comunicado.COM_TYPE = (int)COM_TYPE.Rechazado;
                            break;
                    }
                    if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Rechazado))
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Rechazo");
                        Comunicado.COM_NAME = "Comunicado Tecnico Rechazo.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Rechazado;
                    }
                    else if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                    {
                        if (Solicitud.SOL_IS_OTORGA)
                            mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR_OTORGA", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Requerido");
                        else
                            mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Requerido");
                        Comunicado.COM_NAME = "Comunicado Tecnico Requerido.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Requerido;
                     }
                    else
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Aprobado");
                        Comunicado.COM_NAME = "Comunicado Tecnico Aprobado.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Aprobado;
                    }
                    break;

                case COM_CLASS.Desistimiento:
                    mem = GenerarComunicadoGenerico("ALFA_REG_MINTIC_TO_CONCESIONARIO_DESISTIMIENTO", COM_CLASS.Revision, Signed, "Comunicado Desistimiento", "Desistimiento");
                    Comunicado.COM_NAME = "Comunicado Desistimiento.pdf";
                    Comunicado.COM_TYPE = (int)COM_TYPE.Desistimiento;
                    break;

                case COM_CLASS.Viabilidad:
                    if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Rechazado))
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Rechazo");
                        Comunicado.COM_NAME = "Comunicado Tecnico Rechazo.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Rechazado;
                    }
                    else if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Requerido");
                        Comunicado.COM_NAME = "Comunicado Tecnico Requerido.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Requerido;
                    }
                    else
                    {
                        mem = GenerarComunicadoGenerico("ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", COM_CLASS.Viabilidad, Signed, "Comunicado Tecnico", "Aprobado");                        
                        Comunicado.COM_NAME = "Comunicado Tecnico Aprobado.pdf";
                        Comunicado.COM_TYPE = (int)COM_TYPE.Aprobado;
                    }
                    break;
            }
            return mem;
        }

        private void CrearEncabezados(ref Document documento, string Titulo, string Asunto)
        {
            documento.AddTitle(Titulo);
            documento.AddSubject(Asunto);
            documento.AddKeywords("Comunicado SGE");
            documento.AddCreator("Front Office SGE MinTIC");
            documento.AddAuthor("Tes America Andina S.A.S");
        }

        private MemoryStream GenerarComunicadoGenerico(string NombreDocumento, COM_CLASS Com_Class, bool Signed, string Titulo, string Asunto)
        {
            Document documento = new Document(PageSize.LETTER, 50, 50, 40, 50);
            MemoryStream mem = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(documento, mem);

            try
            {

                CrearEncabezados(ref documento, Titulo, Asunto);

                string sBody = DynamicSolicitud.GetBody(NombreDocumento);
                writer.PageEvent = new PDFFooter(DynamicSolicitud.GetFooterText(NombreDocumento));

                documento.Open();

                AddLogo(documento, DynamicSolicitud.GetHeaderLogo(NombreDocumento));

                documento.Add(new Paragraph("\n"));

                var Parrafos = DynamicSolicitud.GetParagraphs(sBody);
                foreach (var Parrafo in Parrafos)
                {
                    AddParrafo(ref documento, Parrafo);
                }

                switch(Com_Class){
                    case COM_CLASS.Administrativo:
                    case COM_CLASS.Revision:
                    case COM_CLASS.Desistimiento:                    
                        AddFirma(documento, Solicitud.USR_ADMIN.USR_ROLE8, DynamicSolicitud.GetFooterSign(NombreDocumento), Signed);
                        break;
                    case COM_CLASS.Tecnico:
                    case COM_CLASS.Viabilidad:
                        AddFirma(documento, Solicitud.USR_TECH.USR_ROLE8, DynamicSolicitud.GetFooterSign(NombreDocumento), Signed);
                        break;

                }
            }
            catch (DocumentException)
            {

            }
            catch (IOException)
            {

            }
            finally
            {
                writer.CloseStream = false;
                documento.Close();
            }
            return mem;
        }

        private void AddFirma(Document documento, ST_RDSUsuario USR_ROLE8, string sTitle, bool Signed)
        {
            PdfPTable tablaFirma = new PdfPTable(1);
            tablaFirma.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tablaFirma.DefaultCell.BorderWidth = 0;
            tablaFirma.DefaultCell.Padding = 2;
            tablaFirma.DefaultCell.UseVariableBorders = false;
            tablaFirma.KeepTogether = true;

            PdfPCell cell = new PdfPCell(tablaFirma);
            cell.BorderWidth = 0;

            Paragraph parrafoEspacio = new Paragraph(new Phrase("\n", fontBold));
            parrafoEspacio.Alignment = Rectangle.ALIGN_CENTER;
            cell.AddElement(parrafoEspacio);

            if (Signed && USR_ROLE8.USR_SIGN!=null)
            {
                Image pngFirma = Image.GetInstance(USR_ROLE8.USR_SIGN);
                pngFirma.ScalePercent((100.0f / pngFirma.Height)*50.0f);
                pngFirma.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                cell.AddElement(pngFirma);
            }
            else
            {
                documento.Add(new Paragraph("\n\n\n"));
            }

            Paragraph parrafoNombre = new Paragraph(new Phrase(USR_ROLE8.USR_NAME.ToUpper(), fontBold));
            parrafoNombre.Alignment = Rectangle.ALIGN_CENTER;
            cell.AddElement(parrafoNombre);
            Paragraph parrafoCargo = new Paragraph(new Phrase(sTitle, fontNormal));
            parrafoCargo.Alignment = Rectangle.ALIGN_CENTER;
            cell.AddElement(parrafoCargo);
            parrafoCargo.Alignment = Rectangle.ALIGN_LEFT;
            tablaFirma.AddCell(cell);

            documento.Add(tablaFirma);
        }

        private void AddLogo(Document documento, string sLOGO)
        {
            if (string.IsNullOrEmpty(sLOGO) || !DynamicSolicitud.ImageKeyExists(sLOGO))
                return;


            PdfPTable tabla = new PdfPTable(3);
            tabla.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            tabla.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            tabla.DefaultCell.BorderWidth = 1;
            tabla.DefaultCell.Padding = 2;
            tabla.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_CENTER;
            tabla.DefaultCell.UseVariableBorders = false;
            tabla.WidthPercentage = 100;
            tabla.SetWidths(new int[] { 40, 20, 40 });

            Image pngLogoLargo = Image.GetInstance(DynamicSolicitud.GetKeyImageValue(sLOGO));
            //pngLogoLargo.ScalePercent(24f); //No funciona porque cuando se adiciona a la celda se le da fit=true

            PdfPCell cell = new PdfPCell(new Phrase(" "));
            cell.UseVariableBorders = true;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            cell = new PdfPCell(new Phrase(" "));
            cell.UseVariableBorders = true;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            cell = new PdfPCell(pngLogoLargo, true);
            cell.UseVariableBorders = true;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.BorderColor = BaseColor.WHITE;
            tabla.AddCell(cell);

            documento.Add(tabla);
        }

        private void AddParrafo(ref Document documento, ST_Paragraph ST_Parrafo)
        {
            Paragraph Parrafo = new Paragraph(new Phrase("", fontBold));
         
            //string CadParameters = DynamicSolicitud.FormatString(ST_Parrafo.Cadena);

            List<ST_List> Listas = DynamicSolicitud.GetListas(ST_Parrafo.Cadena);
            string CadListas = "";

            foreach (var Lista in Listas)
            {
                if (Lista.isList)
                {
                    switch (Lista.Cadena)
                    {
                        case "AdminAprobar":
                            foreach (var Item in Solicitud.Anexos)
                                CadListas += Item.ANX_TYPE_NAME + ".\n";
                            break;
                        case "AdminRequerir":
                            foreach (var Item in Solicitud.Anexos.Where(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                                CadListas += Item.ANX_TYPE_NAME + ". " + Item.ANX_ROLE1_COMMENT + "\n";
                            break;
                        case "AdminRechazar":
                            if (Solicitud.Financiero.FIN_SEVEN_ALDIA == false)
                                CadListas += "El concesionario NO se encuentra al día con cartera.\n";
                            foreach (var Item in Solicitud.Anexos.Where(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Rechazado))
                                CadListas += Item.ANX_TYPE_NAME + ". " + Item.ANX_ROLE1_COMMENT + "\n";
                            break;
                        case "TechRequerir":
                            foreach (var Item in Solicitud.TechAnexos.Where(bp => bp.ANX_ROLE1_STATE_ID == (int)ROLE1_STATE.Requerido))
                                CadListas += Item.ANX_TYPE_NAME + ". " + Item.ANX_ROLE1_COMMENT + "\n";
                            break;
                    }
                }
                else
                {
                    CadListas += Lista.Cadena;
                }
            }

            List<ST_String> lstString = DynamicSolicitud.GetStringBold(CadListas);

            foreach (var Item in lstString)
            {
                if (Item.Bold)
                    if(Item.Anchor)
                        Parrafo.Add(new Phrase(Item.Cadena, fontBoldLink));
                    else
                        Parrafo.Add(new Phrase(Item.Cadena, fontBold));
                else
                    if (Item.Anchor)
                        Parrafo.Add(new Phrase(Item.Cadena, fontNormalLink));
                    else
                        Parrafo.Add(new Phrase(Item.Cadena, fontNormal));
            }

            switch (ST_Parrafo.Align)
            {
                case "LEFT":
                    Parrafo.Alignment = Rectangle.ALIGN_LEFT;
                    break;
                case "RIGHT":
                    Parrafo.Alignment = Rectangle.ALIGN_RIGHT;
                    break;
                case "JUSTIFIED":
                    Parrafo.Alignment = Rectangle.ALIGN_JUSTIFIED;
                    break;
                case "CENTER":
                    Parrafo.Alignment = Rectangle.ALIGN_CENTER;
                    break;
            }

            documento.Add(Parrafo);
        }

        public class PDFFooter : PdfPageEventHelper
        {
            private string PDF_FOOTER = "";

            public PDFFooter(string PDF_FOOTER)
            {
                this.PDF_FOOTER = PDF_FOOTER;
            }

            // write on top of document
            public override void OnOpenDocument(PdfWriter writer, Document document)
            {
                base.OnOpenDocument(writer, document);
            }

            // write on start of each page
            public override void OnStartPage(PdfWriter writer, Document document)
            {
                base.OnStartPage(writer, document);
            }

            // write on end of each page
            public override void OnEndPage(PdfWriter writer, Document document)
            {
                // Se crea una tabla de una sola columna para escibir la información en texto del footer
                base.OnEndPage(writer, document);

                if (string.IsNullOrEmpty(PDF_FOOTER))
                    return;

                PdfPTable tablaFooter = new PdfPTable(1);
                tablaFooter.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                tablaFooter.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                tablaFooter.DefaultCell.BorderWidth = 0;
                tablaFooter.DefaultCell.Padding = 2;
                tablaFooter.HorizontalAlignment = iTextSharp.text.Rectangle.ALIGN_LEFT;
                tablaFooter.DefaultCell.UseVariableBorders = false;
                tablaFooter.TotalWidth = 300;

                // Se escribe la información de texto del footer
                Font font = FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL);

                Phrase phrase = new Phrase(PDF_FOOTER, font);
                tablaFooter.AddCell(phrase);
                tablaFooter.WriteSelectedRows(0, -1, document.LeftMargin, _margenInferior - 15, writer.DirectContent);
            }

            //write on close of document
            public override void OnCloseDocument(PdfWriter writer, Document document)
            {
                base.OnCloseDocument(writer, document);

            }
        }

    }
}