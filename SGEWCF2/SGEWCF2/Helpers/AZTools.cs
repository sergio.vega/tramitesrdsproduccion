﻿using SGEWCF2.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace SGEWCF2
{
    public class AZTools
    {


        #region Radicar

        private static AZRadicar.RespuestaOperacion AZRadicarTramiteRetry(ref AZRadicar.sgdaPortTypeClient ws,
            string Tramite,
            string Tipo,
            string Modalidad,
            string Dependencia,
            string Usuario,
            string Sitio,
            string MINTIC_NAME,
            string Mintic_NIT,
            string MINTIC_ADDRESS,
            string MINTIC_PAIS,
            string MINTIC_DEPARTAMENTO,
            string MINTIC_CIUDAD,
            string NombrePRST,
            string NitPRST,
            string PaisPRST,
            string DepartamentoPRST,
            string CiudadPRST,
            string NombreRepresentante,
            string IdentificacionRepresentante,
            string TelefonoRepresentante,
            string MailRepresentante,
            string CargoRepresentante,
            string Asunto,
            string Observaciones,
            string Estado,
            string NumRadicado,
            string IdReferencia,
            bool esRadicado = false)
        {
            int Ciudad = int.Parse(CiudadPRST);
            //AZRadicar.RespuestaOperacion wsResult = new AZRadicar.RespuestaOperacion();
            AZRadicar.RespuestaOperacion wsResult = null;
            var tries = 3;

            AZRadicar.PersonaTypeDireccion DireccionMintic = new AZRadicar.PersonaTypeDireccion();
            DireccionMintic.Pais = MINTIC_PAIS;
            DireccionMintic.Estado = MINTIC_DEPARTAMENTO;
            DireccionMintic.Ciudad = MINTIC_CIUDAD;
            DireccionMintic.Value = MINTIC_ADDRESS;
            AZRadicar.PersonaType Mintic = new AZRadicar.PersonaType();
            Mintic.Direccion = DireccionMintic;
            Mintic.Nombre = MINTIC_NAME;
            Mintic.TipoIdentificacion = "NIT";
            Mintic.Identificacion = Mintic_NIT;
            Mintic.Telefono1 = "";
            Mintic.Telefono2 = "";
            Mintic.Fax = "";
            Mintic.Email = "";         

            AZRadicar.PersonaTypeDireccion DireccionOrganizacion = new AZRadicar.PersonaTypeDireccion();
            DireccionOrganizacion.Pais = PaisPRST;
            DireccionOrganizacion.Estado = DepartamentoPRST;
            DireccionOrganizacion.Ciudad = Ciudad.ToString();
            DireccionOrganizacion.Value = "";
            AZRadicar.PersonaType Organizacion = new AZRadicar.PersonaType();
            Organizacion.Direccion = DireccionOrganizacion;
            Organizacion.Nombre = NombrePRST;
            Organizacion.TipoIdentificacion = "NIT";
            Organizacion.Identificacion = NitPRST;
            Organizacion.Telefono1 = "";
            Organizacion.Telefono2 = "";
            Organizacion.Fax = "";
            Organizacion.Email = "";
            AZRadicar.PersonaTypeDireccion DireccionRepresentante = new AZRadicar.PersonaTypeDireccion();
            DireccionRepresentante.Value = "";
            AZRadicar.PersonaType Representante = new AZRadicar.PersonaType();
            Representante.Direccion = DireccionRepresentante;
            Representante.Nombre = NombreRepresentante;
            Representante.Identificacion = IdentificacionRepresentante;
            Representante.Telefono1 = TelefonoRepresentante;
            Representante.Telefono2 = "";
            Representante.Fax = "";
            Representante.Email = MailRepresentante;
            Representante.Cargo = CargoRepresentante;
            AZRadicar.RadicarDestinatario Destinatario = new AZRadicar.RadicarDestinatario();
            AZRadicar.RadicarRemitente Remitente = new AZRadicar.RadicarRemitente();
            if (esRadicado)
            {
                Destinatario.Organizacion = Mintic;
                Destinatario.Dependencia = Dependencia;
                
                Remitente.Persona = Representante;
                Remitente.Organizacion = Organizacion;
                Remitente.Dependencia = Dependencia;
            }
            else
            {                
                Destinatario.Persona = Representante;
                Destinatario.Organizacion = Organizacion;
                Destinatario.Dependencia = Dependencia;

                Remitente.Organizacion = Mintic;
                Remitente.Dependencia = Dependencia;
            }

            AZRadicar.Radicar objRadicar = new AZRadicar.Radicar();
            objRadicar.Asunto = Asunto;
            objRadicar.Observaciones = Observaciones;
            objRadicar.Destinatarios = new AZRadicar.RadicarDestinatario[1];
            objRadicar.Destinatarios[0] = Destinatario;
            objRadicar.Remitentes = new AZRadicar.RadicarRemitente[1];
            objRadicar.Remitentes[0] = Remitente;
            if (Estado == "Cr")
                objRadicar.Estado = AZRadicar.RadicarEstado.Cr;
            else if (Estado == "As")
                objRadicar.Estado = AZRadicar.RadicarEstado.As;
            else if (Estado == "DiS")
                objRadicar.Estado = AZRadicar.RadicarEstado.DiS;
            else if (Estado == "DeS")
                objRadicar.Estado = AZRadicar.RadicarEstado.DeS;
            else if (Estado == "Ent")
                objRadicar.Estado = AZRadicar.RadicarEstado.Ent;
            else if (Estado == "Dev")
                objRadicar.Estado = AZRadicar.RadicarEstado.Dev;
            objRadicar.Tramite = Tramite;

            if (Tipo.ToUpper().Equals("E"))
                objRadicar.Tipo = AZRadicar.RadicarTipo.E;
            else if (Tipo.ToUpper().Equals("S"))
                objRadicar.Tipo = AZRadicar.RadicarTipo.S;

            if (Modalidad.ToUpper().Equals("E"))
                objRadicar.Modalidad = AZRadicar.RadicarModalidad.E;
            else if (Modalidad.ToUpper().Equals("D"))
                objRadicar.Modalidad = AZRadicar.RadicarModalidad.D;
            objRadicar.Dependencia = Dependencia;
            objRadicar.Sitio = Sitio;
            objRadicar.IdReferencia = IdReferencia;
            objRadicar.ImagenAsociada = AZRadicar.RadicarImagenAsociada.Item1;
            objRadicar.UsuarioRadica = Usuario;
            if (NumRadicado != "")
                objRadicar.NumeroRadicadoAsociado = NumRadicado;

            while (tries > 0)
            {
                try
                {
                    wsResult = ws.Radicar(objRadicar);
                    break;
                }
                catch (Exception e)
                {
                    if (--tries == 0)
                    {

                    }
                    Thread.Sleep(1000);
                }
            }

            return wsResult;
        }

        public AZRadicar.RespuestaOperacion AZRadicarConcesionarioToMinticSolicitar(ref ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, ref ST_Helper Helper, ref MemoryStream mem)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");
            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SOLICITUD", "Radicado solicitud", "Concesionario");

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
                Helper.TXT["AZRAD_TRAMITE"],
                Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
                Helper.TXT["AZRAD_MODALIDAD"],
                Helper.TXT["AZRAD_DEPENDENCIA"],
                Helper.TXT["AZRAD_USUARIO"],
                Helper.TXT["AZRAD_SITIO"],
                Helper.TXT["MINTIC_NAME"],
                Helper.TXT["MINTIC_NIT"],
                Helper.TXT["MINTIC_ADDRESS"],
                Helper.TXT["MINTIC_PAIS"],
                Helper.TXT["MINTIC_DEPARTAMENTO"],
                Helper.TXT["MINTIC_CIUDAD"],
                Solicitud.Expediente.Operador.CUS_NAME,
                Solicitud.Expediente.Operador.CUS_IDENT,
                Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
                Solicitud.Expediente.Operador.CUS_STATECODE,
                Solicitud.Expediente.Operador.CUS_CITYCODE,
                Solicitud.Contacto.CONT_NAME,
                Solicitud.Contacto.CONT_NUMBER,
                Solicitud.Contacto.CONT_TEL,
                Solicitud.Contacto.CONT_EMAIL,
                Solicitud.Contacto.CONT_TITLE,
                "Solicitud: " + Solicitud.SOL_TYPE_NAME,
                "Solicitud Consecionario a Mintic.",
                "Cr",
                "",
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                true
                );


            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            //AZRadicar.RespuestaOperacion resultCreacarpeta = CrearCarpeta(ref ws, "D", Convert.ToUInt64(Helper.TXT["AZRAD_IDPADRE"]), resultado.NuevoRaNumero, Solicitud.Expediente.SERV_NAME + "_" + resultado.NuevoRaNumero);
            //if (resultCreacarpeta == null)
            //{
            //    throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada.", " - Error del WebService de AZDigital al intentar crear carpeta"));
            //}
            //Solicitud.AZ_DIRECTORIO = resultCreacarpeta.NuevoDiId;
            Solicitud.AZ_DIRECTORIO = ObtenerIdDirRadicadosAZD(ref ws, Solicitud.Expediente.SERV_NUMBER.ToString(), true, Helper.TXT["AZRAD_IDPADRE"]);


            //Adjunte los documentos asociados
            foreach (var File in Files)
            {
                try
                {
                    AZRadicar.RespuestaOperacion resultCargaArchivo = CargarArchivo(ref ws, File, Solicitud.AZ_DIRECTORIO, resultado.NuevoRaNumero);
                }
                catch (Exception ex)
                {
                    string mensajeError = string.Format("Ocurrió error al adjuntar el archivo '{}' en AZDigital: {1}", File.NAME, ex.Message);
                    // resultado.AddErrorAlEnviarArchivo(mensajeError);
                }
            }
            return resultado;

        }

        public AZRadicar.RespuestaOperacion AZRadicarConcesionarioToMinticSubsanar(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            string Registro = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Registro).Last().DOC_NUMBER;

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            DynamicSolicitud.AddKey("REGISTRO", Registro);

            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SUBSANAR", "Radicado subsanación solicitud", "Concesionario");

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
                Helper.TXT["AZRAD_TRAMITE"],
                Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
                Helper.TXT["AZRAD_MODALIDAD"],
                Helper.TXT["AZRAD_DEPENDENCIA"],
                Helper.TXT["AZRAD_USUARIO"],
                Helper.TXT["AZRAD_SITIO"],
                Helper.TXT["MINTIC_NAME"],
                Helper.TXT["MINTIC_NIT"],
                Helper.TXT["MINTIC_ADDRESS"],
                Helper.TXT["MINTIC_PAIS"],
                Helper.TXT["MINTIC_DEPARTAMENTO"],
                Helper.TXT["MINTIC_CIUDAD"],
                Solicitud.Expediente.Operador.CUS_NAME,
                Solicitud.Expediente.Operador.CUS_IDENT,
                Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
                Solicitud.Expediente.Operador.CUS_STATECODE,
                Solicitud.Expediente.Operador.CUS_CITYCODE,
                Solicitud.Contacto.CONT_NAME,
                Solicitud.Contacto.CONT_NUMBER,
                Solicitud.Contacto.CONT_TEL,
                Solicitud.Contacto.CONT_EMAIL,
                Solicitud.Contacto.CONT_TITLE,
                "Solicitud: " + Solicitud.SOL_TYPE_NAME,
                "Subsanación Consecionario a Mintic.",
                "As",
                Solicitud.Radicado,
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                true
                );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            /*AZRadicar.RespuestaOperacion resultCreacarpeta = CrearCarpeta(ref ws, "D", Convert.ToUInt64(Helper.TXT["AZRAD_IDPADRE"]), resultado.NuevoRaNumero, Solicitud.Expediente.SERV_NAME + "_" + resultado.NuevoRaNumero);
            if (resultCreacarpeta == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada.", " - Error del WebService de AZDigital al intentar crear carpeta"));
            }*/


            //Adjunte los documentos asociados
            foreach (var File in Files)
            {
                try
                {
                    AZRadicar.RespuestaOperacion resultCargaArchivo = CargarArchivo(ref ws, File, Solicitud.AZ_DIRECTORIO, resultado.NuevoRaNumero);
                }
                catch (Exception ex)
                {
                    string mensajeError = string.Format("Ocurrió error al adjuntar el archivo '{}' en AZDigital: {1}", File.NAME, ex.Message);
                }
            }
            return resultado;

        }

        public AZRadicar.RespuestaOperacion AZRadicarConcesionarioToMinticCancelar(ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_CANCELAR", "Radicado cancelación solicitud", "Concesionario");

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
                Helper.TXT["AZRAD_TRAMITE"],
                Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
                Helper.TXT["AZRAD_MODALIDAD"],
                Helper.TXT["AZRAD_DEPENDENCIA"],
                Helper.TXT["AZRAD_USUARIO"],
                Helper.TXT["AZRAD_SITIO"],
                Helper.TXT["MINTIC_NAME"],
                Helper.TXT["MINTIC_NIT"],
                Helper.TXT["MINTIC_ADDRESS"],
                Helper.TXT["MINTIC_PAIS"],
                Helper.TXT["MINTIC_DEPARTAMENTO"],
                Helper.TXT["MINTIC_CIUDAD"],
                Solicitud.Expediente.Operador.CUS_NAME,
                Solicitud.Expediente.Operador.CUS_IDENT,
                Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
                Solicitud.Expediente.Operador.CUS_STATECODE,
                Solicitud.Expediente.Operador.CUS_CITYCODE,
                Solicitud.Contacto.CONT_NAME,
                Solicitud.Contacto.CONT_NUMBER,
                Solicitud.Contacto.CONT_TEL,
                Solicitud.Contacto.CONT_EMAIL,
                Solicitud.Contacto.CONT_TITLE,
                "Solicitud: " + Solicitud.SOL_TYPE_NAME,
                "Cancelación Consecionario a Mintic.",
                "An",
                Solicitud.Radicado,
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                true
                );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }


            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();
            //ListaRadicados.Add(resultado.CodigoRadicado);

            // ArchivarDocumento(ListaRadicados.ToArray(), Helper.TXT["ALFA_SERIE_DOCUMENTAL"], Helper.TXT["ALFA_USER"], Helper.TXT["ALFA_PASSWORD"]);

            return resultado;

        }

        public AZRadicar.RespuestaOperacion AZRadicarConcesionarioToMinticAplazar(ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_APLAZAR", "Radicado aplazamiento requerimiento de solicitud", "Concesionario");

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
              Helper.TXT["AZRAD_TRAMITE"],
              Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
              Helper.TXT["AZRAD_MODALIDAD"],
              Helper.TXT["AZRAD_DEPENDENCIA"],
              Helper.TXT["AZRAD_USUARIO"],
              Helper.TXT["AZRAD_SITIO"],
              Helper.TXT["MINTIC_NAME"],
              Helper.TXT["MINTIC_NIT"],
              Helper.TXT["MINTIC_ADDRESS"],
              Helper.TXT["MINTIC_PAIS"],
              Helper.TXT["MINTIC_DEPARTAMENTO"],
              Helper.TXT["MINTIC_CIUDAD"],
              Solicitud.Expediente.Operador.CUS_NAME,
              Solicitud.Expediente.Operador.CUS_IDENT,
              Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
              Solicitud.Expediente.Operador.CUS_STATECODE,
              Solicitud.Expediente.Operador.CUS_CITYCODE,
              Solicitud.Contacto.CONT_NAME,
              Solicitud.Contacto.CONT_NUMBER,
              Solicitud.Contacto.CONT_TEL,
              Solicitud.Contacto.CONT_EMAIL,
              Solicitud.Contacto.CONT_TITLE,
              "Solicitud: " + Solicitud.SOL_TYPE_NAME,
              "Solicitud aplazamiento Consecionario a Mintic.",
              "As",
              Solicitud.Radicado,
              Solicitud.Expediente.SERV_NUMBER.ToString(),
              true
              );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            return resultado;

        }

        #endregion

        #region Registrar

        public string AzRegistrarAneToMintic(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY, ref MemoryStream mem)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
            Helper.TXT["AZREG_TRAMITE"],
            Helper.TXT["AZREG_TIPO"],    //Radicado de entrada
            Helper.TXT["AZREG_MODALIDAD"],
            Helper.TXT["AZREG_DEPENDENCIA"],
            Helper.TXT["AZRAD_USUARIO"],
            Helper.TXT["AZRAD_SITIO"],
            Helper.TXT["MINTIC_NAME"],
            Helper.TXT["MINTIC_NIT"],
            Helper.TXT["MINTIC_ADDRESS"],
            Helper.TXT["MINTIC_PAIS"],
            Helper.TXT["MINTIC_DEPARTAMENTO"],
            Helper.TXT["MINTIC_CIUDAD"],
            Solicitud.Expediente.Operador.CUS_NAME,
            Solicitud.Expediente.Operador.CUS_IDENT,
            Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
            Solicitud.Expediente.Operador.CUS_STATECODE,
            Solicitud.Expediente.Operador.CUS_CITYCODE,
            Solicitud.Contacto.CONT_NAME,
            Solicitud.Contacto.CONT_NUMBER,
            Solicitud.Contacto.CONT_TEL,
            Solicitud.Contacto.CONT_EMAIL,
            Solicitud.Contacto.CONT_TITLE,
            "Solicitud: " + Solicitud.SOL_TYPE_NAME,
            "Registro ANE a Mintic. " + ALFA_REG_KEY,
            "As",
            Solicitud.Radicado,
            Solicitud.Expediente.SERV_NUMBER.ToString()
            );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            return resultado.NuevoRaNumero;
        }

        public string AzRegistrarAneToConcesionarioRequerir(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
            Helper.TXT["AZREG_TRAMITE"],
            Helper.TXT["AZREG_TIPO"],    //Radicado de entrada
            Helper.TXT["AZREG_MODALIDAD"],
            Helper.TXT["AZREG_DEPENDENCIA"],
            Helper.TXT["AZRAD_USUARIO"],
            Helper.TXT["AZRAD_SITIO"],
            Helper.TXT["MINTIC_NAME"],
            Helper.TXT["MINTIC_NIT"],
            Helper.TXT["MINTIC_ADDRESS"],
            Helper.TXT["MINTIC_PAIS"],
            Helper.TXT["MINTIC_DEPARTAMENTO"],
            Helper.TXT["MINTIC_CIUDAD"],
            Solicitud.Expediente.Operador.CUS_NAME,
            Solicitud.Expediente.Operador.CUS_IDENT,
            Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
            Solicitud.Expediente.Operador.CUS_STATECODE,
            Solicitud.Expediente.Operador.CUS_CITYCODE,
            Solicitud.Contacto.CONT_NAME,
            Solicitud.Contacto.CONT_NUMBER,
            Solicitud.Contacto.CONT_TEL,
            Solicitud.Contacto.CONT_EMAIL,
            Solicitud.Contacto.CONT_TITLE,
            "Solicitud: " + Solicitud.SOL_TYPE_NAME,
            "Registro Requerimiento ANE a Consecionario.",
            "Cr",
            "",
            Solicitud.Expediente.SERV_NUMBER.ToString()
            );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            return resultado.NuevoRaNumero;

        }

        public string AZRegistrarMinticToAne(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");
            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
            Helper.TXT["AZREG_TRAMITE"],
            Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
            Helper.TXT["AZRAD_MODALIDAD"],
            Helper.TXT["AZRAD_DEPENDENCIA"],
            Helper.TXT["AZRAD_USUARIO"],
            Helper.TXT["AZRAD_SITIO"],
            Helper.TXT["MINTIC_NAME"],
            Helper.TXT["MINTIC_NIT"],
            Helper.TXT["MINTIC_ADDRESS"],
            Helper.TXT["MINTIC_PAIS"],
            Helper.TXT["MINTIC_DEPARTAMENTO"],
            Helper.TXT["MINTIC_CIUDAD"],
            Solicitud.Expediente.Operador.CUS_NAME,
            Solicitud.Expediente.Operador.CUS_IDENT,
            Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
            Solicitud.Expediente.Operador.CUS_STATECODE,
            Solicitud.Expediente.Operador.CUS_CITYCODE,
            Solicitud.Contacto.CONT_NAME,
            Solicitud.Contacto.CONT_NUMBER,
            Solicitud.Contacto.CONT_TEL,
            Solicitud.Contacto.CONT_EMAIL,
            Solicitud.Contacto.CONT_TITLE,
            "Solicitud: " + Solicitud.SOL_TYPE_NAME,
            "Registro Mintic a ANE.",
            "Cr",
            "",
            Solicitud.Expediente.SERV_NUMBER.ToString()
            );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            return resultado.NuevoRaNumero;
        }

        public string AZRegistrarMinticToConcesionarioRequerir(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
            Helper.TXT["AZREG_TRAMITE"],
            Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
            Helper.TXT["AZRAD_MODALIDAD"],
            Helper.TXT["AZRAD_DEPENDENCIA"],
            Helper.TXT["AZRAD_USUARIO"],
            Helper.TXT["AZRAD_SITIO"],
            Helper.TXT["MINTIC_NAME"],
            Helper.TXT["MINTIC_NIT"],
            Helper.TXT["MINTIC_ADDRESS"],
            Helper.TXT["MINTIC_PAIS"],
            Helper.TXT["MINTIC_DEPARTAMENTO"],
            Helper.TXT["MINTIC_CIUDAD"],
            Solicitud.Expediente.Operador.CUS_NAME,
            Solicitud.Expediente.Operador.CUS_IDENT,
            Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
            Solicitud.Expediente.Operador.CUS_STATECODE,
            Solicitud.Expediente.Operador.CUS_CITYCODE,
            Solicitud.Contacto.CONT_NAME,
            Solicitud.Contacto.CONT_NUMBER,
            Solicitud.Contacto.CONT_TEL,
            Solicitud.Contacto.CONT_EMAIL,
            Solicitud.Contacto.CONT_TITLE,
            "Solicitud: " + Solicitud.SOL_TYPE_NAME,
            "Registro Requerimiento Mintic a Consecionario.",
            "Cr",
            "",
            Solicitud.Expediente.SERV_NUMBER.ToString()
             );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            return resultado.NuevoRaNumero;
        }

        public string AZRegistrarMinticToConcesionario(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY)
		{
			AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

			RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
			List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();
			AZRadicar.RespuestaOperacion resultado = AZRadicarTramiteRetry(ref ws,
			Helper.TXT["AZREG_TRAMITE"],
			Helper.TXT["AZRAD_TIPO"],    //Radicado de entrada
			Helper.TXT["AZRAD_MODALIDAD"],
			Helper.TXT["AZRAD_DEPENDENCIA"],
			Helper.TXT["AZRAD_USUARIO"],
			Helper.TXT["AZRAD_SITIO"],
			Helper.TXT["MINTIC_NAME"],
			Helper.TXT["MINTIC_NIT"],
			Helper.TXT["MINTIC_ADDRESS"],
			Helper.TXT["MINTIC_PAIS"],
			Helper.TXT["MINTIC_DEPARTAMENTO"],
			Helper.TXT["MINTIC_CIUDAD"],
			Solicitud.Expediente.Operador.CUS_NAME,
			Solicitud.Expediente.Operador.CUS_IDENT,
			Solicitud.Expediente.Operador.CUS_COUNTRYCODE,
			Solicitud.Expediente.Operador.CUS_STATECODE,
			Solicitud.Expediente.Operador.CUS_CITYCODE,
			Solicitud.Contacto.CONT_NAME,
			Solicitud.Contacto.CONT_NUMBER,
			Solicitud.Contacto.CONT_TEL,
			Solicitud.Contacto.CONT_EMAIL,
			Solicitud.Contacto.CONT_TITLE,
			"Solicitud: " + Solicitud.SOL_TYPE_NAME,
            "Registro Mintic a Consecionario.",
			"Cr",
			"",
            Solicitud.Expediente.SERV_NUMBER.ToString()
             );

            if (resultado == null)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", " - Error del WebService de AZDigital al intentar radicar"));
            }

            //ArchivarDocumento(ListaRadicados.ToArray(), Helper.TXT["ALFA_SERIE_DOCUMENTAL"], Helper.TXT["ALFA_USER"], Helper.TXT["ALFA_PASSWORD"]);

            return resultado.NuevoRaNumero;
        }

        #endregion

        #region Auxiliares

        public AZRadicar.InformacionRadicado AZConsultaRadicado(string vRadicadoCodigo)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");
            AZRadicar.InformarRadicado informar = new AZRadicar.InformarRadicado();
            informar.NumeroRadicado = vRadicadoCodigo;
            AZRadicar.InformacionRadicado inforad = null;
            try
            {
                inforad = ws.InformarRadicado(informar);
            }
            catch (Exception e)
            {

            }

            return inforad;
        }

        private string AZAdjuntarImgRegRetry(ref AZRadicar.sgdaPortTypeClient ws, string Registro, string NombreArchivo, ref MemoryStream mem, string user, string password)
        {
            AZRadicar.RespuestaOperacion wsResult;
            var tries = 3;
            while (true)
            {
                try
                {
                    AZRadicar.CargarArchivo Archivo = new AZRadicar.CargarArchivo();
                    Archivo.Archivo = mem.GetBuffer();
                    Archivo.Codificacion = AZRadicar.CargarArchivoCodificacion.Base64;
                    Archivo.Nombre = NombreArchivo;
                    Archivo.IdDirectorio = 1; //IdDir;
                    wsResult = ws.CargarArchivo(Archivo);
                    return "";
                }
                catch (Exception ex)
                {
                    if (--tries == 0)
                        return ex.Message;
                    Thread.Sleep(1000);
                }
            }
        }

        private string AZCargarArchivo(ref AZRadicar.sgdaPortTypeClient ws, ulong IdDir, string NumeroRadicado,
                    string NombreArchivo, ref MemoryStream mem, string user, string password)
        {
            string xmlResult = "";
            AZRadicar.RespuestaOperacion wsResult;
            var tries = 3;
            while (true)
            {
                try
                {
                    AZRadicar.CargarArchivo Archivo = new AZRadicar.CargarArchivo();
                    Archivo.Archivo = mem.GetBuffer();
                    Archivo.Codificacion = AZRadicar.CargarArchivoCodificacion.Base64;
                    Archivo.Nombre = NombreArchivo;
                    Archivo.IdDirectorio = IdDir;
                    Archivo.RadNumero = NumeroRadicado;
                    wsResult = ws.CargarArchivo(Archivo);
                    break;
                }
                catch (Exception e)
                {
                    if (--tries == 0)
                        throw e;
                    Thread.Sleep(1000);
                }
            }
            return xmlResult;
        }


        private string AZArchivarDocumentoRetry(ref AZRadicar.sgdaPortTypeClient ws, string[] DocumentoCodigo, string SerieCodigo, string user, string password)
        {
            string xmlResult = "";
            AZRadicar.RespuestaOperacion wsresult;
            var tries = 3;
            while (true)
            {
                try
                {
                    AZRadicar.Mover mover = new AZRadicar.Mover();


                    wsresult = ws.Mover(mover);
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return xmlResult;
        }

        /*private AZRadicar.InformacionRadicado AZComunicadosXTramite(string vDocumentoCodigo, string vExpedienteCodigo)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            return ConsultaComunicadosXTramiteRetry(ref ws, vDocumentoCodigo, vExpedienteCodigo);
            /*
            ResultadoComunicadosXTramiteAlfa resultado = ResultadoComunicadosXTramiteAlfa.CrearResultadoComunicadosXTramiteAlfa(Res);

            return resultado;

        }
        */
        public string RegisterAddDocument(string registroNumber, string nombreArchivo, ref ST_RDSSolicitud Solicitud, ref MemoryStream mem, ref ST_Helper Helper)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");
            AZRadicar.RespuestaOperacion resultCargaArchivo = null;
            var bytes = new Byte[(int)mem.Length];

            mem.Seek(0, SeekOrigin.Begin);
            mem.Read(bytes, 0, (int)mem.Length);
            string Archivo = Convert.ToBase64String(bytes);
            ST_RDSFiles_Up File = new ST_RDSFiles_Up();
            File.NAME = nombreArchivo;
            File.DATA = Archivo;
            Solicitud.AZ_DIRECTORIO = ObtenerIdDirRadicadosAZD(ref ws, Solicitud.Expediente.SERV_NUMBER.ToString(), true, Helper.TXT["AZRAD_IDPADRE"]);

            try
            {
                resultCargaArchivo = CargarArchivo(ref ws, File, Solicitud.AZ_DIRECTORIO, Solicitud.Radicado);
            }
            catch (Exception ex)
            {
                string mensajeError = string.Format("Ocurrió error al adjuntar el archivo '{}' en AZDigital: {1}", File.NAME, ex.Message);
                // resultado.AddErrorAlEnviarArchivo(mensajeError);
            }

            return resultCargaArchivo.Estado.ToString();
        }
        /*
        private bool ArchivarDocumento(string[] ListaRadicados, string SerieDocumental, string user, string password)
        {
            AZRadicar.sgdaPortTypeClient ws = new AZRadicar.sgdaPortTypeClient("sgdaHttpSoap11Endpoint");

            string XmlResult = null;

            XmlResult = AZArchivarDocumentoRetry(ref ws, ListaRadicados, SerieDocumental, user, password);

            bool respuesta = GetFinalizadoExitosamente(XmlResult);
            return respuesta;
        }

        private static bool GetFinalizadoExitosamente(string alfaResult)
        {
            if (string.IsNullOrEmpty(alfaResult)) return false;
            return alfaResult.Contains("Proceso finalizado exitosamente");
        }
        */

        private static AZRadicar.RespuestaOperacion CrearCarpeta(ref AZRadicar.sgdaPortTypeClient ws,
            string Tipo,
            ulong IdPadre,
            string RadNumero,
            string Nombre)
        {
            AZRadicar.RespuestaOperacion wsResult = null;
            AZRadicar.CrearDirectorio directorio = new AZRadicar.CrearDirectorio();
            if (Tipo.ToUpper() == "R")
                directorio.Tipo = AZRadicar.TipoDirectorioType.R;
            else if (Tipo.ToUpper() == "C")
                directorio.Tipo = AZRadicar.TipoDirectorioType.C;
            else if (Tipo.ToUpper() == "P")
                directorio.Tipo = AZRadicar.TipoDirectorioType.P;
            else if (Tipo.ToUpper() == "D")
                directorio.Tipo = AZRadicar.TipoDirectorioType.D;
            else if (Tipo.ToUpper() == "E")
                directorio.Tipo = AZRadicar.TipoDirectorioType.E;
            else if (Tipo.ToUpper() == "T")
                directorio.Tipo = AZRadicar.TipoDirectorioType.T;
            directorio.IdPadre = IdPadre;
            directorio.RadNumero = RadNumero;
            directorio.Nombre = Nombre;
            try
            {
                wsResult = ws.CrearDirectorio(directorio);
            }
            catch (Exception e)
            {

            }

            return wsResult;
        }

        private static AZRadicar.RespuestaOperacion CargarArchivo(ref AZRadicar.sgdaPortTypeClient ws,
            ST_RDSFiles_Up file,
            string IdDirectorio,
            string RadNumero)
        {
            AZRadicar.RespuestaOperacion wsResult = null;
            AZRadicar.CargarArchivo archivo = new AZRadicar.CargarArchivo();
            archivo.IdDirectorio = Convert.ToUInt64(IdDirectorio);
            archivo.Nombre = file.NAME;
            archivo.Codificacion = AZRadicar.CargarArchivoCodificacion.Base64;
            archivo.RadNumero = RadNumero;
            //int Tam = file.DATA.IndexOf(",");
            //if (Tam < 0)
            //{
            //    throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada.", " - Error del WebService de AZDigital al intentar subir el archivo")); ;
            //}

            //byte[] data = Convert.FromBase64String(file.DATA.Substring(Tam + 1));
            //archivo.Archivo = Convert.ToBase64String(data);
            archivo.Archivo = file.DATA;
            try
            {
                wsResult = ws.CargarArchivo(archivo);
            }
            catch (Exception e)
            {

            }

            return wsResult;
        }

        private string ObtenerIdDirRadicadosAZD(ref AZRadicar.sgdaPortTypeClient ws, string expediente, bool esRadicado, string directorio)
        {
            string idDirPso = "0", idDirExpedientes = "0", idDirExpediente = "0", idDirRadicadosAZD = "0";

            // Obtener directorio "PSO"
            idDirPso = directorio;

            // Obtener directorio "Expedientes"
            idDirExpedientes = ObtenerDirectorioAZD(ref ws, idDirPso, "Expedientes", AZRadicar.TipoDirectorioType.D, true);

            // Obtener directorio del expediente
            idDirExpediente = ObtenerDirectorioAZD(ref ws, idDirExpedientes, expediente, AZRadicar.TipoDirectorioType.D, true);

            // Obtener directorio de registros o radicados
            if (esRadicado)
                idDirRadicadosAZD = ObtenerDirectorioAZD(ref ws, idDirExpediente, "Radicados", AZRadicar.TipoDirectorioType.D, true);
            else
                idDirRadicadosAZD = ObtenerDirectorioAZD(ref ws, idDirExpediente, "Registros", AZRadicar.TipoDirectorioType.D, true);

            return idDirRadicadosAZD;
        }

        /// <summary>
        /// Obtener identificador de directorio en AZ Digital
        /// </summary>
        /// <param name="idPadre">Identificador de la carpeta padre</param>
        /// <param name="nombre">Nombre de la carpeta a crear</param>
        /// <param name="tipo">Tipo de directorio de AZ Digital</param>
        /// <param name="crearDirectorio">Indica si se debe crear el directorio en caso de que no exista</param>
        /// <returns>Identificador del directorio existente o nuevo. 0 si no existe</returns>
        private string ObtenerDirectorioAZD(ref AZRadicar.sgdaPortTypeClient ws, string idPadre, string nombre, AZRadicar.TipoDirectorioType tipo, bool crearDirectorio)
        {
            string idDirectorio = "0";
            bool directorioExistente = false;

            AZRadicar.Informar datosDirectorioExistente = new AZRadicar.Informar
            {
                TipoElemento = AZRadicar.TipoElementoType.Directorio,
                IdElemento = Convert.ToUInt64(idPadre),
            };

           // Verificar si el directorio ya existe en AZ Digital
            try
            {
                AZRadicar.Informacion directoriosHijosData = ws.Informar(datosDirectorioExistente);
                AZRadicar.InformacionDirectorio info = (AZRadicar.InformacionDirectorio)directoriosHijosData.Item;
                if (info.Directorio != null && info.Directorio.Length > 0)
                {
                    foreach (var item in info.Directorio)
                    {
                        if (item.Nombre == nombre)
                        {
                            idDirectorio = item.Id.ToString();
                            directorioExistente = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                idDirectorio = "0";
            }
            finally
            {

            }

            // En caso de que el directorio no exista, se creará uno nuevo
            if (!directorioExistente && crearDirectorio)
            {
                AZRadicar.CrearDirectorio datosDirectorioNuevo = new AZRadicar.CrearDirectorio
                {
                    IdPadre = Convert.ToUInt64(idPadre),
                    Nombre = nombre,
                    Tipo = tipo,
                };
                try
                {
                    AZRadicar.RespuestaOperacion resultado = ws.CrearDirectorio(datosDirectorioNuevo);
                    idDirectorio = resultado.NuevoDiId;
                }
                finally
                {
                }
            }

            return idDirectorio;
        }

        #endregion


    }
}