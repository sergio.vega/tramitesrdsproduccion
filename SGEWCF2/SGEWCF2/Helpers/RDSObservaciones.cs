﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Xceed.Words.NET;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;


namespace SGEWCF2.Helpers
{
    public class RDSObservaciones
    {

        public RDSObservaciones(){}      

        public byte [] GenerarWordPlantilla(string html)
        {
            byte[] bytes = null;
            MemoryStream ms = new MemoryStream();
            try
            {
                //html = Regex.Replace(html, "&nbsp;", string.Empty);
                html = PuntuacionHtml(html);
                using (ms)
                {
                    var ruta = Path.GetFullPath(HttpContext.Current.Server.MapPath(@"~/Contents") + @"\Base_Plantilla_Informe_Observaciones.docx");
                    var document = DocX.Load(ruta);
                    ms.Position = 0;
                    document.SaveAs(ms);

                    using (WordprocessingDocument myDoc = WordprocessingDocument.Open(ms, true))
                    {
                        
                        MainDocumentPart mainPart = myDoc.MainDocumentPart;

                        if (html != null)
                        {
                            SaveHtmlDocument(@"<html><body>" + html + "</body></html>", "AltChunkId1", mainPart);
                        }
                    }

                    document.Save();
                    bytes = ms.ToArray();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return bytes;
        }

        public byte [] GenerarInformeRespuestasObservaciones(string html)
        {
            try
            {
                byte[] bytes = null;
                bytes = GenerarWordPlantilla(html);
                return bytes;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void SaveHtmlDocument(string html, string altChunkId, MainDocumentPart mainPart)
        {
            AlternativeFormatImportPart chunk = mainPart.AddAlternativeFormatImportPart("application/Xhtml+Xml", altChunkId);

            using (Stream chunkStream = chunk.GetStream(FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter stringStream = new StreamWriter(chunkStream))
                {
                    stringStream.Write(html);
                }
            }
            AltChunk altChunk = new AltChunk();
            altChunk.Id = altChunkId;
            mainPart.Document.Body.InsertBefore(altChunk, mainPart.Document.Body.Elements().FirstOrDefault());

        }

        private static string PuntuacionHtml(string html)
        {
            html = html.Replace("á", "&aacute;")
            .Replace("é", "&eacute;")
            .Replace("í", "&iacute;")
            .Replace("ó", "&oacute;")
            .Replace("ú", "&uacute;")
            .Replace("ñ", "&ntilde;")
            .Replace("Á", "&Aacute;")
            .Replace("É", "&Eacute;")
            .Replace("Í", "&Iacute;")
            .Replace("Ó", "&Oacute;")
            .Replace("Ú", "&Uacute;")
            .Replace("Ñ", "&Ntilde;");

            html = Regex.Replace(html, "&nbsp;", string.Empty);

            return html;
        }

    }
}