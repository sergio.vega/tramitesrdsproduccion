﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Services;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using SGEWCF2.Helpers;
using SpreadsheetLight;
using SGEWCF2.BDU;
using SGEWCF2.AZDigitall;
using System.Xml.Serialization;
using Xceed.Words.NET;
using Xceed.Document.NET;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace SGEWCF2
{
    [JSONPSupportBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SGEWS : ISGEWS
    {
        string sSGELogs = ConfigurationManager.AppSettings["SGELogs"];
        string SGETokenTimeOut = ConfigurationManager.AppSettings["SGETokenTimeOut"];
        
        
        #region Main

        public ST_RDSMainData GetMainData()
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSMainData mResult = new ST_RDSMainData();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                mResult.HtmlFooter = cData.TraerValor("SELECT TXT_TEXT FROM RADIO.TEXTS  WHERE TXT_ID = 'MINTIC_HTML_FOOTER'", "");

                sSQL = @"Select SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_CLASS, SOL_TYPE_DESC
                        From RADIO.TIPOS_SOL 
                        Where SOL_IMPLEMENTED=1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult.SOL_TYPES = (from bp in dt
                                   select new ST_RDSTipoSolicitud
                                   {
                                       SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                       SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                       SOL_TYPE_CLASS = bp.Field<int>("SOL_TYPE_CLASS"),
                                       SOL_TYPE_DESC = bp.Field<string>("SOL_TYPE_DESC")
                                   }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Login

        public ST_RDSUsuarioEx Autenticar(string USR_LOGIN, string USR_PASSWORD, string APP_VERSION)
        {
            csData cData = null;

            try
            {

                ST_RDSUsuarioEx Usuario = new ST_RDSUsuarioEx(-1);

                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "USR_LOGIN=" + USR_LOGIN, "!ConectedOk", strConn, "USR_PASSWORD=" + USR_PASSWORD);
                    return Usuario;
                }

                if (string.IsNullOrEmpty(USR_LOGIN) || string.IsNullOrEmpty(USR_PASSWORD))
                    return Usuario;

                ValidateIsConcesionario(ref cData, USR_LOGIN);

                string sSQL;

                sSQL = @" 
                        DECLARE @USR_LOGIN nvarchar(50)='" + USR_LOGIN + @"'
                        DECLARE @USR_PASSWORD nvarchar(128)='" + EncryptSHA256(USR_PASSWORD) + @"'
                        SELECT USR_ID, USR_GROUP, USR_LOGIN, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL, Case When USR_SIGN is null Then 0 else 1 end USR_HAS_SIGN
                        FROM RADIO.USUARIOS
                        Where USR_LOGIN = @USR_LOGIN And USR_PASSWORD = @USR_PASSWORD And USR_STATUS = 1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();


                var mResult = (from bp in dt
                               select new ST_RDSUsuarioEx
                               {
                                   USR_ID = bp.Field<int>("USR_ID"),
                                   USR_GROUP = bp.Field<int>("USR_GROUP"),
                                   USR_LOGIN = bp.Field<string>("USR_LOGIN"),
                                   USR_NAME = bp.Field<string>("USR_NAME"),
                                   USR_ROLE = bp.Field<int>("USR_ROLE"),
                                   USR_ASSIGNABLE = bp.Field<int>("USR_ASSIGNABLE"),
                                   USR_STATUS = bp.Field<int>("USR_STATUS"),
                                   USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                                   USR_HAS_SIGN = bp.Field<int>("USR_HAS_SIGN")
                               }).ToList();


                if (mResult.Count != 1)
                {
                    WriteLogErrorLogin(System.Reflection.MethodBase.GetCurrentMethod().Name, USR_LOGIN, USR_PASSWORD, APP_VERSION, "Usuario no existe");
                    return new ST_RDSUsuarioEx(0); 
                }

                Usuario = mResult.First();

                Guid TOKEN = Guid.NewGuid();
                string USER_AGENT = HttpContext.Current.Request.UserAgent;
                UAgent UA = GetUserAgentInfo(USER_AGENT);

                string IP_ADDRESS = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                sSQL = "Insert Into RADIO.TOKENS (TOKEN, USR_ID, APP_VERSION, IP_ADDRESS, USER_AGENT, OS, BROWSER) Values ('" + TOKEN + "', " + Usuario.USR_ID + ", '" + APP_VERSION + "', '" + IP_ADDRESS + "','" + USER_AGENT + "','" + UA.OS + "','" + UA.BROWSER + "')";

                bool bRet = cData.ClickExecuteNonQuery(sSQL);
                if (!bRet)
                    return new ST_RDSUsuarioEx(-1);

                Usuario.TOKEN = TOKEN;

                return Usuario;
            }

            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "USR_LOGIN=" + USR_LOGIN, "Exception e", e.Message, "USR_PASSWORD=" + USR_PASSWORD);
                return new ST_RDSUsuarioEx(-1);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ST_RDSUsuarioEx AutenticarFO(string CREDENCIALES_FO, string APP_VERSION)
        {
            csData cData = null;

            try
            {
                ST_RDSUsuarioEx Usuario = new ST_RDSUsuarioEx(-1);

                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "CREDENCIALES_FO=" + CREDENCIALES_FO, "!ConectedOk", strConn);
                    return Usuario;
                }

                if (string.IsNullOrEmpty(CREDENCIALES_FO) || CREDENCIALES_FO.Length != 344) return Usuario;

                ST_MyEncrypt Credenciales = MyDecrypt(CREDENCIALES_FO);
                if(!Credenciales.IsValid)
                    return new ST_RDSUsuarioEx(-3); 

                TimeSpan span = DateTime.Now - Credenciales.getExpireDate();
                double totalMinutes = Math.Abs(span.TotalMinutes);
                if (totalMinutes > 3)
                    return new ST_RDSUsuarioEx(-2); 

                if(Credenciales.Group==(int)GROUPS.Concesionario)
                    ValidateIsConcesionario(ref cData, Credenciales.Login);
                if (Credenciales.Group == (int)GROUPS.MinTIC || Credenciales.Group == (int)GROUPS.ANE)
                    ValidateUserExists(ref cData, Credenciales);

                string sSQL;

                sSQL = @" 
                        DECLARE @USR_LOGIN nvarchar(50)='" + Credenciales.Login + @"'
                        SELECT USR_ID, USR_GROUP, USR_LOGIN, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL, Case When USR_SIGN is null Then 0 else 1 end USR_HAS_SIGN
                        FROM RADIO.USUARIOS
                        Where USR_LOGIN = @USR_LOGIN And USR_STATUS = 1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();


                var mResult = (from bp in dt
                               select new ST_RDSUsuarioEx
                               {
                                   USR_ID = bp.Field<int>("USR_ID"),
                                   USR_GROUP = bp.Field<int>("USR_GROUP"),
                                   USR_LOGIN = bp.Field<string>("USR_LOGIN"),
                                   USR_NAME = bp.Field<string>("USR_NAME"),
                                   USR_ROLE = bp.Field<int>("USR_ROLE"),
                                   USR_ASSIGNABLE = bp.Field<int>("USR_ASSIGNABLE"),
                                   USR_STATUS = bp.Field<int>("USR_STATUS"),
                                   USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                                   USR_HAS_SIGN = bp.Field<int>("USR_HAS_SIGN")
                               }).ToList();

                if (mResult.Count != 1)
                {
                    WriteLogErrorLogin(System.Reflection.MethodBase.GetCurrentMethod().Name, CREDENCIALES_FO, APP_VERSION, "Usuario no existe");
                    return new ST_RDSUsuarioEx(-3);
                }

                Usuario = mResult.First();

                Guid TOKEN = Guid.NewGuid();
                string USER_AGENT = HttpContext.Current.Request.UserAgent;
                UAgent UA = GetUserAgentInfo(USER_AGENT);

                string IP_ADDRESS = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                sSQL = "Insert Into RADIO.TOKENS (TOKEN, USR_ID, APP_VERSION, IP_ADDRESS, USER_AGENT, OS, BROWSER) Values ('" + TOKEN + "', " + Usuario.USR_ID + ", '" + APP_VERSION + "', '" + IP_ADDRESS + "','" + USER_AGENT + "','" + UA.OS + "','" + UA.BROWSER + "')";

                bool bRet = cData.ClickExecuteNonQuery(sSQL);
                if (!bRet)
                    return new ST_RDSUsuarioEx(-1);

                Usuario.TOKEN = TOKEN;

                return Usuario;
            }

            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, CREDENCIALES_FO, "Exception e", e.Message);
                return new ST_RDSUsuarioEx(-1);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        private void ValidateIsConcesionario(ref csData cData, string USR_LOGIN)
        {
            string sSQL;

            if (cData.TraerValor("SELECT Count(*) FROM RADIO.USUARIOS WHERE USR_LOGIN = '" + USR_LOGIN + "'", 0) == 1)
                return;

            sSQL = @"
                    DECLARE @USR_LOGIN nvarchar(50)='" + USR_LOGIN + @"'
                    Select  CUS_ID, CUS_IDENT, CUS_NAME , CUS_EMAIL, CUS_BRANCH
                    From RADIO.V_CUSTOMERS
                    Where CUS_IDENT = @USR_LOGIN
                ";

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();


            var mResult = (from bp in dt
                           select new ST_RDSUsuario
                           {

                               USR_GROUP = (int)GROUPS.Concesionario,
                               USR_LOGIN = bp.Field<string>("CUS_IDENT"),
                               USR_NAME = bp.Field<string>("CUS_NAME"),
                               USR_ROLE = (int)ROLES.Concesionario,
                               USR_ASSIGNABLE = 0,
                               USR_STATUS = 1,
                               USR_EMAIL = bp.Field<string>("CUS_EMAIL"),
                               USR_HAS_SIGN = 0
                           }).ToList();

            if (mResult.Count != 1)
                return;

            ST_RDSUsuario Usuario = mResult.First();

            sSQL = "SELECT Top 0 USR_ID, USR_GROUP, USR_LOGIN, USR_PASSWORD, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL FROM RADIO.USUARIOS";

            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DTUsuario = dtSet.Tables[0];

            DataRow DRU = DTUsuario.NewRow();
            DRU["USR_GROUP"] = Usuario.USR_GROUP;
            DRU["USR_LOGIN"] = Usuario.USR_LOGIN;
            DRU["USR_PASSWORD"] = EncryptSHA256("TesAmerica" + USR_LOGIN);
            DRU["USR_NAME"] = Usuario.USR_NAME;
            DRU["USR_ROLE"] = Usuario.USR_ROLE;
            DRU["USR_ASSIGNABLE"] = Usuario.USR_ASSIGNABLE;
            DRU["USR_STATUS"] = Usuario.USR_STATUS;
            DRU["USR_EMAIL"] = Usuario.USR_EMAIL;
            DTUsuario.Rows.Add(DRU);
            cData.InsertDataSet(sSQL, dtSet);

            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = 1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Usuario);

            cData.InsertPortalEvent(1, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);
        }

        private void ValidateUserExists(ref csData cData, ST_MyEncrypt Credenciales)
        {
            string sSQL;

            if (cData.TraerValor("SELECT Count(*) FROM RADIO.USUARIOS WHERE USR_LOGIN = '" + Credenciales.Login + "'", 0) == 1)
                return;

  
            sSQL = "SELECT Top 0 USR_ID, USR_GROUP, USR_LOGIN, USR_PASSWORD, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL FROM RADIO.USUARIOS";

            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DTUsuario = dtSet.Tables[0];

            DataRow DRU = DTUsuario.NewRow();
            DRU["USR_GROUP"] = Credenciales.Group;
            DRU["USR_LOGIN"] = Credenciales.Login;
            DRU["USR_PASSWORD"] = EncryptSHA256("TesAmerica" + Credenciales.Login);
            DRU["USR_NAME"] = Credenciales.Login;
            DRU["USR_ROLE"] = ROLES.Funcionario;
            DRU["USR_ASSIGNABLE"] = 0;
            DRU["USR_STATUS"] = 1;
            DRU["USR_EMAIL"] = Credenciales.Email;
            DTUsuario.Rows.Add(DRU);
            cData.InsertDataSet(sSQL, dtSet);

            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = 1;
            string EVT_DATA_OLD = "";
            ST_RDSUsuario UserNew = new ST_RDSUsuario();
            UserNew.USR_ID = -1;
            UserNew.USR_GROUP = Credenciales.Group;
            UserNew.USR_LOGIN = Credenciales.Login;
            UserNew.USR_NAME = Credenciales.Login;
            UserNew.USR_EMAIL = Credenciales.Email;
            UserNew.USR_ROLE = (int)ROLES.Funcionario;
            UserNew.USR_ASSIGNABLE = 0;
            UserNew.USR_STATUS = 1;
            EVT_DATA_OLD = Serializer.Serialize(UserNew);

            string EVT_DATA_NEW = Serializer.Serialize(UserNew);

            cData.InsertPortalEvent(1, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

        }

        #endregion

        #region Usuarios

        public List<ST_RDSUsuario> GetUsuarios(int USR_ID_FILTER, int USR_GROUP_FILTER, int USR_ROLE_FILTER, int USR_STATUS_FILTER)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSUsuario> mResult = new List<ST_RDSUsuario>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        Declare @USR_ID_FILTER int = " + USR_ID_FILTER + @"
                        Declare @USR_GROUP_FILTER int = " + USR_GROUP_FILTER + @"
                        Declare @USR_ROLE_FILTER int = " + USR_ROLE_FILTER + @"
                        Declare @USR_STATUS_FILTER int = " + USR_STATUS_FILTER + @"

                        SELECT USR_ID, USR_GROUP, USR_LOGIN, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL, Case When USR_SIGN is null Then 0 else 1 end USR_HAS_SIGN
                        FROM RADIO.USUARIOS
                        WHERE (USR_ID = @USR_ID_FILTER Or @USR_ID_FILTER = -1) And (USR_GROUP & @USR_GROUP_FILTER <> 0 Or @USR_GROUP_FILTER = -1) And (USR_ROLE & @USR_ROLE_FILTER <> 0 Or @USR_ROLE_FILTER = -1) And (USR_STATUS & @USR_STATUS_FILTER <> 0)
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSUsuario
                            {
                                USR_ID = bp.Field<int>("USR_ID"),
                                USR_GROUP = bp.Field<int>("USR_GROUP"),
                                USR_LOGIN = bp.Field<string>("USR_LOGIN"),
                                USR_NAME = bp.Field<string>("USR_NAME"),
                                USR_ROLE = bp.Field<int>("USR_ROLE"),
                                USR_ASSIGNABLE = bp.Field<int>("USR_ASSIGNABLE"),
                                USR_STATUS = bp.Field<int>("USR_STATUS"),
                                USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                                USR_HAS_SIGN = bp.Field<int>("USR_HAS_SIGN")
                            }).ToList();

              
                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDUsuario(int IDUserWeb, ST_RDSUsuario User)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(User);

            var sLog = "IDUserWeb=" + IDUserWeb + ", User=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                int USR_ID = cData.TraerValor("Select USR_ID From RADIO.USUARIOS Where USR_LOGIN='" + User.USR_LOGIN + "'", -1);

                sSQL = @" 
                        SELECT USR_ID, USR_GROUP, USR_LOGIN, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL
                        FROM RADIO.USUARIOS
                        WHERE USR_ID=" + User.USR_ID + @";
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTUsuario = dtSet.Tables[0];

                if(User.USR_ID==-1) //Nuevo
                {
                    if (USR_ID != -1)
                        return new ActionInfo(2, "El login se encuentra en uso");
                    EVT_CRUD = 1;

                    DataRow DRU = DTUsuario.NewRow();
                    DRU["USR_GROUP"] = User.USR_GROUP;
                    DRU["USR_LOGIN"] = User.USR_LOGIN;
                    DRU["USR_NAME"] = User.USR_NAME;
                    DRU["USR_EMAIL"] = User.USR_EMAIL;
                    DRU["USR_ROLE"] = User.USR_ROLE;
                    DRU["USR_ASSIGNABLE"] = User.USR_ASSIGNABLE;
                    DRU["USR_STATUS"] = User.USR_STATUS;
                    DTUsuario.Rows.Add(DRU);
                }
                else //Edición
                {
                    if (USR_ID != -1 && USR_ID != User.USR_ID)
                        return new ActionInfo(2, "El login se encuentra en uso");
                    EVT_CRUD = 3;

                    DataRow DRU = DTUsuario.Rows[0];

                    ST_RDSUsuario UserOld = new ST_RDSUsuario();
                    UserOld.USR_ID = (int)DRU["USR_ID"];
                    UserOld.USR_GROUP = (int)DRU["USR_GROUP"];
                    UserOld.USR_LOGIN = (string)DRU["USR_LOGIN"];
                    UserOld.USR_NAME = (string)DRU["USR_NAME"];
                    UserOld.USR_EMAIL = (string)DRU["USR_EMAIL"];
                    UserOld.USR_ROLE = (int)DRU["USR_ROLE"];
                    UserOld.USR_ASSIGNABLE = (int)DRU["USR_ASSIGNABLE"];
                    UserOld.USR_STATUS = (int)DRU["USR_STATUS"];
                    EVT_DATA_OLD = Serializer.Serialize(UserOld);

                    DRU["USR_GROUP"] = User.USR_GROUP;
                    DRU["USR_LOGIN"] = User.USR_LOGIN;
                    DRU["USR_NAME"] = User.USR_NAME;
                    DRU["USR_EMAIL"] = User.USR_EMAIL;
                    DRU["USR_ROLE"] = User.USR_ROLE;
                    DRU["USR_ASSIGNABLE"] = User.USR_ASSIGNABLE;
                    DRU["USR_STATUS"] = User.USR_STATUS;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDUsuarioSignature(int IDUserWeb, ST_RDSUsuario User, string FileSignature)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(User);

            var sLog = "IDUserWeb=" + IDUserWeb + ", User=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                int USR_ID = cData.TraerValor("Select USR_ID From RADIO.USUARIOS Where USR_LOGIN='" + User.USR_LOGIN + "'", -1);

                sSQL = @" 
                        SELECT USR_ID, USR_SIGN
                        FROM RADIO.USUARIOS
                        WHERE USR_ID=" + User.USR_ID + @";
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTUsuario = dtSet.Tables[0];

                DataRow DRU = DTUsuario.Rows[0];
                EVT_CRUD = 1;

                DRU["USR_SIGN"] = Base64ToByteArray(FileSignature); 

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo ResetUsuarioPassword(int IDUserWeb, ST_RDSUsuario User)
        {
            csData cData = null;

            var Serializer = new JavaScriptSerializer_TESExtension();
            var sLog = "IDUserWeb=" + IDUserWeb + ", User=" + Serializer.Serialize(User);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                string USR_NEW_PASSWORD = CreatePassword(8);

                sSQL = @" 
                        SELECT USR_ID, USR_PASSWORD, USR_EMAIL
                        FROM RADIO.USUARIOS
                        WHERE USR_ID=" + User.USR_ID + @";
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTUsuario = dtSet.Tables[0];

                if (DTUsuario.Rows.Count!=1)
                    return new ActionInfo(2, "El usuario no existe");


                DataRow DRU = DTUsuario.Rows[0];
                DRU["USR_PASSWORD"] = EncryptSHA256(USR_NEW_PASSWORD);


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);
                Helper.AddKey("TO.USR_EMAIL", User.USR_EMAIL);
                Helper.AddKey("TO.USR_NAME", User.USR_NAME);
                Helper.AddKey("NEW_PASSWORD", USR_NEW_PASSWORD);

                ST_RDSSolicitud Solicitud = new ST_RDSSolicitud();

                Ret = EnviarCorreo(MAIL_TYPE.RESET_PASSWORD, ref Solicitud, ref Helper);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CambioUsuarioPassword(int IDUserWeb, ST_RDSUsuario User, string USR_NEW_PASSWORD)
        {
            csData cData = null;

            var Serializer = new JavaScriptSerializer_TESExtension();
            var sLog = "IDUserWeb=" + IDUserWeb + ", User=" + Serializer.Serialize(User) + ", USR_NEW_PASSWORD=" + USR_NEW_PASSWORD;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);


                sSQL = @" 
                        SELECT USR_ID, USR_PASSWORD, USR_EMAIL
                        FROM RADIO.USUARIOS
                        WHERE USR_ID=" + User.USR_ID + @";
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTUsuario = dtSet.Tables[0];

                if (DTUsuario.Rows.Count != 1)
                    return new ActionInfo(2, "El usuario no existe");


                DataRow DRU = DTUsuario.Rows[0];
                DRU["USR_PASSWORD"] = EncryptSHA256(USR_NEW_PASSWORD);


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

     
                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }


        #endregion

        #region Textos

        public List<ST_RDSText> GetTexts()
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSText> mResult = new List<ST_RDSText>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT TXT_ID, TXT_TYPE, TXT_GROUP, TXT_TEXT, TXT_SYSTEM From RADIO.TEXTS Order By 1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSText
                           {
                               TXT_ID = bp.Field<string>("TXT_ID"),
                               TXT_TYPE = bp.Field<string>("TXT_TYPE"),
                               TXT_GROUP = bp.Field<string>("TXT_GROUP"),
                               TXT_TEXT = bp.Field<string>("TXT_TEXT"),
                               TXT_SYSTEM = bp.Field<bool>("TXT_SYSTEM")
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDTexts(int IDUserWeb, ST_RDSText Text)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Text);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Text=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        SELECT TXT_ID, TXT_TYPE, TXT_GROUP, TXT_TEXT, TXT_SYSTEM From RADIO.TEXTS 
                        WHERE TXT_ID='" + Text.TXT_ID + "'";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTText = dtSet.Tables[0];

                switch (Text.TXT_CRUD)
                {
                    case 1:
                        {
                            if(DTText.Rows.Count!=0)
                                return new ActionInfo(2, "El key se encuentra en uso");
                            EVT_CRUD = 1;

                            DataRow DRT = DTText.NewRow();
                            DRT["TXT_ID"] = Text.TXT_ID;
                            DRT["TXT_TYPE"] = Text.TXT_TYPE;
                            DRT["TXT_GROUP"] = Text.TXT_GROUP;
                            DRT["TXT_TEXT"] = Text.TXT_TEXT;
                            DRT["TXT_SYSTEM"] = true;
                            DTText.Rows.Add(DRT);
                        }
                        break;
                    case 3:
                        {
                            if (DTText.Rows.Count != 1)
                                return new ActionInfo(2, "El key no existe");
                            EVT_CRUD = 3;

                            DataRow DRT = DTText.Rows[0];

                            ST_RDSText TextOld = new ST_RDSText();
                            TextOld.TXT_ID = (string)DRT["TXT_ID"];
                            TextOld.TXT_TYPE = (string)DRT["TXT_TYPE"];
                            TextOld.TXT_GROUP = (string)DRT["TXT_GROUP"];
                            TextOld.TXT_TEXT = (string)DRT["TXT_TEXT"];
                            TextOld.TXT_CRUD = 3;
                            EVT_DATA_OLD = Serializer.Serialize(TextOld);
                            
                            DRT["TXT_TYPE"] = Text.TXT_TYPE;
                            DRT["TXT_GROUP"] = Text.TXT_GROUP;
                            DRT["TXT_TEXT"] = Text.TXT_TEXT;
                        }
                        break;
                    case 4:
                        {
                            if (DTText.Rows.Count != 1)
                                return new ActionInfo(2, "El key no existe");
                            EVT_CRUD = 4;

                            DataRow DRT = DTText.Rows[0];

                            ST_RDSText TextOld = new ST_RDSText();
                            TextOld.TXT_ID = (string)DRT["TXT_ID"];
                            TextOld.TXT_TYPE = (string)DRT["TXT_TYPE"];
                            TextOld.TXT_GROUP = (string)DRT["TXT_GROUP"];
                            TextOld.TXT_TEXT = (string)DRT["TXT_TEXT"];
                            TextOld.TXT_CRUD = 4;
                            EVT_DATA_OLD = Serializer.Serialize(TextOld);
                            EVT_DATA_NEW = "";

                            DRT.Delete();
                        }
                        break;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }
        public ActionInfo CRUDDoc(int IDUserWeb, ST_RDSDoc Docum)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Docum);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Docum=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;
                string msg ="";

                ValidateToken(ref cData);

                sSQL = @" 
                        SELECT ID_ANX_SOL, TIPO_ANX_SOL, DES_TIPO_ANX_SOL, EXTENSIONS, MAX_SIZE, MAX_NAME_LENGTH, ANX_SYSTEM, ANX_OTORGA From RADIO.TIPO_ANEXO_SOL 
                        WHERE ID_ANX_SOL = " + Docum.ID_ANX_SOL + @";
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTDocum = dtSet.Tables[0];

                switch (Docum.DOC_CRUD)
                {
                    case 1:
                        {
                            if (DTDocum.Rows.Count != 0)
                                return new ActionInfo(2, "El documento ya se encuentra registrado");
                            EVT_CRUD = 1;

                            DataRow DRD = DTDocum.NewRow();
                            DRD["TIPO_ANX_SOL"] = Docum.TIPO_ANX_SOL;
                            DRD["DES_TIPO_ANX_SOL"] = Docum.DES_TIPO_ANX_SOL;
                            DRD["EXTENSIONS"] = Docum.EXTENSIONS;
                            DRD["MAX_SIZE"] = Docum.MAX_SIZE;
                            DRD["MAX_NAME_LENGTH"] = Docum.MAX_NAME_LENGTH;
                            DRD["ANX_SYSTEM"] = true;
                            DRD["ANX_OTORGA"] = false;

                            DTDocum.Rows.Add(DRD);
                            msg = "El documento se creó correctamente";
                        }
                        break;
                    case 3:
                        {
                            if (DTDocum.Rows.Count != 1)
                                return new ActionInfo(2, "El documento no existe");
                            EVT_CRUD = 3;

                            DataRow DRD = DTDocum.Rows[0];

                            ST_RDSDoc DocOld = new ST_RDSDoc();
                            DocOld.ID_ANX_SOL = (int)DRD["ID_ANX_SOL"];
                            DocOld.TIPO_ANX_SOL = (string)DRD["TIPO_ANX_SOL"];
                            DocOld.DES_TIPO_ANX_SOL = (string)DRD["DES_TIPO_ANX_SOL"];
                            DocOld.EXTENSIONS = (string)DRD["EXTENSIONS"];
                            DocOld.MAX_SIZE = (int)DRD["MAX_SIZE"];
                            DocOld.MAX_NAME_LENGTH = (int)DRD["MAX_NAME_LENGTH"];
                            EVT_DATA_OLD = Serializer.Serialize(DocOld);

                            DRD["TIPO_ANX_SOL"] = Docum.TIPO_ANX_SOL;
                            DRD["DES_TIPO_ANX_SOL"] = Docum.DES_TIPO_ANX_SOL;
                            DRD["EXTENSIONS"] = Docum.EXTENSIONS;
                            DRD["MAX_SIZE"] = Docum.MAX_SIZE;
                            DRD["MAX_NAME_LENGTH"] = Docum.MAX_NAME_LENGTH;
                            DRD["ANX_SYSTEM"] = true;
                            DRD["ANX_OTORGA"] = false;

                            msg = "El documento se editó correctamente";
                        }
                        break;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, msg);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDComen(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSComen Comen)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Comen);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Comen=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        SELECT COMMENT, SOL_UID From RADIO.SOLICITUDES 
                        WHERE SERV_NUMBER='" + Solicitud.Expediente.SERV_NUMBER + "'";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTComen = dtSet.Tables[0];
                DataRow DRCO = DTComen.Rows[0];
                DRCO["COMMENT"] = Comen.COMMENT;


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Eventos

        public List<ST_RDSEvent> GetEvents(string DateIni, string DateEnd)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSEvent> mResult = new List<ST_RDSEvent>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        DECLARE @DateIni date='" + DateIni + @"'
                        DECLARE @DateEnd date='" + DateEnd + @"'
                        SELECT EVT_ID, EVT.USR_ID, isnull(USR.USR_NAME,'') USR_NAME, EVT_FUNCTION, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW, EVT_CREATED
                        From RADIO.EVENTS EVT
                        left join RADIO.USUARIOS USR On EVT.USR_ID=USR.USR_ID
                        Where Convert(date,EVT.EVT_CREATED) BETWEEN @DateIni And @DateEnd
                        Order By 1 desc
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSEvent
                           {
                               EVT_ID = bp.Field<int>("EVT_ID"),
                               USR_ID = bp.Field<int>("USR_ID"),
                               USR_NAME = bp.Field<string>("USR_NAME"),
                               EVT_CRUD = bp.Field<int>("EVT_CRUD"),
                               EVT_FUNCTION = bp.Field<string>("EVT_FUNCTION"),
                               EVT_DATA_OLD = bp.Field<string>("EVT_DATA_OLD"),
                               EVT_DATA_NEW = bp.Field<string>("EVT_DATA_NEW"),
                               EVT_CREATED = bp.Field<DateTime>("EVT_CREATED").ToString("yyyy/MM/dd HH:mm:ss")
                           }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Conexiones

        public List<ST_RDSConectionHeader> GetConectionsHeader(string DateIni, string DateEnd)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSConectionHeader> mResult = new List<ST_RDSConectionHeader>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        DECLARE @DateIni date='" + DateIni + @"'
                        DECLARE @DateEnd date='" + DateEnd + @"'
                        Select *
                        From 
                        (
	                        Select USR.USR_ID USR_ID, Max(USR_GROUP) USR_GROUP,  Max(USR_ROLE) USR_ROLE, Max(USR_NAME)USR_NAME, Max(USR_LOGIN)USR_LOGIN, Max(USR_EMAIL)USR_EMAIL, Count(TOK.ID) USR_CONECTIONS,
	                        Sum(isnull(DateDiff(s,TOK.CREATED,TOK.MODIFIED),0))USR_DURATION
	                        From RADIO.USUARIOS USR
	                        Join RADIO.TOKENS TOK On USR.USR_ID=TOK.USR_ID And convert(date, TOK.CREATED) BETWEEN @DateIni And @DateEnd
	                        Where USR.USR_STATUS=1
	                        Group By USR.USR_ID
                        )T1
                        Join
                        (
	                        Select USR_ID, USR_LAST_CONECTION, USR_OS, USR_BROWSER, APP_VERSION
	                        From
	                        (
		                        Select USR_ID, TOK.ID, TOK.OS USR_OS, TOK.BROWSER USR_BROWSER, APP_VERSION, ROW_NUMBER() Over (partition by USR_ID order by TOK.ID desc) RowNum, TOK.CREATED USR_LAST_CONECTION
		                        From RADIO.TOKENS TOK
		                        Where convert(date, TOK.CREATED) BETWEEN @DateIni And @DateEnd
	                        )T 
	                        Where T.RowNum=1
                        )T2 On T1.USR_ID=T2.USR_ID
                        Order By T1.USR_NAME
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSConectionHeader
                           {
                               USR_ID = bp.Field<int>("USR_ID"),
                               USR_GROUP = bp.Field<int>("USR_GROUP"),
                               USR_ROLE = bp.Field<int>("USR_ROLE"),
                               USR_NAME = bp.Field<string>("USR_NAME"),
                               USR_LOGIN = bp.Field<string>("USR_LOGIN"),
                               USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                               USR_CONECTIONS = bp.Field<int>("USR_CONECTIONS"),
                               USR_DURATION = TimeSpan.FromSeconds(bp.Field<int>("USR_DURATION")).ToString(@"d\ \d\í\a\s\ hh\:mm\:ss"),
                               USR_LAST_CONECTION = bp.Field<DateTime?>("USR_LAST_CONECTION").HasValue ? bp.Field<DateTime>("USR_LAST_CONECTION").ToString("yyyy/MM/dd HH:mm:ss") : "",
                               USR_OS = bp.Field<string>("USR_OS"),
                               USR_BROWSER = bp.Field<string>("USR_BROWSER"),
                               APP_VERSION = bp.Field<string>("APP_VERSION") 
                           }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_RDSConectionDetails> GetConectionsDetails(int USR_ID, string DateIni, string DateEnd)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSConectionDetails> mResult = new List<ST_RDSConectionDetails>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        DECLARE @USR_ID int=" + USR_ID + @"
                        DECLARE @DateIni date='" + DateIni + @"'
                        DECLARE @DateEnd date='" + DateEnd + @"'
                        Select TOK.ID TOK_ID , USR_NAME, TOK.CREATED TOK_CREATED, TOK.NCALLS TOK_NCALLS, DateDiff(s,TOK.CREATED,TOK.MODIFIED) TOK_DURATION, TOK.IP_ADDRESS TOK_IP_ADDRESS, TOK.OS TOK_OS, TOK.BROWSER TOK_BROWSER, APP_VERSION
                        From RADIO.USUARIOS USR
                        Join RADIO.TOKENS TOK On USR.USR_ID=TOK.USR_ID And convert(date, TOK.CREATED) BETWEEN @DateIni And @DateEnd
                        Where USR.USR_ID=@USR_ID
                        Order By TOK_CREATED
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSConectionDetails
                           {
                               TOK_ID = bp.Field<int>("TOK_ID"),
                               USR_NAME = bp.Field<string>("USR_NAME"),
                               TOK_CREATED = bp.Field<DateTime>("TOK_CREATED").ToString("yyyy/MM/dd HH:mm:ss"),
                               TOK_NCALLS = bp.Field<int>("TOK_NCALLS"),
                               TOK_DURATION = TimeSpan.FromSeconds(bp.Field<int>("TOK_DURATION")).ToString(@"hh\:mm\:ss"),
                               TOK_IP_ADDRESS = bp.Field<string>("TOK_IP_ADDRESS"),
                               TOK_OS = bp.Field<string>("TOK_OS"),
                               TOK_BROWSER = bp.Field<string>("TOK_BROWSER"),
                               APP_VERSION = bp.Field<string>("APP_VERSION") 
                           }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Alarms

        public List<ST_RDSAlarm> GetAlarms()
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSAlarm> mResult = new List<ST_RDSAlarm>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT ALARM_STEP, ALARM_DESCRIPTION, ALARM_ICON, ALARM_PRE_EXPIRE, ALARM_EXPIRE From RADIO.ALARMS Order By 1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSAlarm
                           {
                               ALARM_STEP = bp.Field<int>("ALARM_STEP"),
                               ALARM_DESCRIPTION = bp.Field<string>("ALARM_DESCRIPTION"),
                               ALARM_ICON = bp.Field<string>("ALARM_ICON"),
                               ALARM_PRE_EXPIRE = bp.Field<int>("ALARM_PRE_EXPIRE"),
                               ALARM_EXPIRE = bp.Field<int>("ALARM_EXPIRE")
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDAlarms(int IDUserWeb, ST_RDSAlarm Alarm)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            
            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Alarm);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Alarm=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT ALARM_STEP, ALARM_DESCRIPTION, ALARM_PRE_EXPIRE, ALARM_EXPIRE 
                        From RADIO.ALARMS 
                        WHERE ALARM_STEP=" + Alarm.ALARM_STEP;

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTAlarm = dtSet.Tables[0];

                if (DTAlarm.Rows.Count != 1)
                    return new ActionInfo(2, "El tipo de alarma no existe");
                EVT_CRUD = 3;

                DataRow DRA = DTAlarm.Rows[0];
                
                ST_RDSAlarm AlarmOld = new ST_RDSAlarm();
                AlarmOld.ALARM_STEP = (int)DRA["ALARM_STEP"];
                AlarmOld.ALARM_DESCRIPTION = (string)DRA["ALARM_DESCRIPTION"];
                AlarmOld.ALARM_PRE_EXPIRE = (int)DRA["ALARM_PRE_EXPIRE"];
                AlarmOld.ALARM_EXPIRE = (int)DRA["ALARM_EXPIRE"];
                EVT_DATA_OLD = Serializer.Serialize(AlarmOld);

                DRA["ALARM_DESCRIPTION"] = Alarm.ALARM_DESCRIPTION;
                DRA["ALARM_PRE_EXPIRE"] = Alarm.ALARM_PRE_EXPIRE;
                DRA["ALARM_EXPIRE"] = Alarm.ALARM_EXPIRE;

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Imagenes

        public List<ST_RDSImage> GetImages()
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSImage> mResult = new List<ST_RDSImage>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT IMG_ID, IMG_CONTENT_TYPE, IMG_SYSTEM, IMG_FILE From RADIO.IMAGES Order By 1";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSImage
                           {

                               IMG_ID = bp.Field<string>("IMG_ID"),
                               IMG_CONTENT_TYPE = bp.Field<string>("IMG_CONTENT_TYPE"),
                               IMG_SYSTEM = bp.Field<bool>("IMG_SYSTEM"),
                               IMG_CRUD = -1,
                               IMG_FILE = "data:" + bp.Field<string>("IMG_CONTENT_TYPE")  + ";base64, " + Convert.ToBase64String(bp.Field<byte[]>("IMG_FILE"))
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDImages(int IDUserWeb, ST_RDSImage Image)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            ST_RDSImageEvent ImageEvt = new ST_RDSImageEvent(Image);
            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(ImageEvt);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Image=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT IMG_ID, IMG_CONTENT_TYPE, IMG_SYSTEM, IMG_FILE 
                        From RADIO.IMAGES 
                        WHERE IMG_ID='" + Image.IMG_ID + "'";



                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTImage = dtSet.Tables[0];

                switch (Image.IMG_CRUD)
                {
                    case 1:
                        {
                            if (DTImage.Rows.Count != 0)
                                return new ActionInfo(2, "El nombre de la imagen se encuentra en uso");
                            EVT_CRUD = 1;

                            DataRow DRI = DTImage.NewRow();
                            DRI["IMG_ID"] = Image.IMG_ID;
                            DRI["IMG_CONTENT_TYPE"] = Image.IMG_CONTENT_TYPE;
                            DRI["IMG_SYSTEM"] = false;
                            DRI["IMG_FILE"] = Base64ToByteArray(Image.IMG_FILE); 
                            DTImage.Rows.Add(DRI);
                        }
                        break;
                    case 3:
                        {
                            if (DTImage.Rows.Count != 1)
                                return new ActionInfo(2, "El nombre de la imagen no existe");
                            EVT_CRUD = 3;

                            DataRow DRI = DTImage.Rows[0];

                            ST_RDSImageEvent ImageOld = new ST_RDSImageEvent();
                            ImageOld.IMG_ID = (string)DRI["IMG_ID"];
                            ImageOld.IMG_CONTENT_TYPE = (string)DRI["IMG_CONTENT_TYPE"];
                            ImageOld.IMG_SYSTEM = (bool)DRI["IMG_SYSTEM"];
                            ImageOld.IMG_FILE_SIZE = ((byte[])DRI["IMG_FILE"]).Length;
                            ImageOld.IMG_CRUD = 3;
                            EVT_DATA_OLD = Serializer.Serialize(ImageOld);

                            DRI["IMG_CONTENT_TYPE"] = Image.IMG_CONTENT_TYPE;
                            DRI["IMG_FILE"] = Base64ToByteArray(Image.IMG_FILE); 
                        }
                        break;
                    case 4:
                        {
                            if (DTImage.Rows.Count != 1)
                                return new ActionInfo(2, "El nombre de la imagen no existe");
                            EVT_CRUD = 4;

                            DataRow DRI = DTImage.Rows[0];

                            ST_RDSImageEvent ImageOld = new ST_RDSImageEvent();
                            ImageOld.IMG_ID = (string)DRI["IMG_ID"];
                            ImageOld.IMG_CONTENT_TYPE = (string)DRI["IMG_CONTENT_TYPE"];
                            ImageOld.IMG_SYSTEM = (bool)DRI["IMG_SYSTEM"];
                            ImageOld.IMG_FILE_SIZE = ((byte[])DRI["IMG_FILE"]).Length;
                            ImageOld.IMG_CRUD = 4;
                            EVT_DATA_OLD = Serializer.Serialize(ImageOld);

                            DRI.Delete();
                        }
                        break;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Resoluciones plantillas

        public ST_RDSResCombos GetResolucionesCombos()
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSResCombos mResult = new ST_RDSResCombos();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT TXT_ID, TXT_TEXT
                        FROM RADIO.TEXTS
                        WHERE TXT_ID In ('COMBO_INFORMACION_ADMINISTRATIVA', 'COMBO_INFORMACION_CONCESIONARIO', 'COMBO_INFORMACION_RADICADOS', 'COMBO_INFORMACION_RESOLUCION', 'COMBO_INFORMACION_TECNICA', 'COMBO_INFORMACION_ANTECEDENTES')
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult.InformacionAdministrativa=dt.Where(bp=>bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_ADMINISTRATIVA")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionConcesionario=dt.Where(bp=>bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_CONCESIONARIO")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionRadicados=dt.Where(bp=>bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_RADICADOS")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionResolucion=dt.Where(bp=>bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_RESOLUCION")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionTecnica=dt.Where(bp=>bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_TECNICA")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionAntecedentes = dt.Where(bp => bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_ANTECEDENTES")).FirstOrDefault().Field<string>("TXT_TEXT");

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_RDSResPlt> GetResolucionesPlantillas(int SOL_TYPE_ID_FILTER, int RES_PLT_STATUS_FILTER)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSResPlt> mResult = new List<ST_RDSResPlt>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Declare @SOL_TYPE_ID_FILTER int = " + SOL_TYPE_ID_FILTER + @"
                        Declare @RES_PLT_STATUS_FILTER int = " + RES_PLT_STATUS_FILTER + @"
                        SELECT RES_PLT_UID, RES_PLT_NAME, RES_PLT.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, RES_PLT_PARSER, RES_PLT_CONTENT, RES_PLT_CODIGO, RES_PLT_VERSION, RES_PLT_STATUS
                        FROM RADIO.RESOLUCION_PLANTILLAS RES_PLT
                        JOIN RADIO.TIPOS_SOL TSOL on RES_PLT.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
                        WHERE (RES_PLT.SOL_TYPE_ID = @SOL_TYPE_ID_FILTER Or @SOL_TYPE_ID_FILTER = -1) And (RES_PLT.RES_PLT_STATUS = @RES_PLT_STATUS_FILTER Or @RES_PLT_STATUS_FILTER = -1)
                        ORDER By RES_PLT_NAME";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSResPlt
                           {
                               RES_PLT_UID = bp.Field<Guid>("RES_PLT_UID"),
                               RES_PLT_NAME = bp.Field<string>("RES_PLT_NAME"),
                               SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                               SOL_TYPE_NAME= bp.Field<string>("SOL_TYPE_NAME"),
                               RES_PLT_CONTENT = bp.Field<string>("RES_PLT_CONTENT"),
                               RES_PLT_PARSER = bp.Field<string>("RES_PLT_PARSER"),
                               RES_PLT_VERSION = bp.Field<string>("RES_PLT_VERSION"),
                               RES_PLT_CODIGO = bp.Field<string>("RES_PLT_CODIGO"),
                               RES_PLT_STATUS = bp.Field<int>("RES_PLT_STATUS")
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDResolucionesPlantillas(int IDUserWeb, ST_RDSResPlt ResPlt)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(ResPlt);

            var sLog = "IDUserWeb=" + IDUserWeb + ", ResPlt=" + EVT_DATA_NEW;


            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT RES_PLT_UID, RES_PLT_NAME, SOL_TYPE_ID, RES_PLT_PARSER, RES_PLT_CONTENT, RES_PLT_CODIGO, RES_PLT_VERSION, RES_PLT_STATUS, RES_PLT_PDF_FILE
                        FROM RADIO.RESOLUCION_PLANTILLAS 
                        WHERE RES_PLT_UID='" + ResPlt.RES_PLT_UID + "'";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTResPlt = dtSet.Tables[0];

                switch (ResPlt.RES_PLT_CRUD)
                {
                    case 1:
                        {
                            if (DTResPlt.Rows.Count != 0)
                                return new ActionInfo(2, "El key se encuentra en uso");
                            EVT_CRUD = 1;
                            ResPlt.RES_PLT_UID = Guid.NewGuid();

                            ST_Helper Helper = new ST_Helper();
                            Helper.TXT = GetDinamicTexts(ref cData);
                            Helper.IMG = GetDinamicImages(ref cData);
                            ST_RDSSolicitud Solicitud = new ST_RDSSolicitud();
                            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                            RDSResolucionPDF RDSResolucion = new RDSResolucionPDF(ref ResPlt, ref DynamicSolicitud);

                            MemoryStream mem = RDSResolucion.GenerarResolucion("Plantilla " + ResPlt.RES_PLT_NAME, ResPlt.SOL_TYPE_NAME + " " + ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");

                            DataRow DRT = DTResPlt.NewRow();
                            DRT["RES_PLT_UID"] = ResPlt.RES_PLT_UID;
                            DRT["RES_PLT_NAME"] = ResPlt.RES_PLT_NAME;
                            DRT["SOL_TYPE_ID"] = ResPlt.SOL_TYPE_ID;
                            DRT["RES_PLT_PARSER"] = ResPlt.RES_PLT_PARSER;
                            DRT["RES_PLT_CODIGO"] = ResPlt.RES_PLT_CODIGO;
                            DRT["RES_PLT_VERSION"] = ResPlt.RES_PLT_VERSION;
                            DRT["RES_PLT_STATUS"] = ResPlt.RES_PLT_STATUS;
                            DRT["RES_PLT_CONTENT"] = ResPlt.RES_PLT_CONTENT;
                            DRT["RES_PLT_PDF_FILE"] = mem.GetBuffer();
                            DTResPlt.Rows.Add(DRT);
                        }
                        break;

                    case 3:
                        {
                            if (DTResPlt.Rows.Count != 1)
                                return new ActionInfo(2, "El key no existe");
                            EVT_CRUD = 3;

                            ST_Helper Helper = new ST_Helper();
                            Helper.TXT = GetDinamicTexts(ref cData);
                            Helper.IMG = GetDinamicImages(ref cData);
                            ST_RDSSolicitud Solicitud = new ST_RDSSolicitud();
                            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                            RDSResolucionPDF RDSResolucion = new RDSResolucionPDF(ref ResPlt, ref DynamicSolicitud);
                            MemoryStream mem = RDSResolucion.GenerarResolucion("Plantilla " + ResPlt.RES_PLT_NAME, ResPlt.SOL_TYPE_NAME + " " + ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");

                            DataRow DRT = DTResPlt.Rows[0];

                            ST_RDSResPlt ResPltOld = new ST_RDSResPlt();
                            ResPltOld.RES_PLT_UID = (Guid)DRT["RES_PLT_UID"];
                            ResPltOld.RES_PLT_NAME = (string)DRT["RES_PLT_NAME"];
                            ResPltOld.SOL_TYPE_ID = (int)DRT["SOL_TYPE_ID"];
                            ResPltOld.RES_PLT_PARSER = (string)DRT["RES_PLT_PARSER"];
                            ResPltOld.RES_PLT_CODIGO = (string)DRT["RES_PLT_CODIGO"];
                            ResPltOld.RES_PLT_VERSION = (string)DRT["RES_PLT_VERSION"];
                            ResPltOld.RES_PLT_STATUS = (int)DRT["RES_PLT_STATUS"];
                            ResPltOld.RES_PLT_CONTENT = (string)DRT["RES_PLT_CONTENT"];
                            ResPltOld.RES_PLT_CRUD = 3;
                            EVT_DATA_OLD = Serializer.Serialize(ResPltOld);

                            DRT["RES_PLT_NAME"] = ResPlt.RES_PLT_NAME;
                            DRT["SOL_TYPE_ID"] = ResPlt.SOL_TYPE_ID;
                            DRT["RES_PLT_PARSER"] = ResPlt.RES_PLT_PARSER;
                            DRT["RES_PLT_CODIGO"] = ResPlt.RES_PLT_CODIGO;
                            DRT["RES_PLT_VERSION"] = ResPlt.RES_PLT_VERSION;
                            DRT["RES_PLT_STATUS"] = ResPlt.RES_PLT_STATUS;
                            DRT["RES_PLT_CONTENT"] = ResPlt.RES_PLT_CONTENT;
                            DRT["RES_PLT_PDF_FILE"] = mem.GetBuffer();
                        }
                        break;

                    case 4:
                        {
                            if (DTResPlt.Rows.Count != 1)
                                return new ActionInfo(2, "El key no existe");
                            EVT_CRUD = 4;

                            DataRow DRT = DTResPlt.Rows[0];

                            ST_RDSResPlt ResPltOld = new ST_RDSResPlt();
                            ResPltOld.RES_PLT_UID = (Guid)DRT["RES_PLT_UID"];
                            ResPltOld.RES_PLT_NAME = (string)DRT["RES_PLT_NAME"];
                            ResPltOld.SOL_TYPE_ID = (int)DRT["SOL_TYPE_ID"];
                            ResPltOld.RES_PLT_PARSER = (string)DRT["RES_PLT_PARSER"];
                            ResPltOld.RES_PLT_CODIGO = (string)DRT["RES_PLT_CODIGO"];
                            ResPltOld.RES_PLT_VERSION = (string)DRT["RES_PLT_VERSION"];
                            ResPltOld.RES_PLT_STATUS = (int)DRT["RES_PLT_STATUS"];
                            ResPltOld.RES_PLT_CONTENT = (string)DRT["RES_PLT_CONTENT"];
                            ResPltOld.RES_PLT_CRUD = 4;
                            EVT_DATA_OLD = Serializer.Serialize(ResPltOld);

                            DRT.Delete();
                        }
                        break;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Resoluciones

        public ST_RDSResPlt ReplaceResolucionPlantilla(ST_RDSResPlt ResPlt, ST_RDSSolicitud Solicitud)
        {
            int IDUserWeb = 1;
            csData cData = null;

            ST_RDSResPlt mResult = new ST_RDSResPlt();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = cData.TraerValor("Select TXT_TEXT From RADIO.TEXTS Where TXT_ID='" + ResPlt.RES_PLT_PARSER + "'", "RESOLUCION_SQL_DEFAULT");
                sSQL = sSQL.Replace("{{SOL_UID}}", Solicitud.SOL_UID.ToString());
                sSQL = sSQL.Replace("{{SERV_ID}}", Solicitud.Expediente.SERV_ID.ToString());

                var dtSet = cData.ObtenerDataSet(sSQL);

                RDSDynamicResolucion DynamicResolucion = new RDSDynamicResolucion();

                foreach (DataTable Tabla in dtSet.Tables)
                {
                    if (Tabla.Columns[0].ColumnName.StartsWith("{{LISTA "))
                        DynamicResolucion.AddKey(Tabla.Columns[0].ColumnName, Tabla.Rows.Count.ToString());

                    foreach (DataColumn Columna in Tabla.Columns)
                    {
                        if (Tabla.Columns[0].ColumnName.StartsWith("{{LISTA "))
                        {
                            if (Columna.ColumnName == Tabla.Columns[0].ColumnName)
                                continue;
                        }

                        int Contador = 0;
                        foreach (DataRow Fila in Tabla.Rows)
                        {
                            if (Tabla.Columns[0].ColumnName.StartsWith("{{LISTA "))
                                DynamicResolucion.AddKey(Columna.ColumnName + "[" + Contador + "]", Fila[Columna].ToString());
                            else
                                DynamicResolucion.AddKey(Columna.ColumnName, Fila[Columna].ToString());

                            Contador++;
                        }
                    }
                }

                ResPlt.RES_PLT_CONTENT = DynamicResolucion.ProcesarResolucionesListas(ResPlt.RES_PLT_CONTENT);

                foreach (var Item in DynamicResolucion.TXT){
                    ResPlt.RES_PLT_CONTENT = ResPlt.RES_PLT_CONTENT.Replace(Item.Key, Item.Value);
                }

                return ResPlt;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDResolucion(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSResolucion Resolucion)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", Resolucion=" + Serializer.Serialize(Resolucion);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT SOL_UID, RES_NAME, RES_CONTENT, RES_CODIGO, RES_VERSION, RES_IS_CREATED, RES_CREATED_DATE, RES_MODIFIED_DATE, 
                        RES_TRACKING_COUNT, RES_PDF_FILE, RES_WORD_FILE
                        FROM RADIO.RESOLUCIONES 
                        WHERE SOL_UID='" + Resolucion.SOL_UID + "'";

                var dtSet = cData.ObtenerDataSet(sSQL);
                DataTable DTResolucion = dtSet.Tables[0];

                switch (DTResolucion.Rows.Count)
                {
                    case 1:
                        {
                            ST_Helper Helper = new ST_Helper();
                            Helper.TXT = GetDinamicTexts(ref cData);
                            Helper.IMG = GetDinamicImages(ref cData);
                            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                            ST_RDSResPlt ResPlt = new ST_RDSResPlt(Resolucion);
                            RDSResolucionPDF RDSResolucion = new RDSResolucionPDF(ref ResPlt, ref DynamicSolicitud);
                            MemoryStream mem = RDSResolucion.GenerarResolucion(Resolucion.RES_NAME, ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");
                            MemoryStream mem2 = RDSResolucion.GenerarResolucionWord(Resolucion.RES_NAME, ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");


                            DataRow DRR = DTResolucion.Rows[0];
                            DRR["RES_NAME"] = Resolucion.RES_NAME;
                            DRR["RES_CONTENT"] = Resolucion.RES_CONTENT;
                            DRR["RES_CODIGO"] = Resolucion.RES_CODIGO;
                            DRR["RES_VERSION"] = Resolucion.RES_VERSION;
                            DRR["RES_IS_CREATED"] = true;
                            if(!Resolucion.RES_IS_CREATED)
                                DRR["RES_CREATED_DATE"] = DateTime.Now;
                            DRR["RES_MODIFIED_DATE"] = DateTime.Now;
                            DRR["RES_TRACKING_COUNT"] = Resolucion.RES_TRACKING_COUNT;
                            DRR["RES_PDF_FILE"] = mem.GetBuffer();
                            string strbase64 = Convert.ToBase64String(mem2.ToArray());
                            byte[] bytes = Convert.FromBase64String(strbase64);
                            DRR["RES_WORD_FILE"] = bytes;

                            Resolucion.RES_CREATED_DATE = ((DateTime)DRR["RES_CREATED_DATE"]).ToString("yyyyMMdd");
                            Resolucion.RES_MODIFIED_DATE = ((DateTime)DRR["RES_MODIFIED_DATE"]).ToString("yyyyMMdd");
                            Resolucion.RES_IS_CREATED = true;
                        }
                        break;

                    default:
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Varias resoluciones para una misma solicitud: " + DTResolucion.Rows.Count, sLog);
                        return new ActionInfo(2, "Error en la operación");
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                return new ActionInfo(1, "Operación exitosa.", Resolucion);
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSResolucion GetResolucion(Guid SOL_UID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSResolucion mResult = new ST_RDSResolucion();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT SOL_UID, RES_NAME, RES_CONTENT, RES_CODIGO, RES_VERSION, RES_IS_CREATED, RES_CREATED_DATE, RES_MODIFIED_DATE, RES_TRACKING_COUNT
                        FROM RADIO.RESOLUCIONES 
                        WHERE SOL_UID='" + SOL_UID + "'";


                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSResolucion
                           {
                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                               RES_NAME = bp.Field<string>("RES_NAME"),
                               RES_CONTENT = bp.Field<string>("RES_CONTENT"),
                               RES_CODIGO = bp.Field<string>("RES_CODIGO"),
                               RES_VERSION = bp.Field<string>("RES_VERSION"),
                               RES_IS_CREATED = bp.Field<bool>("RES_IS_CREATED"),
                               RES_CREATED_DATE = bp.Field<DateTime>("RES_CREATED_DATE").Date.ToString("yyyyMMdd"),
                               RES_MODIFIED_DATE = bp.Field<DateTime>("RES_MODIFIED_DATE").Date.ToString("yyyyMMdd"),
                               RES_TRACKING_COUNT = bp.Field<int>("RES_TRACKING_COUNT")
                           }).ToList().FirstOrDefault();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Conteos

        public List<ST_RDSConteoDevoluciones> GetConteoDevoluciones(int USR_GROUP, int USR_ROLE, string DateIni, string DateEnd)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSConteoDevoluciones> mResult = new List<ST_RDSConteoDevoluciones>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        DECLARE @USR_GROUP int=" + USR_GROUP + @"
                        DECLARE @USR_ROLE int=" + USR_ROLE + @"
                        DECLARE @DateIni date='" + DateIni + @"'
                        DECLARE @DateEnd date='" + DateEnd + @"'

                        SELECT USR.USR_ID, Max(USR_NAME) USR_NAME, Count(CON.USR_ID) CONTEO
                        FROM RADIO.USUARIOS USR
                        LEFT JOIN RADIO.CONTEOS CON On USR.USR_ID=CON.USR_ID And CON.USR_GROUP=@USR_GROUP And convert(date,CON.CON_DATE) BETWEEN @DateIni And @DateEnd 
                        WHERE USR.USR_GROUP=@USR_GROUP And USR.USR_ROLE & @USR_ROLE <> 0 And USR.USR_STATUS=1
                        GROUP BY USR.USR_ID                        
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSConteoDevoluciones
                           {
                               USR_ID = bp.Field<int>("USR_ID"),
                               USR_NAME = bp.Field<string>("USR_NAME"),
                               CONTEO = bp.Field<int>("CONTEO")
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        #endregion

        #region Análisis

        public ActionInfo GuardarTramiteAdministrativo(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado)
        {
            string registroNumber = "";
            List<ST_RDSUsuario> AsignacionesGroup2 = new List<ST_RDSUsuario>();
            ST_RDSCurrent CURRENT = new ST_RDSCurrent();
            ST_Helper Helper = new ST_Helper();
            AZTools AZTools = new AZTools();
            ST_RDSComunicado Comunicado = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Administrativo);
            ROLE248_STATE STATE248 = GetRole248State(ref Solicitud, ROLE);
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            string sSQL="";
            var sLog = "IDUserWeb=" + IDUserWeb + ", ROLE=" + ROLE + ", Solicitud=" + Serializer.Serialize(Solicitud) + ", bGenerarComunicado=" + bGenerarComunicado;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                

                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_EMAIL, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE,
                        FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE, 
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED
                        From RADIO.SOLICITUDES Where SOL_UID='" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT ANX_UID, ANX_STATE_ID, ANX_COMMENT, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        ANX_ROLE1_COMMENT, ANX_ROLE2_COMMENT, ANX_ROLE4_COMMENT, ANX_ROLE8_COMMENT
                        FROM RADIO.ANEXOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';
                
                        SELECT SOL_UID, COM_UID, COM_CLASS, COM_TYPE, COM_NAME, COM_DATE, COM_FILE
                        FROM RADIO.COMUNICADOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"' And COM_CLASS In (1,3);

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        SELECT Top 0 CON_ID, SOL_UID, USR_ID, USR_GROUP, USR_ROLE
                        FROM RADIO.CONTEOS;

                        SELECT SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID 
                        FROM RADIO.SOL_USERS 
                        WHERE SOL_UID='" + Solicitud.SOL_UID.ToString() + @"'
                        Order By USR_GROUP;

                        Select SOL_UID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLE_MINTIC_COMMENT, TECH_ROLEX_CHANGED
		                FROM RADIO.TECH_ANALISIS 
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS 
		                WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT SOL_UID, RES_NAME, RES_CONTENT, RES_CODIGO, RES_VERSION, RES_IS_CREATED, RES_CREATED_DATE, RES_MODIFIED_DATE, 
                        RES_TRACKING_COUNT, RES_PDF_FILE, RES_WORD_FILE, NUMERO_ACTO, ID_ARCHIVO, ID_DIRECTORIO, ID_PROCESO_INICIADO
                        FROM RADIO.RESOLUCIONES
                        WHERE SOL_UID='" + Solicitud.SOL_UID.ToString() + @"';
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTAnexo = dtSet.Tables[1];
                DataTable DTComunicado = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTConteos = dtSet.Tables[4];
                DataTable DTSolUsers = dtSet.Tables[5];
                DataTable DTTech = dtSet.Tables[6];
                DataTable DTResAnalisis = dtSet.Tables[7];
                DataTable DTResolucion = dtSet.Tables[8];

                DataRow DRS = DTSolicitud.Rows[0];

                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;

                switch ((STEPS)Solicitud.CURRENT.STEP)
                {
                    case STEPS.Administrativo:
                        DRS["FIN_SEVEN_ALDIA"] = Solicitud.Financiero.FIN_SEVEN_ALDIA != null ? Solicitud.Financiero.FIN_SEVEN_ALDIA : (object)DBNull.Value;
                        DRS["FIN_ESTADO_CUENTA_NUMBER"] = Solicitud.Financiero.FIN_ESTADO_CUENTA_NUMBER;
                        DRS["FIN_ESTADO_CUENTA_DATE"] = GetDateNulable(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                        DRS["FIN_REGISTRO_ALFA_NUMBER"] = Solicitud.Financiero.FIN_REGISTRO_ALFA_NUMBER;
                        DRS["FIN_REGISTRO_ALFA_DATE"] = GetDateNulable(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                        foreach (DataRow DRA in DTAnexo.Rows)
                        {
                            var Item = Solicitud.Anexos.Find(bp => bp.ANX_UID == (Guid)DRA["ANX_UID"]);
                            if (Item != null)
                            {
                                switch ((ROLES)ROLE)
                                {
                                    case ROLES.Funcionario:
                                        DRA["ANX_ROLE1_STATE_ID"] = Item.ANX_ROLE1_STATE_ID;
                                        DRA["ANX_ROLE1_COMMENT"] = Item.ANX_ROLE1_COMMENT;
                                        break;

                                    case ROLES.Coordinador:
                                        DRA["ANX_ROLE4_STATE_ID"] = Item.ANX_ROLE4_STATE_ID;
                                        DRA["ANX_ROLE4_COMMENT"] = Item.ANX_ROLE4_COMMENT;
                                        break;

                                    case ROLES.Subdirector:
                                        DRA["ANX_ROLE8_STATE_ID"] = Item.ANX_ROLE8_STATE_ID;
                                        DRA["ANX_ROLE8_COMMENT"] = Item.ANX_ROLE8_COMMENT;
                                        break;
                                }
                                DRA["ANX_ROLEX_CHANGED"] = Item.ANX_ROLEX_CHANGED;
                            }
                        }
                        break;

                    case STEPS.Tecnico:
                        DataRow DRT = DTTech.Rows[0];
                        DRT["TECH_ROLE_MINTIC_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_STATE_ID;
                        DRT["TECH_ROLE_MINTIC_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_COMMENT;
                        DRT["TECH_ROLEX_CHANGED"] = Solicitud.AnalisisTecnico.TECH_ROLEX_CHANGED;
                        break;

                    case STEPS.Resolucion:
                        DataRow DRR = DTResAnalisis.Rows[0];
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Funcionario:
                                DRR["RES_ROLE1_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE1_STATE_ID;
                                DRR["RES_ROLE1_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE1_COMMENT;
                                break;
                            case ROLES.Revisor:
                                DRR["RES_ROLE2_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE2_STATE_ID;
                                DRR["RES_ROLE2_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE2_COMMENT;
                                break;
                            case ROLES.Coordinador:
                                DRR["RES_ROLE4_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE4_STATE_ID;
                                DRR["RES_ROLE4_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE4_COMMENT;
                                break;
                            case ROLES.Subdirector:
                                DRR["RES_ROLE8_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE8_STATE_ID;
                                DRR["RES_ROLE8_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE8_COMMENT;
                                break;
                            case ROLES.Asesor:
                                DRR["RES_ROLE16_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE16_STATE_ID;
                                DRR["RES_ROLE16_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE16_COMMENT;
                                break;
                            case ROLES.Director:
                                DRR["RES_ROLE32_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE32_STATE_ID;
                                DRR["RES_ROLE32_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE32_COMMENT;
                                break;
                        }
                        DRR["RES_ROLEX_CHANGED"] = Solicitud.AnalisisResolucion.RES_ROLEX_CHANGED;
                        break;
                }
                
             
                if (bGenerarComunicado)
                {
                    Helper.TXT = GetDinamicTexts(ref cData);
                    Helper.IMG = GetDinamicImages(ref cData);
                    GetUserSign(Solicitud.USR_ADMIN.USR_ROLE8, ref cData);

                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                    RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
                    MemoryStream mem = null;
                    AZDigitall.RtaNotificar resultado = null;

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Administrativo:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Administrativo);
                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, false);
                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Administrativo);
                                            AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_ADMIN.USR_ROLE1.USR_ID, GROUPS.MinTIC, ROLES.Funcionario);
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Administrativo);
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    {
                                        switch (STATE248)
                                        {
                                            case ROLE248_STATE.Devuelto:
                                                CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Administrativo);
                                                AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_ADMIN.USR_ROLE4.USR_ID, GROUPS.MinTIC, ROLES.Coordinador);
                                                break;

                                            case ROLE248_STATE.Aceptado:
                                                IgualarRole1States(ref DTAnexo);

                                                switch ((COM_TYPE)Solicitud.ComunicadoAdministrativo.COM_TYPE)
                                                {
                                                    case COM_TYPE.Requerido:
                                                        DRS["SOL_STATE_ID"] = (int)SOL_STATES.Requerido;
                                                        DRS["SOL_REQUIRED_DATE"] = DateTime.Now;
                                                        DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        registroNumber = AZTools.AZRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        //registroNumber = AlfaRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminRequermiento, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.Concesionario, ROLES.Concesionario, STEPS.Administrativo);
                                                        break;

                                                    case COM_TYPE.Aprobado:
                                                        switch ((SOL_TYPES_CLASS)Solicitud.SOL_TYPE_CLASS)
                                                        {
                                                            case SOL_TYPES_CLASS.AdministrativoTecnico:
                                                                DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                                                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;
                                                                mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                                GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                                registroNumber = AZTools.AZRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_ADMIN_APROBAR");
                                                                //registroNumber = AlfaRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_ADMIN_APROBAR", ref mem);

                                                                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminAprobacion, DOC_SOURCE.MinTIC, DOC_SOURCE.ANE, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                                AsignacionesGroup2 = AutomaticAssignGroup2(ref cData, ref DTSolUsers);
                                                                bool RetManager = CrearSolicitudICSManager(ref cData, ref Solicitud, ref DynamicSolicitud);
                                                                if (!RetManager)
                                                                    return new ActionInfo(2, "Error en la operación escribiendo en ICSManager");
                                                                CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                                                break;

                                                            case SOL_TYPES_CLASS.AdministrativoOnly:
                                                                DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                                                DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                                                                DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnCierre;
                                                                DRS["SOL_ENDED"] = 1;
                                                                //registroNumber = AlfaRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR");
                                                                registroNumber = AZTools.AZRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR");
                                                                mem = RDSComunicados.GenerarComunicado(ref Comunicado, true, registroNumber);
                                                                GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                                AZTools.RegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR"), ref Solicitud, ref mem, ref Helper);
                                                                //AlfaRegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR"), ref Helper, ref mem);
                                                                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminAprobacion, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                                CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.EnCierre);
                                                                break;
                                                        }
                                                        break;

                                                    case COM_TYPE.Rechazado:
                                                        DRS["SOL_STATE_ID"] = (int)SOL_STATES.Rechazada;
                                                        DRS["SOL_ENDED"] = 2;
                                                        //registroNumber = AlfaRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR");
                                                        registroNumber = AZTools.AZRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR");
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true, registroNumber);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        //AlfaRegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR"), ref Helper, ref mem);
                                                        AZTools.RegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR"), ref Solicitud, ref mem, ref Helper);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminRechazo, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.Terminada);
                                                        break;
                                                }
                                                break;
                                        }
                                        break;
                                    }
                            }
                            UpdateAnexosRoleStates(ref DTAnexo, ref CURRENT);
                            break;

                        case STEPS.Tecnico:
                            switch (STATE248)
                            {
                                case ROLE248_STATE.Devuelto:
                                    Comunicado.COM_CLASS = (int)COM_CLASS.Revision;
                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                    //registroNumber = AlfaRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_TECH_DEVOLVER", ref mem);
                                    registroNumber = AZTools.AZRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_TECH_DEVOLVER");
                                    AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechDevolucion, DOC_SOURCE.MinTIC, DOC_SOURCE.ANE, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Subdirector, STEPS.Tecnico);
                                    DTTech.Rows[0]["TECH_ROLEX_CHANGED"] = false;
                                    break;
                                case ROLE248_STATE.Aceptado:
                                    cData.ClickExecuteProcedure("RADIO.SP_CARGARCUADROTECNICO", new ST_RDSParameters("@SOL_UID", Solicitud.SOL_UID));
                                    DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                    CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                    break;
                            }
                            break;

                        case STEPS.Resolucion:

                            DTResAnalisis.Rows[0]["RES_ROLEX_CHANGED"] = false;

                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Revisor, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Director, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                            DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnCierre;
                                            DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                                            DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                            DRS["SOL_ENDED"] = 1;
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.EnCierre);
                                            DataRow DRR = DTResolucion.Rows[0];
                                            resultado = NotificarResolucion(IDUserWeb, ref Solicitud, DRR["RES_NAME"].ToString(), (byte[])DRR["RES_WORD_FILE"]);
                                            DRR["NUMERO_ACTO"] = resultado.NumeroActo;
                                            DRR["ID_ARCHIVO"] = resultado.IdArchivo;
                                            DRR["ID_DIRECTORIO"] = resultado.IdDirectorio;
                                            DRR["ID_PROCESO_INICIADO"] = resultado.IdProcesoIniciado;
                                            break;
                                        case ROLE248_STATE.Revision:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Asesor, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Asesor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Director, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }

                    DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                    DRS["GROUP_CURRENT"] = CURRENT.GROUP;
                    DRS["ROLE_CURRENT"] = CURRENT.ROLE;
                    DRS["STEP_CURRENT"] = CURRENT.STEP;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                #region Logs y Correos

                if (bGenerarComunicado)
                {
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Administrativo:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch ((COM_TYPE)Comunicado.COM_TYPE)
                                    {
                                        case COM_TYPE.Aprobado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_APROBAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Rechazado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_RECHAZAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Requerido:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_REQUERIR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            switch ((COM_TYPE)Solicitud.ComunicadoAdministrativo.COM_TYPE)
                                            {
                                                case COM_TYPE.Requerido:
                                                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_REQUERIDA, ref Solicitud, ref Helper);
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_REQUERIR", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                                    break;

                                                case COM_TYPE.Rechazado:
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_RECHAZAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, registroNumber));
                                                    break;

                                                case COM_TYPE.Aprobado:
                                                    switch ((SOL_TYPES_CLASS)Solicitud.SOL_TYPE_CLASS)
                                                    {
                                                        case SOL_TYPES_CLASS.AdministrativoTecnico:

                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                                            AddLogAutomaticAssign(ref DynamicSolicitud, Solicitud.SOL_UID, ref lstLogs, LOG_SOURCE.ANE, ref AsignacionesGroup2, ref Helper, ROLES.Funcionario, ROLES.Revisor, ROLES.Coordinador, ROLES.Subdirector);
                                                            Solicitud.USR_TECH.USR_ROLE1 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First();
                                                            Solicitud.USR_TECH.USR_ROLE2 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First();
                                                            Solicitud.USR_TECH.USR_ROLE4 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First();
                                                            Solicitud.USR_TECH.USR_ROLE8 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                                                            Ret = EnviarCorreo(MAIL_TYPE.ANE_SOL_ASSIGNED, ref Solicitud, ref Helper);
                                                            break;
                                                        case SOL_TYPES_CLASS.AdministrativoOnly:
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_APROBAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, registroNumber));
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case STEPS.Tecnico:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REVISION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO_TO_ANE, ref Solicitud, ref Helper);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REVISION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", ""));
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case STEPS.Resolucion:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Revisor, Solicitud.USR_ADMIN.USR_ROLE2.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Revisor, Solicitud.USR_ADMIN.USR_ROLE2.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            break;
                                        case ROLE248_STATE.Revision:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_REVISION", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Asesor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Asesor, Solicitud.USR_ADMIN.USR_ROLE16.USR_NAME, ""));
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Asesor, Solicitud.USR_ADMIN.USR_ROLE16.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    AddLog(ref cData, lstLogs);
                }

                #endregion

                return new ActionInfo(1, "Operación exitosa.", GetSolicitudMinTIC(ref cData, Solicitud.SOL_UID, (int)GROUPS.MinTIC));

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sSQL);
                return new ActionInfo(-1, "Error en el servidor. " + e.Message);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo GuardarTramiteTecnico(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado, List<ST_RDSFiles_Up> Files)
        {
            string registroNumber = "";
            ST_RDSCurrent CURRENT = new ST_RDSCurrent();
            ST_Helper Helper = new ST_Helper();
            AZTools AZTools = new AZTools();
            ST_RDSComunicado Comunicado = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Tecnico);
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", ROLE=" + ROLE + ", Solicitud=" + Serializer.Serialize(Solicitud) + ", bGenerarComunicado=" + bGenerarComunicado;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_EMAIL, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE,
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT
                        From RADIO.SOLICITUDES Where SOL_UID='" + Solicitud.SOL_UID.ToString() + @"';
                
                        Select SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLEX_CHANGED,
                        TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_PTNRS_UPDATED, TECH_ROLE8_COMMENT, TECH_MODIFIED_DATE
		                FROM RADIO.TECH_ANALISIS 
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT TECH_UID, SOL_UID, TECH_TYPE, TECH_NAME, TECH_CONTENT_TYPE, TECH_CREATED_DATE, TECH_MODIFIED_DATE, TECH_CHANGED, TECH_FILE
                        FROM RADIO.TECH_DOCUMENTS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT Top 1 SOL_UID, COM_UID, COM_CLASS, COM_TYPE, COM_NAME, COM_DATE, COM_FILE
                        FROM RADIO.COMUNICADOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"' And COM_CLASS=2;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        SELECT Top 0 CON_ID, SOL_UID, USR_ID, USR_GROUP, USR_ROLE
                        FROM RADIO.CONTEOS;
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTTech = dtSet.Tables[1];
                DataTable DTTechDocs = dtSet.Tables[2];
                DataTable DTComunicado = dtSet.Tables[3];
                DataTable DTDocuments = dtSet.Tables[4];
                DataTable DTConteos = dtSet.Tables[5];

                DataRow DRS = DTSolicitud.Rows[0];

                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;


                DataRow DRT = DTTech.Rows[0];

                switch ((ROLES)ROLE)
                {
                    case ROLES.Funcionario:
                        DRT["TECH_ROLE1_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID;
                        DRT["TECH_ROLE1_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE1_COMMENT;
                        DRT["TECH_PTNRS_UPDATED"] = Solicitud.AnalisisTecnico.TECH_PTNRS_UPDATED;
                        break;
                    case ROLES.Revisor:
                        DRT["TECH_ROLE2_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE2_STATE_ID;
                        DRT["TECH_ROLE2_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE2_COMMENT;
                        break;
                    case ROLES.Coordinador:
                        DRT["TECH_ROLE4_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE4_STATE_ID;
                        DRT["TECH_ROLE4_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE4_COMMENT;
                        break;
                    case ROLES.Subdirector:
                        DRT["TECH_ROLE8_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE8_STATE_ID;
                        DRT["TECH_ROLE8_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE8_COMMENT;
                        break;
                }

                foreach (var File in Files)
                {
                    if (DTTechDocs.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("TECH_TYPE").Equals(File.TYPE_ID)).Any())
                    {
                        DataRow DRF = DTTechDocs.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("TECH_TYPE").Equals(File.TYPE_ID)).FirstOrDefault();
                        DRF["TECH_NAME"] = File.NAME;
                        DRF["TECH_TYPE"] = File.TYPE_ID;
                        DRF["TECH_FILE"] = Base64ToByteArray(File.DATA);
                        DRF["TECH_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRF["TECH_MODIFIED_DATE"] = DateTime.Now;
                        DRF["TECH_CHANGED"] = true;
                    }
                    else
                    {
                        DataRow DRF = DTTechDocs.NewRow();
                        DRF["TECH_UID"] = Guid.NewGuid();
                        DRF["SOL_UID"] = Solicitud.SOL_UID;
                        DRF["TECH_NAME"] = File.NAME;
                        DRF["TECH_TYPE"] = File.TYPE_ID;
                        DRF["TECH_FILE"] = Base64ToByteArray(File.DATA);
                        DRF["TECH_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRF["TECH_CREATED_DATE"] = DateTime.Now;
                        DRF["TECH_MODIFIED_DATE"] = DateTime.Now;
                        DRF["TECH_CHANGED"] = true;
                        DTTechDocs.Rows.Add(DRF);
                    }
                }

                DRT["TECH_ROLEX_CHANGED"] = true;

                if (bGenerarComunicado)
                {
                    DRT["TECH_MODIFIED_DATE"] = DateTime.Now;
                    Helper.TXT = GetDinamicTexts(ref cData);
                    Helper.IMG = GetDinamicImages(ref cData);
                    GetUserSign(Solicitud.USR_TECH.USR_ROLE8, ref cData);
                    DRT["TECH_ROLEX_CHANGED"] = false;

                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                    RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
                    MemoryStream mem = null;

                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Funcionario:
                            CURRENT.Fill(GROUPS.ANE, ROLES.Revisor, STEPS.Tecnico);
                            mem = RDSComunicados.GenerarComunicado(ref Comunicado, false);
                            GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                            break;

                        case ROLES.Revisor:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE2_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    ResetTechDocsChanged(ref DTTechDocs);
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                    AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE1.USR_ID, GROUPS.ANE, ROLES.Funcionario);
                                    break;

                                case ROLE248_STATE.Aceptado:
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Coordinador, STEPS.Tecnico);
                                    break;
                            }
                            break;

                        case ROLES.Coordinador:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE4_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    ResetTechDocsChanged(ref DTTechDocs);
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                    AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE1.USR_ID, GROUPS.ANE, ROLES.Funcionario);
                                    break;

                                case ROLE248_STATE.Aceptado:
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Subdirector, STEPS.Tecnico);
                                    break;
                            }
                            break;

                        case ROLES.Subdirector:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE8_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    ResetTechDocsChanged(ref DTTechDocs);
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                    AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE1.USR_ID, GROUPS.ANE, ROLES.Funcionario);
                                    break;

                                case ROLE248_STATE.Aceptado:
                                    switch ((COM_TYPE)Solicitud.ComunicadoTecnico.COM_TYPE){
                                        case COM_TYPE.Requerido:
                                            DRS["SOL_STATE_ID"] = (int)SOL_STATES.Requerido;
                                            DRS["SOL_REQUIRED_DATE"] = DateTime.Now;
                                            DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                            mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                            GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                            //registroNumber = AlfaRegistrarAneToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                            registroNumber = AZTools.AzRegistrarAneToConcesionarioRequerir(ref Solicitud, ref Helper);
                                            AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechRequermiento, DOC_SOURCE.ANE, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                            CURRENT.Fill(GROUPS.Concesionario, ROLES.Concesionario, STEPS.Tecnico);
                                            break;
                                        case COM_TYPE.Aprobado:
                                            mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                            GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                            //registroNumber = AlfaRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", ref mem);
                                            registroNumber = AZTools.AzRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", ref mem);
                                            AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechAprobacion, DOC_SOURCE.ANE , DOC_SOURCE.MinTIC, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Tecnico);
                                            break;
                                        case COM_TYPE.Rechazado:
                                            mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                            GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                            //registroNumber = AlfaRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", ref mem);
                                            registroNumber = AZTools.AzRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", ref mem);
                                            AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechRechazo , DOC_SOURCE.ANE, DOC_SOURCE.MinTIC, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Tecnico);
                                            break;
                                    }

                                    ResetTechDocsChanged(ref DTTechDocs);
                                    
                                    break;
                            }
                            break;
                    }

                    DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                    DRS["GROUP_CURRENT"] = CURRENT.GROUP;
                    DRS["ROLE_CURRENT"] = CURRENT.ROLE;
                    DRS["STEP_CURRENT"] = CURRENT.STEP;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                #region Logs y Correos

                if (bGenerarComunicado)
                {
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Funcionario:
                            switch ((ROLE1_STATE)Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID)
                            {
                                case ROLE1_STATE.Aprobado:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_APROBAR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                    break;

                                case ROLE1_STATE.Rechazado:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_RECHAZAR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                    break;

                                case ROLE1_STATE.Requerido:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REQUERIR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                    break;
                            }
                            break;

                        case ROLES.Revisor:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE2_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Revisor, Solicitud.USR_TECH.USR_ROLE2.USR_NAME, ""));
                                    break;

                                case ROLE248_STATE.Aceptado:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Revisor, Solicitud.USR_TECH.USR_ROLE2.USR_NAME, ""));
                                    break;
                            }
                            break;

                        case ROLES.Coordinador:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE4_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Coordinador, Solicitud.USR_TECH.USR_ROLE4.USR_NAME, ""));
                                    break;

                                case ROLE248_STATE.Aceptado:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Coordinador, Solicitud.USR_TECH.USR_ROLE4.USR_NAME, ""));
                                    break;
                            }
                            break;

                        case ROLES.Subdirector:
                            switch ((ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE8_STATE_ID)
                            {
                                case ROLE248_STATE.Devuelto:
                                    Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_TECH.USR_ROLE8.USR_NAME, ""));
                                    break;
                                case ROLE248_STATE.Aceptado:
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_TECH.USR_ROLE8.USR_NAME, ""));
                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REVISION", LOG_SOURCE.ANE, ROLES.NoAplica, "", registroNumber));
                                    Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_FINALIZADO, ref Solicitud, ref Helper, ROLE);
                                    break;
                            }
                            break;
                    }
                    AddLog(ref cData, lstLogs);
                }

                #endregion

                return new ActionInfo(1, "Operación exitosa.", GetSolicitudMinTIC(ref cData, Solicitud.SOL_UID, (int) GROUPS.ANE));

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor. " + e.Message);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo GuardarOtorgaTramiteTecnico(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado, List<ST_RDSFiles_Up> Files)
        {
            string registroNumber = "";
            List<ST_RDSUsuario> AsignacionesGroup2 = new List<ST_RDSUsuario>();
            ST_RDSCurrent CURRENT = new ST_RDSCurrent();
            ST_Helper Helper = new ST_Helper();
            AZTools AZTools = new AZTools();
            ST_RDSComunicado Comunicado = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Tecnico);
            ROLE248_STATE STATE248 = GetRole248State(ref Solicitud, ROLE);
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            string sSQL = "";
            var sLog = "IDUserWeb=" + IDUserWeb + ", ROLE=" + ROLE + ", Solicitud=" + Serializer.Serialize(Solicitud) + ", bGenerarComunicado=" + bGenerarComunicado;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }


                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_EMAIL, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE,
                        FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE, 
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED
                        From RADIO.SOLICITUDES Where SOL_UID='" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT ANX_UID, ANX_STATE_ID, ANX_COMMENT, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        ANX_ROLE1_COMMENT, ANX_ROLE2_COMMENT, ANX_ROLE4_COMMENT, ANX_ROLE8_COMMENT
                        FROM RADIO.TECH_ANEXOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';
                
                        SELECT SOL_UID, COM_UID, COM_CLASS, COM_TYPE, COM_NAME, COM_DATE, COM_FILE
                        FROM RADIO.COMUNICADOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"' And COM_CLASS In (1,2);

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        SELECT Top 0 CON_ID, SOL_UID, USR_ID, USR_GROUP, USR_ROLE
                        FROM RADIO.CONTEOS;

                        SELECT SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID 
                        FROM RADIO.SOL_USERS 
                        WHERE SOL_UID='" + Solicitud.SOL_UID.ToString() + @"'
                        Order By USR_GROUP;

                        Select SOL_UID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLE_MINTIC_COMMENT, TECH_ROLEX_CHANGED
		                FROM RADIO.TECH_ANALISIS 
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS 
		                WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT TECH_UID, SOL_UID, TECH_TYPE, TECH_NAME, TECH_CONTENT_TYPE, TECH_CREATED_DATE, TECH_MODIFIED_DATE, TECH_CHANGED, TECH_FILE
                        FROM RADIO.TECH_DOCUMENTS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTTechAnexo = dtSet.Tables[1];
                DataTable DTComunicado = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTConteos = dtSet.Tables[4];
                DataTable DTSolUsers = dtSet.Tables[5];
                DataTable DTTech = dtSet.Tables[6];
                DataTable DTResAnalisis = dtSet.Tables[7];
                DataTable DTTechDocs = dtSet.Tables[8];

                DataRow DRS = DTSolicitud.Rows[0];

                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;

                switch ((STEPS)Solicitud.CURRENT.STEP)
                {
                    case STEPS.Tecnico:
                        foreach (DataRow DRA in DTTechAnexo.Rows)
                        {
                            var Item = Solicitud.TechAnexos.Find(bp => bp.ANX_UID == (Guid)DRA["ANX_UID"]);
                            if (Item != null)
                            {
                                switch ((ROLES)ROLE)
                                {
                                    case ROLES.Funcionario:
                                        DRA["ANX_ROLE1_STATE_ID"] = Item.ANX_ROLE1_STATE_ID;
                                        DRA["ANX_ROLE1_COMMENT"] = Item.ANX_ROLE1_COMMENT;
                                        break;

                                    case ROLES.Revisor:
                                        DRA["ANX_ROLE2_STATE_ID"] = Item.ANX_ROLE2_STATE_ID;
                                        DRA["ANX_ROLE2_COMMENT"] = Item.ANX_ROLE2_COMMENT;
                                        break;

                                    case ROLES.Coordinador:
                                        DRA["ANX_ROLE4_STATE_ID"] = Item.ANX_ROLE4_STATE_ID;
                                        DRA["ANX_ROLE4_COMMENT"] = Item.ANX_ROLE4_COMMENT;
                                        break;

                                    case ROLES.Subdirector:
                                        DRA["ANX_ROLE8_STATE_ID"] = Item.ANX_ROLE8_STATE_ID;
                                        DRA["ANX_ROLE8_COMMENT"] = Item.ANX_ROLE8_COMMENT;
                                        break;
                                }
                                DRA["ANX_ROLEX_CHANGED"] = Item.ANX_ROLEX_CHANGED;
                            }
                        }
                        break;
                }

                foreach (var File in Files)
                {
                    if (DTTechDocs.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("TECH_TYPE").Equals(File.TYPE_ID)).Any())
                    {
                        DataRow DRF = DTTechDocs.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("TECH_TYPE").Equals(File.TYPE_ID)).FirstOrDefault();
                        DRF["TECH_NAME"] = File.NAME;
                        DRF["TECH_TYPE"] = File.TYPE_ID;
                        DRF["TECH_FILE"] = Base64ToByteArray(File.DATA);
                        DRF["TECH_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRF["TECH_MODIFIED_DATE"] = DateTime.Now;
                        DRF["TECH_CHANGED"] = true;
                    }
                    else
                    {
                        DataRow DRF = DTTechDocs.NewRow();
                        DRF["TECH_UID"] = Guid.NewGuid();
                        DRF["SOL_UID"] = Solicitud.SOL_UID;
                        DRF["TECH_NAME"] = File.NAME;
                        DRF["TECH_TYPE"] = File.TYPE_ID;
                        DRF["TECH_FILE"] = Base64ToByteArray(File.DATA);
                        DRF["TECH_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRF["TECH_CREATED_DATE"] = DateTime.Now;
                        DRF["TECH_MODIFIED_DATE"] = DateTime.Now;
                        DRF["TECH_CHANGED"] = true;
                        DTTechDocs.Rows.Add(DRF);
                    }
                }

                if (bGenerarComunicado)
                {
                    Helper.TXT = GetDinamicTexts(ref cData);
                    Helper.IMG = GetDinamicImages(ref cData);
                    GetUserSign(Solicitud.USR_TECH.USR_ROLE8, ref cData);

                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                    RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
                    MemoryStream mem = null;

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Tecnico:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Revisor, STEPS.Tecnico);
                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, false);
                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                    break;

                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                            AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE1.USR_ID, GROUPS.ANE, ROLES.Funcionario);
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.ANE, ROLES.Coordinador, STEPS.Tecnico);
                                            break;
                                    }
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                            AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE1.USR_ID, GROUPS.ANE, ROLES.Funcionario);
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.ANE, ROLES.Subdirector, STEPS.Tecnico);
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    {
                                        switch (STATE248)
                                        {
                                            case ROLE248_STATE.Devuelto:
                                                CURRENT.Fill(GROUPS.ANE, ROLES.Funcionario, STEPS.Tecnico);
                                                AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_TECH.USR_ROLE4.USR_ID, GROUPS.ANE, ROLES.Coordinador);
                                                break;

                                            case ROLE248_STATE.Aceptado:
                                                IgualarRole1States(ref DTTechAnexo);

                                                switch ((COM_TYPE)Solicitud.ComunicadoTecnico.COM_TYPE)
                                                {
                                                    case COM_TYPE.Requerido:
                                                        DRS["SOL_STATE_ID"] = (int)SOL_STATES.Requerido;
                                                        DRS["SOL_REQUIRED_DATE"] = DateTime.Now;
                                                        DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        //registroNumber = AlfaRegistrarAneToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        registroNumber = AZTools.AZRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminRequermiento, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.Concesionario, ROLES.Concesionario, STEPS.Tecnico);
                                                        break;

                                                    case COM_TYPE.Aprobado:
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        //registroNumber = AlfaRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", ref mem);
                                                        registroNumber = AZTools.AzRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_APROBAR", ref mem);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechAprobacion, DOC_SOURCE.ANE, DOC_SOURCE.MinTIC, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Tecnico);
                                                        break;
                                                    case COM_TYPE.Rechazado:
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        //registroNumber = AlfaRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", ref mem);
                                                        registroNumber = AZTools.AzRegistrarAneToMintic(ref Solicitud, ref Helper, "ALFA_REG_ANE_TO_MINTIC_TECH_RECHAZAR", ref mem);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechRechazo, DOC_SOURCE.ANE, DOC_SOURCE.MinTIC, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Tecnico);
                                                        break;
                                                }
                                                break;
                                        }
                                        break;
                                    }
                            }
                            UpdateAnexosRoleStates(ref DTTechAnexo, ref CURRENT);
                            break;
                    }

                    DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                    DRS["GROUP_CURRENT"] = CURRENT.GROUP;
                    DRS["ROLE_CURRENT"] = CURRENT.ROLE;
                    DRS["STEP_CURRENT"] = CURRENT.STEP;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                #region Logs y Correos

                if (bGenerarComunicado)
                {
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Tecnico:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch ((COM_TYPE)Comunicado.COM_TYPE)
                                    {
                                        case COM_TYPE.Aprobado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_APROBAR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Rechazado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_RECHAZAR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Requerido:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REQUERIR", LOG_SOURCE.ANE, ROLES.Funcionario, Solicitud.USR_TECH.USR_ROLE1.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Revisor, Solicitud.USR_TECH.USR_ROLE2.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Revisor, Solicitud.USR_TECH.USR_ROLE2.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Coordinador, Solicitud.USR_TECH.USR_ROLE4.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Coordinador, Solicitud.USR_TECH.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_DEVOLVER", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_TECH.USR_ROLE8.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_ACEPTAR", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_TECH.USR_ROLE8.USR_NAME, ""));
                                            switch ((COM_TYPE)Solicitud.ComunicadoAdministrativo.COM_TYPE)
                                            {
                                                case COM_TYPE.Requerido:
                                                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_REQUERIDA, ref Solicitud, ref Helper);
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REQUERIR", LOG_SOURCE.ANE, ROLES.NoAplica, "", registroNumber));
                                                    break;

                                                case COM_TYPE.Rechazado:
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_RECHAZAR", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_TECH.USR_ROLE8.USR_NAME, registroNumber));
                                                    break;

                                                case COM_TYPE.Aprobado:
                                                    switch ((SOL_TYPES_CLASS)Solicitud.SOL_TYPE_CLASS)
                                                    {
                                                        case SOL_TYPES_CLASS.AdministrativoTecnico:

                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO", LOG_SOURCE.ANE, ROLES.NoAplica, "", registroNumber));
                                                            //AddLogAutomaticAssign(ref DynamicSolicitud, Solicitud.SOL_UID, ref lstLogs, LOG_SOURCE.ANE, ref AsignacionesGroup2, ref Helper, ROLES.Funcionario, ROLES.Revisor, ROLES.Coordinador, ROLES.Subdirector);
                                                            //Solicitud.USR_TECH.USR_ROLE1 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First();
                                                            //Solicitud.USR_TECH.USR_ROLE2 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First();
                                                            //Solicitud.USR_TECH.USR_ROLE4 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First();
                                                            //Solicitud.USR_TECH.USR_ROLE8 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                                                            Ret = EnviarCorreo(MAIL_TYPE.ANE_SOL_ASSIGNED, ref Solicitud, ref Helper);
                                                            break;
                                                        case SOL_TYPES_CLASS.AdministrativoOnly:
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_APROBAR", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.ANE, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, registroNumber));
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;

                    }
                    AddLog(ref cData, lstLogs);
                }

                #endregion

                return new ActionInfo(1, "Operación exitosa.", GetSolicitudMinTIC(ref cData, Solicitud.SOL_UID, (int)GROUPS.ANE));

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sSQL);
                return new ActionInfo(-1, "Error en el servidor. " + e.Message);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }
        public ST_RDSResult GetAnalisisFinanciero(ST_RDSSolicitud Solicitud)
        {
            int IDUserWeb = 1;
            var Serializer = new JavaScriptSerializer_TESExtension();
            csData cData = null;
            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);

            ST_RDSResult mResult = new ST_RDSResult();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    mResult.Error = true;
                    mResult.ErrorMessage = "Error en el servidor";
                    return mResult;
                }

                ValidateToken(ref cData);

                mResult = VerificarCarteraAlDiaInterno(Solicitud.Expediente.Operador.CUS_IDENT);

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();

                if (mResult.Error)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_FINANCIERO_FAIL", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                else if (mResult.Estado)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_FINANCIERO_POSITIVO", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_FINANCIERO_NEGATIVO", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));

                AddLog(ref cData, lstLogs);

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, sLog);
                mResult.Error = true;
                mResult.ErrorMessage = "El sistema Seven no está disponible.";
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Solicitudes

        public ST_RDSSolicitudes GetSolicitudesConcesionario(string Nit)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSSolicitudes mResult = new ST_RDSSolicitudes();

            string Parametros = "IDUserWeb='" + IDUserWeb + "',Nit='" + Nit + "'";

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Nit=" + Nit, "!ConectedOk", strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                //           sSQL = @" 
                //Declare @nit nvarchar(50) = '" + Nit + @"'

                //               SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH
                //               FROM RADIO.V_CUSTOMERS CUS
                //               where CUS.CUS_IDENT = @nit

                //               Select SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_DESC, RADIO.F_GetPuedeSolicitar(SOL_TYPE_ID) AS PuedeSolicitar
                //               From RADIO.TIPOS_SOL
                //               Where SOL_IMPLEMENTED = 1 AND SOL_IS_OTORGA = 1

                //Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NAME, SERV.SERV_NUMBER, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
                //               SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
                //               SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase,
                //               SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SOL.STEP_CURRENT,
                //               SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA, SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
                //               CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL, 
                //               SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION, SERV.CUST_TXT3, 
                //               RADIO.F_GetAlarmStateConcesionario(SOL.SOL_REQUIRED_DATE, ALM_REQUIRED.ALARM_PRE_EXPIRE, ALM_REQUIRED.ALARM_EXPIRE, SOL.SOL_ENDED, SOL_REQUIRED_POSTPONE) SOL_ALARM_STATE,
                //               DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE, SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
                //               SOL.SOL_ENDED, SOL.AZ_DIRECTORIO
                //               Into #SOLICITUDES
                //               From RADIO.SOLICITUDES SOL
                //               Join RADIO.SOL_USERS USO On SOL.SOL_UID = USO.SOL_UID And USO.USR_GROUP = 1
                //               Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID
                //               Join RADIO.ESTADOS_SOL ESOL on SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID
                //               Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID = SERV.SERV_ID
                //               Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP = 4
                //               Where SOL.CUS_IDENT = @nit
                //               Order By 2 Desc

                //               Select* From #SOLICITUDES

                //               --Usuarios--
                //               SELECT SOL_UID, USR_GROUP,
                //               USR_ROLE1_ID, USR_ROLE1_NAME, USR_ROLE1_EMAIL, USR_ROLE2_ID, USR_ROLE2_NAME, USR_ROLE2_EMAIL, USR_ROLE4_ID, USR_ROLE4_NAME, USR_ROLE4_EMAIL, USR_ROLE8_ID, USR_ROLE8_NAME, USR_ROLE8_EMAIL
                //               FROM RADIO.V_USUARIOS
                //               WHERE SOL_UID In(Select SOL_UID From #SOLICITUDES)

                //               --Anexos--
                //               SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, ANX_CONTENT_TYPE, ANX.ANX_COMMENT
                //               FROM RADIO.ANEXOS as ANX
                //               JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL
                //               WHERE ANX.SOL_UID In(Select SOL_UID From #SOLICITUDES)
                //               ORDER BY ANX.ANX_NUMBER

                //               --Documents--
                //               SELECT DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_OPENED, Convert(int, ROW_NUMBER() Over(Order By DOC_DATE)) DOC_ORDER
                //              FROM RADIO.DOCUMENTS
                //              WHERE SOL_UID In(Select SOL_UID From #SOLICITUDES) And (DOC_ORIGIN=0 Or DOC_DEST=0)
                //               ORDER BY DOC_DATE

                //               --Anexos Tecnicos--
                //               SELECT TANX.SOL_UID, TANX.ANX_UID, TANX.ANX_NAME, TANX.ANX_NUMBER, TANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, TANX.ANX_TYPE_ID, ANX_CONTENT_TYPE, TANX.ANX_COMMENT
                //               FROM RADIO.TECH_ANEXOS as TANX
                //               JOIN RADIO.TIPO_ANEXO_SOL as TA ON TANX.ANX_TYPE_ID = TA.ID_ANX_SOL
                //               WHERE TANX.SOL_UID In(Select SOL_UID From #SOLICITUDES)
                //               ORDER BY TANX.ANX_NUMBER

                //               Drop Table #SOLICITUDES

                //SELECT CONT_ID, CUS_IDENT, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER, CONT_TITLE, CONT_EMAIL
                //               FROM RADIO.V_CONTACTOS_SIN_EXPEDIENTE
                //               where CUS_IDENT= @nit
                //               ";

                sSQL = @" 
                        Declare @nit nvarchar(50) = '" + Nit + @"'

                        SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH, CUS_CITYCODE, CUS_STATECODE, CUS_COUNTRYCODE 
                        FROM RADIO.V_CUSTOMERS_AZ CUS
                        where CUS.CUS_IDENT = @nit

                        Select SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_DESC, RADIO.F_GetPuedeSolicitar(SOL_TYPE_ID) AS PuedeSolicitar
                        From RADIO.TIPOS_SOL
                        Where SOL_IMPLEMENTED = 1 AND SOL_IS_OTORGA = 1

                        SELECT * INTO #SOLICITUDES
					    from (
							Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NAME, SERV.SERV_NUMBER, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
							SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
							SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase,
							SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, CUS.CUS_CITYCODE, CUS.CUS_STATECODE, CUS.CUS_COUNTRYCODE, SOL.STEP_CURRENT,
							SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA, SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
							CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL, 
							SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION, SERV.CUST_TXT3, 
							RADIO.F_GetAlarmStateConcesionario(SOL.SOL_REQUIRED_DATE, ALM_REQUIRED.ALARM_PRE_EXPIRE, ALM_REQUIRED.ALARM_EXPIRE, SOL.SOL_ENDED, SOL_REQUIRED_POSTPONE) SOL_ALARM_STATE,
							DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE ,SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
							SOL.SOL_ENDED, SOL.AZ_DIRECTORIO 
							From RADIO.SOLICITUDES SOL
							Join RADIO.SOL_USERS USO On SOL.SOL_UID=USO.SOL_UID And USO.USR_GROUP=1
							Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
							Join RADIO.ESTADOS_SOL ESOL on  SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID 
							Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID=SERV.SERV_ID
                            Join RADIO.V_CUSTOMERS_AZ CUS On CUS.CUS_IDENT=SOL.CUS_IDENT
							Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP=4
							Where SOL.CUS_IDENT=@nit 

							UNION ALL

							Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NAME, SERV.SERV_NUMBER, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
							SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
							SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase,
							SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, CUS.CUS_CITYCODE, CUS.CUS_STATECODE, CUS.CUS_COUNTRYCODE, SOL.STEP_CURRENT,
							SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA, SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
							CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL, 
							SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION, SERV.CUST_TXT3, 
							RADIO.F_GetAlarmStateConcesionario(SOL.SOL_REQUIRED_DATE, ALM_REQUIRED.ALARM_PRE_EXPIRE, ALM_REQUIRED.ALARM_EXPIRE, SOL.SOL_ENDED, SOL_REQUIRED_POSTPONE) SOL_ALARM_STATE,
							DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE ,SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
							SOL.SOL_ENDED, SOL.AZ_DIRECTORIO
							From RADIO.SOLICITUDES_TEMP SOL
							Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
							Join RADIO.ESTADOS_SOL ESOL on  SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID 
							Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID=SERV.SERV_ID
                            Join RADIO.V_CUSTOMERS_AZ CUS On CUS.CUS_IDENT=SOL.CUS_IDENT
							Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP=4
							Where SOL.CUS_IDENT=@nit 
					    ) as SOL   
                        Order By 2 Desc

                        Select* From #SOLICITUDES

                        --Usuarios--
                        SELECT SOL_UID, USR_GROUP,
                        USR_ROLE1_ID, USR_ROLE1_NAME, USR_ROLE1_EMAIL, USR_ROLE2_ID, USR_ROLE2_NAME, USR_ROLE2_EMAIL, USR_ROLE4_ID, USR_ROLE4_NAME, USR_ROLE4_EMAIL, USR_ROLE8_ID, USR_ROLE8_NAME, USR_ROLE8_EMAIL
                        FROM RADIO.V_USUARIOS
                        WHERE SOL_UID In(Select SOL_UID From #SOLICITUDES)

                        --Anexos--
                        SELECT * FROM (
							SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, ANX_CONTENT_TYPE, ANX.ANX_COMMENT, ANX.ANX_FILE
							FROM RADIO.ANEXOS as ANX
							JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL
							WHERE ANX.SOL_UID In(Select SOL_UID From #SOLICITUDES)

							UNION ALL

							SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, ANX_CONTENT_TYPE, ANX.ANX_COMMENT, ANX.ANX_FILE
							FROM RADIO.ANEXOS_TEMP as ANX
							JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL
							WHERE ANX.SOL_UID In(Select SOL_UID From #SOLICITUDES)
						) AS ANX
                        ORDER BY ANX.ANX_NUMBER

                        --Documents--
                        SELECT DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_OPENED, Convert(int, ROW_NUMBER() Over(Order By DOC_DATE)) DOC_ORDER
                        FROM RADIO.DOCUMENTS
                        WHERE SOL_UID In(Select SOL_UID From #SOLICITUDES) And (DOC_ORIGIN=0 Or DOC_DEST=0)
                        ORDER BY DOC_DATE

                        --Anexos Tecnicos--
                        SELECT TANX.SOL_UID, TANX.ANX_UID, TANX.ANX_NAME, TANX.ANX_NUMBER, TANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, TANX.ANX_TYPE_ID, ANX_CONTENT_TYPE, TANX.ANX_COMMENT
                        FROM RADIO.TECH_ANEXOS as TANX
                        JOIN RADIO.TIPO_ANEXO_SOL as TA ON TANX.ANX_TYPE_ID = TA.ID_ANX_SOL
                        WHERE TANX.SOL_UID In(Select SOL_UID From #SOLICITUDES)
                        ORDER BY TANX.ANX_NUMBER



                        Drop Table #SOLICITUDES

                        SELECT CONT_ID, CUS_IDENT, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER, CONT_TITLE, CONT_EMAIL
                        FROM RADIO.V_CONTACTOS_SIN_EXPEDIENTE
                        where CUS_IDENT= @nit
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                mResult.Operador = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                    select new ST_RDSOperador
                                    {
                                        CUS_ID = bp.Field<int>("CUS_ID"),
                                        CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                        CUS_NAME = bp.Field<string>("CUS_NAME"),
                                        CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                        CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                        CUS_CITYCODE = bp.Field<string>("CUS_CITYCODE"),
                                        CUS_STATECODE = bp.Field<string>("CUS_STATECODE"),
                                        CUS_COUNTRYCODE = bp.Field<string>("CUS_COUNTRYCODE"),

                                    }).ToList().FirstOrDefault();

                mResult.SOL_TYPES = (from bp in dtSet.Tables[1].AsEnumerable().AsQueryable()
                                     select new ST_RDSTipoSolicitud
                                     {
                                         SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                         SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                         SOL_TYPE_DESC = bp.Field<string>("SOL_TYPE_DESC"),
                                         disabled = !bp.Field<bool>("PuedeSolicitar"),
                                     }).ToList().OrderBy(bp => bp.SOL_TYPE_NAME).ToList();

                mResult.Solicitudes = (from bp in dtSet.Tables[2].AsEnumerable().AsQueryable()
                                       select new ST_RDSSolicitud
                                       {
                                           SOL_UID = bp.Field<Guid>("SOL_UID"),
                                           SERV_ID = bp.Field<int>("SERV_ID"),
                                           SOL_NUMBER = bp.Field<int>("SOL_NUMBER"),
                                           Radicado = bp.Field<string>("RAD_ALFANET"),
                                           SOL_CREATED_DATE = bp.Field<DateTime?>("SOL_CREATED_DATE").HasValue ? bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_MODIFIED_DATE = bp.Field<DateTime?>("SOL_MODIFIED_DATE").HasValue ? bp.Field<DateTime>("SOL_MODIFIED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_LAST_STATE_DATE = bp.Field<DateTime?>("SOL_LAST_STATE_DATE").HasValue ? bp.Field<DateTime>("SOL_LAST_STATE_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_REQUIRED_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_REQUIRED_DEADLINE_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DEADLINE_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DEADLINE_DATE").Date.ToString("yyyyMMdd") : "",

                                           SOL_REQUIRED_POSTPONE = bp.Field<int>("SOL_REQUIRED_POSTPONE"),
                                           SOL_STATE_ID = bp.Field<int>("SOL_STATE_ID"),
                                           SOL_STATE_NAME = bp.Field<string>("SOL_STATE_NAME"),
                                           SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                           SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                           SOL_TYPE_CLASS = bp.Field<int>("SOL_TYPE_CLASS"),
                                           SOL_IS_OTORGA = bp.Field<bool>("SOL_IS_OTORGA"),
                                           SOL_ENDED = bp.Field<int>("SOL_ENDED"),
                                           SOL_CREATOR = bp.Field<int>("SOL_CREATOR"),
                                           SOL_ALARM_STATE = bp.Field<int>("SOL_ALARM_STATE"),
                                           AZ_DIRECTORIO = bp.Field<string>("AZ_DIRECTORIO"),
                                           Contacto = new ST_RDSContacto
                                           {
                                               CONT_ID = bp.Field<int>("CONT_ID"),
                                               CONT_NAME = bp.Field<string>("CONT_NAME"),
                                               CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                                               CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                                               CONT_TITLE = GetContactTitle(bp.Field<int>("CONT_ROLE")),
                                               CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                                           },
                                           Clasificacion = bp.Field<string>("Clasificacion"),
                                           NivelCubrimiento = bp.Field<string>("NivelCubrimiento"),
                                           Tecnologia = bp.Field<string>("Tecnologia"),
                                           DatosComunidad = new ST_RDSDatosComunidad
                                           {
                                               NombreComunidad = bp.Field<string>("NombreComunidad"),
                                               IdLugarComunidad = bp.Field<decimal?>("IdLugarComunidad"),
                                               DireccionComunidad = bp.Field<string>("DireccionComunidad"),
                                               CiudadComunidad = bp.Field<string>("CiudadComunidad"),
                                               DepartamentoComunidad = bp.Field<string>("DepartamentoComunidad"),
                                               CodeLugarComunidad = bp.Field<string>("CodeLugarComunidad"),
                                               Territorio = bp.Field<string>("Territorio"),
                                               ResolucionMinInterior = bp.Field<string>("ResolucionMinInterior"),
                                               GrupoEtnico = bp.Field<string>("GrupoEtnico"),
                                               NombrePuebloComunidad = bp.Field<string>("NombrePuebloComunidad"),
                                               NombreResguardo = bp.Field<string>("NombreResguardo"),
                                               NombreCabildo = bp.Field<string>("NombreCabildo"),
                                               NombreKumpania = bp.Field<string>("NombreKumpania"),
                                               NombreConsejo = bp.Field<string>("NombreConsejo"),
                                               NombreOrganizacionBase = bp.Field<string>("NombreOrganizacionBase"),
                                           },
                                           USR_ADMIN = (from bpu in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 1)
                                                        select new ST_RDSRoles
                                                        {
                                                            USR_ROLE1 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                                            },
                                                            USR_ROLE2 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                                            },
                                                            USR_ROLE4 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                                            },
                                                            USR_ROLE8 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                                            },
                                                        }).FirstOrDefault(),
                                           USR_TECH = (from bpu in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 2)
                                                       select new ST_RDSRoles
                                                       {
                                                           USR_ROLE1 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                                           },
                                                           USR_ROLE2 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                                           },
                                                           USR_ROLE4 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                                           },
                                                           USR_ROLE8 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                                           },
                                                       }).FirstOrDefault(),
                                           Anexos = (from bpa in dtSet.Tables[4].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                     select new ST_RDSAnexos
                                                     {
                                                         ANX_UID = bpa.Field<Guid>("ANX_UID"),
                                                         ANX_NUMBER = bpa.Field<int>("ANX_NUMBER"),
                                                         ANX_STATE_ID = bpa.Field<int>("ANX_STATE_ID"),
                                                         ANX_TYPE_ID = bpa.Field<int>("ANX_TYPE_ID"),
                                                         ANX_TYPE_NAME = bpa.Field<string>("ANX_TYPE_NAME"),
                                                         ANX_NAME = bpa.Field<string>("ANX_NAME"),
                                                         ANX_CONTENT_TYPE = bpa.Field<string>("ANX_CONTENT_TYPE"),
                                                         ANX_COMMENT = bpa.Field<string>("ANX_COMMENT"),
                                                         ANX_FILE = Convert.ToBase64String(bpa.Field<byte[]>("ANX_FILE"))
                                                     }).ToList(),
                                           CURRENT = new ST_RDSCurrent
                                           {
                                               STEP = bp.Field<int>("STEP_CURRENT")
                                           },
                                           Expediente = new ST_RDSExpediente
                                           {
                                               SERV_ID = bp.Field<int>("SERV_ID"),
                                               SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                                               STATE = bp.Field<string>("STATE"),
                                               DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO") : null,
                                               CITY = bp.Field<string>("CITY") != null ? bp.Field<string>("CITY") : null,
                                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                                               POWER = bp.Field<double?>("POWER"),
                                               ASL = bp.Field<double?>("ASL"),
                                               FREQUENCY = bp.Field<double?>("FREQUENCY"),
                                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                                               SERV_NAME = bp.Field<string>("SERV_NAME"),
                                               MODULATION = bp.Field<string>("MODULATION"),
                                               STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                                               CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                                               Operador = new ST_RDSOperador
                                               {
                                                   CUS_ID = bp.Field<int>("CUS_ID"),
                                                   CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                                   CUS_NAME = bp.Field<string>("CUS_NAME"),
                                                   CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                                   CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                                   CUS_CITYCODE = bp.Field<string>("CUS_CITYCODE"),
                                                   CUS_STATECODE = bp.Field<string>("CUS_STATECODE"),
                                                   CUS_COUNTRYCODE = bp.Field<string>("CUS_COUNTRYCODE"),
                                               },
                                           },
                                           Documents = (from bpd in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                        select new ST_RDSDocument
                                                        {
                                                            DOC_UID = bpd.Field<Guid>("DOC_UID"),
                                                            DOC_ORDER = bpd.Field<int>("DOC_ORDER"),
                                                            DOC_CLASS = bpd.Field<int>("DOC_CLASS"),
                                                            DOC_TYPE = bpd.Field<int>("DOC_TYPE"),
                                                            DOC_ORIGIN = bpd.Field<int>("DOC_ORIGIN"),
                                                            DOC_DEST = bpd.Field<int>("DOC_DEST"),
                                                            DOC_NAME = bpd.Field<string>("DOC_NAME"),
                                                            DOC_NUMBER = bpd.Field<string>("DOC_NUMBER"),
                                                            DOC_OPENED = bpd.Field<int>("DOC_OPENED"),
                                                            DOC_DATE = bpd.Field<DateTime>("DOC_DATE").Date.ToString("yyyyMMdd")
                                                        }).ToList(),
                                           TechAnexos = (from bpt in dtSet.Tables[6].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                         select new ST_RDSTechAnexos
                                                         {
                                                             ANX_UID = bpt.Field<Guid>("ANX_UID"),
                                                             ANX_NUMBER = bpt.Field<int>("ANX_NUMBER"),
                                                             ANX_STATE_ID = bpt.Field<int>("ANX_STATE_ID"),
                                                             ANX_TYPE_ID = bpt.Field<int>("ANX_TYPE_ID"),
                                                             ANX_TYPE_NAME = bpt.Field<string>("ANX_TYPE_NAME"),
                                                             ANX_NAME = bpt.Field<string>("ANX_NAME"),
                                                             ANX_CONTENT_TYPE = bpt.Field<string>("ANX_CONTENT_TYPE"),
                                                             ANX_COMMENT = bpt.Field<string>("ANX_COMMENT")
                                                         }).ToList(),

                                       }).ToList();


                mResult.Contactos = (from bp in dtSet.Tables[7].AsEnumerable().AsQueryable()
                                     select new ST_RDSContacto
                                     {
                                         CONT_ID = bp.Field<int>("CONT_ID"),
                                         CONT_NAME = bp.Field<string>("CONT_NAME"),
                                         CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                                         CONT_TEL = bp.Field<string>("CONT_TEL"),
                                         CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                                         CONT_TITLE = bp.Field<string>("CONT_TITLE"),
                                         CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                                     }).ToList();



                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }
        public ST_RDSSolicitudesMintic GetSolicitudesMinTIC(int IDUserWeb, int USR_GROUP, int USR_ROLE, int SOL_ENDED, string DateIni, string DateEnd)
        {
            csData cData = null;
            ST_RDSSolicitudesMintic mResult = new ST_RDSSolicitudesMintic();

            string Parametros = "IDUserWeb='" + IDUserWeb + "',USR_GROUP='" + USR_GROUP + "'" + "',USR_ROLE='" + USR_ROLE + "',SOL_ENDED='" + SOL_ENDED + "'";

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                            
                        Declare @USR_ID int=" + IDUserWeb + @"
                        Declare @USR_GROUP int=" + USR_GROUP + @"
                        Declare @USR_ROLE int=" + USR_ROLE + @"
                        Declare @SOL_ENDED int=" + SOL_ENDED + @"
                        DECLARE @DateIni date='" + DateIni + @"'
                        DECLARE @DateEnd date='" + DateEnd + @"'

                        Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NUMBER, SERV.SERV_NAME, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
                        SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
                        SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase,
                        SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SERV_STATUS, SERV.CUST_TXT3, SERV.MUNICIPIO,
                        SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA, SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
                        SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION,
                        FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE,
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT,
                        CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL,
                        USR_ROLE1_REASIGN, USR_ROLE2_REASIGN, USR_ROLE4_REASIGN, USR_ROLE8_REASIGN, USR_ROLE16_REASIGN,
                        USR_ROLE1_REASIGN_COMMENT, USR_ROLE2_REASIGN_COMMENT, USR_ROLE4_REASIGN_COMMENT, USR_ROLE8_REASIGN_COMMENT, USR_ROLE16_REASIGN_COMMENT,
                        RES.RES_NAME, RES.RES_IS_CREATED, RES.RES_CREATED_DATE, RES.RES_MODIFIED_DATE, RES.RES_TRACKING_COUNT,
                        RESVIA.RES_NAME AS RESVIA_NAME, RESVIA.RES_IS_CREATED AS RESVIA_IS_CREATED, RESVIA.RES_CREATED_DATE AS RESVIA_CREATED_DATE, RESVIA.RES_MODIFIED_DATE AS RESVIA_MODIFIED_DATE, RESVIA.RES_TRACKING_COUNT AS RESVIA_TRACKING_COUNT,
                        RADIO.F_GetAlarmState(SOL.SOL_LAST_STATE_DATE, ALM.ALARM_PRE_EXPIRE, ALM.ALARM_EXPIRE, SOL.SOL_ENDED) SOL_ALARM_STATE,
                        DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE ,SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
                        SOL.SOL_ENDED, SOL.CAMPOS_SOL, SOL.COMMENT, AZ_DIRECTORIO
                        Into #SOLICITUDES
                        From RADIO.SOLICITUDES SOL
                        Join RADIO.SOL_USERS USO On SOL.SOL_UID=USO.SOL_UID And USO.USR_GROUP=@USR_GROUP
                        Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
                        Join RADIO.ESTADOS_SOL ESOL on  SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID 
                        Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID=SERV.SERV_ID
                        Join RADIO.RESOLUCIONES RES On SOL.SOL_UID=RES.SOL_UID
                        Join RADIO.RESOLUCIONES_VIABILIDAD RESVIA On SOL.SOL_UID=RESVIA.SOL_UID
                        Join RADIO.ALARMS ALM On SOL.STEP_CURRENT=ALM.ALARM_STEP
                        Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP=4
                        Where (((SOL.SOL_ENDED=0 And SOL.SOL_ENDED=@SOL_ENDED) Or (SOL.SOL_ENDED=1 And @SOL_ENDED=0) Or (SOL.SOL_ENDED=2 And @SOL_ENDED=0 And Convert(date,SOL.SOL_MODIFIED_DATE)=Convert(date,getdate()))) And ((@USR_ROLE=1 And USO.USR_ROLE1_ID=@USR_ID) Or (@USR_ROLE=2 And USO.USR_ROLE2_ID=@USR_ID) Or (@USR_ROLE=4 And USO.USR_ROLE4_ID=@USR_ID) Or (@USR_ROLE=8 And USO.USR_ROLE8_ID=@USR_ID) Or (@USR_ROLE=16 And USO.USR_ROLE16_ID=@USR_ID) Or (@USR_ROLE=32 And USO.USR_ROLE32_ID=@USR_ID) Or (@USR_ROLE=128))) 
						Or (SOL.SOL_ENDED = 2 And @SOL_ENDED=2 And Convert(date,SOL.SOL_MODIFIED_DATE) BETWEEN @DateIni And @DateEnd)
                        Order By 2 Desc

	                    Select * From #SOLICITUDES

                        --Usuarios--
                        SELECT SOL_UID, USR_GROUP, 
                        USR_ROLE1_ID, USR_ROLE1_NAME, USR_ROLE1_EMAIL, USR_ROLE2_ID, USR_ROLE2_NAME, USR_ROLE2_EMAIL, USR_ROLE4_ID, USR_ROLE4_NAME, USR_ROLE4_EMAIL, USR_ROLE8_ID, USR_ROLE8_NAME, USR_ROLE8_EMAIL, USR_ROLE16_ID, USR_ROLE16_NAME, USR_ROLE16_EMAIL, USR_ROLE32_ID, USR_ROLE32_NAME, USR_ROLE32_EMAIL
                        FROM RADIO.V_USUARIOS
                        WHERE SOL_UID  In (Select SOL_UID From #SOLICITUDES)

                        --Contactos--
                        SELECT SERV_ID, SERV_NUMBER, CONT_ID, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER
                        FROM RADIO.V_CONTACTOS
                        WHERE SERV_ID In (Select SERV_ID From #SOLICITUDES)

                        --Anexos--
                        SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        ANX.ANX_ROLE1_COMMENT, ANX.ANX_ROLE2_COMMENT, ANX.ANX_ROLE4_COMMENT, ANX.ANX_ROLE8_COMMENT
                        FROM RADIO.ANEXOS as ANX 
                        JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL 
                        WHERE ANX.SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                        ORDER BY ANX.ANX_NUMBER

                        --Documents--
                        SELECT DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, Convert(int, ROW_NUMBER() Over (Order By DOC_DATE)) DOC_ORDER
                        FROM RADIO.DOCUMENTS
                        WHERE SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                        ORDER BY DOC_DATE

                        --Comunicado Administrativo y financiero--
                        Select SOL_UID, COM_UID, COM_NAME, COM_DATE, COM_CLASS, COM_TYPE
		                FROM RADIO.COMUNICADOS 
		                WHERE COM_CLASS In (1,2) And SOL_UID In (Select SOL_UID From #SOLICITUDES)
                        
                        --Analisis Tecnico--
                        Select SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLEX_CHANGED,
                        TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT, TECH_ROLE_MINTIC_COMMENT, TECH_PTNRS_UPDATED, TECH_CREATED_DATE, TECH_MODIFIED_DATE
		                FROM RADIO.TECH_ANALISIS 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Documentos técnicos--
                        SELECT TECH_UID, SOL_UID, TECH_TYPE, TECH_NAME, TECH_CONTENT_TYPE, TECH_CREATED_DATE, TECH_MODIFIED_DATE, TECH_CHANGED
                        FROM RADIO.TECH_DOCUMENTS
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Analisis Resolucion--
                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Analisis Resolucion Viabilidad--
                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Anexos Tecnicos--
                        SELECT TANX.SOL_UID, TANX.ANX_UID, TANX.ANX_NAME, TANX.ANX_NUMBER, TANX.ANX_STATE_ID, TA.TIPO_ANX_SOL TANX_TYPE_NAME, 
                        TANX.ANX_TYPE_ID, ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        TANX.ANX_ROLE1_COMMENT, TANX.ANX_ROLE2_COMMENT, TANX.ANX_ROLE4_COMMENT, TANX.ANX_ROLE8_COMMENT
                        FROM RADIO.TECH_ANEXOS as TANX 
                        JOIN RADIO.TIPO_ANEXO_SOL as TA ON TANX.ANX_TYPE_ID = TA.ID_ANX_SOL 
                        WHERE TANX.SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                        ORDER BY TANX.ANX_NUMBER

	                    Drop Table #SOLICITUDES

                        Select SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_DESC, RADIO.F_GetPuedeSolicitar(SOL_TYPE_ID) AS PuedeSolicitar
                        From RADIO.TIPOS_SOL 
                        Where SOL_IMPLEMENTED=1 --AND SOL_IS_OTORGA=1

                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);



                mResult.Solicitudes = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                       select new ST_RDSSolicitud
                                       {
                                           SOL_UID = bp.Field<Guid>("SOL_UID"),
                                           SOL_NUMBER = bp.Field<int>("SOL_NUMBER"),
                                           Radicado = bp.Field<string>("RAD_ALFANET"),
                                           SOL_CREATED_DATE = bp.Field<DateTime?>("SOL_CREATED_DATE").HasValue ? bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_CREATED_DATE_YEAR = bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyy"),
                                           SOL_MODIFIED_DATE = bp.Field<DateTime?>("SOL_MODIFIED_DATE").HasValue ? bp.Field<DateTime>("SOL_MODIFIED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_LAST_STATE_DATE = bp.Field<DateTime?>("SOL_LAST_STATE_DATE").HasValue ? bp.Field<DateTime>("SOL_LAST_STATE_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_REQUIRED_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_REQUIRED_DEADLINE_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DEADLINE_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DEADLINE_DATE").Date.ToString("yyyyMMdd") : "",
                                           SOL_REQUIRED_POSTPONE = bp.Field<int>("SOL_REQUIRED_POSTPONE"),
                                           SOL_STATE_ID = bp.Field<int>("SOL_STATE_ID"),
                                           SOL_STATE_NAME = bp.Field<string>("SOL_STATE_NAME"),
                                           SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                           SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                           SOL_TYPE_CLASS = bp.Field<int>("SOL_TYPE_CLASS"),
                                           SOL_IS_OTORGA = bp.Field<bool>("SOL_IS_OTORGA"),
                                           SOL_ENDED = bp.Field<int>("SOL_ENDED"),
                                           SOL_CREATOR = bp.Field<int>("SOL_CREATOR"),
                                           SOL_ALARM_STATE = bp.Field<int>("SOL_ALARM_STATE"),
                                           CAMPOS_SOL = bp.Field<string>("CAMPOS_SOL"),
                                           COMMENT = bp.Field<string>("COMMENT"),
                                           Clasificacion = bp.Field<string>("Clasificacion"),
                                           NivelCubrimiento = bp.Field<string>("NivelCubrimiento"),
                                           Tecnologia = bp.Field<string>("Tecnologia"),
                                           AZ_DIRECTORIO = bp.Field<string>("AZ_DIRECTORIO"),
                                           DatosComunidad = new ST_RDSDatosComunidad
                                           {
                                               NombreComunidad = bp.Field<string>("NombreComunidad"),
                                               IdLugarComunidad = bp.Field<decimal?>("IdLugarComunidad"),
                                               DireccionComunidad = bp.Field<string>("DireccionComunidad"),
                                               CiudadComunidad = bp.Field<string>("CiudadComunidad"),
                                               DepartamentoComunidad = bp.Field<string>("DepartamentoComunidad"),
                                               CodeLugarComunidad = bp.Field<string>("CodeLugarComunidad"),
                                               Territorio = bp.Field<string>("Territorio"),
                                               ResolucionMinInterior = bp.Field<string>("ResolucionMinInterior"),
                                               GrupoEtnico = bp.Field<string>("GrupoEtnico"),
                                               NombrePuebloComunidad = bp.Field<string>("NombrePuebloComunidad"),
                                               NombreResguardo = bp.Field<string>("NombreResguardo"),
                                               NombreCabildo = bp.Field<string>("NombreCabildo"),
                                               NombreKumpania = bp.Field<string>("NombreKumpania"),
                                               NombreConsejo = bp.Field<string>("NombreConsejo"),
                                               NombreOrganizacionBase = bp.Field<string>("NombreOrganizacionBase"),
                                           },
                                           Resolucion = new ST_RDSResolucion
                                           {
                                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                                               RES_NAME = bp.Field<string>("RES_NAME"),
                                               RES_IS_CREATED = bp.Field<bool>("RES_IS_CREATED"),
                                               RES_CREATED_DATE = bp.Field<bool>("RES_IS_CREATED") ? bp.Field<DateTime>("RES_CREATED_DATE").ToString("yyyyMMdd") : "",
                                               RES_MODIFIED_DATE = bp.Field<bool>("RES_IS_CREATED") ? bp.Field<DateTime>("RES_MODIFIED_DATE").ToString("yyyyMMdd") : "",
                                               RES_TRACKING_COUNT = bp.Field<int>("RES_TRACKING_COUNT")
                                           },
                                           Viabilidad = new ST_RDSResolucion
                                           {
                                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                                               RES_NAME = bp.Field<string>("RESVIA_NAME"),
                                               RES_IS_CREATED = bp.Field<bool>("RESVIA_IS_CREATED"),
                                               RES_CREATED_DATE = bp.Field<bool>("RESVIA_IS_CREATED") ? bp.Field<DateTime>("RESVIA_CREATED_DATE").ToString("yyyyMMdd") : "",
                                               RES_MODIFIED_DATE = bp.Field<bool>("RESVIA_IS_CREATED") ? bp.Field<DateTime>("RESVIA_MODIFIED_DATE").ToString("yyyyMMdd") : "",
                                               RES_TRACKING_COUNT = bp.Field<int>("RESVIA_TRACKING_COUNT")
                                           },
                                           CURRENT = new ST_RDSCurrent
                                           {
                                               GROUP = bp.Field<int>("GROUP_CURRENT"),
                                               ROLE = bp.Field<int>("ROLE_CURRENT"),
                                               STEP = bp.Field<int>("STEP_CURRENT")
                                           },
                                           Contacto = new ST_RDSContacto
                                           {
                                               CONT_ID = bp.Field<int>("CONT_ID"),
                                               CONT_NAME = bp.Field<string>("CONT_NAME"),
                                               CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                                               CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                                               CONT_TITLE = GetContactTitle(bp.Field<int>("CONT_ROLE")),
                                               CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                                           },
                                           USR_ADMIN = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 1)
                                                        select new ST_RDSRoles
                                                        {
                                                            USR_ROLE1 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                                            },
                                                            USR_ROLE2 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                                            },
                                                            USR_ROLE4 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                                            },
                                                            USR_ROLE8 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                                            },
                                                            USR_ROLE16 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE16_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE16_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE16_EMAIL")
                                                            },
                                                            USR_ROLE32 = new ST_RDSUsuario
                                                            {
                                                                USR_ID = bpu.Field<int>("USR_ROLE32_ID"),
                                                                USR_NAME = bpu.Field<string>("USR_ROLE32_NAME"),
                                                                USR_EMAIL = bpu.Field<string>("USR_ROLE32_EMAIL")
                                                            },
                                                        }).FirstOrDefault(),
                                           USR_TECH = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 2)
                                                       select new ST_RDSRoles
                                                       {
                                                           USR_ROLE1 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                                           },
                                                           USR_ROLE2 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                                           },
                                                           USR_ROLE4 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                                           },
                                                           USR_ROLE8 = new ST_RDSUsuario
                                                           {
                                                               USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                               USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                               USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                                           },
                                                       }).FirstOrDefault(),
                                           Reasign = new ST_RDSReasign
                                           {
                                               USR_ROLE1_REASIGN = bp.Field<bool>("USR_ROLE1_REASIGN"),
                                               USR_ROLE2_REASIGN = bp.Field<bool>("USR_ROLE2_REASIGN"),
                                               USR_ROLE4_REASIGN = bp.Field<bool>("USR_ROLE4_REASIGN"),
                                               USR_ROLE8_REASIGN = bp.Field<bool>("USR_ROLE8_REASIGN"),
                                               USR_ROLE16_REASIGN = bp.Field<bool>("USR_ROLE16_REASIGN"),
                                               USR_ROLE1_REASIGN_COMMENT = bp.Field<string>("USR_ROLE1_REASIGN_COMMENT"),
                                               USR_ROLE2_REASIGN_COMMENT = bp.Field<string>("USR_ROLE2_REASIGN_COMMENT"),
                                               USR_ROLE4_REASIGN_COMMENT = bp.Field<string>("USR_ROLE4_REASIGN_COMMENT"),
                                               USR_ROLE8_REASIGN_COMMENT = bp.Field<string>("USR_ROLE8_REASIGN_COMMENT"),
                                               USR_ROLE16_REASIGN_COMMENT = bp.Field<string>("USR_ROLE16_REASIGN_COMMENT")
                                           },
                                           Financiero = new ST_RDSAnalisisFinanciero
                                           {
                                               FIN_SEVEN_ALDIA = bp.Field<bool?>("FIN_SEVEN_ALDIA"),
                                               FIN_ESTADO_CUENTA_NUMBER = bp.Field<string>("FIN_ESTADO_CUENTA_NUMBER"),
                                               FIN_ESTADO_CUENTA_DATE = bp.Field<DateTime?>("FIN_ESTADO_CUENTA_DATE").HasValue ? bp.Field<DateTime>("FIN_ESTADO_CUENTA_DATE").Date.ToString("yyyyMMdd") : "",
                                               FIN_REGISTRO_ALFA_NUMBER = bp.Field<string>("FIN_REGISTRO_ALFA_NUMBER"),
                                               FIN_REGISTRO_ALFA_DATE = bp.Field<DateTime?>("FIN_REGISTRO_ALFA_DATE").HasValue ? bp.Field<DateTime>("FIN_REGISTRO_ALFA_DATE").Date.ToString("yyyyMMdd") : "",
                                           },
                                           Expediente = new ST_RDSExpediente
                                           {
                                               SERV_ID = bp.Field<int>("SERV_ID"),
                                               SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                                               SERV_STATUS = bp.Field<string>("SERV_STATUS"),
                                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                                               STATE = bp.Field<string>("STATE"),
                                               DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO").ToUpperInvariant() : null,
                                               CITY = bp.Field<string>("CITY") != null ? bp.Field<string>("CITY").ToUpperInvariant() : null,
                                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                                               POWER = bp.Field<double?>("POWER"),
                                               ASL = bp.Field<double?>("ASL"),
                                               FREQUENCY = bp.Field<double?>("FREQUENCY"),
                                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                                               SERV_NAME = bp.Field<string>("SERV_NAME").ToUpperInvariant(),
                                               MODULATION = bp.Field<string>("MODULATION"),
                                               STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                                               CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                                               Operador = new ST_RDSOperador
                                               {
                                                   CUS_ID = bp.Field<int>("CUS_ID"),
                                                   CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                                   CUS_NAME = bp.Field<string>("CUS_NAME"),
                                                   CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                                   CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                                   CUS_CITYCODE = bp.Field<string>("MUNICIPIO"),
                                                   CUS_STATECODE = int.Parse(bp.Field<string>("MUNICIPIO").Substring(0, 2)).ToString(),
                                                   CUS_COUNTRYCODE = "169",
                                               },
                                               Contactos = (from bpc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                                            select new ST_RDSContacto
                                                            {
                                                                CONT_ID = bpc.Field<int>("CONT_ID"),
                                                                CONT_NUMBER = bpc.Field<string>("CONT_NUMBER"),
                                                                CONT_NAME = bpc.Field<string>("CONT_NAME"),
                                                                CONT_TEL = bpc.Field<string>("CONT_TEL"),
                                                                CONT_ROLE = bpc.Field<int>("CONT_ROLE"),
                                                                CONT_TITLE = GetContactTitle(bpc.Field<int>("CONT_ROLE")),
                                                            }).ToList()
                                           },
                                           Anexos = (from bpa in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                     select new ST_RDSAnexos
                                                     {
                                                         ANX_UID = bpa.Field<Guid>("ANX_UID"),
                                                         ANX_NUMBER = bpa.Field<int>("ANX_NUMBER"),
                                                         ANX_STATE_ID = bpa.Field<int>("ANX_STATE_ID"),
                                                         ANX_TYPE_ID = bpa.Field<int>("ANX_TYPE_ID"),
                                                         ANX_TYPE_NAME = bpa.Field<string>("ANX_TYPE_NAME"),
                                                         ANX_NAME = bpa.Field<string>("ANX_NAME"),
                                                         ANX_ROLE1_STATE_ID = bpa.Field<int>("ANX_ROLE1_STATE_ID"),
                                                         ANX_ROLE2_STATE_ID = bpa.Field<int>("ANX_ROLE2_STATE_ID"),
                                                         ANX_ROLE4_STATE_ID = bpa.Field<int>("ANX_ROLE4_STATE_ID"),
                                                         ANX_ROLE8_STATE_ID = bpa.Field<int>("ANX_ROLE8_STATE_ID"),
                                                         ANX_ROLEX_CHANGED = bpa.Field<bool>("ANX_ROLEX_CHANGED"),
                                                         ANX_ROLE1_COMMENT = bpa.Field<string>("ANX_ROLE1_COMMENT"),
                                                         ANX_ROLE2_COMMENT = bpa.Field<string>("ANX_ROLE2_COMMENT"),
                                                         ANX_ROLE4_COMMENT = bpa.Field<string>("ANX_ROLE4_COMMENT"),
                                                         ANX_ROLE8_COMMENT = bpa.Field<string>("ANX_ROLE8_COMMENT"),

                                                         ANX_COMMENT = "",
                                                         ANX_OPENED = 0
                                                     }).ToList(),
                                           Documents = (from bpd in dtSet.Tables[4].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                        select new ST_RDSDocument
                                                        {
                                                            DOC_UID = bpd.Field<Guid>("DOC_UID"),
                                                            DOC_ORDER = bpd.Field<int>("DOC_ORDER"),
                                                            DOC_CLASS = bpd.Field<int>("DOC_CLASS"),
                                                            DOC_TYPE = bpd.Field<int>("DOC_TYPE"),
                                                            DOC_ORIGIN = bpd.Field<int>("DOC_ORIGIN"),
                                                            DOC_DEST = bpd.Field<int>("DOC_DEST"),
                                                            DOC_NAME = bpd.Field<string>("DOC_NAME"),
                                                            DOC_NUMBER = bpd.Field<string>("DOC_NUMBER"),
                                                            DOC_DATE = bpd.Field<DateTime>("DOC_DATE").Date.ToString("yyyyMMdd")
                                                        }).ToList(),
                                           ComunicadoAdministrativo = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 1)
                                                                       select new ST_RDSComunicado
                                                                       {
                                                                           COM_UID = bpr.Field<Guid>("COM_UID"),
                                                                           COM_NAME = bpr.Field<string>("COM_NAME"),
                                                                           COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                                           COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                                           COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                                       }).FirstOrDefault(),
                                           ComunicadoTecnico = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 2)
                                                                select new ST_RDSComunicado
                                                                {
                                                                    COM_UID = bpr.Field<Guid>("COM_UID"),
                                                                    COM_NAME = bpr.Field<string>("COM_NAME"),
                                                                    COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                                    COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                                    COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                                }).FirstOrDefault(),
                                           AnalisisTecnico = (from bpt in dtSet.Tables[6].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                              select new ST_RDSTechAnalisis
                                                              {
                                                                  SOL_UID = bpt.Field<Guid>("SOL_UID"),
                                                                  TECH_STATE_ID = 0,
                                                                  TECH_ROLE1_STATE_ID = bpt.Field<int>("TECH_ROLE1_STATE_ID"),
                                                                  TECH_ROLE2_STATE_ID = bpt.Field<int>("TECH_ROLE2_STATE_ID"),
                                                                  TECH_ROLE4_STATE_ID = bpt.Field<int>("TECH_ROLE4_STATE_ID"),
                                                                  TECH_ROLE8_STATE_ID = bpt.Field<int>("TECH_ROLE8_STATE_ID"),
                                                                  TECH_ROLE_MINTIC_STATE_ID = bpt.Field<int>("TECH_ROLE_MINTIC_STATE_ID"),
                                                                  TECH_ROLEX_CHANGED = bpt.Field<bool>("TECH_ROLEX_CHANGED"),
                                                                  TECH_ROLE1_COMMENT = bpt.Field<string>("TECH_ROLE1_COMMENT"),
                                                                  TECH_ROLE2_COMMENT = bpt.Field<string>("TECH_ROLE2_COMMENT"),
                                                                  TECH_ROLE4_COMMENT = bpt.Field<string>("TECH_ROLE4_COMMENT"),
                                                                  TECH_ROLE8_COMMENT = bpt.Field<string>("TECH_ROLE8_COMMENT"),
                                                                  TECH_ROLE_MINTIC_COMMENT = bpt.Field<string>("TECH_ROLE_MINTIC_COMMENT"),
                                                                  TECH_PTNRS_UPDATED = bpt.Field<bool>("TECH_PTNRS_UPDATED"),
                                                                  TECH_CREATED_DATE = bpt.Field<DateTime>("TECH_CREATED_DATE").Date.ToString("yyyyMMdd"),
                                                                  TECH_MODIFIED_DATE = bpt.Field<DateTime>("TECH_MODIFIED_DATE").Date.ToString("yyyyMMdd")
                                                              }).FirstOrDefault(),
                                           TechDocs = (from bptd in dtSet.Tables[7].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                       select new ST_RDSTechDoc
                                                       {
                                                           TECH_UID = bptd.Field<Guid>("TECH_UID"),
                                                           TECH_TYPE = bptd.Field<int>("TECH_TYPE"),
                                                           TECH_CONTENT_TYPE = bptd.Field<string>("TECH_CONTENT_TYPE"),
                                                           TECH_NAME = bptd.Field<string>("TECH_NAME"),
                                                           TECH_CHANGED = bptd.Field<bool>("TECH_CHANGED")
                                                       }).ToList().OrderBy(x => x.TECH_TYPE).ToList(),
                                           AnalisisResolucion = (from bpr in dtSet.Tables[8].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                                 select new ST_RDSResAnalisis
                                                                 {
                                                                     SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                                     RES_STATE_ID = 0,
                                                                     RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                                     RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                                     RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                                     RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                                     RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                                     RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                                     RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                                     RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                                     RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                                     RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                                     RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                                     RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                                     RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT")
                                                                 }).FirstOrDefault(),
                                           AnalisisViabilidad = (from bpr in dtSet.Tables[9].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                                 select new ST_RDSResAnalisis
                                                                 {
                                                                     SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                                     RES_STATE_ID = 0,
                                                                     RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                                     RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                                     RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                                     RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                                     RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                                     RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                                     RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                                     RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                                     RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                                     RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                                     RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                                     RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                                     RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT")
                                                                 }).FirstOrDefault(),
                                           TechAnexos = (from bpt in dtSet.Tables[10].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                         select new ST_RDSTechAnexos
                                                         {
                                                             ANX_UID = bpt.Field<Guid>("ANX_UID"),
                                                             ANX_NUMBER = bpt.Field<int>("ANX_NUMBER"),
                                                             ANX_STATE_ID = bpt.Field<int>("ANX_STATE_ID"),
                                                             ANX_TYPE_ID = bpt.Field<int>("ANX_TYPE_ID"),
                                                             ANX_TYPE_NAME = bpt.Field<string>("TANX_TYPE_NAME"),
                                                             ANX_NAME = bpt.Field<string>("ANX_NAME"),
                                                             ANX_ROLE1_STATE_ID = bpt.Field<int>("ANX_ROLE1_STATE_ID"),
                                                             ANX_ROLE2_STATE_ID = bpt.Field<int>("ANX_ROLE2_STATE_ID"),
                                                             ANX_ROLE4_STATE_ID = bpt.Field<int>("ANX_ROLE4_STATE_ID"),
                                                             ANX_ROLE8_STATE_ID = bpt.Field<int>("ANX_ROLE8_STATE_ID"),
                                                             ANX_ROLEX_CHANGED = bpt.Field<bool>("ANX_ROLEX_CHANGED"),
                                                             ANX_ROLE1_COMMENT = bpt.Field<string>("ANX_ROLE1_COMMENT"),
                                                             ANX_ROLE2_COMMENT = bpt.Field<string>("ANX_ROLE2_COMMENT"),
                                                             ANX_ROLE4_COMMENT = bpt.Field<string>("ANX_ROLE4_COMMENT"),
                                                             ANX_ROLE8_COMMENT = bpt.Field<string>("ANX_ROLE8_COMMENT"),

                                                             ANX_COMMENT = "",
                                                             ANX_OPENED = 0
                                                         }).ToList(),

                                       }).ToList();

                mResult.SOL_TYPES = (from bp in dtSet.Tables[11].AsEnumerable().AsQueryable()
                                     select new ST_RDSTipoSolicitud
                                     {
                                         SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                         SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                         SOL_TYPE_DESC = bp.Field<string>("SOL_TYPE_DESC"),
                                         disabled = !bp.Field<bool>("PuedeSolicitar"),
                                     }).ToList().OrderBy(bp => bp.SOL_TYPE_NAME).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }
        private ST_RDSSolicitud GetSolicitudMinTIC(ref csData cData, Guid SOL_UID, int USR_GROUP)
        {
            ST_RDSSolicitud mResult = new ST_RDSSolicitud();

            string sSQL;

            sSQL = @" 
                    Declare @SOL_UID nvarchar(36)='" + SOL_UID.ToString() + @"'
                    Declare @USR_GROUP int=" + USR_GROUP + @"
                    Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NUMBER, SERV.SERV_NAME, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
                    SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
                    SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase,
                    SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SERV_STATUS, SERV.MUNICIPIO,
                    SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA,SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
                    SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION, SERV.CUST_TXT3, 
                    FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE,
                    GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT,
                    CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL,
                    USR_ROLE1_REASIGN, USR_ROLE2_REASIGN, USR_ROLE4_REASIGN, USR_ROLE8_REASIGN, USR_ROLE16_REASIGN,
                    USR_ROLE1_REASIGN_COMMENT, USR_ROLE2_REASIGN_COMMENT, USR_ROLE4_REASIGN_COMMENT, USR_ROLE8_REASIGN_COMMENT, USR_ROLE16_REASIGN_COMMENT,
                    RES.RES_NAME, RES.RES_IS_CREATED, RES.RES_CREATED_DATE, RES.RES_MODIFIED_DATE, RES.RES_TRACKING_COUNT,
                    RESVIA.RES_NAME AS RESVIA_NAME, RESVIA.RES_IS_CREATED AS RESVIA_IS_CREATED, RESVIA.RES_CREATED_DATE AS RESVIA_CREATED_DATE, RESVIA.RES_MODIFIED_DATE AS RESVIA_MODIFIED_DATE, RESVIA.RES_TRACKING_COUNT AS RESVIA_TRACKING_COUNT,
                    RADIO.F_GetAlarmState(SOL.SOL_LAST_STATE_DATE, ALM.ALARM_PRE_EXPIRE, ALM.ALARM_EXPIRE, SOL.SOL_ENDED) SOL_ALARM_STATE,
                    DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE ,SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
                    SOL.SOL_ENDED, SOL.CAMPOS_SOL, SOL.AZ_DIRECTORIO
                    Into #SOLICITUDES
                    From RADIO.SOLICITUDES SOL
                    Join RADIO.SOL_USERS USO On SOL.SOL_UID=USO.SOL_UID And USO.USR_GROUP=@USR_GROUP
                    Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
                    Join RADIO.ESTADOS_SOL ESOL on  SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID 
                    Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID=SERV.SERV_ID
                    Join RADIO.RESOLUCIONES RES On SOL.SOL_UID=RES.SOL_UID
                    Join RADIO.RESOLUCIONES_VIABILIDAD RESVIA On SOL.SOL_UID=RESVIA.SOL_UID
                    Join RADIO.ALARMS ALM On SOL.STEP_CURRENT=ALM.ALARM_STEP
                    Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP=4
                    Where SOL.SOL_UID = @SOL_UID

	                Select * From #SOLICITUDES

                    --Usuarios--
                    SELECT SOL_UID, USR_GROUP, 
                    USR_ROLE1_ID, USR_ROLE1_NAME, USR_ROLE1_EMAIL, USR_ROLE2_ID, USR_ROLE2_NAME, USR_ROLE2_EMAIL, USR_ROLE4_ID, USR_ROLE4_NAME, USR_ROLE4_EMAIL, USR_ROLE8_ID, USR_ROLE8_NAME, USR_ROLE8_EMAIL, USR_ROLE16_ID, USR_ROLE16_NAME, USR_ROLE16_EMAIL, USR_ROLE32_ID, USR_ROLE32_NAME, USR_ROLE32_EMAIL
                    FROM RADIO.V_USUARIOS
                    WHERE SOL_UID  In (Select SOL_UID From #SOLICITUDES)

                    --Contactos--
                    SELECT SERV_ID, SERV_NUMBER, CONT_ID, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER
                    FROM RADIO.V_CONTACTOS
                    WHERE SERV_ID In (Select SERV_ID From #SOLICITUDES)

                    --Anexos--
                    SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, 
                    ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                    ANX.ANX_ROLE1_COMMENT, ANX.ANX_ROLE2_COMMENT, ANX.ANX_ROLE4_COMMENT, ANX.ANX_ROLE8_COMMENT
                    FROM RADIO.ANEXOS as ANX 
                    JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL 
                    WHERE ANX.SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                    ORDER BY ANX.ANX_NUMBER

                    --Documents--
                    SELECT DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, Convert(int, ROW_NUMBER() Over (Order By DOC_DATE)) DOC_ORDER
                    FROM RADIO.DOCUMENTS
                    WHERE SOL_UID = @SOL_UID
                    ORDER BY DOC_DATE

                    --Comunicado Administrativo y financiero--
                    Select SOL_UID, COM_UID, COM_NAME, COM_DATE, COM_CLASS, COM_TYPE
		            FROM RADIO.COMUNICADOS 
		            WHERE COM_CLASS In(1,2) And SOL_UID = @SOL_UID
                        
                    --Analisis Tecnico--
                    Select SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLEX_CHANGED,
                    TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT, TECH_ROLE_MINTIC_COMMENT, TECH_PTNRS_UPDATED, TECH_CREATED_DATE, TECH_MODIFIED_DATE
		            FROM RADIO.TECH_ANALISIS 
		            WHERE SOL_UID = @SOL_UID

                    --Documentos técnicos--
                    SELECT TECH_UID, SOL_UID, TECH_TYPE, TECH_NAME, TECH_CONTENT_TYPE, TECH_CREATED_DATE, TECH_MODIFIED_DATE, TECH_CHANGED
                    FROM RADIO.TECH_DOCUMENTS
		            WHERE SOL_UID = @SOL_UID

                    --Analisis Resolucion--
                    Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                    RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		            FROM RADIO.RESOLUCION_ANALISIS 
		            WHERE SOL_UID = @SOL_UID

                    --Analisis Resolucion Viabilidad--
                    Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                    RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		            FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD
		            WHERE SOL_UID = @SOL_UID
                    
                    --Anexos Tecnicos--
                    SELECT TANX.SOL_UID, TANX.ANX_UID, TANX.ANX_NAME, TANX.ANX_NUMBER, TANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, TANX.ANX_TYPE_ID, 
                    ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                    TANX.ANX_ROLE1_COMMENT, TANX.ANX_ROLE2_COMMENT, TANX.ANX_ROLE4_COMMENT, TANX.ANX_ROLE8_COMMENT
                    FROM RADIO.TECH_ANEXOS as TANX 
                    JOIN RADIO.TIPO_ANEXO_SOL as TA ON TANX.ANX_TYPE_ID = TA.ID_ANX_SOL 
                    WHERE TANX.SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                    ORDER BY TANX.ANX_NUMBER

	                Drop Table #SOLICITUDES
                    ";

            var dtSet = cData.ObtenerDataSet(sSQL);


            mResult = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                       select new ST_RDSSolicitud
                       {
                           SOL_UID = bp.Field<Guid>("SOL_UID"),
                           SOL_NUMBER = bp.Field<int>("SOL_NUMBER"),
                           Radicado = bp.Field<string>("RAD_ALFANET"),
                           SOL_CREATED_DATE = bp.Field<DateTime?>("SOL_CREATED_DATE").HasValue ? bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyyMMdd") : "",
                           SOL_CREATED_DATE_YEAR = bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyy"),
                           SOL_MODIFIED_DATE = bp.Field<DateTime?>("SOL_MODIFIED_DATE").HasValue ? bp.Field<DateTime>("SOL_MODIFIED_DATE").Date.ToString("yyyyMMdd") : "",
                           SOL_LAST_STATE_DATE = bp.Field<DateTime?>("SOL_LAST_STATE_DATE").HasValue ? bp.Field<DateTime>("SOL_LAST_STATE_DATE").Date.ToString("yyyyMMdd") : "",
                           SOL_REQUIRED_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DATE").Date.ToString("yyyyMMdd") : "",
                           SOL_REQUIRED_DEADLINE_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DEADLINE_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DEADLINE_DATE").Date.ToString("yyyyMMdd") : "",
                           SOL_REQUIRED_POSTPONE = bp.Field<int>("SOL_REQUIRED_POSTPONE"),
                           SOL_STATE_ID = bp.Field<int>("SOL_STATE_ID"),
                           SOL_STATE_NAME = bp.Field<string>("SOL_STATE_NAME"),
                           SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                           SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                           SOL_TYPE_CLASS = bp.Field<int>("SOL_TYPE_CLASS"),
                           SOL_IS_OTORGA = bp.Field<bool>("SOL_IS_OTORGA"),
                           SOL_ENDED = bp.Field<int>("SOL_ENDED"),
                           SOL_CREATOR = bp.Field<int>("SOL_CREATOR"),
                           SOL_ALARM_STATE = bp.Field<int>("SOL_ALARM_STATE"),
                           CAMPOS_SOL = bp.Field<string>("CAMPOS_SOL"),
                           Clasificacion = bp.Field<string>("Clasificacion"),
                           NivelCubrimiento = bp.Field<string>("NivelCubrimiento"),
                           Tecnologia = bp.Field<string>("Tecnologia"),
                           AZ_DIRECTORIO = bp.Field<string>("AZ_DIRECTORIO"),
                           DatosComunidad = new ST_RDSDatosComunidad
                           {
                               NombreComunidad = bp.Field<string>("NombreComunidad"),
                               IdLugarComunidad = bp.Field<decimal?>("IdLugarComunidad"),
                               DireccionComunidad = bp.Field<string>("DireccionComunidad"),
                               CiudadComunidad = bp.Field<string>("CiudadComunidad"),
                               DepartamentoComunidad = bp.Field<string>("DepartamentoComunidad"),
                               CodeLugarComunidad = bp.Field<string>("CodeLugarComunidad"),
                               Territorio = bp.Field<string>("Territorio"),
                               ResolucionMinInterior = bp.Field<string>("ResolucionMinInterior"),
                               GrupoEtnico = bp.Field<string>("GrupoEtnico"),
                               NombrePuebloComunidad = bp.Field<string>("NombrePuebloComunidad"),
                               NombreResguardo = bp.Field<string>("NombreResguardo"),
                               NombreCabildo = bp.Field<string>("NombreCabildo"),
                               NombreKumpania = bp.Field<string>("NombreKumpania"),
                               NombreConsejo = bp.Field<string>("NombreConsejo"),
                               NombreOrganizacionBase = bp.Field<string>("NombreOrganizacionBase"),
                           },
                           Resolucion = new ST_RDSResolucion
                           {
                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                               RES_NAME = bp.Field<string>("RES_NAME"),
                               RES_IS_CREATED = bp.Field<bool>("RES_IS_CREATED"),
                               RES_CREATED_DATE = bp.Field<DateTime>("RES_CREATED_DATE").ToString("yyyyMMdd"),
                               RES_MODIFIED_DATE = bp.Field<DateTime>("RES_MODIFIED_DATE").ToString("yyyyMMdd"),
                               RES_TRACKING_COUNT = bp.Field<int>("RES_TRACKING_COUNT")
                           },
                           Viabilidad = new ST_RDSResolucion
                           {
                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                               RES_NAME = bp.Field<string>("RESVIA_NAME"),
                               RES_IS_CREATED = bp.Field<bool>("RESVIA_IS_CREATED"),
                               RES_CREATED_DATE = bp.Field<DateTime>("RESVIA_CREATED_DATE").ToString("yyyyMMdd"),
                               RES_MODIFIED_DATE = bp.Field<DateTime>("RESVIA_MODIFIED_DATE").ToString("yyyyMMdd"),
                               RES_TRACKING_COUNT = bp.Field<int>("RESVIA_TRACKING_COUNT")
                           },
                           CURRENT = new ST_RDSCurrent
                           {
                               GROUP = bp.Field<int>("GROUP_CURRENT"),
                               ROLE = bp.Field<int>("ROLE_CURRENT"),
                               STEP = bp.Field<int>("STEP_CURRENT")
                           },
                           Contacto = new ST_RDSContacto
                           {
                               CONT_ID = bp.Field<int>("CONT_ID"),
                               CONT_NAME = bp.Field<string>("CONT_NAME"),
                               CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                               CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                               CONT_TITLE = GetContactTitle(bp.Field<int>("CONT_ROLE")),
                               CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                           },
                           USR_ADMIN = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 1)
                                        select new ST_RDSRoles
                                        {
                                            USR_ROLE1 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                            },
                                            USR_ROLE2 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                            },
                                            USR_ROLE4 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                            },
                                            USR_ROLE8 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                            },
                                            USR_ROLE16 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE16_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE16_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE16_EMAIL")
                                            },
                                            USR_ROLE32 = new ST_RDSUsuario
                                            {
                                                USR_ID = bpu.Field<int>("USR_ROLE32_ID"),
                                                USR_NAME = bpu.Field<string>("USR_ROLE32_NAME"),
                                                USR_EMAIL = bpu.Field<string>("USR_ROLE32_EMAIL")
                                            },
                                        }).FirstOrDefault(),
                           USR_TECH = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 2)
                                       select new ST_RDSRoles
                                       {
                                           USR_ROLE1 = new ST_RDSUsuario
                                           {
                                               USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                               USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                               USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                           },
                                           USR_ROLE2 = new ST_RDSUsuario
                                           {
                                               USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                               USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                               USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                           },
                                           USR_ROLE4 = new ST_RDSUsuario
                                           {
                                               USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                               USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                               USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                           },
                                           USR_ROLE8 = new ST_RDSUsuario
                                           {
                                               USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                               USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                               USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                           },
                                       }).FirstOrDefault(),
                           Reasign = new ST_RDSReasign
                           {
                               USR_ROLE1_REASIGN = bp.Field<bool>("USR_ROLE1_REASIGN"),
                               USR_ROLE2_REASIGN = bp.Field<bool>("USR_ROLE2_REASIGN"),
                               USR_ROLE4_REASIGN = bp.Field<bool>("USR_ROLE4_REASIGN"),
                               USR_ROLE8_REASIGN = bp.Field<bool>("USR_ROLE8_REASIGN"),
                               USR_ROLE16_REASIGN = bp.Field<bool>("USR_ROLE16_REASIGN"),
                               USR_ROLE1_REASIGN_COMMENT = bp.Field<string>("USR_ROLE1_REASIGN_COMMENT"),
                               USR_ROLE2_REASIGN_COMMENT = bp.Field<string>("USR_ROLE2_REASIGN_COMMENT"),
                               USR_ROLE4_REASIGN_COMMENT = bp.Field<string>("USR_ROLE4_REASIGN_COMMENT"),
                               USR_ROLE8_REASIGN_COMMENT = bp.Field<string>("USR_ROLE8_REASIGN_COMMENT"),
                               USR_ROLE16_REASIGN_COMMENT = bp.Field<string>("USR_ROLE16_REASIGN_COMMENT")
                           },
                           Financiero = new ST_RDSAnalisisFinanciero
                           {
                               FIN_SEVEN_ALDIA = bp.Field<bool?>("FIN_SEVEN_ALDIA"),
                               FIN_ESTADO_CUENTA_NUMBER = bp.Field<string>("FIN_ESTADO_CUENTA_NUMBER"),
                               FIN_ESTADO_CUENTA_DATE = bp.Field<DateTime?>("FIN_ESTADO_CUENTA_DATE").HasValue ? bp.Field<DateTime>("FIN_ESTADO_CUENTA_DATE").Date.ToString("yyyyMMdd") : "",
                               FIN_REGISTRO_ALFA_NUMBER = bp.Field<string>("FIN_REGISTRO_ALFA_NUMBER"),
                               FIN_REGISTRO_ALFA_DATE = bp.Field<DateTime?>("FIN_REGISTRO_ALFA_DATE").HasValue ? bp.Field<DateTime>("FIN_REGISTRO_ALFA_DATE").Date.ToString("yyyyMMdd") : "",
                           },
                           Expediente = new ST_RDSExpediente
                           {
                               SERV_ID = bp.Field<int>("SERV_ID"),
                               SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                               SERV_STATUS = bp.Field<string>("SERV_STATUS"),
                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                               STATE = bp.Field<string>("STATE"),
                               DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO").ToUpperInvariant() : null,
                               CITY = bp.Field<string>("CITY") != null ? bp.Field<string>("CITY").ToUpperInvariant() : null,
                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                               POWER = bp.Field<double?>("POWER"),
                               ASL = bp.Field<double?>("ASL"),
                               FREQUENCY = bp.Field<double?>("FREQUENCY"),
                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                               SERV_NAME = bp.Field<string>("SERV_NAME").ToUpperInvariant(),
                               MODULATION = bp.Field<string>("MODULATION"),
                               STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                               CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                               Operador = new ST_RDSOperador
                               {
                                   CUS_ID = bp.Field<int>("CUS_ID"),
                                   CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                   CUS_NAME = bp.Field<string>("CUS_NAME"),
                                   CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                   CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                   CUS_CITYCODE = bp.Field<string>("MUNICIPIO"),
                                   CUS_STATECODE = int.Parse(bp.Field<string>("MUNICIPIO").Substring(0, 2)).ToString(),
                                   CUS_COUNTRYCODE = "169",

                               },
                               Contactos = (from bpc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                            select new ST_RDSContacto
                                            {
                                                CONT_ID = bpc.Field<int>("CONT_ID"),
                                                CONT_NUMBER = bpc.Field<string>("CONT_NUMBER"),
                                                CONT_NAME = bpc.Field<string>("CONT_NAME"),
                                                CONT_TEL = bpc.Field<string>("CONT_TEL"),
                                                CONT_ROLE = bpc.Field<int>("CONT_ROLE"),
                                                CONT_TITLE = GetContactTitle(bpc.Field<int>("CONT_ROLE")),
                                            }).ToList()
                           },
                           Anexos = (from bpa in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                     select new ST_RDSAnexos
                                     {
                                         ANX_UID = bpa.Field<Guid>("ANX_UID"),
                                         ANX_NUMBER = bpa.Field<int>("ANX_NUMBER"),
                                         ANX_STATE_ID = bpa.Field<int>("ANX_STATE_ID"),
                                         ANX_TYPE_ID = bpa.Field<int>("ANX_TYPE_ID"),
                                         ANX_TYPE_NAME = bpa.Field<string>("ANX_TYPE_NAME"),
                                         ANX_NAME = bpa.Field<string>("ANX_NAME"),
                                         ANX_ROLE1_STATE_ID = bpa.Field<int>("ANX_ROLE1_STATE_ID"),
                                         ANX_ROLE2_STATE_ID = bpa.Field<int>("ANX_ROLE2_STATE_ID"),
                                         ANX_ROLE4_STATE_ID = bpa.Field<int>("ANX_ROLE4_STATE_ID"),
                                         ANX_ROLE8_STATE_ID = bpa.Field<int>("ANX_ROLE8_STATE_ID"),
                                         ANX_ROLEX_CHANGED = bpa.Field<bool>("ANX_ROLEX_CHANGED"),
                                         ANX_ROLE1_COMMENT = bpa.Field<string>("ANX_ROLE1_COMMENT"),
                                         ANX_ROLE2_COMMENT = bpa.Field<string>("ANX_ROLE2_COMMENT"),
                                         ANX_ROLE4_COMMENT = bpa.Field<string>("ANX_ROLE4_COMMENT"),
                                         ANX_ROLE8_COMMENT = bpa.Field<string>("ANX_ROLE8_COMMENT"),
                                         ANX_COMMENT = "",
                                         ANX_OPENED = 0
                                     }).ToList(),
                           Documents = (from bpd in dtSet.Tables[4].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                        select new ST_RDSDocument
                                        {
                                            DOC_UID = bpd.Field<Guid>("DOC_UID"),
                                            DOC_ORDER = bpd.Field<int>("DOC_ORDER"),
                                            DOC_CLASS = bpd.Field<int>("DOC_CLASS"),
                                            DOC_TYPE = bpd.Field<int>("DOC_TYPE"),
                                            DOC_ORIGIN = bpd.Field<int>("DOC_ORIGIN"),
                                            DOC_DEST = bpd.Field<int>("DOC_DEST"),
                                            DOC_NAME = bpd.Field<string>("DOC_NAME"),
                                            DOC_NUMBER = bpd.Field<string>("DOC_NUMBER"),
                                            DOC_DATE = bpd.Field<DateTime>("DOC_DATE").Date.ToString("yyyyMMdd")
                                        }).ToList(),
                           ComunicadoAdministrativo = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 1)
                                                       select new ST_RDSComunicado
                                                       {
                                                           COM_UID = bpr.Field<Guid>("COM_UID"),
                                                           COM_NAME = bpr.Field<string>("COM_NAME"),
                                                           COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                           COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                           COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                       }).FirstOrDefault(),
                           ComunicadoTecnico = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 2)
                                                select new ST_RDSComunicado
                                                {
                                                    COM_UID = bpr.Field<Guid>("COM_UID"),
                                                    COM_NAME = bpr.Field<string>("COM_NAME"),
                                                    COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                    COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                    COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                }).FirstOrDefault(),
                           AnalisisTecnico = (from bpt in dtSet.Tables[6].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                              select new ST_RDSTechAnalisis
                                              {
                                                  SOL_UID = bpt.Field<Guid>("SOL_UID"),
                                                  TECH_STATE_ID = 0,
                                                  TECH_ROLE1_STATE_ID = bpt.Field<int>("TECH_ROLE1_STATE_ID"),
                                                  TECH_ROLE2_STATE_ID = bpt.Field<int>("TECH_ROLE2_STATE_ID"),
                                                  TECH_ROLE4_STATE_ID = bpt.Field<int>("TECH_ROLE4_STATE_ID"),
                                                  TECH_ROLE8_STATE_ID = bpt.Field<int>("TECH_ROLE8_STATE_ID"),
                                                  TECH_ROLE_MINTIC_STATE_ID = bpt.Field<int>("TECH_ROLE_MINTIC_STATE_ID"),
                                                  TECH_ROLEX_CHANGED = bpt.Field<bool>("TECH_ROLEX_CHANGED"),
                                                  TECH_ROLE1_COMMENT = bpt.Field<string>("TECH_ROLE1_COMMENT"),
                                                  TECH_ROLE2_COMMENT = bpt.Field<string>("TECH_ROLE2_COMMENT"),
                                                  TECH_ROLE4_COMMENT = bpt.Field<string>("TECH_ROLE4_COMMENT"),
                                                  TECH_ROLE8_COMMENT = bpt.Field<string>("TECH_ROLE8_COMMENT"),
                                                  TECH_ROLE_MINTIC_COMMENT = bpt.Field<string>("TECH_ROLE_MINTIC_COMMENT"),
                                                  TECH_PTNRS_UPDATED = bpt.Field<bool>("TECH_PTNRS_UPDATED"),
                                                  TECH_CREATED_DATE = bpt.Field<DateTime>("TECH_CREATED_DATE").Date.ToString("yyyyMMdd"),
                                                  TECH_MODIFIED_DATE = bpt.Field<DateTime>("TECH_MODIFIED_DATE").Date.ToString("yyyyMMdd")
                                              }).FirstOrDefault(),
                           TechDocs = (from bptd in dtSet.Tables[7].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                       select new ST_RDSTechDoc
                                       {
                                           TECH_UID = bptd.Field<Guid>("TECH_UID"),
                                           TECH_TYPE = bptd.Field<int>("TECH_TYPE"),
                                           TECH_CONTENT_TYPE = bptd.Field<string>("TECH_CONTENT_TYPE"),
                                           TECH_NAME = bptd.Field<string>("TECH_NAME"),
                                           TECH_CHANGED = bptd.Field<bool>("TECH_CHANGED")
                                       }).ToList(),
                           AnalisisResolucion = (from bpr in dtSet.Tables[8].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                 select new ST_RDSResAnalisis
                                                 {
                                                     SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                     RES_STATE_ID = 0,
                                                     RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                     RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                     RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                     RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                     RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                     RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                     RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                     RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                     RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                     RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                     RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                     RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                     RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT"),
                                                 }).FirstOrDefault(),
                           AnalisisViabilidad = (from bpr in dtSet.Tables[9].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                 select new ST_RDSResAnalisis
                                                 {
                                                     SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                     RES_STATE_ID = 0,
                                                     RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                     RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                     RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                     RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                     RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                     RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                     RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                     RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                     RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                     RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                     RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                     RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                     RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT"),
                                                 }).FirstOrDefault(),
                           TechAnexos = (from bpt in dtSet.Tables[10].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                         select new ST_RDSTechAnexos
                                         {
                                             ANX_UID = bpt.Field<Guid>("ANX_UID"),
                                             ANX_NUMBER = bpt.Field<int>("ANX_NUMBER"),
                                             ANX_STATE_ID = bpt.Field<int>("ANX_STATE_ID"),
                                             ANX_TYPE_ID = bpt.Field<int>("ANX_TYPE_ID"),
                                             ANX_TYPE_NAME = bpt.Field<string>("ANX_TYPE_NAME"),
                                             ANX_NAME = bpt.Field<string>("ANX_NAME"),
                                             ANX_ROLE1_STATE_ID = bpt.Field<int>("ANX_ROLE1_STATE_ID"),
                                             ANX_ROLE2_STATE_ID = bpt.Field<int>("ANX_ROLE2_STATE_ID"),
                                             ANX_ROLE4_STATE_ID = bpt.Field<int>("ANX_ROLE4_STATE_ID"),
                                             ANX_ROLE8_STATE_ID = bpt.Field<int>("ANX_ROLE8_STATE_ID"),
                                             ANX_ROLEX_CHANGED = bpt.Field<bool>("ANX_ROLEX_CHANGED"),
                                             ANX_ROLE1_COMMENT = bpt.Field<string>("ANX_ROLE1_COMMENT"),
                                             ANX_ROLE2_COMMENT = bpt.Field<string>("ANX_ROLE2_COMMENT"),
                                             ANX_ROLE4_COMMENT = bpt.Field<string>("ANX_ROLE4_COMMENT"),
                                             ANX_ROLE8_COMMENT = bpt.Field<string>("ANX_ROLE8_COMMENT"),
                                             ANX_COMMENT = "",
                                             ANX_OPENED = 0
                                         }).ToList(),
                       }).ToList().FirstOrDefault();

            return mResult;

        }

        public ST_RDSExpedientes GetExpedientes(string Nit)
        {
            int IDUserWeb = 1;
            csData cData = null;

            ST_RDSExpedientes mResult = new ST_RDSExpedientes();
            mResult.Expedientes = new List<ST_RDSExpediente>();

            string Parametros = "IDUserWeb='" + IDUserWeb + "',Nit='" + Nit + "'";

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();

                if (!cData.Connect(strConn))
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Nit=" + Nit, "!ConectedOk", strConn);
                    return mResult;
                }


                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Declare @nit nvarchar(32)='" + Nit + @"'

                        --Expediente--
                        Select SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_IDENT, STATION_CLASS, STATE, DEPTO, CITY, CODE_AREA_MUNICIPIO, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, MODULATION,
                        SERV.CUS_ID, SERV.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SERV.CUST_TXT3, SERV.CODE_AREA_MUNICIPIO, SERV.MUNICIPIO
                        From RADIO.V_EXPEDIENTES SERV
						Where CUS_IDENT=@nit And SERV.SERV_STATUS In('eSOL', 'eAUT', 'eRES', 'eCT', 'eVEN')

                        --Cubrimiento--
                        SELECT SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_IDENT, STATION_CLASS, STATE, DEPTO, CITY, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, NORTH, EAST, LATITUDE, LONGITUDE, LATITUDEGSM, LONGITUDEGSM, ORIGIN, DESIG_EMISSION, PWR_ANT, LOSSES, AGL, GAIN, AZIMUTH, ADDRESS, MODULATION
                        FROM RADIO.V_CUBRIMIENTO
						Where CUS_IDENT=@nit

                        --Punto a Punto Cuabrimiento y Transmovil--
                        SELECT SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_IDENT, DESIG_EMISSION, CLASS, 
                        DEPTO_A, CITY_A, ADDRESS_A, LATITUDE_A, LONGITUDE_A, LATITUDEGSM_A, LONGITUDEGSM_A, FREQUENCY_A, POWER_A, ASL_A, AGL_A, GAIN_A, AZIMUTH_A, TILT_A, POLAR_A, 
                        DEPTO_B, CITY_B, ADDRESS_B, LATITUDE_B, LONGITUDE_B, LATITUDEGSM_B, LONGITUDEGSM_B, FREQUENCY_B, POWER_B, ASL_B, AGL_B, GAIN_B, AZIMUTH_B, TILT_B, POLAR_B, TRASMOVIL
                        FROM RADIO.V_PUNTOAPUNTO
						Where CUS_IDENT=@nit

                        --Transmoviles--
                        SELECT SERV_ID,SERV_NUMBER, LIC_NUMBER, CATEGORY, CARACT, TX_LOW_FREQ, TX_HIGH_FREQ, RX_LOW_FREQ, RX_HIGH_FREQ, 
                        DESIG_EMISSION, BW, PWR_ANT, POWER, AZIMUTH, GAIN, AGL, ADDRESS, LONGITUDE, LATITUDE
                        FROM RADIO.V_TRANSMOVIL
						Where CUS_IDENT=@nit
                       
                        --Contactos--
                        SELECT SERV_ID, SERV_NUMBER, CONT_ID, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER
                        FROM RADIO.V_CONTACTOS
						Where CUS_IDENT=@nit

                        --Tipos de solicitud--
                        Select SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_DESC
                        From RADIO.TIPOS_SOL 
                        Where SOL_IMPLEMENTED=1 AND SOL_IS_OTORGA=0
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                mResult.Expedientes = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                           select new ST_RDSExpediente
                           {
                               SERV_ID = bp.Field<int>("SERV_ID"),
                               SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                               STATE = bp.Field<string>("STATE"),
                               DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO").ToUpperInvariant() : null,
                               CITY = bp.Field<string>("CITY") != null ? bp.Field<string>("CITY").ToUpperInvariant() : null,
                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                               POWER = bp.Field<double?>("POWER"),
                               ASL = bp.Field<double?>("ASL"),
                               FREQUENCY = bp.Field<double?>("FREQUENCY"),
                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                               SERV_NAME = bp.Field<string>("SERV_NAME").ToUpperInvariant(),
                               MODULATION = bp.Field<string>("MODULATION"),
                               STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                               CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                               Operador = new ST_RDSOperador
                               {
                                   CUS_ID = bp.Field<int>("CUS_ID"),
                                   CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                   CUS_NAME = bp.Field<string>("CUS_NAME"),
                                   CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                   CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                   CUS_COUNTRYCODE = "169",
                                   CUS_STATECODE = bp.Field<string>("MUNICIPIO").Substring(0, 2).TrimStart('0'),
                                   CUS_CITYCODE = bp.Field<string>("MUNICIPIO").TrimStart('0'),
                               },
                               RedesCubrimiento = (from bpc in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                                   select new infoTecnicaCubrimiento
                                                   {

                                                       LIC_NUMBER = bpc.Field<int>("LIC_NUMBER"),
                                                       AZIMUTH = bpc.Field<decimal?>("AZIMUTH"),
                                                       ASL = bpc.Field<double?>("ASL"),
                                                       AGL = bpc.Field<decimal?>("AGL"),
                                                       STATION_CLASS = bpc.Field<string>("STATION_CLASS"),
                                                       LATITUDE = bpc.Field<decimal?>("LATITUDE"),
                                                       LONGITUDE = bpc.Field<decimal?>("LONGITUDE"),
                                                       LATITUDEGSM = bpc.Field<string>("LATITUDEGSM"),
                                                       LONGITUDEGSM = bpc.Field<string>("LONGITUDEGSM"),
                                                       NORTH = bpc.Field<decimal?>("NORTH"),
                                                       EAST = bpc.Field<decimal?>("EAST"),
                                                       DESIG_EMISSION = bpc.Field<string>("DESIG_EMISSION"),
                                                       MODULATION = bpc.Field<string>("MODULATION"),
                                                       GAIN = bpc.Field<decimal?>("GAIN"),
                                                       ORIGIN = bpc.Field<string>("ORIGIN"),
                                                       LOSSES = bpc.Field<decimal?>("LOSSES"),
                                                       POWER = bpc.Field<double?>("POWER"),
                                                       PWR_ANT = bpc.Field<decimal?>("PWR_ANT"),
                                                       
                                                   }).ToList(),
                                                   
                               RedesPPCubrimiento = (from bpp in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID") && x.Field<int>("TRASMOVIL") == 0)
                                                     select new infoTecnicaPaP
                                                     {
                                                         LIC_NUMBER = bpp.Field<int>("LIC_NUMBER"),
                                                         CLASS = bpp.Field<string>("CLASS"),
                                                         DESIG_EMISSION = bpp.Field<string>("DESIG_EMISSION"),
                                                         AZIMUTH_A = bpp.Field<decimal?>("AZIMUTH_A"),
                                                         AZIMUTH_B = bpp.Field<decimal?>("AZIMUTH_B"),
                                                         ASL_A = bpp.Field<decimal?>("ASL_A"),
                                                         ASL_B = bpp.Field<decimal?>("ASL_B"),
                                                         AGL_A = bpp.Field<decimal?>("AGL_A"),
                                                         AGL_B = bpp.Field<decimal?>("AGL_B"),
                                                         CITY_A = bpp.Field<string>("CITY_A"),
                                                         CITY_B = bpp.Field<string>("CITY_B"),
                                                         DEPTO_A = bpp.Field<string>("DEPTO_A"),
                                                         DEPTO_B = bpp.Field<string>("DEPTO_B"),
                                                         ADDRESS_A = bpp.Field<string>("ADDRESS_A"),
                                                         ADDRESS_B = bpp.Field<string>("ADDRESS_B"),
                                                         TILT_A = bpp.Field<decimal?>("TILT_A"),
                                                         TILT_B = bpp.Field<decimal?>("TILT_B"),
                                                         FREQUENCY_A = bpp.Field<decimal?>("FREQUENCY_A"),
                                                         FREQUENCY_B = bpp.Field<decimal?>("FREQUENCY_B"),
                                                         GAIN_A = bpp.Field<decimal?>("GAIN_A"),
                                                         GAIN_B = bpp.Field<decimal?>("GAIN_B"),
                                                         POLAR_A = bpp.Field<string>("POLAR_A"),
                                                         POLAR_B = bpp.Field<string>("POLAR_B"),
                                                         POWER_A = bpp.Field<decimal?>("POWER_A"),
                                                         POWER_B = bpp.Field<decimal?>("POWER_B"),
                                                         LATITUDE_A = bpp.Field<decimal?>("LATITUDE_A"),
                                                         LATITUDE_B = bpp.Field<decimal?>("LATITUDE_B"),
                                                         LONGITUDE_A = bpp.Field<decimal?>("LONGITUDE_A"),
                                                         LONGITUDE_B = bpp.Field<decimal?>("LONGITUDE_B"),
                                                         LATITUDEGSM_A = bpp.Field<string>("LATITUDEGSM_A"),
                                                         LATITUDEGSM_B = bpp.Field<string>("LATITUDEGSM_B"),
                                                         LONGITUDEGSM_A = bpp.Field<string>("LONGITUDEGSM_A"),
                                                         LONGITUDEGSM_B = bpp.Field<string>("LONGITUDEGSM_B")
                                                     }).ToList(),
                               RedesPPTranmoviles = (from bpp in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID") && x.Field<int>("TRASMOVIL") == 1)
                                                     select new infoTecnicaPaP
                                                     {
                                                         LIC_NUMBER = bpp.Field<int>("LIC_NUMBER"),
                                                         CLASS = bpp.Field<string>("CLASS"),
                                                         DESIG_EMISSION = bpp.Field<string>("DESIG_EMISSION"),
                                                         AZIMUTH_A = bpp.Field<decimal?>("AZIMUTH_A"),
                                                         AZIMUTH_B = bpp.Field<decimal?>("AZIMUTH_B"),
                                                         ASL_A = bpp.Field<decimal?>("ASL_A"),
                                                         ASL_B = bpp.Field<decimal?>("ASL_B"),
                                                         AGL_A = bpp.Field<decimal?>("AGL_A"),
                                                         AGL_B = bpp.Field<decimal?>("AGL_B"),
                                                         CITY_A = bpp.Field<string>("CITY_A"),
                                                         CITY_B = bpp.Field<string>("CITY_B"),
                                                         DEPTO_A = bpp.Field<string>("DEPTO_A"),
                                                         DEPTO_B = bpp.Field<string>("DEPTO_B"),
                                                         ADDRESS_A = bpp.Field<string>("ADDRESS_A"),
                                                         ADDRESS_B = bpp.Field<string>("ADDRESS_B"),
                                                         TILT_A = bpp.Field<decimal?>("TILT_A"),
                                                         TILT_B = bpp.Field<decimal?>("TILT_B"),
                                                         FREQUENCY_A = bpp.Field<decimal?>("FREQUENCY_A"),
                                                         FREQUENCY_B = bpp.Field<decimal?>("FREQUENCY_B"),
                                                         GAIN_A = bpp.Field<decimal?>("GAIN_A"),
                                                         GAIN_B = bpp.Field<decimal?>("GAIN_B"),
                                                         POLAR_A = bpp.Field<string>("POLAR_A"),
                                                         POLAR_B = bpp.Field<string>("POLAR_B"),
                                                         POWER_A = bpp.Field<decimal?>("POWER_A"),
                                                         POWER_B = bpp.Field<decimal?>("POWER_B"),
                                                         LATITUDE_A = bpp.Field<decimal?>("LATITUDE_A"),
                                                         LATITUDE_B = bpp.Field<decimal?>("LATITUDE_B"),
                                                         LONGITUDE_A = bpp.Field<decimal?>("LONGITUDE_A"),
                                                         LONGITUDE_B = bpp.Field<decimal?>("LONGITUDE_B"),
                                                         LATITUDEGSM_A = bpp.Field<string>("LATITUDEGSM_A"),
                                                         LATITUDEGSM_B = bpp.Field<string>("LATITUDEGSM_B"),
                                                         LONGITUDEGSM_A = bpp.Field<string>("LONGITUDEGSM_A"),
                                                         LONGITUDEGSM_B = bpp.Field<string>("LONGITUDEGSM_B")
                                                     }).ToList(),
                               RedesTranmoviles = (from bpt in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                                   select new infoTecnicaTransmovil
                                                   {
                                                       LIC_NUMBER = bpt.Field<int>("LIC_NUMBER"),
                                                       CATEGORY = bpt.Field<string>("CATEGORY"),
                                                       CARACT = bpt.Field<string>("CARACT"),
                                                       AGL = bpt.Field<decimal?>("AGL"),
                                                       BW = bpt.Field<decimal?>("BW"),
                                                       AZIMUTH = bpt.Field<decimal?>("AZIMUTH"),
                                                       ADDRESS = bpt.Field<string>("ADDRESS"),
                                                       DESIG_EMISSION = bpt.Field<string>("DESIG_EMISSION"),
                                                       TX_LOW_FREQ = bpt.Field<decimal?>("TX_LOW_FREQ"),
                                                       TX_HIGH_FREQ = bpt.Field<decimal?>("TX_HIGH_FREQ"),
                                                       RX_LOW_FREQ = bpt.Field<decimal?>("RX_LOW_FREQ"),
                                                       RX_HIGH_FREQ = bpt.Field<decimal?>("RX_HIGH_FREQ"),
                                                       GAIN = bpt.Field<decimal?>("GAIN"),
                                                       LATITUDE = bpt.Field<decimal?>("LATITUDE"),
                                                       LONGITUDE = bpt.Field<decimal?>("LONGITUDE"),
                                                       POWER = bpt.Field<decimal?>("POWER"),
                                                       PWR_ANT = bpt.Field<decimal?>("PWR_ANT")
                                                   }).ToList(),
                               Contactos = (from bpc in dtSet.Tables[4].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                            select new ST_RDSContacto
                                            {
                                                CONT_ID = bpc.Field<int>("CONT_ID"),
                                                CONT_NUMBER = bpc.Field<string>("CONT_NUMBER"),
                                                CONT_NAME = bpc.Field<string>("CONT_NAME"),
                                                CONT_TEL = bpc.Field<string>("CONT_TEL"),
                                                CONT_ROLE = bpc.Field<int>("CONT_ROLE"),
                                                CONT_TITLE = GetContactTitle(bpc.Field<int>("CONT_ROLE")),
                                            }).ToList()
                           }).ToList();

                mResult.SOL_TYPES = (from bp in dtSet.Tables[5].AsEnumerable().AsQueryable()
                                     select new ST_RDSTipoSolicitud
                                     {
                                         SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                         SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                         SOL_TYPE_DESC = bp.Field<string>("SOL_TYPE_DESC")
                                     }).ToList().OrderBy(bp => bp.SOL_TYPE_NAME).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSExpediente GetExpediente(string ExpedienteCodigo)
        {
            int IDUserWeb = 1;
            csData cData = null;

            ST_RDSExpediente mResult = new ST_RDSExpediente();

            string Parametros = "IDUserWeb='" + IDUserWeb + "',ExpedienteCodigo='" + ExpedienteCodigo + "'";

            try
            {

                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();

                if (!cData.Connect(strConn))
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "ExpedienteCodigo=" + ExpedienteCodigo, "!ConectedOk", strConn);
                    return mResult;
                }


                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Declare @ExpedienteCodigo nvarchar(50)='" + ExpedienteCodigo + @"'

                        --Expediente--
                        Select SERV_ID, SERV_NUMBER, LIC_NUMBER, STATION_CLASS, STATE, DEPTO, CITY, CODE_AREA_MUNICIPIO, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, MODULATION,
                        SERV.CUS_ID, SERV.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SERV.CUST_TXT3
                        From RADIO.V_EXPEDIENTES SERV
                        WHERE SERV.SERV_NUMBER = @ExpedienteCodigo And SERV.SERV_STATUS In('eSOL', 'eAUT', 'eRES', 'eCT', 'eVEN')

                        --Contactos--
                        SELECT SERV_ID, SERV_NUMBER, CONT_ID, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER
                        FROM RADIO.V_CONTACTOS
                        WHERE SERV_NUMBER = @ExpedienteCodigo
                    ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                mResult = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                           select new ST_RDSExpediente
                           {
                               SERV_ID = bp.Field<int>("SERV_ID"),
                               SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                               STATE = bp.Field<string>("STATE"),
                               DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO").ToUpperInvariant() : null,
                               CITY = bp.Field<string>("CITY") != null ? bp.Field<string>("CITY").ToUpperInvariant() : null,
                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                               POWER = bp.Field<double?>("POWER"),
                               ASL = bp.Field<double?>("ASL"),
                               FREQUENCY = bp.Field<double?>("FREQUENCY"),
                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                               SERV_NAME = bp.Field<string>("SERV_NAME").ToUpperInvariant(),
                               MODULATION = bp.Field<string>("MODULATION"),
                               STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                               CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                               Contactos = (from bpc in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                            select new ST_RDSContacto
                                            {
                                                CONT_ID = bpc.Field<int>("CONT_ID"),
                                                CONT_NUMBER = bpc.Field<string>("CONT_NUMBER"),
                                                CONT_NAME = bpc.Field<string>("CONT_NAME"),
                                                CONT_TEL = bpc.Field<string>("CONT_TEL"),
                                                CONT_ROLE = bpc.Field<int>("CONT_ROLE"),
                                                CONT_TITLE = GetContactTitle(bpc.Field<int>("CONT_ROLE")),
                                            }).ToList().OrderBy(bp1 => bp1.CONT_ROLE).ToList(),
                               Operador = new ST_RDSOperador
                               {
                                   CUS_ID = bp.Field<int>("CUS_ID"),
                                   CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                   CUS_NAME = bp.Field<string>("CUS_NAME"),
                                   CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                   CUS_BRANCH = bp.Field<int>("CUS_BRANCH")
                               }
                           }).ToList().FirstOrDefault();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSResult GetRadicado(string RadicadoCodigo)
        {
            int IDUserWeb = 1;

            csData cData = null;

            string Parametros = "IDUserWeb='" + IDUserWeb + "',RadicadoCodigo='" + RadicadoCodigo + "'";

            ST_RDSResult mResult = new ST_RDSResult();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    mResult.Error = true;
                    return mResult;
                }

                ValidateToken(ref cData);

                bool ExisteRadicado = cData.TraerValor("Select Count(*) From RADIO.SOLICITUDES WHERE RAD_ALFANET='" + RadicadoCodigo + "'", 0) > 0;

                if (ExisteRadicado)
                {
                    mResult.Estado = false;
                    mResult.EstadoMessage = "El número de radicado ya existe en el sistema SGE.";
                    return mResult;

                }

                if (int.Parse(RadicadoCodigo) < 211000001)
                {
                    ResultadoComunicadosXTramiteAlfa resultado = AlfaComunicadosXTramite(RadicadoCodigo, string.Empty);

                    if (resultado.OcurrioError == true)
                    {
                        mResult.Estado = false;
                        mResult.EstadoMessage = "El número de radicado no fue encontrado en el sistema Alfa";
                        return mResult;
                    }

                    mResult.Estado = resultado.Comunicados.Where(bp => bp.TipoDocumento == TipoDocumentoAlfa.RadicadoEntrada && bp.NumeroDocumento.Equals(RadicadoCodigo)).Any();

                    if (!mResult.Estado)
                    {
                        mResult.EstadoMessage = "El número de radicado no fue encontrado en el sistema Alfa";
                    }
                }
                else
                {
                    AZTools AZTools = new AZTools();
                    AZRadicar.InformacionRadicado resultado = AZTools.AZConsultaRadicado(RadicadoCodigo);
                    if (resultado == null)
                    {
                        mResult.Estado = false;
                        mResult.EstadoMessage = "El número de radicado no fue encontrado en el sistema AzTools";
                        return mResult;
                    }
                    else
                        mResult.Estado = true;
                }

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                mResult.Error = true;
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo CrearConcesionarioSolicitud(ST_RDSTipoSolicitud SOL_TYPE, ST_RDSExpediente Expediente, ST_RDSContacto Contacto, List<string> Emails, List<ST_RDSFiles_Up> Files)
        {
            int IDUserWeb = 1;

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", SOL_TYPE=" + Serializer.Serialize(SOL_TYPE) + ", Expediente=" + Serializer.Serialize(Expediente) + ", Contacto=" + Serializer.Serialize(Contacto);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                AZTools AZTools = new AZTools();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;

                        SELECT isnull(Max(SOL_NUMBER), 0) + 1 SOL_NUMBER From RADIO.SOLICITUDES SOL where CUS_IDENT='" + Expediente.Operador.CUS_IDENT + "'";

                var dtSet0 = cData.ObtenerDataSet(sSQL);

                Helper.TXT = (from bp in dtSet0.Tables[0].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                int SOL_NUMBER = dtSet0.Tables[1].AsEnumerable().AsQueryable().First().Field<int>("SOL_NUMBER");

                sSQL = @"Select Top 0 SOL_UID, CUS_ID, CUS_IDENT, CUS_NAME, SERV_ID, SERV_NUMBER, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, GROUP_CURRENT, ROLE_CURRENT, SOL_CREATOR, AZ_DIRECTORIO
                        From RADIO.SOLICITUDES;

                        Select Top 0 SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID, USR_ROLE16_ID, USR_ROLE32_ID
                        From RADIO.SOL_USERS;

                        Select Top 0 ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_ROLE1_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, HABILITANTES
                        From RADIO.ANEXOS;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        Select Top 0 SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT
		                FROM RADIO.TECH_ANALISIS; 

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES;

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES_VIABILIDAD;
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTSolUsers = dtSet.Tables[1];
                DataTable DTAnexo = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTTech = dtSet.Tables[4];
                DataTable DTResAnalisis = dtSet.Tables[5];
                DataTable DTResolucion = dtSet.Tables[6];
                DataTable DTResAnalisisViabilidad = dtSet.Tables[7];
                DataTable DTResolucionViabilidad = dtSet.Tables[8];

                Guid SOL_UID = Guid.NewGuid();

                DataRow DRS = DTSolicitud.NewRow();
                DRS["SOL_UID"] = SOL_UID;
                DRS["CUS_ID"] = Expediente.Operador.CUS_ID;
                DRS["CUS_IDENT"] = Expediente.Operador.CUS_IDENT;
                DRS["CUS_NAME"] = Expediente.Operador.CUS_NAME;
                DRS["SERV_ID"] = Expediente.SERV_ID;
                DRS["SERV_NUMBER"] = Expediente.SERV_NUMBER;
                DRS["SOL_TYPE_ID"] = SOL_TYPE.SOL_TYPE_ID;
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.SinRadicar;
                DRS["SOL_CREATOR"] = (int)SOL_CREATOR.Concesionario;

                Contacto.CONT_EMAIL = string.Join(",", Emails.Where(bp => !string.IsNullOrEmpty(bp)).ToList());

                DRS["CONT_ID"] = Contacto.CONT_ID;
                DRS["CONT_NAME"] = Contacto.CONT_NAME;
                DRS["CONT_NUMBER"] = Contacto.CONT_NUMBER;
                DRS["CONT_EMAIL"] = Contacto.CONT_EMAIL;
                DRS["CONT_ROLE"] = Contacto.CONT_ROLE;

                DRS["SOL_NUMBER"] = SOL_NUMBER;
                DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;

                DRS["CAMPOS_SOL"] = "";
                DTSolicitud.Rows.Add(DRS);

                List<ST_RDSUsuario> AsignacionesGroup1 = AutomaticAssignGroup1_V1(ref cData, ref DTSolUsers, ref Helper, SOL_UID);

                CrearAnalisisTecnico(ref DTTech, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisis, SOL_UID);
                CrearResolucion(ref DTResolucion, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisisViabilidad, SOL_UID);
                CrearResolucion(ref DTResolucionViabilidad, SOL_UID);

                int ANX_NUMBER = 1;
                foreach (var File in Files)
                {
                    DataRow DRA = DTAnexo.NewRow();
                    DRA["ANX_UID"] = Guid.NewGuid();
                    DRA["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_CREATED_DATE"] = DateTime.Now;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRA["SOL_UID"] = SOL_UID;
                    DRA["ANX_NUMBER"] = ANX_NUMBER++;
                    DRA["HABILITANTES"] = true; 
                    DTAnexo.Rows.Add(DRA);
                }

                ST_RDSSolicitud Solicitud = MakeSolicitud(SOL_UID, SOL_NUMBER, "#####", SOL_TYPE.SOL_TYPE_NAME, ref Expediente, ref Contacto);

                Solicitud.USR_ADMIN.USR_ROLE1 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First();
                Solicitud.USR_ADMIN.USR_ROLE4 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First();
                Solicitud.USR_ADMIN.USR_ROLE8 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                Solicitud.USR_ADMIN.USR_ROLE16 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First();
                Solicitud.USR_ADMIN.USR_ROLE32 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First();
                
                MemoryStream mem=null;
                //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticSolicitar(ref Solicitud, Files, ref Helper, ref mem); //OscarR                
                AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticSolicitar(ref Solicitud, Files, ref Helper, ref mem);
                //Solicitud.Radicado = resultado.CodigoRadicado;
                Solicitud.Radicado = resultado.NuevoRaNumero;

                DTSolicitud.Rows[0]["RAD_ALFANET"] = resultado.NuevoRaNumero;
                //DTSolicitud.Rows[0]["RAD_ALFANET"] = resultado.CodigoRadicado;
                DTSolicitud.Rows[0]["AZ_DIRECTORIO"] = Solicitud.AZ_DIRECTORIO;
                DTSolicitud.Rows[0]["SOL_CREATED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_MODIFIED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_LAST_STATE_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_STATE_ID"] = (int)SOL_STATES.Radicada;

                AddNewDocument(ref DTDocuments, SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Solicitud, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, resultado.NuevoRaNumero, ref mem, "Solicitud " + SOL_TYPE.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_CREADA, ref Solicitud, ref Helper);
                Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_ASSIGNED, ref Solicitud, ref Helper);

                string ResultMessage = GetMensajeAlertaSolicitud(SOL_TYPE.SOL_TYPE_NAME, resultado.NuevoRaNumero);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                
                List<ST_RDSLog>lstLogs=new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_SOL_CONCESIONARIO_CREATE", LOG_SOURCE.Concesionario, ROLES.NoAplica, "", resultado.NuevoRaNumero));
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", ""));
                AddLogAutomaticAssign(ref DynamicSolicitud, SOL_UID, ref lstLogs, LOG_SOURCE.MinTIC, ref AsignacionesGroup1, ref Helper, ROLES.Funcionario, ROLES.Coordinador, ROLES.Subdirector, ROLES.Asesor, ROLES.Director);
                AddLog(ref cData, lstLogs);

                return new ActionInfo(1, ResultMessage);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo CrearFuncionarioSolicitudProrroga(int IDUserWeb, string RadicadoCodigo, string RadicadoFecha, ST_RDSExpediente Expediente, bool bEnviarCorreoConcesionario, List<ST_RDSFiles_Up> Files)
        {

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", RadicadoCodigo=" + RadicadoCodigo + ", RadicadoFecha=" + RadicadoFecha + ", Expediente=" + Serializer.Serialize(Expediente);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;

                        SELECT SOL_TYPE_ID, SOL_TYPE_NAME, SOL_TYPE_DESC FROM RADIO.TIPOS_SOL WHERE SOL_TYPE_ID=7;

                        SELECT isnull(Max(SOL_NUMBER), 0) + 1 SOL_NUMBER From RADIO.SOLICITUDES SOL where SOL.CUS_IDENT='" + Expediente.Operador.CUS_IDENT + "'";

                var dtSet0 = cData.ObtenerDataSet(sSQL);

                Helper.TXT = (from bp in dtSet0.Tables[0].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                ST_RDSTipoSolicitud SOL_TYPE = (from bp in dtSet0.Tables[1].AsEnumerable().AsQueryable()
                                                select new ST_RDSTipoSolicitud
                                                {
                                                    SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                                                    SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                                                    SOL_TYPE_DESC = bp.Field<string>("SOL_TYPE_DESC")
                                                }).FirstOrDefault();

                int SOL_NUMBER = dtSet0.Tables[2].AsEnumerable().AsQueryable().First().Field<int>("SOL_NUMBER");

                sSQL = @"Select Top 0 SOL_UID, CUS_ID, CUS_IDENT, CUS_NAME, SERV_ID, SERV_NUMBER, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_NUMBER, CONT_EMAIL, CONT_ROLE, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, GROUP_CURRENT, ROLE_CURRENT, SOL_CREATOR
                        From RADIO.SOLICITUDES;

                        Select Top 0 SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID, USR_ROLE16_ID, USR_ROLE32_ID
                        From RADIO.SOL_USERS;

                        Select Top 0 ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_ROLE1_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE
                        From RADIO.ANEXOS;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        Select Top 0 SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT
		                FROM RADIO.TECH_ANALISIS;

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS; 

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES;

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD; 

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES_VIABILIDAD;

                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTSolUsers = dtSet.Tables[1];
                DataTable DTAnexo = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTTech = dtSet.Tables[4];
                DataTable DTResAnalisis = dtSet.Tables[5];
                DataTable DTResolucion = dtSet.Tables[6];
                DataTable DTResAnalisisViabilidad = dtSet.Tables[7];
                DataTable DTResolucionViabilidad = dtSet.Tables[8];

                ST_RDSContacto Contacto = Expediente.Contactos.OrderByDescending(bp => bp.CONT_ROLE).FirstOrDefault();

                Guid SOL_UID = Guid.NewGuid();

                DataRow DRS = DTSolicitud.NewRow();
                DRS["SOL_UID"] = SOL_UID;
                DRS["CUS_ID"] = Expediente.Operador.CUS_ID;
                DRS["CUS_IDENT"] = Expediente.Operador.CUS_IDENT;
                DRS["CUS_NAME"] = Expediente.Operador.CUS_NAME;
                DRS["SERV_ID"] = Expediente.SERV_ID;
                DRS["SERV_NUMBER"] = Expediente.SERV_NUMBER;
                DRS["RAD_ALFANET"] = RadicadoCodigo;
                DRS["SOL_CREATED_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_MODIFIED_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_LAST_STATE_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_TYPE_ID"] = (int)SOL_TYPES.ProrrogaConcesion;
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.Radicada;
                DRS["SOL_CREATOR"] = (int)SOL_CREATOR.FuncionarioMinTIC;

                DRS["CONT_ID"] = Contacto.CONT_ID;
                DRS["CONT_NAME"] = Contacto.CONT_NAME;
                DRS["CONT_NUMBER"] = Contacto.CONT_NUMBER;
                DRS["CONT_EMAIL"] = Expediente.Operador.CUS_EMAIL;
                DRS["CONT_ROLE"] = Contacto.CONT_ROLE;

                DRS["SOL_NUMBER"] = SOL_NUMBER;
                DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;

                DRS["CAMPOS_SOL"] = "";
                DTSolicitud.Rows.Add(DRS);

                List<ST_RDSUsuario> AsignacionesGroup1 = AutomaticAssignGroup1_V2(ref cData, ref DTSolUsers, SOL_UID, IDUserWeb);
                CrearAnalisisTecnico(ref DTTech, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisis, SOL_UID);
                CrearResolucion(ref DTResolucion, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisisViabilidad, SOL_UID);
                CrearResolucion(ref DTResolucionViabilidad, SOL_UID);

                int ANX_NUMBER = 1;
                foreach (var File in Files)
                {
                    DataRow DRA = DTAnexo.NewRow();
                    DRA["ANX_UID"] = Guid.NewGuid();
                    DRA["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_CREATED_DATE"] = DateTime.Now;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRA["SOL_UID"] = SOL_UID;
                    DRA["ANX_NUMBER"] = ANX_NUMBER++;
                    DTAnexo.Rows.Add(DRA);
                }


                ST_RDSSolicitud Solicitud = MakeSolicitud(SOL_UID, SOL_NUMBER, "#####", SOL_TYPE.SOL_TYPE_NAME, ref Expediente, ref Contacto);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
                MemoryStream mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SOLICITUD", "Radicado solicitud", "Funcionario MinTIC");
                AddNewDocument(ref DTDocuments, SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Solicitud, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, RadicadoCodigo, ref mem, "Solicitud " + SOL_TYPE.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                if (bEnviarCorreoConcesionario)
                {
                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_CREADA, ref Solicitud, ref Helper);
                }

                string ResultMessage = GetMensajeAlertaSolicitud(SOL_TYPE.SOL_TYPE_NAME, RadicadoCodigo);


                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_SOL_FUNCIONARIO_CREATE", LOG_SOURCE.MinTIC, ROLES.Funcionario, AsignacionesGroup1.Where(bp => bp.USR_ROLE == 1).First().USR_NAME, RadicadoCodigo));
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO", LOG_SOURCE.MinTIC, ROLES.Funcionario, AsignacionesGroup1.Where(bp => bp.USR_ROLE == 1).First().USR_NAME, ""));
                AddLogAutomaticAssign(ref DynamicSolicitud, SOL_UID, ref lstLogs, LOG_SOURCE.MinTIC, ref AsignacionesGroup1, ref Helper, ROLES.Coordinador, ROLES.Subdirector, ROLES.Asesor, ROLES.Director);

                AddLog(ref cData, lstLogs);

                return new ActionInfo(1, ResultMessage);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo ReasignarSolicitud(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSReasignUp Reasign)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", Reasign=" + Serializer.Serialize(Reasign);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();


                sSQL = @"
                        Select SOL_UID, USR_GROUP, 
                        USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID, USR_ROLE16_ID, USR_ROLE32_ID,
                        USR_ROLE1_REASIGN, USR_ROLE2_REASIGN, USR_ROLE4_REASIGN, USR_ROLE8_REASIGN, USR_ROLE16_REASIGN, 
                        USR_ROLE1_REASIGN_COMMENT, USR_ROLE2_REASIGN_COMMENT, USR_ROLE4_REASIGN_COMMENT, USR_ROLE8_REASIGN_COMMENT, USR_ROLE16_REASIGN_COMMENT
                        From RADIO.SOL_USERS 
                        Where SOL_UID='" + Reasign.SOL_UID.ToString() + "' And USR_GROUP=" + Reasign.USR_GROUP + @";

                        Select USR_ID, USR_NAME, USR_EMAIL, USR_ROLE
                        From RADIO.USUARIOS
                        Where USR_STATUS=1 And (USR_GROUP & " + Reasign.USR_GROUP + @" <> 0) And (USR_ROLE & 0xFF <> 0);

                        SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;
                ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                List<ST_RDSUsuario> Usuarios = (from bp in dtSet.Tables[1].AsEnumerable().AsQueryable()
                                                select new ST_RDSUsuario
                                                {
                                                    USR_ID = bp.Field<int>("USR_ID"),
                                                    USR_NAME = bp.Field<string>("USR_NAME"),
                                                    USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                                                    USR_ROLE = bp.Field<int>("USR_ROLE")
                                                }).ToList();

                Helper.TXT = (from bp in dtSet.Tables[2].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                DataTable DTSolicitud = dtSet.Tables[0];
                DataRow DRS = DTSolicitud.Rows[0];

                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:

                        switch ((ROLES)Reasign.ROLE)
                        {
                            case ROLES.Funcionario:
                                DRS["USR_ROLE1_REASIGN"] = true;
                                DRS["USR_ROLE1_REASIGN_COMMENT"] = Reasign.COMMENT;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                break;
                            case ROLES.Revisor:
                                DRS["USR_ROLE2_REASIGN"] = true;
                                DRS["USR_ROLE2_REASIGN_COMMENT"] = Reasign.COMMENT;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                break;
                            case ROLES.Coordinador:
                                DRS["USR_ROLE4_REASIGN"] = true;
                                DRS["USR_ROLE4_REASIGN_COMMENT"] = Reasign.COMMENT;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE8_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Subdirector;
                                break;
                            case ROLES.Subdirector:
                                DRS["USR_ROLE8_REASIGN"] = true;
                                DRS["USR_ROLE8_REASIGN_COMMENT"] = Reasign.COMMENT;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE32_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Director;
                                break;
                            case ROLES.Asesor:
                                DRS["USR_ROLE16_REASIGN"] = true;
                                DRS["USR_ROLE16_REASIGN_COMMENT"] = Reasign.COMMENT;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE32_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Director;
                                break;
                        }
                        break;

                    case REASIGN_TYPE.Rechazar:

                        switch ((ROLES)Reasign.ROLE)
                        {
                            case ROLES.Coordinador:
                                switch ((ROLES)Reasign.ColumnROLE)
                                {
                                    case ROLES.Funcionario:
                                        DRS["USR_ROLE1_REASIGN"] = false;
                                        DRS["USR_ROLE1_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                        break;
                                    case ROLES.Revisor:
                                        DRS["USR_ROLE2_REASIGN"] = false;
                                        DRS["USR_ROLE2_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                        break;
                                }
                                break;

                            case ROLES.Subdirector:
                                DRS["USR_ROLE4_REASIGN"] = false;
                                DRS["USR_ROLE4_REASIGN_COMMENT"] = (object)DBNull.Value;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE8_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Subdirector;
                                break;

                            case ROLES.Director:
                                switch ((ROLES)Reasign.ColumnROLE)
                                {
                                    case ROLES.Subdirector:
                                        DRS["USR_ROLE8_REASIGN"] = false;
                                        DRS["USR_ROLE8_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE32_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Director;
                                        break;
                                    case ROLES.Asesor:
                                        DRS["USR_ROLE16_REASIGN"] = false;
                                        DRS["USR_ROLE16_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE32_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Director;
                                        break;
                                }
                                break;
                        }
                        break;

                    case REASIGN_TYPE.Aprobar:
                    case REASIGN_TYPE.Reasignar:
                        switch ((ROLES)Reasign.ROLE)
                        {
                            case ROLES.Coordinador:
                                switch ((ROLES)Reasign.ColumnROLE)
                                {
                                    case ROLES.Funcionario:
                                        DRS["USR_ROLE1_REASIGN"] = false;
                                        DRS["USR_ROLE1_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        DRS["USR_ROLE1_ID"] = Reasign.NEW_USR_ID;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                        break;
                                    case ROLES.Revisor:
                                        DRS["USR_ROLE2_REASIGN"] = false;
                                        DRS["USR_ROLE2_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        DRS["USR_ROLE2_ID"] = Reasign.NEW_USR_ID;
                                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE4_ID"];
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                                        break;
                                }
                                break;

                            case ROLES.Subdirector:
                                DRS["USR_ROLE4_REASIGN"] = false;
                                DRS["USR_ROLE4_REASIGN_COMMENT"] = (object)DBNull.Value;
                                DRS["USR_ROLE4_ID"] = Reasign.NEW_USR_ID;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE8_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Subdirector;
                                break;

                            case ROLES.Director:
                                DRS["USR_ROLE16_REASIGN"] = false;
                                DRS["USR_ROLE16_REASIGN_COMMENT"] = (object)DBNull.Value;
                                DRS["USR_ROLE16_ID"] = Reasign.NEW_USR_ID;
                                Reasign.NEXT_LEVEL_USR_ID = (int)DRS["USR_ROLE32_ID"];
                                Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Director;
                                break;

                            case ROLES.Administrador:
                                switch ((ROLES)Reasign.ColumnROLE)
                                {
                                    case ROLES.Subdirector:
                                        DRS["USR_ROLE8_REASIGN"] = false;
                                        DRS["USR_ROLE8_REASIGN_COMMENT"] = (object)DBNull.Value;
                                        DRS["USR_ROLE8_ID"] = Reasign.NEW_USR_ID;
                                        Reasign.NEXT_LEVEL_USR_ID = IDUserWeb;
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Administrador;
                                        break;
                                    case ROLES.Director:
                                        DRS["USR_ROLE32_ID"] = Reasign.NEW_USR_ID;
                                        Reasign.NEXT_LEVEL_USR_ID = IDUserWeb;
                                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Administrador;
                                        break;
                                }
                                break;
                        }
                        break;

                }


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }


                AddLogReasign(ref cData, ref Solicitud, Reasign, ref Usuarios, ref Helper);

                EnviarCorreoReasign(ref Solicitud, Reasign, ref Usuarios, ref Helper);


                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo SubsanarConcesionarioSolicitud(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond)
        {
            int IDUserWeb = 1;
            List<string> Emails= new List<string>();
            csData cData = null;
            AZTools AZTools = new AZTools();
            var Serializer = new JavaScriptSerializer_TESExtension();
            Serializer.MaxJsonLength = int.MaxValue;
            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, 
                        SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT
                        From RADIO.SOLICITUDES
                        Where SOL_UID='" + Solicitud.SOL_UID + @"';

                        Select ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED, HABILITANTES
                        From RADIO.ANEXOS
                        Where SOL_UID='" + Solicitud.SOL_UID + @"' And ANX_STATE_ID=2 and HABILITANTES='true';

                        Select ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED, HABILITANTES
                        From RADIO.ANEXOS
                        Where SOL_UID='" + Solicitud.SOL_UID + @"' And ANX_STATE_ID=2 and HABILITANTES='false';

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTAnexo = dtSet.Tables[1];
                DataTable DTAnexoPond = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];

                DataRow DRS = DTSolicitud.Rows[0];
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.Subsanado;
                DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;
                DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                DRS["SOL_REQUIRED_POSTPONE"] = 0;

                if (Files != null)
                {
                    foreach (var File in Files)
                    {
                        DataRow DRA = DTAnexo.AsEnumerable().AsQueryable().Where(bp => bp.Field<Guid>("ANX_UID").Equals(File.UID)).FirstOrDefault();

                        DRA["ANX_NAME"] = File.NAME;
                        DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                        DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                        DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                        DRA["ANX_ROLE2_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRA["ANX_ROLE4_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRA["ANX_ROLE8_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRA["ANX_ROLEX_CHANGED"] = false;
                        DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    }
                }

                if (FilesPond != null)
                {
                    foreach (var File in FilesPond)
                    {
                        DataRow DRAP = DTAnexoPond.AsEnumerable().AsQueryable().Where(bp => bp.Field<Guid>("ANX_UID").Equals(File.UID)).FirstOrDefault();

                        DRAP["ANX_NAME"] = File.NAME;
                        DRAP["ANX_FILE"] = Base64ToByteArray(File.DATA);
                        DRAP["ANX_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                        DRAP["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                        DRAP["ANX_ROLE2_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRAP["ANX_ROLE4_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRAP["ANX_ROLE8_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                        DRAP["ANX_ROLEX_CHANGED"] = false;
                        DRAP["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                        DRAP["ANX_MODIFIED_DATE"] = DateTime.Now;
                    }
                }

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);
                MemoryStream mem = null;
                //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticSubsanar(Solicitud, Files, ref Helper, ref mem);
                AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticSubsanar(Solicitud, Files, ref Helper, ref mem);
                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.AdminSubsanacion, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, resultado.NuevoRaNumero, ref mem, "Subsanacion " + Solicitud.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }


                Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_SUBSANADA, ref Solicitud, ref Helper);

                Solicitud.Radicado = resultado.NuevoRaNumero;
                Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_SUBSANADA, ref Solicitud, ref Helper);


                string ResultMessage = GetMensajeAlertaSubsanar(Solicitud.SOL_TYPE_NAME, resultado.NuevoRaNumero);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CONCESIONARIO_SUBSANAR", LOG_SOURCE.Concesionario, ROLES.NoAplica, "", Solicitud.Radicado));
                AddLog(ref cData, lstLogs);


                return new ActionInfo(1, ResultMessage);


            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo SubsanarConcesionarioOtorgaTecnico(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files)
        {
            int IDUserWeb = 1;
            List<string> Emails = new List<string>();
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            Serializer.MaxJsonLength = Int32.MaxValue;

            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);
            //OJO DESERIALIZADOR PARA VISTA COMPARATIVA DE EXCEL.
            //Serializer.MaxJsonLength = Int32.MaxValue;
            //ST_RDSAnexoTecnico  mResult = Serializer.Deserialize<ST_RDSAnexoTecnico>(result);
            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, 
                        SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT
                        From RADIO.SOLICITUDES
                        Where SOL_UID='" + Solicitud.SOL_UID + @"';

                        Select ANX_UID, SOL_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE,  ANX_NUMBER, ANX_FILE, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED
                        From RADIO.TECH_ANEXOS
                        Where SOL_UID='" + Solicitud.SOL_UID + @"' And ANX_STATE_ID=2;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTTechAnexos = dtSet.Tables[1];
                DataTable DTDocuments = dtSet.Tables[2];

                DataRow DRS = DTSolicitud.Rows[0];
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.Subsanado;
                DRS["GROUP_CURRENT"] = (int)GROUPS.ANE;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;
                DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                DRS["SOL_REQUIRED_POSTPONE"] = 0;

                List<ST_RDSFiles_Up> fileExcel = new List<ST_RDSFiles_Up>();
                ST_RDSAnexoTecnico AnexTech = new ST_RDSAnexoTecnico();
                fileExcel = Files.Where(ef => ef.CONTENT_TYPE != "application/pdf").ToList();
                if (fileExcel.Count > 0)
                {

                    int Tam = fileExcel[0].DATA.IndexOf(",");
                    if (Tam < 0)
                    {
                        return new ActionInfo(2, "Error al procesar el archivo");
                    }

                    byte[] bExcelFile = Convert.FromBase64String(fileExcel[0].DATA.Substring(Tam + 1));


                    Stream stream = new MemoryStream(bExcelFile);
                    SLDocument sl = new SLDocument(stream);
                    int fila = 11;
                    int col = 1;
                    try
                    {
                        AnexTech.SOL_NUMBER = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.SOL_OTORGA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRESENTA_PATRONES_RADIACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.TRANSMOVILES = sl.GetCellValueAsString(fila, col++);

                        AnexTech.CONSECIONARIO = new ST_RDSConsecionario();
                        AnexTech.CONSECIONARIO.COD_EXPEDIENTE = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.CONSECIONARIO.STATION_NAME = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.CONCES_NAME = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.CONCES_NIT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.DEPTO_CONCESION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.MUNICIPIO_CONCESION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.COD_DANE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.STATION_TYPE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.STATION_CLASS = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.IDENTIFICATIVO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.CEL_REP_LEGAL = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.EMAIL = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.TIPO_TRAMITE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CONSECIONARIO.ADMIN_ACT = sl.GetCellValueAsString(fila, col++);

                        AnexTech.MODIF_PARAMS = new ST_RDSModif_Params();
                        AnexTech.MODIF_PARAMS.ESENCIAL_PARAM = sl.GetCellValueAsString(fila, col++);
                        AnexTech.MODIF_PARAMS.UBIC_SYSTEM_RAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.MODIF_PARAMS.PRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.MODIF_PARAMS.SERVICE_AREA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.MODIF_PARAMS.RADIATION_SYSTEM_TX = sl.GetCellValueAsString(fila, col++);
                        AnexTech.MODIF_PARAMS.FRECUENCY = sl.GetCellValueAsString(fila, col++);

                        AnexTech.AVAL_ESTUDIO = new ST_RDSAvalEstudio();
                        AnexTech.AVAL_ESTUDIO.PROFESIONAL_AVAL = sl.GetCellValueAsString(fila, col++);
                        AnexTech.AVAL_ESTUDIO.PROFESIONAL_NAME = sl.GetCellValueAsString(fila, col++);
                        AnexTech.AVAL_ESTUDIO.PROFESIONAL_APELLIDO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.AVAL_ESTUDIO.PROFESIONAL_CEDULA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.AVAL_ESTUDIO.PROFESIONAL_TP = sl.GetCellValueAsString(fila, col++);

                        AnexTech.CERTIF_AERONAUT = new ST_RDSCertifAeronautica();
                        AnexTech.CERTIF_AERONAUT.NUM_REF_CERTIF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.FECHA_CERTIF = sl.GetCellValueAsDateTime(fila, col++);
                        AnexTech.CERTIF_AERONAUT.DIR_UBICACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.DEPTO_UBICACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.MUNIC_UBICACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.ALTURA_APROB = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.POTENCIA_APROB = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.MODULACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.FREC_APROBADA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.RANGO_FREC = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LAT_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LAT_S_N = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_AERONAUT.ELEVACION = sl.GetCellValueAsString(fila, col++);

                        AnexTech.CERTIF_MUNICIPAL = new ST_RDSCertifPlanMunic();
                        AnexTech.CERTIF_MUNICIPAL.ENTIDAD_EMISORA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.FECHA_CERTIF = sl.GetCellValueAsDateTime(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.NUM_CERTIFICADO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LAT_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LAT_S_N = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.DENTRO_MUNICIPIO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.CERTIF_MUNICIPAL.TIPO_ZONA = sl.GetCellValueAsString(fila, col++);

                        AnexTech.RED_CUBRIMIENTO = new ST_RDSPRA();
                        AnexTech.RED_CUBRIMIENTO.LONG_TX = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.PRA_AUTORIZ_SOLIC = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.PRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.G_VECES = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.G_DBI = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.G_DBD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.PERDIDA_CONECTOR = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.PERDIDA_LINEA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.POTENCIA_INCIDENTE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.RED_CUBRIMIENTO.POTENCIA_NOMINAL = sl.GetCellValueAsString(fila, col++);

                        AnexTech.SISTEMA_RADIANTE = new ST_RDSSistemaRadiante();
                        AnexTech.SISTEMA_RADIANTE.LAT_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LAT_S_N = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.DEPARTAMENTO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.MUNICIPIO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SISTEMA_RADIANTE.AREA_MUNICIP = sl.GetCellValueAsString(fila, col++);

                        AnexTech.PATRON_RADIACION = new ST_RDSPatronRadiacion();
                        AnexTech.PATRON_RADIACION.ALTURA_RADIACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.NRO_BAHIAS_ANT = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.PATRON_RADIACION.GANANCIA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.POLARIZACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.ANGULO_TILT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.AZIMUT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.TIPO_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.DIMENSION_1 = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.DIMENSION_2 = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PATRON_RADIACION.NUMERO_BAHIAS = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.PATRON_RADIACION.ARREGLO_ANTENAS = new List<ST_RDSArregloAntenas>();

                        if (AnexTech.PATRON_RADIACION.NUMERO_BAHIAS == 0)
                            col = col + 9;
                        var jaux = col;
                        for (int i = fila; i < fila + AnexTech.PATRON_RADIACION.NUMERO_BAHIAS; i++)
                        {
                            col = jaux;
                            ST_RDSArregloAntenas ArregloAntena = new ST_RDSArregloAntenas();
                            ArregloAntena.BAHIA_NUMERO = sl.GetCellValueAsInt32(i, col++);
                            ArregloAntena.ALTURA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.DISTANCIA_H = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.AZIMUT = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.GANANCIA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.POLARIZACION = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.LONG_CABLE = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.MARCA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.MODELO = sl.GetCellValueAsString(i, col++);
                            AnexTech.PATRON_RADIACION.ARREGLO_ANTENAS.Add(ArregloAntena);
                        }
                        AnexTech.TOTAL_MUNICIPIOS = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.AREA_SERVICIO = new List<ST_RDSAreaServicio>();
                        if (AnexTech.TOTAL_MUNICIPIOS == 0)
                            col = col + 3;
                        jaux = col;
                        for (int i = fila; i < fila + AnexTech.TOTAL_MUNICIPIOS; i++)
                        {
                            col = jaux;
                            ST_RDSAreaServicio AreaCob = new ST_RDSAreaServicio();
                            AreaCob.DEPARTAMENTO = sl.GetCellValueAsString(i, col++);
                            AreaCob.MUNICIPIO = sl.GetCellValueAsString(i, col++);
                            AreaCob.CODIGO_DANE = sl.GetCellValueAsString(i, col++);
                            AnexTech.AREA_SERVICIO.Add(AreaCob);
                        }

                        AnexTech.ENLACE_PTOAPTO = new ST_RDSEnlacePtoaPto();
                        AnexTech.ENLACE_PTOAPTO.TX_LAT_GRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LAT_N_S = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.TX_ALTURA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LAT_GRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LAT_N_S = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.RX_ALTURA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.ENLACE_PTOAPTO.FRECUEN_APROBADA = sl.GetCellValueAsString(fila, col++);

                        AnexTech.PRA_PTOAPTO = new ST_RDSPRA();
                        AnexTech.PRA_PTOAPTO.LONG_TX = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.PRA_AUTORIZ_SOLIC = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.PRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.G_VECES = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.G_DBI = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.G_DBD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.PERDIDA_CONECTOR = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.PERDIDA_LINEA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.POTENCIA_INCIDENTE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.PRA_PTOAPTO.POTENCIA_NOMINAL = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA = new ST_RDSEquiposRedCobertura();

                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX = new ST_RDSEquipo();
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.POT_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.POT_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.DESVIACION_FREQ = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPO_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX = new ST_RDSAntena();
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.GAIN_VALOR = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.GAIN_UNIT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.NRO_BAHIAS = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.POLARIZACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.ANTENA_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX = new ST_RDSLinea();
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.LONG_LINEA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.LOSS_100M = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.LINEA_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION = new ST_RDSMonitor();
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ = new ST_RDSMonitor();
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_RED_COBERTURA.EQUIPOS_ADICIONALES = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.EQUIPOS_RED_COBERTURA.OTROS_EQUIPOS = new List<ST_RDSOtrosEquipos>();
                        if (AnexTech.EQUIPOS_RED_COBERTURA.EQUIPOS_ADICIONALES == 0)
                            col = col + 4;
                        jaux = col;
                        for (int i = fila; i < fila + AnexTech.EQUIPOS_RED_COBERTURA.EQUIPOS_ADICIONALES; i++)
                        {
                            col = jaux;
                            ST_RDSOtrosEquipos OtroEquip = new ST_RDSOtrosEquipos();
                            OtroEquip.ITEM = sl.GetCellValueAsInt32(i, col++);
                            OtroEquip.MARCA = sl.GetCellValueAsString(i, col++);
                            OtroEquip.MODELO = sl.GetCellValueAsString(i, col++);
                            OtroEquip.OBSERVACION = sl.GetCellValueAsString(i, col++);
                            AnexTech.EQUIPOS_RED_COBERTURA.OTROS_EQUIPOS.Add(OtroEquip);
                        }

                        AnexTech.EQUIPOS_PTOAPTO = new ST_RDSEquiposRedPtoAPto();
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX = new ST_RDSEquipo();
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.POT_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.POT_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.ANCHO_BANDA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.DESVIACION_FREQ = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX = new ST_RDSEquipo();
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_INF = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_SUP = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.EQUIPO_RX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX = new ST_RDSAntena();
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.G_VECES = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.G_dBi = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.G_dbd = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.POLARIZACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX = new ST_RDSAntena();
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX.POLARIZACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.ANTENA_RX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX = new ST_RDSLinea();
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.MARCA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.MODELO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.LONG_LINEA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.LOSS_100M = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.OBSERVACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.LINEA_TX.URL_CATALOGO_EQUIPO = sl.GetCellValueAsString(fila, col++);

                        AnexTech.EQUIPOS_PTOAPTO.EQUIPOS_ADICIONALES = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.EQUIPOS_PTOAPTO.OTROS_EQUIPOS = new List<ST_RDSOtrosEquipos>();
                        if (AnexTech.EQUIPOS_PTOAPTO.EQUIPOS_ADICIONALES == 0)
                            col = col + 4;
                        if (AnexTech.EQUIPOS_PTOAPTO.EQUIPOS_ADICIONALES == 0)
                            col = col + 4;
                        jaux = col;
                        for (int i = fila; i < fila + AnexTech.EQUIPOS_PTOAPTO.EQUIPOS_ADICIONALES; i++)
                        {
                            col = jaux;
                            ST_RDSOtrosEquipos OtroEquip = new ST_RDSOtrosEquipos();
                            OtroEquip.ITEM = sl.GetCellValueAsInt32(i, col++);
                            OtroEquip.MARCA = sl.GetCellValueAsString(i, col++);
                            OtroEquip.MODELO = sl.GetCellValueAsString(i, col++);
                            OtroEquip.OBSERVACION = sl.GetCellValueAsString(i, col++);
                            AnexTech.EQUIPOS_PTOAPTO.OTROS_EQUIPOS.Add(OtroEquip);
                        }

                        AnexTech.SOLO_SISTEMA_RADIANTE = new ST_RDSSoloSistemaRadiante();
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE = new ST_RDSSistemaRadiante();
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LAT_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LAT_S_N = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.DEPARTAMENTO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.MUNICIPIO = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.SISTEMA_RADIANTE.AREA_MUNICIP = sl.GetCellValueAsString(fila, col++);

                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION = new ST_RDSPatronRadiacion();
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.ALTURA_RADIACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.NRO_BAHIAS_ANT = sl.GetCellValueAsInt32(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.GANANCIA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.POLARIZACION = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.ANGULO_TILT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.AZIMUT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.TIPO_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.DIMENSION_1 = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.DIMENSION_2 = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.NUMERO_BAHIAS = sl.GetCellValueAsInt32(fila, col++);

                        AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.ARREGLO_ANTENAS = new List<ST_RDSArregloAntenas>();
                        if (AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.NUMERO_BAHIAS == 0)
                            col = col + 9;
                        jaux = col;
                        for (int i = fila; i < fila + AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.NUMERO_BAHIAS; i++)
                        {
                            col = jaux;
                            ST_RDSArregloAntenas ArregloAntena = new ST_RDSArregloAntenas();
                            ArregloAntena.BAHIA_NUMERO = sl.GetCellValueAsInt32(i, col++);
                            ArregloAntena.ALTURA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.DISTANCIA_H = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.AZIMUT = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.GANANCIA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.POLARIZACION = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.LONG_CABLE = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.MARCA = sl.GetCellValueAsString(i, col++);
                            ArregloAntena.MODELO = sl.GetCellValueAsString(i, col++);
                            AnexTech.SOLO_SISTEMA_RADIANTE.PATRON_RADIACION.ARREGLO_ANTENAS.Add(ArregloAntena);
                        }

                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO = new ST_RDSEnlacePtoaPto();
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LAT_GRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LAT_N_S = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.TX_ALTURA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LAT_GRA = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LAT_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LAT_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LAT_N_S = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LONG_GRAD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LONG_MIN = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LONG_SEG = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LONG_W = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LATITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_LONGITUD = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_ALTURA_TORRE = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.RX_ALTURA_ANT = sl.GetCellValueAsString(fila, col++);
                        AnexTech.SOLO_SISTEMA_RADIANTE.ENLACE_PTOAPTO.FRECUEN_APROBADA = sl.GetCellValueAsString(fila, col++);
                        DRS["CAMPOS_SOL"] = Serializer.Serialize(AnexTech);

                    }
                    catch (Exception e)
                    {
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                        return new ActionInfo(-1, "Archivo no corresponde.");
                    }
                }


                foreach (var File in Files)
                {
                    DataRow DRA = DTTechAnexos.AsEnumerable().AsQueryable().Where(bp => bp.Field<Guid>("ANX_UID").Equals(File.UID)).FirstOrDefault();

                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Subsanado;
                    DRA["ANX_ROLE2_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                    DRA["ANX_ROLE4_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                    DRA["ANX_ROLE8_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
                    DRA["ANX_ROLEX_CHANGED"] = false;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                }

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);
                Helper.IMG = GetDinamicImages(ref cData);
                if (fileExcel.Count > 0)
                {
                    bool RetExcelManager = SubirInfoExcelICSManager(ref cData, ref Solicitud, ref AnexTech);
                    if (!RetExcelManager)
                        return new ActionInfo(2, "Error en la operación escribiendo en ICSManager");
                }
                Helper.TXT = GetDinamicTexts(ref cData);
                MemoryStream mem = null;
                AZTools AZTools = new AZTools();
                //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticSubsanar(Solicitud, Files, ref Helper, ref mem);
                AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticSubsanar(Solicitud, Files, ref Helper, ref mem);
                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.AdminSubsanacion, DOC_SOURCE.Concesionario, DOC_SOURCE.ANE, resultado.NuevoRaNumero, ref mem, "Subsanacion " + Solicitud.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }


                Ret = EnviarCorreo(MAIL_TYPE.ANE_SOL_ASSIGNED, ref Solicitud, ref Helper);

                Solicitud.Radicado = resultado.NuevoRaNumero;
                Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_SUBSANADA, ref Solicitud, ref Helper);


                string ResultMessage = GetMensajeAlertaSubsanar(Solicitud.SOL_TYPE_NAME, resultado.NuevoRaNumero);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CONCESIONARIO_SUBSANAR", LOG_SOURCE.Concesionario, ROLES.NoAplica, "", Solicitud.Radicado));
                AddLog(ref cData, lstLogs);


                return new ActionInfo(1, ResultMessage);


            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo CancelarConcesionarioSolicitud(ST_RDSSolicitud Solicitud)
        {
            int IDUserWeb = 1;
            List<string> Emails = new List<string>();
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);

            try
            {
                
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;
                bool Ret;
                string ResultMessage;

               ValidateToken(ref cData);

                if (Solicitud.Radicado != null) 
                { 
                    sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, 
                        SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED
                        From RADIO.SOLICITUDES
                        Where SOL_UID='" + Solicitud.SOL_UID + @"';

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;
                        ";


                    var dtSet = cData.ObtenerDataSet(sSQL);

                    DataTable DTSolicitud = dtSet.Tables[0];
                    DataTable DTDocuments = dtSet.Tables[1];

                    DataRow DRS = DTSolicitud.Rows[0];
                    DRS["SOL_STATE_ID"] = (int)SOL_STATES.Cancelada;
                    DRS["SOL_ENDED"] = 2;
                    DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                    DRS["ROLE_CURRENT"] = (int)ROLES.NoAplica;
                    DRS["STEP_CURRENT"] = (int)STEPS.Terminada;
                    DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                    DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                    DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                    DRS["SOL_REQUIRED_POSTPONE"] = 0;

                    ST_Helper Helper = new ST_Helper();
                    AZTools AZTools = new AZTools();
                    Helper.TXT = GetDinamicTexts(ref cData);
                    MemoryStream mem = null;
                    //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticCancelar(Solicitud, ref Helper, ref mem);
                    AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticCancelar(Solicitud, ref Helper, ref mem);
                    AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Cancelacion, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, resultado.NuevoRaNumero, ref mem, "Cancelacion " + Solicitud.SOL_TYPE_NAME);

                    Ret = cData.InsertDataSet(sSQL, dtSet);

                    if (!Ret)
                    {
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                        return new ActionInfo(2, "Error en la operación");
                    }

                    Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_CANCELADA, ref Solicitud, ref Helper);
                    Solicitud.Radicado = resultado.NuevoRaNumero;
                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_CANCELADA, ref Solicitud, ref Helper);

                    ResultMessage = GetMensajeAlertaCancelar(Solicitud.SOL_TYPE_NAME, resultado.NuevoRaNumero);

                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CONCESIONARIO_CANCELAR", LOG_SOURCE.Concesionario, ROLES.Concesionario, Solicitud.Expediente.Operador.CUS_NAME, Solicitud.Radicado));
                    AddLog(ref cData, lstLogs);
                }
                else
                {
                    sSQL = @"
                                Select SOL_UID From RADIO.SOLICITUDES_TEMP
                                Where SOL_UID= '" + Solicitud.SOL_UID + @"' ;

                                Select ANX_UID From RADIO.ANEXOS_TEMP
                                Where SOL_UID= '" + Solicitud.SOL_UID + @"';

                                SELECT SERV_ID FROM RADIO.EXPEDIENTES_TEMPORALES
                                WHERE SERV_ID = '" + Solicitud.SERV_ID + @"';
                                ";

                    var dtSetEliminar = cData.ObtenerDataSet(sSQL);

                    DataTable DTSolicitudTemp = dtSetEliminar.Tables[0];
                    DataTable DTAnexosTemp = dtSetEliminar.Tables[1];
                    DataTable DTExpedienteTemp = dtSetEliminar.Tables[2];
                    EliminarSolicitudTemporal(ref DTSolicitudTemp, ref DTAnexosTemp, ref DTExpedienteTemp);
                    Ret = cData.InsertDataSet(sSQL, dtSetEliminar);                    
                    if (!Ret)
                    {
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                        return new ActionInfo(2, "Error en la operación");
                    }

                    ResultMessage = GetMensajeAlertaCancelar(Solicitud.SOL_TYPE_NAME, null);
                }

                return new ActionInfo(1, ResultMessage);
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo AplazarConcesionarioSolicitud(ST_RDSSolicitud Solicitud)
        {
            int IDUserWeb = 1;
            List<string> Emails = new List<string>();
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);
                

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, 
                        SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED
                        From RADIO.SOLICITUDES
                        Where SOL_UID='" + Solicitud.SOL_UID + @"';

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;
                        ";


                int SOL_REQUIRED_POSTPONE = GetIntValue(Helper.GetKeyTextValue("SOLICITUD_REQUERIMIENTO_APLAZAR_PERIODO"), 15);

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTDocuments = dtSet.Tables[1];

                DataRow DRS = DTSolicitud.Rows[0];
                DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                DRS["SOL_REQUIRED_POSTPONE"] = SOL_REQUIRED_POSTPONE;
                
                MemoryStream mem = null;
                //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticAplazar(Solicitud, ref Helper, ref mem);
                AZTools AZTools = new AZTools();
                AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticAplazar(Solicitud, ref Helper, ref mem);
                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Aplazamiento, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, resultado.NuevoRaNumero, ref mem, "Aplazamiento " + Solicitud.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_APLAZADA, ref Solicitud, ref Helper);
                Solicitud.Radicado = resultado.NuevoRaNumero;
                Solicitud.SOL_REQUIRED_POSTPONE = SOL_REQUIRED_POSTPONE;
                Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_APLAZADA, ref Solicitud, ref Helper);

                string ResultMessage = GetMensajeAlertaAplazar(Solicitud.SOL_TYPE_NAME, resultado.NuevoRaNumero, SOL_REQUIRED_POSTPONE);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CONCESIONARIO_APLAZAR", LOG_SOURCE.Concesionario, ROLES.Concesionario, Solicitud.Expediente.Operador.CUS_NAME, Solicitud.Radicado));
                AddLog(ref cData, lstLogs);


                return new ActionInfo(1, ResultMessage);


            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        private bool DesistirSolicitud(ref csData cData, ref ST_RDSSolicitud Solicitud)
        {
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "Solicitud=" + Serializer.Serialize(Solicitud);
            AZTools AZTools = new AZTools();

            string sSQL;

            sSQL = @"
                    Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, 
                    SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED
                    From RADIO.SOLICITUDES
                    Where SOL_UID='" + Solicitud.SOL_UID + @"';

                    SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                    FROM RADIO.DOCUMENTS;

                    SELECT SOL_UID, COM_UID, COM_CLASS, COM_TYPE, COM_NAME, COM_DATE, COM_FILE
                    FROM RADIO.COMUNICADOS
                    WHERE SOL_UID = '" + Solicitud.SOL_UID + @"' And COM_CLASS=4;
                    ";

            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DTSolicitud = dtSet.Tables[0];
            DataTable DTDocuments = dtSet.Tables[1];
            DataTable DTComunicado = dtSet.Tables[2];

            DataRow DRS = DTSolicitud.Rows[0];
            DRS["SOL_STATE_ID"] = (int)SOL_STATES.Desistida;
            DRS["SOL_ENDED"] = 2;
            DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
            DRS["ROLE_CURRENT"] = (int)ROLES.NoAplica;
            DRS["STEP_CURRENT"] = (int)STEPS.Terminada;
            DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
            DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
            DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
            DRS["SOL_REQUIRED_POSTPONE"] = 0;

            ST_RDSComunicado Comunicado = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Desistimiento);
            ST_Helper Helper = new ST_Helper();
            Helper.TXT = GetDinamicTexts(ref cData);
            Helper.IMG = GetDinamicImages(ref cData);
            GetUserSign(Solicitud.USR_ADMIN.USR_ROLE8, ref cData);

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
            MemoryStream mem = null;

            //string registroNumber = AlfaRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_DESISTIMIENTO");
            string registroNumber = AZTools.AZRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_DESISTIMIENTO");
            mem = RDSComunicados.GenerarComunicado(ref Comunicado, true, registroNumber);
            GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
            //AlfaRegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_DESISTIMIENTO"), ref Helper, ref mem);
            AZTools.RegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_DESISTIMIENTO"), ref Solicitud, ref mem, ref Helper);
            AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.Desistimiento, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);

            bool Ret = cData.InsertDataSet(sSQL, dtSet);

            if (!Ret)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                return false;
            }

            Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_DESISTIDA, ref Solicitud, ref Helper);
            Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_DESISTIDA, ref Solicitud, ref Helper);

            List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CONCESIONARIO_DESISTIR", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
            AddLog(ref cData, lstLogs);

            return true;

        }

        public bool ProcesarDesistimientos()
        {
            csData cData = null;
            ST_RDSMainData mResult = new ST_RDSMainData();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, strConn);
                    return false;
                }

                if(HttpContext.Current.Request.Headers["Token"] != "B554D7EE-F794-4A22-959D-11A3F4BCB32C")
                    ValidateToken(ref cData);

                string sSQL;


                sSQL = @"Select SOL.SOL_UID
                        From RADIO.SOLICITUDES SOL
                        Join RADIO.ALARMS ALM On ALM.ALARM_STEP=4
                        Where SOL.SOL_ENDED = 0 And RADIO.F_GetAlarmStateConcesionario(SOL.SOL_REQUIRED_DATE, ALM.ALARM_PRE_EXPIRE, ALM.ALARM_EXPIRE, SOL.SOL_ENDED, SOL_REQUIRED_POSTPONE)=3";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                var ListSolicitudesToDesistir = (from bp in dt
                                                 select new
                                                 {
                                                     SOL_UID = bp.Field<Guid>("SOL_UID")
                                                 }).ToList();


                foreach (var Item in ListSolicitudesToDesistir)
                {
                    ST_RDSSolicitud Solicitud = GetSolicitudMinTIC(ref cData, Item.SOL_UID, (int)GROUPS.MinTIC);
                    DesistirSolicitud(ref cData, ref Solicitud);
                }

                return true;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return false;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }


        #region Otorga
        public ST_RDSCreacionOtorga GetDatosCreacionOtorgaEmisora(int SOL_TYPE_ID, Guid? SOL_UID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSCreacionOtorga mResult = new ST_RDSCreacionOtorga();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                int claseExpediente = GetClaseExpedienteFromTipoSolicitud(SOL_TYPE_ID);
                string idSolicitud = SOL_UID.HasValue ? SOL_UID.ToString() : Guid.Empty.ToString();

                sSQL = @"
                            SELECT CODE_AREA_DEPARTAMENTO, DEPARTAMENTO
                            FROM RADIO.V_PLAN_BRO_PROYECTADOS 
                            
                            GROUP BY CODE_AREA_DEPARTAMENTO, DEPARTAMENTO
                            ORDER BY DEPARTAMENTO

                            SELECT RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL AS ID, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL AS [DESC], RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, 
                                    RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM RADIO.CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.CONFIGURACION_PROCESO.ID = RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.TIPO_ANEXO_SOL ON RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL = RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL
                             WHERE (RADIO.CONFIGURACION_PROCESO.SOL_TYPE = " + SOL_TYPE_ID + @")AND HABILITANTES='TRUE'

                            SELECT RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL AS ID, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL AS [DESC], RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, 
                                    RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM RADIO.CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.CONFIGURACION_PROCESO.ID = RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.TIPO_ANEXO_SOL ON RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL = RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL
                            WHERE (RADIO.CONFIGURACION_PROCESO.SOL_TYPE = " + SOL_TYPE_ID + @")AND HABILITANTES ='FALSE'

                            SELECT RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL AS ID, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL AS [DESC], RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, 
                                    RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM RADIO.CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.CONFIGURACION_PROCESO.ID = RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.TIPO_ANEXO_SOL ON RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL = RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL
                            WHERE (RADIO.CONFIGURACION_PROCESO.SOL_TYPE = " + SOL_TYPE_ID + @")AND HABILITANTES ='TRUE'

                            SELECT RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL AS ID, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL AS [DESC], RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, 
                                    RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM RADIO.CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.CONFIGURACION_PROCESO.ID = RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO INNER JOIN
                                 RADIO.TIPO_ANEXO_SOL ON RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL = RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL
                            WHERE (RADIO.CONFIGURACION_PROCESO.SOL_TYPE = " + SOL_TYPE_ID + @")AND HABILITANTES ='FALSE'
                        
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTDepartamentos = dtSet.Tables[0];
                DataTable DTTiposArchivos = dtSet.Tables[1];
                DataTable DTTiposArchivosPond = dtSet.Tables[2];
                DataTable DTTiposAnexos = dtSet.Tables[3];
                DataTable DTTiposAnexosPond = dtSet.Tables[4];


                mResult.DepartamentosPlanBro = (from bp in DTDepartamentos.AsEnumerable().AsQueryable()
                                         select new ST_Lugar
                                         {
                                             CODE_AREA = bp.Field<int>("CODE_AREA_DEPARTAMENTO"),
                                             NAME = bp.Field<string>("DEPARTAMENTO"),
                                         }).ToList();

                mResult.TiposArchivos  = (from bp in DTTiposArchivos.AsEnumerable().AsQueryable()
                                         select new ST_TipoArchivoProceso
                                         {
                                             ID = bp.Field<int>("ID"),
                                             DESC = bp.Field<string>("DESC"),
                                             DES_TIPO_ANX_SOL = bp.Field<string>("DES_TIPO_ANX_SOL"),
                                             EXTENSIONS = bp.Field<string>("EXTENSIONS"),
                                             MAX_SIZE = bp.Field<int>("MAX_SIZE"),
                                             MAX_NAME_LENGTH = bp.Field<int>("MAX_NAME_LENGTH"),
                                         }).ToList();

                mResult.TiposArchivosPond = (from bp in DTTiposArchivosPond.AsEnumerable().AsQueryable()
                                         select new ST_TipoArchivoProceso
                                         {
                                             ID = bp.Field<int>("ID"),
                                             DESC = bp.Field<string>("DESC"),
                                             DES_TIPO_ANX_SOL = bp.Field<string>("DES_TIPO_ANX_SOL"),
                                             EXTENSIONS = bp.Field<string>("EXTENSIONS"),
                                             MAX_SIZE = bp.Field<int>("MAX_SIZE"),
                                             MAX_NAME_LENGTH = bp.Field<int>("MAX_NAME_LENGTH"),
                                         }).ToList();

                mResult.TiposAnexos = (from bp in DTTiposAnexos.AsEnumerable().AsQueryable()
                                         select new ST_TipoArchivoProceso
                                         {
                                             ID = bp.Field<int>("ID"),
                                             DESC = bp.Field<string>("DESC"),
                                             DES_TIPO_ANX_SOL = bp.Field<string>("DES_TIPO_ANX_SOL"),
                                             EXTENSIONS = bp.Field<string>("EXTENSIONS"),
                                             MAX_SIZE = bp.Field<int>("MAX_SIZE"),
                                             MAX_NAME_LENGTH = bp.Field<int>("MAX_NAME_LENGTH"),
                                         }).ToList();
                mResult.TiposAnexosPond = (from bp in DTTiposAnexosPond.AsEnumerable().AsQueryable()
                                       select new ST_TipoArchivoProceso
                                       {
                                           ID = bp.Field<int>("ID"),
                                           DESC = bp.Field<string>("DESC"),
                                           DES_TIPO_ANX_SOL = bp.Field<string>("DES_TIPO_ANX_SOL"),
                                           EXTENSIONS = bp.Field<string>("EXTENSIONS"),
                                           MAX_SIZE = bp.Field<int>("MAX_SIZE"),
                                           MAX_NAME_LENGTH = bp.Field<int>("MAX_NAME_LENGTH"),
                                       }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_Lugar> GetMunicipiosOtorga(int CODE_AREA_DEPARTAMENTO, int SOL_TYPE_ID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_Lugar> mResult = new List<ST_Lugar>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT CODE_AREA_MUNICIPIO, MUNICIPIO
                            FROM RADIO.V_PLAN_BRO_PROYECTADOS
                            WHERE (CODE_AREA_DEPARTAMENTO = " + CODE_AREA_DEPARTAMENTO.ToString(CultureInfo.InvariantCulture) + @") " +
                            (SOL_TYPE_ID != (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico ? "AND CLASS = " + GetClaseExpedienteFromTipoSolicitud(SOL_TYPE_ID) : "") + @"
                            GROUP BY CODE_AREA_MUNICIPIO, MUNICIPIO
                            ORDER BY MUNICIPIO
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_Lugar
                           {

                               CODE_AREA = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                               NAME = bp.Field<string>("MUNICIPIO"),
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_PlanBroadcast> GetDistintivos(int CODE_AREA_MUNICIPIO, int SOL_TYPE_ID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_PlanBroadcast> mResult = new List<ST_PlanBroadcast>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT ID AS ID_PLAN_BRO, CALL_SIGN, FREQ, STATION_CLASS, POWER, ASL, STATE, DEPARTAMENTO, MUNICIPIO, CODE_AREA_MUNICIPIO
                            FROM RADIO.V_PLAN_BRO_PROYECTADOS
                            WHERE (CODE_AREA_MUNICIPIO = " + CODE_AREA_MUNICIPIO.ToString(CultureInfo.InvariantCulture) + @") " +

                            "AND CLASS = " + GetClaseExpedienteFromTipoSolicitud(SOL_TYPE_ID) + @"
                            ORDER BY FREQ, CALL_SIGN
                        ";


                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_PlanBroadcast
                           {
                               ID_PLAN_BRO = (int)bp.Field<decimal>("ID_PLAN_BRO"),
                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                               FREQ = bp.Field<double>("FREQ"),
                               Descripcion = bp.Field<string>("CALL_SIGN") + " - " + bp.Field<double>("FREQ").ToString(),
                               STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                               POWER = (double)bp.Field<Single>("POWER"),
                               ASL = (double)bp.Field<decimal>("ASL"),
                               STATE = bp.Field<string>("STATE"),
                               DEPARTAMENTO = bp.Field<string>("DEPARTAMENTO"),
                               MUNICIPIO = bp.Field<string>("MUNICIPIO"),
                               CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                           }).ToList();
                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CrearConcesionarioSolicitudOtorga(int IDUserWeb, ST_RDSSolicitud SolicitudTemp, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, ST_RDSContacto Contacto, List<string> Emails, 
            List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia, 
            string CallSign, ST_RDSDatosComunidad DatosComunidad)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", SOL_TYPE=" + Serializer.Serialize(SOL_TYPE) + "LIC_NUMBER=" + LIC_NUMBER + "NitOperador=" + NitOperador + ", Plan=" + Serializer.Serialize(Plan) + ", Contacto=" + Serializer.Serialize(Contacto);

            try
            {
                
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                AZTools AZTools = new AZTools();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();                

                sSQL = @"SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;

                        SELECT isnull(Max(SOL_NUMBER), 0) + 1 SOL_NUMBER From RADIO.SOLICITUDES SOL where CUS_IDENT='" + NitOperador + @"'
                        
                        SELECT MAX(SERV_ID) AS MAX_SERV_ID
                        FROM   RADIO.EXPEDIENTES_TEMPORALES

                        SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH, CUS_COUNTRYCODE, CUS_STATECODE, CUS_CITYCODE
                        FROM RADIO.V_CUSTOMERS_AZ CUS
                        where CUS.CUS_IDENT = '" + NitOperador + @"'

                        SELECT TOP 1 ConfiguracionCalificaciones, FechaFinRecepcionSolicitudes
                        FROM RADIO.CONFIGURACION_PROCESO
                        WHERE SOL_TYPE = " + SOL_TYPE.SOL_TYPE_ID + @"

                   
                        ";

                var dtSet0 = cData.ObtenerDataSet(sSQL);

                Helper.TXT = (from bp in dtSet0.Tables[0].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                int SOL_NUMBER = dtSet0.Tables[1].AsEnumerable().AsQueryable().First().Field<int>("SOL_NUMBER");

                int nuevoIdExpediente = 1000000;
                if (dtSet0.Tables[2].AsEnumerable().AsQueryable().First()[0] != System.DBNull.Value)
                    nuevoIdExpediente = dtSet0.Tables[2].AsEnumerable().AsQueryable().First().Field<int>("MAX_SERV_ID") + 1;
                int nuevoNumeroExpediente = nuevoIdExpediente + 1000000;

                DataTable DTOperador = dtSet0.Tables[3];

                ST_RDSOperador Operador = (from bp in DTOperador.AsEnumerable().AsQueryable()
                                           select new ST_RDSOperador
                                           {
                                               CUS_ID = bp.Field<int>("CUS_ID"),
                                               CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                               CUS_NAME = bp.Field<string>("CUS_NAME"),
                                               CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                               CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                               CUS_COUNTRYCODE = bp.Field<string>("CUS_COUNTRYCODE"),
                                               CUS_STATECODE = bp.Field<string>("CUS_STATECODE"),
                                               CUS_CITYCODE = bp.Field<string>("CUS_CITYCODE"),
                                           }).ToList().FirstOrDefault();

                string configuracionCalificaciones = dtSet0.Tables[4].Rows[0].Field<string>("ConfiguracionCalificaciones");
                DateTime FechaCierre = dtSet0.Tables[4].Rows[0].Field<DateTime>("FechaFinRecepcionSolicitudes");
                if (DateTime.Now > FechaCierre)
                    throw new ApplicationException("La fecha límite para radicar la solicitud ya venció.");

                sSQL = @"
                        Select Top 0 SOL_UID, CUS_ID, CUS_IDENT, CUS_NAME, SERV_ID, SERV_NUMBER, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, GROUP_CURRENT, ROLE_CURRENT, SOL_CREATOR,
                        Clasificacion, NivelCubrimiento, Tecnologia, NombreComunidad, IdLugarComunidad, DireccionComunidad, CiudadComunidad, DepartamentoComunidad, CodeLugarComunidad, Territorio,
                        ResolucionMinInterior, GrupoEtnico, NombrePuebloComunidad, NombreResguardo, NombreCabildo, NombreKumpania, NombreConsejo, NombreOrganizacionBase, Evaluaciones, AZ_DIRECTORIO
                        From RADIO.SOLICITUDES;

                        Select Top 0 SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID, USR_ROLE16_ID, USR_ROLE32_ID
                        From RADIO.SOL_USERS;

                        Select Top 0 ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_ROLE1_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, HABILITANTES
                        From RADIO.ANEXOS;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        Select Top 0 SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT
		                FROM RADIO.TECH_ANALISIS; 

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES;

                        SELECT Top 0 SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, CODE_AREA_MUNICIPIO, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, 
                            MODULATION, SERV_STATUS, CUST_TXT3
                        FROM RADIO.EXPEDIENTES_TEMPORALES;

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES_VIABILIDAD;

                        ";                

                var dtSet = cData.ObtenerDataSet(sSQL);

                if (SolicitudTemp != null)
                {

                    sSQL += @"
                            Select SOL_UID From RADIO.SOLICITUDES_TEMP
                            Where SOL_UID= '" + SolicitudTemp.SOL_UID + @"' ;

                            Select ANX_UID From RADIO.ANEXOS_TEMP
                            Where SOL_UID= '" + SolicitudTemp.SOL_UID + @"';

                            SELECT SERV_ID FROM RADIO.EXPEDIENTES_TEMPORALES
                            WHERE SERV_ID = '" + SolicitudTemp.SERV_ID + @"';
                            ";

                    dtSet = cData.ObtenerDataSet(sSQL);

                    DataTable DTSolicitudTemp = dtSet.Tables[10];
                    DataTable DTAnexosTemp = dtSet.Tables[11];
                    DataTable DTExpedienteTemp = dtSet.Tables[12];
                    EliminarSolicitudTemporal(ref DTSolicitudTemp, ref DTAnexosTemp, ref DTExpedienteTemp);
                }

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTSolUsers = dtSet.Tables[1];
                DataTable DTAnexo = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTTech = dtSet.Tables[4];
                DataTable DTResAnalisis = dtSet.Tables[5];
                DataTable DTResolucion = dtSet.Tables[6];
                DataTable DTExpedienteTemporal = dtSet.Tables[7];
                DataTable DTResAnalisisViabilidad = dtSet.Tables[8];
                DataTable DTResolucionViabilidad = dtSet.Tables[9];


                DataRow DRE = DTExpedienteTemporal.NewRow();
                DRE["SERV_ID"] = nuevoIdExpediente;
                DRE["SERV_NUMBER"] = nuevoNumeroExpediente;
                DRE["LIC_NUMBER"] = LIC_NUMBER;
                DRE["CUS_ID"] = Operador.CUS_ID;
                DRE["CUS_IDENT"] = Operador.CUS_IDENT;
                DRE["CUS_NAME"] = Operador.CUS_NAME;
                DRE["CUS_BRANCH"] = Operador.CUS_BRANCH;
                DRE["CUS_EMAIL"] = Operador.CUS_EMAIL;

                if (Plan != null)
                {
                    DRE["STATION_CLASS"] = Plan.STATION_CLASS;
                    DRE["STATE"] = Plan.STATE;
                    DRE["DEPTO"] = Plan.DEPARTAMENTO;
                    DRE["CITY"] = Plan.MUNICIPIO;
                    DRE["CODE_AREA_MUNICIPIO"] = Plan.CODE_AREA_MUNICIPIO;
                    DRE["POWER"] = Plan.POWER;
                    DRE["ASL"] = Plan.ASL;
                    DRE["FREQUENCY"] = Plan.FREQ;
                    DRE["CALL_SIGN"] = Plan.CALL_SIGN;
                    DRE["SERV_NAME"] = "Temporal " + Plan.CALL_SIGN + " " + Plan.FREQ.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    DRE["DEPTO"] = NombreDepartamentoPlanBro;
                    DRE["CITY"] = NombreMunicipioPlanBro;
                    DRE["CODE_AREA_MUNICIPIO"] = CodeAreaMunicipioPlanBro;
                    DRE["SERV_NAME"] = "Temporal - " + NombreDepartamentoPlanBro.ToUpperInvariant() + " - " + NombreMunicipioPlanBro.ToUpperInvariant();
                    DRE["CALL_SIGN"] = CallSign;
                }

                DRE["STOP_DATE"] = DateTime.Now.AddYears(1);
                DRE["MODULATION"] = Plan != null ? GetModulacionFromFrecuencia(Plan.FREQ) : "TM";
                DRE["SERV_STATUS"] = "eTmp";
                DRE["CUST_TXT3"] = GestionServicio != null ? GestionServicio.ToUpperInvariant() : null;

                DTExpedienteTemporal.Rows.Add(DRE);
                
                ST_RDSExpediente Expediente = new ST_RDSExpediente
                {
                    SERV_ID = nuevoIdExpediente,
                    SERV_NUMBER = nuevoNumeroExpediente,
                    SERV_STATUS = DRE.Field<string>("SERV_STATUS"),
                    STATION_CLASS = DRE.Field<string>("STATION_CLASS"),
                    STATE = DRE.Field<string>("STATE"),
                    DEPTO = DRE.Field<string>("DEPTO") != null ? DRE.Field<string>("DEPTO").ToUpperInvariant() : null,
                    CITY = DRE.Field<string>("CITY") != null ? DRE.Field<string>("CITY").ToUpperInvariant() : null,
                    CODE_AREA_MUNICIPIO = DRE.Field<int>("CODE_AREA_MUNICIPIO"),
                    POWER = DRE.Field<double?>("POWER"),
                    ASL = DRE.Field<double?>("ASL"),
                    FREQUENCY = DRE.Field<double?>("FREQUENCY"),
                    CALL_SIGN = DRE.Field<string>("CALL_SIGN"),
                    SERV_NAME = DRE.Field<string>("SERV_NAME") != null ? DRE.Field<string>("SERV_NAME").ToUpperInvariant() : null,
                    MODULATION = DRE.Field<string>("MODULATION"),
                    STOP_DATE = DRE.Field<DateTime?>("STOP_DATE").HasValue ? DRE.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                    Operador = new ST_RDSOperador
                    {
                        CUS_ID = DRE.Field<int>("CUS_ID"),
                        CUS_IDENT = DRE.Field<string>("CUS_IDENT"),
                        CUS_NAME = DRE.Field<string>("CUS_NAME"),
                        CUS_EMAIL = DRE.Field<string>("CUS_EMAIL"),
                        CUS_BRANCH = DRE.Field<int>("CUS_BRANCH"),
                        CUS_COUNTRYCODE = Operador.CUS_COUNTRYCODE,
                        CUS_STATECODE = Operador.CUS_STATECODE,
                        CUS_CITYCODE = Operador.CUS_CITYCODE,
                    },
                    Contactos = new List<ST_RDSContacto>
                    {
                        new ST_RDSContacto
                                 {
                                     CONT_ID = Contacto.CONT_ID,
                                     CONT_NUMBER = Contacto.CONT_NUMBER,
                                     CONT_NAME = Contacto.CONT_NAME,
                                     CONT_TEL = Contacto.CONT_TEL,
                                     CONT_ROLE = Contacto.CONT_ROLE,
                                     CONT_TITLE = GetContactTitle(Contacto.CONT_ROLE),
                                 }
                    }
                };

                Guid SOL_UID = Guid.NewGuid();

                DataRow DRS = DTSolicitud.NewRow();
                DRS["SOL_UID"] = SOL_UID;
                DRS["CUS_ID"] = Expediente.Operador.CUS_ID;
                DRS["CUS_IDENT"] = Expediente.Operador.CUS_IDENT;
                DRS["CUS_NAME"] = Expediente.Operador.CUS_NAME;
                DRS["SERV_ID"] = Expediente.SERV_ID;
                DRS["SERV_NUMBER"] = Expediente.SERV_NUMBER;
                DRS["SOL_TYPE_ID"] = SOL_TYPE.SOL_TYPE_ID;
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.SinRadicar;
                DRS["SOL_CREATOR"] = (int)SOL_CREATOR.Concesionario;

                Contacto.CONT_EMAIL = string.Join(",", Emails.Where(bp => !string.IsNullOrEmpty(bp)).ToList());

                DRS["CONT_ID"] = Contacto.CONT_ID;
                DRS["CONT_NAME"] = Contacto.CONT_NAME;
                DRS["CONT_NUMBER"] = Contacto.CONT_NUMBER;
                DRS["CONT_EMAIL"] = Contacto.CONT_EMAIL;
                DRS["CONT_ROLE"] = Contacto.CONT_ROLE;

                DRS["SOL_NUMBER"] = SOL_NUMBER;
                DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;

                DRS["CAMPOS_SOL"] = "";

                DRS["Clasificacion"] = Clasificacion;
                DRS["NivelCubrimiento"] = NivelCubrimiento;
                DRS["Tecnologia"] = Tecnologia;
                if (DatosComunidad != null)
                {
                    DRS["NombreComunidad"] = DatosComunidad.NombreComunidad;
                    DRS["IdLugarComunidad"] = DatosComunidad.IdLugarComunidad.HasValue ? (object)DatosComunidad.IdLugarComunidad.Value : DBNull.Value;
                    DRS["DireccionComunidad"] = DatosComunidad.DireccionComunidad;
                    DRS["CiudadComunidad"] = DatosComunidad.CiudadComunidad;
                    DRS["DepartamentoComunidad"] = DatosComunidad.DepartamentoComunidad;
                    DRS["CodeLugarComunidad"] = DatosComunidad.CodeLugarComunidad;
                    DRS["Territorio"] = DatosComunidad.Territorio;
                    DRS["ResolucionMinInterior"] = DatosComunidad.ResolucionMinInterior;
                    DRS["GrupoEtnico"] = DatosComunidad.GrupoEtnico;
                    DRS["NombrePuebloComunidad"] = DatosComunidad.NombrePuebloComunidad;
                    DRS["NombreResguardo"] = DatosComunidad.NombreResguardo;
                    DRS["NombreCabildo"] = DatosComunidad.NombreCabildo;
                    DRS["NombreKumpania"] = DatosComunidad.NombreKumpania;
                    DRS["NombreConsejo"] = DatosComunidad.NombreConsejo;
                    DRS["NombreOrganizacionBase"] = DatosComunidad.NombreOrganizacionBase;
                }
                DRS["Evaluaciones"] = configuracionCalificaciones;
                DTSolicitud.Rows.Add(DRS);

                List<ST_RDSUsuario> AsignacionesGroup1 = AutomaticAssignGroup1_V1(ref cData, ref DTSolUsers, ref Helper, SOL_UID);

                CrearAnalisisTecnico(ref DTTech, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisis, SOL_UID);
                CrearResolucion(ref DTResolucion, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisisViabilidad, SOL_UID);
                CrearResolucion(ref DTResolucionViabilidad, SOL_UID);

                int ANX_NUMBER = 1;
                foreach (var File in Files)
                {
                    DataRow DRA = DTAnexo.NewRow();
                    DRA["ANX_UID"] = Guid.NewGuid();
                    DRA["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_CREATED_DATE"] = DateTime.Now;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRA["SOL_UID"] = SOL_UID;
                    DRA["HABILITANTES"] = true;
                    DRA["ANX_NUMBER"] = ANX_NUMBER++;
                    DTAnexo.Rows.Add(DRA);
                }
                int ANXP_NUMBER = 1;
                foreach (var File in FilesPond)
                {
                    DataRow DRAP = DTAnexo.NewRow();
                    DRAP["ANX_UID"] = Guid.NewGuid();
                    DRAP["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRAP["ANX_NAME"] = File.NAME;
                    DRAP["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRAP["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRAP["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRAP["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRAP["ANX_CREATED_DATE"] = DateTime.Now;
                    DRAP["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRAP["SOL_UID"] = SOL_UID;
                    DRAP["HABILITANTES"] = false;
                    DRAP["ANX_NUMBER"] = ANXP_NUMBER++;
                    DTAnexo.Rows.Add(DRAP);
                }
                ST_RDSSolicitud Solicitud = MakeSolicitud(SOL_UID, SOL_NUMBER, "#####", SOL_TYPE.SOL_TYPE_NAME, ref Expediente, ref Contacto);

                Solicitud.USR_ADMIN.USR_ROLE1 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First();
                Solicitud.USR_ADMIN.USR_ROLE4 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First();
                Solicitud.USR_ADMIN.USR_ROLE8 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                Solicitud.USR_ADMIN.USR_ROLE16 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First();
                Solicitud.USR_ADMIN.USR_ROLE32 = AsignacionesGroup1.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First();

                MemoryStream mem = null;
                //ResultadoRadicacionAlfa resultado = AlfaRadicarConcesionarioToMinticSolicitar(ref Solicitud, Files, ref Helper, ref mem);
                //Solicitud.Radicado = resultado.CodigoRadicado;
                AZRadicar.RespuestaOperacion resultado = AZTools.AZRadicarConcesionarioToMinticSolicitar(ref Solicitud, Files, ref Helper, ref mem);
                Solicitud.Radicado = resultado.NuevoRaNumero;

                //DTSolicitud.Rows[0]["RAD_ALFANET"] = resultado.CodigoRadicado;
                DTSolicitud.Rows[0]["RAD_ALFANET"] = resultado.NuevoRaNumero;
                DTSolicitud.Rows[0]["AZ_DIRECTORIO"] = Solicitud.AZ_DIRECTORIO;
                DTSolicitud.Rows[0]["SOL_CREATED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_MODIFIED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_LAST_STATE_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_STATE_ID"] = (int)SOL_STATES.Radicada;

                AddNewDocument(ref DTDocuments, SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Solicitud, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, resultado.NuevoRaNumero, ref mem, "Solicitud " + SOL_TYPE.SOL_TYPE_NAME);

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_CREADA, ref Solicitud, ref Helper);
                Ret = EnviarCorreo(MAIL_TYPE.MINTIC_SOL_ASSIGNED, ref Solicitud, ref Helper);

                bool mostrarAlertaAsignacionDuplicada = false;
                if (Plan != null)
                {
                    mostrarAlertaAsignacionDuplicada = cData.TraerValor(
                        @"SELECT COUNT(0) 
                            FROM RADIO.V_EXPEDIENTES INNER JOIN 
                                RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN 
                            WHERE (RADIO.V_EXPEDIENTES.CUS_IDENT = N'" + Expediente.Operador.CUS_IDENT + @"') AND 
                                (RADIO.V_PLAN_BRO.STATE = N'ASIGNADO') AND 
                                (RADIO.V_PLAN_BRO.CODE_AREA_MUNICIPIO = " + Plan.CODE_AREA_MUNICIPIO + @") AND 
                                (CASE WHEN RADIO.V_PLAN_BRO.FREQ < 10 THEN 'AM' ELSE 'FM' END = '" + GetModulacionFromFrecuencia(Plan.FREQ) + @"') AND 
                                (RADIO.V_EXPEDIENTES.EsTemporal = 0)", 0) > 0;
                }

                string ResultMessage = GetMensajeAlertaSolicitudOtorga(SOL_TYPE.SOL_TYPE_NAME, resultado.NuevoRaNumero, mostrarAlertaAsignacionDuplicada, false);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_SOL_CONCESIONARIO_CREATE", LOG_SOURCE.Concesionario, ROLES.NoAplica, "", resultado.NuevoRaNumero));
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", ""));
                AddLogAutomaticAssign(ref DynamicSolicitud, SOL_UID, ref lstLogs, LOG_SOURCE.MinTIC, ref AsignacionesGroup1, ref Helper, ROLES.Funcionario, ROLES.Coordinador, ROLES.Subdirector, ROLES.Asesor, ROLES.Director);
                AddLog(ref cData, lstLogs);

                return new ActionInfo(1, ResultMessage);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo GuardarConcesionarioSolicitudOtorga(int IDUserWeb, ST_RDSSolicitud SolicitudTemp, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, ST_RDSContacto Contacto, List<string> Emails,
             List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia,
             string CallSign, ST_RDSDatosComunidad DatosComunidad)
        {

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", SOL_TYPE=" + Serializer.Serialize(SOL_TYPE) + "LIC_NUMBER=" + LIC_NUMBER + "NitOperador=" + NitOperador + ", Plan=" + Serializer.Serialize(Plan) + ", Contacto=" + Serializer.Serialize(Contacto);

            try
            {
                
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"                                         
                        SELECT MAX(SERV_ID) AS MAX_SERV_ID
                        FROM   RADIO.EXPEDIENTES_TEMPORALES

                        SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH, CUS_COUNTRYCODE, CUS_STATECODE, CUS_CITYCODE
                        FROM RADIO.V_CUSTOMERS_AZ CUS
                        where CUS.CUS_IDENT = '" + NitOperador + @"'

                        ";

                var dtSet0 = cData.ObtenerDataSet(sSQL);

                int nuevoIdExpediente = 1000000;
                if (dtSet0.Tables[0].AsEnumerable().AsQueryable().First()[0] != System.DBNull.Value)
                    nuevoIdExpediente = dtSet0.Tables[0].AsEnumerable().AsQueryable().First().Field<int>("MAX_SERV_ID") + 1;
                int nuevoNumeroExpediente = nuevoIdExpediente + 1000000;

                DataTable DTOperador = dtSet0.Tables[1];

                ST_RDSOperador Operador = (from bp in DTOperador.AsEnumerable().AsQueryable()
                                           select new ST_RDSOperador
                                           {
                                               CUS_ID = bp.Field<int>("CUS_ID"),
                                               CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                               CUS_NAME = bp.Field<string>("CUS_NAME"),
                                               CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                               CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                               CUS_COUNTRYCODE = bp.Field<string>("CUS_COUNTRYCODE"),
                                               CUS_STATECODE = bp.Field<string>("CUS_STATECODE"),
                                               CUS_CITYCODE = bp.Field<string>("CUS_CITYCODE"),
                                           }).ToList().FirstOrDefault();


                sSQL = @"
                        Select Top 0 SOL_UID, CUS_ID, CUS_IDENT, CUS_NAME, SERV_ID, SERV_NUMBER, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, 
                        RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, GROUP_CURRENT, ROLE_CURRENT, SOL_CREATOR,
                        Clasificacion, NivelCubrimiento, Tecnologia, NombreComunidad, IdLugarComunidad, DireccionComunidad, CiudadComunidad, DepartamentoComunidad, CodeLugarComunidad, Territorio,
                        ResolucionMinInterior, GrupoEtnico, NombrePuebloComunidad, NombreResguardo, NombreCabildo, NombreKumpania, NombreConsejo, NombreOrganizacionBase, Evaluaciones, AZ_DIRECTORIO
                        From RADIO.SOLICITUDES_TEMP;

                        Select Top 0 ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_ROLE1_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, HABILITANTES
                        From RADIO.ANEXOS_TEMP;

                        SELECT Top 0 SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, CODE_AREA_MUNICIPIO, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, 
                        MODULATION, SERV_STATUS, CUST_TXT3
                        FROM RADIO.EXPEDIENTES_TEMPORALES;

                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);
                if (SolicitudTemp != null)
                {
                    sSQL += @"
                            Select SOL_UID From RADIO.SOLICITUDES_TEMP
                            Where SOL_UID= '" + SolicitudTemp.SOL_UID + @"' ;

                            Select ANX_UID From RADIO.ANEXOS_TEMP
                            Where SOL_UID= '" + SolicitudTemp.SOL_UID + @"';

                            SELECT SERV_ID FROM RADIO.EXPEDIENTES_TEMPORALES
                            WHERE SERV_ID = '" + SolicitudTemp.SERV_ID + @"';
                            ";

                    dtSet = cData.ObtenerDataSet(sSQL);

                    DataTable DTSolicitudTemp = dtSet.Tables[3];
                    DataTable DTAnexosTemp = dtSet.Tables[4];
                    DataTable DTExpedienteTemp = dtSet.Tables[5];
                    EliminarSolicitudTemporal(ref DTSolicitudTemp, ref DTAnexosTemp, ref DTExpedienteTemp);
                }

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTAnexo = dtSet.Tables[1];
                DataTable DTExpedienteTemporal = dtSet.Tables[2];

                DataRow DRE = DTExpedienteTemporal.NewRow();
                DRE["SERV_ID"] = nuevoIdExpediente;
                DRE["SERV_NUMBER"] = nuevoNumeroExpediente;
                DRE["LIC_NUMBER"] = LIC_NUMBER;
                DRE["CUS_ID"] = Operador.CUS_ID;
                DRE["CUS_IDENT"] = Operador.CUS_IDENT;
                DRE["CUS_NAME"] = Operador.CUS_NAME;
                DRE["CUS_BRANCH"] = Operador.CUS_BRANCH;
                DRE["CUS_EMAIL"] = Operador.CUS_EMAIL;

                if (Plan != null)
                {
                    DRE["STATION_CLASS"] = Plan.STATION_CLASS;
                    DRE["STATE"] = Plan.STATE;
                    DRE["DEPTO"] = Plan.DEPARTAMENTO;
                    DRE["CITY"] = Plan.MUNICIPIO;
                    DRE["CODE_AREA_MUNICIPIO"] = Plan.CODE_AREA_MUNICIPIO;
                    DRE["POWER"] = Plan.POWER;
                    DRE["ASL"] = Plan.ASL;
                    DRE["FREQUENCY"] = Plan.FREQ;
                    DRE["CALL_SIGN"] = Plan.CALL_SIGN;
                    DRE["SERV_NAME"] = "Temporal " + Plan.CALL_SIGN + " " + Plan.FREQ.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    DRE["DEPTO"] = NombreDepartamentoPlanBro;
                    DRE["CITY"] = NombreMunicipioPlanBro;
                    DRE["CODE_AREA_MUNICIPIO"] = CodeAreaMunicipioPlanBro;
                    DRE["SERV_NAME"] = "Temporal - " + NombreDepartamentoPlanBro.ToUpperInvariant() + " - " + NombreMunicipioPlanBro.ToUpperInvariant();
                    DRE["CALL_SIGN"] = CallSign;
                }

                DRE["STOP_DATE"] = DateTime.Now.AddYears(1);
                DRE["MODULATION"] = Plan != null ? GetModulacionFromFrecuencia(Plan.FREQ) : "TM";
                DRE["SERV_STATUS"] = "eTmp";
                DRE["CUST_TXT3"] = GestionServicio != null ? GestionServicio.ToUpperInvariant() : null;

                DTExpedienteTemporal.Rows.Add(DRE);

                ST_RDSExpediente Expediente = new ST_RDSExpediente
                {
                    SERV_ID = nuevoIdExpediente,
                    SERV_NUMBER = nuevoNumeroExpediente,
                    SERV_STATUS = DRE.Field<string>("SERV_STATUS"),
                    STATION_CLASS = DRE.Field<string>("STATION_CLASS"),
                    STATE = DRE.Field<string>("STATE"),
                    DEPTO = DRE.Field<string>("DEPTO") != null ? DRE.Field<string>("DEPTO").ToUpperInvariant() : null,
                    CITY = DRE.Field<string>("CITY") != null ? DRE.Field<string>("CITY").ToUpperInvariant() : null,
                    CODE_AREA_MUNICIPIO = DRE.Field<int>("CODE_AREA_MUNICIPIO"),
                    POWER = DRE.Field<double?>("POWER"),
                    ASL = DRE.Field<double?>("ASL"),
                    FREQUENCY = DRE.Field<double?>("FREQUENCY"),
                    CALL_SIGN = DRE.Field<string>("CALL_SIGN"),
                    SERV_NAME = DRE.Field<string>("SERV_NAME") != null ? DRE.Field<string>("SERV_NAME").ToUpperInvariant() : null,
                    MODULATION = DRE.Field<string>("MODULATION"),
                    STOP_DATE = DRE.Field<DateTime?>("STOP_DATE").HasValue ? DRE.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                    Operador = new ST_RDSOperador
                    {
                        CUS_ID = DRE.Field<int>("CUS_ID"),
                        CUS_IDENT = DRE.Field<string>("CUS_IDENT"),
                        CUS_NAME = DRE.Field<string>("CUS_NAME"),
                        CUS_EMAIL = DRE.Field<string>("CUS_EMAIL"),
                        CUS_BRANCH = DRE.Field<int>("CUS_BRANCH"),
                        CUS_COUNTRYCODE = Operador.CUS_COUNTRYCODE,
                        CUS_STATECODE = Operador.CUS_STATECODE,
                        CUS_CITYCODE = Operador.CUS_CITYCODE,
                    },
                    Contactos = new List<ST_RDSContacto>
                    {
                        new ST_RDSContacto
                                 {
                                     CONT_ID = Contacto.CONT_ID,
                                     CONT_NUMBER = Contacto.CONT_NUMBER,
                                     CONT_NAME = Contacto.CONT_NAME,
                                     CONT_TEL = Contacto.CONT_TEL,
                                     CONT_ROLE = Contacto.CONT_ROLE,
                                     CONT_TITLE = GetContactTitle(Contacto.CONT_ROLE),
                                 }
                    }
                };

                Guid SOL_UID = Guid.NewGuid();

                DataRow DRS = DTSolicitud.NewRow();
                DRS["SOL_UID"] = SOL_UID;
                DRS["CUS_ID"] = Expediente.Operador.CUS_ID;
                DRS["CUS_IDENT"] = Expediente.Operador.CUS_IDENT;
                DRS["CUS_NAME"] = Expediente.Operador.CUS_NAME;
                DRS["SERV_ID"] = Expediente.SERV_ID;
                DRS["SERV_NUMBER"] = Expediente.SERV_NUMBER;
                DRS["SOL_TYPE_ID"] = SOL_TYPE.SOL_TYPE_ID;
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.SinRadicar;
                DRS["SOL_CREATOR"] = (int)SOL_CREATOR.Concesionario;

                Contacto.CONT_EMAIL = string.Join(",", Emails.Where(bp => !string.IsNullOrEmpty(bp)).ToList());

                DRS["CONT_ID"] = Contacto.CONT_ID;
                DRS["CONT_NAME"] = Contacto.CONT_NAME;
                DRS["CONT_NUMBER"] = Contacto.CONT_NUMBER;
                DRS["CONT_EMAIL"] = Contacto.CONT_EMAIL;
                DRS["CONT_ROLE"] = Contacto.CONT_ROLE;

                DRS["SOL_NUMBER"] = 0;
                DRS["GROUP_CURRENT"] = (int)GROUPS.NoAplica;
                DRS["ROLE_CURRENT"] = (int)ROLES.NoAplica;

                DRS["CAMPOS_SOL"] = DBNull.Value;

                DRS["Clasificacion"] = Clasificacion;
                DRS["NivelCubrimiento"] = NivelCubrimiento;
                DRS["Tecnologia"] = Tecnologia;
                if (DatosComunidad != null)
                {
                    DRS["NombreComunidad"] = DatosComunidad.NombreComunidad;
                    DRS["IdLugarComunidad"] = DatosComunidad.IdLugarComunidad.HasValue ? (object)DatosComunidad.IdLugarComunidad.Value : DBNull.Value;
                    DRS["DireccionComunidad"] = DatosComunidad.DireccionComunidad;
                    DRS["CiudadComunidad"] = DatosComunidad.CiudadComunidad;
                    DRS["DepartamentoComunidad"] = DatosComunidad.DepartamentoComunidad;
                    DRS["CodeLugarComunidad"] = DatosComunidad.CodeLugarComunidad;
                    DRS["Territorio"] = DatosComunidad.Territorio;
                    DRS["ResolucionMinInterior"] = DatosComunidad.ResolucionMinInterior;
                    DRS["GrupoEtnico"] = DatosComunidad.GrupoEtnico;
                    DRS["NombrePuebloComunidad"] = DatosComunidad.NombrePuebloComunidad;
                    DRS["NombreResguardo"] = DatosComunidad.NombreResguardo;
                    DRS["NombreCabildo"] = DatosComunidad.NombreCabildo;
                    DRS["NombreKumpania"] = DatosComunidad.NombreKumpania;
                    DRS["NombreConsejo"] = DatosComunidad.NombreConsejo;
                    DRS["NombreOrganizacionBase"] = DatosComunidad.NombreOrganizacionBase;
                }
                DRS["Evaluaciones"] = DBNull.Value;
                DTSolicitud.Rows.Add(DRS);


                int ANX_NUMBER = 1;
                foreach (var File in Files)
                {
                    DataRow DRA = DTAnexo.NewRow();
                    DRA["ANX_UID"] = Guid.NewGuid();
                    DRA["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_CREATED_DATE"] = DateTime.Now;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRA["SOL_UID"] = SOL_UID;
                    DRA["HABILITANTES"] = true;
                    DRA["ANX_NUMBER"] = ANX_NUMBER++;
                    DTAnexo.Rows.Add(DRA);
                }
                int ANXP_NUMBER = 1;
                foreach (var File in FilesPond)
                {
                    DataRow DRAP = DTAnexo.NewRow();
                    DRAP["ANX_UID"] = Guid.NewGuid();
                    DRAP["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRAP["ANX_NAME"] = File.NAME;
                    DRAP["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRAP["ANX_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
                    DRAP["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
                    DRAP["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRAP["ANX_CREATED_DATE"] = DateTime.Now;
                    DRAP["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRAP["SOL_UID"] = SOL_UID;
                    DRAP["HABILITANTES"] = false;
                    DRAP["ANX_NUMBER"] = ANXP_NUMBER++;
                    DTAnexo.Rows.Add(DRAP);
                }

                DTSolicitud.Rows[0]["RAD_ALFANET"] = DBNull.Value;
                DTSolicitud.Rows[0]["AZ_DIRECTORIO"] = DBNull.Value;
                DTSolicitud.Rows[0]["SOL_CREATED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_MODIFIED_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_LAST_STATE_DATE"] = DateTime.Now;
                DTSolicitud.Rows[0]["SOL_STATE_ID"] = (int)SOL_STATES.SinRadicar;

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                string ResultMessage = GetMensajeAlertaSolicitudOtorga(SOL_TYPE.SOL_TYPE_NAME, "", false, true);

                return new ActionInfo(1, ResultMessage);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }
   
        public ActionInfo CrearFuncionarioSolicitudOtorga(int IDUserWeb, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, List<string> Emails,
            List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia,
            string CallSign, ST_RDSDatosComunidad DatosComunidad, string RadicadoCodigo, string RadicadoFecha, bool bEnviarCorreoConcesionario)
        {

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", SOL_TYPE=" + Serializer.Serialize(SOL_TYPE) + "LIC_NUMBER=" + LIC_NUMBER + "NitOperador=" + NitOperador + ", Plan=" + Serializer.Serialize(Plan);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;

                        SELECT isnull(Max(SOL_NUMBER), 0) + 1 SOL_NUMBER From RADIO.SOLICITUDES SOL where CUS_IDENT='" + NitOperador + @"'
                        
                        SELECT MAX(SERV_ID) AS MAX_SERV_ID
                        FROM   RADIO.EXPEDIENTES_TEMPORALES

                        SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH
                        FROM RADIO.V_CUSTOMERS CUS
                        where CUS.CUS_IDENT = '" + NitOperador + @"'

					    SELECT CONT_ID, CUS_IDENT, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER, CONT_TITLE, CONT_EMAIL
                        FROM RADIO.V_CONTACTOS_SIN_EXPEDIENTE
                        where CUS_IDENT = '" + NitOperador + @"'
                        ";

                var dtSet0 = cData.ObtenerDataSet(sSQL);

                Helper.TXT = (from bp in dtSet0.Tables[0].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                int SOL_NUMBER = dtSet0.Tables[1].AsEnumerable().AsQueryable().First().Field<int>("SOL_NUMBER");

                int nuevoIdExpediente = 1000000;
                if (dtSet0.Tables[2].AsEnumerable().AsQueryable().First()[0] != System.DBNull.Value)
                    nuevoIdExpediente = dtSet0.Tables[2].AsEnumerable().AsQueryable().First().Field<int>("MAX_SERV_ID") + 1;
                int nuevoNumeroExpediente = nuevoIdExpediente + 1000000;

                DataTable DTOperador = dtSet0.Tables[3];

                ST_RDSOperador Operador = (from bp in DTOperador.AsEnumerable().AsQueryable()
                                           select new ST_RDSOperador
                                           {
                                               CUS_ID = bp.Field<int>("CUS_ID"),
                                               CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                               CUS_NAME = bp.Field<string>("CUS_NAME"),
                                               CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                               CUS_BRANCH = bp.Field<int>("CUS_BRANCH"),
                                           }).ToList().FirstOrDefault();

                ST_RDSContacto Contacto = (from bp in dtSet0.Tables[4].AsEnumerable().AsQueryable()
                                           select new ST_RDSContacto
                                           {
                                               CONT_ID = bp.Field<int>("CONT_ID"),
                                               CONT_NAME = bp.Field<string>("CONT_NAME"),
                                               CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                                               CONT_TEL = bp.Field<string>("CONT_TEL"),
                                               CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                                               CONT_TITLE = bp.Field<string>("CONT_TITLE"),
                                               CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                                           }).ToList().FirstOrDefault();

                sSQL = @"
                        Select Top 0 SOL_UID, CUS_ID, CUS_IDENT, CUS_NAME, SERV_ID, SERV_NUMBER, SOL_TYPE_ID, SOL_STATE_ID, CONT_ID, CONT_NAME, CONT_EMAIL, CONT_ROLE, CONT_NUMBER, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, GROUP_CURRENT, ROLE_CURRENT, SOL_CREATOR,
                        Clasificacion, NivelCubrimiento, Tecnologia, NombreComunidad, IdLugarComunidad, DireccionComunidad, CiudadComunidad, DepartamentoComunidad, CodeLugarComunidad, Territorio,
                        ResolucionMinInterior, GrupoEtnico, NombrePuebloComunidad, NombreResguardo, NombreCabildo, NombreKumpania, NombreConsejo, NombreOrganizacionBase, AZ_DIRECTORIO
                        From RADIO.SOLICITUDES;

                        Select Top 0 SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID, USR_ROLE16_ID, USR_ROLE32_ID
                        From RADIO.SOL_USERS;

                        Select Top 0 ANX_UID, ANX_TYPE_ID, ANX_NAME, ANX_STATE_ID, ANX_ROLE1_STATE_ID, ANX_CONTENT_TYPE, ANX_CREATED_DATE, ANX_MODIFIED_DATE, SOL_UID, ANX_NUMBER, ANX_FILE, HABILITANTES
                        From RADIO.ANEXOS;

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        Select Top 0 SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT
		                FROM RADIO.TECH_ANALISIS; 

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES;

                        SELECT Top 0 SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, CODE_AREA_MUNICIPIO, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, 
                            MODULATION, SERV_STATUS, CUST_TXT3
                        FROM RADIO.EXPEDIENTES_TEMPORALES;

                        Select Top 0 SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, 
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD;

                        Select Top 0 SOL_UID
		                FROM RADIO.RESOLUCIONES_VIABILIDAD;

                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTSolUsers = dtSet.Tables[1];
                DataTable DTAnexo = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTTech = dtSet.Tables[4];
                DataTable DTResAnalisis = dtSet.Tables[5];
                DataTable DTResolucion = dtSet.Tables[6];
                DataTable DTExpedienteTemporal = dtSet.Tables[7];
                DataTable DTResAnalisisViabilidad = dtSet.Tables[8];
                DataTable DTResolucionViabilidad = dtSet.Tables[9];

                DataRow DRE = DTExpedienteTemporal.NewRow();
                DRE["SERV_ID"] = nuevoIdExpediente;
                DRE["SERV_NUMBER"] = nuevoNumeroExpediente;
                DRE["LIC_NUMBER"] = LIC_NUMBER;
                DRE["CUS_ID"] = Operador.CUS_ID;
                DRE["CUS_IDENT"] = Operador.CUS_IDENT;
                DRE["CUS_NAME"] = Operador.CUS_NAME;
                DRE["CUS_BRANCH"] = Operador.CUS_BRANCH;
                DRE["CUS_EMAIL"] = Operador.CUS_EMAIL;

                if (Plan != null)
                {
                    DRE["STATION_CLASS"] = Plan.STATION_CLASS;
                    DRE["STATE"] = Plan.STATE;
                    DRE["DEPTO"] = Plan.DEPARTAMENTO;
                    DRE["CITY"] = Plan.MUNICIPIO;
                    DRE["CODE_AREA_MUNICIPIO"] = Plan.CODE_AREA_MUNICIPIO;
                    DRE["POWER"] = Plan.POWER;
                    DRE["ASL"] = Plan.ASL;
                    DRE["FREQUENCY"] = Plan.FREQ;
                    DRE["CALL_SIGN"] = Plan.CALL_SIGN;
                    DRE["SERV_NAME"] = "Temporal " + Plan.CALL_SIGN + " " + Plan.FREQ.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    DRE["DEPTO"] = NombreDepartamentoPlanBro;
                    DRE["CITY"] = NombreMunicipioPlanBro;
                    DRE["CODE_AREA_MUNICIPIO"] = CodeAreaMunicipioPlanBro;
                    DRE["SERV_NAME"] = "Temporal - " + NombreDepartamentoPlanBro.ToUpperInvariant() + " - " + NombreMunicipioPlanBro.ToUpperInvariant();
                    DRE["CALL_SIGN"] = CallSign;
                }

                DRE["STOP_DATE"] = DateTime.Now.AddYears(1);
                DRE["MODULATION"] = Plan != null ? GetModulacionFromFrecuencia(Plan.FREQ) : "TM";
                DRE["SERV_STATUS"] = "eTmp";
                DRE["CUST_TXT3"] = GestionServicio != null ? GestionServicio.ToUpperInvariant() : null;

                DTExpedienteTemporal.Rows.Add(DRE);

                ST_RDSExpediente Expediente = new ST_RDSExpediente
                {
                    SERV_ID = nuevoIdExpediente,
                    SERV_NUMBER = nuevoNumeroExpediente,
                    SERV_STATUS = DRE.Field<string>("SERV_STATUS"),
                    STATION_CLASS = DRE.Field<string>("STATION_CLASS"),
                    STATE = DRE.Field<string>("STATE"),
                    DEPTO = DRE.Field<string>("DEPTO") != null ? DRE.Field<string>("DEPTO").ToUpperInvariant() : null,
                    CITY = DRE.Field<string>("CITY") != null ? DRE.Field<string>("CITY").ToUpperInvariant() : null,
                    CODE_AREA_MUNICIPIO = DRE.Field<int>("CODE_AREA_MUNICIPIO"),
                    POWER = DRE.Field<double?>("POWER"),
                    ASL = DRE.Field<double?>("ASL"),
                    FREQUENCY = DRE.Field<double?>("FREQUENCY"),
                    CALL_SIGN = DRE.Field<string>("CALL_SIGN"),
                    SERV_NAME = DRE.Field<string>("SERV_NAME") != null ? DRE.Field<string>("SERV_NAME").ToUpperInvariant() : null,
                    MODULATION = DRE.Field<string>("MODULATION"),
                    STOP_DATE = DRE.Field<DateTime?>("STOP_DATE").HasValue ? DRE.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                    Operador = new ST_RDSOperador
                    {
                        CUS_ID = DRE.Field<int>("CUS_ID"),
                        CUS_IDENT = DRE.Field<string>("CUS_IDENT"),
                        CUS_NAME = DRE.Field<string>("CUS_NAME"),
                        CUS_EMAIL = DRE.Field<string>("CUS_EMAIL"),
                        CUS_BRANCH = DRE.Field<int>("CUS_BRANCH")
                    },
                    Contactos = new List<ST_RDSContacto>
                    {
                        new ST_RDSContacto
                                 {
                                     CONT_ID = Contacto.CONT_ID,
                                     CONT_NUMBER = Contacto.CONT_NUMBER,
                                     CONT_NAME = Contacto.CONT_NAME,
                                     CONT_TEL = Contacto.CONT_TEL,
                                     CONT_ROLE = Contacto.CONT_ROLE,
                                     CONT_TITLE = GetContactTitle(Contacto.CONT_ROLE),
                                 }
                    }
                };

                Guid SOL_UID = Guid.NewGuid();

                DataRow DRS = DTSolicitud.NewRow();
                DRS["SOL_UID"] = SOL_UID;
                DRS["CUS_ID"] = Expediente.Operador.CUS_ID;
                DRS["CUS_IDENT"] = Expediente.Operador.CUS_IDENT;
                DRS["CUS_NAME"] = Expediente.Operador.CUS_NAME;
                DRS["SERV_ID"] = Expediente.SERV_ID;
                DRS["SERV_NUMBER"] = Expediente.SERV_NUMBER;

                DRS["RAD_ALFANET"] = RadicadoCodigo;
                DRS["SOL_CREATED_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_MODIFIED_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_LAST_STATE_DATE"] = DateTime.ParseExact(RadicadoFecha, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRS["SOL_TYPE_ID"] = (int)SOL_TYPE.SOL_TYPE_ID;
                DRS["SOL_STATE_ID"] = (int)SOL_STATES.Radicada;
                DRS["SOL_CREATOR"] = (int)SOL_CREATOR.FuncionarioMinTIC;
                
                Contacto.CONT_EMAIL = string.Join(",", Emails.Where(bp => !string.IsNullOrEmpty(bp)).ToList());

                DRS["CONT_ID"] = Contacto.CONT_ID;
                DRS["CONT_NAME"] = Contacto.CONT_NAME;
                DRS["CONT_NUMBER"] = Contacto.CONT_NUMBER;
                DRS["CONT_EMAIL"] = Expediente.Operador.CUS_EMAIL;
                DRS["CONT_ROLE"] = Contacto.CONT_ROLE;

                DRS["SOL_NUMBER"] = SOL_NUMBER;
                DRS["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                DRS["ROLE_CURRENT"] = (int)ROLES.Funcionario;

                DRS["CAMPOS_SOL"] = "";

                DRS["Clasificacion"] = Clasificacion;
                DRS["NivelCubrimiento"] = NivelCubrimiento;
                DRS["Tecnologia"] = Tecnologia;
                if (DatosComunidad != null)
                {
                    DRS["NombreComunidad"] = DatosComunidad.NombreComunidad;
                    DRS["IdLugarComunidad"] = DatosComunidad.IdLugarComunidad.HasValue ? (object)DatosComunidad.IdLugarComunidad.Value : DBNull.Value;
                    DRS["DireccionComunidad"] = DatosComunidad.DireccionComunidad;
                    DRS["CiudadComunidad"] = DatosComunidad.CiudadComunidad;
                    DRS["DepartamentoComunidad"] = DatosComunidad.DepartamentoComunidad;
                    DRS["CodeLugarComunidad"] = DatosComunidad.CodeLugarComunidad;
                    DRS["Territorio"] = DatosComunidad.Territorio;
                    DRS["ResolucionMinInterior"] = DatosComunidad.ResolucionMinInterior;
                    DRS["GrupoEtnico"] = DatosComunidad.GrupoEtnico;
                    DRS["NombrePuebloComunidad"] = DatosComunidad.NombrePuebloComunidad;
                    DRS["NombreResguardo"] = DatosComunidad.NombreResguardo;
                    DRS["NombreCabildo"] = DatosComunidad.NombreCabildo;
                    DRS["NombreKumpania"] = DatosComunidad.NombreKumpania;
                    DRS["NombreConsejo"] = DatosComunidad.NombreConsejo;
                    DRS["NombreOrganizacionBase"] = DatosComunidad.NombreOrganizacionBase;
                }
                DTSolicitud.Rows.Add(DRS);

                List<ST_RDSUsuario> AsignacionesGroup1 = AutomaticAssignGroup1_V2(ref cData, ref DTSolUsers, SOL_UID, IDUserWeb);

                CrearAnalisisTecnico(ref DTTech, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisis, SOL_UID);
                CrearResolucion(ref DTResolucion, SOL_UID);
                CrearAnalisisResolucion(ref DTResAnalisisViabilidad, SOL_UID);
                CrearResolucion(ref DTResolucionViabilidad, SOL_UID);

                int ANX_NUMBER = 1;
                foreach (var File in Files)
                {
                    DataRow DRA = DTAnexo.NewRow();
                    DRA["ANX_UID"] = Guid.NewGuid();
                    DRA["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRA["ANX_NAME"] = File.NAME;
                    DRA["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRA["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRA["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRA["ANX_CREATED_DATE"] = DateTime.Now;
                    DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRA["SOL_UID"] = SOL_UID;
                    DRA["HABILITANTES"] = true;
                    DRA["ANX_NUMBER"] = ANX_NUMBER++;
                    DTAnexo.Rows.Add(DRA);
                }

                int ANXP_NUMBER = 1;
                foreach (var File in FilesPond)
                {
                    DataRow DRAP = DTAnexo.NewRow();
                    DRAP["ANX_UID"] = Guid.NewGuid();
                    DRAP["ANX_TYPE_ID"] = File.TYPE_ID;
                    DRAP["ANX_NAME"] = File.NAME;
                    DRAP["ANX_FILE"] = Base64ToByteArray(File.DATA);
                    DRAP["ANX_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRAP["ANX_ROLE1_STATE_ID"] = (int)ROLE1_STATE.Radicado;
                    DRAP["ANX_CONTENT_TYPE"] = File.CONTENT_TYPE;
                    DRAP["ANX_CREATED_DATE"] = DateTime.Now;
                    DRAP["ANX_MODIFIED_DATE"] = DateTime.Now;
                    DRAP["SOL_UID"] = SOL_UID;
                    DRAP["HABILITANTES"] = false;
                    DRAP["ANX_NUMBER"] = ANXP_NUMBER++;
                    DTAnexo.Rows.Add(DRAP);
                }

                ST_RDSSolicitud Solicitud = MakeSolicitud(SOL_UID, SOL_NUMBER, "#####", SOL_TYPE.SOL_TYPE_NAME, ref Expediente, ref Contacto);


                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
                MemoryStream mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SOLICITUD", "Radicado solicitud", "Funcionario MinTIC");
                AddNewDocument(ref DTDocuments, SOL_UID, DOC_CLASS.Radicado, DOC_TYPE.Solicitud, DOC_SOURCE.Concesionario, DOC_SOURCE.MinTIC, RadicadoCodigo, ref mem, "Solicitud " + SOL_TYPE.SOL_TYPE_NAME);


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                if (bEnviarCorreoConcesionario)
                {
                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_CREADA, ref Solicitud, ref Helper);
                }

                bool mostrarAlertaAsignacionDuplicada = false;
                if (Plan != null)
                {
                    mostrarAlertaAsignacionDuplicada = cData.TraerValor(
                        @"SELECT COUNT(0) 
                            FROM RADIO.V_EXPEDIENTES INNER JOIN 
                                RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN 
                            WHERE (RADIO.V_EXPEDIENTES.CUS_IDENT = N'" + Expediente.Operador.CUS_IDENT + @"') AND 
                                (RADIO.V_PLAN_BRO.STATE = N'ASIGNADO') AND 
                                (RADIO.V_PLAN_BRO.CODE_AREA_MUNICIPIO = " + Plan.CODE_AREA_MUNICIPIO + @") AND 
                                (CASE WHEN RADIO.V_PLAN_BRO.FREQ < 10 THEN 'AM' ELSE 'FM' END = '" + GetModulacionFromFrecuencia(Plan.FREQ) + @"') AND 
                                (RADIO.V_EXPEDIENTES.EsTemporal = 0)", 0) > 0;
                }

                string ResultMessage = GetMensajeAlertaSolicitudOtorga(SOL_TYPE.SOL_TYPE_NAME, RadicadoCodigo, mostrarAlertaAsignacionDuplicada, false);

                List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_SOL_FUNCIONARIO_CREATE", LOG_SOURCE.MinTIC, ROLES.Funcionario, AsignacionesGroup1.Where(bp => bp.USR_ROLE == 1).First().USR_NAME, RadicadoCodigo));
                lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO", LOG_SOURCE.MinTIC, ROLES.Funcionario, AsignacionesGroup1.Where(bp => bp.USR_ROLE == 1).First().USR_NAME, ""));
                AddLogAutomaticAssign(ref DynamicSolicitud, SOL_UID, ref lstLogs, LOG_SOURCE.MinTIC, ref AsignacionesGroup1, ref Helper, ROLES.Coordinador, ROLES.Subdirector, ROLES.Asesor, ROLES.Director);

                AddLog(ref cData, lstLogs);

                return new ActionInfo(1, ResultMessage);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public List<ST_RDSViabilidad> GetViabilidades(int IDUserWeb, ST_RDSConfiguracionProcesoOtorga ConfiguracionProcesoOtorga)
        {
            csData cData = null;
            List<ST_RDSViabilidad> mResult = new List<ST_RDSViabilidad>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT SOL_TYPE_ID, RADIO.F_GetEstadoViabilidadCanal(RADIO.V_PLAN_BRO.CALL_SIGN) AS IdEstadoViabilidad , COUNT(*) AS CantidadDeSolicitudes

                            FROM RADIO.V_EXPEDIENTES INNER JOIN
		                            RADIO.SOLICITUDES ON RADIO.V_EXPEDIENTES.SERV_ID = RADIO.SOLICITUDES.SERV_ID INNER JOIN
		                            RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN
									WHERE (RADIO.SOLICITUDES.SOL_TYPE_ID = " + ConfiguracionProcesoOtorga.SOL_TYPE + @" and RADIO.SOLICITUDES.SOL_TYPE_ID !=2 and RADIO.SOLICITUDES.SOL_TYPE_ID !=7)
                            GROUP BY  RADIO.SOLICITUDES.SOL_TYPE_ID, RADIO.F_GetEstadoViabilidadCanal(RADIO.V_PLAN_BRO.CALL_SIGN)
							ORDER BY SOL_TYPE_ID
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSViabilidad
                           {
                               SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                               IdEstadoViabilidad = bp.Field<int>("IdEstadoViabilidad"),
                               CantidadDeSolicitudes = bp.Field<int>("CantidadDeSolicitudes"),

                           }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        private string GetModulacionFromFrecuencia(double frecuencia)
        {
            return frecuencia > 10 ? "FM" : "AM";
        }

        private string GetMensajeAlertaSolicitudOtorga(string TipoSolicitud, string RadicadoCodigo, bool mostrarAlertaAsignacionDuplicada, bool guardadoTemporal)
        {
            string ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido creada satisfactoriamente.  Radicado No. RDS " + RadicadoCodigo + @"
                                         </td>
                                         </tr>
                                        ";
            if (guardadoTemporal)
            {
                ResultMessage = @"<table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido guardada satisfactoriamente, no olvide terminarla antes de las fechas de recepción de propuestas.
                                         </td>
                                         </tr>
                                   ";
            }
            if (mostrarAlertaAsignacionDuplicada)
            {
                ResultMessage += @"<tr>
                                        <td><i style='margin-right:5px;color:yellow' class='ta ta-warning ta-3x'></i></td>
                                            
                                        <td style='width: 550px;'>
                                            <p style='padding-top: 10px;'>
                                                Señor Usuario, recuerde que no puede ser proveedor del Servicio de 
                                                Radiodifusión Sonora en la misma banda y en el mismo municipio en 
                                                el que vaya a funcionar la emisora, de acuerdo con lo establecido 
                                                en el artículo 58 de la resolución 415 de 2010</p> 
                                         </td>
                                   </tr>
                                   ";
            }
            

            ResultMessage += @"</ table >
                                        ";

            return ResultMessage;
        }


        public List<ST_RDSCanalProcesoOtorga> GetCanalesProcesos(int IDUserWeb, int SOL_TYPE)
        {
            csData cData = null;
            List<ST_RDSCanalProcesoOtorga> mResult = new List<ST_RDSCanalProcesoOtorga>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            IF OBJECT_ID('dbo.#CANALESTMP', 'U') IS NOT NULL
                              DROP TABLE #CANALESTMP; 

                            SELECT RADIO.V_PLAN_BRO.ID, RADIO.V_PLAN_BRO.CALL_SIGN, RADIO.F_GetEstadoViabilidadCanal(RADIO.V_PLAN_BRO.CALL_SIGN) AS IdEstadoViabilidad, RADIO.V_PLAN_BRO.FREQ, 
		                            RADIO.V_PLAN_BRO.MUNICIPIO, RADIO.V_PLAN_BRO.DEPARTAMENTO, RADIO.V_PLAN_BRO.STATE
                            INTO #CANALESTMP
                            FROM RADIO.V_EXPEDIENTES INNER JOIN
		                            RADIO.SOLICITUDES ON RADIO.V_EXPEDIENTES.SERV_ID = RADIO.SOLICITUDES.SERV_ID INNER JOIN
		                            RADIO.V_PLAN_BRO ON RADIO.V_EXPEDIENTES.CALL_SIGN = RADIO.V_PLAN_BRO.CALL_SIGN
                            WHERE (RADIO.SOLICITUDES.SOL_TYPE_ID = " + SOL_TYPE + @" and RADIO.SOLICITUDES.SOL_STATE_ID!=8 and RADIO.SOLICITUDES.SOL_STATE_ID!=9)
                            GROUP BY RADIO.V_PLAN_BRO.ID, RADIO.V_PLAN_BRO.CALL_SIGN, RADIO.F_GetEstadoViabilidadCanal(RADIO.V_PLAN_BRO.CALL_SIGN), RADIO.V_PLAN_BRO.FREQ, RADIO.V_PLAN_BRO.MUNICIPIO, 
		                            RADIO.V_PLAN_BRO.DEPARTAMENTO, RADIO.V_PLAN_BRO.STATE
                        
                            SELECT ID, #CANALESTMP.CALL_SIGN, IdEstadoViabilidad, FREQ, MUNICIPIO, DEPARTAMENTO, STATE, RADIO.V_INFO_SOLICITUDES_VIABLES.CUS_NAME, 
		                            RADIO.V_INFO_SOLICITUDES_VIABLES.CUS_IDENT, RADIO.V_INFO_SOLICITUDES_VIABLES.RAD_ALFANET, RADIO.V_INFO_SOLICITUDES_VIABLES.SERV_NUMBER, RADIO.V_INFO_SOLICITUDES_VIABLES.SERV_STATUS
                            FROM #CANALESTMP LEFT OUTER JOIN
		                            RADIO.V_INFO_SOLICITUDES_VIABLES ON #CANALESTMP.CALL_SIGN = RADIO.V_INFO_SOLICITUDES_VIABLES.CALL_SIGN

                            Drop Table #CANALESTMP
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSCanalProcesoOtorga
                           {
                               ID_PLAN_BRO = (int)bp.Field<decimal>("ID"),
                               CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                               IdEstadoViabilidad = bp.Field<int>("IdEstadoViabilidad"),
                               EstadoViabilidad = GetDescripciónEstadoViabilidad(bp.Field<int>("IdEstadoViabilidad")),
                               FREQ = bp.Field<double>("FREQ"),
                               MUNICIPIO = bp.Field<string>("MUNICIPIO"),
                               DEPARTAMENTO = bp.Field<string>("DEPARTAMENTO"),
                               STATE = bp.Field<string>("STATE"),
                               CUS_NAME = bp.Field<string>("CUS_NAME"),
                               CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                               RAD_ALFANET = bp.Field<string>("RAD_ALFANET"),
                               SERV_NUMBER = bp.Field<int?>("SERV_NUMBER"),
                               SERV_STATUS = bp.Field<string>("SERV_STATUS"),
                           }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        private string GetDescripciónEstadoViabilidad(int idEstadoViabilidad)
        {
            string result = null;
            switch ((EstadoViabilidad)idEstadoViabilidad)
            {
                case EstadoViabilidad.SinSolicitudes:
                    result = "Sin solicitudes";
                    break;
                case EstadoViabilidad.EnTramite:
                    result = "En trámite";
                    break;
                case EstadoViabilidad.ViabilidadPendiente:
                    result = "Viabilidad pendiente";
                    break;
                case EstadoViabilidad.Asignado:
                    result = "Asignado";
                    break;
                case EstadoViabilidad.Desierto:
                    result = "Desierto";
                    break;
            }

            return result;
        }

        public ST_InfoSolicitudesCanal GetSolicitudesCanal(int IDUserWeb, int USR_GROUP, int USR_ROLE, string CALL_SIGN)
         {
            csData cData = null;
            ST_InfoSolicitudesCanal mResult = new ST_InfoSolicitudesCanal();

            string Parametros = "IDUserWeb='" + IDUserWeb + "',USR_GROUP='" + USR_GROUP + "'" + "',USR_ROLE='" + USR_ROLE + "',SOL_ENDED='" + 0 + "'";

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        Declare @USR_GROUP int=" + USR_GROUP + @"
                        Select SOL.SOL_UID, SOL.SOL_NUMBER, SOL.SERV_ID, SERV.SERV_NUMBER, SERV.SERV_NAME, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE, SOL_CREATOR,
                        SOL.Clasificacion, SOL.NivelCubrimiento, SOL.Tecnologia, SOL.NombreComunidad, SOL.IdLugarComunidad, SOL.DireccionComunidad, SOL.CiudadComunidad, SOL.DepartamentoComunidad, SOL.CodeLugarComunidad, SOL.Territorio,
                        SOL.ResolucionMinInterior, SOL.GrupoEtnico, SOL.NombrePuebloComunidad, SOL.NombreResguardo, SOL.NombreCabildo, SOL.NombreKumpania, SOL.NombreConsejo, SOL.NombreOrganizacionBase, SOL.Evaluaciones, SOL.Puntaje,
                        SERV.CUS_ID, SOL.CUS_IDENT, SERV.CUS_NAME, SERV.CUS_EMAIL, SERV.CUS_BRANCH, SERV_STATUS,
                        SOL.SOL_TYPE_ID, TSOL.SOL_TYPE_NAME, TSOL.SOL_TYPE_CLASS, TSOL.SOL_IS_OTORGA, SOL.SOL_STATE_ID, ESOL.SOL_STATE_NAME,
                        SERV.LIC_NUMBER, SERV.STATION_CLASS, SERV.STATE, SERV.DEPTO, SERV.CITY, SERV.CODE_AREA_MUNICIPIO, SERV.POWER, SERV.ASL, SERV.FREQUENCY, SERV.CALL_SIGN, SERV.STOP_DATE, SERV.MODULATION, SERV.CUST_TXT3, 
                        FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE,
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT,
                        CONT_ID, CONT_NAME, CONT_ROLE, CONT_NUMBER, CONT_EMAIL,
                        USR_ROLE1_REASIGN, USR_ROLE2_REASIGN, USR_ROLE4_REASIGN, USR_ROLE8_REASIGN, USR_ROLE16_REASIGN,
                        USR_ROLE1_REASIGN_COMMENT, USR_ROLE2_REASIGN_COMMENT, USR_ROLE4_REASIGN_COMMENT, USR_ROLE8_REASIGN_COMMENT, USR_ROLE16_REASIGN_COMMENT,
                        RES.RES_NAME, RES.RES_IS_CREATED, RES.RES_CREATED_DATE, RES.RES_MODIFIED_DATE, RES.RES_TRACKING_COUNT,
                        RESVIA.RES_NAME AS RESVIA_NAME, RESVIA.RES_IS_CREATED AS RESVIA_IS_CREATED, RESVIA.RES_CREATED_DATE AS RESVIA_CREATED_DATE, RESVIA.RES_MODIFIED_DATE AS RESVIA_MODIFIED_DATE, RESVIA.RES_TRACKING_COUNT AS RESVIA_TRACKING_COUNT,
                        RADIO.F_GetAlarmState(SOL.SOL_LAST_STATE_DATE, ALM.ALARM_PRE_EXPIRE, ALM.ALARM_EXPIRE, SOL.SOL_ENDED) SOL_ALARM_STATE,
                        DateAdd(day, ALM_REQUIRED.ALARM_EXPIRE + SOL_REQUIRED_POSTPONE ,SOL_REQUIRED_DATE) SOL_REQUIRED_DEADLINE_DATE,
                        SOL.SOL_ENDED, SOL.AZ_DIRECTORIO
                        Into #SOLICITUDES
                        From RADIO.SOLICITUDES SOL
                        Join RADIO.SOL_USERS USO On SOL.SOL_UID=USO.SOL_UID And USO.USR_GROUP=@USR_GROUP 
                        Join RADIO.TIPOS_SOL TSOL on SOL.SOL_TYPE_ID = TSOL.SOL_TYPE_ID 
                        Join RADIO.ESTADOS_SOL ESOL on  SOL.SOL_STATE_ID = ESOL.SOL_STATE_ID 
                        Join RADIO.V_EXPEDIENTES SERV On SOL.SERV_ID=SERV.SERV_ID
                        Join RADIO.RESOLUCIONES RES On SOL.SOL_UID=RES.SOL_UID
                        Join RADIO.RESOLUCIONES_VIABILIDAD RESVIA On SOL.SOL_UID=RESVIA.SOL_UID
                        Join RADIO.ALARMS ALM On SOL.STEP_CURRENT=ALM.ALARM_STEP
                        Join RADIO.ALARMS ALM_REQUIRED On ALM_REQUIRED.ALARM_STEP=4
                        Where 
                            (SERV.CALL_SIGN = '" + CALL_SIGN + @"')
                        Order By 2 Desc

	                    Select * From #SOLICITUDES

                        --Usuarios--
                        SELECT SOL_UID, USR_GROUP, 
                        USR_ROLE1_ID, USR_ROLE1_NAME, USR_ROLE1_EMAIL, USR_ROLE2_ID, USR_ROLE2_NAME, USR_ROLE2_EMAIL, USR_ROLE4_ID, USR_ROLE4_NAME, USR_ROLE4_EMAIL, USR_ROLE8_ID, USR_ROLE8_NAME, USR_ROLE8_EMAIL, USR_ROLE16_ID, USR_ROLE16_NAME, USR_ROLE16_EMAIL, USR_ROLE32_ID, USR_ROLE32_NAME, USR_ROLE32_EMAIL
                        FROM RADIO.V_USUARIOS
                        WHERE SOL_UID  In (Select SOL_UID From #SOLICITUDES)

                        --Contactos--
                        SELECT SERV_ID, SERV_NUMBER, CONT_ID, CONT_NAME, CONT_ROLE, CONT_TEL, CONT_NUMBER
                        FROM RADIO.V_CONTACTOS
                        WHERE SERV_ID In (Select SERV_ID From #SOLICITUDES)

                        --Anexos--
                        SELECT ANX.SOL_UID, ANX.ANX_UID, ANX.ANX_NAME, ANX.ANX_NUMBER, ANX.ANX_STATE_ID, TA.TIPO_ANX_SOL ANX_TYPE_NAME, ANX.ANX_TYPE_ID, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        ANX.ANX_ROLE1_COMMENT, ANX.ANX_ROLE2_COMMENT, ANX.ANX_ROLE4_COMMENT, ANX.ANX_ROLE8_COMMENT
                        FROM RADIO.ANEXOS as ANX 
                        JOIN RADIO.TIPO_ANEXO_SOL as TA ON ANX.ANX_TYPE_ID = TA.ID_ANX_SOL 
                        WHERE ANX.SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                        ORDER BY ANX.ANX_NUMBER

                        --Documents--
                        SELECT DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, Convert(int, ROW_NUMBER() Over (Order By DOC_DATE)) DOC_ORDER
                        FROM RADIO.DOCUMENTS
                        WHERE SOL_UID  In (Select SOL_UID From #SOLICITUDES)
                        ORDER BY DOC_DATE

                        --Comunicado Administrativo y financiero--
                        Select SOL_UID, COM_UID, COM_NAME, COM_DATE, COM_CLASS, COM_TYPE
		                FROM RADIO.COMUNICADOS 
		                WHERE COM_CLASS In (1,2) And SOL_UID In (Select SOL_UID From #SOLICITUDES)
                        
                        --Analisis Tecnico--
                        Select SOL_UID, TECH_ROLE1_STATE_ID, TECH_ROLE2_STATE_ID, TECH_ROLE4_STATE_ID, TECH_ROLE8_STATE_ID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLEX_CHANGED,
                        TECH_ROLE1_COMMENT, TECH_ROLE2_COMMENT, TECH_ROLE4_COMMENT, TECH_ROLE8_COMMENT, TECH_ROLE_MINTIC_COMMENT, TECH_PTNRS_UPDATED, TECH_CREATED_DATE, TECH_MODIFIED_DATE
		                FROM RADIO.TECH_ANALISIS 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Documentos técnicos--
                        SELECT TECH_UID, SOL_UID, TECH_TYPE, TECH_NAME, TECH_CONTENT_TYPE, TECH_CREATED_DATE, TECH_MODIFIED_DATE, TECH_CHANGED
                        FROM RADIO.TECH_DOCUMENTS
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Analisis Resolucion--
                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

                        --Analisis Resolucion Viabilidad--
                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD 
		                WHERE SOL_UID In (Select SOL_UID From #SOLICITUDES)

	                    Drop Table #SOLICITUDES

                        SELECT TOP (1) RADIO.CONFIGURACION_PROCESO.PuntajeMinimoViabilidadAutomatica
                        FROM RADIO.CONFIGURACION_PROCESO INNER JOIN
                             RADIO.SOLICITUDES ON RADIO.CONFIGURACION_PROCESO.SOL_TYPE = RADIO.SOLICITUDES.SOL_TYPE_ID INNER JOIN
                             RADIO.V_EXPEDIENTES ON RADIO.SOLICITUDES.SERV_NUMBER = RADIO.V_EXPEDIENTES.SERV_NUMBER
                        GROUP BY RADIO.V_EXPEDIENTES.CALL_SIGN, RADIO.CONFIGURACION_PROCESO.PuntajeMinimoViabilidadAutomatica
                        HAVING (RADIO.V_EXPEDIENTES.CALL_SIGN = '" + CALL_SIGN + @"')
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);


                mResult.Solicitudes = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                           select new ST_RDSSolicitud
                           {
                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                               SOL_NUMBER = bp.Field<int>("SOL_NUMBER"),
                               Radicado = bp.Field<string>("RAD_ALFANET"),
                               SOL_CREATED_DATE = bp.Field<DateTime?>("SOL_CREATED_DATE").HasValue ? bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyyMMdd") : "",
                               SOL_CREATED_DATE_YEAR = bp.Field<DateTime>("SOL_CREATED_DATE").Date.ToString("yyyy"),
                               SOL_MODIFIED_DATE = bp.Field<DateTime?>("SOL_MODIFIED_DATE").HasValue ? bp.Field<DateTime>("SOL_MODIFIED_DATE").Date.ToString("yyyyMMdd") : "",
                               SOL_LAST_STATE_DATE = bp.Field<DateTime?>("SOL_LAST_STATE_DATE").HasValue ? bp.Field<DateTime>("SOL_LAST_STATE_DATE").Date.ToString("yyyyMMdd") : "",
                               SOL_REQUIRED_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DATE").Date.ToString("yyyyMMdd") : "",
                               SOL_REQUIRED_DEADLINE_DATE = bp.Field<DateTime?>("SOL_REQUIRED_DEADLINE_DATE").HasValue ? bp.Field<DateTime>("SOL_REQUIRED_DEADLINE_DATE").Date.ToString("yyyyMMdd") : "",
                               SOL_REQUIRED_POSTPONE = bp.Field<int>("SOL_REQUIRED_POSTPONE"),
                               SOL_STATE_ID = bp.Field<int>("SOL_STATE_ID"),
                               SOL_STATE_NAME = bp.Field<string>("SOL_STATE_NAME"),
                               SOL_TYPE_ID = bp.Field<int>("SOL_TYPE_ID"),
                               SOL_TYPE_NAME = bp.Field<string>("SOL_TYPE_NAME"),
                               SOL_TYPE_CLASS = bp.Field<int>("SOL_TYPE_CLASS"),
                               SOL_IS_OTORGA = bp.Field<bool>("SOL_IS_OTORGA"),
                               SOL_ENDED = bp.Field<int>("SOL_ENDED"),
                               SOL_CREATOR = bp.Field<int>("SOL_CREATOR"),
                               SOL_ALARM_STATE = bp.Field<int>("SOL_ALARM_STATE"),
                               Clasificacion = bp.Field<string>("Clasificacion"),
                               NivelCubrimiento = bp.Field<string>("NivelCubrimiento"),
                               Tecnologia = bp.Field<string>("Tecnologia"),
                               Evaluaciones = bp.Field<string>("Evaluaciones"),
                               Puntaje = bp.Field<decimal?>("Puntaje"),
                               AZ_DIRECTORIO = bp.Field<string>("AZ_DIRECTORIO"),
                               DatosComunidad = new ST_RDSDatosComunidad
                               {
                                   NombreComunidad = bp.Field<string>("NombreComunidad"),
                                   IdLugarComunidad = bp.Field<decimal?>("IdLugarComunidad"),
                                   DireccionComunidad = bp.Field<string>("DireccionComunidad"),
                                   CiudadComunidad = bp.Field<string>("CiudadComunidad"),
                                   DepartamentoComunidad = bp.Field<string>("DepartamentoComunidad"),
                                   CodeLugarComunidad = bp.Field<string>("CodeLugarComunidad"),
                                   Territorio = bp.Field<string>("Territorio"),
                                   ResolucionMinInterior = bp.Field<string>("ResolucionMinInterior"),
                                   GrupoEtnico = bp.Field<string>("GrupoEtnico"),
                                   NombrePuebloComunidad = bp.Field<string>("NombrePuebloComunidad"),
                                   NombreResguardo = bp.Field<string>("NombreResguardo"),
                                   NombreCabildo = bp.Field<string>("NombreCabildo"),
                                   NombreKumpania = bp.Field<string>("NombreKumpania"),
                                   NombreConsejo = bp.Field<string>("NombreConsejo"),
                                   NombreOrganizacionBase = bp.Field<string>("NombreOrganizacionBase"),
                               },
                               Resolucion = new ST_RDSResolucion
                               {
                                   SOL_UID = bp.Field<Guid>("SOL_UID"),
                                   RES_NAME = bp.Field<string>("RES_NAME"),
                                   RES_IS_CREATED = bp.Field<bool>("RES_IS_CREATED"),
                                   RES_CREATED_DATE = bp.Field<bool>("RES_IS_CREATED") ? bp.Field<DateTime>("RES_CREATED_DATE").ToString("yyyyMMdd") : "",
                                   RES_MODIFIED_DATE = bp.Field<bool>("RES_IS_CREATED") ? bp.Field<DateTime>("RES_MODIFIED_DATE").ToString("yyyyMMdd") : "",
                                   RES_TRACKING_COUNT = bp.Field<int>("RES_TRACKING_COUNT")
                               },
                               Viabilidad = new ST_RDSResolucion
                               {
                                   SOL_UID = bp.Field<Guid>("SOL_UID"),
                                   RES_NAME = bp.Field<string>("RESVIA_NAME"),
                                   RES_IS_CREATED = bp.Field<bool>("RESVIA_IS_CREATED"),
                                   RES_CREATED_DATE = bp.Field<bool>("RESVIA_IS_CREATED") ? bp.Field<DateTime>("RESVIA_CREATED_DATE").ToString("yyyyMMdd") : "",
                                   RES_MODIFIED_DATE = bp.Field<bool>("RESVIA_IS_CREATED") ? bp.Field<DateTime>("RESVIA_MODIFIED_DATE").ToString("yyyyMMdd") : "",
                                   RES_TRACKING_COUNT = bp.Field<int>("RESVIA_TRACKING_COUNT")
                               },
                               CURRENT = new ST_RDSCurrent
                               {
                                   GROUP = bp.Field<int>("GROUP_CURRENT"),
                                   ROLE = bp.Field<int>("ROLE_CURRENT"),
                                   STEP = bp.Field<int>("STEP_CURRENT")
                               },
                               Contacto = new ST_RDSContacto
                               {
                                   CONT_ID = bp.Field<int>("CONT_ID"),
                                   CONT_NAME = bp.Field<string>("CONT_NAME"),
                                   CONT_ROLE = bp.Field<int>("CONT_ROLE"),
                                   CONT_NUMBER = bp.Field<string>("CONT_NUMBER"),
                                   CONT_TITLE = GetContactTitle(bp.Field<int>("CONT_ROLE")),
                                   CONT_EMAIL = bp.Field<string>("CONT_EMAIL")
                               },
                               USR_ADMIN = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 1)
                                            select new ST_RDSRoles
                                            {
                                                USR_ROLE1 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                                },
                                                USR_ROLE2 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                                },
                                                USR_ROLE4 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                                },
                                                USR_ROLE8 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                                },
                                                USR_ROLE16 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE16_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE16_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE16_EMAIL")
                                                },
                                                USR_ROLE32 = new ST_RDSUsuario
                                                {
                                                    USR_ID = bpu.Field<int>("USR_ROLE32_ID"),
                                                    USR_NAME = bpu.Field<string>("USR_ROLE32_NAME"),
                                                    USR_EMAIL = bpu.Field<string>("USR_ROLE32_EMAIL")
                                                },
                                            }).FirstOrDefault(),
                               USR_TECH = (from bpu in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("USR_GROUP") == 2)
                                           select new ST_RDSRoles
                                           {
                                               USR_ROLE1 = new ST_RDSUsuario
                                               {
                                                   USR_ID = bpu.Field<int>("USR_ROLE1_ID"),
                                                   USR_NAME = bpu.Field<string>("USR_ROLE1_NAME"),
                                                   USR_EMAIL = bpu.Field<string>("USR_ROLE1_EMAIL")
                                               },
                                               USR_ROLE2 = new ST_RDSUsuario
                                               {
                                                   USR_ID = bpu.Field<int>("USR_ROLE2_ID"),
                                                   USR_NAME = bpu.Field<string>("USR_ROLE2_NAME"),
                                                   USR_EMAIL = bpu.Field<string>("USR_ROLE2_EMAIL")
                                               },
                                               USR_ROLE4 = new ST_RDSUsuario
                                               {
                                                   USR_ID = bpu.Field<int>("USR_ROLE4_ID"),
                                                   USR_NAME = bpu.Field<string>("USR_ROLE4_NAME"),
                                                   USR_EMAIL = bpu.Field<string>("USR_ROLE4_EMAIL")
                                               },
                                               USR_ROLE8 = new ST_RDSUsuario
                                               {
                                                   USR_ID = bpu.Field<int>("USR_ROLE8_ID"),
                                                   USR_NAME = bpu.Field<string>("USR_ROLE8_NAME"),
                                                   USR_EMAIL = bpu.Field<string>("USR_ROLE8_EMAIL")
                                               },
                                           }).FirstOrDefault(),
                               Reasign = new ST_RDSReasign
                               {
                                   USR_ROLE1_REASIGN = bp.Field<bool>("USR_ROLE1_REASIGN"),
                                   USR_ROLE2_REASIGN = bp.Field<bool>("USR_ROLE2_REASIGN"),
                                   USR_ROLE4_REASIGN = bp.Field<bool>("USR_ROLE4_REASIGN"),
                                   USR_ROLE8_REASIGN = bp.Field<bool>("USR_ROLE8_REASIGN"),
                                   USR_ROLE16_REASIGN = bp.Field<bool>("USR_ROLE16_REASIGN"),
                                   USR_ROLE1_REASIGN_COMMENT = bp.Field<string>("USR_ROLE1_REASIGN_COMMENT"),
                                   USR_ROLE2_REASIGN_COMMENT = bp.Field<string>("USR_ROLE2_REASIGN_COMMENT"),
                                   USR_ROLE4_REASIGN_COMMENT = bp.Field<string>("USR_ROLE4_REASIGN_COMMENT"),
                                   USR_ROLE8_REASIGN_COMMENT = bp.Field<string>("USR_ROLE8_REASIGN_COMMENT"),
                                   USR_ROLE16_REASIGN_COMMENT = bp.Field<string>("USR_ROLE16_REASIGN_COMMENT")
                               },
                               Financiero = new ST_RDSAnalisisFinanciero
                               {
                                   FIN_SEVEN_ALDIA = bp.Field<bool?>("FIN_SEVEN_ALDIA"),
                                   FIN_ESTADO_CUENTA_NUMBER = bp.Field<string>("FIN_ESTADO_CUENTA_NUMBER"),
                                   FIN_ESTADO_CUENTA_DATE = bp.Field<DateTime?>("FIN_ESTADO_CUENTA_DATE").HasValue ? bp.Field<DateTime>("FIN_ESTADO_CUENTA_DATE").Date.ToString("yyyyMMdd") : "",
                                   FIN_REGISTRO_ALFA_NUMBER = bp.Field<string>("FIN_REGISTRO_ALFA_NUMBER"),
                                   FIN_REGISTRO_ALFA_DATE = bp.Field<DateTime?>("FIN_REGISTRO_ALFA_DATE").HasValue ? bp.Field<DateTime>("FIN_REGISTRO_ALFA_DATE").Date.ToString("yyyyMMdd") : "",
                               },
                               Expediente = new ST_RDSExpediente
                               {
                                   SERV_ID = bp.Field<int>("SERV_ID"),
                                   SERV_NUMBER = bp.Field<int>("SERV_NUMBER"),
                                   SERV_STATUS = bp.Field<string>("SERV_STATUS"),
                                   STATION_CLASS = bp.Field<string>("STATION_CLASS"),
                                   STATE = bp.Field<string>("STATE"),
                                   DEPTO = bp.Field<string>("DEPTO") != null ? bp.Field<string>("DEPTO").ToUpperInvariant() : null,
                                   CITY = bp.Field<string>("CITY") != null? bp.Field<string>("CITY").ToUpperInvariant() : null,
                                   CODE_AREA_MUNICIPIO = bp.Field<int>("CODE_AREA_MUNICIPIO"),
                                   POWER = bp.Field<double?>("POWER"),
                                   ASL = bp.Field<double?>("ASL"),
                                   FREQUENCY = bp.Field<double?>("FREQUENCY"),
                                   CALL_SIGN = bp.Field<string>("CALL_SIGN"),
                                   SERV_NAME = bp.Field<string>("SERV_NAME").ToUpperInvariant(),
                                   MODULATION = bp.Field<string>("MODULATION"),
                                   STOP_DATE = bp.Field<DateTime?>("STOP_DATE").HasValue ? bp.Field<DateTime>("STOP_DATE").Date.ToString("yyyyMMdd") : "",
                                   CUST_TXT3 = bp.Field<string>("CUST_TXT3"),
                                   Operador = new ST_RDSOperador
                                   {
                                       CUS_ID = bp.Field<int>("CUS_ID"),
                                       CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                       CUS_NAME = bp.Field<string>("CUS_NAME"),
                                       CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                       CUS_BRANCH = bp.Field<int>("CUS_BRANCH")
                                   },
                                   Contactos = (from bpc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("SERV_ID") == bp.Field<int>("SERV_ID"))
                                                select new ST_RDSContacto
                                                {
                                                    CONT_ID = bpc.Field<int>("CONT_ID"),
                                                    CONT_NUMBER = bpc.Field<string>("CONT_NUMBER"),
                                                    CONT_NAME = bpc.Field<string>("CONT_NAME"),
                                                    CONT_TEL = bpc.Field<string>("CONT_TEL"),
                                                    CONT_ROLE = bpc.Field<int>("CONT_ROLE"),
                                                    CONT_TITLE = GetContactTitle(bpc.Field<int>("CONT_ROLE")),
                                                }).ToList()
                               },
                               Anexos = (from bpa in dtSet.Tables[3].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                         select new ST_RDSAnexos
                                         {
                                             ANX_UID = bpa.Field<Guid>("ANX_UID"),
                                             ANX_NUMBER = bpa.Field<int>("ANX_NUMBER"),
                                             ANX_STATE_ID = bpa.Field<int>("ANX_STATE_ID"),
                                             ANX_TYPE_ID = bpa.Field<int>("ANX_TYPE_ID"),
                                             ANX_TYPE_NAME = bpa.Field<string>("ANX_TYPE_NAME"),
                                             ANX_NAME = bpa.Field<string>("ANX_NAME"),
                                             ANX_ROLE1_STATE_ID = bpa.Field<int>("ANX_ROLE1_STATE_ID"),
                                             ANX_ROLE2_STATE_ID = bpa.Field<int>("ANX_ROLE2_STATE_ID"),
                                             ANX_ROLE4_STATE_ID = bpa.Field<int>("ANX_ROLE4_STATE_ID"),
                                             ANX_ROLE8_STATE_ID = bpa.Field<int>("ANX_ROLE8_STATE_ID"),
                                             ANX_ROLEX_CHANGED = bpa.Field<bool>("ANX_ROLEX_CHANGED"),
                                             ANX_ROLE1_COMMENT = bpa.Field<string>("ANX_ROLE1_COMMENT"),
                                             ANX_ROLE2_COMMENT = bpa.Field<string>("ANX_ROLE2_COMMENT"),
                                             ANX_ROLE4_COMMENT = bpa.Field<string>("ANX_ROLE4_COMMENT"),
                                             ANX_ROLE8_COMMENT = bpa.Field<string>("ANX_ROLE8_COMMENT"),

                                             ANX_COMMENT = "",
                                             ANX_OPENED = 0
                                         }).ToList(),
                               Documents = (from bpd in dtSet.Tables[4].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                            select new ST_RDSDocument
                                            {
                                                DOC_UID = bpd.Field<Guid>("DOC_UID"),
                                                DOC_ORDER = bpd.Field<int>("DOC_ORDER"),
                                                DOC_CLASS = bpd.Field<int>("DOC_CLASS"),
                                                DOC_TYPE = bpd.Field<int>("DOC_TYPE"),
                                                DOC_ORIGIN = bpd.Field<int>("DOC_ORIGIN"),
                                                DOC_DEST = bpd.Field<int>("DOC_DEST"),
                                                DOC_NAME = bpd.Field<string>("DOC_NAME"),
                                                DOC_NUMBER = bpd.Field<string>("DOC_NUMBER"),
                                                DOC_DATE = bpd.Field<DateTime>("DOC_DATE").Date.ToString("yyyyMMdd")
                                            }).ToList(),
                               ComunicadoAdministrativo = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 1)
                                                           select new ST_RDSComunicado
                                                           {
                                                               COM_UID = bpr.Field<Guid>("COM_UID"),
                                                               COM_NAME = bpr.Field<string>("COM_NAME"),
                                                               COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                               COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                               COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                           }).FirstOrDefault(),
                               ComunicadoTecnico = (from bpr in dtSet.Tables[5].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID") && x.Field<int>("COM_CLASS") == 2)
                                                    select new ST_RDSComunicado
                                                    {
                                                        COM_UID = bpr.Field<Guid>("COM_UID"),
                                                        COM_NAME = bpr.Field<string>("COM_NAME"),
                                                        COM_DATE = bpr.Field<DateTime>("COM_DATE").Date.ToString("yyyyMMdd"),
                                                        COM_CLASS = bpr.Field<int>("COM_CLASS"),
                                                        COM_TYPE = bpr.Field<int>("COM_TYPE")
                                                    }).FirstOrDefault(),
                               AnalisisTecnico = (from bpt in dtSet.Tables[6].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                  select new ST_RDSTechAnalisis
                                                  {
                                                      SOL_UID = bpt.Field<Guid>("SOL_UID"),
                                                      TECH_STATE_ID = 0,
                                                      TECH_ROLE1_STATE_ID = bpt.Field<int>("TECH_ROLE1_STATE_ID"),
                                                      TECH_ROLE2_STATE_ID = bpt.Field<int>("TECH_ROLE2_STATE_ID"),
                                                      TECH_ROLE4_STATE_ID = bpt.Field<int>("TECH_ROLE4_STATE_ID"),
                                                      TECH_ROLE8_STATE_ID = bpt.Field<int>("TECH_ROLE8_STATE_ID"),
                                                      TECH_ROLE_MINTIC_STATE_ID = bpt.Field<int>("TECH_ROLE_MINTIC_STATE_ID"),
                                                      TECH_ROLEX_CHANGED = bpt.Field<bool>("TECH_ROLEX_CHANGED"),
                                                      TECH_ROLE1_COMMENT = bpt.Field<string>("TECH_ROLE1_COMMENT"),
                                                      TECH_ROLE2_COMMENT = bpt.Field<string>("TECH_ROLE2_COMMENT"),
                                                      TECH_ROLE4_COMMENT = bpt.Field<string>("TECH_ROLE4_COMMENT"),
                                                      TECH_ROLE8_COMMENT = bpt.Field<string>("TECH_ROLE8_COMMENT"),
                                                      TECH_ROLE_MINTIC_COMMENT = bpt.Field<string>("TECH_ROLE_MINTIC_COMMENT"),
                                                      TECH_PTNRS_UPDATED = bpt.Field<bool>("TECH_PTNRS_UPDATED"),
                                                      TECH_CREATED_DATE = bpt.Field<DateTime>("TECH_CREATED_DATE").Date.ToString("yyyyMMdd"),
                                                      TECH_MODIFIED_DATE = bpt.Field<DateTime>("TECH_MODIFIED_DATE").Date.ToString("yyyyMMdd")
                                                  }).FirstOrDefault(),
                               TechDocs = (from bptd in dtSet.Tables[7].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                           select new ST_RDSTechDoc
                                           {
                                               TECH_UID = bptd.Field<Guid>("TECH_UID"),
                                               TECH_TYPE = bptd.Field<int>("TECH_TYPE"),
                                               TECH_CONTENT_TYPE = bptd.Field<string>("TECH_CONTENT_TYPE"),
                                               TECH_NAME = bptd.Field<string>("TECH_NAME"),
                                               TECH_CHANGED = bptd.Field<bool>("TECH_CHANGED")
                                           }).ToList().OrderBy(x => x.TECH_TYPE).ToList(),
                               AnalisisResolucion = (from bpr in dtSet.Tables[8].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                     select new ST_RDSResAnalisis
                                                     {
                                                         SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                         RES_STATE_ID = 0,
                                                         RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                         RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                         RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                         RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                         RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                         RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                         RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                         RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                         RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                         RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                         RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                         RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                         RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT")
                                                     }).FirstOrDefault(),
                               AnalisisViabilidad = (from bpr in dtSet.Tables[9].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("SOL_UID") == bp.Field<Guid>("SOL_UID"))
                                                     select new ST_RDSResAnalisis
                                                     {
                                                         SOL_UID = bpr.Field<Guid>("SOL_UID"),
                                                         RES_STATE_ID = 0,
                                                         RES_ROLE1_STATE_ID = bpr.Field<int>("RES_ROLE1_STATE_ID"),
                                                         RES_ROLE2_STATE_ID = bpr.Field<int>("RES_ROLE2_STATE_ID"),
                                                         RES_ROLE4_STATE_ID = bpr.Field<int>("RES_ROLE4_STATE_ID"),
                                                         RES_ROLE8_STATE_ID = bpr.Field<int>("RES_ROLE8_STATE_ID"),
                                                         RES_ROLE16_STATE_ID = bpr.Field<int>("RES_ROLE16_STATE_ID"),
                                                         RES_ROLE32_STATE_ID = bpr.Field<int>("RES_ROLE32_STATE_ID"),
                                                         RES_ROLEX_CHANGED = bpr.Field<bool>("RES_ROLEX_CHANGED"),
                                                         RES_ROLE1_COMMENT = bpr.Field<string>("RES_ROLE1_COMMENT"),
                                                         RES_ROLE2_COMMENT = bpr.Field<string>("RES_ROLE2_COMMENT"),
                                                         RES_ROLE4_COMMENT = bpr.Field<string>("RES_ROLE4_COMMENT"),
                                                         RES_ROLE8_COMMENT = bpr.Field<string>("RES_ROLE8_COMMENT"),
                                                         RES_ROLE16_COMMENT = bpr.Field<string>("RES_ROLE16_COMMENT"),
                                                         RES_ROLE32_COMMENT = bpr.Field<string>("RES_ROLE32_COMMENT")
                                                     }).FirstOrDefault(),
                           }).ToList();

                mResult.PuntajeMinimoViabilidadAutomatica = dtSet.Tables[10].Rows[0].Field<decimal>("PuntajeMinimoViabilidadAutomatica");

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message, "Parametros=" + Parametros);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }
        
        public ActionInfo OtorgaGuardarTramiteAdministrativo(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado)
        {
            string registroNumber = "";
            List<ST_RDSUsuario> AsignacionesGroup2 = new List<ST_RDSUsuario>();
            ST_RDSCurrent CURRENT = new ST_RDSCurrent();
            ST_Helper Helper = new ST_Helper();
            AZTools AZTools = new AZTools();
            ST_RDSComunicado Comunicado = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Administrativo);
            ROLE248_STATE STATE248 = OtorgaGetRole248State(ref Solicitud, ROLE);
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            string sSQL = "";
            var sLog = "IDUserWeb=" + IDUserWeb + ", ROLE=" + ROLE + ", Solicitud=" + Serializer.Serialize(Solicitud) + ", bGenerarComunicado=" + bGenerarComunicado;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }


                ValidateToken(ref cData);

                sSQL = @"
                        Select SOL_UID, SOL_TYPE_ID, SOL_STATE_ID, CONT_EMAIL, SOL_NUMBER, CAMPOS_SOL, RAD_ALFANET, SOL_CREATED_DATE, SOL_MODIFIED_DATE, SOL_LAST_STATE_DATE, SOL_REQUIRED_DATE, SOL_REQUIRED_POSTPONE,
                        FIN_SEVEN_ALDIA, FIN_ESTADO_CUENTA_NUMBER, FIN_ESTADO_CUENTA_DATE, FIN_REGISTRO_ALFA_NUMBER, FIN_REGISTRO_ALFA_DATE, 
                        GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT, SOL_ENDED, AZ_DIRECTORIO
                        From RADIO.SOLICITUDES Where SOL_UID='" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT ANX_UID, ANX_STATE_ID, ANX_COMMENT, 
                        ANX_ROLE1_STATE_ID, ANX_ROLE2_STATE_ID, ANX_ROLE4_STATE_ID, ANX_ROLE8_STATE_ID, ANX_ROLEX_CHANGED,
                        ANX_ROLE1_COMMENT, ANX_ROLE2_COMMENT, ANX_ROLE4_COMMENT, ANX_ROLE8_COMMENT
                        FROM RADIO.ANEXOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';
                
                        SELECT SOL_UID, COM_UID, COM_CLASS, COM_TYPE, COM_NAME, COM_DATE, COM_FILE
                        FROM RADIO.COMUNICADOS
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"' And COM_CLASS In (1,3);

                        SELECT Top 0 DOC_UID, SOL_UID, DOC_CLASS, DOC_TYPE, DOC_ORIGIN, DOC_DEST, DOC_NUMBER, DOC_NAME, DOC_DATE, DOC_FILE
                        FROM RADIO.DOCUMENTS;

                        SELECT Top 0 CON_ID, SOL_UID, USR_ID, USR_GROUP, USR_ROLE
                        FROM RADIO.CONTEOS;

                        SELECT SOL_UID, USR_GROUP, USR_ROLE1_ID, USR_ROLE2_ID, USR_ROLE4_ID, USR_ROLE8_ID 
                        FROM RADIO.SOL_USERS 
                        WHERE SOL_UID='" + Solicitud.SOL_UID.ToString() + @"'
                        Order By USR_GROUP;

                        Select SOL_UID, TECH_ROLE_MINTIC_STATE_ID, TECH_ROLE_MINTIC_COMMENT, TECH_ROLEX_CHANGED
		                FROM RADIO.TECH_ANALISIS 
                        WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS 
		                WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        Select SOL_UID, RES_ROLE1_STATE_ID, RES_ROLE2_STATE_ID, RES_ROLE4_STATE_ID, RES_ROLE8_STATE_ID, RES_ROLE16_STATE_ID, RES_ROLE32_STATE_ID, RES_ROLEX_CHANGED,
                        RES_ROLE1_COMMENT, RES_ROLE2_COMMENT, RES_ROLE4_COMMENT, RES_ROLE8_COMMENT, RES_ROLE16_COMMENT, RES_ROLE32_COMMENT
		                FROM RADIO.RESOLUCION_ANALISIS_VIABILIDAD
		                WHERE SOL_UID = '" + Solicitud.SOL_UID.ToString() + @"';

                        SELECT SERV_ID, CALL_SIGN
                        FROM RADIO.EXPEDIENTES_TEMPORALES
                        WHERE SERV_ID = " + Solicitud.Expediente.SERV_ID + @";

                        SELECT TOP 0 * FROM RADIO.TECH_ANEXOS;
                        

                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitud = dtSet.Tables[0];
                DataTable DTAnexo = dtSet.Tables[1];
                DataTable DTComunicado = dtSet.Tables[2];
                DataTable DTDocuments = dtSet.Tables[3];
                DataTable DTConteos = dtSet.Tables[4];
                DataTable DTSolUsers = dtSet.Tables[5];
                DataTable DTTech = dtSet.Tables[6];
                DataTable DTResAnalisis = dtSet.Tables[7];
                DataTable DTResAnalisisViabilidad = dtSet.Tables[8];
                DataTable DTExpedientesTemporales = dtSet.Tables[9];
                DataTable DTTechAnexos = dtSet.Tables[10];


                DataRow DRS = DTSolicitud.Rows[0];

                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;

                switch ((STEPS)Solicitud.CURRENT.STEP)
                {
                    case STEPS.Administrativo:
                        DRS["FIN_SEVEN_ALDIA"] = Solicitud.Financiero.FIN_SEVEN_ALDIA != null ? Solicitud.Financiero.FIN_SEVEN_ALDIA : (object)DBNull.Value;
                        DRS["FIN_ESTADO_CUENTA_NUMBER"] = Solicitud.Financiero.FIN_ESTADO_CUENTA_NUMBER;
                        DRS["FIN_ESTADO_CUENTA_DATE"] = GetDateNulable(Solicitud.Financiero.FIN_ESTADO_CUENTA_DATE);
                        DRS["FIN_REGISTRO_ALFA_NUMBER"] = Solicitud.Financiero.FIN_REGISTRO_ALFA_NUMBER;
                        DRS["FIN_REGISTRO_ALFA_DATE"] = GetDateNulable(Solicitud.Financiero.FIN_REGISTRO_ALFA_DATE);
                        foreach (DataRow DRA in DTAnexo.Rows)
                        {
                            var Item = Solicitud.Anexos.Find(bp => bp.ANX_UID == (Guid)DRA["ANX_UID"]);
                            if (Item != null)
                            {
                                switch ((ROLES)ROLE)
                                {
                                    case ROLES.Funcionario:
                                        DRA["ANX_ROLE1_STATE_ID"] = Item.ANX_ROLE1_STATE_ID;
                                        DRA["ANX_ROLE1_COMMENT"] = Item.ANX_ROLE1_COMMENT;
                                        break;

                                    case ROLES.Coordinador:
                                        DRA["ANX_ROLE4_STATE_ID"] = Item.ANX_ROLE4_STATE_ID;
                                        DRA["ANX_ROLE4_COMMENT"] = Item.ANX_ROLE4_COMMENT;
                                        break;

                                    case ROLES.Subdirector:
                                        DRA["ANX_ROLE8_STATE_ID"] = Item.ANX_ROLE8_STATE_ID;
                                        DRA["ANX_ROLE8_COMMENT"] = Item.ANX_ROLE8_COMMENT;
                                        break;
                                }
                                DRA["ANX_ROLEX_CHANGED"] = Item.ANX_ROLEX_CHANGED;
                            }
                        }

                        if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico && DTExpedientesTemporales.Rows.Count > 0 && Solicitud.Expediente.CALL_SIGN != null)
                        {
                            DataRow DRET = DTExpedientesTemporales.Rows[0];
                            DRET["CALL_SIGN"] = Solicitud.Expediente.CALL_SIGN;
                        }
                        break;

                    case STEPS.Tecnico:
                        DataRow DRT = DTTech.Rows[0];
                        DRT["TECH_ROLE_MINTIC_STATE_ID"] = Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_STATE_ID;
                        DRT["TECH_ROLE_MINTIC_COMMENT"] = Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_COMMENT;
                        DRT["TECH_ROLEX_CHANGED"] = Solicitud.AnalisisTecnico.TECH_ROLEX_CHANGED;
                        break;

                    case STEPS.Resolucion:
                        DataRow DRR = DTResAnalisis.Rows[0];
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Funcionario:
                                DRR["RES_ROLE1_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE1_STATE_ID;
                                DRR["RES_ROLE1_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE1_COMMENT;
                                break;
                            case ROLES.Revisor:
                                DRR["RES_ROLE2_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE2_STATE_ID;
                                DRR["RES_ROLE2_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE2_COMMENT;
                                break;
                            case ROLES.Coordinador:
                                DRR["RES_ROLE4_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE4_STATE_ID;
                                DRR["RES_ROLE4_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE4_COMMENT;
                                break;
                            case ROLES.Subdirector:
                                DRR["RES_ROLE8_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE8_STATE_ID;
                                DRR["RES_ROLE8_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE8_COMMENT;
                                break;
                            case ROLES.Asesor:
                                DRR["RES_ROLE16_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE16_STATE_ID;
                                DRR["RES_ROLE16_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE16_COMMENT;
                                break;
                            case ROLES.Director:
                                DRR["RES_ROLE32_STATE_ID"] = Solicitud.AnalisisResolucion.RES_ROLE32_STATE_ID;
                                DRR["RES_ROLE32_COMMENT"] = Solicitud.AnalisisResolucion.RES_ROLE32_COMMENT;
                                break;
                        }
                        DRR["RES_ROLEX_CHANGED"] = Solicitud.AnalisisResolucion.RES_ROLEX_CHANGED;
                        break;

                    case STEPS.EsViable:
                        DataRow DRV = DTResAnalisisViabilidad.Rows[0];
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Funcionario:
                                DRV["RES_ROLE1_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE1_STATE_ID;
                                DRV["RES_ROLE1_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE1_COMMENT;
                                break;
                            case ROLES.Revisor:
                                DRV["RES_ROLE2_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE2_STATE_ID;
                                DRV["RES_ROLE2_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE2_COMMENT;
                                break;
                            case ROLES.Coordinador:
                                DRV["RES_ROLE4_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE4_STATE_ID;
                                DRV["RES_ROLE4_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE4_COMMENT;
                                break;
                            case ROLES.Subdirector:
                                DRV["RES_ROLE8_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE8_STATE_ID;
                                DRV["RES_ROLE8_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE8_COMMENT;
                                break;
                            case ROLES.Asesor:
                                DRV["RES_ROLE16_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE16_STATE_ID;
                                DRV["RES_ROLE16_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE16_COMMENT;
                                break;
                            case ROLES.Director:
                                DRV["RES_ROLE32_STATE_ID"] = Solicitud.AnalisisViabilidad.RES_ROLE32_STATE_ID;
                                DRV["RES_ROLE32_COMMENT"] = Solicitud.AnalisisViabilidad.RES_ROLE32_COMMENT;
                                break;
                        }
                        DRV["RES_ROLEX_CHANGED"] = Solicitud.AnalisisViabilidad.RES_ROLEX_CHANGED;
                        break;
                }


                if (bGenerarComunicado)
                {
                    Helper.TXT = GetDinamicTexts(ref cData);
                    Helper.IMG = GetDinamicImages(ref cData);
                    GetUserSign(Solicitud.USR_ADMIN.USR_ROLE8, ref cData);

                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                    RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
                    MemoryStream mem = null;

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Administrativo:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Administrativo);
                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, false);
                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Administrativo);
                                            AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_ADMIN.USR_ROLE1.USR_ID, GROUPS.MinTIC, ROLES.Funcionario);
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Administrativo);
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    {
                                        switch (STATE248)
                                        {
                                            case ROLE248_STATE.Devuelto:
                                                CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Administrativo);
                                                AddConteoDevoluciones(ref DTConteos, Solicitud.SOL_UID, Solicitud.USR_ADMIN.USR_ROLE4.USR_ID, GROUPS.MinTIC, ROLES.Coordinador);
                                                break;

                                            case ROLE248_STATE.Aceptado:
                                                IgualarRole1States(ref DTAnexo);

                                                switch ((COM_TYPE)Solicitud.ComunicadoAdministrativo.COM_TYPE)
                                                {
                                                    case COM_TYPE.Requerido:
                                                        DRS["SOL_STATE_ID"] = (int)SOL_STATES.Requerido;
                                                        DRS["SOL_REQUIRED_DATE"] = DateTime.Now;
                                                        DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        registroNumber = AZTools.AZRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        //registroNumber = AlfaRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminRequermiento, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.Concesionario, ROLES.Concesionario, STEPS.Administrativo);

                                                        break;

                                                    case COM_TYPE.Aprobado:
                                                        switch ((SOL_TYPES_CLASS)Solicitud.SOL_TYPE_CLASS)
                                                        {
                                                            case SOL_TYPES_CLASS.AdministrativoTecnico:
                                                                if (Solicitud.SOL_IS_OTORGA && !Solicitud.Viabilidad.RES_IS_CREATED)
                                                                {
                                                                    DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                                                    DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;
                                                                    CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Viabilidad);
                                                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                                }
                                                                else
                                                                {
                                                                    DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                                                    DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnTramite;
                                                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                                    registroNumber = AZTools.AZRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_ADMIN_APROBAR");
                                                                    //registroNumber = AlfaRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_ADMIN_APROBAR", ref mem);
                                                                    AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminAprobacion, DOC_SOURCE.MinTIC, DOC_SOURCE.ANE, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                                    AsignacionesGroup2 = AutomaticAssignGroup2(ref cData, ref DTSolUsers);
                                                                    bool RetManager = CrearSolicitudICSManager(ref cData, ref Solicitud, ref DynamicSolicitud);
                                                                    if (!RetManager)
                                                                        return new ActionInfo(2, "Error en la operación escribiendo en ICSManager");
                                                                    DRS["SOL_STATE_ID"] = (int)SOL_STATES.Requerido;
                                                                    //DRS["SOL_REQUIRED_DATE"] = DateTime.Now;
                                                                    DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                                    ST_RDSComunicado ComunicadoTec = new ST_RDSComunicado(Guid.NewGuid(), COM_CLASS.Tecnico);
                                                                    Solicitud.AnalisisTecnico.TECH_ROLE1_STATE_ID = (int)ROLE1_STATE.Requerido;
                                                                    Solicitud.USR_TECH.USR_ROLE8 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                                                                    CrearTechAnexos(ref DTTechAnexos, ref Solicitud);//oscarrp
                                                                    mem = RDSComunicados.GenerarComunicado(ref ComunicadoTec, false);
                                                                    GuardarComunicado(ref DTComunicado, ref Solicitud, ComunicadoTec, ref mem);
                                                                    registroNumber = AZTools.AZRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                                    //registroNumber = AlfaRegistrarMinticToConcesionarioRequerir(ref Solicitud, ref Helper, ref mem);
                                                                    AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechRequermiento, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                                    //RetManager = CrearSolicitudICSManager(ref cData, ref Solicitud, ref DynamicSolicitud);
                                                                    //if (!RetManager)
                                                                    //    return new ActionInfo(2, "Error en la operación escribiendo en ICSManager");
                                                                    CURRENT.Fill(GROUPS.Concesionario, ROLES.Concesionario, STEPS.Tecnico);
                                                                }

                                                                break;

                                                            case SOL_TYPES_CLASS.AdministrativoOnly:
                                                                DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                                                DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                                                                DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                                                DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnCierre;
                                                                DRS["SOL_ENDED"] = 1;
                                                                registroNumber = AZTools.AZRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR");
                                                                //registroNumber = AlfaRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR");
                                                                mem = RDSComunicados.GenerarComunicado(ref Comunicado, true, registroNumber);
                                                                GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                                //AlfaRegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR"), ref Helper, ref mem);
                                                                AZTools.RegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_APROBAR"), ref Solicitud, ref mem, ref Helper);
                                                                AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminAprobacion, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                                CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.EnCierre);
                                                                break;
                                                        }
                                                        break;

                                                    case COM_TYPE.Rechazado:
                                                        DRS["SOL_STATE_ID"] = (int)SOL_STATES.Rechazada;
                                                        DRS["SOL_ENDED"] = 2;
                                                        //registroNumber = AlfaRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR");
                                                        registroNumber = AZTools.AZRegistrarMinticToConcesionario(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR");
                                                        mem = RDSComunicados.GenerarComunicado(ref Comunicado, true, registroNumber);
                                                        GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                                        //AlfaRegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR"), ref Helper, ref mem);
                                                        AZTools.RegisterAddDocument(registroNumber, DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_RECHAZAR"), ref Solicitud, ref mem, ref Helper);
                                                        AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.AdminRechazo, DOC_SOURCE.MinTIC, DOC_SOURCE.Concesionario, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                                        CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.Terminada);
                                                        break;
                                                }
                                                break;
                                        }
                                        break;
                                    }
                            }
                            UpdateAnexosRoleStates(ref DTAnexo, ref CURRENT);
                            break;

                        case STEPS.Tecnico:
                            switch (STATE248)
                            {
                                case ROLE248_STATE.Devuelto:
                                    Comunicado.COM_CLASS = (int)COM_CLASS.Revision;
                                    mem = RDSComunicados.GenerarComunicado(ref Comunicado, true);
                                    GuardarComunicado(ref DTComunicado, ref Solicitud, Comunicado, ref mem);
                                    //registroNumber = AlfaRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_TECH_DEVOLVER", ref mem);
                                    registroNumber = AZTools.AZRegistrarMinticToAne(ref Solicitud, ref Helper, "ALFA_REG_MINTIC_TO_ANE_TECH_DEVOLVER");
                                    AddNewDocument(ref DTDocuments, Solicitud.SOL_UID, DOC_CLASS.Registro, DOC_TYPE.TechDevolucion, DOC_SOURCE.MinTIC, DOC_SOURCE.ANE, registroNumber, ref mem, "Registro " + Solicitud.SOL_TYPE_NAME);
                                    CURRENT.Fill(GROUPS.ANE, ROLES.Subdirector, STEPS.Tecnico);
                                    DTTech.Rows[0]["TECH_ROLEX_CHANGED"] = false;
                                    break;
                                case ROLE248_STATE.Aceptado:
                                    cData.ClickExecuteProcedure("RADIO.SP_CARGARCUADROTECNICO", new ST_RDSParameters("@SOL_UID", Solicitud.SOL_UID));
                                    DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                    CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                    break;
                            }
                            break;

                        case STEPS.Resolucion:

                            DTResAnalisis.Rows[0]["RES_ROLEX_CHANGED"] = false;

                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Revisor, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Subdirector, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Director, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Resolucion);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            DRS["SOL_LAST_STATE_DATE"] = DateTime.Now;
                                            DRS["SOL_STATE_ID"] = (int)SOL_STATES.EnCierre;
                                            DRS["SOL_REQUIRED_DATE"] = (object)DBNull.Value;
                                            DRS["SOL_REQUIRED_POSTPONE"] = 0;
                                            DRS["SOL_ENDED"] = 1;
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.NoAplica, STEPS.EnCierre);
                                            break;
                                        case ROLE248_STATE.Revision:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Asesor, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                                case ROLES.Asesor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Director, STEPS.Resolucion);
                                            break;
                                    }
                                    break;
                            }
                            break;


                        case STEPS.EsViable:

                            DTResAnalisisViabilidad.Rows[0]["RES_ROLEX_CHANGED"] = false;

                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Director, STEPS.EsViable);
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Coordinador, STEPS.EsViable);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            CURRENT.Fill(GROUPS.MinTIC, ROLES.Funcionario, STEPS.Administrativo);
                                            break;
                                    }
                                    break;
                            }
                            break;

                    }

                    DRS["SOL_MODIFIED_DATE"] = DateTime.Now;
                    DRS["GROUP_CURRENT"] = CURRENT.GROUP;
                    DRS["ROLE_CURRENT"] = CURRENT.ROLE;
                    DRS["STEP_CURRENT"] = CURRENT.STEP;

                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                #region Logs y Correos

                if (bGenerarComunicado)
                {
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                    switch ((STEPS)Solicitud.CURRENT.STEP)
                    {
                        case STEPS.Administrativo:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch ((COM_TYPE)Comunicado.COM_TYPE)
                                    {
                                        case COM_TYPE.Aprobado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_APROBAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Rechazado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_RECHAZAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;

                                        case COM_TYPE.Requerido:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_REQUERIR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;

                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            break;

                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            switch ((COM_TYPE)Solicitud.ComunicadoAdministrativo.COM_TYPE)
                                            {
                                                case COM_TYPE.Requerido:
                                                    Ret = EnviarCorreo(MAIL_TYPE.CONCESIONARIO_SOL_REQUERIDA, ref Solicitud, ref Helper);
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_REQUERIR", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                                    break;

                                                case COM_TYPE.Rechazado:
                                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_RECHAZAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, registroNumber));
                                                    break;

                                                case COM_TYPE.Aprobado:
                                                    switch ((SOL_TYPES_CLASS)Solicitud.SOL_TYPE_CLASS)
                                                    {
                                                        case SOL_TYPES_CLASS.AdministrativoTecnico:
                                                            if (Solicitud.SOL_IS_OTORGA && !Solicitud.Viabilidad.RES_IS_CREATED)
                                                            {
                                                                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_ANALIZAR_VIABILIDAD", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                                            }
                                                            else
                                                            {
                                                                Helper.AddKey("NEW_CALL_SIGN", Solicitud.Expediente.CALL_SIGN);
                                                                EnviarCorreo(MAIL_TYPE.ASIGNACION_CANAL, ref Solicitud, ref Helper);

                                                                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                                                AddLogAutomaticAssign(ref DynamicSolicitud, Solicitud.SOL_UID, ref lstLogs, LOG_SOURCE.ANE, ref AsignacionesGroup2, ref Helper, ROLES.Funcionario, ROLES.Revisor, ROLES.Coordinador, ROLES.Subdirector);
                                                                Solicitud.USR_TECH.USR_ROLE1 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First();
                                                                Solicitud.USR_TECH.USR_ROLE2 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First();
                                                                Solicitud.USR_TECH.USR_ROLE4 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First();
                                                                Solicitud.USR_TECH.USR_ROLE8 = AsignacionesGroup2.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First();
                                                                Ret = EnviarCorreo(MAIL_TYPE.ANE_SOL_ASSIGNED, ref Solicitud, ref Helper);
                                                            }
                                                            break;
                                                        case SOL_TYPES_CLASS.AdministrativoOnly:
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_ADMINISTRATIVO_APROBAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, registroNumber));
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case STEPS.Tecnico:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REVISION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", registroNumber));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO_TO_ANE, ref Solicitud, ref Helper);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_TECNICO_REVISION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION", LOG_SOURCE.MinTIC, ROLES.NoAplica, "", ""));
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case STEPS.Resolucion:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Funcionario:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Funcionario, Solicitud.USR_ADMIN.USR_ROLE1.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Revisor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Revisor, Solicitud.USR_ADMIN.USR_ROLE2.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Revisor, Solicitud.USR_ADMIN.USR_ROLE2.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Subdirector:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Subdirector, Solicitud.USR_ADMIN.USR_ROLE8.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            break;
                                        case ROLE248_STATE.Revision:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_REVISION", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Asesor:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Asesor, Solicitud.USR_ADMIN.USR_ROLE16.USR_NAME, ""));
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Asesor, Solicitud.USR_ADMIN.USR_ROLE16.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                            }
                            break;

                        case STEPS.EsViable:
                            switch ((ROLES)ROLE)
                            {
                                case ROLES.Coordinador:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Coordinador, Solicitud.USR_ADMIN.USR_ROLE4.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                                case ROLES.Director:
                                    switch (STATE248)
                                    {
                                        case ROLE248_STATE.Devuelto:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            Ret = EnviarCorreo(MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO, ref Solicitud, ref Helper, ROLE);
                                            break;
                                        case ROLE248_STATE.Aceptado:
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_ANALISIS_RESOLUCION_ACEPTAR", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_SOL_CERRADA", LOG_SOURCE.MinTIC, ROLES.Director, Solicitud.USR_ADMIN.USR_ROLE32.USR_NAME, ""));
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    AddLog(ref cData, lstLogs);
                }

                #endregion

                return new ActionInfo(1, "Operación exitosa.", GetSolicitudMinTIC(ref cData, Solicitud.SOL_UID, (int)GROUPS.MinTIC));

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sSQL);
                return new ActionInfo(-1, "Error en el servidor. " + e.Message);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        private ROLE248_STATE OtorgaGetRole248State(ref ST_RDSSolicitud Solicitud, int ROLE)
        {
            ROLE248_STATE STATE248 = ROLE248_STATE.SinDefinir;

            switch ((STEPS)Solicitud.CURRENT.STEP)
            {
                case STEPS.Administrativo:
                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Coordinador:
                            if (Solicitud.Anexos.Any(bp => bp.ANX_ROLE4_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                STATE248 = ROLE248_STATE.Devuelto;
                            else
                                STATE248 = ROLE248_STATE.Aceptado;
                            break;
                        case ROLES.Subdirector:
                            if (Solicitud.Anexos.Any(bp => bp.ANX_ROLE8_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                STATE248 = ROLE248_STATE.Devuelto;
                            else
                                STATE248 = ROLE248_STATE.Aceptado;
                            break;
                    }
                    break;
                case STEPS.Tecnico:
                    STATE248 = (ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_STATE_ID;
                    break;

                case STEPS.Resolucion:
                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Funcionario:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE1_STATE_ID;
                            break;
                        case ROLES.Revisor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE2_STATE_ID;
                            break;
                        case ROLES.Coordinador:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE4_STATE_ID;
                            break;
                        case ROLES.Subdirector:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE8_STATE_ID;
                            break;
                        case ROLES.Asesor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE16_STATE_ID;
                            break;
                        case ROLES.Director:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE32_STATE_ID;
                            break;

                    }
                    break;

                case STEPS.EsViable:
                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Funcionario:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE1_STATE_ID;
                            break;
                        case ROLES.Revisor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE2_STATE_ID;
                            break;
                        case ROLES.Coordinador:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE4_STATE_ID;
                            break;
                        case ROLES.Subdirector:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE8_STATE_ID;
                            break;
                        case ROLES.Asesor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE16_STATE_ID;
                            break;
                        case ROLES.Director:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisViabilidad.RES_ROLE32_STATE_ID;
                            break;

                    }
                    break;
            }

            return STATE248;
        }

        public ActionInfo CRUDResolucionViabilidad(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSResolucion ResolucionViabilidad)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", ResolucionViabilidad=" + Serializer.Serialize(ResolucionViabilidad);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT SOL_UID, RES_NAME, RES_CONTENT, RES_CODIGO, RES_VERSION, RES_IS_CREATED, RES_CREATED_DATE, RES_MODIFIED_DATE, 
                        RES_TRACKING_COUNT, RES_PDF_FILE, RES_WORD_FILE, NUMERO_ACTO, ID_ARCHIVO, ID_DIRECTORIO, ID_PROCESO_INICIADO
                        FROM RADIO.RESOLUCIONES_VIABILIDAD 
                        WHERE SOL_UID='" + ResolucionViabilidad.SOL_UID + "'";




                var dtSet = cData.ObtenerDataSet(sSQL);
                DataTable DTResolucion = dtSet.Tables[0];               

                switch (DTResolucion.Rows.Count)
                {
                    case 1:
                        {
                            ST_Helper Helper = new ST_Helper();
                            Helper.TXT = GetDinamicTexts(ref cData);
                            Helper.IMG = GetDinamicImages(ref cData);
                            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                            ST_RDSResPlt ResPlt = new ST_RDSResPlt(ResolucionViabilidad);
                            RDSResolucionPDF RDSResolucion = new RDSResolucionPDF(ref ResPlt, ref DynamicSolicitud);
                            MemoryStream mem = RDSResolucion.GenerarResolucion(ResolucionViabilidad.RES_NAME, ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");
                            MemoryStream mem2 = RDSResolucion.GenerarResolucionWord(ResolucionViabilidad.RES_NAME, ResPlt.RES_PLT_CODIGO + " (" + ResPlt.RES_PLT_VERSION + ")");

                            DataRow DRR = DTResolucion.Rows[0];
                            DRR["RES_NAME"] = ResolucionViabilidad.RES_NAME;
                            DRR["RES_CONTENT"] = ResolucionViabilidad.RES_CONTENT;
                            DRR["RES_CODIGO"] = ResolucionViabilidad.RES_CODIGO;
                            DRR["RES_VERSION"] = ResolucionViabilidad.RES_VERSION;
                            DRR["RES_IS_CREATED"] = true;
                            if (!ResolucionViabilidad.RES_IS_CREATED)
                                DRR["RES_CREATED_DATE"] = DateTime.Now;
                            DRR["RES_MODIFIED_DATE"] = DateTime.Now;
                            DRR["RES_TRACKING_COUNT"] = ResolucionViabilidad.RES_TRACKING_COUNT;
                            DRR["RES_PDF_FILE"] = mem.GetBuffer();

                            //byte[] datos = mem2.GetBuffer();
                            //File.WriteAllBytes(@"E:\\Resolucion.docx", datos);

                            string strbase64 = Convert.ToBase64String(mem2.ToArray());
                            byte[] bytes = Convert.FromBase64String(strbase64);
                            //FileStream stream = new FileStream(@"D:\PruebasResolucion\PDF\Resolucion" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".docx", FileMode.Create);
                            //BinaryWriter writer = new BinaryWriter(stream);
                            //writer.Write(bytes, 0, bytes.Length);
                            //writer.Close();

                            //DRR["RES_WORD_FILE"] = mem2.GetBuffer(); 
                            DRR["RES_WORD_FILE"] = bytes;


                            ResolucionViabilidad.RES_CREATED_DATE = ((DateTime)DRR["RES_CREATED_DATE"]).ToString("yyyyMMdd");
                            ResolucionViabilidad.RES_MODIFIED_DATE = ((DateTime)DRR["RES_MODIFIED_DATE"]).ToString("yyyyMMdd");
                            ResolucionViabilidad.RES_IS_CREATED = true;
                            
                        }
                        break;

                    default:
                        WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Varias resoluciones para una misma solicitud: " + DTResolucion.Rows.Count, sLog);
                        return new ActionInfo(2, "Error en la operación");
                }
                
                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }                
                return new ActionInfo(1, "Operación exitosa", ResolucionViabilidad);
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSResolucion GetResolucionViabilidad(Guid SOL_UID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSResolucion mResult = new ST_RDSResolucion();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT SOL_UID, RES_NAME, RES_CONTENT, RES_CODIGO, RES_VERSION, RES_IS_CREATED, RES_CREATED_DATE, RES_MODIFIED_DATE, RES_TRACKING_COUNT
                        FROM RADIO.RESOLUCIONES_VIABILIDAD 
                        WHERE SOL_UID='" + SOL_UID + "'";


                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSResolucion
                           {
                               SOL_UID = bp.Field<Guid>("SOL_UID"),
                               RES_NAME = bp.Field<string>("RES_NAME"),
                               RES_CONTENT = bp.Field<string>("RES_CONTENT"),
                               RES_CODIGO = bp.Field<string>("RES_CODIGO"),
                               RES_VERSION = bp.Field<string>("RES_VERSION"),
                               RES_IS_CREATED = bp.Field<bool>("RES_IS_CREATED"),
                               RES_CREATED_DATE = bp.Field<DateTime>("RES_CREATED_DATE").Date.ToString("yyyyMMdd"),
                               RES_MODIFIED_DATE = bp.Field<DateTime>("RES_MODIFIED_DATE").Date.ToString("yyyyMMdd"),
                               RES_TRACKING_COUNT = bp.Field<int>("RES_TRACKING_COUNT")
                           }).ToList().FirstOrDefault();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo DarViabilidadSolicitud(int IDUserWeb, ST_RDSSolicitud Solicitud, string RazonDesempate)
        {
            ST_RDSCurrent CURRENT = new ST_RDSCurrent();

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            var sLog = "IDUserWeb=" + IDUserWeb + ", Solicitud=" + Serializer.Serialize(Solicitud);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }


                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);
                Helper.IMG = GetDinamicImages(ref cData);

                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                RDSComunicadosPDF RDSComunicados = new RDSComunicadosPDF(ref DynamicSolicitud);
                bool Ret = CearUsuarioSolicitudYExpedienteEnManager(Solicitud, RazonDesempate, cData, DynamicSolicitud);
                


                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }


                return new ActionInfo(1, "Operación exitosa.", GetSolicitudMinTIC(ref cData, Solicitud.SOL_UID, (int)GROUPS.MinTIC));
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message);
                return new ActionInfo(-1, "Error en el servidor. " + e.Message);
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        


        private bool CearUsuarioSolicitudYExpedienteEnManager(ST_RDSSolicitud Solicitud, string RazonDesempate, csData cData, RDSDynamicSolicitud DynamicSolicitud)
        {
            string sSQL = @"
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='USERS';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='USERS_CNTCT';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='SERV_LICENCE';

                            SELECT TOP 0 ID, TABLE_NAME, NAME, STATUS, TYPE, IDENT, IDENT_CHK, REGIST_NUM, REPR_FIRSTNAME, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, LEVEL, CODE 
                            FROM [ICSM_PROD].[dbo].USERS;
                            
                            SELECT TOP 0 ID, OPER_ID, ROLE, TITLE, FIRSTNAME, REGIST_NUM, TEL, EMAIL, CITY_ID, POSTCODE, CITY, PROVINCE, COUNTRY_ID, CUST_TXT1, CUST_TXT4, CUST_TXT5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, EMAIL2 
                            FROM [ICSM_PROD].[dbo].USERS_CNTCT;

                            SELECT TOP 0 ID, OWNER_ID, STATUS, SYNC_STATUS, NUMBER, CLASS, TYPE2, TYPE3, TYPE4, FIRST_DATE, LIQ_ESPECTRO, VALTRF_PROCESS, VALTRF_APP_ID, VALTRF_DUR, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, CUST_TXT3
                            FROM [ICSM_PROD].[dbo].SERV_LICENCE; 

                            SELECT ID, NAME, STATUS, TYPE, IDENT, IDENT_CHK, REPR_FIRSTNAME, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, LEVEL
                            FROM RADIO.USERS_TEMPORALES USRS
                            WHERE USRS.IDENT = '" + Solicitud.Expediente.Operador.CUS_IDENT + @"';

                            SELECT ID, ROLE, TITLE, FIRSTNAME, REGIST_NUM, TEL, EMAIL, CITY_ID, POSTCODE, CITY, PROVINCE, COUNTRY_ID, CUST_TXT1, CUST_TXT4, CUST_TXT5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, EMAIL2
                            FROM RADIO.USERS_CNTCT_TEMPORALES CNTCTS
                            WHERE CNTCTS.OPER_ID = (SELECT TOP 1 ID  FROM RADIO.USERS_TEMPORALES USRS WHERE USRS.IDENT = '" + Solicitud.Expediente.Operador.CUS_IDENT + @"');

                            SELECT SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, MODULATION, SERV_STATUS
                            FROM RADIO.EXPEDIENTES_TEMPORALES EXPS
                            WHERE EXPS.SERV_ID =  " + Solicitud.Expediente.SERV_ID + @";

                            SELECT Top 0 ID, TABLE_NAME, SRVLIC_ID, SRVLICN_ID, DESCRIPTION, STATUS, CESS_ID, NB_YEARS, MOD_EXE_DATE, COMPLETE, APP_USER, ADM_NAME, REF_OFFI, DATE_OFFI, 
                            FRONT_KEY, NEXT_ANSWER, REMARK, ACT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK, CLASS, ACTIVITY, SERVICE_TYPE
                            FROM [ICSM_PROD].[dbo].P_APPLICATION;
                
                            SELECT Top 0 ID, TABLE_NAME, FILE_ID, TYPE, COMPLETE, STATUS, DESCRIPTION, MEDIA, REF_OFFI, DATE_OFFI, REF_OP, DATE_OP, APP_USER, MESSAGE, REMARK, DATE_CREATED, 
                            CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                            FROM [ICSM_PROD].[dbo].P_LETTER;

                            SELECT SOL_UID,RAD_ALFANET,SOL_CREATOR,SOL_CREATED_DATE,SOL_MODIFIED_DATE,SOL_LAST_STATE_DATE,SOL_REQUIRED_DATE,SOL_REQUIRED_POSTPONE,CUS_ID,CUS_IDENT,CUS_NAME,
                            SERV_ID,SERV_NUMBER,SOL_TYPE_ID,SOL_STATE_ID,CONT_ID,CONT_NAME,CONT_NUMBER,CONT_EMAIL,CONT_ROLE,SOL_NUMBER,GROUP_CURRENT,ROLE_CURRENT,STEP_CURRENT,
                            FIN_SEVEN_ALDIA,FIN_ESTADO_CUENTA_NUMBER,FIN_ESTADO_CUENTA_DATE,FIN_REGISTRO_ALFA_NUMBER,FIN_REGISTRO_ALFA_DATE,SOL_ENDED,CAMPOS_SOL, RazonDesempate
                            From RADIO.SOLICITUDES
                            WHERE SOL_UID='" + Solicitud.SOL_UID + @"';

                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='SITE';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='LICENCE';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='FM_STATION';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='COMP_FM_CO';
                            SELECT TABLE_NAME, [NEXT] FROM [ICSM_PROD].[dbo].NEXT_ID WHERE TABLE_NAME='LFMF_STATION';

                            SELECT TOP 0 ID, TABLE_NAME, NAME, CODE, X, Y, CSYS, LONGITUDE, LATITUDE, DATUM, X_MIN, X_MAX, Y_MIN, Y_MAX, TYPE, CUST_CHB1, CUST_CHB2, CUST_CHB3, CUST_CHB4, CUST_CHB5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                            FROM [ICSM_PROD].[dbo].SITE;

                            SELECT TOP 0 ID, TABLE_NAME, ONE, TYPE_ID, OWNER_ID, SRVLIC_ID, STATUS, NUMBER, TYPE3, MOD_PROCESS, APP_MOD_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                            FROM [ICSM_PROD].[dbo].LICENCE;
                            
                            SELECT TOP 0 ID, STATUS, ST_FAMILY, ADM, STPL_TYP, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, TABLE_NAME, ONE, WORKING, TYPE, FREQ, PLAN_ID, CALL_SIGN, TPR_N, DESIG_EM, BW, TRANSMISSION_SYS, AGL, START_TIME, STOP_TIME, COORD_NEEDED, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, SITE_ID
                            FROM [ICSM_PROD].[dbo].FM_STATION;

                            SELECT TOP 0 ID, SV_ID, CMP_DATE, CMP_STATUS
                            FROM [ICSM_PROD].[dbo].COMP_FM_CO;

                            SELECT TOP 0 ID, ADM, STATUS, ST_FAMILY, STANDARD, FREQ, PLAN_ID, CALL_SIGN, SITE_ID, STPL_TYP, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, TABLE_NAME, ONE, HJ, HN, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, NAME
                            FROM [ICSM_PROD].[dbo].LFMF_STATION;

                            SELECT ID,STATUS,TABLE_NAME,STATION_CLASS,STATE,SUBPROVINCE,PROVINCE,CITY,POWER,ASL,FREQ,PLAN_ID,LINK_FREQ,LKPLAN_ID,CALL_SIGN,REMARK,CUST_TXT1,CUST_TXT2,CUST_TXT3,CUST_TXT4,
                            CUST_TXT5,CUST_TXT6,CUST_TXT7,CUST_TXT8,CUST_TXT9,CUST_DAT1,CUST_DAT2,CUST_DAT3,CUST_CHB1,CUST_CHB2,CUST_CHB3,CUST_NBR1,CUST_NBR2,CUST_NBR3,IMPOP_ID,DELRQ_ID,DATE_CREATED,
                            CREATED_BY,DATE_MODIFIED,MODIFIED_BY,LINK_BW,NUMBER,CLASS,CODE_AREA
                            FROM [ICSM_PROD].[dbo].PLAN_BRO_MINTIC PLANB
                            WHERE PLANB.CALL_SIGN = '" + Solicitud.Expediente.CALL_SIGN + @"';
                      
                            SELECT Top 0 ID, OBJ_TBNM, OBJ_TBID, EVENT_DATE, [USER], EVENT, AS_TEXT, AS_XML, CF1_ID, FR_STATE, TO_STATE, EVENT_MORE, AS_XMLX
                            FROM [ICSM_PROD].[dbo].REC_HISTORY;

                            SELECT Top 0 ID, APPL_ID, LIC_ID, REASON, WISHED_DATE, MESSAGE, RPLAC_ID, FOR_UNDO, DATE_CREATED, CREATED_BY, STATUS 
                            FROM ICSM_PROD.dbo.NT_APPLICATION;

                            SELECT TABLE_NAME, [NEXT]
                            FROM [ICSM_PROD].[dbo].NEXT_ID 
                            WHERE TABLE_NAME In('P_APPLICATION', 'P_LETTER', 'REC_HISTORY', 'NT_APPLICATION')
                            ORDER BY 1;

                            SELECT SOL_UID, SOL_STATE_ID, SOL_LAST_STATE_DATE, SOL_ENDED, SOL_MODIFIED_DATE, GROUP_CURRENT, ROLE_CURRENT, STEP_CURRENT
                            FROM RADIO.SOLICITUDES
                            WHERE (SOL_UID IN
                                    (   SELECT SOL_UID
                                        FROM RADIO.SOLICITUDES AS SOLICITUDES_1 INNER JOIN
                                                RADIO.V_EXPEDIENTES ON SOLICITUDES_1.SERV_ID = RADIO.V_EXPEDIENTES.SERV_ID
                                        WHERE (RADIO.V_EXPEDIENTES.CALL_SIGN = '" + Solicitud.Expediente.CALL_SIGN + @"') AND (SOLICITUDES_1.SOL_UID <> '" + Solicitud.SOL_UID + @"')
                                    )
                                  );
                            ";

            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DT_NEXT_ID_USERS = dtSet.Tables[0];
            DataTable DT_NEXT_ID_USERS_CNTCT = dtSet.Tables[1];
            DataTable DT_NEXT_ID_SERV_LICENCE = dtSet.Tables[2];
            DataTable DT_USERS = dtSet.Tables[3];
            DataTable DT_USERS_CNTCT = dtSet.Tables[4];
            DataTable DT_SERV_LICENCE = dtSet.Tables[5];
            DataTable DT_USERS_TEMPORALES = dtSet.Tables[6];
            DataTable DT_USERS_CNTCT_TEMPORALES = dtSet.Tables[7];
            DataTable DT_EXPEDIENTES_TEMPORALES = dtSet.Tables[8];
            DataTable DT_P_APPLICATION = dtSet.Tables[9];
            DataTable DT_P_LETTER = dtSet.Tables[10];
            DataTable DT_SOLICITUDES_RADIO = dtSet.Tables[11];
            DataTable DT_NEXT_ID_SITE = dtSet.Tables[12];
            DataTable DT_NEXT_ID_LICENCE = dtSet.Tables[13];
            DataTable DT_NEXT_ID_FM_STATION = dtSet.Tables[14];
            DataTable DT_NEXT_ID_COMP_FM_CO = dtSet.Tables[15];
            DataTable DT_NEXT_ID_LFMF_STATION = dtSet.Tables[16];
            DataTable DT_SITE = dtSet.Tables[17];
            DataTable DT_LICENCE = dtSet.Tables[18];
            DataTable DT_FM_STATION = dtSet.Tables[19];
            DataTable DT_COMP_FM_CO = dtSet.Tables[20];
            DataTable DT_LFMF_STATION = dtSet.Tables[21];
            DataTable DT_PLAN_BRO_MINTIC = dtSet.Tables[22];
            DataTable DT_REC_HISTORY = dtSet.Tables[23];
            DataTable DT_NT_APPLICATION = dtSet.Tables[24];
            DataTable DT_NEXT_ID = dtSet.Tables[25];
            DataTable DT_SOL_NO_VIABLES = dtSet.Tables[26];

            DateTime horaCambio = DateTime.Now;

            Dictionary<string, decimal> NextID = new Dictionary<string, decimal>();

            NextID = (from bp in DT_NEXT_ID.AsEnumerable().AsQueryable()
                      select new
                      {
                          Key = bp.Field<string>("TABLE_NAME"),
                          Next = bp.Field<decimal>("NEXT")
                      }).ToList().ToDictionary(x => x.Key, x => x.Next);

            DT_NEXT_ID.Rows[0]["NEXT"] = NextID["NT_APPLICATION"] + 2;
            DT_NEXT_ID.Rows[1]["NEXT"] = NextID["P_APPLICATION"] + 1;
            DT_NEXT_ID.Rows[2]["NEXT"] = NextID["P_LETTER"] + 1;
            DT_NEXT_ID.Rows[3]["NEXT"] = NextID["REC_HISTORY"] + 5;

            bool esUsuarioTemporalOtorga = DT_USERS_TEMPORALES.Rows.Count > 0;
            decimal idUser = Solicitud.Expediente.Operador.CUS_ID;
            decimal idCntct = Solicitud.Contacto.CONT_ID;
            int añosExpediente = 10;

            // Si es un usuario temporal, elimina su información de las tablas temporales y la agrega a las tablas oficiales de manager
            if (esUsuarioTemporalOtorga)
            {
                DataRow DR_USERS_TEMPORALES = DT_USERS_TEMPORALES.Rows[0];
                DataRow DR_USERS = DT_USERS.NewRow();

                idUser = DT_NEXT_ID_USERS.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_USERS.Rows[0]["NEXT"] = idUser + 1;

                DR_USERS["ID"] = idUser;
                DR_USERS["TABLE_NAME"] = "USERS";
                DR_USERS["NAME"] = DR_USERS_TEMPORALES["NAME"];
                DR_USERS["STATUS"] = DR_USERS_TEMPORALES["STATUS"];
                DR_USERS["TYPE"] = DR_USERS_TEMPORALES["TYPE"];
                DR_USERS["IDENT"] = DR_USERS_TEMPORALES["IDENT"];
                DR_USERS["IDENT_CHK"] = DR_USERS_TEMPORALES["IDENT_CHK"];
                DR_USERS["REGIST_NUM"] = 0; // Como no se ha creado, se le asigna branch 0
                DR_USERS["REPR_FIRSTNAME"] = DR_USERS_TEMPORALES["REPR_FIRSTNAME"];
                DR_USERS["DATE_CREATED"] = DR_USERS_TEMPORALES["DATE_CREATED"];
                DR_USERS["CREATED_BY"] = DR_USERS_TEMPORALES["CREATED_BY"];
                DR_USERS["DATE_MODIFIED"] = DR_USERS_TEMPORALES["DATE_MODIFIED"];
                DR_USERS["MODIFIED_BY"] = DR_USERS_TEMPORALES["MODIFIED_BY"];
                DR_USERS["LEVEL"] = DR_USERS_TEMPORALES["LEVEL"];
                DR_USERS["DATE_EVAL"] = horaCambio.Date;
                DR_USERS["EVALUATOR"] = "sa";
                DR_USERS["EVALUATION"] = "P";
                DR_USERS["EVAL_REMARK"] = "Análisis financiero aprobado";
                DR_USERS["CODE"] = "Sync"+idUser;

                DT_USERS.Rows.Add(DR_USERS);
                DR_USERS_TEMPORALES.Delete();

                decimal nextIdUsersCntc = DT_NEXT_ID_USERS_CNTCT.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_USERS_CNTCT.Rows[0]["NEXT"] = nextIdUsersCntc + DT_USERS_CNTCT_TEMPORALES.Rows.Count;

                for (int i = 0; i < DT_USERS_CNTCT_TEMPORALES.Rows.Count; i++)
                {
                    DataRow DR_USERS_CNTCT_TEMPORALES = DT_USERS_CNTCT_TEMPORALES.Rows[i];
                    DataRow DR_USERS_CNTCT = DT_USERS_CNTCT.NewRow();

                    if ((decimal)DR_USERS_CNTCT_TEMPORALES["ID"] == idCntct){
                        idCntct = nextIdUsersCntc;
                    }
                    DR_USERS_CNTCT["ID"] = nextIdUsersCntc++;
                    DR_USERS_CNTCT["OPER_ID"] = idUser;
                    DR_USERS_CNTCT["ROLE"] = DR_USERS_CNTCT_TEMPORALES["ROLE"];
                    DR_USERS_CNTCT["TITLE"] = DR_USERS_CNTCT_TEMPORALES["TITLE"];
                    DR_USERS_CNTCT["FIRSTNAME"] = DR_USERS_CNTCT_TEMPORALES["FIRSTNAME"];
                    DR_USERS_CNTCT["REGIST_NUM"] = DR_USERS_CNTCT_TEMPORALES["REGIST_NUM"];
                    DR_USERS_CNTCT["TEL"] = DR_USERS_CNTCT_TEMPORALES["TEL"];
                    DR_USERS_CNTCT["EMAIL"] = DR_USERS_CNTCT_TEMPORALES["EMAIL"];
                    DR_USERS_CNTCT["CITY_ID"] = DR_USERS_CNTCT_TEMPORALES["CITY_ID"];
                    DR_USERS_CNTCT["POSTCODE"] = DR_USERS_CNTCT_TEMPORALES["POSTCODE"];
                    DR_USERS_CNTCT["CITY"] = DR_USERS_CNTCT_TEMPORALES["CITY"];
                    DR_USERS_CNTCT["PROVINCE"] = DR_USERS_CNTCT_TEMPORALES["PROVINCE"];
                    DR_USERS_CNTCT["COUNTRY_ID"] = DR_USERS_CNTCT_TEMPORALES["COUNTRY_ID"];
                    DR_USERS_CNTCT["CUST_TXT1"] = DR_USERS_CNTCT_TEMPORALES["CUST_TXT1"];
                    DR_USERS_CNTCT["CUST_TXT4"] = DR_USERS_CNTCT_TEMPORALES["CUST_TXT4"];
                    DR_USERS_CNTCT["CUST_TXT5"] = DR_USERS_CNTCT_TEMPORALES["CUST_TXT5"];
                    DR_USERS_CNTCT["DATE_CREATED"] = DR_USERS_CNTCT_TEMPORALES["DATE_CREATED"];
                    DR_USERS_CNTCT["CREATED_BY"] = DR_USERS_CNTCT_TEMPORALES["CREATED_BY"];
                    DR_USERS_CNTCT["DATE_MODIFIED"] = DR_USERS_CNTCT_TEMPORALES["DATE_MODIFIED"];
                    DR_USERS_CNTCT["MODIFIED_BY"] = DR_USERS_CNTCT_TEMPORALES["MODIFIED_BY"];
                    DR_USERS_CNTCT["EMAIL2"] = DR_USERS_CNTCT_TEMPORALES["EMAIL2"];
                    DT_USERS_CNTCT.Rows.Add(DR_USERS_CNTCT);
                    DR_USERS_CNTCT_TEMPORALES.Delete();
                }
            }

            decimal idServlicence = DT_NEXT_ID_SERV_LICENCE.Rows[0].Field<decimal>("NEXT");
            DT_NEXT_ID_SERV_LICENCE.Rows[0]["NEXT"] = idServlicence + 1;
            int claseExpediente = GetClaseExpedienteFromTipoSolicitud(Solicitud.SOL_TYPE_ID);
            string type3Expediente = GetType3ExpedienteFromTipoSolicitud(Solicitud.SOL_TYPE_ID);

            // SERV_ID, SERV_NUMBER, LIC_NUMBER, CUS_ID, CUS_IDENT, CUS_NAME, CUS_BRANCH, CUS_EMAIL, STATION_CLASS, STATE, DEPTO, CITY, POWER, ASL, FREQUENCY, CALL_SIGN, SERV_NAME, STOP_DATE, MODULATION, SERV_STATUS
            // ID, OWNER_ID, STATUS, SYNC_STATUS, CLASS, TYPE2, TYPE3, TYPE4, FIRST_DATE, LIQ_ESPECTRO, VALTRF_PROCESS, VALTRF_APP_ID, VALTRF_DUR, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
            DataRow DR_EXPEDIENTES_TEMPORALES = DT_EXPEDIENTES_TEMPORALES.Rows[0];

            DataRow DR_SERV_LICENCE = DT_SERV_LICENCE.NewRow();
            DR_SERV_LICENCE["ID"] = idServlicence;
            DR_SERV_LICENCE["OWNER_ID"] = idUser;
            DR_SERV_LICENCE["STATUS"] = "eSOL";
            DR_SERV_LICENCE["SYNC_STATUS"] = "C";
            DR_SERV_LICENCE["NUMBER"] = Solicitud.Expediente.SERV_NUMBER;
            DR_SERV_LICENCE["CLASS"] = claseExpediente;
            DR_SERV_LICENCE["TYPE2"] = "1";
            DR_SERV_LICENCE["TYPE3"] = type3Expediente;
            DR_SERV_LICENCE["TYPE4"] = "NIN";
            DR_SERV_LICENCE["FIRST_DATE"] = horaCambio;
            DR_SERV_LICENCE["LIQ_ESPECTRO"] = "Y";
            DR_SERV_LICENCE["VALTRF_PROCESS"] = "OTO";
            DR_SERV_LICENCE["VALTRF_APP_ID"] = NextID["P_APPLICATION"];
            DR_SERV_LICENCE["VALTRF_DUR"] = añosExpediente;
            DR_SERV_LICENCE["CUST_TXT3"] = Solicitud.Expediente.CUST_TXT3;
            DR_SERV_LICENCE["DATE_CREATED"] = horaCambio;
            DR_SERV_LICENCE["CREATED_BY"] = "sa";
            DR_SERV_LICENCE["DATE_MODIFIED"] = horaCambio;
            DR_SERV_LICENCE["MODIFIED_BY"] = "sa";
            DT_SERV_LICENCE.Rows.Add(DR_SERV_LICENCE);
            DR_EXPEDIENTES_TEMPORALES.Delete();

            DataRow DR_P_APPLICATION = DT_P_APPLICATION.NewRow();
            DR_P_APPLICATION["ID"] = NextID["P_APPLICATION"];
            DR_P_APPLICATION["TABLE_NAME"] = "P_APPLICATION";
            DR_P_APPLICATION["SRVLIC_ID"] = idServlicence;
            DR_P_APPLICATION["SRVLICN_ID"] = idServlicence;
            DR_P_APPLICATION["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_APPLICATION["STATUS"] = "sTT";
            DR_P_APPLICATION["CLASS"] = claseExpediente;
            DR_P_APPLICATION["CESS_ID"] = (object)DBNull.Value;
            DR_P_APPLICATION["NB_YEARS"] = añosExpediente;
            DR_P_APPLICATION["ACTIVITY"] = "A2";
            DR_P_APPLICATION["SERVICE_TYPE"] = "C7";
            DR_P_APPLICATION["MOD_EXE_DATE"] = (object)DBNull.Value;
            DR_P_APPLICATION["COMPLETE"] = 0;
            DR_P_APPLICATION["APP_USER"] = (object)DBNull.Value;
            DR_P_APPLICATION["DATE_EVAL"] = horaCambio.Date;
            DR_P_APPLICATION["EVALUATOR"] = "sa";
            DR_P_APPLICATION["EVALUATION"] = "P";
            DR_P_APPLICATION["EVAL_REMARK"] = "Análisis Administrativo aprobado";
            DR_P_APPLICATION["ADM_NAME"] = "OT";
            DR_P_APPLICATION["REF_OFFI"] = Solicitud.Radicado;
            DR_P_APPLICATION["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture);
            DR_P_APPLICATION["FRONT_KEY"] = (object)DBNull.Value;
            DR_P_APPLICATION["NEXT_ANSWER"] = (object)DBNull.Value;
            DR_P_APPLICATION["REMARK"] = (object)DBNull.Value;
            DR_P_APPLICATION["ACT_ID"] = NextID["P_LETTER"];
            DR_P_APPLICATION["DATE_CREATED"] = horaCambio;
            DR_P_APPLICATION["CREATED_BY"] = "sa";
            DR_P_APPLICATION["DATE_MODIFIED"] = horaCambio;
            DR_P_APPLICATION["MODIFIED_BY"] = "sa";
            DT_P_APPLICATION.Rows.Add(DR_P_APPLICATION);

            DataRow DR_REC_HISTORY1 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY1["ID"] = NextID["REC_HISTORY"];
            DR_REC_HISTORY1["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY1["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY1["EVENT_DATE"] = horaCambio;
            DR_REC_HISTORY1["USER"] = "sa";
            DR_REC_HISTORY1["EVENT"] = "Solicitud creada";
            DR_REC_HISTORY1["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY1["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY1["FR_STATE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["TO_STATE"] = "sAA";
            DR_REC_HISTORY1["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY1);

            DataRow DR_P_LETTER = DT_P_LETTER.NewRow();
            DR_P_LETTER["ID"] = NextID["P_LETTER"];
            DR_P_LETTER["TABLE_NAME"] = "P_LETTER";
            DR_P_LETTER["FILE_ID"] = NextID["P_APPLICATION"];
            DR_P_LETTER["TYPE"] = "C_SOL";
            DR_P_LETTER["COMPLETE"] = (object)DBNull.Value;
            DR_P_LETTER["STATUS"] = (object)DBNull.Value;
            DR_P_LETTER["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_LETTER["MEDIA"] = (object)DBNull.Value;
            DR_P_LETTER["REF_OFFI"] = Solicitud.Radicado;
            DR_P_LETTER["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture);
            DR_P_LETTER["REF_OP"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_OP"] = (object)DBNull.Value;
            DR_P_LETTER["APP_USER"] = (object)DBNull.Value;
            DR_P_LETTER["MESSAGE"] = (object)DBNull.Value;
            DR_P_LETTER["REMARK"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_CREATED"] = horaCambio;
            DR_P_LETTER["CREATED_BY"] = "sa";
            DR_P_LETTER["DATE_MODIFIED"] = horaCambio;
            DR_P_LETTER["MODIFIED_BY"] = "sa";
            DT_P_LETTER.Rows.Add(DR_P_LETTER);

            DataRow DR_REC_HISTORY2 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY2["ID"] = NextID["REC_HISTORY"] + 1;
            DR_REC_HISTORY2["OBJ_TBNM"] = "SERV_LICENCE";
            DR_REC_HISTORY2["OBJ_TBID"] = idServlicence;
            DR_REC_HISTORY2["EVENT_DATE"] = horaCambio;
            DR_REC_HISTORY2["USER"] = "sa";
            DR_REC_HISTORY2["EVENT"] = "Solicitud recibida";
            DR_REC_HISTORY2["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY2["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY2["FR_STATE"] = "eAUT";
            DR_REC_HISTORY2["TO_STATE"] = "eSOL";
            DR_REC_HISTORY2["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY2);

            DataRow DR_REC_HISTORY3 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY3["ID"] = NextID["REC_HISTORY"] + 2;
            DR_REC_HISTORY3["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY3["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY3["EVENT_DATE"] = horaCambio;
            DR_REC_HISTORY3["USER"] = "sa";
            DR_REC_HISTORY3["EVENT"] = "Anal. Administrativo positivo";
            DR_REC_HISTORY3["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY3["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY3["FR_STATE"] = "sAA";
            DR_REC_HISTORY3["TO_STATE"] = "sTT";
            DR_REC_HISTORY3["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY3);

            DataRow DR_REC_HISTORY4 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY4["ID"] = NextID["REC_HISTORY"] + 3;
            DR_REC_HISTORY4["OBJ_TBNM"] = "USERS";
            DR_REC_HISTORY4["OBJ_TBID"] = idUser;
            DR_REC_HISTORY4["EVENT_DATE"] = horaCambio;
            DR_REC_HISTORY4["USER"] = "sa";
            DR_REC_HISTORY4["EVENT"] = "Anal. Financiero positivo";
            DR_REC_HISTORY4["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY4["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY4["FR_STATE"] = "uACT";
            DR_REC_HISTORY4["TO_STATE"] = "uACT";
            DR_REC_HISTORY4["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY4);

            DataRow DR_NT_APPLICATION = DT_NT_APPLICATION.NewRow();
            DR_NT_APPLICATION["ID"] = NextID["NT_APPLICATION"];
            DR_NT_APPLICATION["APPL_ID"] = NextID["P_APPLICATION"];
            DR_NT_APPLICATION["LIC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["REASON"] = "OTO";
            DR_NT_APPLICATION["WISHED_DATE"] = horaCambio.AddDays(37).Date;
            DR_NT_APPLICATION["MESSAGE"] = (object)DBNull.Value;
            DR_NT_APPLICATION["STATUS"] = "S";
            DR_NT_APPLICATION["RPLAC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["FOR_UNDO"] = (object)DBNull.Value;
            DR_NT_APPLICATION["DATE_CREATED"] = horaCambio;
            DR_NT_APPLICATION["CREATED_BY"] = "sa";
            DT_NT_APPLICATION.Rows.Add(DR_NT_APPLICATION);

            DataRow DR_SOLICITUDES_RADIO = DT_SOLICITUDES_RADIO.Rows[0];
            DR_SOLICITUDES_RADIO["CUS_IDENT"] = Solicitud.Expediente.Operador.CUS_IDENT;
            DR_SOLICITUDES_RADIO["SERV_ID"] = idServlicence;
            DR_SOLICITUDES_RADIO["CONT_ID"] = (int)idCntct;
            DR_SOLICITUDES_RADIO["SOL_MODIFIED_DATE"] = horaCambio;
            DR_SOLICITUDES_RADIO["ROLE_CURRENT"] = (int)ROLES.Coordinador;
            DR_SOLICITUDES_RADIO["STEP_CURRENT"] = (int)STEPS.EsViable;
            DR_SOLICITUDES_RADIO["RazonDesempate"] = RazonDesempate;

            for (int i = 0; i < DT_SOL_NO_VIABLES.Rows.Count; i++)
            {
                DataRow DRSNV = DT_SOL_NO_VIABLES.Rows[i];
                if (DRSNV["SOL_ENDED"] as int? != 2)
                {
                    DRSNV["SOL_STATE_ID"] = (int)SOL_STATES.Finalizada;
                    DRSNV["SOL_LAST_STATE_DATE"] = horaCambio;
                    DRSNV["SOL_ENDED"] = 2;
                    DRSNV["SOL_MODIFIED_DATE"] = horaCambio;
                    DRSNV["GROUP_CURRENT"] = (int)GROUPS.MinTIC;
                    DRSNV["ROLE_CURRENT"] = (int)ROLES.NoAplica;
                    DRSNV["STEP_CURRENT"] = (int)STEPS.Terminada;
                }
            }

            decimal idSite = DT_NEXT_ID_SITE.Rows[0].Field<decimal>("NEXT");
            DT_NEXT_ID_SITE.Rows[0]["NEXT"] = idSite + 1;

            DataRow DR_SITE = DT_SITE.NewRow();
            DR_SITE["ID"] = idSite;
            DR_SITE["TABLE_NAME"] = "SITE";
            DR_SITE["NAME"] = "Sitio " + idSite.ToString(CultureInfo.InvariantCulture);
            DR_SITE["CODE"] = idSite.ToString(CultureInfo.InvariantCulture);
            DR_SITE["X"] = -73.1111;
            DR_SITE["Y"] = 7.1111;
            DR_SITE["CSYS"] = "4DMS";
            DR_SITE["LONGITUDE"] = -73.1111;
            DR_SITE["LATITUDE"] = 7.1111;
            DR_SITE["DATUM"] = 4;
            DR_SITE["X_MIN"] = -73.1111;
            DR_SITE["X_MAX"] = -73.1111;
            DR_SITE["Y_MIN"] = 7.1111;
            DR_SITE["Y_MAX"] = 7.1111;
            DR_SITE["TYPE"] = "L";
            DR_SITE["CUST_CHB1"] = 0;
            DR_SITE["CUST_CHB2"] = 0;
            DR_SITE["CUST_CHB3"] = 0;
            DR_SITE["CUST_CHB4"] = 0;
            DR_SITE["CUST_CHB5"] = 0;
            DR_SITE["DATE_CREATED"] = horaCambio;
            DR_SITE["CREATED_BY"] = "sa";
            DR_SITE["DATE_MODIFIED"] = horaCambio;
            DR_SITE["MODIFIED_BY"] = "sa";
            DT_SITE.Rows.Add(DR_SITE);

            decimal idLicence = DT_NEXT_ID_LICENCE.Rows[0].Field<decimal>("NEXT");
            DT_NEXT_ID_LICENCE.Rows[0]["NEXT"] = idLicence + 1;

            DataRow DR_LICENCE = DT_LICENCE.NewRow();
            DR_LICENCE["ID"] = idLicence;
            DR_LICENCE["TABLE_NAME"] = "LICENCE";
            DR_LICENCE["ONE"] = 1;
            DR_LICENCE["TYPE_ID"] = 2;
            DR_LICENCE["OWNER_ID"] = idUser;
            DR_LICENCE["SRVLIC_ID"] = idServlicence;
            DR_LICENCE["STATUS"] = "rTT";
            DR_LICENCE["NUMBER"] = 1;
            DR_LICENCE["TYPE3"] = "1";
            DR_LICENCE["MOD_PROCESS"] = "ADD";
            DR_LICENCE["APP_MOD_ID"] = NextID["P_APPLICATION"];
            DR_LICENCE["DATE_CREATED"] = horaCambio;
            DR_LICENCE["CREATED_BY"] = "sa";
            DR_LICENCE["DATE_MODIFIED"] = horaCambio;
            DR_LICENCE["MODIFIED_BY"] = "sa";
            DT_LICENCE.Rows.Add(DR_LICENCE);

            DR_NT_APPLICATION = DT_NT_APPLICATION.NewRow();
            DR_NT_APPLICATION["ID"] = NextID["NT_APPLICATION"] + 1;
            DR_NT_APPLICATION["APPL_ID"] = NextID["P_APPLICATION"];
            DR_NT_APPLICATION["LIC_ID"] = idLicence;
            DR_NT_APPLICATION["REASON"] = "ADD";
            DR_NT_APPLICATION["WISHED_DATE"] = (object)DBNull.Value;
            DR_NT_APPLICATION["MESSAGE"] = (object)DBNull.Value;
            DR_NT_APPLICATION["STATUS"] = "S";
            DR_NT_APPLICATION["RPLAC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["FOR_UNDO"] = (object)DBNull.Value;
            DR_NT_APPLICATION["DATE_CREATED"] = horaCambio;
            DR_NT_APPLICATION["CREATED_BY"] = "sa";
            DT_NT_APPLICATION.Rows.Add(DR_NT_APPLICATION);

            DataRow DR_PLAN_BRO_MINTIC = DT_PLAN_BRO_MINTIC.Rows[0];
            DR_PLAN_BRO_MINTIC["STATE"] = "ASIGNADO";
            DR_PLAN_BRO_MINTIC["DATE_MODIFIED"] = horaCambio;
            DR_PLAN_BRO_MINTIC["MODIFIED_BY"] = "sa";

            // Aquí se revisa si es am o fm
            if (Solicitud.Expediente.FREQUENCY > 10)
            {
                // FM
                decimal idFM_STATION = DT_NEXT_ID_FM_STATION.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_FM_STATION.Rows[0]["NEXT"] = idFM_STATION + 1;

                DataRow DR_FM_STATION = DT_FM_STATION.NewRow();
                DR_FM_STATION["ID"] = idFM_STATION;
                DR_FM_STATION["STATUS"] = "aSOL";
                DR_FM_STATION["ST_FAMILY"] = "P";
                DR_FM_STATION["ADM"] = "AAA";
                DR_FM_STATION["STPL_TYP"] = "A";
                DR_FM_STATION["LIC_ID"] = idLicence;
                DR_FM_STATION["OWNER_ID"] = idUser;
                DR_FM_STATION["ITM_ST"] = "N";
                DR_FM_STATION["ITM_ANS"] = "U";
                DR_FM_STATION["TABLE_NAME"] = "FM_STATION";
                DR_FM_STATION["ONE"] = 1;
                DR_FM_STATION["WORKING"] = 0;
                DR_FM_STATION["TYPE"] = "E";
                DR_FM_STATION["FREQ"] = DR_PLAN_BRO_MINTIC["FREQ"];
                DR_FM_STATION["PLAN_ID"] = DR_PLAN_BRO_MINTIC["PLAN_ID"];
                DR_FM_STATION["CALL_SIGN"] = DR_PLAN_BRO_MINTIC["CALL_SIGN"];
                DR_FM_STATION["SITE_ID"] = idSite;
                DR_FM_STATION["TPR_N"] = 0;
                DR_FM_STATION["DESIG_EM"] = "12K5F3E";
                DR_FM_STATION["BW"] = DR_PLAN_BRO_MINTIC["LINK_BW"];
                DR_FM_STATION["TRANSMISSION_SYS"] = "4";
                DR_FM_STATION["AGL"] = 50;
                DR_FM_STATION["START_TIME"] = 0;
                DR_FM_STATION["STOP_TIME"] = 24;
                DR_FM_STATION["COORD_NEEDED"] = 1;
                DR_FM_STATION["DATE_CREATED"] = horaCambio;
                DR_FM_STATION["CREATED_BY"] = "sa";
                DR_FM_STATION["DATE_MODIFIED"] = horaCambio;
                DR_FM_STATION["MODIFIED_BY"] = "sa";
                DT_FM_STATION.Rows.Add(DR_FM_STATION);

                decimal idCOMP_FM_CO = DT_NEXT_ID_COMP_FM_CO.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_COMP_FM_CO.Rows[0]["NEXT"] = idCOMP_FM_CO + 1;

                DataRow DR_COMP_FM_CO = DT_COMP_FM_CO.NewRow();
                DR_COMP_FM_CO["ID"] = idCOMP_FM_CO;
                DR_COMP_FM_CO["SV_ID"] = idFM_STATION;
                DR_COMP_FM_CO["CMP_DATE"] = horaCambio;
                DR_COMP_FM_CO["CMP_STATUS"] = "Z";
                DT_COMP_FM_CO.Rows.Add(DR_COMP_FM_CO);
            }
            else
            {
                // AM
                decimal idLFMF_STATION = DT_NEXT_ID_LFMF_STATION.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_LFMF_STATION.Rows[0]["NEXT"] = idLFMF_STATION + 1;

                DataRow DR_LFMF_STATION = DT_LFMF_STATION.NewRow();
                DR_LFMF_STATION["ID"] = idLFMF_STATION;
                DR_LFMF_STATION["ADM"] = "AAA";
                DR_LFMF_STATION["STATUS"] = "aSOL";
                DR_LFMF_STATION["ST_FAMILY"] = "P";
                DR_LFMF_STATION["STANDARD"] = "92";
                DR_LFMF_STATION["FREQ"] = DR_PLAN_BRO_MINTIC["FREQ"];
                DR_LFMF_STATION["PLAN_ID"] = DR_PLAN_BRO_MINTIC["PLAN_ID"];
                DR_LFMF_STATION["CALL_SIGN"] = DR_PLAN_BRO_MINTIC["CALL_SIGN"];
                DR_LFMF_STATION["SITE_ID"] = idSite;
                DR_LFMF_STATION["STPL_TYP"] = "A";
                DR_LFMF_STATION["LIC_ID"] = idLicence;
                DR_LFMF_STATION["OWNER_ID"] = idUser;
                DR_LFMF_STATION["NAME"] = idUser;
                DR_LFMF_STATION["ITM_ST"] = "N";
                DR_LFMF_STATION["ITM_ANS"] = "U";
                DR_LFMF_STATION["TABLE_NAME"] = "LFMF_STATION";
                DR_LFMF_STATION["ONE"] = 1;
                DR_LFMF_STATION["HJ"] = "HJ";
                DR_LFMF_STATION["HN"] = "HN";
                DR_LFMF_STATION["DATE_CREATED"] = horaCambio;
                DR_LFMF_STATION["CREATED_BY"] = "sa";
                DR_LFMF_STATION["DATE_MODIFIED"] = horaCambio;
                DR_LFMF_STATION["MODIFIED_BY"] = "sa";

                DT_LFMF_STATION.Rows.Add(DR_LFMF_STATION);
            }

            DataRow DR_REC_HISTORY5 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY5["ID"] = NextID["REC_HISTORY"] + 4;
            DR_REC_HISTORY5["USER"] = "sa";
            DR_REC_HISTORY5["EVENT_DATE"] = horaCambio;
            DR_REC_HISTORY5["EVENT"] = "UserEdit";
            DR_REC_HISTORY5["OBJ_TBNM"] = "PLAN_BRO_MINTIC";
            DR_REC_HISTORY5["OBJ_TBID"] = DR_PLAN_BRO_MINTIC["ID"];
            DR_REC_HISTORY5["FR_STATE"] = "";
            DR_REC_HISTORY5["TO_STATE"] = "";
            DR_REC_HISTORY5["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY5);

            bool Ret = cData.InsertDataSet(sSQL, dtSet);

            string usuarioCode = null;
            string usuarioSyncKey = null;
            if (esUsuarioTemporalOtorga)
            {
                //try{ 

                //string tipoDeDocumento = DT_USERS.Rows[0]["TYPE"].ToString();
                //tipoDeDocumento = tipoDeDocumento.Replace(".", "");

                //ActualizacionExternaClient ws = new BDU.ActualizacionExternaClient();
                //int codigoBdu = ws.CrearOperador(tipoDeDocumento, int.Parse(DT_USERS.Rows[0]["IDENT"].ToString()), 6, DT_USERS.Rows[0]["NAME"].ToString(), DT_USERS.Rows[0]["NAME"].ToString(), DT_USERS.Rows[0]["NAME"].ToString(), DT_USERS.Rows[0]["NAME"].ToString(), 15.ToString(), DT_USERS.Rows[0]["NAME"].ToString());
                //usuarioCode = "Bdu" + codigoBdu;
                //usuarioSyncKey = "Bdu" + codigoBdu.ToString().PadLeft(10, '0');
                //}

                //catch{
                //    usuarioCode = "Bdu" + DT_USERS.Rows[0]["IDENT"].ToString();
                //    usuarioSyncKey = "Bdu" + DT_USERS.Rows[0]["IDENT"].ToString().PadLeft(10, '0');
                //}

                usuarioCode = "TES123";
                usuarioSyncKey = "TES123";

            }

            // Finalmente obtenemos el número del expediente después de realizar la operación anterior porque se actualiza en un trigger
            string sSQLNumber = @"

                                SELECT ID, NUMBER FROM [ICSM_PROD].[dbo].SERV_LICENCE WHERE ID = " + idServlicence + @";

                                SELECT SOL_UID,RAD_ALFANET,SOL_CREATOR,SOL_CREATED_DATE,SOL_MODIFIED_DATE,SOL_LAST_STATE_DATE,SOL_REQUIRED_DATE,SOL_REQUIRED_POSTPONE,CUS_ID,CUS_IDENT,CUS_NAME,
                                SERV_ID,SERV_NUMBER,SOL_TYPE_ID,SOL_STATE_ID,CONT_ID,CONT_NAME,CONT_NUMBER,CONT_EMAIL,CONT_ROLE,SOL_NUMBER,GROUP_CURRENT,ROLE_CURRENT,STEP_CURRENT,
                                FIN_SEVEN_ALDIA,FIN_ESTADO_CUENTA_NUMBER,FIN_ESTADO_CUENTA_DATE,FIN_REGISTRO_ALFA_NUMBER,FIN_REGISTRO_ALFA_DATE,SOL_ENDED,CAMPOS_SOL
                                FROM RADIO.SOLICITUDES WHERE SOL_UID='" + Solicitud.SOL_UID + @"';
                            
                                SELECT ID,NUMBER
                                FROM [ICSM_PROD].[dbo].PLAN_BRO_MINTIC PLANB
                                WHERE PLANB.CALL_SIGN = '" + Solicitud.Expediente.CALL_SIGN + @"';

                                SELECT ID, CODE, SYNC_KEY
                                FROM [ICSM_PROD].[dbo].USERS WHERE IDENT = '" + Solicitud.Contacto.CONT_NUMBER + @"';
                                ";

            var dtNumber = cData.ObtenerDataSet(sSQLNumber);

            DataTable DT_SERV_LICENCE_NUMBER = dtNumber.Tables[0];
            DataTable DT_SOLICITUDES_RADIO_NUMBER = dtNumber.Tables[1];
            DataTable DT_PLAN_BRO_MINTIC_NUMBER = dtNumber.Tables[2];
            DataTable DT_USUARIOSBDU = dtNumber.Tables[3];

            DataRow DR_SOLICITUDES_RADIO_NUMBER = DT_SOLICITUDES_RADIO_NUMBER.Rows[0];
            DR_SOLICITUDES_RADIO_NUMBER["SERV_NUMBER"] = DT_SERV_LICENCE_NUMBER.Rows[0]["NUMBER"];
            cData.InsertDataSet(sSQLNumber, dtNumber);

            DataRow DR_PLAN_BRO_MINTIC_NUMBER = DT_PLAN_BRO_MINTIC_NUMBER.Rows[0];
            DR_PLAN_BRO_MINTIC_NUMBER["NUMBER"] = DT_SERV_LICENCE_NUMBER.Rows[0]["NUMBER"];

            if (esUsuarioTemporalOtorga)
            {

                DataRow DR_USUARIOSBDU = DT_USUARIOSBDU.Rows[0];
                DR_USUARIOSBDU["CODE"] = usuarioCode;
                DR_USUARIOSBDU["SYNC_KEY"] = usuarioSyncKey;

            }
            cData.InsertDataSet(sSQLNumber, dtNumber);

            return Ret;
        }

        private int GetClaseExpedienteFromTipoSolicitud(int SOL_TYPE_ID)
        {
            switch (SOL_TYPE_ID)
            {
                case (int)SOL_TYPES.OtorgaEmisoraComercial:
                    return 13;
                case (int)SOL_TYPES.OtorgaEmisoraComunitaria:
                case (int)SOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos:
                    return 4;
                case (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico:
                    return 27;
                default:
                    throw new NotSupportedException();
            }
        }

        private string GetType3ExpedienteFromTipoSolicitud(int SOL_TYPE_ID)
        {
            switch (SOL_TYPE_ID)
            {
                case (int)SOL_TYPES.OtorgaEmisoraComercial:
                    return "CV";
                case (int)SOL_TYPES.OtorgaEmisoraComunitaria:
                case (int)SOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos:
                case (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico:
                    return "CP";
                default:
                    throw new NotSupportedException();
            }
        }

        public List<ST_Lugar> GetDepartamentos()
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_Lugar> mResult = new List<ST_Lugar>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                //ValidateToken(ref cData);

                sSQL = @"
                            SELECT        ID, CODE_AREA, NAME
                            FROM            RADIO.V_DEPARTAMENTOS
                            ORDER BY NAME
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_Lugar
                           {
                               ID = bp.Field<decimal>("ID"),
                               CODE_AREA = bp.Field<int>("CODE_AREA"),
                               NAME = bp.Field<string>("NAME"),
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_Lugar> GetMunicipios(int CODE_AREA_DEPARTAMENTO)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_Lugar> mResult = new List<ST_Lugar>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                //ValidateToken(ref cData);

                sSQL = @"
                            SELECT ID, CODE_AREA, NAME
                            FROM RADIO.[V_MUNICIPIOS]
                            WHERE CODE_DEPARTAMENTO = " + CODE_AREA_DEPARTAMENTO.ToString(CultureInfo.InvariantCulture) + @"

                        ";
                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_Lugar
                           {
                               ID = bp.Field<decimal>("ID"),
                               CODE_AREA = bp.Field<int>("CODE_AREA"),
                               NAME = bp.Field<string>("NAME"),
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_ResultExisteManifestacionUsuario ExisteManifestacionUsuario(string IDENT)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_ResultExisteManifestacionUsuario mResult = new ST_ResultExisteManifestacionUsuario();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                //ValidateToken(ref cData);

                sSQL = @"
                            SELECT COUNT(0) AS Expr1
                            FROM RADIO.V_CUSTOMERS
                            GROUP BY CUS_IDENT
                            HAVING (CUS_IDENT = N'" + IDENT + @"');
                        ";
                var valor = cData.TraerValor(sSQL, 0);
                mResult.ExisteUsuario = valor > 0;

                sSQL = @"
                            SELECT Identificacion, NombreRazonSocial, IdLugar, DireccionCorrespondencia, Telefono, Email, Ciudad, Departamento, CodeLugar, NombreRepresentante, IdentificacionRepresentante, CodeAreaDepartamento
                            FROM RADIO.V_INFO_REGISTRO_MANIFESTACION
                            WHERE Identificacion = N'" + IDENT + @"';
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTInfo = dtSet.Tables[0];

                if (DTInfo.Rows.Count > 0)
                {
                    DataRow DRIRU = DTInfo.Rows[0];
                    STV_RDSInfoRegistroUsuario infoRegistroUsuario = new STV_RDSInfoRegistroUsuario()
                    {
                        Identificacion = ((long)DRIRU["Identificacion"]).ToString(CultureInfo.InvariantCulture),
                        NombreRazonSocial = (string)DRIRU["NombreRazonSocial"],
                        IdLugar = (int)DRIRU["IdLugar"],
                        DireccionCorrespondencia = DRIRU["DireccionCorrespondencia"] != DBNull.Value ? (string)DRIRU["DireccionCorrespondencia"] : null,
                        Telefono = DRIRU["Telefono"] != DBNull.Value ? (string)DRIRU["Telefono"] : null,
                        Email = DRIRU["Email"] != DBNull.Value ? (string)DRIRU["Email"] : null,
                        Ciudad = DRIRU["Ciudad"] != DBNull.Value ? (string)DRIRU["Ciudad"] : null,
                        Departamento = DRIRU["Departamento"] != DBNull.Value ? (string)DRIRU["Departamento"] : null,
                        CodeLugar = DRIRU["CodeLugar"] != DBNull.Value ? ((int)DRIRU["CodeLugar"]).ToString(CultureInfo.InvariantCulture) : null,
                        NombreRepresentante = DRIRU["NombreRepresentante"] != DBNull.Value ? (string)DRIRU["NombreRepresentante"] : null,
                        IdentificacionRepresentante = DRIRU["IdentificacionRepresentante"] != DBNull.Value ? ((long)DRIRU["IdentificacionRepresentante"]).ToString(CultureInfo.InvariantCulture) : null,
                        CodeAreaDepartamento = DRIRU["CodeAreaDepartamento"] != DBNull.Value ? (int)DRIRU["CodeAreaDepartamento"] : 0,
                    };
                    mResult.InfoRegistroManfestacion = infoRegistroUsuario;
                }

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CrearUsuario(STV_RDSInfoRegistroUsuario infoUsuario)
        {
            int IDUserWeb = 1;

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", InfoRegistroUsuario=" + Serializer.Serialize(infoUsuario);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                //ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();
                Helper.TXT = GetDinamicTexts(ref cData);

                string sSQLMaxId = @"
                        SELECT MAX(ID) AS MAX_ID FROM RADIO.USERS_TEMPORALES;
                        ";

                var dtSetMaxId = cData.ObtenerDataSet(sSQLMaxId);

                int UserTempNewId = 1;
                if (dtSetMaxId.Tables[0].AsEnumerable().AsQueryable().First()[0] != System.DBNull.Value)
                    UserTempNewId = (int)dtSetMaxId.Tables[0].AsEnumerable().AsQueryable().First().Field<decimal>("MAX_ID") + 1;

                sSQL = @"
                        SELECT TOP 0  ID,NAME,STATUS,TYPE,IDENT,IDENT_CHK,REPR_FIRSTNAME,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,LEVEL
                        FROM RADIO.USERS_TEMPORALES;

                        SELECT TOP 0  ID,OPER_ID,ROLE,TITLE,FIRSTNAME,REGIST_NUM,TEL,EMAIL,CITY_ID,POSTCODE,CITY,SUBPROVINCE,PROVINCE,COUNTRY_ID,CUST_TXT1,CUST_TXT4,CUST_TXT5,DATE_CREATED,CREATED_BY,DATE_MODIFIED,MODIFIED_BY,EMAIL2,ADDRESS
                        FROM RADIO.USERS_CNTCT_TEMPORALES;

                        SELECT Top 0 USR_ID, USR_GROUP, USR_LOGIN, USR_PASSWORD, USR_NAME, USR_ROLE, USR_ASSIGNABLE, USR_STATUS, USR_EMAIL 
                        FROM RADIO.USUARIOS;
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTUsers = dtSet.Tables[0];
                DataTable DTCntcts = dtSet.Tables[1];
                DataTable DTUsuarios = dtSet.Tables[2];

                DateTime horaCambio = DateTime.Now;
                string TITLE_Empresa = "EMP";
                string ROLE_Empresa = "4";
                string TITLE_Representante = "REP";
                string ROLE_Representante = "2";
                string TITLE_Apoderado = "APO";
                string ROLE_Apoderado = "1";
                string COUNTRY_ID_Colombia = "CLM";

                DataRow DRUT = DTUsers.NewRow();
                DRUT["ID"] = UserTempNewId;
                DRUT["NAME"] = infoUsuario.NombreRazonSocial;
                DRUT["STATUS"] = "uACT";
                DRUT["TYPE"] = infoUsuario.TipoDocumento;
                DRUT["IDENT"] = infoUsuario.Identificacion;
                DRUT["IDENT_CHK"] = CalcularCheck(infoUsuario.Identificacion, infoUsuario.TipoDocumento);
                DRUT["REPR_FIRSTNAME"] = infoUsuario.EsPersonaNatural ? infoUsuario.NombreRazonSocial :
                    (String.IsNullOrWhiteSpace(infoUsuario.NombreComercial) ? infoUsuario.NombreRazonSocial : infoUsuario.NombreComercial);
                DRUT["DATE_CREATED"] = horaCambio;
                DRUT["CREATED_BY"] = "sa";
                DRUT["DATE_MODIFIED"] = horaCambio;
                DRUT["MODIFIED_BY"] = "sa";
                DRUT["LEVEL"] = 0;
                DTUsers.Rows.Add(DRUT);

                if (infoUsuario.EsPersonaNatural)
                {
                    DataRow DRCT = DTCntcts.NewRow();
                    DRCT["OPER_ID"] = UserTempNewId;
                    DRCT["ROLE"] = ROLE_Empresa;
                    DRCT["TITLE"] = TITLE_Empresa;
                    DRCT["FIRSTNAME"] = infoUsuario.NombreRazonSocial;
                    DRCT["REGIST_NUM"] = infoUsuario.Identificacion;
                    DRCT["TEL"] = infoUsuario.Telefono;
                    DRCT["EMAIL"] = infoUsuario.Email;
                    DRCT["CITY_ID"] = infoUsuario.IdLugar;
                    DRCT["POSTCODE"] = 0;
                    DRCT["CITY"] = infoUsuario.Ciudad;
                    DRCT["SUBPROVINCE"] = infoUsuario.Departamento;
                    DRCT["COUNTRY_ID"] = COUNTRY_ID_Colombia;
                    DRCT["CUST_TXT1"] = infoUsuario.Ciudad;
                    DRCT["CUST_TXT4"] = 1;
                    DRCT["CUST_TXT5"] = infoUsuario.CodeLugar;
                    DRCT["EMAIL2"] = infoUsuario.Email;
                    DRCT["ADDRESS"] = infoUsuario.DireccionCorrespondencia;
                    DRCT["DATE_CREATED"] = horaCambio;
                    DRCT["CREATED_BY"] = "sa";
                    DRCT["DATE_MODIFIED"] = horaCambio;
                    DRCT["MODIFIED_BY"] = "sa";
                    DTCntcts.Rows.Add(DRCT);

                    DataRow DRCTRP = DTCntcts.NewRow();
                    DRCTRP["OPER_ID"] = UserTempNewId;
                    DRCTRP["ROLE"] = ROLE_Representante;
                    DRCTRP["TITLE"] = TITLE_Representante;
                    DRCTRP["FIRSTNAME"] = infoUsuario.NombreRazonSocial;
                    DRCTRP["REGIST_NUM"] = infoUsuario.Identificacion;
                    DRCTRP["TEL"] = infoUsuario.Telefono;
                    DRCTRP["EMAIL"] = infoUsuario.Email;
                    DRCTRP["CITY_ID"] = infoUsuario.IdLugar;
                    DRCTRP["POSTCODE"] = 0;
                    DRCTRP["CITY"] = infoUsuario.Ciudad;
                    DRCTRP["SUBPROVINCE"] = infoUsuario.Departamento;
                    DRCTRP["COUNTRY_ID"] = COUNTRY_ID_Colombia;
                    DRCTRP["CUST_TXT1"] = infoUsuario.Ciudad;
                    DRCTRP["CUST_TXT4"] = 1;
                    DRCTRP["CUST_TXT5"] = infoUsuario.CodeLugar;
                    DRCTRP["EMAIL2"] = infoUsuario.Email;
                    DRCTRP["ADDRESS"] = infoUsuario.DireccionCorrespondencia;
                    DRCTRP["DATE_CREATED"] = horaCambio;
                    DRCTRP["CREATED_BY"] = "sa";
                    DRCTRP["DATE_MODIFIED"] = horaCambio;
                    DRCTRP["MODIFIED_BY"] = "sa";
                    DTCntcts.Rows.Add(DRCTRP);
                }
                else
                {
                    DataRow DRCT = DTCntcts.NewRow();
                    DRCT["OPER_ID"] = UserTempNewId;
                    DRCT["ROLE"] = ROLE_Empresa;
                    DRCT["TITLE"] = TITLE_Empresa;
                    DRCT["FIRSTNAME"] = infoUsuario.NombreRazonSocial;
                    DRCT["REGIST_NUM"] = infoUsuario.Identificacion;
                    DRCT["TEL"] = infoUsuario.Telefono;
                    DRCT["EMAIL"] = infoUsuario.Email;
                    DRCT["CITY_ID"] = infoUsuario.IdLugarEmpresa;
                    DRCT["POSTCODE"] = 0;
                    DRCT["CITY"] = infoUsuario.CiudadEmpresa;
                    DRCT["SUBPROVINCE"] = infoUsuario.DepartamentoEmpresa;
                    DRCT["COUNTRY_ID"] = COUNTRY_ID_Colombia;
                    DRCT["CUST_TXT1"] = infoUsuario.CiudadEmpresa;
                    DRCT["CUST_TXT4"] = 1;
                    DRCT["CUST_TXT5"] = infoUsuario.CodeLugarEmpresa;
                    DRCT["EMAIL2"] = infoUsuario.Email;
                    DRCT["ADDRESS"] = infoUsuario.DireccionEmpresa;
                    DRCT["DATE_CREATED"] = horaCambio;
                    DRCT["CREATED_BY"] = "sa";
                    DRCT["DATE_MODIFIED"] = horaCambio;
                    DRCT["MODIFIED_BY"] = "sa";
                    DTCntcts.Rows.Add(DRCT);

                    DataRow DRCTRP = DTCntcts.NewRow();
                    DRCTRP["OPER_ID"] = UserTempNewId;
                    DRCTRP["ROLE"] = ROLE_Representante;
                    DRCTRP["TITLE"] = TITLE_Representante;
                    DRCTRP["FIRSTNAME"] = infoUsuario.NombreRepresentante;
                    DRCTRP["REGIST_NUM"] = infoUsuario.IdentificacionRepresentante;
                    DRCTRP["TEL"] = infoUsuario.Telefono;
                    DRCTRP["EMAIL"] = infoUsuario.Email;
                    DRCTRP["CITY_ID"] = infoUsuario.IdLugar;
                    DRCTRP["POSTCODE"] = 0;
                    DRCTRP["CITY"] = infoUsuario.Ciudad;
                    DRCTRP["SUBPROVINCE"] = infoUsuario.Departamento;
                    DRCTRP["COUNTRY_ID"] = COUNTRY_ID_Colombia;
                    DRCTRP["CUST_TXT1"] = infoUsuario.Ciudad;
                    DRCTRP["CUST_TXT4"] = 1;
                    DRCTRP["CUST_TXT5"] = infoUsuario.CodeLugar;
                    DRCTRP["EMAIL2"] = infoUsuario.Email;
                    DRCTRP["ADDRESS"] = infoUsuario.DireccionCorrespondencia;
                    DRCTRP["DATE_CREATED"] = horaCambio;
                    DRCTRP["CREATED_BY"] = "sa";
                    DRCTRP["DATE_MODIFIED"] = horaCambio;
                    DRCTRP["MODIFIED_BY"] = "sa";
                    DTCntcts.Rows.Add(DRCTRP);

                    if (!String.IsNullOrWhiteSpace(infoUsuario.NombreApoderado) && !String.IsNullOrWhiteSpace(infoUsuario.IdentificacionApoderado))
                    {
                        DataRow DRCTAPO = DTCntcts.NewRow();
                        DRCTAPO["OPER_ID"] = UserTempNewId;
                        DRCTAPO["ROLE"] = ROLE_Apoderado;
                        DRCTAPO["TITLE"] = TITLE_Apoderado;
                        DRCTAPO["FIRSTNAME"] = infoUsuario.NombreApoderado;
                        DRCTAPO["REGIST_NUM"] = infoUsuario.IdentificacionApoderado;
                        DRCTAPO["TEL"] = infoUsuario.Telefono;
                        DRCTAPO["EMAIL"] = infoUsuario.Email;
                        DRCTAPO["CITY_ID"] = infoUsuario.IdLugar;
                        DRCTAPO["POSTCODE"] = 0;
                        DRCTAPO["CITY"] = infoUsuario.Ciudad;
                        DRCTAPO["SUBPROVINCE"] = infoUsuario.Departamento;
                        DRCTAPO["COUNTRY_ID"] = COUNTRY_ID_Colombia;
                        DRCTAPO["CUST_TXT1"] = infoUsuario.Ciudad;
                        DRCTAPO["CUST_TXT4"] = 1;
                        DRCTAPO["CUST_TXT5"] = infoUsuario.CodeLugar;
                        DRCTAPO["EMAIL2"] = infoUsuario.Email;
                        DRCTAPO["ADDRESS"] = infoUsuario.DireccionCorrespondencia;
                        DRCTAPO["DATE_CREATED"] = horaCambio;
                        DRCTAPO["CREATED_BY"] = "sa";
                        DRCTAPO["DATE_MODIFIED"] = horaCambio;
                        DRCTAPO["MODIFIED_BY"] = "sa";
                        DTCntcts.Rows.Add(DRCTAPO);
                    }
                }

                DataRow DRUSR = DTUsuarios.NewRow();
                DRUSR["USR_GROUP"] = 0;
                DRUSR["USR_LOGIN"] = infoUsuario.Identificacion;
                DRUSR["USR_PASSWORD"] = EncryptSHA256(infoUsuario.Contrasena);
                DRUSR["USR_NAME"] = infoUsuario.NombreRazonSocial;
                DRUSR["USR_ROLE"] = 0;
                DRUSR["USR_ASSIGNABLE"] = 0;
                DRUSR["USR_STATUS"] = 1;
                DRUSR["USR_EMAIL"] = infoUsuario.Email;
                DTUsuarios.Rows.Add(DRUSR);

                bool ret = cData.InsertDataSet(sSQL, dtSet);

                if (!ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                Helper.AddKey("NEW_USER_EMAIL", infoUsuario.Email);
                Helper.AddKey("NEW_USER_NAME", infoUsuario.NombreRazonSocial);
                Helper.AddKey("NEW_USER_LOGIN", infoUsuario.Identificacion);
                Helper.AddKey("NEW_USER_PASSWORD", infoUsuario.Contrasena);
                ST_RDSSolicitud Solicitud = new ST_RDSSolicitud();
                ret = EnviarCorreo(MAIL_TYPE.REGISTRO_USUARIO, ref Solicitud, ref Helper);



                return new ActionInfo(1, "Operación exitosa.");
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        private string CalcularCheck(string identificacion, string tipoDocumento)
        {
            int check = 0;

            if (tipoDocumento == "Nit")
            {
                for (int i = 0; i < identificacion.Length; i++)
                {
                    int digito = int.Parse(identificacion[i].ToString(CultureInfo.InvariantCulture));
                    check += digito;
                    if (check >= 10){
                        check = check - 10 + 1;
                    }
                }
            }

            return check.ToString(CultureInfo.InvariantCulture);
        }

        public ST_InfoConfiguracionesProcesos GetConfiguracionesProcesos(int IDUserWeb)
        {
            csData cData = null;
            ST_InfoConfiguracionesProcesos mResult = new ST_InfoConfiguracionesProcesos();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT        ID, SOL_TYPE, CLASS, NumeroResolucion, FechaResolucion, Nombre, ConfiguracionCalificaciones, FechaInicioRecepcionSolicitudes, FechaFinRecepcionSolicitudes, PuntajeMinimoViabilidadAutomatica,
                                            GETDATE() AS FechaActual, RADIO.F_GetPuedeSolicitar(SOL_TYPE) AS PuedeSolicitar 
                            FROM            RADIO.CONFIGURACION_PROCESO
                            ORDER BY SOL_TYPE;

                            SELECT        RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, 
                                                     RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM            RADIO.TIPO_ANEXO_SOL INNER JOIN
                                                     RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL = RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL INNER JOIN
                                                     RADIO.CONFIGURACION_PROCESO ON RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO = RADIO.CONFIGURACION_PROCESO.ID WHERE HABILITANTES = 'TRUE';
                            

                            SELECT        RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL, RADIO.TIPO_ANEXO_SOL.TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.DES_TIPO_ANX_SOL, RADIO.TIPO_ANEXO_SOL.EXTENSIONS, RADIO.TIPO_ANEXO_SOL.MAX_SIZE, RADIO.TIPO_ANEXO_SOL.MAX_NAME_LENGTH, 
                                                     RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO, RADIO.DOCUMENTOS_CONFIGURACION.HABILITANTES
                            FROM            RADIO.TIPO_ANEXO_SOL INNER JOIN
                                                     RADIO.DOCUMENTOS_CONFIGURACION ON RADIO.TIPO_ANEXO_SOL.ID_ANX_SOL = RADIO.DOCUMENTOS_CONFIGURACION.ID_TIPO_ANEXO_SOL INNER JOIN
                                                     RADIO.CONFIGURACION_PROCESO ON RADIO.DOCUMENTOS_CONFIGURACION.ID_CONFIGURACION_PROCESO = RADIO.CONFIGURACION_PROCESO.ID WHERE HABILITANTES = 'FALSE';
                            

                           

                            SELECT ID_ANX_SOL, TIPO_ANX_SOL, DES_TIPO_ANX_SOL, EXTENSIONS, MAX_SIZE, MAX_NAME_LENGTH FROM RADIO.TIPO_ANEXO_SOL;
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                mResult.Configuraciones = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                           select new ST_RDSConfiguracionProcesoOtorga
                                           {
                                               ID = bp.Field<Guid>("ID"),
                                               SOL_TYPE = bp.Field<int>("SOL_TYPE"),
                                               CLASS = (int)bp.Field<decimal>("CLASS"),
                                               NumeroResolucion = bp.Field<string>("NumeroResolucion"),
                                               FechaResolucion = bp.Field<DateTime>("FechaResolucion").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                               Nombre = bp.Field<string>("Nombre"),
                                               ConfiguracionCalificaciones = bp.Field<string>("ConfiguracionCalificaciones"),
                                               FechaInicioRecepcionSolicitudes = bp.Field<DateTime>("FechaInicioRecepcionSolicitudes").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                               FechaFinRecepcionSolicitudes = bp.Field<DateTime>("FechaFinRecepcionSolicitudes").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                               PuntajeMinimoViabilidadAutomatica = bp.Field<decimal>("PuntajeMinimoViabilidadAutomatica"),
                                               Descripcion_SOL_TYPE = GetDescipcionTipoSolicitud(bp.Field<int>("SOL_TYPE")),
                                               FechaActual = bp.Field<DateTime>("FechaActual").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                               ProcesoBloqueado = bp.Field<bool>("PuedeSolicitar") && bp.Field<int>("SOL_TYPE") != (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico,
                                               TiposDocumentos = (from bpc in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("ID_CONFIGURACION_PROCESO") == bp.Field<Guid>("ID"))
                                                                  select new ST_TipoDocumentoProcesoOtorga
                                                                  {
                                                                      ID_ANX_SOL = bpc.Field<int>("ID_ANX_SOL"),
                                                                      TIPO_ANX_SOL = bpc.Field<string>("TIPO_ANX_SOL"),
                                                                      DES_TIPO_ANX_SOL = bpc.Field<string>("DES_TIPO_ANX_SOL"),
                                                                      EXTENSIONS = bpc.Field<string>("EXTENSIONS"),
                                                                      MAX_SIZE = bpc.Field<int>("MAX_SIZE"),
                                                                      MAX_NAME_LENGTH = bpc.Field<int>("MAX_NAME_LENGTH"),
                                                                      Descripcion = bpc.Field<string>("TIPO_ANX_SOL") + " - " + bpc.Field<string>("DES_TIPO_ANX_SOL"),
                                                                      HABILITANTES = bpc.Field<bool>("HABILITANTES")
                                                                  }).ToList(),

                                               TiposDocumentosPond = (from bpc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<Guid>("ID_CONFIGURACION_PROCESO") == bp.Field<Guid>("ID"))
                                                                  select new ST_TipoDocumentoProcesoOtorgaPond
                                                                  {
                                                                      ID_ANX_SOL = bpc.Field<int>("ID_ANX_SOL"),
                                                                      TIPO_ANX_SOL = bpc.Field<string>("TIPO_ANX_SOL"),
                                                                      DES_TIPO_ANX_SOL = bpc.Field<string>("DES_TIPO_ANX_SOL"),
                                                                      EXTENSIONS = bpc.Field<string>("EXTENSIONS"),
                                                                      MAX_SIZE = bpc.Field<int>("MAX_SIZE"),
                                                                      MAX_NAME_LENGTH = bpc.Field<int>("MAX_NAME_LENGTH"),
                                                                      Descripcion = bpc.Field<string>("TIPO_ANX_SOL") + " - " + bpc.Field<string>("DES_TIPO_ANX_SOL"),
                                                                      HABILITANTES = bpc.Field<bool>("HABILITANTES")
                                                                  }).ToList(),
                                           }).ToList();


                mResult.TiposDocumentos = (from bpd in dtSet.Tables[3].AsEnumerable().AsQueryable()
                                           select new ST_TipoDocumentoProcesoOtorga
                                           {
                                               ID_ANX_SOL = bpd.Field<int>("ID_ANX_SOL"),
                                               TIPO_ANX_SOL = bpd.Field<string>("TIPO_ANX_SOL"),
                                               DES_TIPO_ANX_SOL = bpd.Field<string>("DES_TIPO_ANX_SOL"),
                                               EXTENSIONS = bpd.Field<string>("EXTENSIONS"),
                                               MAX_SIZE = bpd.Field<int>("MAX_SIZE"),
                                               MAX_NAME_LENGTH = bpd.Field<int>("MAX_NAME_LENGTH"),
                                               Descripcion = bpd.Field<string>("TIPO_ANX_SOL") + " - " + bpd.Field<string>("DES_TIPO_ANX_SOL")
                                           }).ToList();

               

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        private string GetDescipcionTipoSolicitud(int SOL_TYPE)
        {
            string result = string.Empty;
            switch (SOL_TYPE)
            {
                case 15:
                    result = "Emisoras comunitarias";
                    break;
                case 16:
                    result = "Emisoras comunitarias grupos étnicos";
                    break;
                case 14:
                    result = "Emisoras comerciales";
                    break;
                case 17:
                    result = "Emisoras de interés público";
                    break;
            }
            return result;
        }

        public ActionInfo ActualizarConfiguracionProceso(ST_RDSConfiguracionProcesoOtorga ConfiguracionProceso)
        {
            int IDUserWeb = 1;

            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", ConfiguracionProceso=" + Serializer.Serialize(ConfiguracionProceso);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"
                            SELECT        ID, SOL_TYPE, CLASS, NumeroResolucion, FechaResolucion, Nombre, ConfiguracionCalificaciones, FechaInicioRecepcionSolicitudes, FechaFinRecepcionSolicitudes, PuntajeMinimoViabilidadAutomatica
                            FROM            RADIO.CONFIGURACION_PROCESO
                            WHERE ID = '" + ConfiguracionProceso.ID + @"';

                            SELECT ID, ID_CONFIGURACION_PROCESO, ID_TIPO_ANEXO_SOL, HABILITANTES
                              FROM RADIO.DOCUMENTOS_CONFIGURACION
                            WHERE ID_CONFIGURACION_PROCESO = '" + ConfiguracionProceso.ID + @"'AND HABILITANTES='TRUE';

                        SELECT ID, ID_CONFIGURACION_PROCESO, ID_TIPO_ANEXO_SOL, HABILITANTES
                              FROM RADIO.DOCUMENTOS_CONFIGURACION
                            WHERE ID_CONFIGURACION_PROCESO = '" + ConfiguracionProceso.ID + @"'AND HABILITANTES='FALSE';
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTConfiguracion = dtSet.Tables[0];
                DataTable DTAnexos = dtSet.Tables[1];
                DataTable DTAnexosPond = dtSet.Tables[2];

                DataRow DRCP = DTConfiguracion.Rows[0];
                DRCP["NumeroResolucion"] = ConfiguracionProceso.NumeroResolucion;
                DRCP["FechaResolucion"] = DateTime.ParseExact(ConfiguracionProceso.FechaResolucion, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRCP["Nombre"] = ConfiguracionProceso.Nombre;
                DRCP["ConfiguracionCalificaciones"] = ConfiguracionProceso.ConfiguracionCalificaciones;
                DRCP["FechaInicioRecepcionSolicitudes"] = DateTime.ParseExact(ConfiguracionProceso.FechaInicioRecepcionSolicitudes, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRCP["FechaFinRecepcionSolicitudes"] = DateTime.ParseExact(ConfiguracionProceso.FechaFinRecepcionSolicitudes, "yyyyMMdd", CultureInfo.InvariantCulture);
                DRCP["PuntajeMinimoViabilidadAutomatica"] = ConfiguracionProceso.PuntajeMinimoViabilidadAutomatica;

                foreach (DataRow dataRow in DTAnexos.Rows)
                {
                    dataRow.Delete();
                }

                foreach (DataRow dataRow in DTAnexosPond.Rows)
                {
                    dataRow.Delete();
                }

                foreach (ST_TipoDocumentoProcesoOtorga documento in ConfiguracionProceso.TiposDocumentos)
                {
                    DataRow DRANX = DTAnexos.NewRow();
                    DRANX["ID_CONFIGURACION_PROCESO"] = ConfiguracionProceso.ID;
                    DRANX["ID_TIPO_ANEXO_SOL"] = documento.ID_ANX_SOL;
                    DRANX["HABILITANTES"] = true;

                    DTAnexos.Rows.Add(DRANX);
                }
                foreach (ST_TipoDocumentoProcesoOtorgaPond documento in ConfiguracionProceso.TiposDocumentosPond)
                {
                    DataRow DRANXP = DTAnexosPond.NewRow();
                    DRANXP["ID_CONFIGURACION_PROCESO"] = ConfiguracionProceso.ID;
                    DRANXP["ID_TIPO_ANEXO_SOL"] = documento.ID_ANX_SOL;
                    DRANXP["HABILITANTES"] = false;

                    DTAnexosPond.Rows.Add(DRANXP);
                }

                bool ret = cData.InsertDataSet(sSQL, dtSet);

                if (!ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                return new ActionInfo(1, "Operación exitosa.");
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ActionInfo ActualizarCalificacionSolicitud(int IDUserWeb, Guid SOL_UID, string CalificacionSolicitud, decimal Puntaje)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", SOL_UID=" + Serializer.Serialize(SOL_UID) + ", CalificacionSolicitud=" + Serializer.Serialize(CalificacionSolicitud);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();

                sSQL = @"
                            SELECT        SOL_UID, Evaluaciones, Puntaje
                            FROM            RADIO.SOLICITUDES
                            WHERE SOL_UID = '" + SOL_UID + @"';
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTSolicitudes = dtSet.Tables[0];

                DataRow DRSL = DTSolicitudes.Rows[0];
                DRSL["Evaluaciones"] = CalificacionSolicitud;
                DRSL["Puntaje"] = Puntaje;

                bool ret = cData.InsertDataSet(sSQL, dtSet);

                if (!ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                return new ActionInfo(1, "Operación exitosa.");
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ST_RDSOperador GetOperador(string NitOperador)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSOperador mResult = new ST_RDSOperador();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        SELECT CUS_ID, CUS_IDENT, CUS_NAME, CUS_EMAIL, CUS_BRANCH
                        FROM RADIO.V_CUSTOMERS CUS
                        where CUS.CUS_IDENT = '" + NitOperador + @"';
                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTInfo = dtSet.Tables[0];

                if (DTInfo.Rows.Count > 0)
                {
                    mResult = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                        select new ST_RDSOperador
                                        {
                                            CUS_ID = bp.Field<int>("CUS_ID"),
                                            CUS_IDENT = bp.Field<string>("CUS_IDENT"),
                                            CUS_NAME = bp.Field<string>("CUS_NAME"),
                                            CUS_EMAIL = bp.Field<string>("CUS_EMAIL"),
                                            CUS_BRANCH = bp.Field<int>("CUS_BRANCH")
                                        }).ToList().FirstOrDefault();
                }

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }        

        public string GetConfiguracionCalificacionesSolicitud(int SOL_TYPE_ID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            string mResult = null;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT TOP 1 [ConfiguracionCalificaciones]
                              FROM [SageFrontOffice].[RADIO].[CONFIGURACION_PROCESO]
                              WHERE SOL_TYPE = " + SOL_TYPE_ID + @"
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DConfiguracion = dtSet.Tables[0];

                mResult = DConfiguracion.Rows[0].Field<string>("ConfiguracionCalificaciones");

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }
        
        #endregion Otorga

        #endregion

        #region Observaciones

        public ActionInfo ReasignarObservacion(int IDUserWeb, ST_RDSObservacion Observacion, ST_RDSReasignUp Reasign)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var sLog = "IDUserWeb=" + IDUserWeb + ", Reasign=" + Serializer.Serialize(Reasign);

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                ST_Helper Helper = new ST_Helper();


                sSQL = @"
                        SELECT ID_OBSERVACION, ID_FUNCIONARIO, ID_COORDINADOR, ROL_ACTUAL, RESPUESTA, FECHA_OBSERVACION, FECHA_RESPUESTA,
                        USR_ROLE1_REASIGN, USR_ROLE1_REASIGN_COMMENT
                        FROM RADIO.OBSERVACIONES 
                        Where ID_OBSERVACION='" + Reasign.ID_OBSERVACION.ToString() + @"';

                        Select USR_ID, USR_NAME, USR_EMAIL, USR_ROLE
                        From RADIO.USUARIOS
                        Where USR_STATUS=1 And (USR_GROUP & " + Reasign.USR_GROUP + @" <> 0) And (USR_ROLE & 0xFF <> 0);

                        SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;
                ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                List<ST_RDSUsuario> Usuarios = (from bp in dtSet.Tables[1].AsEnumerable().AsQueryable()
                                                select new ST_RDSUsuario
                                                {
                                                    USR_ID = bp.Field<int>("USR_ID"),
                                                    USR_NAME = bp.Field<string>("USR_NAME"),
                                                    USR_EMAIL = bp.Field<string>("USR_EMAIL"),
                                                    USR_ROLE = bp.Field<int>("USR_ROLE")
                                                }).ToList();

                Helper.TXT = (from bp in dtSet.Tables[2].AsEnumerable().AsQueryable()
                              select new
                              {
                                  Key = bp.Field<string>("TXT_ID"),
                                  Text = bp.Field<string>("TXT_TEXT")
                              }).ToList().ToDictionary(x => x.Key, x => x.Text);


                DataTable DTObservacion = dtSet.Tables[0];
                DataRow DRS = DTObservacion.Rows[0];

                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:
                        DRS["USR_ROLE1_REASIGN"] = true;
                        DRS["USR_ROLE1_REASIGN_COMMENT"] = Reasign.COMMENT;
                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["ID_COORDINADOR"];
                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                        break;

                    case REASIGN_TYPE.Rechazar:
                        DRS["USR_ROLE1_REASIGN"] = false;
                        DRS["USR_ROLE1_REASIGN_COMMENT"] = (object)DBNull.Value;
                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["ID_COORDINADOR"];
                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                        break;

                    case REASIGN_TYPE.Aprobar:
                    case REASIGN_TYPE.Reasignar:
                        DRS["USR_ROLE1_REASIGN"] = false;
                        DRS["USR_ROLE1_REASIGN_COMMENT"] = (object)DBNull.Value;
                        DRS["ID_FUNCIONARIO"] = Reasign.NEW_USR_ID;
                        Reasign.NEXT_LEVEL_USR_ID = (int)DRS["ID_COORDINADOR"];
                        Reasign.NEXT_LEVEL_ROLE = (int)ROLES.Coordinador;
                        break;

                }


                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }


                AddLogReasignObs(ref cData, ref Observacion, Reasign, ref Usuarios, ref Helper);

                EnviarCorreoObsReasign(ref Observacion, Reasign, ref Usuarios, ref Helper);


                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ST_RDSObservaciones GetObservacionesMinTIC(int IDUserWeb, int USR_ROLE)//oscar
        {
            csData cData = null;
            ST_RDSObservaciones mResult = new ST_RDSObservaciones();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                            
                        Declare @USR_ID int=" + IDUserWeb + @"
                        Declare @USR_ROLE int=" + USR_ROLE + @"

                        SELECT O.ID_OBSERVACION, O.ID_CONVOCATORIA, E.NOMBRE_ETAPA ETAPA, O.CONSECUTIVO, O.TIPO_EMISORA, O.TIPO_DOCUMENTO, O.DOCUMENTO,
                        O.NOMBRE, O.EMAIL, O.NOMBRE_ORGANIZACION, O.CARGO, O.ID_CATEGORIA, C.NOMBRE_CATEGORIA, O.OBSERVACION, O.ESTADO, 
                        O.ID_FUNCIONARIO, O.ID_COORDINADOR, O.ROL_ACTUAL, O.RESPUESTA, O.FECHA_OBSERVACION, O.FECHA_RESPUESTA,
                        USR1.USR_NAME NAME1, USR1.USR_EMAIL EMAIL1, USR2.USR_NAME NAME4, USR2.USR_EMAIL EMAIL4, O.USR_ROLE1_REASIGN, O.USR_ROLE1_REASIGN_COMMENT
                        INTO #OBSERVACIONES
                        FROM RADIO.OBSERVACIONES O
                        LEFT OUTER JOIN RADIO.USUARIOS AS USR1 ON O.ID_FUNCIONARIO = USR1.USR_ID
                        LEFT OUTER JOIN RADIO.USUARIOS AS USR2 ON O.ID_COORDINADOR = USR2.USR_ID
                        INNER JOIN RADIO.OBSERVACIONES_CATEGORIAS C ON C.ID_CATEGORIA = O.ID_CATEGORIA
                        INNER JOIN RADIO.OBSERVACIONES_ETAPAS AS E ON E.ID_ETAPA = O.ETAPA
                        WHERE ((@USR_ROLE=1 And O.ID_FUNCIONARIO=@USR_ID) Or (@USR_ROLE=4 And O.ID_COORDINADOR=@USR_ID)) --AND (O.ROL_ACTUAL = @USR_ROLE)) 
                        Or (@USR_ROLE=128)

                        SELECT * FROM #OBSERVACIONES

                        SELECT ID_IMAGEN, ID_OBSERVACION, NOMBRE_IMAGEN, IMAGEN
                        FROM RADIO.OBSERVACIONES_IMAGENES
                        WHERE ID_OBSERVACION IN (SELECT ID_OBSERVACION FROM #OBSERVACIONES)

                        SELECT C.ID_CONVOCATORIA, A.ID_CATEGORIA, A.NOMBRE_CATEGORIA
                        FROM RADIO.OBSERVACIONES_CONFIGURACION C
                        INNER JOIN RADIO.OBSERVACIONES_CATEGORIAS A ON C.ID_CATEGORIA = A.ID_CATEGORIA
                        WHERE ID_CONVOCATORIA IN (SELECT ID_CONVOCATORIA FROM #OBSERVACIONES)

                        DROP TABLE #OBSERVACIONES

                        ";

                var dtSet = cData.ObtenerDataSet(sSQL);



                mResult.Observaciones = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                         select new ST_RDSObservacion
                                         {
                                             ID_OBSERVACION = bp.Field<int>("ID_OBSERVACION"),
                                             ID_CONVOCATORIA = bp.Field<int>("ID_CONVOCATORIA"),
                                             ETAPA = bp.Field<string>("ETAPA"),
                                             CONSECUTIVO = bp.Field<string>("CONSECUTIVO"),
                                             TIPO_EMISORA = bp.Field<string>("TIPO_EMISORA"),
                                             TIPO_DOCUMENTO = bp.Field<string>("TIPO_DOCUMENTO"),
                                             DOCUMENTO = bp.Field<string>("DOCUMENTO"),
                                             NOMBRE = bp.Field<string>("NOMBRE"),
                                             EMAIL = bp.Field<string>("EMAIL"),
                                             NOMBRE_ORGANIZACION = bp.Field<string>("NOMBRE_ORGANIZACION"),
                                             CARGO = bp.Field<string>("CARGO"),
                                             ID_CATEGORIA = bp.Field<int>("ID_CATEGORIA"),
                                             NOMBRE_CATEGORIA = bp.Field<string>("NOMBRE_CATEGORIA"),
                                             OBSERVACION = bp.Field<string>("OBSERVACION"),
                                             ESTADO = bp.Field<string>("ESTADO"),
                                             ID_FUNCIONARIO = bp.Field<int>("ID_FUNCIONARIO"),
                                             ID_COORDINADOR = bp.Field<int>("ID_COORDINADOR"),
                                             ROL_ACTUAL = bp.Field<int>("ROL_ACTUAL"),
                                             RESPUESTA = bp.Field<string>("RESPUESTA"),
                                             FECHA_OBSERVACION = bp.Field<DateTime>("FECHA_OBSERVACION").Date.ToString("yyyyMMdd"),
                                             FECHA_RESPUESTA = bp.Field<DateTime?>("FECHA_RESPUESTA").HasValue ? bp.Field<DateTime>("FECHA_RESPUESTA").Date.ToString("yyyyMMdd") : "",
                                             NAME1 = bp.Field<string>("NAME1"),
                                             EMAIL1 = bp.Field<string>("EMAIL1"),
                                             NAME4 = bp.Field<string>("NAME4"),
                                             EMAIL4 = bp.Field<string>("EMAIL4"),
                                             USR_ROLE1_REASIGN = bp.Field<bool>("USR_ROLE1_REASIGN"),
                                             USR_ROLE1_REASIGN_COMMENT = bp.Field<string>("USR_ROLE1_REASIGN_COMMENT"),
                                             Adjuntos = (from bpa in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<int>("ID_OBSERVACION") == bp.Field<int>("ID_OBSERVACION"))
                                                         select new ST_ImagenObservacion
                                                         {
                                                             ID_IMAGEN = bpa.Field<Guid>("ID_IMAGEN"),
                                                             ID_OBSERVACION = bpa.Field<int>("ID_OBSERVACION"),
                                                             NOMBRE_IMAGEN = bpa.Field<string>("NOMBRE_IMAGEN"),
                                                         }).ToList(),
                                             Categorias = (from bpc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("ID_CONVOCATORIA") == bp.Field<int>("ID_CONVOCATORIA"))
                                                           select new ST_CategoriasObservacion
                                                           {
                                                               ID_CATEGORIA = bpc.Field<int>("ID_CATEGORIA"),
                                                               NOMBRE_CATEGORIA = bpc.Field<string>("NOMBRE_CATEGORIA"),
                                                           }).ToList(),
                                         }).ToList();

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo ResponderObservacionOtorga(ST_RDSObservacion Observacion, bool RespondePregunta, string estadoObs)
        {
            int IDUserWeb = 1;
            List<string> Emails = new List<string>();
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            Serializer.MaxJsonLength = Int32.MaxValue;

            var sLog = "IDUserWeb=" + IDUserWeb + ", Observacion=" + Serializer.Serialize(Observacion);
            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        SELECT ID_OBSERVACION, ID_CATEGORIA, ESTADO, ROL_ACTUAL, RESPUESTA, FECHA_RESPUESTA
                        FROM RADIO.OBSERVACIONES
                        Where ID_OBSERVACION='" + Observacion.ID_OBSERVACION + @"';

                        SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS;

                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTObservacion = dtSet.Tables[0];

                DataRow DRS = DTObservacion.Rows[0];
                DRS["ID_CATEGORIA"] = Observacion.ID_CATEGORIA;
                DRS["ESTADO"] = Observacion.ESTADO;
                DRS["ROL_ACTUAL"] = Observacion.ROL_ACTUAL;
                DRS["RESPUESTA"] = Observacion.RESPUESTA;
                if (RespondePregunta)
                    DRS["FECHA_RESPUESTA"] = DateTime.Now;
                else
                    DRS["FECHA_RESPUESTA"] = (object)DBNull.Value;

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                if (estadoObs != "")
                {
                    List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();
                    ST_Helper Helper = new ST_Helper();
                    Helper.TXT = (from bp in dtSet.Tables[1].AsEnumerable().AsQueryable()
                                  select new
                                  {
                                      Key = bp.Field<string>("TXT_ID"),
                                      Text = bp.Field<string>("TXT_TEXT")
                                  }).ToList().ToDictionary(x => x.Key, x => x.Text);
                    RDSDynamicObservacion DynamicObservacion = new RDSDynamicObservacion(ref Observacion, ref Helper);
                    if (estadoObs == "Devuelta")
                    {
                        Ret = EnviarCorreoObs("MAIL_OBSERVACION_DEVUELTO", ref DynamicObservacion, Observacion.ROL_ACTUAL);
                        lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_OBSERVACION_DEVOLVER", LOG_SOURCE.MinTIC, ROLES.Coordinador, Observacion.NAME4, ""));
                    }
                    if (estadoObs == "Resuelta")
                    {
                        lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_OBS_APROBADA", LOG_SOURCE.MinTIC, ROLES.Coordinador, Observacion.NAME4, ""));
                    }
                    AddLogObs(ref cData, lstLogs);
                }
                return new ActionInfo(1, "Proceso realizado con exito.");


            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }

        }

        public ST_RDSConfigObservacionOtorga GetConfiguracionObservaciones(int IDUserWeb)
        {
            csData cData = null;
            ST_RDSConfigObservacionOtorga mResult = new ST_RDSConfigObservacionOtorga();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                            SELECT ID, NOMBRE_CONVOCATORIA, CODIGO_CONVOCATORIA, ANIO_CONVOCATORIA, SOL_TYPE
                            FROM RADIO.OBSERVACIONES_CONVOCATORIAS;

                            SELECT ID_ETAPA, ID_CONVOCATORIA, NOMBRE_ETAPA, FECHA_INICIO, FECHA_FIN, DESCRIPCION
                            FROM RADIO.OBSERVACIONES_ETAPAS;

                            SELECT  A.ID, B.ID_CATEGORIA, C.NOMBRE_CATEGORIA
                            FROM RADIO.OBSERVACIONES_CONVOCATORIAS A
                            INNER JOIN RADIO.OBSERVACIONES_CONFIGURACION B ON A.ID = B.ID_CONVOCATORIA
                            INNER JOIN RADIO.OBSERVACIONES_CATEGORIAS C ON B.ID_CATEGORIA = C.ID_CATEGORIA;

                            SELECT ID_CATEGORIA, NOMBRE_CATEGORIA
                            FROM RADIO.OBSERVACIONES_CATEGORIAS;
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                mResult.Configuraciones = (from bp in dtSet.Tables[0].AsEnumerable().AsQueryable()
                                           select new ST_RDSObservacionConvocatoriaOtorga
                                           {
                                               ID = bp.Field<int>("ID"),
                                               NOMBRE_CONVOCATORIA = bp.Field<string>("NOMBRE_CONVOCATORIA"),
                                               CODIGO_CONVOCATORIA = bp.Field<string>("CODIGO_CONVOCATORIA"),
                                               ANIO_CONVOCATORIA = bp.Field<string>("ANIO_CONVOCATORIA"),
                                               SOL_TYPE = bp.Field<int>("SOL_TYPE"),
                                               Etapas = (from bpc in dtSet.Tables[1].AsEnumerable().AsQueryable().Where(x => x.Field<int>("ID_CONVOCATORIA") == bp.Field<int>("ID"))
                                                         select new ST_ObservacionesEtapasOtorga
                                                         {
                                                             ID_ETAPA = bpc.Field<int>("ID_ETAPA"),
                                                             ID_CONVOCATORIA = bpc.Field<int>("ID_CONVOCATORIA"),
                                                             NOMBRE_ETAPA = bpc.Field<string>("NOMBRE_ETAPA"),
                                                             FECHA_INICIO = bpc.Field<DateTime>("FECHA_INICIO").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                                             FECHA_FIN = bpc.Field<DateTime>("FECHA_FIN").ToString("yyyyMMdd", CultureInfo.InvariantCulture),
                                                             DESCRIPCION = bpc.Field<string>("DESCRIPCION"),
                                                         }).ToList(),
                                               Categorias = (from bc in dtSet.Tables[2].AsEnumerable().AsQueryable().Where(x => x.Field<int>("ID") == bp.Field<int>("ID"))
                                                             select new ST_ObservacionesCategorias
                                                             {
                                                                 ID_ETAPA = bc.Field<int>("ID"),
                                                                 ID_CATEGORIA = bc.Field<int>("ID_CATEGORIA"),
                                                                 NOMBRE_CATEGORIA = bc.Field<string>("NOMBRE_CATEGORIA"),
                                                             }).ToList(),

                                           }).ToList();

                mResult.Categorias = (from bc in dtSet.Tables[3].AsEnumerable().AsQueryable()
                                      select new ST_ObservacionesCategorias
                                      {
                                          ID_CATEGORIA = bc.Field<int>("ID_CATEGORIA"),
                                          NOMBRE_CATEGORIA = bc.Field<string>("NOMBRE_CATEGORIA"),

                                      }).ToList();

                var fechaHoy = DateTime.Today;
                foreach (var item in mResult.Configuraciones)
                {
                    item.ProcesoActivo = "Inactiva";
                    foreach (var itemEtapa in item.Etapas)
                    {
                        if (fechaHoy >= DateTime.ParseExact(itemEtapa.FECHA_INICIO, "yyyyMMdd", CultureInfo.InvariantCulture) && fechaHoy <= DateTime.ParseExact(itemEtapa.FECHA_FIN, "yyyyMMdd", CultureInfo.InvariantCulture))
                            item.ProcesoActivo = "Activa";
                    }
                }

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDConvocatorias(int IDUserWeb, ST_RDSObservacionConvocatoriaOtorga Convocatoria)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Convocatoria);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Convocatoria=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        SELECT ID, NOMBRE_CONVOCATORIA, CODIGO_CONVOCATORIA, ANIO_CONVOCATORIA, SOL_TYPE
                        FROM RADIO.OBSERVACIONES_CONVOCATORIAS
                        WHERE ID=" + Convocatoria.ID + @";

                        SELECT ID_CONFIGURACION, ID_CONVOCATORIA, ID_CATEGORIA
                        FROM RADIO.OBSERVACIONES_CONFIGURACION
                        WHERE ID_CONVOCATORIA =" + Convocatoria.ID + @";

                        SELECT ID_ETAPA, ID_CONVOCATORIA, NOMBRE_ETAPA, FECHA_INICIO, FECHA_FIN, DESCRIPCION
                        FROM RADIO.OBSERVACIONES_ETAPAS
                        WHERE ID_CONVOCATORIA =" + Convocatoria.ID + @";
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTConvocatorias = dtSet.Tables[0];
                DataTable DTConfiguracion = dtSet.Tables[1];
                DataTable DTEtapas = dtSet.Tables[2];

                foreach (DataRow dataRow in DTConfiguracion.Rows)
                {
                    dataRow.Delete();
                }

                foreach (var item in Convocatoria.Categorias)
                {
                    DataRow DR = DTConfiguracion.NewRow();
                    DR["ID_CONVOCATORIA"] = Convocatoria.ID;
                    DR["ID_CATEGORIA"] = item.ID_CATEGORIA;
                    DTConfiguracion.Rows.Add(DR);
                }

                foreach (DataRow DR in DTEtapas.Rows)
                {
                    var etapa = Convocatoria.Etapas.Find(x => x.ID_ETAPA == (int)DR["ID_ETAPA"]);
                    if (etapa == null)
                    {
                        DR.Delete();
                    }
                    else
                    {
                        if (etapa.ID_ETAPA == (int)DR["ID_ETAPA"])
                        {
                            ST_ObservacionesEtapasOtorga EtapaAnterior = new ST_ObservacionesEtapasOtorga();
                            EtapaAnterior.ID_ETAPA = (int)DR["ID_ETAPA"];
                            EtapaAnterior.NOMBRE_ETAPA = (string)DR["NOMBRE_ETAPA"];
                            EtapaAnterior.FECHA_INICIO = DR["FECHA_INICIO"].ToString();
                            EtapaAnterior.FECHA_FIN = DR["FECHA_FIN"].ToString();
                            EtapaAnterior.DESCRIPCION = DR["DESCRIPCION"].ToString();
                            EVT_DATA_OLD = Serializer.Serialize(EtapaAnterior);

                            DR["NOMBRE_ETAPA"] = etapa.NOMBRE_ETAPA;
                            DR["FECHA_INICIO"] = etapa.FECHA_INICIO;
                            DR["FECHA_FIN"] = etapa.FECHA_FIN;
                            DR["DESCRIPCION"] = etapa.DESCRIPCION;
                        }
                    }
                }

                foreach (var item in Convocatoria.Etapas)
                {
                    if (item.ID_ETAPA < 0)
                    {
                        DataRow DRNew = DTEtapas.NewRow();
                        DRNew["ID_CONVOCATORIA"] = Convocatoria.ID;
                        DRNew["NOMBRE_ETAPA"] = item.NOMBRE_ETAPA;
                        DRNew["FECHA_INICIO"] = item.FECHA_INICIO;
                        DRNew["FECHA_FIN"] = item.FECHA_FIN;
                        DRNew["DESCRIPCION"] = item.DESCRIPCION;
                        DTEtapas.Rows.Add(DRNew);
                    }
                }


                switch (Convocatoria.CRUD)
                {
                    case 1:
                        {
                            EVT_CRUD = 1;

                            DataRow DR = DTConvocatorias.NewRow();
                            DR["ID"] = Convocatoria.ID;
                            DR["NOMBRE_CONVOCATORIA"] = Convocatoria.NOMBRE_CONVOCATORIA;
                            DR["CODIGO_CONVOCATORIA"] = Convocatoria.CODIGO_CONVOCATORIA;
                            DR["ANIO_CONVOCATORIA"] = Convocatoria.ANIO_CONVOCATORIA;
                            DR["SOL_TYPE"] = Convocatoria.SOL_TYPE;

                            DTConvocatorias.Rows.Add(DR);
                        }
                        break;
                    case 3:
                        {
                            EVT_CRUD = 3;

                            DataRow DR = DTConvocatorias.Rows[0];

                            ST_RDSObservacionConvocatoriaOtorga ConvocatoriaAnterior = new ST_RDSObservacionConvocatoriaOtorga();
                            ConvocatoriaAnterior.ID = (int)DR["ID"];
                            ConvocatoriaAnterior.NOMBRE_CONVOCATORIA = (string)DR["NOMBRE_CONVOCATORIA"];
                            ConvocatoriaAnterior.CODIGO_CONVOCATORIA = (string)DR["CODIGO_CONVOCATORIA"];
                            ConvocatoriaAnterior.ANIO_CONVOCATORIA = (string)DR["ANIO_CONVOCATORIA"];
                            ConvocatoriaAnterior.SOL_TYPE = (int)DR["SOL_TYPE"];
                            EVT_DATA_OLD = Serializer.Serialize(ConvocatoriaAnterior);

                            DR["NOMBRE_CONVOCATORIA"] = Convocatoria.NOMBRE_CONVOCATORIA;
                            DR["CODIGO_CONVOCATORIA"] = Convocatoria.CODIGO_CONVOCATORIA;
                            DR["ANIO_CONVOCATORIA"] = Convocatoria.ANIO_CONVOCATORIA;
                            DR["SOL_TYPE"] = Convocatoria.SOL_TYPE;
                        }
                        break;
                    case 4:
                        {
                            EVT_CRUD = 4;

                            DataRow DR = DTConvocatorias.Rows[0];

                            ST_RDSObservacionConvocatoriaOtorga ConvocatoriaEliminar = new ST_RDSObservacionConvocatoriaOtorga();
                            ConvocatoriaEliminar.ID = (int)DR["ID"];
                            EVT_DATA_OLD = Serializer.Serialize(ConvocatoriaEliminar);

                            DR.Delete();
                        }
                        break;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, "Operación exitosa.");

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDCategoria(int IDUserWeb, ST_ObservacionesCategorias Categoria)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();

            var msg = "";
            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(Categoria);

            var sLog = "IDUserWeb=" + IDUserWeb + ", Categoria=" + EVT_DATA_NEW;

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        SELECT ID_CATEGORIA, NOMBRE_CATEGORIA
                        FROM RADIO.OBSERVACIONES_CATEGORIAS
                        WHERE ID_CATEGORIA=" + Categoria.ID_CATEGORIA + @";
                        ";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTCategorias = dtSet.Tables[0];

                switch (Categoria.CRUD)
                {
                    case 1:
                        {
                            EVT_CRUD = 1;

                            DataRow DR = DTCategorias.NewRow();
                            DR["NOMBRE_CATEGORIA"] = Categoria.NOMBRE_CATEGORIA;

                            DTCategorias.Rows.Add(DR);
                            msg = "Se creó categoría exitosamente.";
                        }
                        break;
                    case 3:
                        {
                            EVT_CRUD = 3;

                            DataRow DR = DTCategorias.Rows[0];

                            ST_ObservacionesCategorias CategoriaAnterior = new ST_ObservacionesCategorias();
                            CategoriaAnterior.ID_CATEGORIA = (int)DR["ID_CATEGORIA"];
                            CategoriaAnterior.NOMBRE_CATEGORIA = (string)DR["NOMBRE_CATEGORIA"];
                            EVT_DATA_OLD = Serializer.Serialize(CategoriaAnterior);

                            DR["NOMBRE_CATEGORIA"] = Categoria.NOMBRE_CATEGORIA;
                            msg = "Se editó categoría exitosamente.";
                        }
                        break;
                    case 4:
                        {
                            EVT_CRUD = 4;

                            DataRow DR = DTCategorias.Rows[0];

                            ST_ObservacionesCategorias CategoriaEliminar = new ST_ObservacionesCategorias();
                            CategoriaEliminar.ID_CATEGORIA = (int)DR["ID_CATEGORIA"];
                            CategoriaEliminar.NOMBRE_CATEGORIA = (string)DR["NOMBRE_CATEGORIA"];
                            EVT_DATA_OLD = Serializer.Serialize(CategoriaEliminar);

                            DR.Delete();
                            msg = "Se eliminó categoría exitosamente.";
                        }
                        break;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, msg);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public List<ST_RDSObsPlt> GetObservacionesPlantillas()
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSObsPlt> mResult = new List<ST_RDSObsPlt>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                        SELECT ID_PLANTILLA, NOMBRE, PLT_HTML
                        FROM RADIO.OBSERVACIONES_PLANTILLAS";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSObsPlt
                           {
                               ID_PLANTILLA = bp.Field<string>("ID_PLANTILLA"),
                               NOMBRE = bp.Field<string>("NOMBRE"),
                               PLT_HTML = bp.Field<string>("PLT_HTML"),
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ActionInfo CRUDObservacionesPlantillas(int IDUserWeb, ST_RDSObsPlt ObsPlt)
        {
            csData cData = null;
            var Serializer = new JavaScriptSerializer_TESExtension();
            int EVT_CRUD = -1;
            string EVT_DATA_OLD = "";
            string EVT_DATA_NEW = Serializer.Serialize(ObsPlt);
            string mensaje = "";
            var sLog = "IDUserWeb=" + IDUserWeb + ", ObsPlt=" + EVT_DATA_NEW;


            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "!ConectedOk", strConn, sLog);
                    return new ActionInfo(-1, "Error en el servidor");
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT ID_PLANTILLA, NOMBRE, PLT_HTML, ARCHIVO
                        FROM RADIO.OBSERVACIONES_PLANTILLAS 
                        WHERE ID_PLANTILLA='" + ObsPlt.ID_PLANTILLA + "'";


                var dtSet = cData.ObtenerDataSet(sSQL);

                DataTable DTObsPlt = dtSet.Tables[0];

                switch (ObsPlt.PLT_CRUD)
                {
                    case 1:
                        {
                            if (DTObsPlt.Rows.Count != 0)
                                return new ActionInfo(2, "El identificador se encuentra en uso");
                            EVT_CRUD = 1;
                            ObsPlt.ID_PLANTILLA = ObsPlt.ID_PLANTILLA;

                            RDSObservaciones RDSObservacion = new RDSObservaciones();

                            var bytes = RDSObservacion.GenerarWordPlantilla(ObsPlt.PLT_HTML);

                            DataRow DRT = DTObsPlt.NewRow();
                            DRT["ID_PLANTILLA"] = ObsPlt.ID_PLANTILLA;
                            DRT["NOMBRE"] = ObsPlt.NOMBRE;
                            DRT["PLT_HTML"] = ObsPlt.PLT_HTML;
                            DRT["ARCHIVO"] = bytes;
                            DTObsPlt.Rows.Add(DRT);
                            mensaje = "Se creó la plantilla con exito!";
                        }
                        break;

                    case 3:
                        {
                            if (DTObsPlt.Rows.Count != 1)
                                return new ActionInfo(2, "El identificador no existe");
                            EVT_CRUD = 3;
                            RDSObservaciones RDSObservacion = new RDSObservaciones();
                            var bytes = RDSObservacion.GenerarWordPlantilla(ObsPlt.PLT_HTML);

                            DataRow DRT = DTObsPlt.Rows[0];

                            ST_RDSObsPlt ObsPltOld = new ST_RDSObsPlt();
                            ObsPltOld.ID_PLANTILLA = (string)DRT["ID_PLANTILLA"];
                            ObsPltOld.NOMBRE = (string)DRT["NOMBRE"];
                            ObsPltOld.PLT_HTML = (string)DRT["PLT_HTML"];
                            ObsPltOld.PLT_CRUD = 3;
                            EVT_DATA_OLD = Serializer.Serialize(ObsPltOld);

                            DRT["NOMBRE"] = ObsPlt.NOMBRE;
                            DRT["PLT_HTML"] = ObsPlt.PLT_HTML;
                            DRT["ARCHIVO"] = bytes;
                            mensaje = "Se modificó la plantilla con exito!";
                        }
                        break;

                    case 4:
                        {
                            if (DTObsPlt.Rows.Count != 1)
                                return new ActionInfo(2, "El identificador no existe");
                            EVT_CRUD = 4;

                            DataRow DRT = DTObsPlt.Rows[0];

                            ST_RDSObsPlt ObsPltOld = new ST_RDSObsPlt();
                            ObsPltOld.ID_PLANTILLA = (string)DRT["ID_PLANTILLA"];
                            ObsPltOld.NOMBRE = (string)DRT["NOMBRE"];
                            ObsPltOld.PLT_HTML = (string)DRT["PLT_HTML"];
                            ObsPltOld.PLT_CRUD = 4;
                            EVT_DATA_OLD = Serializer.Serialize(ObsPltOld);

                            DRT.Delete();
                            mensaje = "Se eliminó la plantilla con exito!";
                        }
                        break;
                }

                bool Ret = cData.InsertDataSet(sSQL, dtSet);

                if (!Ret)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed", sLog);
                    return new ActionInfo(2, "Error en la operación");
                }

                cData.InsertPortalEvent(IDUserWeb, System.Reflection.MethodBase.GetCurrentMethod().Name, EVT_CRUD, EVT_DATA_OLD, EVT_DATA_NEW);

                return new ActionInfo(1, mensaje);

            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e", e.Message, sLog);
                return new ActionInfo(-1, "Error en el servidor");
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSObsCombos GetObservacionesCombos()
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSObsCombos mResult = new ST_RDSObsCombos();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"SELECT TXT_ID, TXT_TEXT
                        FROM RADIO.TEXTS
                        WHERE TXT_ID In ('COMBO_INFORMACION_OBSERVACIONES', 'COMBO_INFORMACION_OBSERVACIONES_CONVOCATORIA', 'COMBO_INFORMACION_OBSERVACIONES_ETAPA')
                        ";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult.InformacionObservaciones = dt.Where(bp => bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_OBSERVACIONES")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionObsConvocatoria = dt.Where(bp => bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_OBSERVACIONES_CONVOCATORIA")).FirstOrDefault().Field<string>("TXT_TEXT");
                mResult.InformacionObsEtapa = dt.Where(bp => bp.Field<string>("TXT_ID").Equals("COMBO_INFORMACION_OBSERVACIONES_ETAPA")).FirstOrDefault().Field<string>("TXT_TEXT");

                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public ST_RDSObsPlt ReplaceObservacionesPlantilla(ST_RDSObsPlt ObsPlt,string idConvocatoria)
        {
            int IDUserWeb = 1;
            csData cData = null;

            ST_RDSObsPlt mResult = new ST_RDSObsPlt();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = cData.TraerValor("Select TXT_TEXT From RADIO.TEXTS Where TXT_ID='OBSERVACIONES_SQL'", "OBSERVACIONES_SQL");
                sSQL = sSQL.Replace("{{IdConvocatoria}}", idConvocatoria);

                var dtSet = cData.ObtenerDataSet(sSQL);

                RDSDynamicObservacion DynamicObservacion = new RDSDynamicObservacion();

                foreach (DataTable Tabla in dtSet.Tables)
                {
                    if (Tabla.Columns[0].ColumnName.StartsWith("{{Lista "))
                        DynamicObservacion.AddKey2(Tabla.Columns[0].ColumnName, Tabla.Rows.Count.ToString());

                    foreach (DataColumn Columna in Tabla.Columns)
                    {
                        if (Tabla.Columns[0].ColumnName.StartsWith("{{Lista "))
                        {
                            if (Columna.ColumnName == Tabla.Columns[0].ColumnName)
                                continue;
                        }

                        int Contador = 0;
                        foreach (DataRow Fila in Tabla.Rows)
                        {
                            if (Tabla.Columns[0].ColumnName.StartsWith("{{Lista "))
                                DynamicObservacion.AddKey2(Columna.ColumnName + "[" + Contador + "]", Fila[Columna].ToString());
                            else
                                DynamicObservacion.AddKey2(Columna.ColumnName, Fila[Columna].ToString());

                            Contador++;
                        }
                    }
                }

                ObsPlt.PLT_HTML = DynamicObservacion.ProcesarObservacionesListas(ObsPlt.PLT_HTML);

                foreach (var Item in DynamicObservacion.TXT)
                {
                    ObsPlt.PLT_HTML = ObsPlt.PLT_HTML.Replace(Item.Key, Item.Value);
                }

                return ObsPlt;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public byte[] GenerarInformeRespuestasObservaciones(int IDUserWeb, ST_RDSObservacionConvocatoriaOtorga Convocatoria)
        {
            RDSObservaciones RDSObservaciones = new RDSObservaciones();
            ST_RDSObsPlt plantilla = ReplaceObservacionesPlantilla(GetPantillaObservacion("PLT_DOC_INFORME"), Convocatoria.ID.ToString());
            byte[] bytes = RDSObservaciones.GenerarInformeRespuestasObservaciones(plantilla.PLT_HTML);

            return bytes;
        }

        private ST_RDSObsPlt GetPantillaObservacion(string IdPlantilla)
        {
            int IDUserWeb = 1;
            csData cData = null;
            ST_RDSObsPlt mResult = new ST_RDSObsPlt();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @"
                         SELECT ID_PLANTILLA, NOMBRE, PLT_HTML
                         FROM RADIO.OBSERVACIONES_PLANTILLAS 
                         WHERE ID_PLANTILLA ='" + IdPlantilla + @"';
                        ";


                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSObsPlt
                           {
                               ID_PLANTILLA = bp.Field<string>("ID_PLANTILLA"),
                               NOMBRE = bp.Field<string>("NOMBRE"),
                               PLT_HTML = bp.Field<string>("PLT_HTML")
                           }).ToList().FirstOrDefault();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        #endregion

        #region Alfa

        #region Radicar

        private string AlfaRadicarTramiteRetry(ref AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws, string NUI, string Nombre, string Direccion, string Ciudad, string Telefono1, string Telefono2, string Email1, string Email2, string Fax, string Naturaleza, string Dependencia, string Expediente, string Detalle, string NombreArchivo, ref MemoryStream mem)
        {
            string xmlResult = "";
            var tries = 3;
            while (true)
            {
                try
                {
                    xmlResult = ws.Radicar_Tramite(NUI, Nombre, Direccion, Ciudad, Telefono1, Telefono2, Email1, Email2, Fax, Naturaleza, Dependencia, Expediente, Detalle, NombreArchivo, mem.GetBuffer());
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return xmlResult;
        }

        private ResultadoRadicacionAlfa AlfaRadicarConcesionarioToMinticSolicitar(ref ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            
            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SOLICITUD", "Radicado solicitud", "Concesionario");

            string xmlResult = AlfaRadicarTramiteRetry(ref ws,
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME, 
                "", 
                "11001", 
                "No disponible", 
                "No disponible",
                Solicitud.Expediente.Operador.CUS_EMAIL,
                Solicitud.Contacto.CONT_EMAIL,
                "",
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                DynamicSolicitud.GetHeaderDetail("ALFA_RAD_SOLICITUD"),
                DynamicSolicitud.GetHeaderFile("ALFA_RAD_SOLICITUD"),
                ref mem
                );

            ResultadoRadicacionAlfa resultado = ResultadoRadicacionAlfa.CrearResultadoRadicacionAlfa(xmlResult);

            if (resultado.CodigoError != 0)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", resultado.MensajeError + " - Error del WebService de alfa al intentar radicar"));
            }

            //Adjunte los documentos asociados
            foreach (var File in Files)
            {
                try
                {
                    if (!File.NAME.EndsWith(".mp3"))
                    {
                        string ResultSendFile = ws.AdjuntarImgRad(resultado.CodigoRadicado, File.NAME, Base64ToByteArray(File.DATA));
                    }
                }
                catch (Exception ex)
                {
                    // Si ocurre algún error al adjuntar el archivo, agregar el mensaje de error a la lista de errores del resultado
                    string mensajeError = string.Format("Ocurrió el siguiente error al adjuntar el archivo '{}' en AlfaNet: {1}", File.NAME, ex.Message);
                    resultado.AddErrorAlEnviarArchivo(mensajeError);
                }
            }
            return resultado;

        }

        private ResultadoRadicacionAlfa AlfaRadicarConcesionarioToMinticSubsanar(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string Registro = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Registro).Last().DOC_NUMBER;

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            DynamicSolicitud.AddKey("REGISTRO", Registro);

            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_SUBSANAR", "Radicado subsanación solicitud", "Concesionario");

            string xmlResult = AlfaRadicarTramiteRetry(ref ws,
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                "",
                "11001",
                "No disponible",
                "No disponible",
                Solicitud.Expediente.Operador.CUS_EMAIL,
                Solicitud.Contacto.CONT_EMAIL,
                "",
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                DynamicSolicitud.GetHeaderDetail("ALFA_RAD_SUBSANAR"),
                DynamicSolicitud.GetHeaderFile("ALFA_RAD_SUBSANAR"),
                ref mem
                );

            ResultadoRadicacionAlfa resultado = ResultadoRadicacionAlfa.CrearResultadoRadicacionAlfa(xmlResult);

            if (resultado.CodigoError != 0)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", resultado.MensajeError + " - Error del WebService de alfa al intentar radicar"));
            }

            //Adjunte los documentos asociados
            foreach (var File in Files)
            {
                try
                {
                    string ResultSendFile = ws.AdjuntarImgRad(resultado.CodigoRadicado, File.NAME, Base64ToByteArray(File.DATA));
                }
                catch (Exception ex)
                {
                    // Si ocurre algún error al adjuntar el archivo, agregar el mensaje de error a la lista de errores del resultado
                    string mensajeError = string.Format("Ocurrió el siguiente error al adjuntar el archivo '{}' en AlfaNet: {1}", File.NAME, ex.Message);
                    resultado.AddErrorAlEnviarArchivo(mensajeError);
                }
            }
            return resultado;

        }

        private ResultadoRadicacionAlfa AlfaRadicarConcesionarioToMinticCancelar(ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_CANCELAR", "Radicado cancelación solicitud", "Concesionario");

            string xmlResult = AlfaRadicarTramiteRetry(ref ws,
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                "",
                "11001",
                "No disponible",
                "No disponible",
                Solicitud.Expediente.Operador.CUS_EMAIL,
                Solicitud.Contacto.CONT_EMAIL,
                "",
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                DynamicSolicitud.GetHeaderDetail("ALFA_RAD_CANCELAR"),
                DynamicSolicitud.GetHeaderFile("ALFA_RAD_CANCELAR"),
                ref mem
                );

            ResultadoRadicacionAlfa resultado = ResultadoRadicacionAlfa.CrearResultadoRadicacionAlfa(xmlResult);

            if (resultado.CodigoError != 0)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", resultado.MensajeError + " - Error del WebService de alfa al intentar radicar"));
            }


            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();
            ListaRadicados.Add(resultado.CodigoRadicado);

            ArchivarDocumento(ListaRadicados.ToArray(), Helper.TXT["ALFA_SERIE_DOCUMENTAL"], Helper.TXT["ALFA_USER"], Helper.TXT["ALFA_PASSWORD"]);

            return resultado;

        }

        private ResultadoRadicacionAlfa AlfaRadicarConcesionarioToMinticAplazar(ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {

            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            RDSRadicadosPDF RDSRadicados = new RDSRadicadosPDF(ref DynamicSolicitud);
            mem = RDSRadicados.GenerarRadicado("ALFA_RAD_APLAZAR", "Radicado aplazamiento requerimiento de solicitud", "Concesionario");

            string xmlResult = AlfaRadicarTramiteRetry(ref ws,
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                "",
                "11001",
                "No disponible",
                "No disponible",
                Solicitud.Expediente.Operador.CUS_EMAIL,
                Solicitud.Contacto.CONT_EMAIL,
                "",
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                DynamicSolicitud.GetHeaderDetail("ALFA_RAD_CANCELAR"),
                DynamicSolicitud.GetHeaderFile("ALFA_RAD_CANCELAR"),
                ref mem
                );

            ResultadoRadicacionAlfa resultado = ResultadoRadicacionAlfa.CrearResultadoRadicacionAlfa(xmlResult);

            if (resultado.CodigoError != 0)
            {
                throw new ApplicationException(String.Format("{0}: {1}", "El servicio no respondio de forma adecuada", resultado.MensajeError + " - Error del WebService de alfa al intentar radicar"));
            }

            return resultado;

        }

        #endregion

        #region Registrar

        private string AlfaRegistrarTramiteRetry(ref AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws, string DependenciaRemite, string CodDestino, string NomDestino, string WFTipo, string[] RadFuente, string Expediente, string Naturaleza, string SerieDocumental, string Detalle, string NombreArchivo, byte[] Archivo, string MetododeEnvio, string guia, string archivaren, string anexos, string copiar, string user, string password)
        {
            string xmlResult = "";
            var tries = 3;
            while (true)
            {
                try
                {
                    xmlResult = ws.Registrar_Tramite(DependenciaRemite, CodDestino, NomDestino, WFTipo, RadFuente, Expediente, Naturaleza, SerieDocumental, Detalle, NombreArchivo, Archivo, MetododeEnvio, guia, archivaren, anexos, copiar, user, password);
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return xmlResult;
        }

        private string AlfaRegistrarAneToMintic(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY, ref MemoryStream mem)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string registroNumber = "";

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            string xmlResult = AlfaRegistrarTramiteRetry(ref ws,
                Helper.TXT["ALFA_DEPENDENCIA"],
                Helper.TXT["MINTIC_CODIGO_DESTINO"],
                Helper.TXT["MINTIC_NAME"],
                Helper.TXT["ALFA_INTERNO"],
                ListaRadicados.ToArray(),
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                DynamicSolicitud.GetHeaderDetail(ALFA_REG_KEY),
                DynamicSolicitud.GetHeaderFile(ALFA_REG_KEY),
                mem.GetBuffer(),
                Helper.TXT["ALFA_METODO_ENVIO"],
                "",
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                "",
                "",
                Helper.TXT["ALFA_USER"],
                Helper.TXT["ALFA_PASSWORD"]
            );

            registroNumber = GetRegistroNumber(xmlResult);

            return registroNumber;
        }

        private string AlfaRegistrarAneToConcesionarioRequerir(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string registroNumber = "";
            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            string xmlResult = AlfaRegistrarTramiteRetry(ref ws,
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                Helper.TXT["ALFA_EXTERNO"],
                ListaRadicados.ToArray(),
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                DynamicSolicitud.GetHeaderDetail("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR"),
                DynamicSolicitud.GetHeaderFile("ALFA_REG_ANE_TO_CONCESIONARIO_TECH_REQUERIR"),
                mem.GetBuffer(),
                Helper.TXT["ALFA_METODO_ENVIO"],
                "",
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                "",
                "",
                Helper.TXT["ALFA_USER"],
                Helper.TXT["ALFA_PASSWORD"]
            );

            registroNumber = GetRegistroNumber(xmlResult);

            return registroNumber;

        }

        private string AlfaRegistrarMinticToAne(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY, ref MemoryStream mem)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");
            string registroNumber = "";
            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            string xmlResult = AlfaRegistrarTramiteRetry(ref ws,
                Helper.TXT["ALFA_DEPENDENCIA"],
                Helper.TXT["ANE_CODIGO_DESTINO"],
                Helper.TXT["ANE_NAME"],
                Helper.TXT["ALFA_INTERNO"],
                ListaRadicados.ToArray(),
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                DynamicSolicitud.GetHeaderDetail(ALFA_REG_KEY),
                DynamicSolicitud.GetHeaderFile(ALFA_REG_KEY),
                mem.GetBuffer(),
                Helper.TXT["ALFA_METODO_ENVIO"],
                "",
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                "",
                "",
                Helper.TXT["ALFA_USER"],
                Helper.TXT["ALFA_PASSWORD"]
            );

            registroNumber = GetRegistroNumber(xmlResult);
            return registroNumber;
        }

        private string AlfaRegistrarMinticToConcesionarioRequerir(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, ref MemoryStream mem)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string registroNumber = "";
            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            string xmlResult = AlfaRegistrarTramiteRetry(ref ws,
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                Helper.TXT["ALFA_EXTERNO"],
                ListaRadicados.ToArray(),
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                DynamicSolicitud.GetHeaderDetail("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_REQUERIR"),
                DynamicSolicitud.GetHeaderFile("ALFA_REG_MINTIC_TO_CONCESIONARIO_ADMIN_REQUERIR"),
                mem.GetBuffer(),
                Helper.TXT["ALFA_METODO_ENVIO"],
                "",
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                "",
                "",
                Helper.TXT["ALFA_USER"],
                Helper.TXT["ALFA_PASSWORD"]
            );

            registroNumber = GetRegistroNumber(xmlResult);

            return registroNumber;
        }

        private string AlfaRegistrarMinticToConcesionario(ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, string ALFA_REG_KEY)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string registroNumber = "";

            RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
            List<string> ListaRadicados = Solicitud.Documents.Where(bp => bp.DOC_CLASS == (int)DOC_CLASS.Radicado).Select(bp => bp.DOC_NUMBER).ToList();

            string xmlResult = AlfaRegistrarTramiteRetry(ref ws,
                Helper.TXT["ALFA_DEPENDENCIA"],
                Solicitud.Expediente.Operador.CUS_IDENT + Solicitud.Expediente.Operador.CUS_BRANCH,
                Solicitud.Expediente.Operador.CUS_NAME,
                Helper.TXT["ALFA_EXTERNO"],
                ListaRadicados.ToArray(),
                Solicitud.Expediente.SERV_NUMBER.ToString(),
                Helper.TXT["ALFA_NATURALEZA"],
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                DynamicSolicitud.GetHeaderDetail(ALFA_REG_KEY),
                "",
                null,
                Helper.TXT["ALFA_METODO_ENVIO"],
                "",
                Helper.TXT["ALFA_SERIE_DOCUMENTAL"],
                "",
                "",
                Helper.TXT["ALFA_USER"],
                Helper.TXT["ALFA_PASSWORD"]
            );

            registroNumber = GetRegistroNumber(xmlResult);

            ArchivarDocumento(ListaRadicados.ToArray(), Helper.TXT["ALFA_SERIE_DOCUMENTAL"], Helper.TXT["ALFA_USER"], Helper.TXT["ALFA_PASSWORD"]);
               
            return registroNumber;
        }

        #endregion

        #region Auxiliares

        private string[] ConsultaComunicadosXTramiteRetry(ref AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws, string vDocumentoCodigo, string vExpedienteCodigo)
        {
            string[] Res;
            var tries = 3;
            while (true)
            {
                try
                {
                    Res = ws.ConsultaComunicadosXTramite(vDocumentoCodigo, vExpedienteCodigo);
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return Res;
        }

        private string AlfaAdjuntarImgRegRetry(ref AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws, string Registro, string NombreArchivo, ref MemoryStream mem, string user, string password)
        {
            string xmlResult = "";
            var tries = 3;
            while (true)
            {
                try
                {
                    xmlResult = ws.AdjuntarImgReg(Registro, NombreArchivo, mem.GetBuffer(), user, password);

                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return xmlResult;
        }

        private string AlfaArchivarDocumentoRetry(ref AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws, string[] DocumentoCodigo, string SerieCodigo, string user, string password)
        {
            string xmlResult = "";
            //var tries = 3;
            //while (true)
            //{
            //    try
            //    {
            //        xmlResult = ws.ArchivarDocumento(DocumentoCodigo, SerieCodigo, user, password);
            //        break;
            //    }
            //    catch
            //    {
            //        if (--tries == 0)
            //            throw;
            //        Thread.Sleep(1000);
            //    }
            //}

            return xmlResult;
        }

        private ResultadoComunicadosXTramiteAlfa AlfaComunicadosXTramite(string vDocumentoCodigo, string vExpedienteCodigo)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string[] Res = ConsultaComunicadosXTramiteRetry(ref ws, vDocumentoCodigo, vExpedienteCodigo).ToArray();

            ResultadoComunicadosXTramiteAlfa resultado = ResultadoComunicadosXTramiteAlfa.CrearResultadoComunicadosXTramiteAlfa(Res);

            return resultado;

        }

        private string AlfaRegisterAddDocument(string registroNumber, string nombreArchivo, ref ST_Helper Helper, ref MemoryStream mem)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string sRet = AlfaAdjuntarImgRegRetry(ref ws, registroNumber, nombreArchivo, ref mem, Helper.TXT["ALFA_USER"], Helper.TXT["ALFA_PASSWORD"]);
            return sRet;
        }

        private bool ArchivarDocumento(string[] ListaRadicados, string SerieDocumental, string user, string password)
        {
            AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient ws = new AlfaWS.EsbOtiInterOpAlfaNetSagePortTypeClient("EsbOtiInterOpAlfaNetSageHttpSoap11Endpoint");

            string XmlResult = null;

            XmlResult = AlfaArchivarDocumentoRetry(ref ws, ListaRadicados, SerieDocumental, user, password);

            bool respuesta = GetFinalizadoExitosamente(XmlResult);
            return respuesta;
        }

        private static bool GetFinalizadoExitosamente(string alfaResult)
        {
            if (string.IsNullOrEmpty(alfaResult)) return false;
            return alfaResult.Contains("Proceso finalizado exitosamente");
        }

        #endregion

        #endregion

        #region Seven

        private string SevenEstadoCarteraRetry(ref SevenWS.ServiceInteropClient ws, string NitOperador)
        {
            string xmlResult = "";
            var tries = 3;
            while (true)
            {
                try
                {
                    xmlResult = ws.EstadoCartera(NitOperador);
                    break;
                }
                catch
                {
                    if (--tries == 0)
                        throw;
                    Thread.Sleep(1000);
                }
            }

            return xmlResult;
        }

        public ST_RDSResult VerificarCarteraAlDiaInterno(string CUS_IDENT)
        {

            int SGEConsultarCartera = GetIntValue(ConfigurationManager.AppSettings["SGEConsultarCartera"], 1);

            ST_RDSResult mResult = new ST_RDSResult();
            if (SGEConsultarCartera == 0)
            {
                mResult.Estado = true;
                return mResult;
            }
            else
            { 
                SevenWS.ServiceInteropClient ws = new SevenWS.ServiceInteropClient();
                string respuestaCartera = SevenEstadoCarteraRetry(ref ws, CUS_IDENT);
                if (respuestaCartera.Contains("NO posee obligaciones vencidas"))
                {
                    mResult.Estado = true;
                    return mResult;
                }
                else if (respuestaCartera.Contains("SI posee obligaciones vencidas"))
                {
                    mResult.Estado = false;
                    return mResult;
                }
                else if (respuestaCartera.Contains("NIT no existe"))
                {
                    mResult.Estado = true;
                    return mResult;
                    //mResult.Error = true;
                    //mResult.ErrorMessage = "Seven responde: NIT no existe.";
                    //return mResult;
                }
                else if (respuestaCartera.Contains("No es posible entregar resultados sobre la consulta"))
                {
                    mResult.Error = true;
                    mResult.ErrorMessage = "Seven responde: No es posible entregar resultados sobre la consulta.";
                    return mResult;
                }
                else
                {
                    mResult.Error = true;
                    mResult.ErrorMessage = "Seven responde: " + respuestaCartera;
                    return mResult;
                }
            }
        }

        public ST_RDSResult VerificarCarteraAlDia(string CUS_IDENT)
        {

            ST_RDSResult mResult = new ST_RDSResult();
         
            SevenWS.ServiceInteropClient ws = new SevenWS.ServiceInteropClient();
            string respuestaCartera = SevenEstadoCarteraRetry(ref ws, CUS_IDENT);
            if (respuestaCartera.Contains("NO posee obligaciones vencidas"))
            {
                mResult.Estado = true;
                return mResult;
            }
            else if (respuestaCartera.Contains("SI posee obligaciones vencidas"))
            {
                mResult.Estado = false;
                return mResult;
            }
            else if (respuestaCartera.Contains("NIT no existe"))
            {
                mResult.Estado = true;
                return mResult;
                //mResult.Error = true;
                //mResult.ErrorMessage = "Seven responde: NIT no existe.";
                //return mResult;
            }
            else if (respuestaCartera.Contains("No es posible entregar resultados sobre la consulta"))
            {
                mResult.Error = true;
                mResult.ErrorMessage = "Seven responde: No es posible entregar resultados sobre la consulta.";
                return mResult;
            }
            else
            {
                mResult.Error = true;
                mResult.ErrorMessage = "Seven responde: " + respuestaCartera;
                return mResult;
            }
            
        }

        #endregion

        #region Correos

        private bool EnviarCorreo(MAIL_TYPE TipoCorreo, ref ST_RDSSolicitud Solicitud, ref ST_Helper Helper, int ROLE=-1)
        {
            bool SendOK = true;
            string MAIL_KEY_NAME = "";

            try
            {
                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                ST_RDSEmail Correo = new ST_RDSEmail();

                switch (TipoCorreo)
                {
                    case MAIL_TYPE.REGISTRO_USUARIO:
                        MAIL_KEY_NAME = "MAIL_REGISTRO_USUARIO";
                        break;
                    case MAIL_TYPE.ASIGNACION_CANAL:
                        MAIL_KEY_NAME = "MAIL_ASIGNACION_CANAL";
                        break;
                    case MAIL_TYPE.RESET_PASSWORD:
                        MAIL_KEY_NAME = "MAIL_RESET_PASSWORD";
                        break;
                    case MAIL_TYPE.MINTIC_SOL_ASSIGNED:
                        MAIL_KEY_NAME = "MAIL_MINTIC_SOL_ASSIGNED";
                        break;
                    case MAIL_TYPE.ANE_SOL_ASSIGNED:
                        MAIL_KEY_NAME = "MAIL_ANE_SOL_ASSIGNED";
                        break;
                    case MAIL_TYPE.MINTIC_SOL_SUBSANADA:
                        MAIL_KEY_NAME = "MAIL_MINTIC_SOL_SUBSANADA";
                        break;
                    case MAIL_TYPE.MINTIC_SOL_APLAZADA:
                        MAIL_KEY_NAME = "MAIL_MINTIC_SOL_APLAZADA";
                        break;
                    case MAIL_TYPE.MINTIC_SOL_CANCELADA:
                        MAIL_KEY_NAME = "MAIL_MINTIC_SOL_CANCELADA";
                        break;
                    case MAIL_TYPE.MINTIC_SOL_DESISTIDA:
                        MAIL_KEY_NAME = "MAIL_MINTIC_SOL_DESISTIDA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_CREADA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_CREADA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_REQUERIDA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_REQUERIDA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_SUBSANADA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_SUBSANADA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_APLAZADA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_APLAZADA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_CANCELADA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_CANCELADA";
                        break;
                    case MAIL_TYPE.CONCESIONARIO_SOL_DESISTIDA:
                        MAIL_KEY_NAME = "MAIL_CONCESIONARIO_SOL_DESISTIDA";
                        break;
                    case MAIL_TYPE.ANALISIS_ADMINISTRATIVO_DEVUELTO:
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Coordinador:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_ADMINISTRATIVO_DEVUELTO_COORDINADOR";
                                break;
                            case ROLES.Subdirector:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_ADMINISTRATIVO_DEVUELTO_SUBDIRECTOR";
                                break;
                        }
                        break;
                    case MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO:
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Revisor:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_TECNICO_DEVUELTO_REVISOR";
                                break;
                            case ROLES.Coordinador:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_TECNICO_DEVUELTO_COORDINADOR";
                                break;
                            case ROLES.Subdirector:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_TECNICO_DEVUELTO_SUBDIRECTOR";
                                break;
                        }
                        break;
                    case MAIL_TYPE.ANALISIS_RESOLUCION_DEVUELTO:
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Revisor:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_RESOLUCION_DEVUELTO_REVISOR";
                                break;
                            case ROLES.Coordinador:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_RESOLUCION_DEVUELTO_COORDINADOR";
                                break;
                            case ROLES.Subdirector:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_RESOLUCION_DEVUELTO_SUBDIRECTOR";
                                break;
                            case ROLES.Director:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_RESOLUCION_DEVUELTO_DIRECTOR";
                                break;
                            case ROLES.Asesor:
                                MAIL_KEY_NAME = "MAIL_ANALISIS_RESOLUCION_DEVUELTO_ASESOR";
                                break;

                        }
                        break;
                    case MAIL_TYPE.ANALISIS_TECNICO_DEVUELTO_TO_ANE:
                        MAIL_KEY_NAME = "MAIL_ANALISIS_TECNICO_DEVUELTO_TO_ANE";
                        break;
                    case MAIL_TYPE.ANALISIS_TECNICO_FINALIZADO:
                        MAIL_KEY_NAME = "MAIL_ANALISIS_TECNICO_FINALIZADO";
                        break;

                }

                AddCuentasDestino(ref Correo, DynamicSolicitud.GetHeaderAddress(MAIL_KEY_NAME));
                Correo.asuntoCorreo = DynamicSolicitud.GetHeaderSubject(MAIL_KEY_NAME);
                Correo.mensajeCorreo.AppendLine(DynamicSolicitud.GetBody(MAIL_KEY_NAME));

                if (Correo.cuentasDestino.Any(bp => !string.IsNullOrEmpty(bp)))
                {
                    string[] CuentasCC = GetCuentas_CC_BCC(DynamicSolicitud.GetKeyTextValue("MAIL_CARBON_COPY"));
                    string[] CuentasBCC = GetCuentas_CC_BCC(DynamicSolicitud.GetKeyTextValue("MAIL_BLIND_CARBON_COPY"));

                    SendOK = ReadPopMail.MailHelper.Correo.SendMessage(Correo.cuentasDestino.Where(bp => !string.IsNullOrEmpty(bp)).ToArray(), CuentasCC, CuentasBCC, Correo.asuntoCorreo, "", Correo.mensajeCorreo.ToString());
                }
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
            return SendOK;
        }

        private bool EnviarCorreoObs(string MAIL_KEY_NAME, ref RDSDynamicObservacion DynamicObservacion, int ROLE = -1)
        {
            bool SendOK = true;
            
            try
            {                
                ST_RDSEmail Correo = new ST_RDSEmail();               

                AddCuentasDestino(ref Correo, DynamicObservacion.GetHeaderAddress(MAIL_KEY_NAME));
                Correo.asuntoCorreo = DynamicObservacion.GetHeaderSubject(MAIL_KEY_NAME);
                Correo.mensajeCorreo.AppendLine(DynamicObservacion.GetBody(MAIL_KEY_NAME));

                if (Correo.cuentasDestino.Any(bp => !string.IsNullOrEmpty(bp)))
                {
                    string[] CuentasCC = GetCuentas_CC_BCC(DynamicObservacion.GetKeyTextValue("MAIL_CARBON_COPY"));
                    string[] CuentasBCC = GetCuentas_CC_BCC(DynamicObservacion.GetKeyTextValue("MAIL_BLIND_CARBON_COPY"));

                    SendOK = ReadPopMail.MailHelper.Correo.SendMessage(Correo.cuentasDestino.Where(bp => !string.IsNullOrEmpty(bp)).ToArray(), CuentasCC, CuentasBCC, Correo.asuntoCorreo, "", Correo.mensajeCorreo.ToString());
                }
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
            return SendOK;
        }

        private ST_RDSEmail GetCorreoReasign(ref RDSDynamicSolicitud DynamicSolicitud, ref ST_RDSUsuario TO_USR, ref ST_RDSUsuario FM_USR, ref ST_RDSReasignUp Reasign, string MAIL_KEY_NAME)
        {
            ST_RDSEmail Correo = new ST_RDSEmail();

            DynamicSolicitud.AddKey("FROM.USR_EMAIL", FM_USR.USR_EMAIL);
            DynamicSolicitud.AddKey("FROM.USR_NAME", FM_USR.USR_NAME);
            DynamicSolicitud.AddKey("FROM.USR_ROLE", GetUserRole(FM_USR.USR_ROLE));
            DynamicSolicitud.AddKey("FROM.USR_COMMENT", Reasign.COMMENT);
            DynamicSolicitud.AddKey("TO.USR_EMAIL", TO_USR.USR_EMAIL);
            DynamicSolicitud.AddKey("TO.USR_NAME", TO_USR.USR_NAME);
            DynamicSolicitud.AddKey("TO.USR_ROLE", GetUserRole(TO_USR.USR_ROLE));

            AddCuentasDestino(ref Correo, DynamicSolicitud.GetHeaderAddress(MAIL_KEY_NAME));
            Correo.asuntoCorreo = DynamicSolicitud.GetHeaderSubject(MAIL_KEY_NAME);
            Correo.mensajeCorreo.AppendLine(DynamicSolicitud.GetBody(MAIL_KEY_NAME));

            return Correo;
        }

        private bool EnviarCorreoReasign(ref ST_RDSSolicitud Solicitud, ST_RDSReasignUp Reasign, ref List<ST_RDSUsuario> Usuarios, ref ST_Helper Helper)
        {
            bool SendOK = true;
            List<ST_RDSEmail> Correos = new List<ST_RDSEmail>();
            try
            {
                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);

                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                            {
                                ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                TO_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                FM_USR.USR_ROLE = Reasign.ColumnROLE;
                                Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_SOLICITAR"));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Rechazar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_RECHAZAR"));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Aprobar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_APROBAR"));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_ASIGNAR"));
                                }
                            }
                        }
                        break;

                    case REASIGN_TYPE.Reasignar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_DESASIGNAR"));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasign(ref DynamicSolicitud, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_ASIGNAR"));
                                }
                            }
                        }
                        break;
                }

                string[] CuentasCC = GetCuentas_CC_BCC(DynamicSolicitud.GetKeyTextValue("MAIL_CARBON_COPY"));
                string[] CuentasBCC = GetCuentas_CC_BCC(DynamicSolicitud.GetKeyTextValue("MAIL_BLIND_CARBON_COPY"));

                foreach (var Correo in Correos)
                {
                    if (Correo.cuentasDestino.Any(bp => !string.IsNullOrEmpty(bp)))
                    {
                        SendOK = ReadPopMail.MailHelper.Correo.SendMessage(Correo.cuentasDestino.Where(bp => !string.IsNullOrEmpty(bp)).ToArray(), CuentasCC, CuentasBCC, Correo.asuntoCorreo, "", Correo.mensajeCorreo.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
            return true;
        }

        private ST_RDSEmail GetCorreoReasignObs(ref RDSDynamicObservacion DynamicObservacion, ref ST_RDSUsuario TO_USR, ref ST_RDSUsuario FM_USR, ref ST_RDSReasignUp Reasign, string MAIL_KEY_NAME)
        {
            ST_RDSEmail Correo = new ST_RDSEmail();

            DynamicObservacion.AddKey("FROM.USR_EMAIL", FM_USR.USR_EMAIL);
            DynamicObservacion.AddKey("FROM.USR_NAME", FM_USR.USR_NAME);
            DynamicObservacion.AddKey("FROM.USR_ROLE", GetUserRole(FM_USR.USR_ROLE));
            DynamicObservacion.AddKey("FROM.USR_COMMENT", Reasign.COMMENT);
            DynamicObservacion.AddKey("TO.USR_EMAIL", TO_USR.USR_EMAIL);
            DynamicObservacion.AddKey("TO.USR_NAME", TO_USR.USR_NAME);
            DynamicObservacion.AddKey("TO.USR_ROLE", GetUserRole(TO_USR.USR_ROLE));

            AddCuentasDestino(ref Correo, DynamicObservacion.GetHeaderAddress(MAIL_KEY_NAME));
            Correo.asuntoCorreo = DynamicObservacion.GetHeaderSubject(MAIL_KEY_NAME);
            Correo.mensajeCorreo.AppendLine(DynamicObservacion.GetBody(MAIL_KEY_NAME));

            return Correo;
        }

        private bool EnviarCorreoObsReasign(ref ST_RDSObservacion Observacion, ST_RDSReasignUp Reasign, ref List<ST_RDSUsuario> Usuarios, ref ST_Helper Helper)
        {
            bool SendOK = true;
            List<ST_RDSEmail> Correos = new List<ST_RDSEmail>();
            try
            {
                RDSDynamicObservacion DynamicObservacion = new RDSDynamicObservacion(ref Observacion, ref Helper);

                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                            {
                                ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                TO_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                FM_USR.USR_ROLE = Reasign.ColumnROLE;
                                Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_SOLICITAR"));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Rechazar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_RECHAZAR"));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Aprobar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_APROBAR"));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_OBS_ASIGNAR"));
                                }
                            }
                        }
                        break;

                    case REASIGN_TYPE.Reasignar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_OBS_DESASIGNAR"));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario TO_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario FM_USR = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);
                                    TO_USR.USR_ROLE = Reasign.ColumnROLE;
                                    FM_USR.USR_ROLE = Reasign.NEXT_LEVEL_ROLE;
                                    Correos.Add(GetCorreoReasignObs(ref DynamicObservacion, ref TO_USR, ref FM_USR, ref Reasign, "MAIL_REASIGN_OBS_ASIGNAR"));
                                }
                            }
                        }
                        break;
                }

                string[] CuentasCC = GetCuentas_CC_BCC(DynamicObservacion.GetKeyTextValue("MAIL_CARBON_COPY"));
                string[] CuentasBCC = GetCuentas_CC_BCC(DynamicObservacion.GetKeyTextValue("MAIL_BLIND_CARBON_COPY"));

                foreach (var Correo in Correos)
                {
                    if (Correo.cuentasDestino.Any(bp => !string.IsNullOrEmpty(bp)))
                    {
                        SendOK = ReadPopMail.MailHelper.Correo.SendMessage(Correo.cuentasDestino.Where(bp => !string.IsNullOrEmpty(bp)).ToArray(), CuentasCC, CuentasBCC, Correo.asuntoCorreo, "", Correo.mensajeCorreo.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
            return true;
        }

        private string[] GetCuentas_CC_BCC(string CadCuentas)
        {
            CadCuentas = CadCuentas.Replace(" ", "").Replace("\r", "").Replace("\n", "");
            string[] ArrCuentas = CadCuentas.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            return ArrCuentas;
        }

        private void AddCuentasDestino(ref ST_RDSEmail Correo, string Cuentas)
        {
            if(!string.IsNullOrEmpty(Cuentas))
                Correo.cuentasDestino.AddRange(Cuentas.Split(',').ToList());
        }

        #endregion

        #region Locales

        private Dictionary<string, string> GetDinamicTexts(ref csData cData)
        {
            string sSQL = @"SELECT TXT_ID, TXT_TEXT From RADIO.TEXTS";

            Dictionary<string, string> DynamicTexts = new Dictionary<string, string>();

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

            DynamicTexts = (from bp in dt
                           select new
                           {
                               Key = bp.Field<string>("TXT_ID"),
                               Text = bp.Field<string>("TXT_TEXT")
                           }).ToList().ToDictionary(x => x.Key, x => x.Text);

            return DynamicTexts;
        }

        private Dictionary<string, byte[]> GetDinamicImages(ref csData cData, string IMG_ID_FILTER="")
        {
            string sSQL = @"SELECT IMG_ID, IMG_FILE From RADIO.IMAGES"; 

            if(!string.IsNullOrEmpty(IMG_ID_FILTER))
                sSQL+=" WHERE IMG_ID = '" + IMG_ID_FILTER + "'"; 


            Dictionary<string, byte[]> DynamicImages = new Dictionary<string, byte[]>();

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

            DynamicImages = (from bp in dt
                           select new
                           {
                               Key = bp.Field<string>("IMG_ID"),
                               Data = bp.Field<byte[]>("IMG_FILE")
                           }).ToList().ToDictionary(x => x.Key, x => x.Data);

            return DynamicImages;
        }

        private byte[] Base64ToByteArray(string Base64File){

            int Tam = Base64File.IndexOf(",");
            if (Tam < 0)
                throw new Exception("Archivo base 64 con formato incorrecto");

            return Convert.FromBase64String(Base64File.Substring(Tam + 1));
        }

        private string GetMensajeAlertaSolicitud(string TipoSolicitud, string RadicadoCodigo)
        {
            string ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido creada satisfactoriamente.  Radicado No. RDS " + RadicadoCodigo + @"
                                         </td>
                                         </tr>
                                         </table>
                                        ";

            return ResultMessage; 
        }

        private string GetMensajeAlertaSubsanar(string TipoSolicitud, string RadicadoCodigo)
        {
            string ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido subsanada satisfactoriamente.  Radicado No. RDS " + RadicadoCodigo + @"
                                         </td>
                                         </tr>
                                         </table>
                                        ";

            return ResultMessage;
        }

        private string GetMensajeAlertaCancelar(string TipoSolicitud, string RadicadoCodigo)
        {
            string ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido cancelada satisfactoriamente.  Radicado No. RDS " + RadicadoCodigo + @"
                                         </td>
                                         </tr>
                                         </table>
                                        ";
            if (RadicadoCodigo == null)
            {
                ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido cancelada satisfactoriamente.
                                         </td>
                                         </tr>
                                         </table>
                                        ";
            }

            return ResultMessage;
        }

        private string GetMensajeAlertaAplazar(string TipoSolicitud, string RadicadoCodigo, int SOL_REQUIRED_POSTPONE)
        {
            string ResultMessage = @"
                                        <table>
                                         <tr>
                                         <td><i style='margin-right:5px;color:green' class='ta ta-chulo ta-3x'></i></td>
                                            
                                         <td>
                                         <p>Su solicitud de :</p> 
                                         <p><b> Comunicación " + TipoSolicitud + @" </b></p> 
                                          ha sido aplazada por " + SOL_REQUIRED_POSTPONE + @" días satisfactoriamente.  Radicado No. RDS " + RadicadoCodigo + @"
                                         </td>
                                         </tr>
                                         </table>
                                        ";

            return ResultMessage;
        }

        private Object GetDateNulable(string Fecha)
        {
            return (string.IsNullOrEmpty(Fecha) ? (object)DBNull.Value : DateTime.ParseExact(Fecha, "yyyyMMdd", CultureInfo.InvariantCulture));
        }

        private string GetUserRole(int USR_ROLE)
        {
            string sResult = "";

            switch ((ROLES)USR_ROLE)
            {
                case ROLES.NoAplica:
                    sResult = "No aplica";
                    break;
                case ROLES.Concesionario:
                    sResult = "Concesionario";
                    break;
                case ROLES.Funcionario:
                    sResult = "Funcionario";
                    break;
                case ROLES.Revisor:
                    sResult = "Revisor";
                    break;
                case ROLES.Coordinador:
                    sResult = "Coordinador";
                    break;
                case ROLES.Subdirector:
                    sResult = "Subdirector";
                    break;
                case ROLES.Director:
                    sResult = "Director";
                    break;
                case ROLES.Asesor:
                    sResult = "Asesor";
                    break;
                case ROLES.Administrador:
                    sResult = "Administrador";
                    break;
            }

            return sResult;

        }

        private bool GetSolEnded(int SOL_STATE_ID)
        {
            int[] Numbers = {(int)SOL_STATES.EnCierre, (int)SOL_STATES.Finalizada, (int)SOL_STATES.Cancelada, (int)SOL_STATES.Rechazada};

            return Numbers.Contains(SOL_STATE_ID);

        }

        private static string GetTagValue(string alfaResult, string tagName)
        {
            XmlDocument xmlResult = new XmlDocument();
            xmlResult.LoadXml(alfaResult);
            string alfaFilingCode = string.Empty;

            var nodes = xmlResult.GetElementsByTagName(tagName);

            if (null != nodes[0])
                alfaFilingCode = nodes[0].InnerText;

            if (String.IsNullOrEmpty(alfaFilingCode))
            {
                var errors = xmlResult.GetElementsByTagName("MensajeError");
                StringBuilder errorBuilder = new StringBuilder();
                for (int i = 0; i < errors.Count; i++)
                {
                    errorBuilder.AppendLine(errors[i].InnerText);
                }
                throw new ApplicationException(errorBuilder.ToString());
            }

            return alfaFilingCode;
        }

        private static string GetTagValueAlfa(string alfaResult, string tagName)
        {
            return DateTime.Now.ToString("HHmmss");
        }

        private static string GetRegistroNumber(string alfaResult)
        {
            return GetTagValueAlfa(alfaResult, "RegistroCodigo");
        }

        private void GetUserSign(ST_RDSUsuario Usuario, ref csData cData)
        {
            string sSQL = @"Select USR_SIGN From RADIO.USUARIOS Where USR_ID=" + Usuario.USR_ID;

            DataTable dt = cData.ObtenerDataTable(sSQL);

            if (dt.Rows.Count != 1)
            {
                Usuario.USR_SIGN = null;
                return;
            }
            Usuario.USR_SIGN = dt.Rows[0].Field<byte[]>("USR_SIGN");

        }

        private void GuardarComunicado(ref DataTable DTComunicado, ref ST_RDSSolicitud Solicitud, ST_RDSComunicado Comunicado,  ref MemoryStream mem)
        {

            if (DTComunicado.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("COM_CLASS") == Comunicado.COM_CLASS).Any())
            {
                DataRow DRC = DTComunicado.AsEnumerable().AsQueryable().Where(bp => bp.Field<int>("COM_CLASS") == Comunicado.COM_CLASS).FirstOrDefault();
                DRC["SOL_UID"] = Solicitud.SOL_UID;
                DRC["COM_CLASS"] = Comunicado.COM_CLASS;
                DRC["COM_TYPE"] = Comunicado.COM_TYPE;
                DRC["COM_DATE"] = DateTime.Now;
                DRC["COM_NAME"] = Comunicado.COM_NAME;
                DRC["COM_FILE"] = mem.GetBuffer();
                Comunicado.COM_UID = (Guid)DRC["COM_UID"];
            }
            else
            {
                DataRow DRC = DTComunicado.NewRow();
                DRC["SOL_UID"] = Solicitud.SOL_UID;
                DRC["COM_UID"] = Comunicado.COM_UID;
                DRC["COM_CLASS"] = Comunicado.COM_CLASS;
                DRC["COM_TYPE"] = Comunicado.COM_TYPE;
                DRC["COM_DATE"] = DateTime.Now;
                DRC["COM_NAME"] = Comunicado.COM_NAME;
                DRC["COM_FILE"] = mem.GetBuffer();
                DTComunicado.Rows.Add(DRC);
            }

        }

        private void AddConteoDevoluciones(ref DataTable DTConteos, Guid SOL_UID, int USR_ID, GROUPS USR_GROUP, ROLES USR_ROLE)
        {
            DataRow DRC = DTConteos.NewRow();

            DRC["SOL_UID"] = SOL_UID;
            DRC["USR_ID"] = USR_ID;
            DRC["USR_GROUP"] = (int)USR_GROUP;
            DRC["USR_ROLE"] = (int)USR_ROLE;
            
            DTConteos.Rows.Add(DRC);
        }

        private ST_RDSSolicitud MakeSolicitud(Guid SOL_UID, int SOL_NUMBER, string Radicado, string SOL_TYPE_NAME, ref ST_RDSExpediente Expediente, ref ST_RDSContacto Contacto)
        {

            ST_RDSSolicitud Solicitud = new ST_RDSSolicitud();
            Solicitud.SOL_UID = SOL_UID;
            Solicitud.SOL_NUMBER = SOL_NUMBER;
            Solicitud.Radicado = Radicado;
            Solicitud.SOL_CREATED_DATE = DateTime.Now.ToString("dd/MM/yyyy");
            Solicitud.SOL_CREATED_DATE_YEAR = DateTime.Now.ToString("yyyy");
            Solicitud.SOL_TYPE_NAME = SOL_TYPE_NAME;
            Solicitud.Contacto = Contacto;
            Solicitud.Expediente = Expediente;
            Solicitud.USR_ADMIN = new ST_RDSRoles();
            Solicitud.USR_TECH = new ST_RDSRoles();
            return Solicitud;
        }

        private List<ST_RDSUsuario> AutomaticAssignGroup1_V1(ref csData cData, ref DataTable DTSolUsers, ref ST_Helper Helper, Guid SOL_UID)
        {
            List<ST_RDSUsuario> Asignaciones;

            string sSQL = cData.TraerValor("Select TXT_TEXT From RADIO.TEXTS Where TXT_ID='ASIGNACIONES_GROUP_1_V1'", "");

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

            Asignaciones = (from bp in dt
                            select new ST_RDSUsuario
                            {
                                USR_ROLE = bp.Field<int>("USR_ROLE"),
                                USR_ID = bp.Field<int>("USR_ID"),
                                USR_NAME = bp.Field<string>("USR_NAME"),
                                USR_EMAIL = bp.Field<string>("USR_EMAIL")
                            }).ToList();


            DataRow DRSU_Type1 = DTSolUsers.NewRow();
            DRSU_Type1["SOL_UID"] = SOL_UID;
            DRSU_Type1["USR_GROUP"] = (int)GROUPS.MinTIC;
            DRSU_Type1["USR_ROLE1_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First().USR_ID;
            DRSU_Type1["USR_ROLE2_ID"] = (int)ROLES.NoAplica;
            DRSU_Type1["USR_ROLE4_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First().USR_ID;
            DRSU_Type1["USR_ROLE8_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First().USR_ID;
            DRSU_Type1["USR_ROLE16_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First().USR_ID;
            DRSU_Type1["USR_ROLE32_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First().USR_ID;
            DTSolUsers.Rows.Add(DRSU_Type1);

            DataRow DRSU_Type2 = DTSolUsers.NewRow();
            DRSU_Type2["SOL_UID"] = SOL_UID;
            DRSU_Type2["USR_GROUP"] = (int)GROUPS.ANE;
            DTSolUsers.Rows.Add(DRSU_Type2);

            return Asignaciones;
        }

        private List<ST_RDSUsuario> AutomaticAssignGroup1_V2(ref csData cData, ref DataTable DTSolUsers, Guid SOL_UID, int USR_ROLE1_ID)
        {
            List<ST_RDSUsuario> Asignaciones;

            string sSQL = cData.TraerValor("Select TXT_TEXT From RADIO.TEXTS Where TXT_ID='ASIGNACIONES_GROUP_1_V2'", "");
            sSQL = sSQL.Replace("{{USR_ROLE1_ID}}", USR_ROLE1_ID.ToString());

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

            Asignaciones = (from bp in dt
                            select new ST_RDSUsuario
                            {
                                USR_ROLE = bp.Field<int>("USR_ROLE"),
                                USR_ID = bp.Field<int>("USR_ID"),
                                USR_NAME = bp.Field<string>("USR_NAME"),
                                USR_EMAIL = bp.Field<string>("USR_EMAIL")
                            }).ToList();


            DataRow DRSU_Type1 = DTSolUsers.NewRow();
            DRSU_Type1["SOL_UID"] = SOL_UID;
            DRSU_Type1["USR_GROUP"] = (int)GROUPS.MinTIC;
            DRSU_Type1["USR_ROLE1_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First().USR_ID;
            DRSU_Type1["USR_ROLE2_ID"] = (int)ROLES.NoAplica;
            DRSU_Type1["USR_ROLE4_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First().USR_ID;
            DRSU_Type1["USR_ROLE8_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First().USR_ID;
            DRSU_Type1["USR_ROLE16_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First().USR_ID;
            DRSU_Type1["USR_ROLE32_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First().USR_ID;
            DTSolUsers.Rows.Add(DRSU_Type1);

            DataRow DRSU_Type2 = DTSolUsers.NewRow();
            DRSU_Type2["SOL_UID"] = SOL_UID;
            DRSU_Type2["USR_GROUP"] = (int)GROUPS.ANE;
            DTSolUsers.Rows.Add(DRSU_Type2);

            return Asignaciones;
        }

        private List<ST_RDSUsuario> AutomaticAssignGroup2(ref csData cData, ref DataTable DTSolUsers)
        {
            List<ST_RDSUsuario> Asignaciones;

            string sSQL = cData.TraerValor("Select TXT_TEXT From RADIO.TEXTS Where TXT_ID='ASIGNACIONES_GROUP_2'", "");

            var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

            Asignaciones = (from bp in dt
                            select new ST_RDSUsuario
                            {
                                USR_ROLE = bp.Field<int>("USR_ROLE"),
                                USR_ID = bp.Field<int>("USR_ID"),
                                USR_NAME = bp.Field<string>("USR_NAME"),
                                USR_EMAIL = bp.Field<string>("USR_EMAIL")
                            }).ToList();



            if (DTSolUsers.Rows.Count != 2)
                return Asignaciones;

            DataRow DRSU_Type2 = DTSolUsers.Rows[1];
            DRSU_Type2["USR_ROLE1_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First().USR_ID;
            DRSU_Type2["USR_ROLE2_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First().USR_ID;
            DRSU_Type2["USR_ROLE4_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First().USR_ID;
            DRSU_Type2["USR_ROLE8_ID"] = Asignaciones.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First().USR_ID;

            return Asignaciones;
        }

        private void CrearAnalisisTecnico(ref DataTable DTTech, Guid SOL_UID)
        {
            DataRow DRT = DTTech.NewRow();
            DRT["SOL_UID"] = SOL_UID;
            DRT["TECH_ROLE1_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
            DRT["TECH_ROLE2_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRT["TECH_ROLE4_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRT["TECH_ROLE8_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DTTech.Rows.Add(DRT);
        }

        private void CrearAnalisisResolucion(ref DataTable DTResAnalisis, Guid SOL_UID)
        {
            DataRow DRR = DTResAnalisis.NewRow();
            DRR["SOL_UID"] = SOL_UID;
            DRR["RES_ROLE1_STATE_ID"] = (int)ROLE1_STATE.SinDefinir;
            DRR["RES_ROLE2_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRR["RES_ROLE4_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRR["RES_ROLE8_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRR["RES_ROLE16_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DRR["RES_ROLE32_STATE_ID"] = (int)ROLE248_STATE.SinDefinir;
            DTResAnalisis.Rows.Add(DRR);
        }

        private void CrearResolucion(ref DataTable DTResolucion, Guid SOL_UID)
        {
            DataRow DRR = DTResolucion.NewRow();
            DRR["SOL_UID"] = SOL_UID;
            DTResolucion.Rows.Add(DRR);
        }

        private void AddNewDocument(ref DataTable DTDocuments, Guid SOL_UID, DOC_CLASS Doc_Class, DOC_TYPE DocType, DOC_SOURCE DocOrigin, DOC_SOURCE DocDest, string DocNumber, ref MemoryStream mem, string DocName)
        {
            DataRow DRD = DTDocuments.NewRow();
            DRD["DOC_UID"] = Guid.NewGuid();
            DRD["SOL_UID"] = SOL_UID;
            DRD["DOC_CLASS"] = (int)Doc_Class;
            DRD["DOC_TYPE"] = (int)DocType;
            DRD["DOC_ORIGIN"] = (int)DocOrigin;
            DRD["DOC_DEST"] = (int)DocDest;
            DRD["DOC_NUMBER"] = DocNumber;
            DRD["DOC_NAME"] = DocName + ".pdf";
            DRD["DOC_DATE"] = DateTime.Now;
            DRD["DOC_FILE"] = mem.GetBuffer();
            DTDocuments.Rows.Add(DRD);
        }

        private ROLE248_STATE GetRole248State(ref ST_RDSSolicitud Solicitud, int ROLE)
        {
            ROLE248_STATE STATE248 = ROLE248_STATE.SinDefinir;

            switch ((STEPS)Solicitud.CURRENT.STEP)
            {
                case STEPS.Administrativo:
                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Coordinador:
                            if (Solicitud.Anexos.Any(bp => bp.ANX_ROLE4_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                STATE248 = ROLE248_STATE.Devuelto;
                            else
                                STATE248 = ROLE248_STATE.Aceptado;
                            break;
                        case ROLES.Subdirector:
                            if (Solicitud.Anexos.Any(bp => bp.ANX_ROLE8_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                STATE248 = ROLE248_STATE.Devuelto;
                            else
                                STATE248 = ROLE248_STATE.Aceptado;
                            break;
                    }
                    break;
                case STEPS.Tecnico:
                    STATE248 = (ROLE248_STATE)Solicitud.AnalisisTecnico.TECH_ROLE_MINTIC_STATE_ID;
                    if (STATE248 == ROLE248_STATE.SinDefinir)
                    {
                        switch ((ROLES)ROLE)
                        {
                            case ROLES.Revisor:
                                if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE2_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                    STATE248 = ROLE248_STATE.Devuelto;
                                else
                                    STATE248 = ROLE248_STATE.Aceptado;
                                break;
                            case ROLES.Coordinador:
                                if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE4_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                    STATE248 = ROLE248_STATE.Devuelto;
                                else
                                    STATE248 = ROLE248_STATE.Aceptado;
                                break;
                            case ROLES.Subdirector:
                                if (Solicitud.TechAnexos.Any(bp => bp.ANX_ROLE8_STATE_ID == (int)ROLE248_STATE.Devuelto))
                                    STATE248 = ROLE248_STATE.Devuelto;
                                else
                                    STATE248 = ROLE248_STATE.Aceptado;
                                break;
                        }

                    }
                    break;

                case STEPS.Resolucion:
                    switch ((ROLES)ROLE)
                    {
                        case ROLES.Funcionario:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE1_STATE_ID;
                            break;
                        case ROLES.Revisor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE2_STATE_ID;
                            break;
                        case ROLES.Coordinador:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE4_STATE_ID;
                            break;
                        case ROLES.Subdirector:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE8_STATE_ID;
                            break;
                        case ROLES.Asesor:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE16_STATE_ID;
                            break;
                        case ROLES.Director:
                            STATE248 = (ROLE248_STATE)Solicitud.AnalisisResolucion.RES_ROLE32_STATE_ID;
                            break;

                    }
                    break;
                   
            }

            return STATE248;
        }

        private void IgualarRole1States(ref DataTable DTAnexo)
        {
            foreach (DataRow DRA in DTAnexo.Rows)
            {
                DRA["ANX_STATE_ID"] = DRA["ANX_ROLE1_STATE_ID"];
                DRA["ANX_COMMENT"] = DRA["ANX_ROLE1_COMMENT"];
            }
        }

        private void ResetTechDocsChanged(ref DataTable DTTechDocs)
        {
            foreach (DataRow DRF in DTTechDocs.Rows)
            {
                DRF["TECH_CHANGED"] = false;
            }
        }

        private void UpdateAnexosRoleStates(ref DataTable DTAnexo, ref ST_RDSCurrent CURRENT)
        {
            foreach (DataRow DRA in DTAnexo.Rows)
            {
                switch ((GROUPS)CURRENT.GROUP)
                {
                    case GROUPS.MinTIC:
                        switch ((ROLES)CURRENT.ROLE)
                        {
                            case ROLES.Funcionario:
                                DRA["ANX_ROLEX_CHANGED"] = ((int)DRA["ANX_ROLE4_STATE_ID"] == (int)ROLE248_STATE.Aceptado);
                                break;
                            case ROLES.Coordinador:
                                DRA["ANX_ROLEX_CHANGED"] = ((int)DRA["ANX_ROLE8_STATE_ID"] == (int)ROLE248_STATE.Aceptado);
                                break;
                            case ROLES.Subdirector:
                                DRA["ANX_ROLEX_CHANGED"] = ((int)DRA["ANX_ROLE8_STATE_ID"] == (int)ROLE248_STATE.Aceptado);
                                break;
                        }
                        break;

                    case GROUPS.ANE:
                        DRA["ANX_ROLEX_CHANGED"] = false;
                        break;
                }
            }
        }

        private UAgent GetUserAgentInfo(string UserAgent)
        {
            UAgent UA = new UAgent();

            if (UserAgent.Contains("Windows"))
            {
                UA.OS = "Windows";
                if (UserAgent.Contains("Edge"))
                {
                    UA.BROWSER = "Edge";
                }
                else if (UserAgent.Contains("OPR"))
                {
                    UA.BROWSER = "Opera";
                }
                else if (UserAgent.Contains("Trident"))
                {
                    UA.BROWSER = "IE";
                }
                else if (UserAgent.Contains("Firefox"))
                {
                    UA.BROWSER = "Firefox";
                }
                else if (UserAgent.Contains("Chrome") && UserAgent.Contains("AppleWebKit"))
                {
                    UA.BROWSER = "Chrome";
                }
                else
                {
                    UA.BROWSER = "NA";
                }
            }
            else if (UserAgent.Contains("Macintosh"))
            {
                UA.OS = "Macintosh";
                if (UserAgent.Contains("Firefox"))
                {
                    UA.BROWSER = "Firefox";
                }
                else if (UserAgent.Contains("Chrome"))
                {
                    UA.BROWSER = "Chrome";
                }
                else if (UserAgent.Contains("Safari"))
                {
                    UA.BROWSER = "Safari";
                }
                else
                {
                    UA.BROWSER = "NA";
                }
            }
            else if (UserAgent.Contains("iPad"))
            {
                UA.OS = "iPad";
                if (UserAgent.Contains("CriOS"))
                {
                    UA.BROWSER = "Chrome";
                }
                else if (UserAgent.Contains("OPiOS"))
                {
                    UA.BROWSER = "Opera";
                }
                else if (UserAgent.Contains("Safari"))
                {
                    UA.BROWSER = "Safari";
                }
                else
                {
                    UA.BROWSER = "NA";
                }
            }
            else if (UserAgent.Contains("iPhone"))
            {
                UA.OS = "iPhone";
                if (UserAgent.Contains("CriOS"))
                {
                    UA.BROWSER = "Chrome";
                }
                else if (UserAgent.Contains("OPiOS"))
                {
                    UA.BROWSER = "Opera";
                }
                else if (UserAgent.Contains("Safari"))
                {
                    UA.BROWSER = "Safari";
                }
                else
                {
                    UA.BROWSER = "NA";
                }
            }
            else if (UserAgent.Contains("Android"))
            {
                UA.OS = "Android";
                if (UserAgent.Contains("OPR"))
                {
                    UA.BROWSER = "Opera";
                }
                else if (UserAgent.Contains("Chrome"))
                {
                    UA.BROWSER = "Chrome";
                }
                else if (UserAgent.Contains("Safari"))
                {
                    UA.BROWSER = "Nativo";
                }
                else
                {
                    UA.BROWSER = "NA";
                }
            }
            else
            {
                UA.OS = "NA";
                UA.BROWSER = "NA";
            }

            return UA;
        }

        private string EncryptSHA256(string USR_PASSWORD)
        {

            string sResult = "";

            try
            {
                var crypt = new System.Security.Cryptography.SHA256Managed();
                var hash = new System.Text.StringBuilder();
                byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(USR_PASSWORD));
                foreach (byte theByte in crypto)
                {
                    hash.Append(theByte.ToString("x2"));
                }

                sResult = hash.ToString();
                return sResult;
            }
            catch (Exception)
            {
                return "";
            }
        }

        private void ValidateToken(ref csData cData)
        {
            int TOKEN_ID = cData.TraerValor("Select ID From RADIO.TOKENS Where TOKEN='" + HttpContext.Current.Request.Headers["Token"] + "' And DateDiff(mi,MODIFIED,getdate())<=" + SGETokenTimeOut, -1);
            if (TOKEN_ID == -1)
                throw new DataServiceException(403, "Sesión ha expirado");
            cData.ClickExecuteNonQuery("Update RADIO.TOKENS Set NCALLS=NCALLS+1, MODIFIED=getdate() Where ID=" + TOKEN_ID);
        }

        private static ST_MyEncrypt MyDecrypt(string sText)
        {
            try
            {
                var Serializer = new JavaScriptSerializer_TESExtension();
                int checkSum = 0;
                ST_MyEncrypt mResult = new ST_MyEncrypt();

                byte[] clearBytes = Convert.FromBase64String(sText);
                int n = clearBytes.Length;
                if (n != 258)
                    return mResult;

                byte[] KeyBytes = Encoding.UTF8.GetBytes("CRGID10288018CRGCEL3114409465ABPID75082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034PAOCEL3124896735CRGID10288018CRGCEL3114409465YCID1007275830YCCEL310792247375082570XOAIDXAOID52252942XAOCEL313488153252252942ABPID75082570ABPCEL3148871203AOMCEL3104079112HPCEL3104320034");

                for (int i = 0; i < n; i++)
                {
                    clearBytes[i] ^= KeyBytes[i];
                }

                for (int i = 0; i < n - 4; i += 2)
                {
                    clearBytes[i] ^= clearBytes[n - 3];
                    clearBytes[i + 1] ^= clearBytes[n - 4];
                }

                for (int i = 0; i < n - 2; i += 2)
                {
                    clearBytes[i] ^= clearBytes[n - 1];
                    clearBytes[i + 1] ^= clearBytes[n - 2];
                }

                sText = Encoding.UTF8.GetString(clearBytes);

                if (sText.Substring(0, 1) != "Ð")
                    return mResult;

                for (int i = 0; i < 248; i++)
                {
                    checkSum += sText[i + 8];
                }

                int checkSumOK = Convert.ToInt32(sText.Substring(1, 6));

                if (checkSumOK != checkSum)
                    return mResult;

                string mDecrypt = sText.Substring(8, 248).Trim();
                mResult = Serializer.Deserialize<ST_MyEncrypt>(mDecrypt);
                mResult.IsValid = true;
                return mResult;
            }
            catch (Exception)
            {
                return new ST_MyEncrypt();
            }
        }

        private string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        private int GetIntValue(string sValor, int nDefault)
        {
            int nRet;
            if (!Int32.TryParse(sValor, out nRet))
                nRet = nDefault;
            return nRet;
        }

        private string GetContactTitle(int CONT_ROLE)
        {
            string sResult = "";

            switch (CONT_ROLE)
            {
                case 1:
                    sResult = "Apoderado";
                    break;
                case 2:
                    sResult = "Representante legal";
                    break;
                case 8:
                    sResult = "Representante legal suplente";
                    break;
                default:
                    sResult = "No disponible";
                    break;
            }
            return sResult;
        }

        private void CrearTechAnexos(ref DataTable DTTechAnexos, ref ST_RDSSolicitud Solicitud)
        {
            int NumAnex = 4;
            if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico)
                NumAnex = 5;
            for (int i = 0; i < NumAnex; i++)
            {
                DataRow DRA = DTTechAnexos.NewRow();
                DRA["ANX_UID"] = Guid.NewGuid();
                DRA["SOL_UID"] = Solicitud.SOL_UID;
                if (i == 0)
                    DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaEstudioTecnico;
                if (i == 1)
                    DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaConceptoAeronautica;
                if (i == 2)
                {
                    if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComercial || Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico)
                        DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaPlaneacionMunicipal;
                    else if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComunitaria || Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos)
                        DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaJuntaProgramacion;
                }
                if (i == 3)
                {
                    if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComercial || Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComunitaria || Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraComunitariaGruposEtnicos)
                        DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaAnexoTecnico;
                    else if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.OtorgaEmisoraDeInteresPublico)
                        DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaManualEstilo;
                }
                if (i == 4)
                    DRA["ANX_TYPE_ID"] = (int)TiposDeAnexoSolicitudRDS.OtorgaAnexoTecnico;
                DRA["ANX_STATE_ID"] = 2;
                DRA["ANX_NUMBER"] = 1 + i;
                DRA["ANX_NAME"] = "";
                if (i < NumAnex - 1)
                    DRA["ANX_CONTENT_TYPE"] = "application/pdf";
                else
                    DRA["ANX_CONTENT_TYPE"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                DRA["ANX_CREATED_DATE"] = DateTime.Now;
                DRA["ANX_MODIFIED_DATE"] = DateTime.Now;
                DRA["ANX_ROLE1_STATE_ID"] = 2;
                DRA["ANX_ROLE2_STATE_ID"] = 0;
                DRA["ANX_ROLE4_STATE_ID"] = 0;
                DRA["ANX_ROLE8_STATE_ID"] = 0;
                DRA["ANX_ROLEX_CHANGED"] = 0;
                //DRA["ANX_NUMBER"] = 1 + i;
                DTTechAnexos.Rows.Add(DRA);
            }
        }

        private void EliminarSolicitudTemporal(ref DataTable DTSolicitudTemp, ref DataTable DTAnexosTemp, ref DataTable DTExpedienteTemp)
        {
            DataRow DRS = DTSolicitudTemp.Rows[0];
            ST_RDSSolicitud SolicitudEliminar = new ST_RDSSolicitud();
            SolicitudEliminar.SOL_UID = (Guid)DRS["SOL_UID"];
            DRS.Delete();

            foreach (DataRow DRA in DTAnexosTemp.Rows)
            {
                DRA.Delete();
            }

            DataRow DRE = DTExpedienteTemp.Rows[0];
            DRE.Delete();
        }

        private AZDigitall.RtaNotificar NotificarResolucion(int IDUserWeb, ref ST_RDSSolicitud Solicitud, string Resolucion_Name, byte[] ResolArchivo)
        {
            AZDigitall.RtaNotificar wsResult = null;
            ServiciosNotificaciones_SOAPClient ws = new AZDigitall.ServiciosNotificaciones_SOAPClient();

            Notificar Notif = new Notificar();
            NotificarArchivo archivo = new NotificarArchivo();
            NotificarPersona[] personas = new NotificarPersona[1];
            personas[0] = new NotificarPersona();

            byte[] data = ResolArchivo;
            //byte[] data = File.ReadAllBytes(@"E:\resprueba.docx");
            archivo.Value = Convert.ToBase64String(data);

            archivo.Codificacion = "Base64";
            archivo.Nombre = "Resolucion.docx";
            personas[0].TipoIdentificacion = "CC";
            personas[0].Nombre = Solicitud.Contacto.CONT_NAME;
            personas[0].NumeroIdentificacion = Solicitud.Contacto.CONT_ID.ToString();
            personas[0].Direccion = new DireccionType();
            personas[0].Direccion.Direccion = "";
            personas[0].Direccion.Departamento = "11";
            personas[0].Direccion.Ciudad = "11001";
            personas[0].Direccion.Pais = "CO";
            Notif.PersonasNotificar = personas;
            Notif.Archivo = archivo;
            Notif.Descripcion = Resolucion_Name;
            Notif.Dependencia = System.Configuration.ConfigurationManager.AppSettings["DependenciaAZResolucion"];
            Notif.TipoActo = "R";
            Notif.TipoNotificacion = "P";
            Notif.UsuarioFirmante = "admin";
            //ws.Notificar(Notif);
            try
            {
                wsResult = ws.Notificar(Notif);
            }
            catch (Exception e)
            {
                var SerializerXML = new XmlSerializer(typeof(Notificar));
                StringWriter stringWriter = new StringWriter();
                XmlWriter writer = XmlWriter.Create(stringWriter);
                string slog = "";
                using (var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings { Indent = true }))
                {
                    SerializerXML.Serialize(xmlWriter, Notif);
                    slog = stringWriter.ToString();
                }
                System.Net.HttpWebResponse res = ((System.Net.HttpWebResponse)((System.Net.WebException)e.InnerException).Response);
                Stream receiveStream = res.GetResponseStream();
                Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                StreamReader readStream = new StreamReader(receiveStream, encode);
                Char[] read = new Char[2048];
                string s = "";
                int count = readStream.Read(read, 0, 2048);
                while (count > 0)
                {
                    String str = new String(read, 0, count);
                    s += str;
                    count = readStream.Read(read, 0, 2048);
                }
                receiveStream.Close();
                XmlSerializer serializer = new XmlSerializer(typeof(AZRadicar.Errores));
                XmlReader xReader = XmlReader.Create(new StringReader(s));
                SGEWS servidor = new SGEWS();
                servidor.WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, "Exception e" + e.Message, slog, s);
                while (xReader.Read())
                {
                    if (xReader.IsStartElement())
                    {
                        if (xReader.Name.ToString() == "faultstring")
                            throw new ApplicationException(String.Format(xReader.ReadString()));
                    }

                }
            }

            return wsResult;
        }

        #endregion

        #region Logs

        public List<ST_RDSLog> GetEventLogs(Guid SOL_UID)
        {
            int IDUserWeb = 1;
            csData cData = null;
            List<ST_RDSLog> mResult = new List<ST_RDSLog>();

            try
            {
                string strConn = ConfigurationManager.ConnectionStrings["SGEFrontConnectionString"].ToString();
                cData = new csData();
                bool ConectedOk = cData.Connect(strConn);

                if (!ConectedOk)
                {
                    WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "IDUserWeb=" + IDUserWeb, strConn);
                    return mResult;
                }
                string sSQL;

                ValidateToken(ref cData);

                sSQL = @" 
                        SELECT LOG_ID, SOL_UID, LOG_EVENT, LOG_ICON, LOG_SOURCE, LOG_USR_ROLE, LOG_USR_NAME, LOG_NUMBER, LOG_DESCRIPTION, LOG_DATE
                        FROM RADIO.LOGS
                        WHERE SOL_UID = '" + SOL_UID.ToString() + "'";

                var dt = cData.ObtenerDataTable(sSQL).AsEnumerable().AsQueryable();

                mResult = (from bp in dt
                           select new ST_RDSLog
                           {
                               LOG_ID = bp.Field<int>("LOG_ID"),
                               LOG_EVENT = bp.Field<string>("LOG_EVENT"),
                               LOG_ICON = bp.Field<string>("LOG_ICON"),
                               LOG_SOURCE = bp.Field<int>("LOG_SOURCE"),
                               LOG_USR_ROLE = bp.Field<int>("LOG_USR_ROLE"),
                               LOG_USR_NAME = bp.Field<string>("LOG_USR_NAME"),                             
                               LOG_NUMBER = bp.Field<string>("LOG_NUMBER"),
                               LOG_DESCRIPTION = bp.Field<string>("LOG_DESCRIPTION"),
                               LOG_DATE = bp.Field<DateTime>("LOG_DATE").ToString("yyyy/MM/dd HH:mm:ss")
                           }).ToList();


                return mResult;
            }
            catch (DataServiceException)
            {
                throw new WebFaultException<string>("Sesión ha expirado", System.Net.HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
                return mResult;
            }
            finally
            {
                if (cData != null)
                {
                    cData.CloseConnection();
                }
            }
        }

        public void WriteLogError(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
        {
            try
            {
                string fname = Path.Combine(sSGELogs, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".txt");

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                sb.AppendLine(FuncName);

                sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (!string.IsNullOrEmpty(sDesc0))
                    sb.AppendLine("Desc0=" + sDesc0);
                if (!string.IsNullOrEmpty(sDesc1))
                    sb.AppendLine("Desc1=" + sDesc1);
                if (!string.IsNullOrEmpty(sDesc2))
                    sb.AppendLine("Desc2=" + sDesc2);
                if (!string.IsNullOrEmpty(sDesc3))
                    sb.AppendLine("Desc3=" + sDesc3);
                sb.AppendLine("-------------------------------");

                File.AppendAllText(fname, sb.ToString());
            }
            catch
            {

            }

        }

        private void WriteLogErrorLogin(string FuncName, string sDesc0 = "", string sDesc1 = "", string sDesc2 = "", string sDesc3 = "")
        {

            try
            {
                string fname = Path.Combine(sSGELogs, System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".log");

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                sb.AppendLine(FuncName);

                sb.AppendLine(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (!string.IsNullOrEmpty(sDesc0))
                    sb.AppendLine("Desc0=" + sDesc0);
                if (!string.IsNullOrEmpty(sDesc1))
                    sb.AppendLine("Desc1=" + sDesc1);
                if (!string.IsNullOrEmpty(sDesc2))
                    sb.AppendLine("Desc2=" + sDesc2);
                if (!string.IsNullOrEmpty(sDesc3))
                    sb.AppendLine("Desc3=" + sDesc3);
                sb.AppendLine("-------------------------------");

                File.AppendAllText(fname, sb.ToString());
            }
            catch
            {

            }

        }

        private void AddLogReasign(ref csData cData, ref ST_RDSSolicitud Solicitud, ST_RDSReasignUp Reasign, ref List<ST_RDSUsuario> Usuarios, ref ST_Helper Helper)
        {
            List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();

            try
            {
                RDSDynamicSolicitud DynamicSolicitud = new RDSDynamicSolicitud(ref Solicitud, ref Helper);
                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                            {
                                ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                DynamicSolicitud.AddKey("FROM.USR_EMAIL", Usuario.USR_EMAIL);
                                DynamicSolicitud.AddKey("FROM.USR_NAME", Usuario.USR_NAME);
                                DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));
                                DynamicSolicitud.AddKey("TO.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                DynamicSolicitud.AddKey("TO.USR_NAME", UsuarioNextLevel.USR_NAME);
                                DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.NEXT_LEVEL_ROLE));

                                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_SOLICITAR", Reasign.USR_GROUP, Reasign.ColumnROLE, Usuario.USR_NAME, ""));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Rechazar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                DynamicSolicitud.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                DynamicSolicitud.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                DynamicSolicitud.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                DynamicSolicitud.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_RECHAZAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Aprobar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicSolicitud.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicSolicitud.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicSolicitud.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicSolicitud.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_APROBAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicSolicitud.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicSolicitud.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicSolicitud.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicSolicitud.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_ASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }
                            }
                        }
                        break;

                    case REASIGN_TYPE.Reasignar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicSolicitud.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicSolicitud.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicSolicitud.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicSolicitud.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_DESASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicSolicitud.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicSolicitud.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicSolicitud.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicSolicitud.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicSolicitud.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicSolicitud.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLog(ref DynamicSolicitud, Solicitud.SOL_UID, "LOG_REASIGN_ASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }
                            }
                        }
                        break;
                }

                AddLog(ref cData, lstLogs);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }

        }

        private void AddLogReasignObs(ref csData cData, ref ST_RDSObservacion Observacion, ST_RDSReasignUp Reasign, ref List<ST_RDSUsuario> Usuarios, ref ST_Helper Helper)
        {
            List<ST_RDSLog> lstLogs = new List<ST_RDSLog>();

            try
            {
                RDSDynamicObservacion DynamicObservacion = new RDSDynamicObservacion(ref Observacion, ref Helper);
                switch ((REASIGN_TYPE)Reasign.TYPE)
                {
                    case REASIGN_TYPE.Solicitar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                            {
                                ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                DynamicObservacion.AddKey("FROM.USR_EMAIL", Usuario.USR_EMAIL);
                                DynamicObservacion.AddKey("FROM.USR_NAME", Usuario.USR_NAME);
                                DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));
                                DynamicObservacion.AddKey("TO.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                DynamicObservacion.AddKey("TO.USR_NAME", UsuarioNextLevel.USR_NAME);
                                DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.NEXT_LEVEL_ROLE));

                                lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_SOLICITAR", Reasign.USR_GROUP, Reasign.ColumnROLE, Usuario.USR_NAME, ""));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Rechazar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID) && Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                DynamicObservacion.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                DynamicObservacion.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                DynamicObservacion.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                DynamicObservacion.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_RECHAZAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                            }
                        }
                        break;

                    case REASIGN_TYPE.Aprobar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicObservacion.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicObservacion.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicObservacion.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicObservacion.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_APROBAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicObservacion.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicObservacion.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicObservacion.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicObservacion.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_ASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }
                            }
                        }
                        break;

                    case REASIGN_TYPE.Reasignar:
                        {
                            if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID))
                            {
                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicObservacion.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicObservacion.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicObservacion.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicObservacion.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_DESASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }

                                if (Usuarios.Any(bp => bp.USR_ID == Reasign.NEW_USR_ID))
                                {
                                    ST_RDSUsuario Usuario = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEW_USR_ID);
                                    ST_RDSUsuario UsuarioNextLevel = Usuarios.FirstOrDefault(bp => bp.USR_ID == Reasign.NEXT_LEVEL_USR_ID);

                                    DynamicObservacion.AddKey("FROM.USR_EMAIL", UsuarioNextLevel.USR_EMAIL);
                                    DynamicObservacion.AddKey("FROM.USR_NAME", UsuarioNextLevel.USR_NAME);
                                    DynamicObservacion.AddKey("FROM.USR_ROLE", GetLogUserRole(Reasign.ROLE));
                                    DynamicObservacion.AddKey("TO.USR_EMAIL", Usuario.USR_EMAIL);
                                    DynamicObservacion.AddKey("TO.USR_NAME", Usuario.USR_NAME);
                                    DynamicObservacion.AddKey("TO.USR_ROLE", GetLogUserRole(Reasign.ColumnROLE));

                                    lstLogs.Add(CreateLogObs(ref DynamicObservacion, Observacion.ID_OBSERVACION, "LOG_REASIGN_ASIGNAR", Reasign.USR_GROUP, Reasign.ROLE, UsuarioNextLevel.USR_NAME, ""));
                                }
                            }
                        }
                        break;
                }

                AddLogObs(ref cData, lstLogs);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }

        }

        private void AddLog(ref csData cData, List<ST_RDSLog> lstLogs)
        {
            string sSQL;

            try
            {
                sSQL = @"SELECT TOP 0 LOG_ID, SOL_UID, LOG_ICON, LOG_EVENT, LOG_SOURCE, LOG_USR_ROLE, LOG_USR_NAME, LOG_NUMBER, LOG_DESCRIPTION
                         FROM RADIO.LOGS;";

                DataTable DTLogs = cData.ObtenerDataTable(sSQL);

                foreach (var Log in lstLogs)
                {
                    DataRow DRL = DTLogs.NewRow();

                    DRL["SOL_UID"] = Log.SOL_UID;
                    DRL["LOG_ICON"] = Log.LOG_ICON;
                    DRL["LOG_EVENT"] = Log.LOG_EVENT;
                    DRL["LOG_SOURCE"] = Log.LOG_SOURCE;
                    DRL["LOG_USR_ROLE"] = Log.LOG_USR_ROLE;
                    DRL["LOG_USR_NAME"] = Log.LOG_USR_NAME;
                    DRL["LOG_NUMBER"] = Log.LOG_NUMBER;
                    DRL["LOG_DESCRIPTION"] = Log.LOG_DESCRIPTION;

                    DTLogs.Rows.Add(DRL);
                }

                cData.InsertDataTable(sSQL, DTLogs);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
        }

        private void AddLogObs(ref csData cData, List<ST_RDSLog> lstLogs)
        {
            string sSQL;

            try
            {
                sSQL = @"SELECT TOP 0 LOG_ID, ID_OBSERVACION, LOG_ICON, LOG_EVENT, LOG_SOURCE, LOG_USR_ROLE, LOG_USR_NAME, LOG_NUMBER, LOG_DESCRIPTION
                         FROM RADIO.LOGS_OBSERVACIONES;";

                DataTable DTLogs = cData.ObtenerDataTable(sSQL);

                foreach (var Log in lstLogs)
                {
                    DataRow DRL = DTLogs.NewRow();

                    DRL["ID_OBSERVACION"] = Log.ID_OBSERVACION;
                    DRL["LOG_ICON"] = Log.LOG_ICON;
                    DRL["LOG_EVENT"] = Log.LOG_EVENT;
                    DRL["LOG_SOURCE"] = Log.LOG_SOURCE;
                    DRL["LOG_USR_ROLE"] = Log.LOG_USR_ROLE;
                    DRL["LOG_USR_NAME"] = Log.LOG_USR_NAME;
                    DRL["LOG_NUMBER"] = Log.LOG_NUMBER;
                    DRL["LOG_DESCRIPTION"] = Log.LOG_DESCRIPTION;

                    DTLogs.Rows.Add(DRL);
                }

                cData.InsertDataTable(sSQL, DTLogs);
            }
            catch (Exception e)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "Exception e", e.Message);
            }
        }

        private string GetLogUserRole(int USR_ROLE)
        {
            string sResult = "";

            sResult = GetUserRole(USR_ROLE).ToLower();

            return sResult;

        }

        private void AddLogAutomaticAssign(ref RDSDynamicSolicitud DynamicSolicitud, Guid SOL_UID, ref List<ST_RDSLog> lstLogs, LOG_SOURCE Source, ref List<ST_RDSUsuario> AsignacionesGroupX, ref ST_Helper Helper, params ROLES[] FilterArray)
        {
            int ROLE_FILTER = 0; ;

            foreach (int Item in FilterArray)
                ROLE_FILTER |= (int)Item;

            if ((ROLE_FILTER & (int)ROLES.Funcionario) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_FUNCIONARIO_FAIL", Source, ROLES.Funcionario, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_FUNCIONARIO", Source, ROLES.Funcionario, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Funcionario).First().USR_NAME, ""));
            }
            if ((ROLE_FILTER & (int)ROLES.Revisor) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_REVISOR_FAIL", Source, ROLES.Revisor, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_REVISOR", Source, ROLES.Revisor, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Revisor).First().USR_NAME, ""));
            }
            if ((ROLE_FILTER & (int)ROLES.Coordinador) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_COORDINADOR_FAIL", Source, ROLES.Coordinador, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_COORDINADOR", Source, ROLES.Coordinador, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Coordinador).First().USR_NAME, ""));
            }
            if ((ROLE_FILTER & (int)ROLES.Subdirector) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_SUBDIRECTOR_FAIL", Source, ROLES.Subdirector, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_SUBDIRECTOR", Source, ROLES.Subdirector, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Subdirector).First().USR_NAME, ""));
            }
            if ((ROLE_FILTER & (int)ROLES.Asesor) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_ASESOR_FAIL", Source, ROLES.Asesor, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_ASESOR", Source, ROLES.Asesor, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Asesor).First().USR_NAME, ""));
            }
            if ((ROLE_FILTER & (int)ROLES.Director) != 0)
            {
                if (AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First().USR_ID == -1)
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_DIRECTOR_FAIL", Source, ROLES.Director, "", ""));
                else
                    lstLogs.Add(CreateLog(ref DynamicSolicitud, SOL_UID, "LOG_AUTOMATIC_ASSIGN_DIRECTOR", Source, ROLES.Director, AsignacionesGroupX.Where(bp => bp.USR_ROLE == (int)ROLES.Director).First().USR_NAME, ""));
            }
        }
       
        private ST_RDSLog CreateLog(ref RDSDynamicSolicitud DynamicSolicitud, Guid SOL_UID, string LOG_KEY_NAME, int LOG_SOURCE, int LOG_USR_ROLE, string LOG_USR_NAME, string LOG_NUMBER)
        {
            ST_RDSLog RDSLog = new ST_RDSLog();

            RDSLog.SOL_UID = SOL_UID;
            RDSLog.LOG_ICON = DynamicSolicitud.GetHeaderIcon(LOG_KEY_NAME);
            RDSLog.LOG_EVENT = DynamicSolicitud.GetHeaderEvent(LOG_KEY_NAME);
            RDSLog.LOG_SOURCE = LOG_SOURCE;
            RDSLog.LOG_USR_ROLE = LOG_USR_ROLE;
            RDSLog.LOG_USR_NAME = LOG_USR_NAME;
            RDSLog.LOG_NUMBER = LOG_NUMBER;
            RDSLog.LOG_DESCRIPTION = DynamicSolicitud.GetHeaderDesc(LOG_KEY_NAME);

            return RDSLog;
        }

        private ST_RDSLog CreateLog(ref RDSDynamicSolicitud DynamicSolicitud, Guid SOL_UID, string LOG_KEY_NAME, LOG_SOURCE LOG_SOURCE, ROLES LOG_USR_ROL, string LOG_USR_NAME, string LOG_NUMBER)
        {
            ST_RDSLog RDSLog = CreateLog(ref DynamicSolicitud, SOL_UID, LOG_KEY_NAME, (int)LOG_SOURCE, (int)LOG_USR_ROL, LOG_USR_NAME, LOG_NUMBER);
            return RDSLog;
        }

        private ST_RDSLog CreateLogObs(ref RDSDynamicObservacion DynamicObservacion, int ID_OBSERVACION, string LOG_KEY_NAME, int LOG_SOURCE, int LOG_USR_ROLE, string LOG_USR_NAME, string LOG_NUMBER)
        {
            ST_RDSLog RDSLog = new ST_RDSLog();

            RDSLog.ID_OBSERVACION = ID_OBSERVACION;
            RDSLog.LOG_ICON = DynamicObservacion.GetHeaderIcon(LOG_KEY_NAME);
            RDSLog.LOG_EVENT = DynamicObservacion.GetHeaderEvent(LOG_KEY_NAME);
            RDSLog.LOG_SOURCE = LOG_SOURCE;
            RDSLog.LOG_USR_ROLE = LOG_USR_ROLE;
            RDSLog.LOG_USR_NAME = LOG_USR_NAME;
            RDSLog.LOG_NUMBER = LOG_NUMBER;
            RDSLog.LOG_DESCRIPTION = DynamicObservacion.GetHeaderDesc(LOG_KEY_NAME);

            return RDSLog;
        }

        private ST_RDSLog CreateLogObs(ref RDSDynamicObservacion DynamicObservacion, int ID_OBSERVACION, string LOG_KEY_NAME, LOG_SOURCE LOG_SOURCE, ROLES LOG_USR_ROL, string LOG_USR_NAME, string LOG_NUMBER)
        {
            ST_RDSLog RDSLog = CreateLogObs(ref DynamicObservacion, ID_OBSERVACION, LOG_KEY_NAME, (int)LOG_SOURCE, (int)LOG_USR_ROL, LOG_USR_NAME, LOG_NUMBER);
            return RDSLog;
        }

        #endregion

        #region ICSManager

        private bool CrearSolicitudICSManager(ref csData cData, ref ST_RDSSolicitud Solicitud, ref RDSDynamicSolicitud DynamicSolicitud)
        {
            bool bRet = true;
            string sSQL;


            sSQL = @"
                        SELECT Top 0 ID, TABLE_NAME, SRVLIC_ID, SRVLICN_ID, DESCRIPTION, STATUS, CESS_ID, NB_YEARS, MOD_EXE_DATE, COMPLETE, APP_USER, ADM_NAME, REF_OFFI, DATE_OFFI, 
                        FRONT_KEY, NEXT_ANSWER, REMARK, ACT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK, CLASS, ACTIVITY, SERVICE_TYPE
                        FROM [ICSM_PROD].[dbo].P_APPLICATION;
                
                        SELECT Top 0 ID, TABLE_NAME, FILE_ID, TYPE, COMPLETE, STATUS, DESCRIPTION, MEDIA, REF_OFFI, DATE_OFFI, REF_OP, DATE_OP, APP_USER, MESSAGE, REMARK, DATE_CREATED, 
                        CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                        FROM [ICSM_PROD].[dbo].P_LETTER;

                        SELECT Top 0 ID, OBJ_TBNM, OBJ_TBID, EVENT_DATE, [USER], EVENT, AS_TEXT, AS_XML, CF1_ID, FR_STATE, TO_STATE, EVENT_MORE, AS_XMLX
                        FROM [ICSM_PROD].[dbo].REC_HISTORY;

                        SELECT ID, STATUS, VALTRF_PROCESS, DATE_MODIFIED, MODIFIED_BY
                        FROM [ICSM_PROD].[dbo].SERV_LICENCE 
                        WHERE ID=" + Solicitud.Expediente.SERV_ID + @";
            
                        SELECT ID, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK
                        FROM [ICSM_PROD].[dbo].USERS 
                        WHERE ID=" + Solicitud.Expediente.Operador.CUS_ID + @";

                        SELECT Top 0 ID, APPL_ID, LIC_ID, REASON, WISHED_DATE, MESSAGE, RPLAC_ID, FOR_UNDO, DATE_CREATED, CREATED_BY, STATUS 
                        FROM ICSM_PROD.dbo.NT_APPLICATION;

                        SELECT TABLE_NAME, [NEXT]
                        FROM [ICSM_PROD].[dbo].NEXT_ID 
                        WHERE TABLE_NAME In('P_APPLICATION', 'P_LETTER', 'REC_HISTORY', 'NT_APPLICATION')
                        ORDER BY 1;
                        ";


            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DT_P_APPLICATION = dtSet.Tables[0];
            DataTable DT_P_LETTER = dtSet.Tables[1];
            DataTable DT_REC_HISTORY = dtSet.Tables[2];
            DataTable DT_SERV_LICENCE = dtSet.Tables[3];
            DataTable DT_USERS = dtSet.Tables[4];
            DataTable DT_NT_APPLICATION = dtSet.Tables[5];
            DataTable DT_NEXT_ID = dtSet.Tables[6];

            Dictionary<string, decimal> NextID = new Dictionary<string, decimal>();

            NextID = (from bp in DT_NEXT_ID.AsEnumerable().AsQueryable()
                            select new
                            {
                                Key = bp.Field<string>("TABLE_NAME"),
                                Next = bp.Field<decimal>("NEXT")
                            }).ToList().ToDictionary(x => x.Key, x => x.Next);

            DT_NEXT_ID.Rows[0]["NEXT"] = NextID["NT_APPLICATION"] + 1;
            DT_NEXT_ID.Rows[1]["NEXT"] = NextID["P_APPLICATION"] + 1;
            DT_NEXT_ID.Rows[2]["NEXT"] = NextID["P_LETTER"] + 1;
            DT_NEXT_ID.Rows[3]["NEXT"] = NextID["REC_HISTORY"] + 4;

            DataRow DR_P_APPLICATION = DT_P_APPLICATION.NewRow();
            DR_P_APPLICATION["ID"] = NextID["P_APPLICATION"];
            DR_P_APPLICATION["TABLE_NAME"] = "P_APPLICATION";
            DR_P_APPLICATION["SRVLIC_ID"] = Solicitud.Expediente.SERV_ID;
            DR_P_APPLICATION["SRVLICN_ID"] = Solicitud.Expediente.SERV_ID;
            DR_P_APPLICATION["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_APPLICATION["STATUS"] = "sAA";
            DR_P_APPLICATION["CESS_ID"] = (object)DBNull.Value;
            DR_P_APPLICATION["NB_YEARS"] = GetIntValue(DynamicSolicitud.GetKeyTextValue("SOLICITUD_PRORROGA_PERIODO"), 10);
            DR_P_APPLICATION["MOD_EXE_DATE"] = (object)DBNull.Value;
            DR_P_APPLICATION["COMPLETE"] = 0;
            DR_P_APPLICATION["APP_USER"] = (object)DBNull.Value;
            DR_P_APPLICATION["ADM_NAME"] = "PR";
            DR_P_APPLICATION["REF_OFFI"] = Solicitud.Radicado;
            if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.ProrrogaConcesion)
                DR_P_APPLICATION["DATE_OFFI"] = DateTime.Today;
            else
                DR_P_APPLICATION["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture); ;
            DR_P_APPLICATION["FRONT_KEY"] = (object)DBNull.Value; 
            DR_P_APPLICATION["NEXT_ANSWER"] = (object)DBNull.Value;
            DR_P_APPLICATION["REMARK"] = (object)DBNull.Value;
            DR_P_APPLICATION["ACT_ID"] = NextID["P_LETTER"];
            DR_P_APPLICATION["DATE_CREATED"] = DateTime.Now;
            DR_P_APPLICATION["CREATED_BY"] = "sa";
            DR_P_APPLICATION["DATE_MODIFIED"] = DateTime.Now;
            DR_P_APPLICATION["MODIFIED_BY"] = "sa";
            DR_P_APPLICATION["DATE_EVAL"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVALUATOR"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVALUATION"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVAL_REMARK"] = (object)DBNull.Value;
            DR_P_APPLICATION["CLASS"] = (object)DBNull.Value;
            DR_P_APPLICATION["ACTIVITY"] = "A2";
            DR_P_APPLICATION["SERVICE_TYPE"] = "C7";
            DT_P_APPLICATION.Rows.Add(DR_P_APPLICATION);

            DataRow DR_REC_HISTORY1 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY1["ID"] = NextID["REC_HISTORY"];
            DR_REC_HISTORY1["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY1["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY1["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY1["USER"] = "sa";
            DR_REC_HISTORY1["EVENT"] = "Solicitud creada";
            DR_REC_HISTORY1["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY1["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY1["FR_STATE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["TO_STATE"] = "sAA";
            DR_REC_HISTORY1["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY1);

            DataRow DR_P_LETTER = DT_P_LETTER.NewRow();
            DR_P_LETTER["ID"] = NextID["P_LETTER"];
            DR_P_LETTER["TABLE_NAME"] = "P_LETTER";
            DR_P_LETTER["FILE_ID"] = NextID["P_APPLICATION"];
            DR_P_LETTER["TYPE"] = "C_SOL";
            DR_P_LETTER["COMPLETE"] = (object)DBNull.Value;
            DR_P_LETTER["STATUS"] = (object)DBNull.Value;
            DR_P_LETTER["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_LETTER["MEDIA"] = (object)DBNull.Value;
            DR_P_LETTER["REF_OFFI"] = Solicitud.Radicado;
            if (Solicitud.SOL_TYPE_ID == (int)SOL_TYPES.ProrrogaConcesion)
                DR_P_LETTER["DATE_OFFI"] = DateTime.Today;
            else
                DR_P_LETTER["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture);
            DR_P_LETTER["REF_OP"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_OP"] = (object)DBNull.Value;
            DR_P_LETTER["APP_USER"] = (object)DBNull.Value;
            DR_P_LETTER["MESSAGE"] = (object)DBNull.Value;
            DR_P_LETTER["REMARK"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_CREATED"] = DateTime.Now;
            DR_P_LETTER["CREATED_BY"] = "sa";
            DR_P_LETTER["DATE_MODIFIED"] = DateTime.Now;
            DR_P_LETTER["MODIFIED_BY"] = "sa";
            DT_P_LETTER.Rows.Add(DR_P_LETTER);

            DataRow DR_SERV_LICENCE = DT_SERV_LICENCE.Rows[0];
            DR_SERV_LICENCE["STATUS"] = "eSOL";
            if (Solicitud.SOL_IS_OTORGA)
                DR_SERV_LICENCE["VALTRF_PROCESS"] = "OTO";
            else
                DR_SERV_LICENCE["VALTRF_PROCESS"] = "PRO";
            DR_SERV_LICENCE["DATE_MODIFIED"] = DateTime.Now;
            DR_SERV_LICENCE["MODIFIED_BY"] = "sa";

            DataRow DR_REC_HISTORY2 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY2["ID"] = NextID["REC_HISTORY"] + 1;
            DR_REC_HISTORY2["OBJ_TBNM"] = "SERV_LICENCE";
            DR_REC_HISTORY2["OBJ_TBID"] = Solicitud.Expediente.SERV_ID;
            DR_REC_HISTORY2["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY2["USER"] = "sa";
            DR_REC_HISTORY2["EVENT"] = "Solicitud recibida";
            DR_REC_HISTORY2["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY2["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY2["FR_STATE"] = "eAUT";
            DR_REC_HISTORY2["TO_STATE"] = "eSOL";
            DR_REC_HISTORY2["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY2);

            DR_P_APPLICATION = DT_P_APPLICATION.Rows[0];
            DR_P_APPLICATION["STATUS"] = "sTT";
            DR_P_APPLICATION["DATE_MODIFIED"] = DateTime.Now;
            DR_P_APPLICATION["DATE_EVAL"] = DateTime.Now.Date;
            DR_P_APPLICATION["EVALUATOR"] = "sa";
            DR_P_APPLICATION["EVALUATION"] = "P";
            DR_P_APPLICATION["EVAL_REMARK"] = "Análisis Administrativo aprobado";

            DataRow DR_REC_HISTORY3 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY3["ID"] = NextID["REC_HISTORY"] + 2;
            DR_REC_HISTORY3["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY3["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY3["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY3["USER"] = "sa";
            DR_REC_HISTORY3["EVENT"] = "Anal. Administrativo positivo";
            DR_REC_HISTORY3["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY3["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY3["FR_STATE"] = "sAA";
            DR_REC_HISTORY3["TO_STATE"] = "sTT";
            DR_REC_HISTORY3["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY3);

            DataRow DR_USERS = DT_USERS.Rows[0];
            DR_USERS["DATE_EVAL"] = DateTime.Now.Date;
            DR_USERS["EVALUATOR"] = "sa";
            DR_USERS["EVALUATION"] = "P";
            DR_USERS["EVAL_REMARK"] = "Análisis financiero aprobado";


            DataRow DR_REC_HISTORY4 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY4["ID"] = NextID["REC_HISTORY"] + 3;
            DR_REC_HISTORY4["OBJ_TBNM"] = "USERS";
            DR_REC_HISTORY4["OBJ_TBID"] = Solicitud.Expediente.Operador.CUS_ID;
            DR_REC_HISTORY4["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY4["USER"] = "sa";
            DR_REC_HISTORY4["EVENT"] = "Anal. Financiero positivo";
            DR_REC_HISTORY4["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY4["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY4["FR_STATE"] = "uACT";
            DR_REC_HISTORY4["TO_STATE"] = "uACT";
            DR_REC_HISTORY4["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY4);

            DataRow DR_NT_APPLICATION = DT_NT_APPLICATION.NewRow();
            DR_NT_APPLICATION["ID"] = NextID["NT_APPLICATION"];
            DR_NT_APPLICATION["APPL_ID"] = NextID["P_APPLICATION"];
            DR_NT_APPLICATION["LIC_ID"] = (object)DBNull.Value;
            if (Solicitud.SOL_IS_OTORGA)
                DR_NT_APPLICATION["REASON"] = "OTO";
            else
                DR_NT_APPLICATION["REASON"] = "PRO";
            DR_NT_APPLICATION["WISHED_DATE"] = DateTime.Now.AddDays(37).Date;
            DR_NT_APPLICATION["MESSAGE"] = (object)DBNull.Value;
            DR_NT_APPLICATION["RPLAC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["FOR_UNDO"] = (object)DBNull.Value;
            DR_NT_APPLICATION["DATE_CREATED"] = DateTime.Now;
            DR_NT_APPLICATION["CREATED_BY"] = "sa";
            DR_NT_APPLICATION["STATUS"] = "S";
            DT_NT_APPLICATION.Rows.Add(DR_NT_APPLICATION);


            bRet = cData.InsertDataSet(sSQL, dtSet);

            if (!bRet)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed");
            }


            return bRet;
        }

        private bool SubirInfoExcelICSManager(ref csData cData, ref ST_RDSSolicitud Solicitud, ref ST_RDSAnexoTecnico AnexoExcel)//oscarrp
        {
            bool bRet = true;
            string sSQL, sSQL1;


            sSQL = @"
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='ANTENNA';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='LICENCE';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='ADM_EQUIP';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='EQUIP_MW';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MICROWA';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MICROWS';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MW_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='LFMF_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='FM_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='SITE';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='NT_APPLICATION';

                    -- red multipunto--
                    SELECT L.ID ID_LICENCE, F.SITE_ID SITE_FM, LF.SITE_ID SITE_AM, F.ID FM_ID, LF.ID AM_ID,M.EQUIP_ID RELACION_FM, S.EQUIP_ID RELACION_AM,
                    F.EQUIP_ID EQUIPO_FM, F.ANT_ID FM_ANTENA, L.SRVLIC_ID
                    Into #LICENCE_MULTIPTO
                    from ICSM_PROD.dbo.LICENCE L
                    LEFT JOIN ICSM_PROD.dbo.FM_STATION F ON L.ID = F.LIC_ID
                    LEFT JOIN ICSM_PROD.dbo.LFMF_STATION LF ON L.ID = LF.LIC_ID
                    LEFT JOIN ICSM_PROD.dbo.FM_STDEV M ON F.ID = M.STA_ID 
                    LEFT JOIN ICSM_PROD.dbo.LFMF_STDEV S ON LF.ID = S.STA_ID
                    WHERE L.SRVLIC_ID = '" + Solicitud.Expediente.SERV_ID + @"' And L.NUMBER = 1;                   

                    SELECT ID, TABLE_NAME, ONE, TYPE_ID, OWNER_ID, SRVLIC_ID, STATUS, NUMBER, TYPE3, MOD_PROCESS, APP_MOD_ID, 
                    EXEMPTED, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY                    
                    FROM ICSM_PROD.dbo.LICENCE WHERE ID in (Select ID_LICENCE From #LICENCE_MULTIPTO);

                    SELECT ID, TABLE_NAME, CODE, MANUFACTURER, NAME, POLARIZATION, GAIN_TYPE, GAIN, 
                    LOW_FREQ, HIGH_FREQ, REMARK, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY      
                    FROM ICSM_PROD.dbo.ANTENNA WHERE ID in (Select FM_ANTENA From #LICENCE_MULTIPTO);

                    SELECT ID, TABLE_NAME, CODE, MANUFACTURER, NAME, REMARK, LOWER_FREQ, UPPER_FREQ,  
                    DESIG_EMISSION, BW, MIN_POWER, MAX_POWER, ANT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.EQUIP_MW WHERE ID in (Select EQUIPO_FM From #LICENCE_MULTIPTO);
                    
                    SELECT ID, STANDARD, ARI, NAME, DESIG_EM, BW, ANT_ID, EQUIP_ID, AZIMUTH, AGL,                 
                    DATE_MODIFIED, MODIFIED_BY 
                    FROM ICSM_PROD.dbo.FM_STATION WHERE LIC_ID  in (Select ID_LICENCE From #LICENCE_MULTIPTO); 
       
                    SELECT ID, TABLE_NAME, ST_FAMILY, MANUFACTURER, NAME, CATEGORY, REMARK, LOW_FREQ, HIGH_FREQ, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.ADM_EQUIP WHERE ID in (select RELACION_AM from #LICENCE_MULTIPTO )
                    OR ID IN (select RELACION_FM from #LICENCE_MULTIPTO );

                    SELECT ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.FM_STDEV
                    where STA_ID in (select FM_ID from #LICENCE_MULTIPTO );

                    SELECT ID, ADM, STATUS, ST_FAMILY, NAME, STANDARD, FREQ, PLAN_ID, SITE_ID, STPL_TYP, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, TABLE_NAME,
                    ONE, HJ, HN, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.LFMF_STATION WHERE LIC_ID  in (Select ID_LICENCE From #LICENCE_MULTIPTO);

                    SELECT ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.LFMF_STDEV
                    where STA_ID in (select AM_ID from #LICENCE_MULTIPTO );
                    
                    SELECT ID, TABLE_NAME, NAME, CODE, X, Y, CSYS, LONGITUDE, LATITUDE, DATUM, CITY, PROVINCE, X_MIN, X_MAX, Y_MIN, Y_MAX, TYPE, CUST_CHB1,
                    CUST_CHB2, CUST_CHB3, CUST_CHB4, CUST_CHB5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.SITE
                    WHERE ID IN (SELECT SITE_FM FROM #LICENCE_MULTIPTO) OR ID IN (SELECT SITE_AM FROM #LICENCE_MULTIPTO);

                    SELECT L.ID ID_LICENCE, A.ID ID_MICROWA, S.ID ID_MICROWS, A.EQPMW_ID, A.EQPMWB_ID, S.SITE_ID, S.ANT_ID, S.ANT2_ID,
                    M.EQUIP_ID, M.ID RELACION, N.ID ID_NT_APPLICATION, N.APPL_ID
                    Into #LICENCE_PTOAPTO
                    FROM ICSM_PROD.dbo.LICENCE L
                    LEFT JOIN ICSM_PROD.dbo.NT_APPLICATION N ON L.ID = N.LIC_ID
                    LEFT JOIN ICSM_PROD.dbo.MICROWA A ON L.ID = A.LIC_ID
                    LEFT JOIN ICSM_PROD.dbo.MICROWS S ON A.ID = S.MW_ID
                    LEFT JOIN ICSM_PROD.dbo.MW_STDEV M ON S.ID = M.STA_ID
                    WHERE L.SRVLIC_ID = '" + Solicitud.Expediente.SERV_ID + @"' And L.NUMBER = 2;

                    SELECT ID, TABLE_NAME, ONE, TYPE_ID, OWNER_ID, SRVLIC_ID, STATUS, NUMBER, TYPE3, MOD_PROCESS, APP_MOD_ID, 
                    EXEMPTED, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY                    
                    FROM ICSM_PROD.dbo.LICENCE WHERE ID in (Select ID_LICENCE From #LICENCE_PTOAPTO);

                    SELECT ID, APPL_ID, LIC_ID, REASON, WISHED_DATE, MESSAGE, STATUS, RPLAC_ID, FOR_UNDO, DATE_CREATED, CREATED_BY 
                    FROM ICSM_PROD.dbo.NT_APPLICATION WHERE LIC_ID  in (Select ID_LICENCE From #LICENCE_PTOAPTO); 

                    SELECT ID, TABLE_NAME, ST_FAMILY, MANUFACTURER, NAME, CATEGORY, REMARK, LOW_FREQ, HIGH_FREQ, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.ADM_EQUIP WHERE ID in (select EQUIP_ID from #LICENCE_PTOAPTO );

                    SELECT ID, TABLE_NAME, CODE, MANUFACTURER, NAME, REMARK, LOWER_FREQ, UPPER_FREQ, 
                    DESIG_EMISSION, BW, MIN_POWER, MAX_POWER, ANT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.EQUIP_MW WHERE ID in (Select EQPMW_ID From #LICENCE_PTOAPTO) or ID in (Select EQPMWB_ID From #LICENCE_PTOAPTO);
       
                    SELECT ID, TABLE_NAME, ONE, ADM, NAME, PASSIVE, A, PA, PB, B, CLASS, F10_HOUR, DESIG_EM, BW,CHANNEL_SEP, EXCUS_PLAN,
                    EXCUS_BAND, XPIC_ON, ATPC_ON, ATPC, MODUL_ADAPT_ON, EQPMW_ID, EQPMWB_ID, MBITPS, ALLOTM_EXCUS, STATUS, ST_FAMILY, STANDARD,
                    NOTIF_NEEDED, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, CUST_NBR1, C_LONGITUDE, C_LATITUDE, DISTANCE, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.MICROWA WHERE LIC_ID  in (Select ID_LICENCE From #LICENCE_PTOAPTO);

                    SELECT ID, TABLE_NAME, CODE, MANUFACTURER, NAME, POLARIZATION, GAIN_TYPE, GAIN, 
                    LOW_FREQ, HIGH_FREQ, REMARK, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY      
                    FROM ICSM_PROD.dbo.ANTENNA WHERE ID in (Select ANT_ID From #LICENCE_PTOAPTO)
                    OR ID in (Select ANT2_ID From #LICENCE_PTOAPTO);

                    SELECT TOP(1) ID, TABLE_NAME, NAME, CODE, X, Y, CSYS, LONGITUDE, LATITUDE, DATUM, CITY, PROVINCE, X_MIN, X_MAX, Y_MIN, Y_MAX, TYPE, 
                    CUST_CHB1, CUST_CHB2, CUST_CHB3, CUST_CHB4, CUST_CHB5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.SITE
                    WHERE ID in (SELECT SITE_ID FROM #LICENCE_PTOAPTO) ORDER BY DATE_CREATED DESC;

                    SELECT ID, TABLE_NAME, ONE, MW_ID, ROLE, END_ROLE, CALL_SIGN, SITE_ID, TX_FREQ, POLAR, PASSIVE, EXCUS_PAR, ANT_ID, GAIN,
                    POWER, EIRP, RADOM_ATTN,TX_LOSSES, TX_ADDLOSSES, AGL1, AGL2, AZIMUTH, ANGLE_ELEV, MANUAL_ANGLES, COMSIS_STAT, TX_RR_EXC, 
                    TX_RR_SER, ANT2_ID
                    FROM ICSM_PROD.dbo.MICROWS WHERE ID  in (Select ID_MICROWS From #LICENCE_PTOAPTO);

                    SELECT ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.MW_STDEV
                    where STA_ID in (select ID_MICROWS from #LICENCE_PTOAPTO );

                    drop table #LICENCE_MULTIPTO;

                    drop table #LICENCE_PTOAPTO;
                    ";

            sSQL1 = @"
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='ANTENNA';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='LICENCE';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='ADM_EQUIP';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='EQUIP_MW';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MICROWA';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MICROWS';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='MW_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='LFMF_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='FM_STDEV';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='SITE';
                    SELECT TABLE_NAME, NEXT FROM ICSM_PROD.dbo.NEXT_ID WHERE TABLE_NAME='NT_APPLICATION';

                    SELECT TOP 0 ID, TABLE_NAME, ONE, TYPE_ID, OWNER_ID, SRVLIC_ID, STATUS, NUMBER, TYPE3, MOD_PROCESS, APP_MOD_ID, 
                    EXEMPTED, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY                    
                    FROM ICSM_PROD.dbo.LICENCE;

                    SELECT TOP 0 ID, TABLE_NAME, CODE, MANUFACTURER, NAME, POLARIZATION, GAIN_TYPE, GAIN, 
                    LOW_FREQ, HIGH_FREQ, REMARK, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY      
                    FROM ICSM_PROD.dbo.ANTENNA;

                    SELECT TOP 0 ID, TABLE_NAME, CODE, MANUFACTURER, NAME, REMARK, LOWER_FREQ, UPPER_FREQ, 
                    DESIG_EMISSION, BW, MIN_POWER, MAX_POWER, ANT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.EQUIP_MW;
                    
                    SELECT TOP 0 ID, STANDARD, ARI, NAME, DESIG_EM, BW, ANT_ID, EQUIP_ID, AZIMUTH, AGL,                
                    DATE_MODIFIED, MODIFIED_BY 
                    FROM ICSM_PROD.dbo.FM_STATION; 
       
                    SELECT TOP 0 ID, TABLE_NAME, ST_FAMILY, MANUFACTURER, NAME, CATEGORY, REMARK, LOW_FREQ, HIGH_FREQ, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.ADM_EQUIP;

                    SELECT TOP 0 ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.FM_STDEV;

                    SELECT TOP 0 ID, ADM, STATUS, ST_FAMILY, NAME, STANDARD, FREQ, PLAN_ID, SITE_ID, STPL_TYP, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, TABLE_NAME,
                    ONE, HJ, HN, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.LFMF_STATION;

                    SELECT TOP 0 ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.LFMF_STDEV;
                    
                    SELECT TOP 0 ID, TABLE_NAME, NAME, CODE, X, Y, CSYS, LONGITUDE, LATITUDE, DATUM, CITY, PROVINCE, X_MIN, X_MAX, Y_MIN, Y_MAX, TYPE, 
                    CUST_CHB1, CUST_CHB2, CUST_CHB3, CUST_CHB4, CUST_CHB5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.SITE;

                    SELECT TOP 0 ID, TABLE_NAME, ONE, TYPE_ID, OWNER_ID, SRVLIC_ID, STATUS, NUMBER, TYPE3, MOD_PROCESS, APP_MOD_ID, 
                    EXEMPTED, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY                    
                    FROM ICSM_PROD.dbo.LICENCE;
                   
                    SELECT TOP 0 ID, APPL_ID, LIC_ID, REASON, WISHED_DATE, MESSAGE, STATUS, RPLAC_ID, FOR_UNDO, DATE_CREATED, CREATED_BY 
                    FROM ICSM_PROD.dbo.NT_APPLICATION; 

                    SELECT TOP 0 ID, TABLE_NAME, ST_FAMILY, MANUFACTURER, NAME, CATEGORY, REMARK, LOW_FREQ, HIGH_FREQ, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.ADM_EQUIP;

                    SELECT TOP 0 ID, TABLE_NAME, CODE, MANUFACTURER, NAME, REMARK, LOWER_FREQ, UPPER_FREQ, 
                    DESIG_EMISSION, BW, MIN_POWER, MAX_POWER, ANT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.EQUIP_MW;
       
                    SELECT TOP 0 ID, TABLE_NAME, ONE, ADM, NAME, PASSIVE, A, PA, PB, B, CLASS, F10_HOUR, DESIG_EM, BW,CHANNEL_SEP, EXCUS_PLAN,
                    EXCUS_BAND, XPIC_ON, ATPC_ON, ATPC, MODUL_ADAPT_ON, EQPMW_ID, EQPMWB_ID, MBITPS, ALLOTM_EXCUS, STATUS, ST_FAMILY, STANDARD,
                    NOTIF_NEEDED, LIC_ID, OWNER_ID, ITM_ST, ITM_ANS, CUST_NBR1, C_LONGITUDE, C_LATITUDE, DISTANCE, DATE_CREATED, CREATED_BY,
                    DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.MICROWA;

                    SELECT TOP 0 ID, TABLE_NAME, CODE, MANUFACTURER, NAME, POLARIZATION, GAIN_TYPE, GAIN, 
                    LOW_FREQ, HIGH_FREQ, REMARK, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY      
                    FROM ICSM_PROD.dbo.ANTENNA;

                    SELECT TOP 0 ID, TABLE_NAME, NAME, CODE, X, Y, CSYS, LONGITUDE, LATITUDE, DATUM, CITY, PROVINCE, X_MIN, X_MAX, Y_MIN, Y_MAX, TYPE, 
                    CUST_CHB1, CUST_CHB2, CUST_CHB3, CUST_CHB4, CUST_CHB5, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                    FROM ICSM_PROD.dbo.SITE;

                    SELECT TOP 0 ID, TABLE_NAME, ONE, MW_ID, ROLE, END_ROLE, CALL_SIGN, SITE_ID, TX_FREQ, POLAR, PASSIVE, EXCUS_PAR, ANT_ID, GAIN,
                    POWER, EIRP, RADOM_ATTN,TX_LOSSES, TX_ADDLOSSES, AGL1, AGL2, AZIMUTH, ANGLE_ELEV, MANUAL_ANGLES, COMSIS_STAT, TX_RR_EXC, 
                    TX_RR_SER, ANT2_ID
                    FROM ICSM_PROD.dbo.MICROWS;

                    SELECT TOP 0 ID, STA_ID, EQUIP_ID, SERIAL_NO FROM ICSM_PROD.dbo.MW_STDEV;
                    ";


            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DT_NEXT_ID_ANTENA = dtSet.Tables[0];
            DataTable DT_NEXT_ID_LICENCE = dtSet.Tables[1];
            DataTable DT_NEXT_ID_ADM_EQUIP = dtSet.Tables[2];
            DataTable DT_NEXT_ID_EQUIP_MW = dtSet.Tables[3];
            DataTable DT_NEXT_ID_MICROWA = dtSet.Tables[4];
            DataTable DT_NEXT_ID_MICROWS = dtSet.Tables[5];
            DataTable DT_NEXT_ID_MW_STDEV = dtSet.Tables[6];
            DataTable DT_NEXT_ID_LFMF_STDEV = dtSet.Tables[7];
            DataTable DT_NEXT_ID_FM_STDEV = dtSet.Tables[8];
            DataTable DT_NEXT_ID_SITE = dtSet.Tables[9];
            DataTable DT_NEXT_ID_NT_APPLICATION = dtSet.Tables[10];
            DataTable DT_LICENCE = dtSet.Tables[11];
            DataTable DT_ANTENA = dtSet.Tables[12];
            DataTable DT_EQUIP_MW = dtSet.Tables[13];
            DataTable DT_FM_STATION = dtSet.Tables[14];
            DataTable DT_ADM_EQUIP = dtSet.Tables[15];
            DataTable DT_FM_STDEV = dtSet.Tables[16];
            DataTable DT_LFMF_STATION = dtSet.Tables[17];
            DataTable DT_LFMF_STDEV = dtSet.Tables[18];
            DataTable DT_SITE = dtSet.Tables[19];
            DataTable DT_LICENCE1 = dtSet.Tables[20];
            DataTable DT_NT_APPLICATION = dtSet.Tables[21];
            DataTable DT_ADM_EQUIP1 = dtSet.Tables[22];
            DataTable DT_EQUIP_MW1 = dtSet.Tables[23];
            DataTable DT_MICROWA = dtSet.Tables[24];
            DataTable DT_ANTENA1 = dtSet.Tables[25];
            DataTable DT_SITE1 = dtSet.Tables[26];
            DataTable DT_MICROWS = dtSet.Tables[27];
            DataTable DT_MW_STDEV = dtSet.Tables[28];

            DateTime horaCambio = DateTime.Now;

            string d = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
            string m = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator;

            DT_SITE.Rows[0]["NAME"] = AnexoExcel.CERTIF_AERONAUT.DIR_UBICACION;
            DT_SITE.Rows[0]["X"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LONG_GRAD + "," + AnexoExcel.SISTEMA_RADIANTE.LONG_MIN.PadLeft(2, '0') + AnexoExcel.SISTEMA_RADIANTE.LONG_SEG.PadLeft(2, '0'));
            DT_SITE.Rows[0]["Y"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LAT_GRAD + "," + AnexoExcel.SISTEMA_RADIANTE.LAT_MIN.PadLeft(2, '0') + AnexoExcel.SISTEMA_RADIANTE.LAT_SEG.PadLeft(2, '0'));
            DT_SITE.Rows[0]["LONGITUDE"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LONGITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["LATITUDE"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LATITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["CITY"] = AnexoExcel.SISTEMA_RADIANTE.MUNICIPIO;
            DT_SITE.Rows[0]["PROVINCE"] = GetCodeDeparment(ref cData, AnexoExcel.SISTEMA_RADIANTE.DEPARTAMENTO.ToUpper());
            DT_SITE.Rows[0]["X_MIN"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LONGITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["X_MAX"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LONGITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["Y_MIN"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LATITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["Y_MAX"] = Convert.ToDecimal(AnexoExcel.SISTEMA_RADIANTE.LATITUD.Replace(m[0], d[0]));
            DT_SITE.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_SITE.Rows[0]["MODIFIED_BY"] = "sa";

            if (DT_FM_STATION.Rows.Count > 0)
            {
                if (DT_EQUIP_MW.Rows.Count == 0)
                {
                    var IdEquipMw = DT_NEXT_ID_EQUIP_MW.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_EQUIP_MW.Rows[0]["NEXT"] = IdEquipMw + 1;
                    DataRow DR_EQUIP_MW = DT_EQUIP_MW.NewRow();
                    DR_EQUIP_MW["ID"] = IdEquipMw;
                    DR_EQUIP_MW["TABLE_NAME"] = "EQUIP_MW";
                    DR_EQUIP_MW["CODE"] = IdEquipMw.ToString();
                    DR_EQUIP_MW["DATE_CREATED"] = horaCambio;
                    DR_EQUIP_MW["CREATED_BY"] = "sa";
                    DT_EQUIP_MW.Rows.Add(DR_EQUIP_MW);
                }
                DT_EQUIP_MW.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.MARCA;
                DT_EQUIP_MW.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.MODELO;
                DT_EQUIP_MW.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.OBSERVACION;
                DT_EQUIP_MW.Rows[0]["LOWER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.FREQ_INF.Replace(m[0], d[0]));
                DT_EQUIP_MW.Rows[0]["UPPER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.FREQ_SUP.Replace(m[0], d[0]));
                DT_EQUIP_MW.Rows[0]["DESIG_EMISSION"] = AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA;
                DT_EQUIP_MW.Rows[0]["BW"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA.Substring(0, 3));
                DT_EQUIP_MW.Rows[0]["MIN_POWER"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.POT_INF.Replace(m[0], d[0]));
                DT_EQUIP_MW.Rows[0]["MAX_POWER"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.POT_SUP.Replace(m[0], d[0]));
                DT_EQUIP_MW.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_EQUIP_MW.Rows[0]["MODIFIED_BY"] = "sa";

                DataRow DR_ANTENA;
                if (DT_ADM_EQUIP.Rows.Count == 0)
                {
                    var IdAntena = DT_NEXT_ID_ANTENA.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ANTENA.Rows[0]["NEXT"] = IdAntena + 1;
                    DR_ANTENA = DT_ANTENA.NewRow();
                    DR_ANTENA["ID"] = IdAntena;
                    DR_ANTENA["TABLE_NAME"] = "ANTENA";
                    DR_ANTENA["CODE"] = IdAntena.ToString();
                    DR_ANTENA["DATE_CREATED"] = horaCambio;
                    DR_ANTENA["CREATED_BY"] = "sa";
                    DT_ANTENA.Rows.Add(DR_ANTENA);
                }
                DT_ANTENA.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.MARCA;
                DT_ANTENA.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.MODELO;
                var polarizacion = AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.POLARIZACION.Substring(0, 1);
                if (polarizacion == "C")
                    polarizacion = "CX";
                if (polarizacion == "O")
                    polarizacion = "C-C";
                DT_ANTENA.Rows[0]["POLARIZATION"] = polarizacion;
                DT_ANTENA.Rows[0]["GAIN_TYPE"] = AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.GAIN_UNIT.Substring(0, 1);
                DT_ANTENA.Rows[0]["GAIN"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.GAIN_VALOR.Replace(m[0], d[0]));
                DT_ANTENA.Rows[0]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.FREQ_INF.Replace(m[0], d[0]));
                DT_ANTENA.Rows[0]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.FREQ_SUP.Replace(m[0], d[0]));
                DT_ANTENA.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.ANTENA_TX.OBSERVACION;
                DT_ANTENA.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_ANTENA.Rows[0]["MODIFIED_BY"] = "sa";
                DT_FM_STATION.Rows[0]["NAME"] = AnexoExcel.CONSECIONARIO.STATION_NAME;
                DT_FM_STATION.Rows[0]["EQUIP_ID"] = DT_EQUIP_MW.Rows[0]["ID"];
                DT_FM_STATION.Rows[0]["AZIMUTH"] = Convert.ToDecimal(AnexoExcel.PATRON_RADIACION.AZIMUT.Replace(m[0], d[0]));
                DT_FM_STATION.Rows[0]["AGL"] = Convert.ToDecimal(AnexoExcel.PATRON_RADIACION.ALTURA_TORRE.Replace(m[0], d[0]));
                DT_FM_STATION.Rows[0]["ANT_ID"] = DT_ANTENA.Rows[0]["ID"];
                DT_FM_STATION.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_FM_STATION.Rows[0]["MODIFIED_BY"] = "sa";
                DT_EQUIP_MW.Rows[0]["ANT_ID"] = DT_ANTENA.Rows[0]["ID"];

                if (DT_FM_STDEV.Rows.Count == 0)
                {
                    var IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP1 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP1["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP1["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP1["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP1["CATEGORY"] = "LINE_TX";
                    DR_ADM_EQUIP1["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP1["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP1);

                    DataRow DR_FM_STDEV1 = DT_FM_STDEV.NewRow();
                    var IdFmStdev = DT_NEXT_ID_FM_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_FM_STDEV.Rows[0]["NEXT"] = IdFmStdev + 1;
                    DR_FM_STDEV1["ID"] = IdFmStdev;
                    DR_FM_STDEV1["STA_ID"] = DT_FM_STATION.Rows[0]["ID"];
                    DR_FM_STDEV1["EQUIP_ID"] = IdAdmEquip;
                    DR_FM_STDEV1["SERIAL_NO"] = "LINEA DE TRANSMISIÓN";
                    DT_FM_STDEV.Rows.Add(DR_FM_STDEV1);

                    IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP2 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP2["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP2["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP2["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP2["CATEGORY"] = "MONITOR MODUL";
                    DR_ADM_EQUIP2["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP2["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP2);

                    DataRow DR_FM_STDEV2 = DT_FM_STDEV.NewRow();
                    IdFmStdev = DT_NEXT_ID_FM_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_FM_STDEV.Rows[0]["NEXT"] = IdFmStdev + 1;
                    DR_FM_STDEV2["ID"] = IdFmStdev;
                    DR_FM_STDEV2["STA_ID"] = DT_FM_STATION.Rows[0]["ID"];
                    DR_FM_STDEV2["EQUIP_ID"] = IdAdmEquip;
                    DR_FM_STDEV2["SERIAL_NO"] = "MONITOR DE MODULACIÓN";
                    DT_FM_STDEV.Rows.Add(DR_FM_STDEV2);

                    IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP3 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP3["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP3["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP3["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP3["CATEGORY"] = "MONITOR FREC";
                    DR_ADM_EQUIP3["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP3["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP3);

                    DataRow DR_FM_STDEV3 = DT_FM_STDEV.NewRow();
                    IdFmStdev = DT_NEXT_ID_FM_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_FM_STDEV.Rows[0]["NEXT"] = IdFmStdev + 1;
                    DR_FM_STDEV3["ID"] = IdFmStdev;
                    DR_FM_STDEV3["STA_ID"] = DT_FM_STATION.Rows[0]["ID"];
                    DR_FM_STDEV3["EQUIP_ID"] = IdAdmEquip;
                    DR_FM_STDEV3["SERIAL_NO"] = "MONITOR DE FRECUENCIA";
                    DT_FM_STDEV.Rows.Add(DR_FM_STDEV3);
                }
                DT_ADM_EQUIP.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.MARCA;
                DT_ADM_EQUIP.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.MODELO;
                DT_ADM_EQUIP.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.OBSERVACION;
                DT_ADM_EQUIP.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[0]["MODIFIED_BY"] = "sa";

                DT_ADM_EQUIP.Rows[1]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MARCA;
                DT_ADM_EQUIP.Rows[1]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MODELO;
                DT_ADM_EQUIP.Rows[1]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.OBSERVACION;
                DT_ADM_EQUIP.Rows[1]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_INF.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[1]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_SUP.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[1]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[1]["MODIFIED_BY"] = "sa";

                DT_ADM_EQUIP.Rows[2]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MARCA;
                DT_ADM_EQUIP.Rows[2]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MODELO;
                DT_ADM_EQUIP.Rows[2]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.OBSERVACION;
                DT_ADM_EQUIP.Rows[2]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_INF.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[2]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_SUP.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[2]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[2]["MODIFIED_BY"] = "sa";
            }
            else if (DT_LFMF_STATION.Rows.Count > 0)
            {
                DT_LFMF_STATION.Rows[0]["NAME"] = AnexoExcel.CONSECIONARIO.STATION_NAME;
                DT_LFMF_STATION.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_LFMF_STATION.Rows[0]["MODIFIED_BY"] = "sa";
                if (DT_ADM_EQUIP.Rows.Count == 0)
                {
                    var IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP1 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP1["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP1["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP1["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP1["CATEGORY"] = "LINE_TX";
                    DR_ADM_EQUIP1["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP1["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP1);

                    DataRow DR_LFMF_STDEV1 = DT_LFMF_STDEV.NewRow();
                    var IdLfmfStdev = DT_NEXT_ID_LFMF_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_LFMF_STDEV.Rows[0]["NEXT"] = IdLfmfStdev + 1;
                    DR_LFMF_STDEV1["ID"] = IdLfmfStdev;
                    DR_LFMF_STDEV1["STA_ID"] = DT_LFMF_STATION.Rows[0]["ID"];
                    DR_LFMF_STDEV1["EQUIP_ID"] = IdAdmEquip;
                    DR_LFMF_STDEV1["SERIAL_NO"] = "LINEA DE TRANSMISIÓN";
                    DT_LFMF_STDEV.Rows.Add(DR_LFMF_STDEV1);

                    IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP2 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP2["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP2["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP2["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP2["CATEGORY"] = "MONITOR MODUL";
                    DR_ADM_EQUIP2["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP2["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP2);

                    DataRow DR_LFMF_STDEV2 = DT_LFMF_STDEV.NewRow();
                    IdLfmfStdev = DT_NEXT_ID_LFMF_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_LFMF_STDEV.Rows[0]["NEXT"] = IdLfmfStdev + 1;
                    DR_LFMF_STDEV2["ID"] = IdLfmfStdev;
                    DR_LFMF_STDEV2["STA_ID"] = DT_LFMF_STATION.Rows[0]["ID"];
                    DR_LFMF_STDEV2["EQUIP_ID"] = IdAdmEquip;
                    DR_LFMF_STDEV2["SERIAL_NO"] = "MONITOR DE MODULACIÓN";
                    DT_LFMF_STDEV.Rows.Add(DR_LFMF_STDEV2);

                    IdAdmEquip = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip + 1;
                    DataRow DR_ADM_EQUIP3 = DT_ADM_EQUIP.NewRow();
                    DR_ADM_EQUIP3["ID"] = IdAdmEquip;
                    DR_ADM_EQUIP3["ST_FAMILY"] = "X";
                    DR_ADM_EQUIP3["TABLE_NAME"] = "ADM_EQUIP";
                    DR_ADM_EQUIP3["CATEGORY"] = "MONITOR FREC";
                    DR_ADM_EQUIP3["DATE_CREATED"] = horaCambio;
                    DR_ADM_EQUIP3["CREATED_BY"] = "sa";
                    DT_ADM_EQUIP.Rows.Add(DR_ADM_EQUIP3);

                    DataRow DR_LFMF_STDEV3 = DT_LFMF_STDEV.NewRow();
                    IdLfmfStdev = DT_NEXT_ID_LFMF_STDEV.Rows[0].Field<decimal>("NEXT");
                    DT_NEXT_ID_LFMF_STDEV.Rows[0]["NEXT"] = IdLfmfStdev + 1;
                    DR_LFMF_STDEV3["ID"] = IdLfmfStdev;
                    DR_LFMF_STDEV3["STA_ID"] = DT_LFMF_STATION.Rows[0]["ID"];
                    DR_LFMF_STDEV3["EQUIP_ID"] = IdAdmEquip;
                    DR_LFMF_STDEV3["SERIAL_NO"] = "MONITOR DE FRECUENCIA";
                    DT_LFMF_STDEV.Rows.Add(DR_LFMF_STDEV3);
                }
                DT_ADM_EQUIP.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.MARCA;
                DT_ADM_EQUIP.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.MODELO;
                DT_ADM_EQUIP.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.LINEA_TX.OBSERVACION;
                DT_ADM_EQUIP.Rows[0]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[0]["MODIFIED_BY"] = "sa";

                DT_ADM_EQUIP.Rows[1]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MARCA;
                DT_ADM_EQUIP.Rows[1]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.MODELO;
                DT_ADM_EQUIP.Rows[1]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.OBSERVACION;
                DT_ADM_EQUIP.Rows[1]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_INF.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[1]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_MODULACION.FREQ_SUP.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[1]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[1]["MODIFIED_BY"] = "sa";

                DT_ADM_EQUIP.Rows[2]["MANUFACTURER"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MARCA;
                DT_ADM_EQUIP.Rows[2]["NAME"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.MODELO;
                DT_ADM_EQUIP.Rows[2]["REMARK"] = AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.OBSERVACION;
                DT_ADM_EQUIP.Rows[2]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_INF.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[2]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.MONITOR_FREQ.FREQ_SUP.Replace(m[0], d[0]));
                DT_ADM_EQUIP.Rows[2]["DATE_MODIFIED"] = horaCambio;
                DT_ADM_EQUIP.Rows[2]["MODIFIED_BY"] = "sa";
            }

            if (DT_LICENCE1.Rows.Count == 0)
            {
                decimal idLicence = DT_NEXT_ID_LICENCE.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_LICENCE.Rows[0]["NEXT"] = idLicence + 1;

                DataRow DR_LICENCE1 = DT_LICENCE1.NewRow();
                DR_LICENCE1["ID"] = idLicence;
                DR_LICENCE1["TABLE_NAME"] = "LICENCE";
                DR_LICENCE1["ONE"] = 1;
                DR_LICENCE1["TYPE_ID"] = 2;
                DR_LICENCE1["OWNER_ID"] = Solicitud.Expediente.Operador.CUS_ID;
                DR_LICENCE1["SRVLIC_ID"] = Solicitud.Expediente.SERV_ID;
                DR_LICENCE1["STATUS"] = "rTT";
                DR_LICENCE1["NUMBER"] = 2;
                DR_LICENCE1["TYPE3"] = "P";
                DR_LICENCE1["MOD_PROCESS"] = "OTO";
                DR_LICENCE1["APP_MOD_ID"] = DT_LICENCE.Rows[0].Field<decimal>("APP_MOD_ID");
                DR_LICENCE1["DATE_CREATED"] = horaCambio;
                DR_LICENCE1["CREATED_BY"] = "sa";
                DR_LICENCE1["DATE_MODIFIED"] = horaCambio;
                DR_LICENCE1["MODIFIED_BY"] = "sa";
                DT_LICENCE1.Rows.Add(DR_LICENCE1);

                var idNtAplication = DT_NEXT_ID_NT_APPLICATION.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_NT_APPLICATION.Rows[0]["NEXT"] = idNtAplication + 1;

                DataRow DR_NT_APPLICATION = DT_NT_APPLICATION.NewRow();
                DR_NT_APPLICATION["ID"] = idNtAplication;
                DR_NT_APPLICATION["APPL_ID"] = DT_LICENCE.Rows[0].Field<decimal>("APP_MOD_ID");
                DR_NT_APPLICATION["LIC_ID"] = idLicence;
                DR_NT_APPLICATION["REASON"] = "OTO";
                DR_NT_APPLICATION["STATUS"] = "S";
                DR_NT_APPLICATION["WISHED_DATE"] = DateTime.Now.AddDays(37).Date;
                DR_NT_APPLICATION["MESSAGE"] = (object)DBNull.Value;
                DR_NT_APPLICATION["RPLAC_ID"] = (object)DBNull.Value;
                DR_NT_APPLICATION["FOR_UNDO"] = (object)DBNull.Value;
                DR_NT_APPLICATION["DATE_CREATED"] = horaCambio;
                DR_NT_APPLICATION["CREATED_BY"] = "sa";
                DT_NT_APPLICATION.Rows.Add(DR_NT_APPLICATION);

                var IdEquipMw = DT_NEXT_ID_EQUIP_MW.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_EQUIP_MW.Rows[0]["NEXT"] = IdEquipMw + 1;
                DataRow DR_EQUIP_MW1 = DT_EQUIP_MW1.NewRow();
                DR_EQUIP_MW1["ID"] = IdEquipMw;
                DR_EQUIP_MW1["TABLE_NAME"] = "EQUIP_MW";
                DR_EQUIP_MW1["CODE"] = IdEquipMw.ToString();
                DR_EQUIP_MW1["DATE_CREATED"] = horaCambio;
                DR_EQUIP_MW1["CREATED_BY"] = "sa";
                DT_EQUIP_MW1.Rows.Add(DR_EQUIP_MW1);

                var IdEquipMw2 = DT_NEXT_ID_EQUIP_MW.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_EQUIP_MW.Rows[0]["NEXT"] = IdEquipMw2 + 1;
                DataRow DR_EQUIP_MW2 = DT_EQUIP_MW1.NewRow();
                DR_EQUIP_MW2["ID"] = IdEquipMw2;
                DR_EQUIP_MW2["TABLE_NAME"] = "EQUIP_MW";
                DR_EQUIP_MW2["CODE"] = IdEquipMw2.ToString();
                DR_EQUIP_MW2["DATE_CREATED"] = horaCambio;
                DR_EQUIP_MW2["CREATED_BY"] = "sa";
                DT_EQUIP_MW1.Rows.Add(DR_EQUIP_MW2);

                var idMicrowa = DT_NEXT_ID_MICROWA.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_MICROWA.Rows[0]["NEXT"] = idMicrowa + 1;

                DataRow DR_MICROWA = DT_MICROWA.NewRow();
                DR_MICROWA["ID"] = idMicrowa;
                DR_MICROWA["TABLE_NAME"] = "MICROWA";
                DR_MICROWA["ONE"] = 1;
                DR_MICROWA["ADM"] = "CLM";
                DR_MICROWA["NAME"] = "E0" + idMicrowa.ToString();
                DR_MICROWA["PASSIVE"] = 0;
                DR_MICROWA["A"] = "A";
                DR_MICROWA["PA"] = "PA";
                DR_MICROWA["B"] = "B";
                DR_MICROWA["PB"] = "PB";
                DR_MICROWA["CLASS"] = "FX";
                DR_MICROWA["F10_HOUR"] = "HX";
                DR_MICROWA["EXCUS_PLAN"] = "N";
                DR_MICROWA["EXCUS_BAND"] = "N";
                DR_MICROWA["XPIC_ON"] = "N";
                DR_MICROWA["ATPC_ON"] = "N";
                DR_MICROWA["ATPC"] = 0;
                DR_MICROWA["EQPMW_ID"] = IdEquipMw;
                DR_MICROWA["EQPMWB_ID"] = IdEquipMw2;
                DR_MICROWA["ALLOTM_EXCUS"] = "N";
                DR_MICROWA["STATUS"] = "aSOL";
                DR_MICROWA["ST_FAMILY"] = "U";
                DR_MICROWA["STANDARD"] = "2";
                DR_MICROWA["NOTIF_NEEDED"] = "Y";
                DR_MICROWA["LIC_ID"] = idLicence;
                DR_MICROWA["OWNER_ID"] = Solicitud.Expediente.Operador.CUS_ID;
                DR_MICROWA["DATE_CREATED"] = horaCambio;
                DR_MICROWA["CREATED_BY"] = "sa";
                DT_MICROWA.Rows.Add(DR_MICROWA);

                var IdAntenaTx = DT_NEXT_ID_ANTENA.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_ANTENA.Rows[0]["NEXT"] = IdAntenaTx + 1;
                DataRow DR_ANTENATX = DT_ANTENA1.NewRow();
                DR_ANTENATX["ID"] = IdAntenaTx;
                DR_ANTENATX["TABLE_NAME"] = "ANTENA";
                DR_ANTENATX["CODE"] = IdAntenaTx.ToString();
                DR_ANTENATX["DATE_CREATED"] = horaCambio;
                DR_ANTENATX["CREATED_BY"] = "sa";
                DT_ANTENA1.Rows.Add(DR_ANTENATX);

                var IdAntenaRx = DT_NEXT_ID_ANTENA.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_ANTENA.Rows[0]["NEXT"] = IdAntenaRx + 1;
                DataRow DR_ANTENARX = DT_ANTENA1.NewRow();
                DR_ANTENARX["ID"] = IdAntenaRx;
                DR_ANTENARX["TABLE_NAME"] = "ANTENA";
                DR_ANTENARX["CODE"] = IdAntenaRx.ToString();
                DR_ANTENARX["DATE_CREATED"] = horaCambio;
                DR_ANTENARX["CREATED_BY"] = "sa";
                DT_ANTENA1.Rows.Add(DR_ANTENARX);

                var idMicrows = DT_NEXT_ID_MICROWS.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_MICROWS.Rows[0]["NEXT"] = idMicrows + 1;

                DataRow DR_MICROWS1 = DT_MICROWS.NewRow();
                DR_MICROWS1["ID"] = idMicrows;
                DR_MICROWS1["TABLE_NAME"] = "MICROWS";
                DR_MICROWS1["ONE"] = 1;
                DR_MICROWS1["MW_ID"] = idMicrowa;
                DR_MICROWS1["ROLE"] = "A";
                DR_MICROWS1["END_ROLE"] = "B";
                DR_MICROWS1["CALL_SIGN"] = Solicitud.Expediente.CALL_SIGN;
                DR_MICROWS1["SITE_ID"] = DT_SITE.Rows[0]["ID"];
                DR_MICROWS1["PASSIVE"] = 0;
                DR_MICROWS1["EXCUS_PAR"] = "N";
                DR_MICROWS1["ANT_ID"] = IdAntenaTx;
                DR_MICROWS1["RADOM_ATTN"] = 0;
                DR_MICROWS1["TX_LOSSES"] = 0;
                DR_MICROWS1["TX_ADDLOSSES"] = 0;
                DR_MICROWS1["MANUAL_ANGLES"] = "N";
                DR_MICROWS1["ANT2_ID"] = IdAntenaRx;
                DR_MICROWS1["COMSIS_STAT"] = "SM;AM";
                DR_MICROWS1["TX_RR_EXC"] = "N";
                DR_MICROWS1["TX_RR_SER"] = "F";
                DT_MICROWS.Rows.Add(DR_MICROWS1);

                var IdSite1 = DT_NEXT_ID_SITE.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_SITE.Rows[0]["NEXT"] = IdSite1 + 1;
                DataRow DR_SITE1 = DT_SITE1.NewRow();
                DR_SITE1["ID"] = IdSite1;
                DR_SITE1["TABLE_NAME"] = "SITE";
                DR_SITE1["NAME"] = "Sitio " + IdSite1.ToString(CultureInfo.InvariantCulture);
                DR_SITE1["CODE"] = IdSite1.ToString(CultureInfo.InvariantCulture);
                DR_SITE1["CSYS"] = "4DMS";
                DR_SITE1["DATUM"] = 4;
                DR_SITE1["TYPE"] = "L";
                DR_SITE1["CUST_CHB1"] = 0;
                DR_SITE1["CUST_CHB2"] = 0;
                DR_SITE1["CUST_CHB3"] = 0;
                DR_SITE1["CUST_CHB4"] = 0;
                DR_SITE1["CUST_CHB5"] = 0;
                DR_SITE1["DATE_CREATED"] = horaCambio;
                DR_SITE1["CREATED_BY"] = "sa";
                DT_SITE1.Rows.Add(DR_SITE1);

                idMicrows = DT_NEXT_ID_MICROWS.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_MICROWS.Rows[0]["NEXT"] = idMicrows + 1;

                DataRow DR_MICROWS = DT_MICROWS.NewRow();
                DR_MICROWS["ID"] = idMicrows;
                DR_MICROWS["TABLE_NAME"] = "MICROWS";
                DR_MICROWS["ONE"] = 1;
                DR_MICROWS["MW_ID"] = idMicrowa;
                DR_MICROWS["ROLE"] = "B";
                DR_MICROWS["END_ROLE"] = "A";
                DR_MICROWS["CALL_SIGN"] = Solicitud.Expediente.CALL_SIGN;
                DR_MICROWS["SITE_ID"] = IdSite1;
                DR_MICROWS["PASSIVE"] = 0;
                DR_MICROWS["EXCUS_PAR"] = "N";
                DR_MICROWS["ANT_ID"] = IdAntenaRx;
                DR_MICROWS["RADOM_ATTN"] = 0;
                DR_MICROWS["TX_LOSSES"] = 0;
                DR_MICROWS["TX_ADDLOSSES"] = 0;
                DR_MICROWS["MANUAL_ANGLES"] = "N";
                DR_MICROWS["ANT2_ID"] = IdAntenaTx;
                DR_MICROWS["COMSIS_STAT"] = "SM;AM";
                DR_MICROWS["TX_RR_EXC"] = "N";
                DR_MICROWS["TX_RR_SER"] = "F";
                DT_MICROWS.Rows.Add(DR_MICROWS);

                var IdAdmEquip1 = DT_NEXT_ID_ADM_EQUIP.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_ADM_EQUIP.Rows[0]["NEXT"] = IdAdmEquip1 + 1;
                DataRow DR_ADM_EQUIPMS = DT_ADM_EQUIP1.NewRow();
                DR_ADM_EQUIPMS["ID"] = IdAdmEquip1;
                DR_ADM_EQUIPMS["ST_FAMILY"] = "X";
                DR_ADM_EQUIPMS["TABLE_NAME"] = "ADM_EQUIP";
                DR_ADM_EQUIPMS["CATEGORY"] = "LINE_TX";
                DR_ADM_EQUIPMS["DATE_CREATED"] = horaCambio;
                DR_ADM_EQUIPMS["CREATED_BY"] = "sa";
                DT_ADM_EQUIP1.Rows.Add(DR_ADM_EQUIPMS);

                DataRow DR_MW_STDEV = DT_MW_STDEV.NewRow();
                var IdMwStdev = DT_NEXT_ID_MW_STDEV.Rows[0].Field<decimal>("NEXT");
                DT_NEXT_ID_MW_STDEV.Rows[0]["NEXT"] = IdMwStdev + 1;
                DR_MW_STDEV["ID"] = IdMwStdev;
                DR_MW_STDEV["STA_ID"] = idMicrows;
                DR_MW_STDEV["EQUIP_ID"] = IdAdmEquip1;
                DR_MW_STDEV["SERIAL_NO"] = "LINEA DE TRANSMISIÓN";
                DT_MW_STDEV.Rows.Add(DR_MW_STDEV);
            }

            DT_MICROWA.Rows[0]["DESIG_EM"] = AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA;
            DT_MICROWA.Rows[0]["BW"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA.Substring(0, 3));
            DT_MICROWA.Rows[0]["CHANNEL_SEP"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_RED_COBERTURA.EQUIPO_TX.ANCHO_BANDA.Substring(0, 3));
            DT_MICROWA.Rows[0]["CUST_NBR1"] = Convert.ToDouble(AnexoExcel.CERTIF_AERONAUT.ELEVACION);
            DT_MICROWA.Rows[0]["C_LONGITUDE"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.TX_LONGITUD.Replace(m[0], d[0]));
            DT_MICROWA.Rows[0]["C_LATITUDE"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.TX_LATITUD.Replace(m[0], d[0]));
            DT_MICROWA.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_MICROWA.Rows[0]["MODIFIED_BY"] = "sa";

            DT_EQUIP_MW1.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.MARCA;
            DT_EQUIP_MW1.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.MODELO;
            DT_EQUIP_MW1.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.OBSERVACION;
            DT_EQUIP_MW1.Rows[0]["LOWER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_INF.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[0]["UPPER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_SUP.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[0]["DESIG_EMISSION"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.ANCHO_BANDA;
            DT_EQUIP_MW1.Rows[0]["BW"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.ANCHO_BANDA.Substring(0, 3));
            DT_EQUIP_MW1.Rows[0]["MIN_POWER"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.POT_INF.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[0]["MAX_POWER"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.POT_SUP.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_EQUIP_MW1.Rows[0]["MODIFIED_BY"] = "sa";

            DT_EQUIP_MW1.Rows[1]["MANUFACTURER"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.MARCA;
            DT_EQUIP_MW1.Rows[1]["NAME"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.MODELO;
            DT_EQUIP_MW1.Rows[1]["REMARK"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.OBSERVACION;
            DT_EQUIP_MW1.Rows[1]["LOWER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_INF.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[1]["UPPER_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_SUP.Replace(m[0], d[0]));
            DT_EQUIP_MW1.Rows[1]["DESIG_EMISSION"] = AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.ANCHO_BANDA;
            DT_EQUIP_MW1.Rows[1]["DATE_MODIFIED"] = horaCambio;
            DT_EQUIP_MW1.Rows[1]["MODIFIED_BY"] = "sa";

            DT_SITE1.Rows[0]["X"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LONG_GRAD + "," + AnexoExcel.ENLACE_PTOAPTO.RX_LONG_MIN.PadLeft(2, '0') + AnexoExcel.ENLACE_PTOAPTO.RX_LONG_SEG.PadLeft(2, '0'));
            DT_SITE1.Rows[0]["Y"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LAT_GRA + "," + AnexoExcel.ENLACE_PTOAPTO.RX_LAT_MIN.PadLeft(2, '0') + AnexoExcel.ENLACE_PTOAPTO.RX_LAT_SEG.PadLeft(2, '0'));
            DT_SITE1.Rows[0]["LONGITUDE"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LONGITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["LATITUDE"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LATITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["CITY"] = (object)DBNull.Value;
            DT_SITE1.Rows[0]["PROVINCE"] = (object)DBNull.Value;
            DT_SITE1.Rows[0]["X_MIN"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LONGITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["X_MAX"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LONGITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["Y_MIN"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LATITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["Y_MAX"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_LATITUD.Replace(m[0], d[0]));
            DT_SITE1.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_SITE1.Rows[0]["MODIFIED_BY"] = "sa";

            DT_ANTENA1.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_TX.MARCA;
            DT_ANTENA1.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_TX.MODELO;
            DT_ANTENA1.Rows[0]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_INF.Replace(m[0], d[0]));
            DT_ANTENA1.Rows[0]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_TX.FREQ_SUP.Replace(m[0], d[0]));
            var polarizacion1 = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_TX.POLARIZACION.Substring(0, 1);
            if (polarizacion1 == "C")
                polarizacion1 = "CX";
            if (polarizacion1 == "O")
                polarizacion1 = "C-C";
            DT_ANTENA1.Rows[0]["POLARIZATION"] = polarizacion1;
            DT_ANTENA1.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_TX.OBSERVACION;
            DT_ANTENA1.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_ANTENA1.Rows[0]["MODIFIED_BY"] = "sa";

            DT_ANTENA1.Rows[1]["MANUFACTURER"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_RX.MARCA;
            DT_ANTENA1.Rows[1]["NAME"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_RX.MODELO;
            DT_ANTENA1.Rows[1]["LOW_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_INF.Replace(m[0], d[0]));
            DT_ANTENA1.Rows[1]["HIGH_FREQ"] = Convert.ToDecimal(AnexoExcel.EQUIPOS_PTOAPTO.EQUIPO_RX.FREQ_SUP.Replace(m[0], d[0]));
            polarizacion1 = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_RX.POLARIZACION.Substring(0, 1);
            if (polarizacion1 == "C")
                polarizacion1 = "CX";
            if (polarizacion1 == "O")
                polarizacion1 = "C-C";
            DT_ANTENA1.Rows[1]["POLARIZATION"] = polarizacion1;
            DT_ANTENA1.Rows[1]["REMARK"] = AnexoExcel.EQUIPOS_PTOAPTO.ANTENA_RX.OBSERVACION;
            DT_ANTENA1.Rows[1]["DATE_MODIFIED"] = horaCambio;
            DT_ANTENA1.Rows[1]["MODIFIED_BY"] = "sa";

            DT_MICROWS.Rows[0]["TX_FREQ"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.FRECUEN_APROBADA.Replace(m[0], d[0]));
            DT_MICROWS.Rows[0]["POLAR"] = polarizacion1;
            DT_MICROWS.Rows[0]["EIRP"] = Convert.ToDecimal(AnexoExcel.PRA_PTOAPTO.POTENCIA_NOMINAL.Replace(m[0], d[0]));
            DT_MICROWS.Rows[0]["AGL1"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.TX_ALTURA_ANT.Replace(m[0], d[0]));
            DT_MICROWS.Rows[0]["AGL2"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_ALTURA_ANT.Replace(m[0], d[0]));

            DT_MICROWS.Rows[1]["TX_FREQ"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.FRECUEN_APROBADA.Replace(m[0], d[0]));
            DT_MICROWS.Rows[1]["POLAR"] = polarizacion1;
            DT_MICROWS.Rows[1]["EIRP"] = Convert.ToDecimal(AnexoExcel.PRA_PTOAPTO.POTENCIA_NOMINAL.Replace(m[0], d[0]));
            DT_MICROWS.Rows[1]["AGL1"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.RX_ALTURA_ANT.Replace(m[0], d[0]));
            DT_MICROWS.Rows[1]["AGL2"] = Convert.ToDecimal(AnexoExcel.ENLACE_PTOAPTO.TX_ALTURA_ANT.Replace(m[0], d[0]));


            DT_ADM_EQUIP1.Rows[0]["MANUFACTURER"] = AnexoExcel.EQUIPOS_PTOAPTO.LINEA_TX.MARCA;
            DT_ADM_EQUIP1.Rows[0]["NAME"] = AnexoExcel.EQUIPOS_PTOAPTO.LINEA_TX.MODELO;
            DT_ADM_EQUIP1.Rows[0]["REMARK"] = AnexoExcel.EQUIPOS_PTOAPTO.LINEA_TX.OBSERVACION;
            DT_ADM_EQUIP1.Rows[0]["DATE_MODIFIED"] = horaCambio;
            DT_ADM_EQUIP1.Rows[0]["MODIFIED_BY"] = "sa";

            bRet = cData.InsertDataSet(sSQL1, dtSet);

            if (!bRet)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed");
            }


            return bRet;
        }

        private string GetCodeDeparment(ref csData cData, string departamento)
        {
            string sSQL = @"SELECT ITEM FROM ICSM_PROD.DBO.PROVINCES WHERE CUST_TXT1 = '" + departamento.Substring(0, 3) + "'";
            
            DataTable dt = cData.ObtenerDataTable(sSQL);
            string CODIGO = dt.Rows[0].Field<string>("ITEM");

            if (departamento.Substring(0, 3) == "SAN")
            {
                if (departamento != "SANTANDER")
                    CODIGO = "88";
            }
            return CODIGO;

        }

        private bool CrearSolicitudOtorgaICSManager(ref csData cData, ref ST_RDSSolicitud Solicitud, ref RDSDynamicSolicitud DynamicSolicitud)
        {
            bool bRet = true;
            string sSQL;


            sSQL = @"
                        SELECT Top 0 ID, TABLE_NAME, SRVLIC_ID, SRVLICN_ID, DESCRIPTION, STATUS, CESS_ID, NB_YEARS, MOD_EXE_DATE, COMPLETE, APP_USER, ADM_NAME, REF_OFFI, DATE_OFFI, 
                        FRONT_KEY, NEXT_ANSWER, REMARK, ACT_ID, DATE_CREATED, CREATED_BY, DATE_MODIFIED, MODIFIED_BY, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK, CLASS, ACTIVITY, SERVICE_TYPE
                        FROM [ICSM_PROD].[dbo].P_APPLICATION;
                
                        SELECT Top 0 ID, TABLE_NAME, FILE_ID, TYPE, COMPLETE, STATUS, DESCRIPTION, MEDIA, REF_OFFI, DATE_OFFI, REF_OP, DATE_OP, APP_USER, MESSAGE, REMARK, DATE_CREATED, 
                        CREATED_BY, DATE_MODIFIED, MODIFIED_BY
                        FROM [ICSM_PROD].[dbo].P_LETTER;

                        SELECT Top 0 ID, OBJ_TBNM, OBJ_TBID, EVENT_DATE, [USER], EVENT, AS_TEXT, AS_XML, CF1_ID, FR_STATE, TO_STATE, EVENT_MORE, AS_XMLX
                        FROM [ICSM_PROD].[dbo].REC_HISTORY;

                        SELECT ID, STATUS, VALTRF_PROCESS, VALTRF_APP_ID, VALTRF_START_DATE, VALTRF_STOP_DATE, DATE_MODIFIED, MODIFIED_BY, STOP_DATE
                        FROM [ICSM_PROD].[dbo].SERV_LICENCE 
                        WHERE ID=" + Solicitud.Expediente.SERV_ID + @";
            
                        SELECT ID, DATE_EVAL, EVALUATOR, EVALUATION, EVAL_REMARK
                        FROM [ICSM_PROD].[dbo].USERS 
                        WHERE ID=" + Solicitud.Expediente.Operador.CUS_ID + @";

                        SELECT Top 0 ID, APPL_ID, LIC_ID, REASON, WISHED_DATE, MESSAGE, RPLAC_ID, FOR_UNDO, DATE_CREATED, CREATED_BY, STATUS 
                        FROM ICSM_PROD.dbo.NT_APPLICATION;

                        SELECT TABLE_NAME, [NEXT]
                        FROM [ICSM_PROD].[dbo].NEXT_ID 
                        WHERE TABLE_NAME In('P_APPLICATION', 'P_LETTER', 'REC_HISTORY', 'NT_APPLICATION')
                        ORDER BY 1;
                        ";


            var dtSet = cData.ObtenerDataSet(sSQL);

            DataTable DT_P_APPLICATION = dtSet.Tables[0];
            DataTable DT_P_LETTER = dtSet.Tables[1];
            DataTable DT_REC_HISTORY = dtSet.Tables[2];
            DataTable DT_SERV_LICENCE = dtSet.Tables[3];
            DataTable DT_USERS = dtSet.Tables[4];
            DataTable DT_NT_APPLICATION = dtSet.Tables[5];
            DataTable DT_NEXT_ID = dtSet.Tables[6];

            Dictionary<string, decimal> NextID = new Dictionary<string, decimal>();

            NextID = (from bp in DT_NEXT_ID.AsEnumerable().AsQueryable()
                      select new
                      {
                          Key = bp.Field<string>("TABLE_NAME"),
                          Next = bp.Field<decimal>("NEXT")
                      }).ToList().ToDictionary(x => x.Key, x => x.Next);

            DT_NEXT_ID.Rows[0]["NEXT"] = NextID["NT_APPLICATION"] + 1;
            DT_NEXT_ID.Rows[1]["NEXT"] = NextID["P_APPLICATION"] + 1;
            DT_NEXT_ID.Rows[2]["NEXT"] = NextID["P_LETTER"] + 1;
            DT_NEXT_ID.Rows[3]["NEXT"] = NextID["REC_HISTORY"] + 4;

            DataRow DR_P_APPLICATION = DT_P_APPLICATION.NewRow();
            DR_P_APPLICATION["ID"] = NextID["P_APPLICATION"];
            DR_P_APPLICATION["TABLE_NAME"] = "P_APPLICATION";
            DR_P_APPLICATION["SRVLIC_ID"] = Solicitud.Expediente.SERV_ID;
            DR_P_APPLICATION["SRVLICN_ID"] = Solicitud.Expediente.SERV_ID;
            DR_P_APPLICATION["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_APPLICATION["STATUS"] = "sAA";
            DR_P_APPLICATION["CESS_ID"] = (object)DBNull.Value;
            DR_P_APPLICATION["NB_YEARS"] = GetIntValue(DynamicSolicitud.GetKeyTextValue("SOLICITUD_PRORROGA_PERIODO"), 10);
            DR_P_APPLICATION["MOD_EXE_DATE"] = (object)DBNull.Value;
            DR_P_APPLICATION["COMPLETE"] = 0;
            DR_P_APPLICATION["APP_USER"] = (object)DBNull.Value;
            DR_P_APPLICATION["ADM_NAME"] = "OT";
            DR_P_APPLICATION["REF_OFFI"] = Solicitud.Radicado;
            DR_P_APPLICATION["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture);
            DR_P_APPLICATION["FRONT_KEY"] = (object)DBNull.Value;
            DR_P_APPLICATION["NEXT_ANSWER"] = (object)DBNull.Value;
            DR_P_APPLICATION["REMARK"] = (object)DBNull.Value;
            DR_P_APPLICATION["ACT_ID"] = NextID["P_LETTER"];
            DR_P_APPLICATION["DATE_CREATED"] = DateTime.Now;
            DR_P_APPLICATION["CREATED_BY"] = "sa";
            DR_P_APPLICATION["DATE_MODIFIED"] = DateTime.Now;
            DR_P_APPLICATION["MODIFIED_BY"] = "sa";
            DR_P_APPLICATION["DATE_EVAL"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVALUATOR"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVALUATION"] = (object)DBNull.Value;
            DR_P_APPLICATION["EVAL_REMARK"] = (object)DBNull.Value;
            DR_P_APPLICATION["CLASS"] = (object)DBNull.Value;
            DR_P_APPLICATION["ACTIVITY"] = "A2";
            DR_P_APPLICATION["SERVICE_TYPE"] = "C7";
            DT_P_APPLICATION.Rows.Add(DR_P_APPLICATION);

            DataRow DR_REC_HISTORY1 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY1["ID"] = NextID["REC_HISTORY"];
            DR_REC_HISTORY1["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY1["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY1["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY1["USER"] = "sa";
            DR_REC_HISTORY1["EVENT"] = "Solicitud creada";
            DR_REC_HISTORY1["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY1["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY1["FR_STATE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["TO_STATE"] = "sAA";
            DR_REC_HISTORY1["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY1["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY1);

            DataRow DR_P_LETTER = DT_P_LETTER.NewRow();
            DR_P_LETTER["ID"] = NextID["P_LETTER"];
            DR_P_LETTER["TABLE_NAME"] = "P_LETTER";
            DR_P_LETTER["FILE_ID"] = NextID["P_APPLICATION"];
            DR_P_LETTER["TYPE"] = "C_SOL";
            DR_P_LETTER["COMPLETE"] = (object)DBNull.Value;
            DR_P_LETTER["STATUS"] = (object)DBNull.Value;
            DR_P_LETTER["DESCRIPTION"] = (object)DBNull.Value;
            DR_P_LETTER["MEDIA"] = (object)DBNull.Value;
            DR_P_LETTER["REF_OFFI"] = Solicitud.Radicado;
            DR_P_LETTER["DATE_OFFI"] = DateTime.ParseExact(Solicitud.SOL_CREATED_DATE, "yyyyMMdd", CultureInfo.InvariantCulture);
            DR_P_LETTER["REF_OP"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_OP"] = (object)DBNull.Value;
            DR_P_LETTER["APP_USER"] = (object)DBNull.Value;
            DR_P_LETTER["MESSAGE"] = (object)DBNull.Value;
            DR_P_LETTER["REMARK"] = (object)DBNull.Value;
            DR_P_LETTER["DATE_CREATED"] = DateTime.Now;
            DR_P_LETTER["CREATED_BY"] = "sa";
            DR_P_LETTER["DATE_MODIFIED"] = DateTime.Now;
            DR_P_LETTER["MODIFIED_BY"] = "sa";
            DT_P_LETTER.Rows.Add(DR_P_LETTER);

            DataRow DR_SERV_LICENCE = DT_SERV_LICENCE.Rows[0];
            DR_SERV_LICENCE["STATUS"] = "eSOL";
            DR_SERV_LICENCE["VALTRF_PROCESS"] = "OTO";
            DR_SERV_LICENCE["VALTRF_APP_ID"] = NextID["P_APPLICATION"];
            DR_SERV_LICENCE["VALTRF_START_DATE"] = ((DateTime)DR_SERV_LICENCE["STOP_DATE"]).AddDays(1);
            DR_SERV_LICENCE["VALTRF_STOP_DATE"] = ((DateTime)DR_SERV_LICENCE["VALTRF_START_DATE"]).AddYears(GetIntValue(DynamicSolicitud.GetKeyTextValue("SOLICITUD_PRORROGA_PERIODO"), 10));
            DR_SERV_LICENCE["DATE_MODIFIED"] = DateTime.Now;
            DR_SERV_LICENCE["MODIFIED_BY"] = "sa";

            DataRow DR_REC_HISTORY2 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY2["ID"] = NextID["REC_HISTORY"] + 1;
            DR_REC_HISTORY2["OBJ_TBNM"] = "SERV_LICENCE";
            DR_REC_HISTORY2["OBJ_TBID"] = Solicitud.Expediente.SERV_ID;
            DR_REC_HISTORY2["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY2["USER"] = "sa";
            DR_REC_HISTORY2["EVENT"] = "Solicitud recibida";
            DR_REC_HISTORY2["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY2["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY2["FR_STATE"] = "eAUT";
            DR_REC_HISTORY2["TO_STATE"] = "eSOL";
            DR_REC_HISTORY2["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY2["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY2);

            DR_P_APPLICATION = DT_P_APPLICATION.Rows[0];
            DR_P_APPLICATION["STATUS"] = "sTT";
            DR_P_APPLICATION["DATE_MODIFIED"] = DateTime.Now;
            DR_P_APPLICATION["DATE_EVAL"] = DateTime.Now.Date;
            DR_P_APPLICATION["EVALUATOR"] = "sa";
            DR_P_APPLICATION["EVALUATION"] = "P";
            DR_P_APPLICATION["EVAL_REMARK"] = "Análisis Administrativo aprobado";

            DataRow DR_REC_HISTORY3 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY3["ID"] = NextID["REC_HISTORY"] + 2;
            DR_REC_HISTORY3["OBJ_TBNM"] = "P_APPLICATION";
            DR_REC_HISTORY3["OBJ_TBID"] = NextID["P_APPLICATION"];
            DR_REC_HISTORY3["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY3["USER"] = "sa";
            DR_REC_HISTORY3["EVENT"] = "Anal. Administrativo positivo";
            DR_REC_HISTORY3["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY3["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY3["FR_STATE"] = "sAA";
            DR_REC_HISTORY3["TO_STATE"] = "sTT";
            DR_REC_HISTORY3["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY3["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY3);

            DataRow DR_USERS = DT_USERS.Rows[0];
            DR_USERS["DATE_EVAL"] = DateTime.Now.Date;
            DR_USERS["EVALUATOR"] = "sa";
            DR_USERS["EVALUATION"] = "P";
            DR_USERS["EVAL_REMARK"] = "Análisis financiero aprobado";

            DataRow DR_REC_HISTORY4 = DT_REC_HISTORY.NewRow();
            DR_REC_HISTORY4["ID"] = NextID["REC_HISTORY"] + 3;
            DR_REC_HISTORY4["OBJ_TBNM"] = "USERS";
            DR_REC_HISTORY4["OBJ_TBID"] = Solicitud.Expediente.Operador.CUS_ID;
            DR_REC_HISTORY4["EVENT_DATE"] = DateTime.Now;
            DR_REC_HISTORY4["USER"] = "sa";
            DR_REC_HISTORY4["EVENT"] = "Anal. Financiero positivo";
            DR_REC_HISTORY4["AS_TEXT"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XML"] = (object)DBNull.Value;
            DR_REC_HISTORY4["CF1_ID"] = (object)DBNull.Value;
            DR_REC_HISTORY4["FR_STATE"] = "uACT";
            DR_REC_HISTORY4["TO_STATE"] = "uACT";
            DR_REC_HISTORY4["EVENT_MORE"] = (object)DBNull.Value;
            DR_REC_HISTORY4["AS_XMLX"] = (object)DBNull.Value;
            DT_REC_HISTORY.Rows.Add(DR_REC_HISTORY4);

            DataRow DR_NT_APPLICATION = DT_NT_APPLICATION.NewRow();
            DR_NT_APPLICATION["ID"] = NextID["NT_APPLICATION"];
            DR_NT_APPLICATION["APPL_ID"] = NextID["P_APPLICATION"];
            DR_NT_APPLICATION["LIC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["REASON"] = "OTO";
            DR_NT_APPLICATION["WISHED_DATE"] = DateTime.Now.AddDays(37).Date;
            DR_NT_APPLICATION["MESSAGE"] = (object)DBNull.Value;
            DR_NT_APPLICATION["RPLAC_ID"] = (object)DBNull.Value;
            DR_NT_APPLICATION["FOR_UNDO"] = (object)DBNull.Value;
            DR_NT_APPLICATION["DATE_CREATED"] = DateTime.Now;
            DR_NT_APPLICATION["CREATED_BY"] = "sa";
            DR_NT_APPLICATION["STATUS"] = "S";
            DT_NT_APPLICATION.Rows.Add(DR_NT_APPLICATION);


            bRet = cData.InsertDataSet(sSQL, dtSet);

            if (!bRet)
            {
                WriteLogError(System.Reflection.MethodBase.GetCurrentMethod().Name, "InsertDataSet Failed");
            }


            return bRet;
        }

        #endregion
    }
}
