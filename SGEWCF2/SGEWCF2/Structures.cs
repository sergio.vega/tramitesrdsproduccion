﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace SGEWCF2
{

    public class ST_RDSParameters
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public ST_RDSParameters() { }
        public ST_RDSParameters(string Name, object Value)
        {
            this.Name = Name;
            this.Value = Value;
        }
    }


    public class ST_RDSMainData
    {
        public string HtmlFooter { get; set; }
        public List<ST_RDSTipoSolicitud> SOL_TYPES { get; set; }
        public ST_RDSMainData() { }
    }

    public class ST_RDSEvent
    {
        public int EVT_ID { get; set; }
        public int USR_ID { get; set; }
        public string USR_NAME { get; set; }
        public string EVT_FUNCTION { get; set; }
        public int EVT_CRUD { get; set; }
        public string EVT_DATA_OLD { get; set; }
        public string EVT_DATA_NEW { get; set; }
        public string EVT_CREATED { get; set; }
        
        public ST_RDSEvent() { }
    }

    public class ST_RDSConectionHeader
    {
        public int USR_ID { get; set; }
        public int USR_GROUP { get; set; }
        public int USR_ROLE { get; set; }
        public string USR_NAME { get; set; }
        public string USR_LOGIN { get; set; }
        public string USR_EMAIL { get; set; }
        public int USR_CONECTIONS { get; set; }
        public string USR_DURATION { get; set; }
        public string USR_LAST_CONECTION { get; set; }
        public string USR_OS { get; set; }
        public string USR_BROWSER { get; set; }
        public string APP_VERSION { get; set; }

        public ST_RDSConectionHeader() { }
    }

    public class ST_RDSConectionDetails
    {
        public int TOK_ID { get; set; }
        public string USR_NAME { get; set; }
        public string TOK_CREATED { get; set; }
        public int TOK_NCALLS { get; set; }
        public string TOK_DURATION { get; set; }
        public string TOK_IP_ADDRESS { get; set; }
        public string TOK_OS { get; set; }
        public string TOK_BROWSER { get; set; }
        public string APP_VERSION { get; set; }        
        public ST_RDSConectionDetails() { }
    }

    public class ST_RDSUserInfo
    {
        public int USR_ID { get; set; }
        public string USR_NAME { get; set; }
        public int BLOCK { get; set; } //Bloqueado
        public Guid TOKEN { get; set; } //Token

        public ST_RDSUserInfo() { }
        public ST_RDSUserInfo(int USR_ID) { this.USR_ID = USR_ID; this.USR_NAME = ""; BLOCK = 0; }
    }

    public class ST_RDSUsuario
    {
        public int USR_ID { get; set; }
        public int USR_GROUP { get; set; }
        public int USR_ROLE { get; set; }
        public int USR_ASSIGNABLE { get; set; }
        public int USR_STATUS { get; set; }
        public string USR_LOGIN { get; set; }
        public string USR_NAME { get; set; }
        public string USR_EMAIL { get; set; }
        public int USR_HAS_SIGN { get; set; }
        public byte[] USR_SIGN { get; set; }
        public ST_RDSUsuario() { }
        public ST_RDSUsuario(int USR_ID)
        {
            this.USR_ID = USR_ID;
        }
    }



    public class ST_RDSUsuarioEx : ST_RDSUsuario
    {
        public Guid TOKEN { get; set; }

        public ST_RDSUsuarioEx() { }
        public ST_RDSUsuarioEx(int USR_ID)
        {
            this.USR_ID = USR_ID;
        }
    }


    public class UAgent
    {
        public string OS { get; set; } //Sistema Operativo
        public string BROWSER { get; set; } //Browser
        public UAgent() { }
    }

    public class ST_RDSResCombos
    {

        public string InformacionAdministrativa { get; set; }
        public string InformacionConcesionario { get; set; }
        public string InformacionTecnica { get; set; }
        public string InformacionRadicados { get; set; }
        public string InformacionResolucion { get; set; }
        public string InformacionAntecedentes { get; set; }
        
        public ST_RDSResCombos() { }
    }



    public class ST_RDSResPlt
    {
        public Guid RES_PLT_UID { get; set; }
        public string RES_PLT_NAME { get; set; }
        public int SOL_TYPE_ID { get; set; }
        public string SOL_TYPE_NAME { get; set; }
        public string RES_PLT_PARSER { get; set; }
        public string RES_PLT_CONTENT { get; set; }
        public string RES_PLT_EPIGRAFE { get; set; }
        public string RES_PLT_CODIGO { get; set; }
        public string RES_PLT_VERSION { get; set; }
        public int RES_PLT_STATUS { get; set; }
        public int RES_PLT_CRUD { get; set; }

        public ST_RDSResPlt() { }

        public ST_RDSResPlt(ST_RDSResolucion Resolucion) {
            this.RES_PLT_CONTENT = Resolucion.RES_CONTENT;
            this.RES_PLT_CODIGO = Resolucion.RES_CODIGO;
            this.RES_PLT_VERSION = Resolucion.RES_VERSION;
        }
    }

    public class ST_RDSResolucion
    {
        public Guid SOL_UID { get; set; }
        public string RES_NAME { get; set; }
        public string RES_CONTENT { get; set; }
        public string RES_CODIGO { get; set; }
        public string RES_VERSION { get; set; }
        public bool RES_IS_CREATED { get; set; }
        public string RES_CREATED_DATE { get; set; }
        public string RES_MODIFIED_DATE { get; set; }
        public int RES_TRACKING_COUNT { get; set; }
        public ST_RDSResolucion() { }
    }

    public class ST_RDSText
    {
        public string TXT_ID { get; set; }
        public string TXT_TYPE { get; set; }
        public string TXT_GROUP { get; set; }
        public string TXT_TEXT { get; set; }
        public bool TXT_SYSTEM { get; set; }
        public int TXT_CRUD { get; set; }
        public ST_RDSText() { }
    }
    public class ST_RDSDoc
    {
        public int ID_ANX_SOL { get; set; }
        public string TIPO_ANX_SOL { get; set; }
        public string DES_TIPO_ANX_SOL { get; set; }
        public string EXTENSIONS { get; set; }
        public int MAX_SIZE { get; set; }
        public int MAX_NAME_LENGTH { get; set; }
        public bool ANX_SYSTEM { get; set; }
        public bool ANX_OTORGA { get; set; }
        public int DOC_CRUD { get; set; }
        public ST_RDSDoc() { }
    }

    public class ST_RDSComen
    {

        public Guid SOL_UID { get; set; }
        public string COMMENT { get; set; }

        public ST_RDSComen() { }
    }

    public class ST_RDSImage
    {
        public string IMG_ID { get; set; }
        public bool IMG_SYSTEM { get; set; }
        public string IMG_CONTENT_TYPE { get; set; }
        public int IMG_CRUD { get; set; }
        public string IMG_FILE { get; set; }
        public ST_RDSImage() { }
    }

    public class ST_RDSImageEvent
    {
        public string IMG_ID { get; set; }
        public bool IMG_SYSTEM { get; set; }
        public string IMG_CONTENT_TYPE { get; set; }
        public int IMG_CRUD { get; set; }
        public int IMG_FILE_SIZE { get; set; }
        public ST_RDSImageEvent() { }

        public ST_RDSImageEvent(ST_RDSImage Image)
        {
            this.IMG_ID = Image.IMG_ID;
            this.IMG_SYSTEM = Image.IMG_SYSTEM;
            this.IMG_CONTENT_TYPE = Image.IMG_CONTENT_TYPE;
            this.IMG_CRUD = Image.IMG_CRUD;
            this.IMG_FILE_SIZE = Image.IMG_FILE.Length;
        }
    }

    public class ST_RDSAlarm
    {
        public int ALARM_STEP { get; set; }
        public string ALARM_DESCRIPTION { get; set; }
        public string ALARM_ICON { get; set; }
        public int ALARM_PRE_EXPIRE { get; set; }
        public int ALARM_EXPIRE { get; set; }
        public ST_RDSAlarm() { }
    }

    public class ST_RDSLog
    {
        public int LOG_ID { get; set; }
        public Guid SOL_UID { get; set; }
        public int ID_OBSERVACION { get; set; }
        public string LOG_ICON { get; set; }
        public string LOG_EVENT { get; set; }
        public int LOG_SOURCE { get; set; }
        public int LOG_USR_ROLE { get; set; }
        public string LOG_USR_NAME { get; set; }
        public string LOG_NUMBER { get; set; }
        public string LOG_DESCRIPTION { get; set; }
        public string LOG_DATE { get; set; }

        public ST_RDSLog() { }
    }


    public class ST_RDSConteoDevoluciones
    {
        public int USR_ID { get; set; }
        public string USR_NAME { get; set; }
        public int CONTEO { get; set; }
        public ST_RDSConteoDevoluciones() { }
    }

    public class ST_RDSOperador
    {
        public int CUS_ID { get; set; } //ID en Manager
        public string CUS_IDENT { get; set; }
        public string CUS_NAME { get; set; }
        public string CUS_EMAIL { get; set; }
        public int CUS_BRANCH { get; set; }
        public string CUS_COUNTRYCODE { get; set; }
        public string CUS_STATECODE { get; set; }
        public string CUS_CITYCODE { get; set; }
        public ST_RDSOperador() { }
    }

    public class ST_RDSUsuarioBDU
    {
        public string NombreRazonSocial { get; set; }
        public string Identificacion { get; set; }
        public string TipoDocumento { get; set; }
        public ST_RDSUsuarioBDU() { }
    }

    public class ST_RDSSolicitudes
    {
        public ST_RDSOperador Operador { get; set; }
        public List<ST_RDSTipoSolicitud> SOL_TYPES { get; set; }
        public List<ST_RDSSolicitud> Solicitudes { get; set; }
        public List<ST_RDSContacto> Contactos { get; set; }
        public ST_RDSSolicitudes() { }
    }

    public class ST_RDSSolicitudesMintic
    {
        public List<ST_RDSTipoSolicitud> SOL_TYPES { get; set; }
        public List<ST_RDSSolicitud> Solicitudes { get; set; }
        public ST_RDSSolicitudesMintic() { }
    }

    public class ST_RDSExpedientes
    {
        public List<ST_RDSTipoSolicitud> SOL_TYPES { get; set; }
        public List<ST_RDSExpediente> Expedientes { get; set; }
        public ST_RDSExpedientes() { }
    }

    public class ST_RDSTipoSolicitud
    {
        public int SOL_TYPE_ID { get; set; }
        public string SOL_TYPE_NAME { get; set; }
        public int SOL_TYPE_CLASS { get; set; }
        public string SOL_TYPE_DESC { get; set; }
        public bool disabled { get; set; }
        public ST_RDSTipoSolicitud() { }
    }

    public class ST_RDSRoles
    {
        public ST_RDSUsuario USR_ROLE1 { get; set; }
        public ST_RDSUsuario USR_ROLE2 { get; set; }
        public ST_RDSUsuario USR_ROLE4 { get; set; }
        public ST_RDSUsuario USR_ROLE8 { get; set; }
        public ST_RDSUsuario USR_ROLE16 { get; set; }
        public ST_RDSUsuario USR_ROLE32 { get; set; }
        public ST_RDSRoles() { }
    }

    public class ST_RDSSolicitud
    {
        public Guid SOL_UID { get; set; }
        public int SOL_CREATOR { get; set; }
        public int SERV_ID { get; set; }
        public int SOL_NUMBER { get; set; }
        public string Radicado { get; set; }
        public int SOL_TYPE_ID { get; set; }
        public int SOL_TYPE_CLASS { get; set; }
        public bool SOL_IS_OTORGA { get; set; }
        public string SOL_TYPE_NAME { get; set; }
        public string SOL_CREATED_DATE { get; set; }
        public string SOL_CREATED_DATE_YEAR { get; set; }
        public string SOL_MODIFIED_DATE { get; set; }
        public string SOL_LAST_STATE_DATE { get; set; }
        public string SOL_REQUIRED_DATE { get; set; }
        public string SOL_REQUIRED_DEADLINE_DATE { get; set; }
        public int SOL_REQUIRED_POSTPONE { get; set; }
        public int SOL_STATE_ID { get; set; }
        public string SOL_STATE_NAME { get; set; }
        public int SOL_ALARM_STATE { get; set; }
        public string Clasificacion { get; set; }
        public string NivelCubrimiento { get; set; }
        public string Tecnologia { get; set; }
        public string Evaluaciones { get; set; }
        public decimal? Puntaje { get; set; }
        public ST_RDSDatosComunidad DatosComunidad { get; set; }
        public ST_RDSCurrent CURRENT { get; set; }
        public ST_RDSRoles USR_ADMIN { get; set; }
        public ST_RDSRoles USR_TECH { get; set; }
        public ST_RDSReasign Reasign { get; set; }
        public ST_RDSContacto Contacto { get; set; }
        public ST_RDSExpediente Expediente { get; set; }
        public ST_RDSAnalisisFinanciero Financiero { get; set; }
        public ST_RDSComunicado ComunicadoAdministrativo { get; set; }
        public ST_RDSComunicado ComunicadoTecnico { get; set; }
        public List<ST_RDSAnexos> Anexos { get; set; }
        public List<ST_RDSTechAnexos> TechAnexos { get; set; }
        public List<ST_RDSDocument> Documents { get; set; }
        public ST_RDSTechAnalisis AnalisisTecnico { get; set; }
        public List<ST_RDSTechDoc> TechDocs { get; set; }
        public ST_RDSResAnalisis AnalisisResolucion { get; set; }
        public ST_RDSResolucion Resolucion { get; set; }
        public ST_RDSResAnalisis AnalisisViabilidad { get; set; }
        public ST_RDSResolucion Viabilidad { get; set; }
        //public STV_RDSInfoRegistroUsuario InfoUsuario { get; set; }

        public int SOL_ENDED { get; set; }
        public string CAMPOS_SOL { get; set; }
        public string COMMENT { get; set; }
        public List<ST_RDSTipoSolicitud> SOL_TYPES { get; set; }
        public string AZ_DIRECTORIO { get; set; }
        public ST_RDSSolicitud() { }
    }

    public class ST_RDSTechDoc
    {
        public Guid TECH_UID { get; set; }
        public int TECH_TYPE { get; set; }
        public string TECH_CONTENT_TYPE { get; set; }
        public string TECH_NAME { get; set; }
        public bool TECH_CHANGED { get; set; }

        public ST_RDSTechDoc() { }
    }


    public class ST_RDSReasign
    {
        public bool USR_ROLE1_REASIGN { get; set; }
        public bool USR_ROLE2_REASIGN { get; set; }
        public bool USR_ROLE4_REASIGN { get; set; }
        public bool USR_ROLE8_REASIGN { get; set; }
        public bool USR_ROLE16_REASIGN { get; set; }
        public string USR_ROLE1_REASIGN_COMMENT { get; set; }
        public string USR_ROLE2_REASIGN_COMMENT { get; set; }
        public string USR_ROLE4_REASIGN_COMMENT { get; set; }
        public string USR_ROLE8_REASIGN_COMMENT { get; set; }
        public string USR_ROLE16_REASIGN_COMMENT { get; set; }
        public ST_RDSReasign() { }
    }

    public class ST_RDSReasignUp
    {
        public int TYPE { get; set; }
        public int USR_GROUP { get; set; }
        public int ROLE { get; set; }
        public int ColumnROLE { get; set; }
        public Guid SOL_UID{ get; set; }
        public int ID_OBSERVACION { get; set; }
        public int USR_ID { get; set; }
        public int NEW_USR_ID { get; set; }
        public string COMMENT { get; set; }
        public int NEXT_LEVEL_USR_ID { get; set; }
        public int NEXT_LEVEL_ROLE { get; set; }
        public ST_RDSReasignUp() { }
    }



    public class ST_RDSExpediente
    {
        public int SERV_ID { get; set; } //ID Manager
        public int SERV_NUMBER { get; set; }
        public string SERV_STATUS { get; set; }
        public string STATION_CLASS { get; set; }
        public string STATE { get; set; }
        public string SERV_NAME { get; set; }
        public string DEPTO { get; set; }
        public string CITY { get; set; }
        public int CODE_AREA_MUNICIPIO { get; set; }
        public double? POWER { get; set; }
        public double? ASL { get; set; }
        public double? FREQUENCY { get; set; }
        public string CALL_SIGN { get; set; }
        public string MODULATION { get; set; }
        public string STOP_DATE { get; set; }
        public string CUST_TXT3 { get; set; }
        public ST_RDSOperador Operador { get; set; }
        public List<infoTecnicaCubrimiento> RedesCubrimiento { get; set; }
        public List<infoTecnicaPaP> RedesPPCubrimiento { get; set; }
        public List<infoTecnicaTransmovil> RedesTranmoviles { get; set; }
        public List<infoTecnicaPaP> RedesPPTranmoviles { get; set; }
        public List<ST_RDSContacto> Contactos { get; set; }
        public ST_RDSExpediente() { }
    }

    public class ST_RDSContacto
    {
        public int CONT_ID { get; set; }
        public string CONT_NUMBER { get; set; }
        public string CONT_NAME { get; set; }
        public string CONT_TEL { get; set; }
        public string CONT_TITLE { get; set; }
        public int CONT_ROLE { get; set; }
        public string CONT_EMAIL { get; set; }
        public string CONT_TYPE { get; set; }
        public ST_RDSContacto() { }
    }

    public class infoTecnicaCubrimiento
    {
        public int LIC_NUMBER { get; set; }
        public decimal? NORTH { get; set; }
        public decimal? EAST { get; set; }
        public decimal? LATITUDE { get; set; }
        public decimal? LONGITUDE { get; set; }
        public string LATITUDEGSM { get; set; }
        public string LONGITUDEGSM { get; set; }

        public string ORIGIN { get; set; }
        public string STATION_CLASS { get; set; }
        public double? ASL { get; set; }
        public string DESIG_EMISSION { get; set; }
        public string MODULATION { get; set; }
        public decimal? PWR_ANT { get; set; }
        public decimal? LOSSES { get; set; }
        public double? POWER { get; set; }
        public decimal? AGL { get; set; }
        public decimal? GAIN { get; set; }
        public decimal? AZIMUTH { get; set; }
    }

    public class infoTecnicaPaP
    {
        public int LIC_NUMBER { get; set; }
        public string CLASS { get; set; }
        public string DESIG_EMISSION { get; set; }

        public string CITY_A { get; set; }
        public string DEPTO_A { get; set; }
        public string ADDRESS_A { get; set; }
        public decimal? LATITUDE_A{ get; set; }
        public decimal? LONGITUDE_A { get; set; }
        public string LATITUDEGSM_A { get; set; }
        public string LONGITUDEGSM_A { get; set; }
        public decimal? FREQUENCY_A { get; set; }
        public decimal? POWER_A { get; set; }
        public decimal? ASL_A { get; set; }
        public decimal? AGL_A { get; set; }
        public decimal? GAIN_A { get; set; }
        public decimal? AZIMUTH_A { get; set; }
        public decimal? TILT_A { get; set; }
        public string POLAR_A { get; set; }

        public string CITY_B { get; set; }
        public string DEPTO_B { get; set; }
        public string ADDRESS_B { get; set; }
        public decimal? LATITUDE_B { get; set; }
        public decimal? LONGITUDE_B { get; set; }
        public string LATITUDEGSM_B { get; set; }
        public string LONGITUDEGSM_B { get; set; }

        public decimal? FREQUENCY_B { get; set; }
        public decimal? POWER_B { get; set; }
        public decimal? ASL_B { get; set; }
        public decimal? AGL_B { get; set; }
        public decimal? GAIN_B { get; set; }
        public decimal? AZIMUTH_B { get; set; }
        public decimal? TILT_B { get; set; }
        public string POLAR_B { get; set; }

    }

    public class infoTecnicaTransmovil
    {
        public int LIC_NUMBER { get; set; }
        public string CATEGORY { get; set; }
        public string CARACT { get; set; }
        public string DESIG_EMISSION { get; set; }
        public decimal? BW { get; set; }

        public decimal? TX_LOW_FREQ { get; set; }
        public decimal? TX_HIGH_FREQ { get; set; }
        public decimal? RX_LOW_FREQ { get; set; }
        public decimal? RX_HIGH_FREQ { get; set; }
        public decimal? POWER { get; set; }
        public decimal? PWR_ANT { get; set; }
        public decimal? AGL { get; set; }
        public decimal? AZIMUTH { get; set; }
        public decimal? LATITUDE { get; set; }
        public decimal? LONGITUDE { get; set; }
        public decimal? GAIN { get; set; }
        public string ADDRESS { get; set; }
    }

    public class ST_RDSAnexos
    {
        public Guid ANX_UID { get; set; }
        public int ANX_TYPE_ID { get; set; }
        public string ANX_TYPE_NAME { get; set; }
        public int ANX_STATE_ID { get; set; }
        public string ANX_COMMENT { get; set; } 
        public string ANX_NAME { get; set; }
        public int ANX_NUMBER { get; set; }
        public string ANX_CONTENT_TYPE { get; set; }
        public int ANX_ROLE1_STATE_ID { get; set; }
        public int ANX_ROLE2_STATE_ID { get; set; }
        public int ANX_ROLE4_STATE_ID { get; set; }
        public int ANX_ROLE8_STATE_ID { get; set; }
        public bool ANX_ROLEX_CHANGED { get; set; }
        public string ANX_ROLE1_COMMENT { get; set; }
        public string ANX_ROLE2_COMMENT { get; set; }
        public string ANX_ROLE4_COMMENT { get; set; }
        public string ANX_ROLE8_COMMENT { get; set; }
        public string ANX_FILE { get; set; }
        public int ANX_OPENED { get; set; }

        public ST_RDSAnexos() { }
    }

    public class ST_RDSTechAnexos
    {
        public Guid ANX_UID { get; set; }
        public int ANX_TYPE_ID { get; set; }
        public string ANX_TYPE_NAME { get; set; }
        public int ANX_STATE_ID { get; set; }
        public string ANX_COMMENT { get; set; }
        public string ANX_NAME { get; set; }
        public int ANX_NUMBER { get; set; }
        public string ANX_CONTENT_TYPE { get; set; }
        public int ANX_ROLE1_STATE_ID { get; set; }
        public int ANX_ROLE2_STATE_ID { get; set; }
        public int ANX_ROLE4_STATE_ID { get; set; }
        public int ANX_ROLE8_STATE_ID { get; set; }
        public bool ANX_ROLEX_CHANGED { get; set; }
        public string ANX_ROLE1_COMMENT { get; set; }
        public string ANX_ROLE2_COMMENT { get; set; }
        public string ANX_ROLE4_COMMENT { get; set; }
        public string ANX_ROLE8_COMMENT { get; set; }
        public int ANX_OPENED { get; set; }

        public ST_RDSTechAnexos() { }
    }

    public class ST_RDSTechAnalisis
    {
        public Guid SOL_UID { get; set; }
        public int TECH_STATE_ID { get; set; } //Dummi
        public int TECH_ROLE1_STATE_ID { get; set; }
        public int TECH_ROLE2_STATE_ID { get; set; }
        public int TECH_ROLE4_STATE_ID { get; set; }
        public int TECH_ROLE8_STATE_ID { get; set; }
        public int TECH_ROLE_MINTIC_STATE_ID { get; set; }
        public bool TECH_ROLEX_CHANGED { get; set; }
        public string TECH_ROLE1_COMMENT { get; set; }
        public string TECH_ROLE2_COMMENT { get; set; }
        public string TECH_ROLE4_COMMENT { get; set; }
        public string TECH_ROLE8_COMMENT { get; set; }
        public string TECH_ROLE_MINTIC_COMMENT { get; set; }
        public bool TECH_PTNRS_UPDATED { get; set; }
        public string TECH_CREATED_DATE { get; set; }
        public string TECH_MODIFIED_DATE { get; set; }
        
        public ST_RDSTechAnalisis() { }
    }


    public class ST_RDSResAnalisis
    {
        public Guid SOL_UID { get; set; }
        public int RES_STATE_ID { get; set; } //Dummi
        public int RES_ROLE1_STATE_ID { get; set; }
        public int RES_ROLE2_STATE_ID { get; set; }
        public int RES_ROLE4_STATE_ID { get; set; }
        public int RES_ROLE8_STATE_ID { get; set; }
        public int RES_ROLE16_STATE_ID { get; set; }
        public int RES_ROLE32_STATE_ID { get; set; }
        public bool RES_ROLEX_CHANGED { get; set; }
        public string RES_ROLE1_COMMENT { get; set; }
        public string RES_ROLE2_COMMENT { get; set; }
        public string RES_ROLE4_COMMENT { get; set; }
        public string RES_ROLE8_COMMENT { get; set; }
        public string RES_ROLE16_COMMENT { get; set; }
        public string RES_ROLE32_COMMENT { get; set; }

        public ST_RDSResAnalisis() { }
    }

    public class ST_RDSAnexosUp
    {
        public Guid ANX_UID { get; set; }
        public int ANX_TYPE_ID { get; set; }
        public string ANX_NAME { get; set; }
        public string ANX_CONTENT_TYPE { get; set; }
        public string ANX_FILE { get; set; }
        public ST_RDSAnexosUp() { }
    }


    public class ST_RDSFiles_Up
    {
        public Guid UID { get; set; }
        public int TYPE_ID { get; set; }
        public string NAME { get; set; }
        public string CONTENT_TYPE { get; set; }
        public string DATA { get; set; }
        public ST_RDSFiles_Up() { }
    }

    public class ST_RDSComunicado
    {
        public Guid COM_UID { get; set; }
        public string COM_NAME { get; set; }
        public string COM_DATE { get; set; }
        public int COM_CLASS { get; set; }
        public int COM_TYPE { get; set; }
        public ST_RDSComunicado() { }
        public ST_RDSComunicado(Guid COM_UID, COM_CLASS COM_CLASS)
        { 
            this.COM_UID = COM_UID;
            this.COM_CLASS = (int)COM_CLASS;
        }
    }


    public class ST_RDSCurrent
    {
        public int GROUP { get; set; }
        public int ROLE { get; set; }
        public int STEP { get; set; }
        public ST_RDSCurrent() { }
        public ST_RDSCurrent(int GROUP, int ROLE, int STEP) { this.GROUP = GROUP; this.ROLE = ROLE; this.STEP = STEP; }
        public void Fill(GROUPS GROUP, ROLES ROLE, STEPS STEP) { this.GROUP = (int)GROUP; this.ROLE = (int)ROLE; this.STEP = (int)STEP; }
    }

    public class ST_RDSDocument
    {
        public Guid DOC_UID { get; set; }
        public int DOC_ORDER { get; set; }
        public int DOC_CLASS { get; set; }
        public int DOC_TYPE { get; set; }
        public int DOC_ORIGIN { get; set; }
        public int DOC_DEST { get; set; }
        public string DOC_NUMBER { get; set; }
        public string DOC_NAME { get; set; }
        public string DOC_DATE { get; set; }
        public int DOC_OPENED { get; set; }
        public ST_RDSDocument() { }
    }

    public class ActionInfo
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public int NewID { get; set; }
        public Guid NewUID { get; set; }
        public string NewName { get; set; }
        public ST_RDSSolicitud Solicitud { get; set; }
        public ST_RDSResolucion Resolucion { get; set; }
        
        public ActionInfo() { }

        public ActionInfo(int Status, string Message) { this.Status = Status; this.Message = Message; }
        public ActionInfo(int Status, string Message, int NewID) { this.Status = Status; this.Message = Message; this.NewID = NewID; }
        public ActionInfo(int Status, string Message, ST_RDSSolicitud Solicitud) { this.Status = Status; this.Message = Message; this.Solicitud = Solicitud; }
        public ActionInfo(int Status, string Message, ST_RDSResolucion Resolucion) { this.Status = Status; this.Message = Message; this.Resolucion = Resolucion; }

    }

    public class ST_RDSResult
    {
        public bool Estado { get; set; }
        public bool Error { get; set; }
        public string ErrorMessage { get; set; }
        public string EstadoMessage { get; set; }
        public ST_RDSResult() { this.Estado = false; this.Error = false; this.ErrorMessage = ""; }
    }

    public class ST_RDSAnalisisFinanciero
    {
        public bool? FIN_SEVEN_ALDIA { get; set; }
        public string FIN_ESTADO_CUENTA_NUMBER { get; set; }
        public string FIN_ESTADO_CUENTA_DATE { get; set; }
        public string FIN_REGISTRO_ALFA_NUMBER { get; set; }
        public string FIN_REGISTRO_ALFA_DATE { get; set; }
        public ST_RDSAnalisisFinanciero() { }
    }


    public class ST_RDSEmail
    {
        public List<string> cuentasDestino { get; set; }
        public string asuntoCorreo { get; set; }
        public StringBuilder mensajeCorreo { get; set; }
        public ST_RDSEmail() {
            this.cuentasDestino = new List<string>();
            this.mensajeCorreo = new StringBuilder();
        }
    }

    public class ST_Helper
    {
        public Dictionary<string, string> TXT { get; set; }
        public Dictionary<string, byte[]> IMG { get; set; }
        public ST_Helper()
        {
            this.TXT = null;
            this.IMG = null;
        }

        public string GetKeyTextValue(string key)
        {
            if (TXT.ContainsKey(key))
                return TXT[key];
            else
                return "";
        }

        public void AddKey(string key, string value)
        {
            if (TXT.ContainsKey(key))
                TXT[key] = value;
            else
                TXT.Add(key, value);
        }
    }


    public class ST_String
    {
        public string Cadena { get; set; }
        public bool Evaluate { get; set; }
        public bool Bold { get; set; }
        public bool Anchor { get; set; }
        public ST_String() { }
        public ST_String(string Cadena, bool Evaluate, bool Bold, bool Anchor) { this.Cadena = Cadena; this.Evaluate = Evaluate; this.Bold = Bold; this.Anchor = Anchor; }
    }

    public class ST_List
    {
        public string Cadena { get; set; }
        public bool isList { get; set; }
        public ST_List() { }
        public ST_List(string Cadena, bool isList) { this.Cadena = Cadena; this.isList = isList; }
    }

    public class ST_Anchor
    {
        public string Cadena { get; set; }
        public bool isAnchor { get; set; }
        public ST_Anchor() { }
        public ST_Anchor(string Cadena, bool isAnchor) { this.Cadena = Cadena; this.isAnchor = isAnchor; }
    }

    public class ST_Paragraph
    {
        public string Cadena { get; set; }
        public bool isParagraph { get; set; }
        public string Align { get; set; }
        public ST_Paragraph() { }
        public ST_Paragraph(string Cadena) { this.Cadena = Cadena; }
        public ST_Paragraph(string Cadena, bool isParagraph, string Align) { this.Cadena = Cadena; this.isParagraph = isParagraph; this.Align = Align; }
    }

    public class ST_MyEncrypt
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public int Group { get; set; } //0:Concesionario, 1:MinTIC, 2:ANE
        public string Date { get; set; }
        public bool IsValid { get; set; }
        public ST_MyEncrypt()
        {
            this.Login = "";
            this.Email = "";
            this.Group = 0;
            this.IsValid = false;
            this.Date = DateTime.Now.AddYears(-10).ToString("yyyyMMddHHmmss");
        }
        public ST_MyEncrypt(string Login, string Email, int Group)
        {
            this.Login = Login;
            this.Email = Email;
            this.Group = Group;
            this.IsValid = false;
            this.Date = DateTime.Now.AddYears(-10).ToString("yyyyMMddHHmmss");
        }
        public DateTime getExpireDate()
        {
            return DateTime.ParseExact(this.Date, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
        }
    }

    public class ST_Lugar
    {
        public decimal ID { get; set; }
        public int CODE_AREA { get; set; }
        public string NAME { get; set; }
        public ST_Lugar() { }
    }

    public class ST_PlanBroadcast
    {
        public int ID_PLAN_BRO { get; set; }
        public string CALL_SIGN { get; set; }
        public double FREQ { get; set; }
        public string Descripcion { get; set; }
        public string STATION_CLASS { get; set; }
        public double POWER { get; set; }
        public double ASL { get; set; }
        public string STATE { get; set; }
        public string DEPARTAMENTO { get; set; }
        public string MUNICIPIO { get; set; }
        public int CODE_AREA_MUNICIPIO { get; set; }
        public ST_PlanBroadcast() { }
    }

    public class ST_RDSCanalProcesoOtorga
    {
        public int ID_PLAN_BRO { get; set; }
        public string CALL_SIGN { get; set; }
        public int IdEstadoViabilidad { get; set; }
        public string EstadoViabilidad { get; set; }
        public double FREQ { get; set; }
        public string MUNICIPIO { get; set; }
        public string DEPARTAMENTO { get; set; }
        public string STATE { get; set; }
        public string CUS_NAME { get; set; }
        public string CUS_IDENT { get; set; }
        public string RAD_ALFANET { get; set; }
        public int? SERV_NUMBER { get; set; }
        public string SERV_STATUS { get; set; }
        public ST_RDSCanalProcesoOtorga() { }
    }

    public class ST_RDSViabilidad
    {
        public int SOL_TYPE_ID { get; set; }
        public int IdEstadoViabilidad { get; set; }
        public int CantidadDeSolicitudes { get; set; }
        public ST_RDSViabilidad() { }

    }

    public class ST_TipoArchivoProceso
    {
        public int ID { get; set; }
        public string DESC { get; set; }
        public string DES_TIPO_ANX_SOL { get; set; }
        public string EXTENSIONS { get; set; }
        public int MAX_SIZE { get; set; }
        public int MAX_NAME_LENGTH { get; set; }
        public ST_TipoArchivoProceso() { }
    }

    public class ST_RDSCreacionOtorga
    {
        public List<ST_Lugar> DepartamentosPlanBro { get; set; }
        public List<ST_TipoArchivoProceso> TiposArchivos { get; set; }
        public List<ST_TipoArchivoProceso> TiposAnexos { get; set; }
        public List<ST_TipoArchivoProceso> TiposAnexosPond { get; set; }
        public List<ST_TipoArchivoProceso> TiposArchivosPond { get; set; }
        public ST_RDSCreacionOtorga() { }
    }

    public class ST_ResultExisteManifestacionUsuario
    {
        public bool? ExisteUsuario { get; set; }
        public STV_RDSInfoRegistroUsuario InfoRegistroManfestacion { get; set; }
    }

    public class STV_RDSInfoRegistroUsuario
    {
        public string Identificacion { get; set; }
        public string NombreRazonSocial { get; set; }
        public int IdLugar { get; set; }
        public int CodeAreaDepartamento { get; set; }
        public string DireccionCorrespondencia { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string TipoDocumento { get; set; }
        public string NombreComercial { get; set; }
        public string NombreRepresentante { get; set; }
        public int IdLugarEmpresa { get; set; }
        public string DireccionEmpresa { get; set; }
        public string IdentificacionRepresentante { get; set; }
        public string NombreApoderado { get; set; }
        public string IdentificacionApoderado { get; set; }
        public string Contrasena { get; set; }
        public bool EsPersonaNatural { get; set; }
        public string TipoDocumentoRepresentante { get; set; }
        public string TipoDocumentoApoderado { get; set; }
        public string Ciudad { get; set; }
        public string Departamento { get; set; }
        public string CodeLugar { get; set; }
        public string CiudadEmpresa { get; set; }
        public string DepartamentoEmpresa { get; set; }
        public string CodeLugarEmpresa { get; set; }
        public STV_RDSInfoRegistroUsuario() { }
    }

    public class ST_RDSConfiguracionProcesoOtorga
    {
        public Guid ID { get; set; }
        public int SOL_TYPE { get; set; }
        public int CLASS { get; set; }
        public string NumeroResolucion { get; set; }
        public string FechaResolucion { get; set; }
        public string Nombre { get; set; }
        public string ConfiguracionCalificaciones { get; set; }
        public string FechaInicioRecepcionSolicitudes { get; set; }
        public string FechaFinRecepcionSolicitudes { get; set; }
        public decimal PuntajeMinimoViabilidadAutomatica { get; set; }
        public string Descripcion_SOL_TYPE { get; set; }
        public string FechaActual { get; set; }
        public bool ProcesoBloqueado { get; set; }
        public List<ST_TipoDocumentoProcesoOtorga> TiposDocumentos { get; set; }
        public List<ST_TipoDocumentoProcesoOtorgaPond> TiposDocumentosPond { get; set; }
        public ST_RDSConfiguracionProcesoOtorga() { }
    }

    public class ST_TipoDocumentoProcesoOtorga
    {
        public int ID_ANX_SOL { get; set; }
        public string TIPO_ANX_SOL { get; set; }
        public string DES_TIPO_ANX_SOL { get; set; }
        public string EXTENSIONS { get; set; }
        public int MAX_SIZE { get; set; }
        public int MAX_NAME_LENGTH { get; set; }
        public string Descripcion { get; set; }
        public bool HABILITANTES { get; set; }

        public ST_TipoDocumentoProcesoOtorga() { }
    }
    public class ST_TipoDocumentoProcesoOtorgaPond
    {
        public int ID_ANX_SOL { get; set; }
        public string TIPO_ANX_SOL { get; set; }
        public string DES_TIPO_ANX_SOL { get; set; }
        public string EXTENSIONS { get; set; }
        public int MAX_SIZE { get; set; }
        public int MAX_NAME_LENGTH { get; set; }
        public string Descripcion { get; set; }
        public bool HABILITANTES { get; set; }

        public ST_TipoDocumentoProcesoOtorgaPond() { }
    }

    public class ST_InfoConfiguracionesProcesos
    {
        public List<ST_RDSConfiguracionProcesoOtorga> Configuraciones { get; set; }

        public List<ST_TipoDocumentoProcesoOtorga> TiposDocumentos { get; set; }
        public List<ST_TipoDocumentoProcesoOtorgaPond> TiposDocumentosPond { get; set; }
        public ST_InfoConfiguracionesProcesos() { }
    }

    public class ST_RDSDatosComunidad
    {
        public string NombreComunidad { get; set; }
        public decimal? IdLugarComunidad { get; set; }
        public string DireccionComunidad { get; set; }
        public string CiudadComunidad { get; set; }
        public string DepartamentoComunidad { get; set; }
        public string CodeLugarComunidad { get; set; }
        public string Territorio { get; set; }
        public string ResolucionMinInterior { get; set; }
        public string GrupoEtnico { get; set; }
        public string NombrePuebloComunidad { get; set; }
        public string NombreResguardo { get; set; }
        public string NombreCabildo { get; set; }
        public string NombreKumpania { get; set; }
        public string NombreConsejo { get; set; }
        public string NombreOrganizacionBase { get; set; }
        public ST_RDSDatosComunidad() { }
    }

    public class ST_InfoSolicitudesCanal
    {
        public List<ST_RDSSolicitud> Solicitudes { get; set; }
        public decimal PuntajeMinimoViabilidadAutomatica { get; set; }
    }

    public class ST_RDSObservacionConvocatoriaOtorga
    {
        public int ID { get; set; }
        public string NOMBRE_CONVOCATORIA { get; set; }
        public string CODIGO_CONVOCATORIA { get; set; }
        public string ANIO_CONVOCATORIA { get; set; }
        public string NumeroResolucion { get; set; }
        public int SOL_TYPE { get; set; }        
        public string ProcesoActivo { get; set; }
        public int CRUD { get; set; }        
        public List<ST_ObservacionesEtapasOtorga> Etapas { get; set; }
        public List<ST_ObservacionesCategorias> Categorias { get; set; }
        public ST_RDSObservacionConvocatoriaOtorga() { }
    }

    public class ST_ObservacionesEtapasOtorga
    {
        public int ID_ETAPA { get; set; }
        public int ID_CONVOCATORIA { get; set; }
        public string NOMBRE_ETAPA { get; set; }
        public string FECHA_INICIO { get; set; }  
        public string FECHA_FIN { get; set; }
        public string DESCRIPCION { get; set; }

        public ST_ObservacionesEtapasOtorga() { }
    }

    public class ST_ObservacionesCategorias
    {
        public int ID_ETAPA { get; set; }
        public int ID_CATEGORIA { get; set; }
        public string NOMBRE_CATEGORIA { get; set; }
        public int CRUD { get; set; }        
        public ST_ObservacionesCategorias() { }
    }

    public class ST_RDSConfigObservacionOtorga
    {
        public List<ST_RDSObservacionConvocatoriaOtorga> Configuraciones { get; set; }
        public List<ST_ObservacionesCategorias> Categorias { get; set; }
        public ST_RDSConfigObservacionOtorga() { }
    }

    public class ST_RDSObservacion
    {
        public int ID_OBSERVACION { get; set; }
        public int ID_CONVOCATORIA { get; set; }
        public string ETAPA { get; set; }
        public string CONSECUTIVO { get; set; }
        public string TIPO_EMISORA { get; set; }
        public string TIPO_DOCUMENTO { get; set; }
        public string DOCUMENTO { get; set; }
        public string NOMBRE { get; set; }
        public string EMAIL { get; set; }
        public string NOMBRE_ORGANIZACION { get; set; }
        public string CARGO { get; set; }
        public int ID_CATEGORIA { get; set; }
        public string NOMBRE_CATEGORIA { get; set; }
        public string OBSERVACION { get; set; }
        public string ESTADO { get; set; }
        public int ID_FUNCIONARIO { get; set; }
        public int ID_COORDINADOR { get; set; }
        public int ROL_ACTUAL { get; set; }
        public string RESPUESTA { get; set; }
        public string FECHA_OBSERVACION { get; set; }
        public string NAME1 { get; set; }
        public string EMAIL1 { get; set; }
        public string NAME4 { get; set; }
        public string EMAIL4 { get; set; }
        public string FECHA_RESPUESTA { get; set; }
        public bool USR_ROLE1_REASIGN { get; set; }
        public string USR_ROLE1_REASIGN_COMMENT { get; set; }
        public List<ST_ImagenObservacion> Adjuntos { get; set; }
        public List<ST_CategoriasObservacion> Categorias { get; set; }
        public ST_RDSObservacion() { }
    }

    public class ST_ImagenObservacion
    {
        public Guid ID_IMAGEN { get; set; }
        public int ID_OBSERVACION { get; set; }
        public string NOMBRE_IMAGEN { get; set; }
        public ST_ImagenObservacion() { }
    }

    public class ST_RDSObservaciones
    {
        public List<ST_RDSObservacion> Observaciones { get; set; }
        public ST_RDSObservaciones() { }
    }

    public class ST_CategoriasObservacion
    {
        public int ID_CATEGORIA { get; set; }
        public string NOMBRE_CATEGORIA { get; set; }
        public ST_CategoriasObservacion() { }
    }

    public class ST_RDSPlantillasObservaciones
    {
        public string ID_PLANTILLA { get; set; }
        public string NOMBRE { get; set; }
        public string TIPO { get; set; }
        public byte ARCHIVO { get; set; }
        public ST_RDSPlantillasObservaciones() { }
    }

    public class ST_RDSObsPlt
    {
        public string ID_PLANTILLA { get; set; }
        public string NOMBRE { get; set; }
        public string PLT_HTML{ get; set; }
        public int PLT_CRUD { get; set; }
        public ST_RDSObsPlt() { }
    }

    public class ST_RDSObsCombos
    {

        public string InformacionObservaciones { get; set; }
        public string InformacionObsEtapa{ get; set; }
        public string InformacionObsConvocatoria { get; set; }

        public ST_RDSObsCombos() { }
    }

    public class JavaScriptSerializer_TESExtension : JavaScriptSerializer { 
        public JavaScriptSerializer_TESExtension()
        {
            MaxJsonLength = int.MaxValue;
        }
    }


}