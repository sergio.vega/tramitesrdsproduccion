﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGEWCF2
{
    public class ST_RDSAnexoTecnico
    {
        public int SOL_NUMBER { get; set; }
        public string SOL_OTORGA { get; set; }
        public string PRESENTA_PATRONES_RADIACION { get; set; }
        public string TRANSMOVILES { get; set; }
        public ST_RDSConsecionario CONSECIONARIO { get; set; }
        public ST_RDSModif_Params MODIF_PARAMS { get; set; }
        public ST_RDSAvalEstudio AVAL_ESTUDIO { get; set; }
        public ST_RDSCertifAeronautica CERTIF_AERONAUT { get; set; }
        public ST_RDSCertifPlanMunic CERTIF_MUNICIPAL { get; set; }
        public ST_RDSPRA RED_CUBRIMIENTO { get; set; }
        public ST_RDSSistemaRadiante SISTEMA_RADIANTE { get; set; }
        public ST_RDSPatronRadiacion PATRON_RADIACION { get; set; }
        public int TOTAL_MUNICIPIOS { get; set; }
        public List<ST_RDSAreaServicio> AREA_SERVICIO { get; set; }
        public ST_RDSEnlacePtoaPto ENLACE_PTOAPTO { get; set; }
        public ST_RDSRedPtoAPto RED_PTOAPTO { get; set; }
        public ST_RDSPRA PRA_PTOAPTO { get; set; }
        public ST_RDSEquiposRedCobertura EQUIPOS_RED_COBERTURA { get; set; }
        public ST_RDSEquiposRedPtoAPto EQUIPOS_PTOAPTO { get; set; }
        public ST_RDSSoloSistemaRadiante SOLO_SISTEMA_RADIANTE { get; set; }
        public ST_RDSAnexoTecnico() { }
    }

    public class ST_RDSConsecionario
    {
        public int COD_EXPEDIENTE { get; set; }
        public string STATION_NAME { get; set; }
        public string CONCES_NAME { get; set; }
        public string CONCES_NIT { get; set; }
        public string DEPTO_CONCESION { get; set; }
        public string MUNICIPIO_CONCESION { get; set; }
        public string COD_DANE { get; set; }
        public string STATION_TYPE { get; set; }
        public string STATION_CLASS { get; set; }
        public string IDENTIFICATIVO { get; set; }
        public string CEL_REP_LEGAL { get; set; }
        public string EMAIL { get; set; }
        public string TIPO_TRAMITE { get; set; }
        public string ADMIN_ACT { get; set; }
        public ST_RDSConsecionario() { }
    }

    public class ST_RDSModif_Params
    {
        public string ESENCIAL_PARAM { get; set; }
        public string UBIC_SYSTEM_RAD { get; set; }
        public string PRA { get; set; }
        public string SERVICE_AREA { get; set; }
        public string RADIATION_SYSTEM_TX { get; set; }
        public string FRECUENCY { get; set; }
        public ST_RDSModif_Params() { }
    }
    public class ST_RDSAvalEstudio
    {
        public string PROFESIONAL_AVAL { get; set; }
        public string PROFESIONAL_NAME { get; set; }
        public string PROFESIONAL_APELLIDO { get; set; }
        public string PROFESIONAL_CEDULA { get; set; }
        public string PROFESIONAL_TP { get; set; }
        public ST_RDSAvalEstudio() { }
    }

    public class ST_RDSCertifAeronautica
    {
        public string NUM_REF_CERTIF { get; set; }
        public DateTime FECHA_CERTIF { get; set; }
        public string DIR_UBICACION { get; set; }
        public string DEPTO_UBICACION { get; set; }
        public string MUNIC_UBICACION { get; set; }
        public string ALTURA_APROB { get; set; }
        public string MODULACION { get; set; }
        public string POTENCIA_APROB { get; set; }
        public string FREC_APROBADA { get; set; }
        public string RANGO_FREC { get; set; }
        public string LAT_GRAD { get; set; }
        public string LAT_MIN { get; set; }
        public string LAT_SEG { get; set; }
        public string LAT_S_N { get; set; }
        public string LONG_GRAD { get; set; }
        public string LONG_MIN { get; set; }
        public string LONG_SEG { get; set; }
        public string LONG_W { get; set; }
        public string ELEVACION { get; set; }
        public ST_RDSCertifAeronautica() { }
    }

    public class ST_RDSCertifPlanMunic
    {
        public string ENTIDAD_EMISORA { get; set; }
        public DateTime FECHA_CERTIF { get; set; }
        public string NUM_CERTIFICADO { get; set; }
        public string LAT_GRAD { get; set; }
        public string LAT_MIN { get; set; }
        public string LAT_SEG { get; set; }
        public string LAT_S_N { get; set; }
        public string LONG_GRAD { get; set; }
        public string LONG_MIN { get; set; }
        public string LONG_SEG { get; set; }
        public string LONG_W { get; set; }
        public string DENTRO_MUNICIPIO { get; set; }
        public string TIPO_ZONA { get; set; }
        public ST_RDSCertifPlanMunic() { }
    }

    public class ST_RDSPRA
    {
        public string LONG_TX { get; set; }
        public string PRA_AUTORIZ_SOLIC { get; set; }
        public string PRA { get; set; }
        public string G_VECES { get; set; }
        public string G_DBI { get; set; }
        public string G_DBD { get; set; }
        public string PERDIDA_CONECTOR { get; set; }
        public string PERDIDA_LINEA { get; set; }
        public string POTENCIA_INCIDENTE { get; set; }
        public string POTENCIA_NOMINAL { get; set; }
        public ST_RDSPRA() { }
    }

    public class ST_RDSSistemaRadiante
    {
        public string LAT_GRAD { get; set; }
        public string LAT_MIN { get; set; }
        public string LAT_SEG { get; set; }
        public string LAT_S_N { get; set; }
        public string LONG_GRAD { get; set; }
        public string LONG_MIN { get; set; }
        public string LONG_SEG { get; set; }
        public string LONG_W { get; set; }
        public string LATITUD { get; set; }
        public string LONGITUD { get; set; }
        public string DEPARTAMENTO { get; set; }
        public string MUNICIPIO { get; set; }
        public string AREA_MUNICIP { get; set; }
        public ST_RDSSistemaRadiante() { }
    }

    public class ST_RDSPatronRadiacion
    {
        public string ALTURA_RADIACION { get; set; }
        public int NRO_BAHIAS_ANT { get; set; }
        public string GANANCIA_ANT { get; set; }
        public string POLARIZACION { get; set; }
        public string ANGULO_TILT { get; set; }
        public string AZIMUT { get; set; }
        public string ALTURA_TORRE { get; set; }
        public string TIPO_TORRE { get; set; }
        public string DIMENSION_1 { get; set; }
        public string DIMENSION_2 { get; set; }
        public int NUMERO_BAHIAS { get; set; }
        public List<ST_RDSArregloAntenas> ARREGLO_ANTENAS { get; set; }
        public ST_RDSPatronRadiacion() { }
    }

    public class ST_RDSArregloAntenas
    {
        public int BAHIA_NUMERO { get; set; }
        public string ALTURA { get; set; }
        public string DISTANCIA_H { get; set; }
        public string AZIMUT { get; set; }
        public string GANANCIA { get; set; }
        public string POLARIZACION { get; set; }
        public string LONG_CABLE { get; set; }
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public ST_RDSArregloAntenas() { }
    }

    public class ST_RDSAreaServicio
    {
        public string DEPARTAMENTO { get; set; }
        public string MUNICIPIO { get; set; }
        public string CODIGO_DANE { get; set; }
        public ST_RDSAreaServicio() { }
    }

    public class ST_RDSEnlacePtoaPto
    {
        public string TX_LAT_GRA { get; set; }
        public string TX_LAT_MIN { get; set; }
        public string TX_LAT_SEG { get; set; }
        public string TX_LAT_N_S { get; set; }
        public string TX_LONG_GRAD { get; set; }
        public string TX_LONG_MIN { get; set; }
        public string TX_LONG_SEG { get; set; }
        public string TX_LONG_W { get; set; }
        public string TX_LATITUD { get; set; }
        public string TX_LONGITUD { get; set; }
        public string TX_ALTURA_TORRE { get; set; }
        public string TX_ALTURA_ANT { get; set; }
        public string RX_LAT_GRA { get; set; }
        public string RX_LAT_MIN { get; set; }
        public string RX_LAT_SEG { get; set; }
        public string RX_LAT_N_S { get; set; }
        public string RX_LONG_GRAD { get; set; }
        public string RX_LONG_MIN { get; set; }
        public string RX_LONG_SEG { get; set; }
        public string RX_LONG_W { get; set; }
        public string RX_LATITUD { get; set; }
        public string RX_LONGITUD { get; set; }
        public string RX_ALTURA_TORRE { get; set; }
        public string RX_ALTURA_ANT { get; set; }
        public string FRECUEN_APROBADA { get; set; }
        public ST_RDSEnlacePtoaPto() { }
    }

    public class ST_RDSRedPtoAPto
    {
        public string LONG_TX { get; set; }
        public string PRA_AUTORIZ_SOLIC { get; set; }
        public string PRA { get; set; }
        public string G_VECES { get; set; }
        public string G_DBI { get; set; }
        public string G_DBD { get; set; }
        public string PERDIDA_CONECTOR { get; set; }
        public string PERDIDA_LINEA { get; set; }
        public string POTENCIA_INCIDENTE { get; set; }
        public string POTENCIA_NOMINAL { get; set; }
        public ST_RDSRedPtoAPto() { }
    }

    public class ST_RDSEquipo
    {
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string FREQ_INF { get; set; }
        public string FREQ_SUP { get; set; }
        public string POT_INF { get; set; }
        public string POT_SUP { get; set; }
        public string ANCHO_BANDA { get; set; }
        public string DESVIACION_FREQ { get; set; }
        public string OBSERVACION { get; set; }
        public string URL_CATALOGO_EQUIPO { get; set; }
        public ST_RDSEquipo() { }
    }

    public class ST_RDSAntena
    {
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string FREQ_INF { get; set; }
        public string FREQ_SUP { get; set; }
        public string GAIN_VALOR { get; set; }
        public string GAIN_UNIT { get; set; }
        public string G_VECES { get; set; }
        public string G_dBi { get; set; }
        public string G_dbd { get; set; }
        public string NRO_BAHIAS { get; set; }
        public string POLARIZACION { get; set; }
        public string OBSERVACION { get; set; }
        public string URL_CATALOGO_EQUIPO { get; set; }
        public ST_RDSAntena() { }
    }

    public class ST_RDSLinea
    {
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string LONG_LINEA { get; set; }
        public string LOSS_100M { get; set; }
        public string OBSERVACION { get; set; }
        public string URL_CATALOGO_EQUIPO { get; set; }
        public ST_RDSLinea() { }
    }

    public class ST_RDSMonitor
    {
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string FREQ_INF { get; set; }
        public string FREQ_SUP { get; set; }
        public string OBSERVACION { get; set; }
        public string URL_CATALOGO_EQUIPO { get; set; }
        public ST_RDSMonitor() { }
    }

    public class ST_RDSOtrosEquipos
    {
        public int ITEM { get; set; }
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string OBSERVACION { get; set; }
        public ST_RDSOtrosEquipos() { }
    }

    public class ST_RDSEquiposRedCobertura
    {
        public ST_RDSEquipo EQUIPO_TX { get; set; }
        public ST_RDSAntena ANTENA_TX { get; set; }
        public ST_RDSLinea LINEA_TX { get; set; }
        public ST_RDSMonitor MONITOR_MODULACION { get; set; }
        public ST_RDSMonitor MONITOR_FREQ { get; set; }
        public int EQUIPOS_ADICIONALES { get; set; }
        public List<ST_RDSOtrosEquipos> OTROS_EQUIPOS { get; set; }
        public ST_RDSEquiposRedCobertura() { }
    }

    public class ST_RDSEquiposRedPtoAPto
    {
        public ST_RDSEquipo EQUIPO_TX { get; set; }
        public ST_RDSEquipo EQUIPO_RX { get; set; }
        public ST_RDSAntena ANTENA_TX { get; set; }
        public ST_RDSAntena ANTENA_RX { get; set; }
        public ST_RDSLinea LINEA_TX { get; set; }
        public int EQUIPOS_ADICIONALES { get; set; }
        public List<ST_RDSOtrosEquipos> OTROS_EQUIPOS { get; set; }
        public ST_RDSEquiposRedPtoAPto() { }
    }

    public class ST_RDSSoloSistemaRadiante
    {
        public ST_RDSSistemaRadiante SISTEMA_RADIANTE { get; set; }
        public ST_RDSPatronRadiacion PATRON_RADIACION { get; set; }
        public ST_RDSEnlacePtoaPto ENLACE_PTOAPTO { get; set; }
        public ST_RDSSoloSistemaRadiante() { }
    }

    public class ST_TreeExcel
    {
        public string text { get; set; }
        public string value { get; set; }
        public string Observaciones { get; set; }
        public List<ST_TreeExcel> items { get; set; }

        public ST_TreeExcel() { }
    }
}