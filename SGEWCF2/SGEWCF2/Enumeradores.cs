﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGEWCF2
{
    //Estados que puede tener una solicitud de Radiodifusión
    public enum SOL_STATES
    {
        SinRadicar = 1,  //El Usuario agregó la información a una solicitud, pero aún no ha generado el radicado.
        Radicada,        //Solicitud en espera de ser tramitada, después de haber sido radicada.
        EnTramite,       //La solicitud está siendo tramitada por el ministerio. Sin aprobación.
        Requerido,       //La documentación fue solicitada de cambio.
        Subsanado,       //La documentación fue cambiada por el solicitante.
        EnCierre,         //La solicitud ha sido tramitada completamente.
        Finalizada,      //La respuesta del trámite de la solicitud fue aceptada por el solicitante.
        Cancelada,       //La solicitud fue cancelada por el solicitante.,
        Rechazada,        //La solicitud fue rechazada
        Desistida         //La solicitud no fue subsanada a tiempo
    }



    //Tipos de Solicitudes de Radiodifusión  
    public enum SOL_TYPES
    {
        ComunicacionArriendoEmisora = 1,
        JuntaDeProgramacion = 2,
        ManualDeEstilo = 3,
        FusionesEIntegraciones = 4,
        SuspensionesTemporales = 5,
        ArchivoDeLaConcesion = 6,
        ProrrogaConcesion = 7,
        ModParametrosTecnicos = 8,
        Interferencias = 9,
        CesionExpediente = 10,
        RedTransmovil = 11,
        CancelacionRedes = 12,
        OtorgaEmisoraComercial = 14,
        OtorgaEmisoraComunitaria = 15,
        OtorgaEmisoraComunitariaGruposEtnicos = 16,
        OtorgaEmisoraDeInteresPublico = 17,
    }

    public enum SOL_TYPES_CLASS
    {
        AdministrativoOnly = 1,
        AdministrativoTecnico
    }

    //Tipos de anexos Solicitudes de Radiodifusión  
    public enum TiposDeAnexoSolicitudRDS
    {
        FormatoArchivoDeLaConcesion = 1,
        FormatoArriendoEmisora = 2,
        FormatoFusionEIntegracion = 3,
        FormatoJuntaProgramacion = 4,
        FormatoSuspencionTemporal = 5,
        Contrato = 6,
        ActaFirmadaJunta = 7,
        ParrillaProgramacion = 8,
        SoporteSonoro = 9,
        EvidenciaSuspencion = 10,
        FirmaDigital = 11,
        DatosProveedorIntegrarArrendador = 12,
        FormatoManualEstilo = 13,
        FormatoInterferencia = 14,
        CartaProrroga = 15,
        CartaCanFrecEnlace = 16,
        PromesaNegocio = 17,
        MatriculaProfContador = 18,
        EstadosFinancierosCesionario = 19,
        FotocopiaCedua = 20,
        CartaCesionExpediente = 21,
        CertExistenciaRepLegal = 22,
        DatosCalculoCesion = 23,
        estudioTecnicoCub = 26,
        certificadoAeronauticaCivilCub = 30,
        certificadoPlaneacionMunicipalCub = 31,
        catalogoEquiposCub = 32,
        formatoModificacionCub = 33,
        catalogoEquiposPAP = 40,
        formatoModificacionPAP = 41,
        formatoCancelacion = 42,
        CatalogoEquipo = 45,
        FormatoSolicitudModifEquipos = 46,
        FormatoRedTransmovil = 52,
        PdfAdjunto = 53,
        //OtorgaEstudioTecnico = 63,
        //OtorgaConceptoAeronautica = 64,
        //OtorgaPlaneacionMunicipal = 65,
        //OtorgaAnexoTecnico = 66,
        //OtorgaManualEstilo = 67,
        //OtorgaJuntaProgramacion = 68
        OtorgaEstudioTecnico = 57,
        OtorgaConceptoAeronautica = 58,
        OtorgaPlaneacionMunicipal = 59,
        OtorgaAnexoTecnico = 60,
        OtorgaManualEstilo = 61,
        OtorgaJuntaProgramacion = 62

    }


    public enum ROLE1_STATE
    {
        SinDefinir = 0,
        Radicado,
        Requerido,   
        Subsanado,
        Aprobado,    
        Rechazado,
        NoRevision
    }

    public enum ROLE248_STATE
    {
        SinDefinir = 0,
        Aceptado,
        Devuelto,
        Revision
    }


    public enum REASIGN_TYPE
    {
        Solicitar = 1,
        Rechazar,
        Aprobar,
        Reasignar       //Aprobar sin solicitud
    }

    public enum DOC_CLASS
    {
        Radicado = 1,
        Registro
    }

    public enum DOC_TYPE
    {
        Solicitud = 1,
        AdminSubsanacion,
        AdminRequermiento,
        AdminRechazo,
        AdminAprobacion,
        Cancelacion,
        TechDevolucion,
        TechSubsanacion,
        TechRequermiento,
        TechRechazo,
        TechAprobacion,
        Aplazamiento,
        Desistimiento
    }

    public enum GROUPS
    {
        NoAplica = -1,
        Concesionario = 0,
        MinTIC,
        ANE
    }

    public enum ROLES
    {
        NoAplica = -1,
        Concesionario = 0,
        Funcionario = 1,
        Revisor = 2,
        Coordinador = 4,
        Subdirector = 8,
        Asesor = 16,
        Director = 32,
        Administrador = 128
    }

    public enum STEPS
    {
        EsViable = -3,
        Viabilidad = -2,
        NoAplica = -1,
        Administrativo = 0,
        Tecnico,
        Resolucion,
        EnCierre,
        Terminada
    }

    public enum DOC_SOURCE
    {
        Concesionario = 0,
        MinTIC,
        ANE
    }

    public enum LOG_SOURCE
    {
        Concesionario = 0,
        MinTIC,
        ANE
    }


    public enum COM_CLASS
    {
        Administrativo = 1,
        Tecnico,
        Revision,
        Desistimiento,
        Viabilidad
    }

    public enum COM_TYPE
    {
        Aprobado = 1,
        Requerido,
        Rechazado,
        Devuelto,
        Desistimiento
    }

    public enum MAIL_TYPE
    {
        MINTIC_SOL_SUBSANADA = 1,
        MINTIC_SOL_ASSIGNED,
        MINTIC_SOL_CANCELADA,
        CONCESIONARIO_SOL_REQUERIDA,
        CONCESIONARIO_SOL_CREADA,
        CONCESIONARIO_SOL_SUBSANADA,
        CONCESIONARIO_SOL_CANCELADA,
        ANALISIS_ADMINISTRATIVO_DEVUELTO,
        ANALISIS_TECNICO_DEVUELTO,
        ANE_SOL_ASSIGNED,
        ANALISIS_TECNICO_DEVUELTO_TO_ANE,
        ANALISIS_TECNICO_FINALIZADO,
        ANALISIS_RESOLUCION_DEVUELTO,
        RESET_PASSWORD,
        MINTIC_SOL_APLAZADA,
        CONCESIONARIO_SOL_APLAZADA,
        MINTIC_SOL_DESISTIDA,
        CONCESIONARIO_SOL_DESISTIDA,
        REGISTRO_USUARIO,
        ASIGNACION_CANAL,
    }

    public enum SOL_CREATOR
    {
        Concesionario = 0,
        FuncionarioMinTIC
    }

    public enum DOC_TECH_TYPE
    {
        Soporte = 1,
        Concepto,
        Cuadro
    }

    public enum EstadoViabilidad
    {
        SinSolicitudes = 0,
        EnTramite = 1,
        ViabilidadPendiente = 2,
        Asignado = 3,
        Desierto = 4,
    }
  
}