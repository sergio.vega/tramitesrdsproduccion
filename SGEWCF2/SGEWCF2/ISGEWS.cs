﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SGEWCF2
{

    [ServiceContract]
    public interface ISGEWS
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Autenticar", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSUsuarioEx Autenticar(string USR_LOGIN, string USR_PASSWORD, string APP_VERSION);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AutenticarFO", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSUsuarioEx AutenticarFO(string CREDENCIALES_FO, string APP_VERSION);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ResetUsuarioPassword", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ResetUsuarioPassword(int IDUserWeb, ST_RDSUsuario User);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CambioUsuarioPassword", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CambioUsuarioPassword(int IDUserWeb, ST_RDSUsuario User, string USR_NEW_PASSWORD);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetMainData", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSMainData GetMainData();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetSolicitudesConcesionario", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSSolicitudes GetSolicitudesConcesionario(string Nit);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetExpedientes", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSExpedientes GetExpedientes(string Nit);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetExpediente", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSExpediente GetExpediente(string ExpedienteCodigo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearConcesionarioSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CrearConcesionarioSolicitud(ST_RDSTipoSolicitud SOL_TYPE, ST_RDSExpediente Expediente, ST_RDSContacto Contacto, List<string> Emails, List<ST_RDSFiles_Up> Files);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetRadicado", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResult GetRadicado(string RadicadoCodigo);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearFuncionarioSolicitudProrroga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CrearFuncionarioSolicitudProrroga(int IDUserWeb, string RadicadoCodigo, string RadicadoFecha, ST_RDSExpediente Expediente, bool bEnviarCorreoConcesionario, List<ST_RDSFiles_Up> Files);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetUsuarios", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSUsuario> GetUsuarios(int USR_ID_FILTER, int USR_GROUP_FILTER, int USR_ROLE_FILTER, int USR_STATUS_FILTER);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetSolicitudesMinTIC", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSSolicitudesMintic GetSolicitudesMinTIC(int IDUserWeb, int USR_GROUP, int USR_ROLE, int SOL_ENDED, string DateIni, string DateEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetAnalisisFinanciero", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResult GetAnalisisFinanciero(ST_RDSSolicitud Solicitud);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GuardarTramiteAdministrativo", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo GuardarTramiteAdministrativo(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ReasignarSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ReasignarSolicitud(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSReasignUp Reasign);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDUsuario", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDUsuario(int IDUserWeb, ST_RDSUsuario User);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SubsanarConcesionarioSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo SubsanarConcesionarioSolicitud(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDUsuarioSignature", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDUsuarioSignature(int IDUserWeb, ST_RDSUsuario User, string FileSignature);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CancelarConcesionarioSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CancelarConcesionarioSolicitud(ST_RDSSolicitud Solicitud);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConteoDevoluciones", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSConteoDevoluciones> GetConteoDevoluciones(int USR_GROUP, int USR_ROLE, string DateIni, string DateEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GuardarTramiteTecnico", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo GuardarTramiteTecnico(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado, List<ST_RDSFiles_Up> Files);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetEventLogs", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSLog> GetEventLogs(Guid SOL_UID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetTexts", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSText> GetTexts();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDTexts", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDTexts(int IDUserWeb, ST_RDSText Text);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDDoc", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDDoc(int IDUserWeb, ST_RDSDoc Docum);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDComen", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDComen(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSComen Comen);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetImages", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSImage> GetImages();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDImages", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDImages(int IDUserWeb, ST_RDSImage Image);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetResolucionesPlantillas", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSResPlt> GetResolucionesPlantillas(int SOL_TYPE_ID_FILTER, int RES_PLT_STATUS_FILTER);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDResolucionesPlantillas", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDResolucionesPlantillas(int IDUserWeb, ST_RDSResPlt ResPlt);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetResolucionesCombos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResCombos GetResolucionesCombos();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ReplaceResolucionPlantilla", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResPlt ReplaceResolucionPlantilla(ST_RDSResPlt ResPlt, ST_RDSSolicitud Solicitud);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDResolucion", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDResolucion(int IDUserWeb,  ST_RDSSolicitud Solicitud, ST_RDSResolucion Resolucion);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetResolucion", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResolucion GetResolucion(Guid SOL_UID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetAlarms", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSAlarm> GetAlarms();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDAlarms", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDAlarms(int IDUserWeb, ST_RDSAlarm Alarm);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "AplazarConcesionarioSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo AplazarConcesionarioSolicitud(ST_RDSSolicitud Solicitud);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ProcesarDesistimientos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        bool ProcesarDesistimientos();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetEvents", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSEvent> GetEvents(string DateIni, string DateEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConectionsHeader", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSConectionHeader> GetConectionsHeader(string DateIni, string DateEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConectionsDetails", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSConectionDetails> GetConectionsDetails(int USR_ID, string DateIni, string DateEnd);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetDatosCreacionOtorgaEmisora", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSCreacionOtorga GetDatosCreacionOtorgaEmisora(int SOL_TYPE_ID, Guid? SOL_UID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetMunicipiosOtorga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_Lugar> GetMunicipiosOtorga(int CODE_AREA_DEPARTAMENTO, int SOL_TYPE_ID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetDistintivos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_PlanBroadcast> GetDistintivos(int CODE_AREA_MUNICIPIO, int SOL_TYPE_ID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearConcesionarioSolicitudOtorga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CrearConcesionarioSolicitudOtorga(int IDUserWeb, ST_RDSSolicitud SolicitudTemp, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, ST_RDSContacto Contacto, List<string> Emails, 
            List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia, 
            string CallSign, ST_RDSDatosComunidad DatosComunidad);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GuardarConcesionarioSolicitudOtorga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo GuardarConcesionarioSolicitudOtorga(int IDUserWeb, ST_RDSSolicitud SolicitudTemp, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, ST_RDSContacto Contacto, List<string> Emails,
            List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia,
            string CallSign, ST_RDSDatosComunidad DatosComunidad);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetCanalesProcesos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSCanalProcesoOtorga> GetCanalesProcesos(int IDUserWeb, int SOL_TYPE);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetViabilidades", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSViabilidad> GetViabilidades(int IDUserWeb, ST_RDSConfiguracionProcesoOtorga ConfiguracionProcesoOtorga);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetSolicitudesCanal", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_InfoSolicitudesCanal GetSolicitudesCanal(int IDUserWeb, int USR_GROUP, int USR_ROLE, string CALL_SIGN);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDResolucionViabilidad", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDResolucionViabilidad(int IDUserWeb, ST_RDSSolicitud Solicitud, ST_RDSResolucion ResolucionViabilidad);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "OtorgaGuardarTramiteAdministrativo", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo OtorgaGuardarTramiteAdministrativo(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "DarViabilidadSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo DarViabilidadSolicitud(int IDUserWeb, ST_RDSSolicitud Solicitud, string RazonDesempate);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetResolucionViabilidad", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResolucion GetResolucionViabilidad(Guid SOL_UID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetDepartamentos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_Lugar> GetDepartamentos();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetMunicipios", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_Lugar> GetMunicipios(int CODE_AREA_DEPARTAMENTO);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ExisteManifestacionUsuario", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_ResultExisteManifestacionUsuario ExisteManifestacionUsuario(string IDENT);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearUsuario", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CrearUsuario(STV_RDSInfoRegistroUsuario infoUsuario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConfiguracionesProcesos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_InfoConfiguracionesProcesos GetConfiguracionesProcesos(int IDUserWeb);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ActualizarConfiguracionProceso", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ActualizarConfiguracionProceso(ST_RDSConfiguracionProcesoOtorga ConfiguracionProceso);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CrearFuncionarioSolicitudOtorga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CrearFuncionarioSolicitudOtorga(int IDUserWeb, ST_RDSTipoSolicitud SOL_TYPE, int LIC_NUMBER, string NitOperador, ST_PlanBroadcast Plan, List<string> Emails,
             List<ST_RDSFiles_Up> Files, List<ST_RDSFiles_Up> FilesPond, string NombreDepartamentoPlanBro, string NombreMunicipioPlanBro, int CodeAreaMunicipioPlanBro, string GestionServicio, string Clasificacion, string NivelCubrimiento, string Tecnologia,
            string CallSign, ST_RDSDatosComunidad DatosComunidad, string RadicadoCodigo, string RadicadoFecha, bool bEnviarCorreoConcesionario);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetOperador", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSOperador GetOperador(string NitOperador);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConfiguracionCalificacionesSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        string GetConfiguracionCalificacionesSolicitud(int SOL_TYPE_ID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ActualizarCalificacionSolicitud", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ActualizarCalificacionSolicitud(int IDUserWeb, Guid SOL_UID, string CalificacionSolicitud, decimal Puntaje);
        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SubsanarConcesionarioOtorgaTecnico", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo SubsanarConcesionarioOtorgaTecnico(ST_RDSSolicitud Solicitud, List<ST_RDSFiles_Up> Files);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GuardarOtorgaTramiteTecnico", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo GuardarOtorgaTramiteTecnico(int IDUserWeb, int ROLE, ST_RDSSolicitud Solicitud, bool bGenerarComunicado, List<ST_RDSFiles_Up> Files);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetConfiguracionObservaciones", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSConfigObservacionOtorga GetConfiguracionObservaciones(int IDUserWeb);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetObservacionesMinTIC", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSObservaciones GetObservacionesMinTIC(int IDUserWeb, int USR_ROLE);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ResponderObservacionOtorga", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ResponderObservacionOtorga(ST_RDSObservacion Observacion, bool RespondePregunta, string estadoObs);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDCategoria", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDCategoria(int IDUserWeb, ST_ObservacionesCategorias Categoria);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDConvocatorias", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDConvocatorias(int IDUserWeb, ST_RDSObservacionConvocatoriaOtorga Convocatoria);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ReasignarObservacion", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo ReasignarObservacion(int IDUserWeb, ST_RDSObservacion Observacion, ST_RDSReasignUp Reasign);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GenerarInformeRespuestasObservaciones", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        byte[] GenerarInformeRespuestasObservaciones(int IDUserWeb, ST_RDSObservacionConvocatoriaOtorga Convocatoria);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetObservacionesPlantillas", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        List<ST_RDSObsPlt> GetObservacionesPlantillas();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "CRUDObservacionesPlantillas", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ActionInfo CRUDObservacionesPlantillas(int IDUserWeb, ST_RDSObsPlt ObsPlt);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetObservacionesCombos", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSObsCombos GetObservacionesCombos();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ReplaceObservacionesPlantilla", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSObsPlt ReplaceObservacionesPlantilla(ST_RDSObsPlt ObsPlt, string idConvocatoria);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "VerificarCarteraAlDia", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResult VerificarCarteraAlDia(string CUS_IDENT);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "VerificarCarteraAlDiaInterno", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ST_RDSResult VerificarCarteraAlDiaInterno(string CUS_IDENT);
        
    }

}
